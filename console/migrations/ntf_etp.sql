--etp | one
DROP TABLE IF EXISTS "public"."ntf_etp";
CREATE TABLE "public"."ntf_etp" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  --TODO:редко EF
  "code"        TEXT COLLATE "default",
  --TODO:редко EF
  "name"        TEXT COLLATE "default",
  --TODO:редко EF
  "url"         TEXT COLLATE "default"

) WITH (OIDS = FALSE
);

--ALTER TABLE "public"."ntf_etp"
--  ADD FOREIGN KEY ("ntf_main_id")
--REFERENCES "public"."ntf_main" ("id")
--ON DELETE CASCADE ON UPDATE CASCADE;
--ALTER TABLE "public"."ntf_etp"
--  ADD UNIQUE ("ntf_main_id");