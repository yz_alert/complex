--procedure_info_collecting | one
DROP TABLE IF EXISTS "public"."ntf_procedure_info_collecting";
CREATE TABLE "public"."ntf_procedure_info_collecting" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "start_date"  TEXT COLLATE "default",
  "place"       TEXT COLLATE "default",
  "ntf_order"   TEXT COLLATE "default",
  "end_date"    TEXT COLLATE "default",
  --TODO:редко PO
  "form"        TEXT COLLATE "default"
) WITH (OIDS = FALSE
);


