--purchase_documentation | one
DROP TABLE IF EXISTS "public"."ntf_purchase_documentation";
CREATE TABLE "public"."ntf_purchase_documentation" (
  "id"                          SERIAL PRIMARY KEY,
  "ntf_main_id"                 INT4,

  "grant_start_date"            TEXT COLLATE "default",
  "grant_place"                 TEXT COLLATE "default",
  "grant_order"                 TEXT COLLATE "default",
  "languages"                   TEXT COLLATE "default",
  "grant_means"                 TEXT COLLATE "default",
  "grant_end_date"              TEXT COLLATE "default",

  "pay_currency_code"           TEXT COLLATE "default",
  "pay_currency_name"           TEXT COLLATE "default",

  "pay_info_part"               TEXT COLLATE "default",
  "pay_info_procedure_info"     TEXT COLLATE "default",
  "pay_info_settlement_account" TEXT COLLATE "default",
  "pay_info_personal_account"   TEXT COLLATE "default",
  "pay_info_bik"                TEXT COLLATE "default",

  "pay_info_pay_currency_code"  TEXT COLLATE "default",
  "pay_info_pay_currency_name"  TEXT COLLATE "default"
) WITH (OIDS = FALSE
);


