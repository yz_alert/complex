--placing_way | one
DROP TABLE IF EXISTS "public"."ntf_placing_way";
CREATE TABLE "public"."ntf_placing_way" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "code"        TEXT COLLATE "default",
  "name"        TEXT COLLATE "default"
) WITH (OIDS = FALSE
);


