--customer_quantity | many
DROP TABLE IF EXISTS "public"."ntf_customer_quantity";
CREATE TABLE "public"."ntf_customer_quantity" (
  "id"                         SERIAL PRIMARY KEY,
  "ntf_purchase_object_id"     INT4,

  "customer_reg_num"           TEXT COLLATE "default",
  "customer_cons_registry_num" TEXT COLLATE "default",
  "customer_full_name"         TEXT COLLATE "default",

  "quantity"                   TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


