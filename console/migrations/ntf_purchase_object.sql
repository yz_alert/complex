--purchase_object | many
DROP TABLE IF EXISTS "public"."ntf_purchase_object";
CREATE TABLE "public"."ntf_purchase_object" (
  "id"                 SERIAL PRIMARY KEY,
  "ntf_lot_id"         INT4,

  "okpd_code"          TEXT COLLATE "default",
  "okpd_name"          TEXT COLLATE "default",

  "okpd2_code"         TEXT COLLATE "default",
  "okpd2_name"         TEXT COLLATE "default",

  "name"               TEXT COLLATE "default",

  "okei_code"          TEXT COLLATE "default",
  "okei_national_code" TEXT COLLATE "default",

  "price"              TEXT COLLATE "default",

  "quantity_value"     TEXT COLLATE "default",
  "quantity_undefined" TEXT COLLATE "default",

  "sum"                TEXT COLLATE "default"
) WITH (OIDS = FALSE
);



