--drug_quantity_customer_info | many
DROP TABLE IF EXISTS "public"."ntf_drug_quantity_customer_info";
CREATE TABLE "public"."ntf_drug_quantity_customer_info" (
  "id"                               SERIAL PRIMARY KEY,
  "ntf_drug_purchase_object_info_id" INT4,

  "customer_reg_num"                 TEXT COLLATE "default",
  "customer_cons_registry_num"       TEXT COLLATE "default",
  "customer_full_name"               TEXT COLLATE "default",
  "quantity"                         TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


