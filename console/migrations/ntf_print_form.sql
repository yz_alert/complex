--print_form | one
DROP TABLE IF EXISTS "public"."ntf_print_form";
CREATE TABLE "public"."ntf_print_form" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "url"         TEXT COLLATE "default"
  --"print_form_signature_type" TEXT COLLATE "default"
) WITH (OIDS = FALSE
);


