--object_info_using_reference_info | one
DROP TABLE IF EXISTS "public"."ntf_object_info_using_reference_info";
CREATE TABLE "public"."ntf_object_info_using_reference_info" (
  "id"                                                      SERIAL PRIMARY KEY,
  "ntf_drug_purchase_object_info_id"                        INT4,

  "drug_info_mmn_info_mmn_external_code"                    TEXT COLLATE "default",
  "drug_info_mmn_info_mmn_name"                             TEXT COLLATE "default",

  "drug_info_trade_info_position_trade_name_external_code"  TEXT COLLATE "default",
  "drug_info_trade_info_trade_name"                         TEXT COLLATE "default",

  "drug_info_medicamental_form_info_medicamental_form_name" TEXT COLLATE "default",

  "drug_info_dosage_info_dosage_grls_value"                 TEXT COLLATE "default",
  "drug_info_dosage_info_dosage_user_okei_code"             TEXT COLLATE "default",
  "drug_info_dosage_info_dosage_user_okei_name"             TEXT COLLATE "default",

  "drug_info_packaging_info_packaging1_quantity"            TEXT COLLATE "default",
  "drug_info_packaging_info_packaging2_quantity"            TEXT COLLATE "default",
  "drug_info_packaging_info_summary_packaging_quantity"     TEXT COLLATE "default",

  "drug_info_manual_user_okei_code"                         TEXT COLLATE "default",
  "drug_info_manual_user_okei_name"                         TEXT COLLATE "default",

  "drug_info_basic_unit"                                    TEXT COLLATE "default",
  "drug_info_drug_quantity"                                 TEXT COLLATE "default",

  "must_specify_drug_package_specify_drug_package_reason"   TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


