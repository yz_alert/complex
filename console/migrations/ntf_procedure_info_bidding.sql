--procedure_info_bidding | one
--TODO:редко EF, ZakA
DROP TABLE IF EXISTS "public"."ntf_procedure_info_bidding";
CREATE TABLE "public"."ntf_procedure_info_bidding" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "date"        TEXT COLLATE "default",
  --TODO:редко 2 (ZakA)
  "place"       TEXT COLLATE "default",
  "add_info"    TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


ALTER TABLE "public"."ntf_procedure_info_bidding"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_bidding"
  ADD UNIQUE ("ntf_main_id");