--okpd2 | one
DROP TABLE IF EXISTS "public"."ntf_okpd2";
CREATE TABLE "public"."ntf_okpd2" (
  "id"         SERIAL PRIMARY KEY,
  "ntf_lot_id" INT4,

  "code"       TEXT COLLATE "default",
  "name"       TEXT COLLATE "default"

) WITH (OIDS = FALSE
);

--ALTER TABLE "public"."ntf_okpd2"
--  ADD FOREIGN KEY ("ntf_lot_id")
--REFERENCES "public"."ntf_lot" ("id")
--ON DELETE CASCADE ON UPDATE CASCADE;
--ALTER TABLE "public"."ntf_okpd2"
--  ADD UNIQUE ("ntf_lot_id");

