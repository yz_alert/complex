--drug_purchase_object_info | many
DROP TABLE IF EXISTS "public"."ntf_drug_purchase_object_info";
CREATE TABLE "public"."ntf_drug_purchase_object_info" (
  "id"                                 SERIAL PRIMARY KEY,
  "ntf_lot_id"                         INT4,

  "isznvlp"                            TEXT COLLATE "default",
  --TODO:убрать общую сумму
  "drug_quantity_customers_info_total" TEXT COLLATE "default",

  "price_per_unit"                     TEXT COLLATE "default",
  "position_price"                     TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


