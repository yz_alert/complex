--procedure_info_contracting | one
--TODO:редко 2 (ZK,PO)
DROP TABLE IF EXISTS "public"."ntf_procedure_info_contracting";
CREATE TABLE "public"."ntf_procedure_info_contracting" (
  "id"               SERIAL PRIMARY KEY,
  "ntf_main_id"      INT4,

  "contracting_term" TEXT COLLATE "default",
  "evade_conditions" TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


ALTER TABLE "public"."ntf_procedure_info_contracting"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_contracting"
  ADD UNIQUE ("ntf_main_id");