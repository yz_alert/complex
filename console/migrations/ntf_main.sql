--ntf_main | one
DROP TABLE IF EXISTS "public"."ntf_main";
CREATE TABLE "public"."ntf_main" (
  "id"                                    SERIAL PRIMARY KEY,
  "ntf_id"                                TEXT COLLATE "default",
  "type"                                  TEXT COLLATE "default",
  "external_id"                           TEXT COLLATE "default",
  "purchase_number"                       TEXT COLLATE "default",
  "direct_date"                           TEXT COLLATE "default",
  "doc_publish_date"                      TEXT COLLATE "default",
  "doc_number"                            TEXT COLLATE "default",
  "href"                                  TEXT COLLATE "default",

  "purchase_object_info"                  TEXT COLLATE "default",
  "purchase_responsible_responsible_role" TEXT COLLATE "default",

  "okpd2okved2"                           TEXT COLLATE "default",
  --TODO:редко ZK
  "contract_service_info"                 TEXT COLLATE "default",
  "filename"                              TEXT COLLATE "default",
  "created_at"                            TIMESTAMP(0) DEFAULT NULL

) WITH (OIDS = FALSE
);