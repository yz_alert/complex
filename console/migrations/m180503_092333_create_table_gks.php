<?php

use yii\db\Migration;

/**
 * Class m180503_092333_create_table_gks
 */
class m180503_092333_create_table_gks extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        $this->createTable('{{%gks}}', [
            'id' => $this->primaryKey(),
            'inn' => $this->text()->notNull(),
            'year' => $this->text()->notNull(),
            'response' => $this->text(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->execute('ALTER TABLE "gks" ADD CONSTRAINT "gks_inn_year_key" UNIQUE ("inn", "year");');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%gks}}');
    }
}
