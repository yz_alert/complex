--budget_financing | many
DROP TABLE IF EXISTS "public"."ntf_budget_financing";
CREATE TABLE "public"."ntf_budget_financing" (
  "id"                          SERIAL PRIMARY KEY,
  "ntf_customer_requirement_id" INT4,

  "kbk_code"                    TEXT COLLATE "default",
  "kbk_code2016"                TEXT COLLATE "default",
  "year"                        TEXT COLLATE "default",
  "sum"                         TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


