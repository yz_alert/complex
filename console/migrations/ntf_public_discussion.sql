--public_discussion | one
DROP TABLE IF EXISTS "public"."ntf_public_discussion";
CREATE TABLE "public"."ntf_public_discussion" (
  "id"                                                            SERIAL PRIMARY KEY,
  "ntf_lot_id"                                                    INT4,

  "number"                                                        TEXT COLLATE "default",
  "organization_ch5_st15"                                         TEXT COLLATE "default",
  "href"                                                          TEXT COLLATE "default",
  "place"                                                         TEXT COLLATE "default",

  "public_discussion2017_p_d_l_p_p2_protocol_date"                TEXT COLLATE "default",
  "public_discussion2017_p_d_l_p_p2_protocol_publish_date"        TEXT COLLATE "default",
  "public_discussion2017_p_d_l_p_p2_public_discussion_phase2_num" TEXT COLLATE "default",
  "public_discussion2017_p_d_l_p_p2_href_phase2"                  TEXT COLLATE "default"

  --attachment | many
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_published_content_id" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_file_name" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_file_size" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_doc_description" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_doc_date" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_url" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_content_id" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_content" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_crypto_signs_signature_type" TEXT COLLATE "default",
  --"public_discussion2017_public_discussion_large_purchase_phase2_attachments_attachment_placing_date" TEXT COLLATE "default",


) WITH (OIDS = FALSE
);