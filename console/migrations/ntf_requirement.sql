--requirement | many
DROP TABLE IF EXISTS "public"."ntf_requirement";
CREATE TABLE "public"."ntf_requirement" (
  "id"         SERIAL PRIMARY KEY,
  "ntf_lot_id" INT4,

  "code"       TEXT COLLATE "default",
  "short_name" TEXT COLLATE "default",
  "name"       TEXT COLLATE "default",
  "content"    TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


