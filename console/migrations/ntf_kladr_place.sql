--kladr_place | many
--TODO: проработать эту таблицу
DROP TABLE IF EXISTS "public"."ntf_kladr_place";
CREATE TABLE "public"."ntf_kladr_place" (
  "id"                                        SERIAL PRIMARY KEY,
  "ntf_customer_requirement_id"               INT4,

  "kladr_kladr_type"                          TEXT COLLATE "default",
  "kladr_kladr_code"                          TEXT COLLATE "default",
  "kladr_full_name"                           TEXT COLLATE "default",

  "country_country_code"                      TEXT COLLATE "default",
  "country_country_full_name"                 TEXT COLLATE "default",

  --TODO:редко 3 (EP,EF,ZakA)
  "no_kladr_for_region_settlement"            TEXT COLLATE "default",

  "delivery_place"                            TEXT COLLATE "default",

  "no_kladr_for_region_settlement_region"     TEXT COLLATE "default",
  "no_kladr_for_region_settlement_settlement" TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


