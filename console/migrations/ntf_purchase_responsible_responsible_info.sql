--purchase_responsible_responsible_info | one
DROP TABLE IF EXISTS "public"."ntf_purchase_responsible_responsible_info";
CREATE TABLE "public"."ntf_purchase_responsible_responsible_info" (
  "id"                         SERIAL PRIMARY KEY,
  "ntf_main_id"                INT4,

  "org_post_address"           TEXT COLLATE "default",
  "org_fact_address"           TEXT COLLATE "default",

  "contact_person_last_name"   TEXT COLLATE "default",
  "contact_person_first_name"  TEXT COLLATE "default",
  "contact_person_middle_name" TEXT COLLATE "default",

  "contact_email"              TEXT COLLATE "default",
  "contact_phone"              TEXT COLLATE "default",
  "contact_fax"                TEXT COLLATE "default",
  "add_info"                   TEXT COLLATE "default"
) WITH (OIDS = FALSE
);


