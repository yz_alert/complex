--modification | one
DROP TABLE IF EXISTS "public"."ntf_modification";
CREATE TABLE "public"."ntf_modification" (
  "id"                                                                    SERIAL PRIMARY KEY,
  "ntf_lot_id"                                                            INT4,

  "modification_number"                                                   TEXT COLLATE "default",
  "info"                                                                  TEXT COLLATE "default",
  "add_info"                                                              TEXT COLLATE "default",

  "reason_responsible_decision_decision_date"                             TEXT COLLATE "default",

  "reason_authority_prescription_reestr_prescription_check_result_number" TEXT COLLATE "default",
  "reason_authority_prescription_reestr_prescription_prescription_number" TEXT COLLATE "default",
  "reason_authority_prescription_reestr_prescription_foundation"          TEXT COLLATE "default",
  "reason_authority_prescription_reestr_prescription_authority_name"      TEXT COLLATE "default",
  "reason_authority_prescription_reestr_prescription_doc_date"            TEXT COLLATE "default",

  "reason_authority_prescription_external_prescription_authority_name"    TEXT COLLATE "default",
  "reason_authority_prescription_external_prescription_authority_type"    TEXT COLLATE "default",
  "reason_authority_prescription_external_prescription_doc_name"          TEXT COLLATE "default",
  "reason_authority_prescription_external_prescription_doc_date"          TEXT COLLATE "default",
  "reason_authority_prescription_external_prescription_doc_number"        TEXT COLLATE "default",

  "reason_court_decision_court_name"                                      TEXT COLLATE "default",
  "reason_court_decision_doc_name"                                        TEXT COLLATE "default",
  "reason_court_decision_doc_date"                                        TEXT COLLATE "default",
  "reason_court_decision_doc_number"                                      TEXT COLLATE "default",

  "reason_discussion_result_doc_name"                                     TEXT COLLATE "default",
  "reason_discussion_result_doc_date"                                     TEXT COLLATE "default",
  "reason_discussion_result_doc_number"                                   TEXT COLLATE "default"

) WITH (OIDS = FALSE
);
