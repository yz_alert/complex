--restriction | many
DROP TABLE IF EXISTS "public"."ntf_restriction";
CREATE TABLE "public"."ntf_restriction" (
  "id"         SERIAL PRIMARY KEY,
  "ntf_lot_id" INT4,

  "short_name" TEXT COLLATE "default",
  "name"       TEXT COLLATE "default",
  "content"    TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


