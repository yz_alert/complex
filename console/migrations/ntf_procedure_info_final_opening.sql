--procedure_info_final_opening | one
--TODO:редко ZP
DROP TABLE IF EXISTS "public"."ntf_procedure_info_final_opening";
CREATE TABLE "public"."ntf_procedure_info_final_opening" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "date"        TEXT COLLATE "default",
  "place"       TEXT COLLATE "default",
  "add_info"    TEXT COLLATE "default"

) WITH (OIDS = FALSE
);

--ALTER TABLE "public"."ntf_procedure_info_final_opening"
--  ADD FOREIGN KEY ("ntf_main_id")
--REFERENCES "public"."ntf_main" ("id")
--ON DELETE CASCADE ON UPDATE CASCADE;
--ALTER TABLE "public"."ntf_procedure_info_final_opening"
--  ADD UNIQUE ("ntf_main_id");