--nonbudget_financing | many
DROP TABLE IF EXISTS "public"."ntf_nonbudget_financing";
CREATE TABLE "public"."ntf_nonbudget_financing" (
  "id"                          SERIAL PRIMARY KEY,
  "ntf_customer_requirement_id" INT4,

  "kosgu_code"                  TEXT COLLATE "default",
  "kvr_code"                    TEXT COLLATE "default",
  "year"                        TEXT COLLATE "default",
  "sum"                         TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


