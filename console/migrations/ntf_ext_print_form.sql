--ext_print_form | one
DROP TABLE IF EXISTS "public"."ntf_ext_print_form";
CREATE TABLE "public"."ntf_ext_print_form" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "content"     TEXT COLLATE "default",
  "url"         TEXT COLLATE "default",
  --"signature_type" TEXT COLLATE "default",
  "file_type"   TEXT COLLATE "default"
  --"control_personal_signature_type" TEXT COLLATE "default",
) WITH (OIDS = FALSE
);


