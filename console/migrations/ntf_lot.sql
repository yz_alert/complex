--lot | many
DROP TABLE IF EXISTS "public"."ntf_lot";
CREATE TABLE "public"."ntf_lot" (
  "id"                                       SERIAL PRIMARY KEY,
  "ntf_main_id"                              INT4,

  "lot_number"                               TEXT COLLATE "default",
  "lot_object_info"                          TEXT COLLATE "default",
  "max_price"                                TEXT COLLATE "default",
  "max_price_info"                           TEXT COLLATE "default",
  "price_formula"                            TEXT COLLATE "default",
  "standard_contract_number"                 TEXT COLLATE "default",

  "currency_code"                            TEXT COLLATE "default",
  "currency_name"                            TEXT COLLATE "default",

  "finance_source"                           TEXT COLLATE "default",
  "interbudgetary_transfer"                  TEXT COLLATE "default",
  "quantity_undefined"                       TEXT COLLATE "default",

  --TODO:убрать общую сумму
  "purchase_objects_total_sum"               TEXT COLLATE "default",
  --TODO:убрать общую сумму
  "drug_purchase_objects_info_total"         TEXT COLLATE "default",

  "restrict_info"                            TEXT COLLATE "default",
  "restrict_foreigns_info"                   TEXT COLLATE "default",
  "add_info"                                 TEXT COLLATE "default",

  --TODO:редко 6 (EP,OKD,EF,OKOU,OK,ZK,ZP)
  "no_public_discussion"                     TEXT COLLATE "default",
  --TODO:редко 8 (EP,OKD,EF,OKOU,OK,ZK,ZP,)
  "must_public_discussion"                   TEXT COLLATE "default",

  --TODO:редко PO
  "before_pay"                               TEXT COLLATE "default",

  --TODO:редко 111
  "purchase_code"                            TEXT COLLATE "default",
  --TODO:редко 111
  "max_cost_definition_order"                TEXT COLLATE "default",
  --TODO:редко 111
  "tender_plain_info_plain_number"           TEXT COLLATE "default",
  --TODO:редко 111
  "tender_plain_info_position_number"        TEXT COLLATE "default",
  --TODO:редко 111
  "tender_plain_info_purchase83st544"        TEXT COLLATE "default",
  --TODO:редко 111
  "tender_plain_info_plan2017_number"        TEXT COLLATE "default",
  --TODO:редко 111
  "tender_plain_info_positin2017_number"     TEXT COLLATE "default",
  --TODO:редко 111
  "tender_plain_info_positin2017_ext_number" TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


