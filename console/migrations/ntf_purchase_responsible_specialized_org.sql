--purchase_responsible_specialized_org | one
DROP TABLE IF EXISTS "public"."ntf_purchase_responsible_specialized_org";
CREATE TABLE "public"."ntf_purchase_responsible_specialized_org" (
  "id"                SERIAL PRIMARY KEY,
  "ntf_main_id"       INT4,

  "reg_num"           TEXT COLLATE "default",
  "cons_registry_num" TEXT COLLATE "default",
  "full_name"         TEXT COLLATE "default",
  "short_name"        TEXT COLLATE "default",
  "post_address"      TEXT COLLATE "default",
  "fact_address"      TEXT COLLATE "default",
  "inn"               TEXT COLLATE "default",
  "kpp"               TEXT COLLATE "default"
) WITH (OIDS = FALSE
);


