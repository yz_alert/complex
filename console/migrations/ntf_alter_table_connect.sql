ALTER TABLE "public"."ntf_main"
  ADD UNIQUE ("ntf_id");

ALTER TABLE "public"."ntf_procedure_info_selecting"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_selecting"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_procedure_info_final_opening"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_final_opening"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_okpd"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_okpd"
  ADD UNIQUE ("ntf_lot_id");

ALTER TABLE "public"."ntf_okpd2"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_okpd2"
  ADD UNIQUE ("ntf_lot_id");

ALTER TABLE "public"."ntf_etp"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_etp"
  ADD UNIQUE ("ntf_main_id");
------------------------------
ALTER TABLE "public"."ntf_public_discussion"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_public_discussion"
  ADD UNIQUE ("ntf_lot_id");

ALTER TABLE "public"."ntf_restriction"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_requirement"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_purchase_responsible_specialized_org"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_purchase_responsible_specialized_org"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_purchase_responsible_responsible_org"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_purchase_responsible_responsible_org"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_purchase_responsible_responsible_info"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_purchase_responsible_responsible_info"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_purchase_responsible_last_specialized_org"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_purchase_responsible_last_specialized_org"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_purchase_object"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_purchase_documentation"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_purchase_documentation"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_procedure_info_scoring"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_scoring"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_procedure_info_prequalification"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_prequalification"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_procedure_info_opening"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_opening"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_procedure_info_collecting"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_procedure_info_collecting"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_print_form"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_print_form"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_preferense"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_placing_way"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_placing_way"
  ADD UNIQUE ("ntf_main_id");

ALTER TABLE "public"."ntf_object_info_using_text_form"
  ADD FOREIGN KEY ("ntf_drug_purchase_object_info_id")
REFERENCES "public"."ntf_drug_purchase_object_info" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_modification"
  ADD UNIQUE ("ntf_lot_id");

ALTER TABLE "public"."ntf_object_info_using_reference_info"
  ADD FOREIGN KEY ("ntf_drug_purchase_object_info_id")
REFERENCES "public"."ntf_drug_purchase_object_info" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_nonbudget_financing"
  ADD FOREIGN KEY ("ntf_customer_requirement_id")
REFERENCES "public"."ntf_customer_requirement" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_modification"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."ntf_modification"
  ADD UNIQUE ("ntf_lot_id");

ALTER TABLE "public"."ntf_kladr_place"
  ADD FOREIGN KEY ("ntf_customer_requirement_id")
REFERENCES "public"."ntf_customer_requirement" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_ext_print_form"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_drug_quantity_customer_info"
  ADD FOREIGN KEY ("ntf_drug_purchase_object_info_id")
REFERENCES "public"."ntf_drug_purchase_object_info" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_drug_purchase_object_info"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_customer_requirement"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_customer_quantity"
  ADD FOREIGN KEY ("ntf_purchase_object_id")
REFERENCES "public"."ntf_purchase_object" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_budget_financing"
  ADD FOREIGN KEY ("ntf_customer_requirement_id")
REFERENCES "public"."ntf_customer_requirement" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_attachment"
  ADD FOREIGN KEY ("ntf_lot_id")
REFERENCES "public"."ntf_lot" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "public"."ntf_lot"
  ADD FOREIGN KEY ("ntf_main_id")
REFERENCES "public"."ntf_main" ("id")
ON DELETE CASCADE ON UPDATE CASCADE;