--preferense | many
DROP TABLE IF EXISTS "public"."ntf_preferense";
CREATE TABLE "public"."ntf_preferense" (
  "id"         SERIAL PRIMARY KEY,
  "ntf_lot_id" INT4,

  "code"       TEXT COLLATE "default",
  "short_name" TEXT COLLATE "default",
  "name"       TEXT COLLATE "default",
  "pref_value" TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


