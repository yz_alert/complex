/*
Navicat PGSQL Data Transfer

Source Server         : PG_localhost
Source Server Version : 90605
Source Host           : localhost:5432
Source Database       : zcb_local
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90605
File Encoding         : 65001

Date: 2017-11-04 15:28:22
*/


-- ----------------------------
-- Sequence structure for company_card_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_card_id_seq";
CREATE SEQUENCE "public"."company_card_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 27
 CACHE 1;
SELECT setval('"public"."company_card_id_seq"', 27, true);

-- ----------------------------
-- Sequence structure for company_search_result_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."company_search_result_id_seq";
CREATE SEQUENCE "public"."company_search_result_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 43
 CACHE 1;
SELECT setval('"public"."company_search_result_id_seq"', 43, true);

-- ----------------------------
-- Sequence structure for court_arbitration_case_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."court_arbitration_case_id_seq";
CREATE SEQUENCE "public"."court_arbitration_case_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 953
 CACHE 1;
SELECT setval('"public"."court_arbitration_case_id_seq"', 953, true);

-- ----------------------------
-- Sequence structure for court_arbitration_case_members_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."court_arbitration_case_members_id_seq";
CREATE SEQUENCE "public"."court_arbitration_case_members_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2390
 CACHE 1;
SELECT setval('"public"."court_arbitration_case_members_id_seq"', 2390, true);

-- ----------------------------
-- Sequence structure for court_arbitration_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."court_arbitration_id_seq";
CREATE SEQUENCE "public"."court_arbitration_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 6
 CACHE 1;
SELECT setval('"public"."court_arbitration_id_seq"', 6, true);

-- ----------------------------
-- Sequence structure for disqualified_persons_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."disqualified_persons_id_seq";
CREATE SEQUENCE "public"."disqualified_persons_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 19201
 CACHE 1;
SELECT setval('"public"."disqualified_persons_id_seq"', 19201, true);

-- ----------------------------
-- Sequence structure for financial_statement_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."financial_statement_id_seq";
CREATE SEQUENCE "public"."financial_statement_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 20
 CACHE 1;
SELECT setval('"public"."financial_statement_id_seq"', 20, true);

-- ----------------------------
-- Sequence structure for list_of_expired_passports_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."list_of_expired_passports_id_seq";
CREATE SEQUENCE "public"."list_of_expired_passports_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."list_of_expired_passports_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for mass_address_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mass_address_id_seq";
CREATE SEQUENCE "public"."mass_address_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 59399
 CACHE 1;
SELECT setval('"public"."mass_address_id_seq"', 59399, true);

-- ----------------------------
-- Sequence structure for mass_founders_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mass_founders_id_seq";
CREATE SEQUENCE "public"."mass_founders_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 6179
 CACHE 1;
SELECT setval('"public"."mass_founders_id_seq"', 6179, true);

-- ----------------------------
-- Sequence structure for mass_leaders_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mass_leaders_id_seq";
CREATE SEQUENCE "public"."mass_leaders_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8505
 CACHE 1;
SELECT setval('"public"."mass_leaders_id_seq"', 8505, true);

-- ----------------------------
-- Sequence structure for msp_document_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_document_id_seq";
CREATE SEQUENCE "public"."msp_document_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 25666
 CACHE 1;
SELECT setval('"public"."msp_document_id_seq"', 25666, true);

-- ----------------------------
-- Sequence structure for msp_file_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_file_id_seq";
CREATE SEQUENCE "public"."msp_file_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 63
 CACHE 1;
SELECT setval('"public"."msp_file_id_seq"', 63, true);

-- ----------------------------
-- Sequence structure for msp_gorod_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_gorod_id_seq";
CREATE SEQUENCE "public"."msp_gorod_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 10202
 CACHE 1;
SELECT setval('"public"."msp_gorod_id_seq"', 10202, true);

-- ----------------------------
-- Sequence structure for msp_id_otpr2_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_id_otpr2_id_seq";
CREATE SEQUENCE "public"."msp_id_otpr2_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 46
 CACHE 1;
SELECT setval('"public"."msp_id_otpr2_id_seq"', 46, true);

-- ----------------------------
-- Sequence structure for msp_ip_vkl_msp2_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_ip_vkl_msp2_id_seq";
CREATE SEQUENCE "public"."msp_ip_vkl_msp2_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7113
 CACHE 1;
SELECT setval('"public"."msp_ip_vkl_msp2_id_seq"', 7113, true);

-- ----------------------------
-- Sequence structure for msp_nasel_punkt_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_nasel_punkt_id_seq";
CREATE SEQUENCE "public"."msp_nasel_punkt_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 913
 CACHE 1;
SELECT setval('"public"."msp_nasel_punkt_id_seq"', 913, true);

-- ----------------------------
-- Sequence structure for msp_org_vkl_msp2_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_org_vkl_msp2_id_seq";
CREATE SEQUENCE "public"."msp_org_vkl_msp2_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7461
 CACHE 1;
SELECT setval('"public"."msp_org_vkl_msp2_id_seq"', 7461, true);

-- ----------------------------
-- Sequence structure for msp_rayon_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_rayon_id_seq";
CREATE SEQUENCE "public"."msp_rayon_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 894
 CACHE 1;
SELECT setval('"public"."msp_rayon_id_seq"', 894, true);

-- ----------------------------
-- Sequence structure for msp_sv_kontr_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_kontr_id_seq";
CREATE SEQUENCE "public"."msp_sv_kontr_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for msp_sv_litsenz2_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_litsenz2_id_seq";
CREATE SEQUENCE "public"."msp_sv_litsenz2_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 977
 CACHE 1;
SELECT setval('"public"."msp_sv_litsenz2_id_seq"', 977, true);

-- ----------------------------
-- Sequence structure for msp_sv_okved_dop_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_okved_dop_id_seq";
CREATE SEQUENCE "public"."msp_sv_okved_dop_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 206016
 CACHE 1;
SELECT setval('"public"."msp_sv_okved_dop_id_seq"', 206016, true);

-- ----------------------------
-- Sequence structure for msp_sv_okved_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_okved_id_seq";
CREATE SEQUENCE "public"."msp_sv_okved_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 18306
 CACHE 1;
SELECT setval('"public"."msp_sv_okved_id_seq"', 18306, true);

-- ----------------------------
-- Sequence structure for msp_sv_okved_osn_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_okved_osn_id_seq";
CREATE SEQUENCE "public"."msp_sv_okved_osn_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 18124
 CACHE 1;
SELECT setval('"public"."msp_sv_okved_osn_id_seq"', 18124, true);

-- ----------------------------
-- Sequence structure for msp_sv_prod_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_prod_id_seq";
CREATE SEQUENCE "public"."msp_sv_prod_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for msp_sv_prog_dog_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_prog_dog_id_seq";
CREATE SEQUENCE "public"."msp_sv_prog_dog_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for msp_sv_prog_part_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sv_prog_part_id_seq";
CREATE SEQUENCE "public"."msp_sv_prog_part_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for msp_sved_mn_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."msp_sved_mn_id_seq";
CREATE SEQUENCE "public"."msp_sved_mn_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 18307
 CACHE 1;
SELECT setval('"public"."msp_sved_mn_id_seq"', 18307, true);

-- ----------------------------
-- Sequence structure for register_disqualified_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."register_disqualified_id_seq";
CREATE SEQUENCE "public"."register_disqualified_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 14255
 CACHE 1;
SELECT setval('"public"."register_disqualified_id_seq"', 14255, true);

-- ----------------------------
-- Sequence structure for request_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."request_id_seq";
CREATE SEQUENCE "public"."request_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 70
 CACHE 1;
SELECT setval('"public"."request_id_seq"', 70, true);

-- ----------------------------
-- Sequence structure for rus_terrorists_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."rus_terrorists_id_seq";
CREATE SEQUENCE "public"."rus_terrorists_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 72006
 CACHE 1;
SELECT setval('"public"."rus_terrorists_id_seq"', 72006, true);

-- ----------------------------
-- Sequence structure for terrorists_ul_rus_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."terrorists_ul_rus_id_seq";
CREATE SEQUENCE "public"."terrorists_ul_rus_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 748
 CACHE 1;
SELECT setval('"public"."terrorists_ul_rus_id_seq"', 748, true);

-- ----------------------------
-- Table structure for company_card
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_card";
CREATE TABLE "public"."company_card" (
"id" int4 DEFAULT nextval('company_card_id_seq'::regclass) NOT NULL,
"tip_dokumenta" text COLLATE "default",
"ogrn" text COLLATE "default",
"naim_ul_sokr" text COLLATE "default",
"naim_ul_poln" text COLLATE "default",
"prizn_otsut_adres_ul" text COLLATE "default",
"obr_data" text COLLATE "default",
"data_ogrn" text COLLATE "default",
"data_prekr_ul" text COLLATE "default",
"aktivnost" text COLLATE "default",
"inn" text COLLATE "default",
"kpp" text COLLATE "default",
"okpo" text COLLATE "default",
"adres" text COLLATE "default",
"sv_upr_org_array" text COLLATE "default",
"sum_kap" text COLLATE "default",
"nomin_stoim" text COLLATE "default",
"derzh_inn" text COLLATE "default",
"derzh_ogrn" text COLLATE "default",
"derzh_naim_ul_poln" text COLLATE "default",
"kod_okved" text COLLATE "default",
"naim_okved" text COLLATE "default",
"sv_okved_dop_array" text COLLATE "default",
"sv_litsenziya" text COLLATE "default",
"naim_no" text COLLATE "default",
"data_post_uch" text COLLATE "default",
"reg_nom_pf" text COLLATE "default",
"data_reg_pf" text COLLATE "default",
"reg_nom_fss" text COLLATE "default",
"data_reg_fss" text COLLATE "default",
"sv_filial" text COLLATE "default",
"sv_predstav" text COLLATE "default",
"rukovoditeli_array" text COLLATE "default",
"fo2015" text COLLATE "default",
"fo2014" text COLLATE "default",
"fo2013" text COLLATE "default",
"fo2012" text COLLATE "default",
"fo2011" text COLLATE "default",
"sv_uchredit_sum_cap" text COLLATE "default",
"sv_uchredit_all_array" text COLLATE "default",
"response" text COLLATE "default",
"request_string" text COLLATE "default",
"opisanie" text COLLATE "default",
"fio" text COLLATE "default",
"data_ogrnip" text COLLATE "default",
"data_prekrashch" text COLLATE "default",
"innfl" text COLLATE "default",
"ogrnip" text COLLATE "default",
"uch_naim_no" text COLLATE "default",
"okato" text COLLATE "default",
"oktmo" text COLLATE "default",
"okogu" text COLLATE "default",
"okopf" text COLLATE "default",
"okfs" text COLLATE "default",
"naim_vid_ip" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for company_search
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_search";
CREATE TABLE "public"."company_search" (
"id" int4 DEFAULT nextval('request_id_seq'::regclass) NOT NULL,
"request_string" text COLLATE "default",
"response" text COLLATE "default",
"created_at" timestamp(6),
"is_success" bool DEFAULT false NOT NULL,
"total_records" int4 DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."company_search"."request_string" IS 'Строка поиска';
COMMENT ON COLUMN "public"."company_search"."response" IS 'Ответ с сервера в формате JSON';
COMMENT ON COLUMN "public"."company_search"."created_at" IS 'Дата и время создания';
COMMENT ON COLUMN "public"."company_search"."is_success" IS 'Статус ответа - Ok 200';
COMMENT ON COLUMN "public"."company_search"."total_records" IS 'Всего записей';

-- ----------------------------
-- Table structure for company_search_result
-- ----------------------------
DROP TABLE IF EXISTS "public"."company_search_result";
CREATE TABLE "public"."company_search_result" (
"id" int4 DEFAULT nextval('company_search_result_id_seq'::regclass) NOT NULL,
"request_id" int4 NOT NULL,
"ogrn" text COLLATE "default",
"inn" text COLLATE "default",
"is_new" bool DEFAULT true
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for court_arbitration
-- ----------------------------
DROP TABLE IF EXISTS "public"."court_arbitration";
CREATE TABLE "public"."court_arbitration" (
"id" int4 DEFAULT nextval('court_arbitration_id_seq'::regclass) NOT NULL,
"request_string" text COLLATE "default",
"response" text COLLATE "default",
"tochno_vsego" text COLLATE "default" DEFAULT 0,
"netochno_vsego" text COLLATE "default",
"ogrn" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for court_arbitration_case
-- ----------------------------
DROP TABLE IF EXISTS "public"."court_arbitration_case";
CREATE TABLE "public"."court_arbitration_case" (
"id" int4 DEFAULT nextval('court_arbitration_case_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default",
"nomer_dela" text COLLATE "default",
"summa_iska" text COLLATE "default",
"start_dat" text COLLATE "default",
"ogrn_id" int8 NOT NULL,
"is_sure" bool
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for court_arbitration_case_members
-- ----------------------------
DROP TABLE IF EXISTS "public"."court_arbitration_case_members";
CREATE TABLE "public"."court_arbitration_case_members" (
"id" int4 DEFAULT nextval('court_arbitration_case_members_id_seq'::regclass) NOT NULL,
"court_arbitration_case_id" int4,
"inn" text COLLATE "default",
"ogrn" text COLLATE "default",
"name" text COLLATE "default",
"type" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for disqualified_persons
-- ----------------------------
DROP TABLE IF EXISTS "public"."disqualified_persons";
CREATE TABLE "public"."disqualified_persons" (
"id" int4 DEFAULT nextval('disqualified_persons_id_seq'::regclass) NOT NULL,
"full_name" text COLLATE "default" NOT NULL,
"ogrn" text COLLATE "default" NOT NULL,
"inn" text COLLATE "default" NOT NULL,
"kpp" text COLLATE "default" NOT NULL,
"address" text COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."disqualified_persons"."full_name" IS 'Full name of organization';
COMMENT ON COLUMN "public"."disqualified_persons"."ogrn" IS 'PSRN organization';
COMMENT ON COLUMN "public"."disqualified_persons"."inn" IS 'Taxpayer identification number';
COMMENT ON COLUMN "public"."disqualified_persons"."kpp" IS 'The reason of tax registration';
COMMENT ON COLUMN "public"."disqualified_persons"."address" IS 'Postal address';

-- ----------------------------
-- Table structure for expired_passports
-- ----------------------------
DROP TABLE IF EXISTS "public"."expired_passports";
CREATE TABLE "public"."expired_passports" (
"id" int4 DEFAULT nextval('list_of_expired_passports_id_seq'::regclass) NOT NULL,
"series" text COLLATE "default",
"number" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for financial_statement
-- ----------------------------
DROP TABLE IF EXISTS "public"."financial_statement";
CREATE TABLE "public"."financial_statement" (
"id" int4 DEFAULT nextval('financial_statement_id_seq'::regclass) NOT NULL,
"response" text COLLATE "default",
"request_string" text COLLATE "default",
"va_nematerialnyye_aktivy_2016" text COLLATE "default",
"va_nematerialnyye_aktivy_2012" text COLLATE "default",
"va_nematerialnyye_aktivy_2013" text COLLATE "default",
"va_nematerialnyye_aktivy_2014" text COLLATE "default",
"va_nematerialnyye_aktivy_2015" text COLLATE "default",
"va_osnovnyye_sredstva_2016" text COLLATE "default",
"va_osnovnyye_sredstva_2012" text COLLATE "default",
"va_osnovnyye_sredstva_2013" text COLLATE "default",
"va_osnovnyye_sredstva_2014" text COLLATE "default",
"va_osnovnyye_sredstva_2015" text COLLATE "default",
"va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2016" text COLLATE "default",
"va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2012" text COLLATE "default",
"va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2013" text COLLATE "default",
"va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2014" text COLLATE "default",
"va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2015" text COLLATE "default",
"va_finansovyye_vlozheniya_2016" text COLLATE "default",
"va_finansovyye_vlozheniya_2012" text COLLATE "default",
"va_finansovyye_vlozheniya_2013" text COLLATE "default",
"va_finansovyye_vlozheniya_2014" text COLLATE "default",
"va_finansovyye_vlozheniya_2015" text COLLATE "default",
"va_otlozhennyye_nalogovyye_aktivy_2016" text COLLATE "default",
"va_otlozhennyye_nalogovyye_aktivy_2012" text COLLATE "default",
"va_otlozhennyye_nalogovyye_aktivy_2013" text COLLATE "default",
"va_otlozhennyye_nalogovyye_aktivy_2014" text COLLATE "default",
"va_otlozhennyye_nalogovyye_aktivy_2015" text COLLATE "default",
"va_itogo_po_razdelu_1_2016" text COLLATE "default",
"va_itogo_po_razdelu_1_2012" text COLLATE "default",
"va_itogo_po_razdelu_1_2013" text COLLATE "default",
"va_itogo_po_razdelu_1_2014" text COLLATE "default",
"va_itogo_po_razdelu_1_2015" text COLLATE "default",
"oa_zapasy_2016" text COLLATE "default",
"oa_zapasy_2012" text COLLATE "default",
"oa_zapasy_2013" text COLLATE "default",
"oa_zapasy_2014" text COLLATE "default",
"oa_zapasy_2015" text COLLATE "default",
"oa_nds_po_priobretennym_tsennostyam_2016" text COLLATE "default",
"oa_nds_po_priobretennym_tsennostyam_2012" text COLLATE "default",
"oa_nds_po_priobretennym_tsennostyam_2013" text COLLATE "default",
"oa_nds_po_priobretennym_tsennostyam_2014" text COLLATE "default",
"oa_nds_po_priobretennym_tsennostyam_2015" text COLLATE "default",
"oa_debitorskaya_zadolzhennost_2016" text COLLATE "default",
"oa_debitorskaya_zadolzhennost_2012" text COLLATE "default",
"oa_debitorskaya_zadolzhennost_2013" text COLLATE "default",
"oa_debitorskaya_zadolzhennost_2014" text COLLATE "default",
"oa_debitorskaya_zadolzhennost_2015" text COLLATE "default",
"oa_finansovyye_vlozheniya_2016" text COLLATE "default",
"oa_finansovyye_vlozheniya_2012" text COLLATE "default",
"oa_finansovyye_vlozheniya_2013" text COLLATE "default",
"oa_finansovyye_vlozheniya_2014" text COLLATE "default",
"oa_finansovyye_vlozheniya_2015" text COLLATE "default",
"oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2016" text COLLATE "default",
"oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2012" text COLLATE "default",
"oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2013" text COLLATE "default",
"oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2014" text COLLATE "default",
"oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2015" text COLLATE "default",
"oa_prochiye_oborotnyye_aktivy_2016" text COLLATE "default",
"oa_prochiye_oborotnyye_aktivy_2012" text COLLATE "default",
"oa_prochiye_oborotnyye_aktivy_2013" text COLLATE "default",
"oa_prochiye_oborotnyye_aktivy_2014" text COLLATE "default",
"oa_prochiye_oborotnyye_aktivy_2015" text COLLATE "default",
"oa_itogo_po_razdelu_2_2016" text COLLATE "default",
"oa_itogo_po_razdelu_2_2012" text COLLATE "default",
"oa_itogo_po_razdelu_2_2013" text COLLATE "default",
"oa_itogo_po_razdelu_2_2014" text COLLATE "default",
"oa_itogo_po_razdelu_2_2015" text COLLATE "default",
"oa_balans_2016" text COLLATE "default",
"oa_balans_2012" text COLLATE "default",
"oa_balans_2013" text COLLATE "default",
"oa_balans_2014" text COLLATE "default",
"oa_balans_2015" text COLLATE "default",
"kir_ustavnyy_kapital_2016" text COLLATE "default",
"kir_ustavnyy_kapital_2012" text COLLATE "default",
"kir_ustavnyy_kapital_2013" text COLLATE "default",
"kir_ustavnyy_kapital_2014" text COLLATE "default",
"kir_ustavnyy_kapital_2015" text COLLATE "default",
"kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2016" text COLLATE "default",
"kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2012" text COLLATE "default",
"kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2013" text COLLATE "default",
"kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2014" text COLLATE "default",
"kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2015" text COLLATE "default",
"kir_dobavochnyy_kapital_2016" text COLLATE "default",
"kir_dobavochnyy_kapital_2012" text COLLATE "default",
"kir_dobavochnyy_kapital_2013" text COLLATE "default",
"kir_dobavochnyy_kapital_2014" text COLLATE "default",
"kir_dobavochnyy_kapital_2015" text COLLATE "default",
"kir_rezervnyy_kapital_2016" text COLLATE "default",
"kir_rezervnyy_kapital_2012" text COLLATE "default",
"kir_rezervnyy_kapital_2013" text COLLATE "default",
"kir_rezervnyy_kapital_2014" text COLLATE "default",
"kir_rezervnyy_kapital_2015" text COLLATE "default",
"kir_neraspredelennaya_pribyl_2016" text COLLATE "default",
"kir_neraspredelennaya_pribyl_2012" text COLLATE "default",
"kir_neraspredelennaya_pribyl_2013" text COLLATE "default",
"kir_neraspredelennaya_pribyl_2014" text COLLATE "default",
"kir_neraspredelennaya_pribyl_2015" text COLLATE "default",
"kir_itogo_po_razdelu_3_2016" text COLLATE "default",
"kir_itogo_po_razdelu_3_2012" text COLLATE "default",
"kir_itogo_po_razdelu_3_2013" text COLLATE "default",
"kir_itogo_po_razdelu_3_2014" text COLLATE "default",
"kir_itogo_po_razdelu_3_2015" text COLLATE "default",
"do_zayemnyye_sredstva_2016" text COLLATE "default",
"do_zayemnyye_sredstva_2012" text COLLATE "default",
"do_zayemnyye_sredstva_2013" text COLLATE "default",
"do_zayemnyye_sredstva_2014" text COLLATE "default",
"do_zayemnyye_sredstva_2015" text COLLATE "default",
"do_otlozhennyye_nalogovyye_obyazatelstva_2016" text COLLATE "default",
"do_otlozhennyye_nalogovyye_obyazatelstva_2012" text COLLATE "default",
"do_otlozhennyye_nalogovyye_obyazatelstva_2013" text COLLATE "default",
"do_otlozhennyye_nalogovyye_obyazatelstva_2014" text COLLATE "default",
"do_otlozhennyye_nalogovyye_obyazatelstva_2015" text COLLATE "default",
"do_prochiye_obyazatelstva_2016" text COLLATE "default",
"do_prochiye_obyazatelstva_2012" text COLLATE "default",
"do_prochiye_obyazatelstva_2013" text COLLATE "default",
"do_prochiye_obyazatelstva_2014" text COLLATE "default",
"do_prochiye_obyazatelstva_2015" text COLLATE "default",
"do_itogo_po_razdelu_4_2016" text COLLATE "default",
"do_itogo_po_razdelu_4_2012" text COLLATE "default",
"do_itogo_po_razdelu_4_2013" text COLLATE "default",
"do_itogo_po_razdelu_4_2014" text COLLATE "default",
"do_itogo_po_razdelu_4_2015" text COLLATE "default",
"ko_zayemnyye_sredstva_2016" text COLLATE "default",
"ko_zayemnyye_sredstva_2012" text COLLATE "default",
"ko_zayemnyye_sredstva_2013" text COLLATE "default",
"ko_zayemnyye_sredstva_2014" text COLLATE "default",
"ko_zayemnyye_sredstva_2015" text COLLATE "default",
"ko_kreditorskaya_zadolzhennost_2016" text COLLATE "default",
"ko_kreditorskaya_zadolzhennost_2012" text COLLATE "default",
"ko_kreditorskaya_zadolzhennost_2013" text COLLATE "default",
"ko_kreditorskaya_zadolzhennost_2014" text COLLATE "default",
"ko_kreditorskaya_zadolzhennost_2015" text COLLATE "default",
"ko_dokhody_budushchikh_periodov_2016" text COLLATE "default",
"ko_dokhody_budushchikh_periodov_2012" text COLLATE "default",
"ko_dokhody_budushchikh_periodov_2013" text COLLATE "default",
"ko_dokhody_budushchikh_periodov_2014" text COLLATE "default",
"ko_dokhody_budushchikh_periodov_2015" text COLLATE "default",
"ko_prochiye_obyazatelstva_2016" text COLLATE "default",
"ko_prochiye_obyazatelstva_2012" text COLLATE "default",
"ko_prochiye_obyazatelstva_2013" text COLLATE "default",
"ko_prochiye_obyazatelstva_2014" text COLLATE "default",
"ko_prochiye_obyazatelstva_2015" text COLLATE "default",
"ko_itogo_po_razdelu_5_2016" text COLLATE "default",
"ko_itogo_po_razdelu_5_2012" text COLLATE "default",
"ko_itogo_po_razdelu_5_2013" text COLLATE "default",
"ko_itogo_po_razdelu_5_2014" text COLLATE "default",
"ko_itogo_po_razdelu_5_2015" text COLLATE "default",
"ko_balans_2016" text COLLATE "default",
"ko_balans_2012" text COLLATE "default",
"ko_balans_2013" text COLLATE "default",
"ko_balans_2014" text COLLATE "default",
"ko_balans_2015" text COLLATE "default",
"dirpovd_vyruchka_2012" text COLLATE "default",
"dirpovd_vyruchka_2013" text COLLATE "default",
"dirpovd_vyruchka_2014" text COLLATE "default",
"dirpovd_vyruchka_2015" text COLLATE "default",
"dirpovd_vyruchka_2016" text COLLATE "default",
"dirpovd_sebestoimost_prodazh_2012" text COLLATE "default",
"dirpovd_sebestoimost_prodazh_2013" text COLLATE "default",
"dirpovd_sebestoimost_prodazh_2014" text COLLATE "default",
"dirpovd_sebestoimost_prodazh_2015" text COLLATE "default",
"dirpovd_sebestoimost_prodazh_2016" text COLLATE "default",
"dirpovd_valovaya_pribyl_2012" text COLLATE "default",
"dirpovd_valovaya_pribyl_2013" text COLLATE "default",
"dirpovd_valovaya_pribyl_2014" text COLLATE "default",
"dirpovd_valovaya_pribyl_2015" text COLLATE "default",
"dirpovd_valovaya_pribyl_2016" text COLLATE "default",
"dirpovd_kommercheskiye_raskhody_2012" text COLLATE "default",
"dirpovd_kommercheskiye_raskhody_2013" text COLLATE "default",
"dirpovd_kommercheskiye_raskhody_2014" text COLLATE "default",
"dirpovd_kommercheskiye_raskhody_2015" text COLLATE "default",
"dirpovd_kommercheskiye_raskhody_2016" text COLLATE "default",
"dirpovd_upravlencheskiye_raskhody_2012" text COLLATE "default",
"dirpovd_upravlencheskiye_raskhody_2013" text COLLATE "default",
"dirpovd_upravlencheskiye_raskhody_2014" text COLLATE "default",
"dirpovd_upravlencheskiye_raskhody_2015" text COLLATE "default",
"dirpovd_upravlencheskiye_raskhody_2016" text COLLATE "default",
"dirpovd_pribyl_ot_prodazh_2012" text COLLATE "default",
"dirpovd_pribyl_ot_prodazh_2013" text COLLATE "default",
"dirpovd_pribyl_ot_prodazh_2014" text COLLATE "default",
"dirpovd_pribyl_ot_prodazh_2015" text COLLATE "default",
"dirpovd_pribyl_ot_prodazh_2016" text COLLATE "default",
"pdir_protsenty_k_polucheniyu_2012" text COLLATE "default",
"pdir_protsenty_k_polucheniyu_2013" text COLLATE "default",
"pdir_protsenty_k_polucheniyu_2014" text COLLATE "default",
"pdir_protsenty_k_polucheniyu_2015" text COLLATE "default",
"pdir_protsenty_k_polucheniyu_2016" text COLLATE "default",
"pdir_protsenty_k_uplate_2012" text COLLATE "default",
"pdir_protsenty_k_uplate_2013" text COLLATE "default",
"pdir_protsenty_k_uplate_2014" text COLLATE "default",
"pdir_protsenty_k_uplate_2015" text COLLATE "default",
"pdir_protsenty_k_uplate_2016" text COLLATE "default",
"pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2012" text COLLATE "default",
"pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2013" text COLLATE "default",
"pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2014" text COLLATE "default",
"pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2015" text COLLATE "default",
"pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2016" text COLLATE "default",
"pdir_prochiye_dokhody_2012" text COLLATE "default",
"pdir_prochiye_dokhody_2013" text COLLATE "default",
"pdir_prochiye_dokhody_2014" text COLLATE "default",
"pdir_prochiye_dokhody_2015" text COLLATE "default",
"pdir_prochiye_dokhody_2016" text COLLATE "default",
"pdir_prochiye_raskhody_2012" text COLLATE "default",
"pdir_prochiye_raskhody_2013" text COLLATE "default",
"pdir_prochiye_raskhody_2014" text COLLATE "default",
"pdir_prochiye_raskhody_2015" text COLLATE "default",
"pdir_prochiye_raskhody_2016" text COLLATE "default",
"pdir_pribyl_do_nalogooblozheniya_2012" text COLLATE "default",
"pdir_pribyl_do_nalogooblozheniya_2013" text COLLATE "default",
"pdir_pribyl_do_nalogooblozheniya_2014" text COLLATE "default",
"pdir_pribyl_do_nalogooblozheniya_2015" text COLLATE "default",
"pdir_pribyl_do_nalogooblozheniya_2016" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2012" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2013" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2014" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2015" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2016" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2012" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2013" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2014" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2015" text COLLATE "default",
"pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2016" text COLLATE "default",
"pdir_tekushchiy_nalog_na_pribyl_2012" text COLLATE "default",
"pdir_tekushchiy_nalog_na_pribyl_2013" text COLLATE "default",
"pdir_tekushchiy_nalog_na_pribyl_2014" text COLLATE "default",
"pdir_tekushchiy_nalog_na_pribyl_2015" text COLLATE "default",
"pdir_tekushchiy_nalog_na_pribyl_2016" text COLLATE "default",
"pdir_chistaya_pribyl_2012" text COLLATE "default",
"pdir_chistaya_pribyl_2013" text COLLATE "default",
"pdir_chistaya_pribyl_2014" text COLLATE "default",
"pdir_chistaya_pribyl_2015" text COLLATE "default",
"pdir_chistaya_pribyl_2016" text COLLATE "default",
"pdir_postoyannyye_nalogovyye_obyazatelstva_2012" text COLLATE "default",
"pdir_postoyannyye_nalogovyye_obyazatelstva_2013" text COLLATE "default",
"pdir_postoyannyye_nalogovyye_obyazatelstva_2014" text COLLATE "default",
"pdir_postoyannyye_nalogovyye_obyazatelstva_2015" text COLLATE "default",
"pdir_postoyannyye_nalogovyye_obyazatelstva_2016" text COLLATE "default",
"va_prochiye_vneoborotnyye_aktivy_2012" text COLLATE "default",
"va_prochiye_vneoborotnyye_aktivy_2013" text COLLATE "default",
"va_prochiye_vneoborotnyye_aktivy_2014" text COLLATE "default",
"va_prochiye_vneoborotnyye_aktivy_2015" text COLLATE "default",
"va_prochiye_vneoborotnyye_aktivy_2016" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for mass_address
-- ----------------------------
DROP TABLE IF EXISTS "public"."mass_address";
CREATE TABLE "public"."mass_address" (
"id" int4 DEFAULT nextval('mass_address_id_seq'::regclass) NOT NULL,
"region" text COLLATE "default",
"location" text COLLATE "default",
"city" text COLLATE "default",
"settlement" text COLLATE "default",
"street" text COLLATE "default",
"house_number" text COLLATE "default",
"corpus_number" text COLLATE "default",
"office_number" text COLLATE "default",
"quantity" text COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."mass_address"."region" IS 'The name of the region';
COMMENT ON COLUMN "public"."mass_address"."location" IS 'The name of the  location';
COMMENT ON COLUMN "public"."mass_address"."city" IS 'The name of the city';
COMMENT ON COLUMN "public"."mass_address"."settlement" IS 'The name of the settlement';
COMMENT ON COLUMN "public"."mass_address"."street" IS 'The street name';
COMMENT ON COLUMN "public"."mass_address"."house_number" IS 'The number of house';
COMMENT ON COLUMN "public"."mass_address"."corpus_number" IS 'The number of corpus';
COMMENT ON COLUMN "public"."mass_address"."office_number" IS 'The number of the apartment (office)';
COMMENT ON COLUMN "public"."mass_address"."quantity" IS 'The number of juridical entity';

-- ----------------------------
-- Table structure for mass_founders
-- ----------------------------
DROP TABLE IF EXISTS "public"."mass_founders";
CREATE TABLE "public"."mass_founders" (
"id" int4 DEFAULT nextval('mass_founders_id_seq'::regclass) NOT NULL,
"inn" text COLLATE "default",
"surname" text COLLATE "default" NOT NULL,
"name" text COLLATE "default" NOT NULL,
"patronymic" text COLLATE "default" NOT NULL,
"quantity" int4 NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."mass_founders"."inn" IS 'Taxpayer identification number';
COMMENT ON COLUMN "public"."mass_founders"."surname" IS 'Surname';
COMMENT ON COLUMN "public"."mass_founders"."name" IS 'Name';
COMMENT ON COLUMN "public"."mass_founders"."patronymic" IS 'Patronymic';
COMMENT ON COLUMN "public"."mass_founders"."quantity" IS 'The number of organizations which he is a member/participant';

-- ----------------------------
-- Table structure for mass_leaders
-- ----------------------------
DROP TABLE IF EXISTS "public"."mass_leaders";
CREATE TABLE "public"."mass_leaders" (
"id" int4 DEFAULT nextval('mass_leaders_id_seq'::regclass) NOT NULL,
"inn" text COLLATE "default",
"surname" text COLLATE "default" NOT NULL,
"name" text COLLATE "default" NOT NULL,
"patronymic" text COLLATE "default" NOT NULL,
"quantity" int4 NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."mass_leaders"."inn" IS 'Taxpayer identification number';
COMMENT ON COLUMN "public"."mass_leaders"."surname" IS 'Surname';
COMMENT ON COLUMN "public"."mass_leaders"."name" IS 'Name';
COMMENT ON COLUMN "public"."mass_leaders"."patronymic" IS 'Patronymic';
COMMENT ON COLUMN "public"."mass_leaders"."quantity" IS 'The number of organizations where the man is the head';

-- ----------------------------
-- Table structure for msp_dokument
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_dokument";
CREATE TABLE "public"."msp_dokument" (
"id" int4 DEFAULT nextval('msp_document_id_seq'::regclass) NOT NULL,
"id_dok" text COLLATE "default",
"data_sost" date,
"data_vkl_msp" date,
"vid_sub_msp" text COLLATE "default",
"kat_sub_msp" text COLLATE "default",
"priz_nov_msp" text COLLATE "default",
"msp_fayl_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_fayl
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_fayl";
CREATE TABLE "public"."msp_fayl" (
"id" int4 DEFAULT nextval('msp_file_id_seq'::regclass) NOT NULL,
"id_fayl" text COLLATE "default",
"vers_form" text COLLATE "default",
"tip_inf" text COLLATE "default",
"vers_prog" text COLLATE "default",
"kol_dok" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_gorod
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_gorod";
CREATE TABLE "public"."msp_gorod" (
"id" int4 DEFAULT nextval('msp_gorod_id_seq'::regclass) NOT NULL,
"tip" text COLLATE "default",
"naim" text COLLATE "default",
"msp_sved_mn_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_id_otpr
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_id_otpr";
CREATE TABLE "public"."msp_id_otpr" (
"id" int4 DEFAULT nextval('msp_id_otpr2_id_seq'::regclass) NOT NULL,
"msp_fayl_id" int4 NOT NULL,
"dolzh_otv" text COLLATE "default",
"tlf" text COLLATE "default",
"email" text COLLATE "default",
"fio_otv_familiya" text COLLATE "default",
"fio_otv_imya" text COLLATE "default",
"fio_otv_otchestvo" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_ip_vkl_msp
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_ip_vkl_msp";
CREATE TABLE "public"."msp_ip_vkl_msp" (
"id" int4 DEFAULT nextval('msp_ip_vkl_msp2_id_seq'::regclass) NOT NULL,
"msp_dokument_id" int4 NOT NULL,
"inn" text COLLATE "default",
"fio_ip_familiya" text COLLATE "default",
"fio_ip_imya" text COLLATE "default",
"fio_ip_otchestvo" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_nasel_punkt
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_nasel_punkt";
CREATE TABLE "public"."msp_nasel_punkt" (
"id" int4 DEFAULT nextval('msp_nasel_punkt_id_seq'::regclass) NOT NULL,
"tip" text COLLATE "default",
"naim" text COLLATE "default",
"msp_sved_mn_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_org_vkl_msp
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_org_vkl_msp";
CREATE TABLE "public"."msp_org_vkl_msp" (
"id" int4 DEFAULT nextval('msp_org_vkl_msp2_id_seq'::regclass) NOT NULL,
"msp_dokument_id" int4 NOT NULL,
"naim_org" text COLLATE "default",
"naim_org_sokr" text COLLATE "default",
"innul" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_rayon
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_rayon";
CREATE TABLE "public"."msp_rayon" (
"id" int4 DEFAULT nextval('msp_rayon_id_seq'::regclass) NOT NULL,
"tip" text COLLATE "default",
"naim" text COLLATE "default",
"msp_sved_mn_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_dog
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_dog";
CREATE TABLE "public"."msp_sv_dog" (
"id" int4 DEFAULT nextval('msp_sv_prog_dog_id_seq'::regclass) NOT NULL,
"naim_ul_zd" text COLLATE "default",
"inn_ul_zd" text COLLATE "default",
"predm_dog" text COLLATE "default",
"nom_dog_reestr" text COLLATE "default",
"data_dog" date,
"msp_dokument_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_kontr
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_kontr";
CREATE TABLE "public"."msp_sv_kontr" (
"id" int4 DEFAULT nextval('msp_sv_kontr_id_seq'::regclass) NOT NULL,
"naim_ul_zd" text COLLATE "default",
"inn_ul_zd" text COLLATE "default",
"predm_kontr" text COLLATE "default",
"nom_kontr_reestr" text COLLATE "default",
"data_kontr" date,
"msp_dokument_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_litsenz
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_litsenz";
CREATE TABLE "public"."msp_sv_litsenz" (
"id" int4 DEFAULT nextval('msp_sv_litsenz2_id_seq'::regclass) NOT NULL,
"msp_dokument_id" int4 NOT NULL,
"naim_litsenz_vd" text COLLATE "default",
"sved_adr_lits_vd" text COLLATE "default",
"ser_litsenz" text COLLATE "default",
"nom_litsenz" text COLLATE "default",
"vid_litsenz" text COLLATE "default",
"data_litsenz" date,
"data_nach_litsenz" date,
"data_kon_litsenz" date,
"org_vyd_litsenz" text COLLATE "default",
"data_ost_litsenz" date,
"org_ost_litsenz" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_okved
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_okved";
CREATE TABLE "public"."msp_sv_okved" (
"id" int4 DEFAULT nextval('msp_sv_okved_id_seq'::regclass) NOT NULL,
"msp_dokument_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_okved_dop
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_okved_dop";
CREATE TABLE "public"."msp_sv_okved_dop" (
"id" int4 DEFAULT nextval('msp_sv_okved_dop_id_seq'::regclass) NOT NULL,
"kod_okved" text COLLATE "default",
"naim_okved" text COLLATE "default",
"vers_okved" text COLLATE "default",
"msp_sv_okved_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_okved_osn
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_okved_osn";
CREATE TABLE "public"."msp_sv_okved_osn" (
"id" int4 DEFAULT nextval('msp_sv_okved_osn_id_seq'::regclass) NOT NULL,
"kod_okved" text COLLATE "default",
"naim_okved" text COLLATE "default",
"vers_okved" text COLLATE "default",
"msp_sv_okved_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_prod
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_prod";
CREATE TABLE "public"."msp_sv_prod" (
"id" int4 DEFAULT nextval('msp_sv_prod_id_seq'::regclass) NOT NULL,
"kod_prod" text COLLATE "default",
"naim_prod" text COLLATE "default",
"pr_otn_prod" text COLLATE "default",
"msp_dokument_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sv_prog_part
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sv_prog_part";
CREATE TABLE "public"."msp_sv_prog_part" (
"id" int4 DEFAULT nextval('msp_sv_prog_part_id_seq'::regclass) NOT NULL,
"naim_ul_pp" text COLLATE "default",
"inn_ul_pp" text COLLATE "default",
"nom_dog" text COLLATE "default",
"data_dog" date,
"msp_dokument_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for msp_sved_mn
-- ----------------------------
DROP TABLE IF EXISTS "public"."msp_sved_mn";
CREATE TABLE "public"."msp_sved_mn" (
"id" int4 DEFAULT nextval('msp_sved_mn_id_seq'::regclass) NOT NULL,
"kod_region" text COLLATE "default",
"msp_dokument_id" int4 NOT NULL,
"region_tip" text COLLATE "default",
"region_naim" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for register_disqualified
-- ----------------------------
DROP TABLE IF EXISTS "public"."register_disqualified";
CREATE TABLE "public"."register_disqualified" (
"id" int4 DEFAULT nextval('register_disqualified_id_seq'::regclass) NOT NULL,
"number" int8 NOT NULL,
"full_name" text COLLATE "default" NOT NULL,
"date_of_birth" date NOT NULL,
"place_of_birth" text COLLATE "default" NOT NULL,
"organization_name" text COLLATE "default" NOT NULL,
"organization_inn" text COLLATE "default",
"position" text COLLATE "default",
"administrative_code_article" text COLLATE "default",
"protocol_organization_name" text COLLATE "default" NOT NULL,
"judge_full_name" text COLLATE "default" NOT NULL,
"judge_position" text COLLATE "default",
"period_of_ineligibility" text COLLATE "default" NOT NULL,
"start_date" date,
"expiration_date" date NOT NULL
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."register_disqualified"."id" IS 'Номер записи из реестра дисквалифицированных лиц';
COMMENT ON COLUMN "public"."register_disqualified"."number" IS 'The number of the record from the register of disqualified persons';
COMMENT ON COLUMN "public"."register_disqualified"."full_name" IS 'Full name';
COMMENT ON COLUMN "public"."register_disqualified"."date_of_birth" IS 'Date of birth of the person';
COMMENT ON COLUMN "public"."register_disqualified"."place_of_birth" IS 'Place of birth';
COMMENT ON COLUMN "public"."register_disqualified"."organization_name" IS 'Name of the organization where the person worked during the time of the offence';
COMMENT ON COLUMN "public"."register_disqualified"."organization_inn" IS 'Identification number of the organization';
COMMENT ON COLUMN "public"."register_disqualified"."position" IS 'The post in which the person worked during the time of the offence';
COMMENT ON COLUMN "public"."register_disqualified"."administrative_code_article" IS 'Article of the administrative code, providing for administrative responsibility for committing an administrative offense';
COMMENT ON COLUMN "public"."register_disqualified"."protocol_organization_name" IS 'Name of the organization who made the Protocol on an administrative offence';
COMMENT ON COLUMN "public"."register_disqualified"."judge_full_name" IS 'Full name of the judge who made the decision on disqualification';
COMMENT ON COLUMN "public"."register_disqualified"."judge_position" IS 'The position of the judge';
COMMENT ON COLUMN "public"."register_disqualified"."period_of_ineligibility" IS 'The period of Ineligibility';
COMMENT ON COLUMN "public"."register_disqualified"."start_date" IS 'Start date';
COMMENT ON COLUMN "public"."register_disqualified"."expiration_date" IS 'Date of expiry of the period of Ineligibility';

-- ----------------------------
-- Table structure for terrorists_fl_rus
-- ----------------------------
DROP TABLE IF EXISTS "public"."terrorists_fl_rus";
CREATE TABLE "public"."terrorists_fl_rus" (
"id" int4 DEFAULT nextval('rus_terrorists_id_seq'::regclass) NOT NULL,
"full_name" text COLLATE "default" NOT NULL,
"date_of_birth" date,
"place_of_birth" text COLLATE "default",
"status" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for terrorists_ul_rus
-- ----------------------------
DROP TABLE IF EXISTS "public"."terrorists_ul_rus";
CREATE TABLE "public"."terrorists_ul_rus" (
"id" int4 DEFAULT nextval('terrorists_ul_rus_id_seq'::regclass) NOT NULL,
"name" text COLLATE "default",
"additional_info" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."company_card_id_seq" OWNED BY "company_card"."id";
ALTER SEQUENCE "public"."company_search_result_id_seq" OWNED BY "company_search_result"."id";
ALTER SEQUENCE "public"."court_arbitration_case_id_seq" OWNED BY "court_arbitration_case"."id";
ALTER SEQUENCE "public"."court_arbitration_case_members_id_seq" OWNED BY "court_arbitration_case_members"."id";
ALTER SEQUENCE "public"."court_arbitration_id_seq" OWNED BY "court_arbitration"."id";
ALTER SEQUENCE "public"."disqualified_persons_id_seq" OWNED BY "disqualified_persons"."id";
ALTER SEQUENCE "public"."financial_statement_id_seq" OWNED BY "financial_statement"."id";
ALTER SEQUENCE "public"."list_of_expired_passports_id_seq" OWNED BY "expired_passports"."id";
ALTER SEQUENCE "public"."mass_address_id_seq" OWNED BY "mass_address"."id";
ALTER SEQUENCE "public"."mass_founders_id_seq" OWNED BY "mass_founders"."id";
ALTER SEQUENCE "public"."mass_leaders_id_seq" OWNED BY "mass_leaders"."id";
ALTER SEQUENCE "public"."msp_document_id_seq" OWNED BY "msp_dokument"."id";
ALTER SEQUENCE "public"."msp_file_id_seq" OWNED BY "msp_fayl"."id";
ALTER SEQUENCE "public"."msp_gorod_id_seq" OWNED BY "msp_gorod"."id";
ALTER SEQUENCE "public"."msp_id_otpr2_id_seq" OWNED BY "msp_id_otpr"."id";
ALTER SEQUENCE "public"."msp_ip_vkl_msp2_id_seq" OWNED BY "msp_ip_vkl_msp"."id";
ALTER SEQUENCE "public"."msp_nasel_punkt_id_seq" OWNED BY "msp_nasel_punkt"."id";
ALTER SEQUENCE "public"."msp_org_vkl_msp2_id_seq" OWNED BY "msp_org_vkl_msp"."id";
ALTER SEQUENCE "public"."msp_rayon_id_seq" OWNED BY "msp_rayon"."id";
ALTER SEQUENCE "public"."msp_sv_kontr_id_seq" OWNED BY "msp_sv_kontr"."id";
ALTER SEQUENCE "public"."msp_sv_litsenz2_id_seq" OWNED BY "msp_sv_litsenz"."id";
ALTER SEQUENCE "public"."msp_sv_okved_dop_id_seq" OWNED BY "msp_sv_okved_dop"."id";
ALTER SEQUENCE "public"."msp_sv_okved_id_seq" OWNED BY "msp_sv_okved"."id";
ALTER SEQUENCE "public"."msp_sv_okved_osn_id_seq" OWNED BY "msp_sv_okved_osn"."id";
ALTER SEQUENCE "public"."msp_sv_prod_id_seq" OWNED BY "msp_sv_prod"."id";
ALTER SEQUENCE "public"."msp_sv_prog_dog_id_seq" OWNED BY "msp_sv_dog"."id";
ALTER SEQUENCE "public"."msp_sv_prog_part_id_seq" OWNED BY "msp_sv_prog_part"."id";
ALTER SEQUENCE "public"."msp_sved_mn_id_seq" OWNED BY "msp_sved_mn"."id";
ALTER SEQUENCE "public"."register_disqualified_id_seq" OWNED BY "register_disqualified"."id";
ALTER SEQUENCE "public"."request_id_seq" OWNED BY "company_search"."id";
ALTER SEQUENCE "public"."rus_terrorists_id_seq" OWNED BY "terrorists_fl_rus"."id";
ALTER SEQUENCE "public"."terrorists_ul_rus_id_seq" OWNED BY "terrorists_ul_rus"."id";

-- ----------------------------
-- Primary Key structure for table company_card
-- ----------------------------
ALTER TABLE "public"."company_card" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table company_search
-- ----------------------------
ALTER TABLE "public"."company_search" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table company_search_result
-- ----------------------------
ALTER TABLE "public"."company_search_result" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table court_arbitration
-- ----------------------------
ALTER TABLE "public"."court_arbitration" ADD UNIQUE ("ogrn");

-- ----------------------------
-- Primary Key structure for table court_arbitration
-- ----------------------------
ALTER TABLE "public"."court_arbitration" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table court_arbitration_case
-- ----------------------------
ALTER TABLE "public"."court_arbitration_case" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table court_arbitration_case_members
-- ----------------------------
ALTER TABLE "public"."court_arbitration_case_members" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table disqualified_persons
-- ----------------------------
ALTER TABLE "public"."disqualified_persons" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table expired_passports
-- ----------------------------
ALTER TABLE "public"."expired_passports" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table financial_statement
-- ----------------------------
ALTER TABLE "public"."financial_statement" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mass_address
-- ----------------------------
ALTER TABLE "public"."mass_address" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mass_founders
-- ----------------------------
ALTER TABLE "public"."mass_founders" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table mass_leaders
-- ----------------------------
ALTER TABLE "public"."mass_leaders" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_dokument
-- ----------------------------
ALTER TABLE "public"."msp_dokument" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_fayl
-- ----------------------------
ALTER TABLE "public"."msp_fayl" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_gorod
-- ----------------------------
ALTER TABLE "public"."msp_gorod" ADD UNIQUE ("msp_sved_mn_id");

-- ----------------------------
-- Primary Key structure for table msp_gorod
-- ----------------------------
ALTER TABLE "public"."msp_gorod" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_id_otpr
-- ----------------------------
ALTER TABLE "public"."msp_id_otpr" ADD UNIQUE ("msp_fayl_id");

-- ----------------------------
-- Primary Key structure for table msp_id_otpr
-- ----------------------------
ALTER TABLE "public"."msp_id_otpr" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_ip_vkl_msp
-- ----------------------------
ALTER TABLE "public"."msp_ip_vkl_msp" ADD UNIQUE ("msp_dokument_id");

-- ----------------------------
-- Primary Key structure for table msp_ip_vkl_msp
-- ----------------------------
ALTER TABLE "public"."msp_ip_vkl_msp" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_nasel_punkt
-- ----------------------------
ALTER TABLE "public"."msp_nasel_punkt" ADD UNIQUE ("msp_sved_mn_id");

-- ----------------------------
-- Primary Key structure for table msp_nasel_punkt
-- ----------------------------
ALTER TABLE "public"."msp_nasel_punkt" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_org_vkl_msp
-- ----------------------------
ALTER TABLE "public"."msp_org_vkl_msp" ADD UNIQUE ("msp_dokument_id");

-- ----------------------------
-- Primary Key structure for table msp_org_vkl_msp
-- ----------------------------
ALTER TABLE "public"."msp_org_vkl_msp" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_rayon
-- ----------------------------
ALTER TABLE "public"."msp_rayon" ADD UNIQUE ("msp_sved_mn_id");

-- ----------------------------
-- Primary Key structure for table msp_rayon
-- ----------------------------
ALTER TABLE "public"."msp_rayon" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_sv_dog
-- ----------------------------
ALTER TABLE "public"."msp_sv_dog" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_sv_kontr
-- ----------------------------
ALTER TABLE "public"."msp_sv_kontr" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_sv_litsenz
-- ----------------------------
ALTER TABLE "public"."msp_sv_litsenz" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_sv_okved
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved" ADD UNIQUE ("msp_dokument_id");

-- ----------------------------
-- Primary Key structure for table msp_sv_okved
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_sv_okved_dop
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved_dop" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_sv_okved_osn
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved_osn" ADD UNIQUE ("msp_sv_okved_id");

-- ----------------------------
-- Primary Key structure for table msp_sv_okved_osn
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved_osn" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_sv_prod
-- ----------------------------
ALTER TABLE "public"."msp_sv_prod" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table msp_sv_prog_part
-- ----------------------------
ALTER TABLE "public"."msp_sv_prog_part" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table msp_sved_mn
-- ----------------------------
ALTER TABLE "public"."msp_sved_mn" ADD UNIQUE ("msp_dokument_id");

-- ----------------------------
-- Primary Key structure for table msp_sved_mn
-- ----------------------------
ALTER TABLE "public"."msp_sved_mn" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table register_disqualified
-- ----------------------------
ALTER TABLE "public"."register_disqualified" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table terrorists_fl_rus
-- ----------------------------
ALTER TABLE "public"."terrorists_fl_rus" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table terrorists_ul_rus
-- ----------------------------
ALTER TABLE "public"."terrorists_ul_rus" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."company_search_result"
-- ----------------------------
ALTER TABLE "public"."company_search_result" ADD FOREIGN KEY ("request_id") REFERENCES "public"."company_search" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."court_arbitration_case"
-- ----------------------------
ALTER TABLE "public"."court_arbitration_case" ADD FOREIGN KEY ("ogrn_id") REFERENCES "public"."court_arbitration" ("ogrn") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."court_arbitration_case_members"
-- ----------------------------
ALTER TABLE "public"."court_arbitration_case_members" ADD FOREIGN KEY ("court_arbitration_case_id") REFERENCES "public"."court_arbitration_case" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_dokument"
-- ----------------------------
ALTER TABLE "public"."msp_dokument" ADD FOREIGN KEY ("msp_fayl_id") REFERENCES "public"."msp_fayl" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_gorod"
-- ----------------------------
ALTER TABLE "public"."msp_gorod" ADD FOREIGN KEY ("msp_sved_mn_id") REFERENCES "public"."msp_sved_mn" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_id_otpr"
-- ----------------------------
ALTER TABLE "public"."msp_id_otpr" ADD FOREIGN KEY ("msp_fayl_id") REFERENCES "public"."msp_fayl" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_ip_vkl_msp"
-- ----------------------------
ALTER TABLE "public"."msp_ip_vkl_msp" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_nasel_punkt"
-- ----------------------------
ALTER TABLE "public"."msp_nasel_punkt" ADD FOREIGN KEY ("msp_sved_mn_id") REFERENCES "public"."msp_sved_mn" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_org_vkl_msp"
-- ----------------------------
ALTER TABLE "public"."msp_org_vkl_msp" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_rayon"
-- ----------------------------
ALTER TABLE "public"."msp_rayon" ADD FOREIGN KEY ("msp_sved_mn_id") REFERENCES "public"."msp_sved_mn" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_dog"
-- ----------------------------
ALTER TABLE "public"."msp_sv_dog" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_kontr"
-- ----------------------------
ALTER TABLE "public"."msp_sv_kontr" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_litsenz"
-- ----------------------------
ALTER TABLE "public"."msp_sv_litsenz" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_okved"
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_okved_dop"
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved_dop" ADD FOREIGN KEY ("msp_sv_okved_id") REFERENCES "public"."msp_sv_okved" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_okved_osn"
-- ----------------------------
ALTER TABLE "public"."msp_sv_okved_osn" ADD FOREIGN KEY ("msp_sv_okved_id") REFERENCES "public"."msp_sv_okved" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_prod"
-- ----------------------------
ALTER TABLE "public"."msp_sv_prod" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sv_prog_part"
-- ----------------------------
ALTER TABLE "public"."msp_sv_prog_part" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Key structure for table "public"."msp_sved_mn"
-- ----------------------------
ALTER TABLE "public"."msp_sved_mn" ADD FOREIGN KEY ("msp_dokument_id") REFERENCES "public"."msp_dokument" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
