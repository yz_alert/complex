--attachment | many
DROP TABLE IF EXISTS "public"."ntf_attachment";
CREATE TABLE "public"."ntf_attachment" (
  "id"                          SERIAL PRIMARY KEY,
  "ntf_lot_id"                  INT4,

  "published_content_id"        TEXT COLLATE "default",
  "file_name"                   TEXT COLLATE "default",
  "file_size"                   TEXT COLLATE "default",
  "doc_description"             TEXT COLLATE "default",
  "doc_date"                    TEXT COLLATE "default",
  "url"                         TEXT COLLATE "default",
  "content_id"                  TEXT COLLATE "default",
  "content"                     TEXT COLLATE "default",
  "crypto_signs_signature_type" TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


