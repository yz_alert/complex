--procedure_info_prequalification | one
DROP TABLE IF EXISTS "public"."ntf_procedure_info_prequalification";
CREATE TABLE "public"."ntf_procedure_info_prequalification" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "date"        TEXT COLLATE "default",
  "place"       TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


