--customer_requirement | many
DROP TABLE IF EXISTS "public"."ntf_customer_requirement";
CREATE TABLE "public"."ntf_customer_requirement" (
  "id"                                       SERIAL PRIMARY KEY,
  "ntf_lot_id"                               INT4,

  "customer_reg_num"                         TEXT COLLATE "default",
  "customer_cons_registry_num"               TEXT COLLATE "default",
  "customer_full_name"                       TEXT COLLATE "default",

  "max_price"                                TEXT COLLATE "default",
  "delivery_place"                           TEXT COLLATE "default",

  "delivery_term"                            TEXT COLLATE "default",
  --TODO:редко 2 (ZK,ZP)
  "oneside_rejection"                        TEXT COLLATE "default",

  "application_guarantee_amount"             TEXT COLLATE "default",
  "application_guarantee_part"               TEXT COLLATE "default",
  "application_guarantee_procedure_info"     TEXT COLLATE "default",
  "application_guarantee_settlement_account" TEXT COLLATE "default",
  "application_guarantee_personal_account"   TEXT COLLATE "default",
  "application_guarantee_bik"                TEXT COLLATE "default",

  "contract_guarantee_amount"                TEXT COLLATE "default",
  "contract_guarantee_part"                  TEXT COLLATE "default",
  "contract_guarantee_procedure_info"        TEXT COLLATE "default",
  "contract_guarantee_settlement_account"    TEXT COLLATE "default",
  "contract_guarantee_personal_account"      TEXT COLLATE "default",
  "contract_guarantee_bik"                   TEXT COLLATE "default",

  "add_info"                                 TEXT COLLATE "default",
  "purchase_code"                            TEXT COLLATE "default",

  "tender_plan_info_plan_number"             TEXT COLLATE "default",
  "tender_plan_info_position_number"         TEXT COLLATE "default",
  "tender_plan_info_purchase83st544"         TEXT COLLATE "default",
  "tender_plan_info_plan2017_number"         TEXT COLLATE "default",

  "tender_plan_info_position2017_number"     TEXT COLLATE "default",
  "tender_plan_info_position2017_ext_number" TEXT COLLATE "default",

  --TODO:убрать общую сумму
  "nonbudget_financings_total_sum"           TEXT COLLATE "default",
  --TODO:убрать общую сумму
  "budget_financings_total_sum"              TEXT COLLATE "default",

  "bo_info_bo_number"                        TEXT COLLATE "default",
  "bo_info_bo_date"                          TEXT COLLATE "default",
  "bo_info_input_bo_flag"                    TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


