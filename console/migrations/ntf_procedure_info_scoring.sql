--procedure_info_scoring | one
DROP TABLE IF EXISTS "public"."ntf_procedure_info_scoring";
CREATE TABLE "public"."ntf_procedure_info_scoring" (
  "id"          SERIAL PRIMARY KEY,
  "ntf_main_id" INT4,

  "date"        TEXT COLLATE "default",
  "place"       TEXT COLLATE "default",
  "add_info"    TEXT COLLATE "default"

) WITH (OIDS = FALSE
);


