<?php
/**
 * Created by PhpStorm.
 * User: alert
 * Date: 25.09.2017
 * Time: 22:02
 */

namespace console\controllers;

use common\models\CompanyCard;
use common\models\CompanySearchResult;
use common\models\CourtArbitration;
use common\models\CourtArbitrationCase;
use common\models\CourtArbitrationCaseMembers;
use common\models\FinancialStatement;
use common\models\MspDokument;
use common\models\MspFayl;
use common\models\MspGorod;
use common\models\MspIdOtpr;
use common\models\MspIpVklMsp;
use common\models\MspNaselPunkt;
use common\models\MspOrgVklMsp;
use common\models\MspRayon;
use common\models\MspSvDog;
use common\models\MspSvedMn;
use common\models\MspSvKontr;
use common\models\MspSvLitsenz;
use common\models\MspSvOkved;
use common\models\MspSvOkvedDop;
use common\models\MspSvOkvedOsn;
use common\models\MspSvProd;
use common\models\MspSvProgPart;
use common\models\UnfairSupplier;
use common\models\UnfairSupplierFounder;
use yii\console\Controller;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;
use common\models\CompanySearch;
use Yii;

class ZcbController extends Controller
{
    //private $apiKey = '1s5xU1d-hq5adg5XmfXfIemZPGh6Ce-P';
    //private $domain = 'https://zachestnyibiznesapi.ru/demo/data';

    public function actionFtpZakupki44()
    {
        $start = microtime(true);
        $counter = 0;
        $user = "free";
        $password = "free";
        $host = "ftp.zakupki.gov.ru";
        $connect = ftp_connect($host);
        $result = ftp_login($connect, $user, $password);
        if (!$result) {
            exit("Не могу соединиться");
        }
        $contents = $contents = ftp_nlist($connect, "/fcs_fas/unfairSupplier/currMonth");
        $path = 'input/unfairSupplier/';
        $extractedPath = $path . '/extracted';
        UnfairSupplier::downloadFromFTP($connect, $contents, $path, $extractedPath);

        ///*
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $contentString = file_get_contents($extractedPath . '/' . $file);
                    $processedString = str_replace('oos:', "", $contentString);
                    $processedString = str_replace('ns2:', "", $processedString);
                    $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                    //print_r($file . "\n");
                    if (pathinfo($extractedPath . '/' . $file, PATHINFO_EXTENSION) == 'xml') {
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);
                    } else {
                        unlink($extractedPath . '/' . $file);
                    }
                    if ($xml) {
                        $counter++;
                        UnfairSupplier::newRecord($xml);
                    } else {
                        echo "Ошибка в файле: " . $file . PHP_EOL;
                    }
                    if ($counter % 100 == 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
        //*/
        ftp_quit($connect);

        echo "Завершено. Обработано файлов: " . $counter;
        echo PHP_EOL;
        $time = (int)microtime(true) - $start;
        print_r("Время выполнения скрипта: " . $time . " сек.");
        //print_r("Время выполнения скрипта: " . microtime(true) - $start);
        echo PHP_EOL;
    }

    public function actionFtpZakupki223()
    {
        $start = microtime(true);
        $counter = 0;
        $user = "fz223free";
        $password = "fz223free";
        $host = "ftp.zakupki.gov.ru";
        $connect = ftp_connect($host);
        $result = ftp_login($connect, $user, $password);
        if (!$result) {
            exit("Не могу соединиться");
        }
        $contents = $contents = ftp_nlist($connect, "/fcs_fas/unfairSupplier/currMonth");
        $path = 'input/unfairSupplier/';
        $extractedPath = $path . '/extracted';
        UnfairSupplier::downloadFromFTP($connect, $contents, $path, $extractedPath);

        ///*
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $contentString = file_get_contents($extractedPath . '/' . $file);
                    $processedString = str_replace('oos:', "", $contentString);
                    $processedString = str_replace('ns2:', "", $processedString);
                    $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                    //print_r($file . "\n");
                    if (pathinfo($extractedPath . '/' . $file, PATHINFO_EXTENSION) == 'xml') {
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);
                    } else {
                        unlink($extractedPath . '/' . $file);
                    }
                    if ($xml) {
                        $counter++;
                        UnfairSupplier::newRecord($xml);
                    } else {
                        echo "Ошибка в файле: " . $file . PHP_EOL;
                    }
                    if ($counter % 100 == 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
        //*/
        ftp_quit($connect);

        echo "Завершено. Обработано файлов: " . $counter;
        echo PHP_EOL;
        print_r("Время выполнения скрипта: " . (int)microtime(true) - $start . " секунд.");
        //print_r("Время выполнения скрипта: " . microtime(true) - $start);
        echo PHP_EOL;
    }

    public function actionFillMsp()
    {
        $counter = 0;
        $counterFiles = 0;
        //$path = 'input/smp';
        $path = 'input/smp_temp';
        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    echo "$file\n";
                    $counterFiles++;
                    $xml = simplexml_load_string(
                        file_get_contents($path . '/' . $file),
                        "SimpleXMLElement",
                        LIBXML_NOCDATA);
                    //print_r($xml);
                    //die;
                    Yii::$app->db->createCommand()->batchInsert('msp_fayl',
                        [
                            'id_fayl',
                            'vers_form',
                            'tip_inf',
                            'vers_prog',
                            'kol_dok',
                        ], [
                            [
                                (string)$xml['ИдФайл'],
                                (string)$xml['ВерсФорм'],
                                (string)$xml['ТипИнф'],
                                (string)$xml['ВерсПрог'],
                                (string)$xml['КолДок'],
                            ],
                        ])->execute();
                    $mspDokumentId = Yii::$app->db->getLastInsertID();

                    Yii::$app->db->createCommand()->batchInsert('msp_id_otpr', [
                        'msp_fayl_id',
                        'dolzh_otv',
                        'tlf',
                        'email',
                        'fio_otv_familiya',
                        'fio_otv_imya',
                        'fio_otv_otchestvo',
                    ], [
                        [
                            Yii::$app->db->getLastInsertID(),
                            (string)$xml->ИдОтпр['ДолжОтв'],
                            (string)$xml->ИдОтпр['Тлф'],
                            (string)$xml->ИдОтпр['Email'],
                            (string)$xml->ИдОтпр->ФИООтв['Фамилия'],
                            (string)$xml->ИдОтпр->ФИООтв['Имя'],
                            (string)$xml->ИдОтпр->ФИООтв['Отчество'],
                        ],
                    ])->execute();

                    foreach ($xml->Документ as $element) {
                        $counter++;

                        Yii::$app->db->createCommand()->batchInsert('msp_dokument', [
                            'msp_fayl_id',
                            'id_dok',
                            'data_sost',
                            'data_vkl_msp',
                            'vid_sub_msp',
                            'kat_sub_msp',
                            'priz_nov_msp',
                        ], [
                            [
                                $mspDokumentId,
                                (string)$element['ИдДок'],
                                (string)$element['ДатаСост'],
                                (string)$element['ДатаВклМСП'],
                                (string)$element['ВидСубМСП'],
                                (string)$element['КатСубМСП'],
                                (string)$element['ПризНовМСП'],
                            ],
                        ])->execute();

                        if ($element->ОргВклМСП) {
                            $mspOrgVklMsp = new MspOrgVklMsp();
                            $mspOrgVklMsp->msp_dokument_id = $mspDokumentId;

                            if ($element->ОргВклМСП['НаимОрг']) {
                                $mspOrgVklMsp->naim_org = (string)$element->ОргВклМСП['НаимОрг'];
                            }
                            if ($element->ОргВклМСП['НаимОргСокр']) {
                                $mspOrgVklMsp->naim_org_sokr = (string)$element->ОгрВклМСП['НаимОргСокр'];
                            }
                            if ($element->ОргВклМСП['ИННЮЛ']) {
                                $mspOrgVklMsp->innul = (string)$element->ОргВклМСП['ИННЮЛ'];
                            }
                            $mspOrgVklMsp->save();
                        }
                        if ($element->ИПВклМСП) {
                            $mspIpVklMsp = new MspIpVklMsp();
                            $mspIpVklMsp->msp_dokument_id = $mspDokumentId;

                            if ($element->ИПВклМСП['ИННФЛ']) {
                                $mspIpVklMsp->inn = (string)$element->ИПВклМСП['ИННФЛ'];
                            }
                            if ($element->ИПВклМСП->ФИОИП['Фамилия']) {
                                $mspIpVklMsp->fio_ip_familiya = (string)$element->ИПВклМСП->ФИОИП['Фамилия'];
                            }
                            if ($element->ИПВклМСП->ФИОИП['Имя']) {
                                $mspIpVklMsp->fio_ip_imya = (string)$element->ИПВклМСП->ФИОИП['Имя'];
                            }
                            if ($element->ИПВклМСП->ФИОИП['Отчество']) {
                                $mspIpVklMsp->fio_ip_otchestvo = (string)$element->ИПВклМСП->ФИОИП['Отчество'];
                            }
                            $mspIpVklMsp->save();
                        }

                        if ($element->СведМН) {
                            $mspSvedMn = new MspSvedMn();
                            $mspSvedMn->msp_dokument_id = $mspDokumentId;

                            if ($element->СведМН['КодРегион']) {
                                $mspSvedMn->kod_region = (string)$element->СведМН['КодРегион'];
                            }
                            if ($element->СведМН->Регион['Тип']) {
                                $mspSvedMn->region_tip = (string)$element->СведМН->Регион['Тип'];
                            }
                            if ($element->СведМН->Регион['Наим']) {
                                $mspSvedMn->region_naim = (string)$element->СведМН->Регион['Наим'];
                            }
                            $mspSvedMn->save();

                            if ($element->СведМН->Район) {
                                $mspRayon = new MspRayon();
                                $mspRayon->msp_sved_mn_id = $mspSvedMn->id;

                                if ($element->СведМН->Район['Тип']) {
                                    $mspRayon->tip = (string)$element->СведМН->Район['Тип'];
                                }
                                if ($element->СведМН->Район['Наим']) {
                                    $mspRayon->naim = (string)$element->СведМН->Район['Наим'];
                                }
                                $mspRayon->save();
                            }
                            if ($element->СведМН->Город) {
                                $mspGorod = new MspGorod();
                                $mspGorod->msp_sved_mn_id = $mspSvedMn->id;

                                if ($element->СведМН->Город['Тип']) {
                                    $mspGorod->tip = (string)$element->СведМН->Город['Тип'];
                                }
                                if ($element->СведМН->Город['Наим']) {
                                    $mspGorod->naim = (string)$element->СведМН->Город['Наим'];
                                }
                                $mspGorod->save();
                            }
                            if ($element->СведМН->НаселПункт) {
                                $mspNaselPunkt = new MspNaselPunkt();
                                $mspNaselPunkt->msp_sved_mn_id = $mspSvedMn->id;

                                if ($element->СведМН->НаселПункт['Тип']) {
                                    $mspNaselPunkt->tip = (string)$element->СведМН->НаселПункт['Тип'];
                                }
                                if ($element->СведМН->НаселПункт['Наим']) {
                                    $mspNaselPunkt->naim = (string)$element->СведМН->НаселПункт['Наим'];
                                }
                                $mspNaselPunkt->save();
                            }
                        }
                        if ($element->СвОКВЭД) {
                            $mspSvOkved = new MspSvOkved();
                            $mspSvOkved->msp_dokument_id = $mspDokumentId;
                            $mspSvOkved->save();

                            if ($element->СвОКВЭД->СвОКВЭДОсн) {
                                $mspSvOkvedOsn = new MspSvOkvedOsn();
                                $mspSvOkvedOsn->msp_sv_okved_id = $mspSvOkved->id;

                                if ($element->СвОКВЭД->СвОКВЭДОсн['КодОКВЭД']) {
                                    $mspSvOkvedOsn->kod_okved = (string)$element->СвОКВЭД->СвОКВЭДОсн['КодОКВЭД'];
                                }
                                if ($element->СвОКВЭД->СвОКВЭДОсн['НаимОКВЭД']) {
                                    $mspSvOkvedOsn->naim_okved = (string)$element->СвОКВЭД->СвОКВЭДОсн['НаимОКВЭД'];
                                }
                                if ($element->СвОКВЭД->СвОКВЭДОсн['ВерсОКВЭД']) {
                                    $mspSvOkvedOsn->vers_okved = (string)$element->СвОКВЭД->СвОКВЭДОсн['ВерсОКВЭД'];
                                }

                                $mspSvOkvedOsn->save();
                            }
                            if ($element->СвОКВЭД->СвОКВЭДДоп) {
                                foreach ($element->СвОКВЭД->СвОКВЭДДоп as $dopOkved) {
                                    $mspSvOkvedDop = new MspSvOkvedDop();
                                    $mspSvOkvedDop->msp_sv_okved_id = $mspSvOkved->id;
                                    if ($dopOkved['КодОКВЭД']) {
                                        $mspSvOkvedDop->kod_okved = (string)$dopOkved['КодОКВЭД'];
                                    }
                                    if ($dopOkved['НаимОКВЭД']) {
                                        $mspSvOkvedDop->naim_okved = (string)$dopOkved['НаимОКВЭД'];
                                    }
                                    if ($dopOkved['ВерсОКВЭД']) {
                                        $mspSvOkvedDop->vers_okved = (string)$dopOkved['ВерсОКВЭД'];
                                    }

                                    $mspSvOkvedDop->save();
                                }
                            }
                        }

                        if ($element->СвЛиценз) {
                            $arraySvLitsenz = [];
                            foreach ($element->СвЛиценз as $svLitsenz) {
                                $inSvLitsenz = [];
                                array_push($inSvLitsenz,
                                    $mspDokumentId,
                                    (string)$svLitsenz['СерЛиценз'],
                                    (string)$svLitsenz['НомЛиценз'],
                                    (string)$svLitsenz['ВидЛиценз'],
                                    (string)$svLitsenz['ДатаЛиценз'],
                                    (string)$svLitsenz['ДатаНачЛиценз'],
                                    (string)$svLitsenz['ДатаКонЛиценз'],
                                    (string)$svLitsenz['ОргВыдЛиценз'],
                                    (string)$svLitsenz['ДатаОстЛиценз'],
                                    (string)$svLitsenz['ОргОстЛиценз'],
                                    (string)$svLitsenz->НаимЛицВД,
                                    (string)$svLitsenz->СведАдрЛицВД
                                );
                                array_push($arraySvLitsenz, $inSvLitsenz);
                            }
                            Yii::$app->db->createCommand()->batchInsert('msp_sv_litsenz', [
                                'msp_dokument_id',
                                'ser_litsenz',
                                'nom_litsenz',
                                'vid_litsenz',
                                'data_litsenz',
                                'data_nach_litsenz',
                                'data_kon_litsenz',
                                'org_vyd_litsenz',
                                'data_ost_litsenz',
                                'org_ost_litsenz',
                                'naim_litsenz_vd',
                                'sved_adr_lits_vd',
                            ], [
                                [
                                    $arraySvLitsenz,
                                ],
                            ])->execute();
                        }

                        if ($element->СвПрод) {
                            foreach ($element->СвПрод as $svProd) {
                                $mspSvProd = new MspSvProd();
                                $mspSvProd->msp_dokument_id = $mspDokumentId;

                                if ($svProd['КодПрод']) {
                                    $mspSvProd->kod_prod = (string)$svProd['КодПрод'];
                                }
                                if ($svProd['НаимПрод']) {
                                    $mspSvProd->naim_prod = (string)$svProd['НаимПрод'];
                                }
                                if ($svProd['ПрОтнПрод']) {
                                    $mspSvProd->pr_otn_prod = (string)$svProd['ПрОтнПрод'];
                                }

                                $mspSvProd->save();
                            }
                        }
                        if ($element->СвПрогПарт) {
                            foreach ($element->СвПрогПарт as $svProgPart) {
                                $mspSvProgPart = new MspSvProgPart();
                                $mspSvProgPart->msp_dokument_id = $mspDokumentId;

                                if ($svProgPart['НаимЮЛ_ПП']) {
                                    $mspSvProgPart->naim_ul_pp = (string)$svProgPart['НаимЮЛ_ПП'];
                                }
                                if ($svProgPart['ИННЮЛ_ПП']) {
                                    $mspSvProgPart->inn_ul_pp = (string)$svProgPart['ИННЮЛ_ПП'];
                                }
                                if ($svProgPart['НомДог']) {
                                    $mspSvProgPart->nom_dog = (string)$svProgPart['НомДог'];
                                }
                                if ($svProgPart['ДатаДог']) {
                                    $mspSvProgPart->data_dog = (string)$svProgPart['ДатаДог'];
                                }

                                $mspSvProgPart->save();
                            }
                        }
                        if ($element->СвКонтр) {
                            $arraySvKontr = [];
                            foreach ($element->СвКонтр as $svKontr) {
                                $inSvKontr = [];
                                array_push($inSvKontr,
                                    (string)$mspDokumentId,
                                    (string)$svKontr['НаимЮЛ_ЗК'],
                                    (string)$svKontr['ИННЮЛ_ЗК'],
                                    (string)$svKontr['ПредмКонтр'],
                                    (string)$svKontr['НомКонтрРеестр'],
                                    (string)$svKontr['НомКонтрРеестр']
                                );

                                array_push($arraySvKontr, $inSvKontr);
                            }
                            Yii::$app->db->createCommand()->batchInsert('msp_sv_kontr', [
                                'msp_dokument_id',
                                'naim_ul_zd',
                                'inn_ul_zd',
                                'predm_kontr',
                                'nom_kontr_reestr',
                                'data_kontr',
                            ], [
                                [
                                    $arraySvKontr,
                                ],
                            ])->execute();
                        }

                        $arraySvKontr = [];
                        foreach ($element->СвДог as $svDog) {
                            $mspSvDog = new MspSvDog();
                            $mspSvDog->msp_dokument_id = $mspDokumentId;

                            if ($svDog['НаимЮЛ_ЗД']) {
                                $mspSvDog->naim_ul_zd = (string)$svDog['НаимЮЛ_ЗД'];
                            }
                            if ($svDog['ИННЮЛ_ЗД']) {
                                $mspSvDog->inn_ul_zd = (string)$svDog['ИННЮЛ_ЗД'];
                            }
                            if ($svDog['ПредмДог']) {
                                $mspSvDog->predm_dog = (string)$svDog['ПредмДог'];
                            }
                            if ($svDog['НомДогРеестр']) {
                                $mspSvDog->nom_dog_reestr = (string)$svDog['НомДогРеестр'];
                            }
                            if ($svDog['ДатаДог']) {
                                $mspSvDog->data_dog = (string)$svDog['ДатаДог'];
                            }
                            $mspSvDog->save();
                        }
                    }
                }
                //1 файл
                //break;
            }
        }
        closedir($handle);
    }

    public function actionTest()
    {
        $request_string = '6455006215';
        $model = CompanySearch::findOne(['request_string' => $request_string]);
        print_r(Json::decode($model->response));
        die;
        foreach (ArrayHelper::getValue(Json::decode($model->response), 'docs') as $result) {
            //ИНН
            if (ArrayHelper::keyExists('ИНН', $result)) {
                if (ArrayHelper::getValue($result, 'ИНН') == '530701656918') {
                    continue;
                    //return $result;
                }
            }
            //ИННФЛ
            if (ArrayHelper::keyExists('ИННФЛ', $result)) {
                if (ArrayHelper::getValue($result, 'ИННФЛ') == '530701656918') {
                    //var_dump($result);
                    $companySearchResult = $result;
                    $result = new CompanySearchResult();
                    $result->request_id = 39;
                    $result->ogrn = ArrayHelper::getValue($companySearchResult, 'id');
                    $result->inn = ArrayHelper::getValue($result, 'ИННФЛ');
                    $result->save();
                    if (!$result->save()) {
                        echo PHP_EOL . $result->getErrors();
                    }
                    var_dump(CompanySearchResult::findOne(['inn' => $result->inn]));
                    return CompanySearchResult::findOne(['inn' => $result->inn]);

                }
            } else {
                continue;
            }

        }
    }
}






