<?php

namespace console\controllers;

use common\models\MassAddress;
use common\models\RegisterDisqualified;
use serhatozles\simplehtmldom\SimpleHTMLDom;
use yii\console\Controller;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;
use common\models\CompanySearch;
use Yii;
use console\models\Notification;

class NalogController extends Controller
{
    public static function clearString($string)
    {
        $string = trim($string);
        //Удаление системных символов из строки
        $string = preg_replace('/\r\n|\r|\n/u', ' ', $string);
        //Из HTML в строку
        $string = html_entity_decode($string);
        //Удаление пробелов в середине строки
        $string = preg_replace("/ {2,}/", " ", $string);
        //Удаление символов (Tab и т.д.)
        $string = preg_replace("/\s{2,}/", ' ', $string);

        return $string;
    }

    public function actionMassAddress()
    {
        $url = 'https://www.nalog.ru/opendata/7707329152-masaddress/';
        $html = SimpleHTMLDom::str_get_html(MassAddress::curlGetNewProxy($url));

        if ($html->find('div .content', 0) !== null) {
            foreach ($html->find('div .content', 0)->find('tr') as $noticeTabBoxWrapper) {
                if ($noticeTabBoxWrapper->find('td', 0) !== null) {
                    $key = self::clearString($noticeTabBoxWrapper->find('td', 1)->plaintext);
                    $value = self::clearString($noticeTabBoxWrapper->find('td', 2)->plaintext);
                    if (strpos($key, 'Гиперссылки (URL) на предыдущие релизы набора данных') !== false) {
                        $url = $value;
                    }
                }
            }
        }

        //TODO: добавить Proxy и вынести метод curl отдельно для закгрузки файлов

        $massAddressFolder = "input/mass_address";

        if (!is_dir($massAddressFolder)) {
            mkdir($massAddressFolder, 0777, true);
        }
        $filename = "result_nalog.csv";
        $path = "$massAddressFolder/$filename";
        $fp = fopen($path, 'w');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        MassAddress::deleteAll();

        $content = file_get_contents($path);
        $massAddresses = explode("\r\n", $content);
        unset($massAddresses[0]);
        unset($massAddresses[count($massAddresses)]);

        foreach ($massAddresses as $massAddress) {
            $elements = explode(";", $massAddress);

            $model = new MassAddress();

            $model->region = $elements[0];
            $model->location = $elements[1];
            $model->city = $elements[2];
            $model->settlement = $elements[3];
            $model->street = $elements[4];
            $model->house_number = $elements[5];
            $model->corpus_number = $elements[6];
            $model->office_number = $elements[7];
            $model->quantity = $elements[8];

            $model->save();
        }

    }

    public function actionRegisterDisqualified()
    {
        $url = 'https://www.nalog.ru/opendata/7707329152-registerdisqualified/';
        $html = SimpleHTMLDom::str_get_html(MassAddress::curlGetNewProxy($url));

        if ($html->find('div .content', 0) !== null) {
            foreach ($html->find('div .content', 0)->find('tr') as $noticeTabBoxWrapper) {
                if ($noticeTabBoxWrapper->find('td', 0) !== null) {
                    $key = self::clearString($noticeTabBoxWrapper->find('td', 1)->plaintext);
                    $value = self::clearString($noticeTabBoxWrapper->find('td', 2)->plaintext);
                    if (strpos($key, 'Гиперссылки (URL) на предыдущие релизы набора данных') !== false) {
                        $url = $value;
                    }
                }
            }
        }

        //TODO: добавить Proxy и вынести метод curl отдельно для закгрузки файлов

        $registerDisqualifiedFolder = "input/register_disqualified";

        if (!is_dir($registerDisqualifiedFolder)) {
            mkdir($registerDisqualifiedFolder, 0777, true);
        }
        $filename = "result_register_disqualified.csv";
        $path = "$registerDisqualifiedFolder/$filename";
        $fp = fopen($path, 'w');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        RegisterDisqualified::deleteAll();

        $content = file_get_contents($path);
        $registerDisqualified = explode("\r\n", $content);
        unset($registerDisqualified[0]);
        unset($registerDisqualified[count($registerDisqualified)]);

        foreach ($registerDisqualified as $rD) {
            $elements = explode(";", $rD);

            $model = new RegisterDisqualified();

            $model->number = (int)$elements[0];
            $model->full_name = $elements[1];
            $model->date_of_birth = $elements[2];
            $model->place_of_birth = $elements[3];
            $model->organization_name = $elements[4];
            $model->organization_inn = $elements[5];
            $model->position = $elements[6];
            $model->administrative_code_article = $elements[7];
            $model->protocol_organization_name = $elements[8];
            $model->judge_full_name = $elements[9];
            $model->judge_position = $elements[10];
            $model->period_of_ineligibility = $elements[11];
            $model->start_date = $elements[12];
            $model->expiration_date = $elements[13];

            $model->save();
        }
    }
}