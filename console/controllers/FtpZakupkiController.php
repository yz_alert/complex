<?php
/**
 * Created by PhpStorm.
 * User: alert
 * Date: 25.09.2017
 * Time: 22:02
 */

namespace console\controllers;

use common\models\UnfairSupplier44;
use common\models\UnfairSupplier223;
use common\models\UnfairSupplier615;
use common\models\UnfairSupplierFounder;
use console\models\Guarantee;
use yii\console\Controller;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;
use common\models\CompanySearch;
use Yii;
use console\models\Notification;

class FtpZakupkiController extends Controller
{
    public function actionUnfairSupplier44()
    {
        $start = microtime(true);
        $counter = 0;
        $user = "free";
        $password = "free";
        $host = "ftp.zakupki.gov.ru";
        $connect = ftp_connect($host) or die("Не удалось установить соединение с $host");
        $result = ftp_login($connect, $user, $password);
        if (!$result) {
            exit("Не могу соединиться");
        }
        $contents = $contents = ftp_nlist($connect, "/fcs_fas/unfairSupplier/currMonth");
        $contents2 = $contents = ftp_nlist($connect, "/fcs_fas/unfairSupplier");
        $path = 'input/unfairSupplier44/';
        $extractedPath = $path . Notification::LOCAL_EXTRACTED_PATH;
        UnfairSupplier44::downloadFromFTP($connect, $contents, $path, $extractedPath);
        UnfairSupplier44::downloadFromFTP($connect, $contents2, $path, $extractedPath);

        ///*
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $contentString = file_get_contents($extractedPath . '/' . $file);
                    $processedString = str_replace('oos:', "", $contentString);
                    $processedString = str_replace('ns2:', "", $processedString);
                    $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                    if (pathinfo($extractedPath . '/' . $file, PATHINFO_EXTENSION) == 'xml') {
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);
                    } else {
                        unlink($extractedPath . '/' . $file);
                    }
                    if ($xml) {
                        $counter++;
                        UnfairSupplier44::newRecord($xml);
                    } else {
                        echo "Ошибка в файле: " . $file . PHP_EOL;
                    }
                    if ($counter % 100 == 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
        //*/
        ftp_quit($connect);

        echo "Завершено. Обработано файлов: " . $counter;
        echo PHP_EOL;
    }

    public function actionUnfairSupplier615()
    {
        $start = microtime(true);
        $counter = 0;
        $user = "free";
        $password = "free";
        $host = "ftp.zakupki.gov.ru";
        $connect = ftp_connect($host) or die("Не удалось установить соединение с $host");
        $result = ftp_login($connect, $user, $password);
        if (!$result) {
            exit("Не могу соединиться");
        }
        $contents = $contents = ftp_nlist($connect, "/fcs_fas/pprf615unfairContractor/currMonth");
        $path = 'input/unfairSupplier615/';
        $extractedPath = $path . Notification::LOCAL_EXTRACTED_PATH;
        //----------
        UnfairSupplier615::downloadFromFTP($connect, $contents, $path, $extractedPath);

        ///*
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $contentString = file_get_contents($extractedPath . '/' . $file);
                    $processedString = str_replace('oos:', "", $contentString);
                    $processedString = str_replace('ns2:', "", $processedString);
                    $processedString = str_replace('ns3:', "", $processedString);
                    $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                    if (pathinfo($extractedPath . '/' . $file, PATHINFO_EXTENSION) == 'xml') {
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);
                    } else {
                        unlink($extractedPath . '/' . $file);
                    }
                    if ($xml) {
                        $counter++;
                        UnfairSupplier615::newRecord($xml);
                    } else {
                        echo "Ошибка в файле: " . $file . PHP_EOL;
                    }
                    if ($counter % 100 == 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
        //*/
        ftp_quit($connect);

        echo "Завершено. Обработано файлов: " . $counter;
        echo PHP_EOL;
        print_r("Время выполнения скрипта: " . microtime(true) - $start . " секунд.");
        echo PHP_EOL;
    }

    //Загрузить файлы xml в БД 223ФЗ
    public function actionSaveFromXmlToDb223()
    {
        $counter = 0;
        $path = 'input/unfairSupplier223/';
        $extractedPath = $path . '/extracted';
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    //print_r($file . "\n");
                    //Удаляем все пустые файлы
                    if (filesize($extractedPath . '/' . $file) < 1000 || pathinfo($extractedPath . '/' . $file,
                            PATHINFO_EXTENSION) !== 'xml') {
                        unlink($extractedPath . '/' . $file);
                    } else {
                        $contentString = file_get_contents($extractedPath . '/' . $file);
                        $processedString = str_replace('oos:', "", $contentString);
                        $processedString = str_replace('ns2:', "", $processedString);
                        $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);
                        UnfairSupplier223::newRecord($xml);
                        $counter++;
                    }
                    if ($counter % 100 == 0 && $counter != 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
    }

    public function actionSaveFromXmlToDb615()
    {
        $counter = 0;
        $path = 'input/unfairSupplier615/';
        $extractedPath = $path . '/extracted';
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    //print_r($file . "\n");
                    //Удаляем все пустые файлы
                    if (filesize($extractedPath . '/' . $file) < 1000 || pathinfo($extractedPath . '/' . $file,
                            PATHINFO_EXTENSION) !== 'xml') {
                        unlink($extractedPath . '/' . $file);
                    } else {
                        $contentString = file_get_contents($extractedPath . '/' . $file);
                        $processedString = str_replace('oos:', "", $contentString);
                        $processedString = str_replace('ns2:', "", $processedString);
                        $processedString = str_replace('ns3:', "", $processedString);
                        $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);

                        UnfairSupplier615::newRecord($xml);
                        $counter++;
                    }
                    if ($counter % 100 == 0 && $counter != 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
    }

    public function actionSaveFromXmlToDb44()
    {
        $counter = 0;
        $path = 'input/unfairSupplier44/';
        $extractedPath = $path . '/extracted';
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    //print_r($file . "\n");
                    //Удаляем все пустые файлы
                    if (filesize($extractedPath . '/' . $file) < 1000 || pathinfo($extractedPath . '/' . $file,
                            PATHINFO_EXTENSION) !== 'xml') {
                        unlink($extractedPath . '/' . $file);
                    } else {
                        $contentString = file_get_contents($extractedPath . '/' . $file);
                        $processedString = str_replace('oos:', "", $contentString);
                        $processedString = str_replace('ns2:', "", $processedString);
                        $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);
                        UnfairSupplier44::newRecord($xml);
                        $counter++;
                    }
                    if ($counter % 100 == 0 && $counter != 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
    }

    //Скачать и распаковать архивы 223ФЗ
    public function actionUnfairSupplier223()
    {
        $start = microtime(true);
        $counter = 0;
        $user = "fz223free";
        $password = "fz223free";
        $host = "ftp.zakupki.gov.ru";
        $connect = ftp_connect($host);
        $result = ftp_login($connect, $user, $password);
        if (!$result) {
            exit("Не могу соединиться");
        }

        $contents = ftp_nlist($connect, "/out/published");
        $path = 'input/unfairSupplier223/';
        $extractedPath = $path . Notification::LOCAL_EXTRACTED_PATH;
        UnfairSupplier223::downloadFromFTP($connect, $contents, $path, $extractedPath);
        die;
        ///*
        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $contentString = file_get_contents($extractedPath . '/' . $file);
                    $processedString = str_replace('oos:', "", $contentString);
                    $processedString = str_replace('ns2:', "", $processedString);
                    $processedString = str_replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="UTF-8"?>',
                        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>", $processedString);
                    //print_r($file . "\n");
                    if (pathinfo($extractedPath . '/' . $file, PATHINFO_EXTENSION) == 'xml') {
                        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
                            LIBXML_NOCDATA | LIBXML_NOBLANKS);
                    } else {
                        unlink($extractedPath . '/' . $file);
                    }
                    if ($xml) {
                        $counter++;
                        UnfairSupplier::newRecord($xml);
                    } else {
                        echo "Ошибка в файле: " . $file . PHP_EOL;
                    }
                    if ($counter % 100 == 0) {
                        print_r("Обработано файлов XML: " . $counter);
                        echo PHP_EOL;
                    }
                }
            }
            closedir($handle);
        }
        //*/
        ftp_quit($connect);

        echo "Завершено. Обработано файлов: " . $counter;
        //echo PHP_EOL;
        //print_r("Время выполнения скрипта: " . (int)microtime(true) - $start . " секунд.");
        //echo PHP_EOL;
    }

    public function actionParseNotificationsDeleteXml()
    {
        Notification::deleteDir();
    }

    //Очищает папку с данными (по формату)
    public function actionParseNotificationsClearFolder()
    {
        //Notification::clearFormatsFromFolder(Notification::LOCAL_ZIP_PATH . Notification::LOCAL_EXTRACTED_PATH_NORM);
    }

    //Распаковка архивов и извлечение xml за определенную дату
    public function actionParseNotificationsDate($from = '', $to = '')
    {
        //скачать все архивы
        self::actionParseNotificationsAsZip();

        $zipFilesPath = Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_ZIP_PATH;
        $extractedPath = Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_EXTRACTED_PATH;
        $dirBuffer = Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_EXTRACTED_PATH . Notification::LOCAL_EXTRACTED_PATH_BUFFER;

        //---------------Переменные для xml
        $totalCounter = 0;
        $xmlFormatCount = 0;
        $otherFormatCount = 0;
        $fcsPlacementResult = 0;
        $fcsClarification = 0;
        $fcsContractSign = 0;
        $fcsNotificationEFDateChange = 0;
        $fcsNotificationCancel = 0;
        $fcsNotificationCancelFailure = 0;
        $fcsNotificationLotCancel = 0;
        $fcsNotificationOrgChange = 0;
        $fcsPurchaseProlongationOK = 0;
        $fcsPurchaseProlongationZK = 0;
        $fcsNotificationEA44 = 0;
        $fcsNotificationEP44 = 0;
        $fcsNotificationINM = 0;
        $fcsNotificationOK44 = 0;
        $fcsNotificationOKU44 = 0;
        $fcsNotificationPO44 = 0;
        $fcsNotificationZK44 = 0;
        $fcsNotificationZP44 = 0;
        $fcsNotificationZA44 = 0;
        $fcsNotificationZKK = 0;
        $fcsNotificationOKD44 = 0;
        ///*---------------Переменные для xml

        if ($from === '') {
            $from = date("Ymd", strtotime('now'));
        } else {
            $from = date("Ymd", strtotime($from));
        }
        if ($to === '') {
            if ($from !== '') {
                $to = $from;
            } else {
                $to = date("Ymd", strtotime('now'));
            }
        } else {
            $to = date("Ymd", strtotime($to));
        }

        $startDate = date("Ymd", strtotime($from));
        $startDateClose = date("Ymd", strtotime($from . '+1day'));
        $endDate = date("Ymd", strtotime($to));

        $zipCounter = 0;
        $searchString = $startDate . "00_" . $startDateClose . "00";

        while ($startDate <= $endDate) {
            echo "\nПоиск по дню: " . date('d-M-Y', strtotime($startDate));
            if ($handle = opendir($zipFilesPath)) {
                while (false !== ($content = readdir($handle))) {
                    //if ($content != "." && $content != ".." && $content != "extracted" && $content != "zip") {
                    if ($content != "." && $content != "..") {
                        if (stripos(basename($content), $searchString) !== false) {
                            //Нужные действия над файлами
                            //вынеснти обработку zip в отдельную функцию

                            //-----------Выгрузка из zip во временную папку всех файлов
                            $local_file = $zipFilesPath . basename($content);

                            $zip = new \ZipArchive;
                            if ($zip->open($local_file) === true) {
                                $zip->extractTo($dirBuffer);
                                $zip->close();
                            } else {
                                echo "\n" . $local_file . "- archive not found\n";
                            }
                            $zipCounter++;
                            echo "\nUnpacked: " . $local_file . "\n";
                            echo "Processed archives: " . $zipCounter . "\n";
                            //-----------Выгрузка из zip во временную папку всех файлов

                            //---------------
                            if (!is_dir($dirBuffer)) {
                                mkdir($dirBuffer);
                            }

                            if ($innerHandle = opendir($dirBuffer)) {
                                while (false !== ($file = readdir($innerHandle))) {
                                    if ($file != "." && $file != "..") {
                                        $totalCounter++;
                                        if (pathinfo($dirBuffer . $file,
                                                PATHINFO_EXTENSION) == 'xml') {
                                            $xmlFormatCount++;
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsPlacementResult") !== false) {
                                                $fcsPlacementResult++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsPlacementResult");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsClarification") !== false) {
                                                $fcsClarification++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsClarification");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsContractSign") !== false) {
                                                $fcsContractSign++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsContractSign");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationEA44") !== false) {
                                                $fcsNotificationEA44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationEA44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationEP44") !== false) {
                                                $fcsNotificationEP44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationEP44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationINM") !== false) {
                                                $fcsNotificationINM++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationINM");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationOK44") !== false) {
                                                $fcsNotificationOK44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationOK44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationOKU44") !== false) {
                                                $fcsNotificationOKU44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationOKU44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationPO44") !== false) {
                                                $fcsNotificationPO44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationPO44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationZK44") !== false) {
                                                $fcsNotificationZK44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationZK44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationZA44") !== false) {
                                                $fcsNotificationZA44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationZA44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationZKK") !== false) {
                                                $fcsNotificationZKK++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationZKK");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationZP44") !== false) {
                                                $fcsNotificationZP44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationZP44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationOKD44") !== false) {
                                                $fcsNotificationOKD44++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationOKD44");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcs_notificationEFDateChange") !== false) {
                                                $fcsNotificationEFDateChange++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcs_notificationEFDateChange");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationCancelFailure") !== false) {
                                                $fcsNotificationCancelFailure++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationCancelFailure");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationLotCancel") !== false) {
                                                $fcsNotificationLotCancel++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationLotCancel");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationOrgChange") !== false) {
                                                $fcsNotificationOrgChange++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationOrgChange");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsNotificationCancel") !== false) {
                                                $fcsNotificationCancel++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsNotificationCancel");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsPurchaseProlongationOK") !== false) {
                                                $fcsPurchaseProlongationOK++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsPurchaseProlongationOK");
                                                continue;
                                            }
                                            if (stripos(pathinfo($dirBuffer . $file,
                                                    PATHINFO_FILENAME),
                                                    "fcsPurchaseProlongationZK") !== false) {
                                                $fcsPurchaseProlongationZK++;
                                                Notification::decomposeFile($dirBuffer, $extractedPath, $file,
                                                    "fcsPurchaseProlongationZK");
                                                continue;
                                            }
                                        } else {
                                            $otherFormatCount++;
                                            unlink($dirBuffer . $file);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $startDate = date("Ymd", strtotime($startDate . "+1day"));
            $startDateClose = date("Ymd", strtotime($startDate . '+1day'));
            $searchString = $startDate . "00_" . $startDateClose . "00";
        }

        //сохранили
        self::actionParseNotificationsXmlToDb();
        //очистили папку распаковки
        self::actionParseNotificationsDeleteExtractedFolder();
    }

    //Скачать все zip за текущий месяц
    public function actionParseNotificationsAsZip()
    {
        $start = microtime(true);
        $pathForSavingZipFiles = Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_ZIP_PATH;
        Notification::downloadFromFtpForAllRegions($pathForSavingZipFiles);
        print_r("\n\nScript as-zip execution time: " . round((microtime(true) - $start), 4) . " second(s).\n");
    }

    //Распаковать все архивы из LOCAL_ZIP_PATH в LOCAL_EXTRACTED_PATH
    public function actionParseNotificationsUnzip()
    {
        $start = microtime(true);
        Notification::unzip(
            Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_ZIP_PATH,
            Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_EXTRACTED_PATH,
            Notification::LOCAL_EXTRACTED_PATH_BUFFER);
        print_r("\n\nScript unzip execution time: " . round((microtime(true) - $start), 4) . " second(s).\n");
    }

    //Сохранить xml всех подпапок LOCAL_EXTRACTED_PATH в DB
    public function actionParseNotificationsXmlToDb()
    {
        $start = microtime(true);
        Notification::saveXmlToDb();
        print_r("\n\nScript xml-to-db execution time: " . round((microtime(true) - $start), 4) . " second(s).\n");
    }

    //Кол-во файлов во вложенных папках LOCAL_EXTRACTED_PATH
    public function actionParseNotificationsFolderCount()
    {
        Notification::totalXmlCount();
    }

    public function actionParseNotificationsClearDb()
    {
        Notification::clearDb();
    }

    public function actionParseNotificationsDeleteExtractedFolder()
    {
        $start = microtime(true);
        exec('rm -rf ' . escapeshellarg(
                Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_EXTRACTED_PATH));
        print_r("\n\nScript delete-extracted-folder execution time: " . round((microtime(true) - $start),
                4) . " second(s).\n");

    }

    public function actionParseNotificationsDeleteZipFolder()
    {
        $start = microtime(true);
        exec('rm -rf ' . escapeshellarg(
                Notification::LOCAL_NOTIFICATION_PATH . Notification::LOCAL_ZIP_PATH));
        print_r("\n\nScript execution time: " . round((microtime(true) - $start), 4) . " second(s).\n");

    }

    public function actionParseGuarantees()
    {
        $start = microtime(true);
        Guarantee::downloadGuarantees();
        print_r("\n\nScript parse guarantees execution time: " . round((microtime(true) - $start),
                4) . " second(s).\n");

    }

    public function actionParseGuaranteesUnzip()
    {
        $start = microtime(true);
        Guarantee::unzip(
            Guarantee::LOCAL_GUARANTEE_PATH . Guarantee::LOCAL_ZIP_PATH,
            Guarantee::LOCAL_GUARANTEE_PATH . Guarantee::LOCAL_EXTRACTED_PATH,
            Guarantee::LOCAL_EXTRACTED_PATH_BUFFER);
        print_r("\n\nScript unzip execution time: " . round((microtime(true) - $start), 4) . " second(s).\n");
    }

    //Сохранить xml всех подпапок LOCAL_EXTRACTED_PATH в DB
    public function actionParseGuaranteesXmlToDb()
    {
        $start = microtime(true);
        Guarantee::saveXmlToDb();
        print_r("\n\nScript xml-to-db execution time: " . round((microtime(true) - $start), 4) . " second(s).\n");
    }

    public function actionParseGuaranteesClearDb()
    {
        Guarantee::clearDb();
    }
}