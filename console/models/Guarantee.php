<?php

namespace console\models;

use common\models\zakupki\guarantee\Bank;
use common\models\zakupki\guarantee\BankGuarantee;
use common\models\zakupki\guarantee\BankGuaranteeInvalid;
use common\models\zakupki\guarantee\BankGuaranteeRefusal;
use common\models\zakupki\guarantee\BankGuaranteeReturn;
use common\models\zakupki\guarantee\BankGuaranteeTermination;
use common\models\zakupki\guarantee\ContactInfo;
use common\models\zakupki\guarantee\ContractExecutionEnsure;
use common\models\zakupki\guarantee\Country;
use common\models\zakupki\guarantee\Currency;
use common\models\zakupki\guarantee\Customer;
use common\models\zakupki\guarantee\GuaranteeAdditionalInfo;
use common\models\zakupki\guarantee\GuaranteeReturn;
use common\models\zakupki\guarantee\GuaranteeTermination;
use common\models\zakupki\guarantee\IndividualPersonForeignState;
use common\models\zakupki\guarantee\IndividualPersonRf;
use common\models\zakupki\guarantee\LegalEntityForeignState;
use common\models\zakupki\guarantee\LegalEntityRf;
use common\models\zakupki\guarantee\LegalForm;
use common\models\zakupki\guarantee\Okato;
use common\models\zakupki\guarantee\Oktmo;
use common\models\zakupki\guarantee\PlaceOfStayInRf;
use common\models\zakupki\guarantee\Placer;
use common\models\zakupki\guarantee\PlacingOrg;
use common\models\zakupki\guarantee\PurchaseCode;
use common\models\zakupki\guarantee\PurchaseRequestEnsure;
use common\models\zakupki\guarantee\query\RegisterInRfTaxBodiesQuery;
use common\models\zakupki\guarantee\RefusalInfo;
use common\models\zakupki\guarantee\RefusalReason;
use common\models\zakupki\guarantee\RegisterInRfTaxBodies;
use common\models\zakupki\guarantee\SubjectRf;
use common\models\zakupki\guarantee\Supplier;
use Yii;

class Guarantee
{
    const ZIP_EXTRACTED_COUNT = -1;

    const FTP_USER = "free";
    const FTP_PASSWORD = "free";
    const FTP_HOST = "ftp.zakupki.gov.ru";
    const LOCAL_GUARANTEE_PATH = "input/guarantee/";
    const LOCAL_ZIP_PATH = "zip/";
    const LOCAL_EXTRACTED_PATH = "extracted/";
    const LOCAL_EXTRACTED_PATH_BUFFER = "buffer/";
    const EMPTY_FILE_SIZE = 25;

    public static function deleteDir()
    {
        //$dirPath = self::LOCAL_ZIP_PATH . self::LOCAL_EXTRACTED_PATH_NORM;
    }

    public static function parseGuaranteeInfo($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);

        if ($xml) {
            $main = [
                'id' => '',
                'filename' => basename($file),
                'bgId' => '',
                'externalId' => '',
                'docNumber' => '',
                'extendedDocNumber' => '',
                'docPublishDate' => '',
                'href' => '',

            ];

            if (isset($xml->bankGuarantee->id)) {
                $main['bgId'] = (string)$xml->bankGuarantee->id;
            }
            if (isset($xml->bankGuarantee->externalId)) {
                $main['externalId'] = (string)$xml->bankGuarantee->externalId;
            }
            if (isset($xml->bankGuarantee->docNumber)) {
                $main['docNumber'] = (string)$xml->bankGuarantee->docNumber;
            }
            if (isset($xml->bankGuarantee->extendedDocNumber)) {
                $main['extendedDocNumber'] = (string)$xml->bankGuarantee->extendedDocNumber;
            }
            if (isset($xml->bankGuarantee->docPublishDate)) {
                $main['docPublishDate'] = (string)$xml->bankGuarantee->docPublishDate;
            }
            if (isset($xml->bankGuarantee->href)) {
                $main['href'] = (string)$xml->bankGuarantee->href;
            }

            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO main (
                          filename,
                          bg_id,
                          external_id,
                          doc_number,
                          extended_doc_number,
                          doc_publish_date,
                          href
                      ) 
                      VALUES(
                          :filename,
                          :bgId,
                          :externalId,
                          :docNumber,
                          :extendedDocNumber,
                          :docPublishDate,
                          :href
                      )                      
					 ON CONFLICT (bg_id) DO NOTHING 
                      ";

            $returningState = Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':filename' => $main['filename'],
                    ':bgId' => $main['bgId'],
                    ':externalId' => $main['externalId'],
                    ':docNumber' => $main['docNumber'],
                    ':extendedDocNumber' => $main['extendedDocNumber'],
                    ':docPublishDate' => $main['docPublishDate'],
                    ':href' => $main['href'],
                ])
                ->execute();

            $main['id'] = Yii::$app->db_bank_guarantee->getLastInsertID('main_id_seq');

            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
            } else {
                echo "Record with bg_id = " . $main['bgId'] . " exist." . "\n";
                return;
            }
        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }

        //-----------print_form
        $printForm = [
            'main_id' => $main['id'],
            'url' => '',
        ];

        if (isset($xml->bankGuarantee->printForm->url)) {
            $printForm['url'] = (string)$xml->bankGuarantee->printForm->url;
        }
        //*-----------print_form
        //-----------print_form
        $printFormCondition =
            $printForm['url'] !== '';

        if ($printFormCondition) {
            $sql = "
                      INSERT INTO print_form (
                          main_id,
                          url
                      ) 
                      VALUES(
                          :mainId,
                          :url
                      )";

            Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':mainId' => $printForm['main_id'],
                    ':url' => $printForm['url'],
                ])
                ->execute();
        }
        //*-----------print_form

        $legalFormBank = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuarantee->bank->legalForm->code)) {
            $legalFormBank['code'] = (string)$xml->bankGuarantee->bank->legalForm->code;
        }
        if (isset($xml->bankGuarantee->bank->legalForm->singularName)) {
            $legalFormBank['singular_name'] = (string)$xml->bankGuarantee->bank->legalForm->singularName;
        }

        $legalFormBank['id'] = self::getLegalFormId($legalFormBank);

        $legalFormPlacingOrg = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuarantee->placingOrg->legalForm->code)) {
            $legalFormPlacingOrg['code'] = (string)$xml->bankGuarantee->placingOrg->legalForm->code;
        }
        if (isset($xml->bankGuarantee->placingOrg->legalForm->singularName)) {
            $legalFormPlacingOrg['singular_name'] = (string)$xml->bankGuarantee->placingOrg->legalForm->singularName;
        }

        $legalFormPlacingOrg['id'] = self::getLegalFormId($legalFormPlacingOrg);

        $legalFormSupplier = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuarantee->supplier->legalForm->code)) {
            $legalFormSupplier['code'] = (string)$xml->bankGuarantee->supplier->legalForm->code;
        }
        if (isset($xml->bankGuarantee->supplier->legalForm->singularName)) {
            $legalFormSupplier['singular_name'] = (string)$xml->bankGuarantee->supplier->legalForm->singularName;
        }

        $legalFormSupplier['id'] = self::getLegalFormId($legalFormSupplier);

        $legalFormLegalEntityRF = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->legalForm->code)) {
            $legalFormLegalEntityRF['code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->legalForm->code;
        }
        if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->legalForm->singularName)) {
            $legalFormLegalEntityRF['singular_name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->legalForm->singularName;
        }

        $legalFormLegalEntityRF['id'] = self::getLegalFormId($legalFormLegalEntityRF);

        $legalFormCustomer = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuarantee->guarantee->customer->legalForm->code)) {
            $legalFormCustomer['code'] = (string)$xml->bankGuarantee->guarantee->customer->legalForm->code;
        }
        if (isset($xml->bankGuarantee->guarantee->customer->legalForm->singularName)) {
            $legalFormCustomer['singular_name'] = (string)$xml->bankGuarantee->guarantee->customer->legalForm->singularName;
        }

        $legalFormCustomer['id'] = self::getLegalFormId($legalFormCustomer);

        //var_dump("legalFormBank " . $legalFormBank['id'] . " code = " . $legalFormBank['code']);
        //var_dump("legalFormPlacingOrg " . $legalFormPlacingOrg['id'] . " code = " . $legalFormPlacingOrg['code']);
        //var_dump("legalFormSupplier " . $legalFormSupplier['id'] . " code = " . $legalFormSupplier['code']);
        //var_dump("legalFormLegalEntityRF " . $legalFormLegalEntityRF['id'] . " code = " . $legalFormLegalEntityRF['code']);
        //var_dump("legalFormCustomer " . $legalFormCustomer['id'] . " code = " . $legalFormCustomer['code']);

        if (isset($xml->bankGuarantee->supplier->contactInfo)) {
            $contactInfoSupplier = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuarantee->supplier->contactInfo->lastName)) {
                $contactInfoSupplier['last_name'] = (string)$xml->bankGuarantee->supplier->contactInfo->lastName;
            }
            if (isset($xml->bankGuarantee->supplier->contactInfo->firstName)) {
                $contactInfoSupplier['first_name'] = (string)$xml->bankGuarantee->supplier->contactInfo->firstName;
            }
            if (isset($xml->bankGuarantee->supplier->contactInfo->middleName)) {
                $contactInfoSupplier['middle_name'] = (string)$xml->bankGuarantee->supplier->contactInfo->middleName;
            }

            $contactInfoSupplier['id'] = self::getContactInfoId($contactInfoSupplier);
        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF)) {
            $contactInfoIPRF = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->lastName)) {
                $contactInfoIPRF['last_name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->lastName;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->firstName)) {
                $contactInfoIPRF['first_name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->firstName;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->middleName)) {
                $contactInfoIPRF['middle_name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->middleName;
            }

            $contactInfoIPRF['id'] = self::getContactInfoId($contactInfoIPRF);
        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState)) {
            $contactInfoIPFS = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->lastName)) {
                $contactInfoIPFS['last_name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->lastName;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->firstName)) {
                $contactInfoIPFS['first_name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->firstName;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->middleName)) {
                $contactInfoIPFS['middle_name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->middleName;

            }

            $contactInfoIPFS['id'] = self::getContactInfoId($contactInfoIPFS);
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF)) {
            $subjectRFPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code)) {
                $subjectRFPOSIRF['code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name)) {
                $subjectRFPOSIRF['name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name;
            }

            $subjectRFPOSIRF['id'] = self::getSubjectRFId($subjectRFPOSIRF);

        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->subjectRF)) {
            $subjectRFIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->subjectRF->code)) {
                $subjectRFIPRF['code'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->subjectRF->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->subjectRF->name)) {
                $subjectRFIPRF['name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->subjectRF->name;
            }

            $subjectRFIPRF['id'] = self::getSubjectRFId($subjectRFIPRF);

        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->subjectRF)) {
            $subjectRFLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->subjectRF->code)) {
                $subjectRFLERF['code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->subjectRF->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->subjectRF->name)) {
                $subjectRFLERF['name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->subjectRF->name;
            }

            $subjectRFLERF['id'] = self::getSubjectRFId($subjectRFLERF);
        }

        if (isset($xml->bankGuarantee->bank->subjectRF)) {
            $subjectRFBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->bank->subjectRF->code)) {
                $subjectRFBank['code'] = (string)$xml->bankGuarantee->bank->subjectRF->code;
            }
            if (isset($xml->bankGuarantee->bank->subjectRF->name)) {
                $subjectRFBank['name'] = (string)$xml->bankGuarantee->bank->subjectRF->name;
            }

            $subjectRFBank['id'] = self::getSubjectRFId($subjectRFBank);
        }

        if (isset($xml->bankGuarantee->guarantee->customer->subjectRF)) {
            $subjectRFCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->guarantee->customer->subjectRF->code)) {
                $subjectRFCustomer['code'] = (string)$xml->bankGuarantee->guarantee->customer->subjectRF->code;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->subjectRF->name)) {
                $subjectRFCustomer['name'] = (string)$xml->bankGuarantee->guarantee->customer->subjectRF->name;
            }

            $subjectRFCustomer['id'] = self::getSubjectRFId($subjectRFCustomer);
        }

        if (isset($xml->bankGuarantee->placingOrg->subjectRF)) {
            $subjectRFPlacingOrg = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->placingOrg->subjectRF->code)) {
                $subjectRFPlacingOrg['code'] = (string)$xml->bankGuarantee->placingOrg->subjectRF->code;
            }
            if (isset($xml->bankGuarantee->placingOrg->subjectRF->name)) {
                $subjectRFPlacingOrg['name'] = (string)$xml->bankGuarantee->placingOrg->subjectRF->name;
            }

            $subjectRFPlacingOrg['id'] = self::getSubjectRFId($subjectRFPlacingOrg);
        }

        if (isset($xml->bankGuarantee->placingOrg->OKTMO)) {
            $oktmoPlacingOrg = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->placingOrg->OKTMO->code)) {
                $oktmoPlacingOrg['code'] = (string)$xml->bankGuarantee->placingOrg->OKTMO->code;
            }
            if (isset($xml->bankGuarantee->placingOrg->OKTMO->name)) {
                $oktmoPlacingOrg['name'] = (string)$xml->bankGuarantee->placingOrg->OKTMO->name;
            }

            $oktmoPlacingOrg['id'] = self::getOktmoId($oktmoPlacingOrg);
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->OKTMO)) {
            $oktmoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->OKTMO->code)) {
                $oktmoLERF['code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->OKTMO->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->OKTMO->name)) {
                $oktmoLERF['name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->OKTMO->name;
            }

            $oktmoLERF['id'] = self::getOktmoId($oktmoLERF);
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO)) {
            $oktmoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code)) {
                $oktmoPOSIRF['code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name)) {
                $oktmoPOSIRF['name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name;
            }

            $oktmoPOSIRF['id'] = self::getOktmoId($oktmoPOSIRF);
        }

        if (isset($xml->bankGuarantee->guarantee->customer->OKTMO)) {
            $oktmoCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->guarantee->customer->OKTMO->code)) {
                $oktmoCustomer['code'] = (string)$xml->bankGuarantee->guarantee->customer->OKTMO->code;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->OKTMO->name)) {
                $oktmoCustomer['name'] = (string)$xml->bankGuarantee->guarantee->customer->OKTMO->name;
            }

            $oktmoCustomer['id'] = self::getOktmoId($oktmoCustomer);
        }

        if (isset($xml->bankGuarantee->bank->OKTMO)) {
            $oktmoBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->bank->OKTMO->code)) {
                $oktmoBank['code'] = (string)$xml->bankGuarantee->bank->OKTMO->code;
            }
            if (isset($xml->bankGuarantee->bank->OKTMO->name)) {
                $oktmoBank['name'] = (string)$xml->bankGuarantee->bank->OKTMO->name;
            }

            $oktmoBank['id'] = self::getOktmoId($oktmoBank);
        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->OKTMO)) {
            $oktmoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->OKTMO->code)) {
                $oktmoIPRF['code'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->OKTMO->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->OKTMO->name)) {
                $oktmoIPRF['name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->OKTMO->name;
            }

            $oktmoIPRF['id'] = self::getOktmoId($oktmoIPRF);
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO)) {
            $okatoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code)) {
                $okatoPOSIRF['code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name)) {
                $okatoPOSIRF['name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name;
            }

            $okatoPOSIRF['id'] = self::getOkatoId($okatoPOSIRF);
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->OKATO)) {
            $okatoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->OKATO->code)) {
                $okatoLERF['code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->OKATO->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->OKATO->name)) {
                $okatoLERF['name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->OKATO->name;
            }

            $okatoLERF['id'] = self::getOkatoId($okatoLERF);
        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->OKATO)) {
            $okatoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->OKATO->code)) {
                $okatoIPRF['code'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->OKATO->code;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->OKATO->name)) {
                $okatoIPRF['name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->OKATO->name;
            }

            $okatoIPRF['id'] = self::getOkatoId($okatoIPRF);
        }

        if (isset($xml->bankGuarantee->guarantee->currency)) {
            $currencyGuarantee = [
                'id' => '',
                'code' => '',
                'digital_code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuarantee->guarantee->currency->code)) {
                $currencyGuarantee['code'] = (string)$xml->bankGuarantee->guarantee->currency->code;
            }
            if (isset($xml->bankGuarantee->guarantee->currency->digitalCode)) {
                $currencyGuarantee['digital_code'] = (string)$xml->bankGuarantee->guarantee->currency->digitalCode;
            }
            if (isset($xml->bankGuarantee->guarantee->currency->name)) {
                $currencyGuarantee['name'] = (string)$xml->bankGuarantee->guarantee->currency->name;
            }

            $currencyGuarantee['id'] = self::getCurrencyId($currencyGuarantee);
        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->country)) {
            $countryIPFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->country->countryCode)) {
                $countryIPFS['country_code'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->country->countryCode;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->country->countryFullName)) {
                $countryIPFS['country_full_name'] = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->country->countryFullName;
            }

            $countryIPFS['id'] = self::getCountryId($countryIPFS);
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->country)) {
            $countryLEFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->country->countryCode)) {
                $countryLEFS['country_code'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->country->countryCode;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->country->countryFullName)) {
                $countryLEFS['country_full_name'] = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->country->countryFullName;
            }

            $countryLEFS['id'] = self::getCountryId($countryLEFS);
        }

        if (isset($xml->bankGuarantee->supplier->country)) {
            $countrySupplier = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuarantee->supplier->country->countryCode)) {
                $countrySupplier['country_code'] = (string)$xml->bankGuarantee->supplier->country->countryCode;
            }
            if (isset($xml->bankGuarantee->supplier->country->countryFullName)) {
                $countrySupplier['country_full_name'] = (string)$xml->bankGuarantee->supplier->country->countryFullName;
            }

            $countrySupplier['id'] = self::getCountryId($countrySupplier);
        }

        if (isset($xml->bankGuarantee->guarantee->customer)) {
            $customer = new Customer();

            if (isset($xml->bankGuarantee->guarantee->customer->regNum)) {
                $customer->reg_num = (string)$xml->bankGuarantee->guarantee->customer->regNum;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->consRegistryNum)) {
                $customer->cons_registry_num = (string)$xml->bankGuarantee->guarantee->customer->consRegistryNum;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->fullName)) {
                $customer->full_name = (string)$xml->bankGuarantee->guarantee->customer->fullName;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->shortName)) {
                $customer->short_name = (string)$xml->bankGuarantee->guarantee->customer->shortName;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->postAddress)) {
                $customer->post_address = (string)$xml->bankGuarantee->guarantee->customer->postAddress;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->factAddress)) {
                $customer->fact_address = (string)$xml->bankGuarantee->guarantee->customer->factAddress;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->INN)) {
                $customer->inn = (string)$xml->bankGuarantee->guarantee->customer->INN;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->KPP)) {
                $customer->kpp = (string)$xml->bankGuarantee->guarantee->customer->KPP;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->location)) {
                $customer->location = (string)$xml->bankGuarantee->guarantee->customer->location;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->registrationDate)) {
                $customer->registration_date = (string)$xml->bankGuarantee->guarantee->customer->registrationDate;
            }
            if (isset($xml->bankGuarantee->guarantee->customer->IKU)) {
                $customer->iku = (string)$xml->bankGuarantee->guarantee->customer->IKU;
            }

            if (isset($legalFormCustomer) && !empty($legalFormCustomer['id'])) {
                $customer->legal_form_id = $legalFormCustomer['id'];
            }
            if (isset($subjectRFCustomer) && !empty($subjectRFCustomer['id'])) {
                $customer->subject_rf_id = $subjectRFCustomer['id'];
            }
            if (isset($oktmoCustomer) && !empty($oktmoCustomer['id'])) {
                $customer->oktmo_id = $oktmoCustomer['id'];
            }

            $customer->save();
            //$customer->id;
        }

        if (isset($xml->bankGuarantee->bank)) {
            $bank = new Bank();

            if (isset($xml->bankGuarantee->bank->regNum)) {
                $bank->reg_num = (string)$xml->bankGuarantee->bank->regNum;
            }
            if (isset($xml->bankGuarantee->bank->consRegistryNum)) {
                $bank->cons_registry_num = (string)$xml->bankGuarantee->bank->consRegistryNum;
            }
            if (isset($xml->bankGuarantee->bank->fullName)) {
                $bank->full_name = (string)$xml->bankGuarantee->bank->fullName;
            }
            if (isset($xml->bankGuarantee->bank->shortName)) {
                $bank->short_name = (string)$xml->bankGuarantee->bank->shortName;
            }
            if (isset($xml->bankGuarantee->bank->postAddress)) {
                $bank->post_address = (string)$xml->bankGuarantee->bank->postAddress;
            }
            if (isset($xml->bankGuarantee->bank->factAddress)) {
                $bank->fact_address = (string)$xml->bankGuarantee->bank->factAddress;
            }
            if (isset($xml->bankGuarantee->bank->INN)) {
                $bank->inn = (string)$xml->bankGuarantee->bank->INN;
            }
            if (isset($xml->bankGuarantee->bank->KPP)) {
                $bank->kpp = (string)$xml->bankGuarantee->bank->KPP;
            }
            if (isset($xml->bankGuarantee->bank->location)) {
                $bank->location = (string)$xml->bankGuarantee->bank->location;
            }
            if (isset($xml->bankGuarantee->bank->registrationDate)) {
                $bank->registration_date = (string)$xml->bankGuarantee->bank->registrationDate;
            }
            if (isset($xml->bankGuarantee->bank->IKU)) {
                $bank->iku = (string)$xml->bankGuarantee->bank->IKU;
            }

            if (isset($main) && !empty($main['id'])) {
                $bank->main_id = $main['id'];
            }
            if (isset($legalFormBank) && !empty($legalFormBank['id'])) {
                $bank->legal_form_id = $legalFormBank['id'];
            }
            if (isset($subjectRFBank) && !empty($subjectRFBank['id'])) {
                $bank->subject_rf_id = $subjectRFBank['id'];
            }
            if (isset($oktmoBank) && !empty($oktmoBank['id'])) {
                $bank->oktmo_id = $oktmoBank['id'];
            }

            $bank->save();
        }

        if ($xml->bankGuarantee->placingOrg) {
            $placingOrg = new PlacingOrg();

            if (isset($xml->bankGuarantee->placingOrg->regNum)) {
                $placingOrg->reg_num = (string)$xml->bankGuarantee->placingOrg->regNum;
            }
            if (isset($xml->bankGuarantee->placingOrg->consRegistryNum)) {
                $placingOrg->cons_registry_num = (string)$xml->bankGuarantee->placingOrg->consRegistryNum;
            }
            if (isset($xml->bankGuarantee->placingOrg->fullName)) {
                $placingOrg->full_name = (string)$xml->bankGuarantee->placingOrg->fullName;
            }
            if (isset($xml->bankGuarantee->placingOrg->shortName)) {
                $placingOrg->short_name = (string)$xml->bankGuarantee->placingOrg->shortName;
            }
            if (isset($xml->bankGuarantee->placingOrg->postAddress)) {
                $placingOrg->post_address = (string)$xml->bankGuarantee->placingOrg->postAddress;
            }
            if (isset($xml->bankGuarantee->placingOrg->factAddress)) {
                $placingOrg->fact_address = (string)$xml->bankGuarantee->placingOrg->factAddress;
            }
            if (isset($xml->bankGuarantee->placingOrg->INN)) {
                $placingOrg->inn = (string)$xml->bankGuarantee->placingOrg->INN;
            }
            if (isset($xml->bankGuarantee->placingOrg->KPP)) {
                $placingOrg->kpp = (string)$xml->bankGuarantee->placingOrg->KPP;
            }
            if (isset($xml->bankGuarantee->placingOrg->location)) {
                $placingOrg->location = (string)$xml->bankGuarantee->placingOrg->location;
            }
            if (isset($xml->bankGuarantee->placingOrg->registrationDate)) {
                $placingOrg->registration_date = (string)$xml->bankGuarantee->placingOrg->registrationDate;
            }
            if (isset($xml->bankGuarantee->placingOrg->IKU)) {
                $placingOrg->iku = (string)$xml->bankGuarantee->placingOrg->IKU;
            }

            if (isset($legalFormPlacingOrg) && !empty($legalFormPlacingOrg['id'])) {
                $placingOrg->legal_form_id = $legalFormPlacingOrg['id'];
            }
            if (isset($subjectRFPlacingOrg) && !empty($legalFormPlacingOrg['id'])) {
                $placingOrg->subject_rf_id = $subjectRFPlacingOrg['id'];
            }
            if (isset($oktmoPlacingOrg) && !empty($legalFormPlacingOrg['id'])) {
                $placingOrg->oktmo_id = $oktmoPlacingOrg['id'];
            }

            $placingOrg->save();
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF)) {
            $legalEntityRF = new LegalEntityRf();

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->fullName)) {
                $legalEntityRF->full_name = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->fullName;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->shortName)) {
                $legalEntityRF->short_name = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->shortName;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->INN)) {
                $legalEntityRF->inn = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->INN;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->KPP)) {
                $legalEntityRF->kpp = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->KPP;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->OGRN)) {
                $legalEntityRF->ogrn = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->OGRN;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->registrationDate)) {
                $legalEntityRF->registration_date = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->registrationDate;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityRF->address)) {
                $legalEntityRF->address = (string)$xml->bankGuarantee->supplierInfo->legalEntityRF->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityRF->main_id = $main['id'];
            }
            if (isset($legalFormLegalEntityRF) && !empty($legalFormLegalEntityRF['id'])) {
                $legalEntityRF->legal_form_id = $legalFormLegalEntityRF['id'];
            }
            if (isset($subjectRFLERF) && !empty($subjectRFLERF['id'])) {
                $legalEntityRF->subject_rf_id = $subjectRFLERF['id'];
            }
            if (isset($okatoLERF) && !empty($okatoLERF['id'])) {
                $legalEntityRF->okato_id = $okatoLERF['id'];
            }
            if (isset($oktmoLERF) && !empty($oktmoLERF['id'])) {
                $legalEntityRF->oktmo_id = $oktmoLERF['id'];
            }

            $legalEntityRF->save();
        }

        if (isset($xml->bankGuarantee->guarantee->contractExecutionEnsure)) {
            $contractExecutionEnsure = new ContractExecutionEnsure();

            if (isset($xml->bankGuarantee->guarantee->contractExecutionEnsure->regNum)) {
                $contractExecutionEnsure->reg_num = (string)$xml->bankGuarantee->guarantee->contractExecutionEnsure->regNum;
            }
            if (isset($xml->bankGuarantee->guarantee->contractExecutionEnsure->purchase->purchaseNumber)) {
                $contractExecutionEnsure->purchase_purchase_number = (string)$xml->bankGuarantee->guarantee->contractExecutionEnsure->purchase->purchaseNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->contractExecutionEnsure->purchase->notificationNumber)) {
                $contractExecutionEnsure->purchase_notification_number = (string)$xml->bankGuarantee->guarantee->contractExecutionEnsure->purchase->notificationNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->contractExecutionEnsure->purchase->lotNumber)) {
                $contractExecutionEnsure->purchase_lot_number = (string)$xml->bankGuarantee->guarantee->contractExecutionEnsure->purchase->lotNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->contractExecutionEnsure->mLots)) {
                $contractExecutionEnsure->m_lots = (string)$xml->bankGuarantee->guarantee->contractExecutionEnsure->mLots;
            }
            if (isset($xml->bankGuarantee->guarantee->contractExecutionEnsure->singleSupplier)) {
                $contractExecutionEnsure->single_supplier = (string)$xml->bankGuarantee->guarantee->contractExecutionEnsure->singleSupplier;
            }

            $contractExecutionEnsure->save();
        }

        if (isset($xml->bankGuarantee->guarantee->purchaseRequestEnsure->purchaseNumber)) {
            $purchaseRequestEnsure = new PurchaseRequestEnsure();

            if (isset($xml->bankGuarantee->guarantee->purchaseRequestEnsure->purchaseNumber)) {
                $purchaseRequestEnsure->purchase_number = (string)$xml->bankGuarantee->guarantee->purchaseRequestEnsure->purchaseNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->purchaseRequestEnsure->notificationNumber)) {
                $purchaseRequestEnsure->notification_number = (string)$xml->bankGuarantee->guarantee->purchaseRequestEnsure->notificationNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->purchaseRequestEnsure->lotNumber)) {
                $purchaseRequestEnsure->lot_number = (string)$xml->bankGuarantee->guarantee->purchaseRequestEnsure->lotNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->purchaseRequestEnsure->mLots)) {
                $purchaseRequestEnsure->m_lots = (string)$xml->bankGuarantee->guarantee->purchaseRequestEnsure->mLots;
            }

            $purchaseRequestEnsure->save();
        }

        if (isset($xml->bankGuarantee->guarantee)) {
            $guarantee = new \common\models\zakupki\guarantee\Guarantee();

            if (isset($xml->bankGuarantee->guarantee->majorRepairEnsure->EFNumber)) {
                $guarantee->major_repair_ensure_ef_number = (string)$xml->bankGuarantee->guarantee->majorRepairEnsure->EFNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->guaranteeDate)) {
                $guarantee->guarantee_date = (string)$xml->bankGuarantee->guarantee->guaranteeDate;
            }
            if (isset($xml->bankGuarantee->guarantee->guaranteeNumber)) {
                $guarantee->guarantee_number = (string)$xml->bankGuarantee->guarantee->guaranteeNumber;
            }
            if (isset($xml->bankGuarantee->guarantee->guaranteeAmount)) {
                $guarantee->guarantee_amount = (string)$xml->bankGuarantee->guarantee->guaranteeAmount;
            }
            if (isset($xml->bankGuarantee->guarantee->expireDate)) {
                $guarantee->expire_date = (string)$xml->bankGuarantee->guarantee->expireDate;
            }
            if (isset($xml->bankGuarantee->guarantee->entryForceDate)) {
                $guarantee->entry_force_date = (string)$xml->bankGuarantee->guarantee->entryForceDate;
            }
            if (isset($xml->bankGuarantee->guarantee->procedure)) {
                $guarantee->guarantee_procedure = (string)$xml->bankGuarantee->guarantee->procedure;
            }
            if (isset($xml->bankGuarantee->guarantee->guaranteeAmountRUR)) {
                $guarantee->guarantee_amount_rur = (string)$xml->bankGuarantee->guarantee->guaranteeAmountRUR;
            }
            if (isset($xml->bankGuarantee->guarantee->currencyRate)) {
                $guarantee->currency_rate = (string)$xml->bankGuarantee->guarantee->currencyRate;
            }

            if (isset($main) && !empty($main['id'])) {
                $guarantee->main_id = $main['id'];
            }
            if (isset($purchaseRequestEnsure)) {
                $guarantee->purchase_request_ensure_id = $purchaseRequestEnsure->id;
            }

            if (isset($contractExecutionEnsure)) {
                $guarantee->contract_execution_ensure_id = $contractExecutionEnsure->id;
            }

            if (isset($customer)) {
                $guarantee->customer_id = $customer->id;
            }

            if (isset($currencyGuarantee) && !empty($currencyGuarantee['id'])) {
                $guarantee->currency_id = $currencyGuarantee['id'];
            }

            $guarantee->save();
        }

        if (isset($xml->bankGuarantee->guarantee->purchaseCodes)) {

            foreach ($xml->bankGuarantee->guarantee->purchaseCodes as $pC) {
                $purchaseCode = new PurchaseCode();

                if (isset($guarantee)) {
                    $purchaseCode->guarantee_id = $guarantee->id;
                }
                if (isset($pC->purchaseCode)) {
                    $purchaseCode->purchase_code = (string)$pC->purchaseCode;
                }
                $purchaseCode->save();
            }
        }

        if (isset($xml->bankGuarantee->supplier)) {
            $supplier = new Supplier();

            if (isset($xml->bankGuarantee->supplier->participantType)) {
                $supplier->participant_type = (string)$xml->bankGuarantee->supplier->participantType;
            }
            if (isset($xml->bankGuarantee->supplier->inn)) {
                $supplier->inn = (string)$xml->bankGuarantee->supplier->inn;
            }
            if (isset($xml->bankGuarantee->supplier->kpp)) {
                $supplier->kpp = (string)$xml->bankGuarantee->supplier->kpp;
            }
            if (isset($xml->bankGuarantee->supplier->ogrn)) {
                $supplier->ogrn = (string)$xml->bankGuarantee->supplier->ogrn;
            }
            if (isset($xml->bankGuarantee->supplier->idNumber)) {
                $supplier->id_number = (string)$xml->bankGuarantee->supplier->idNumber;
            }
            if (isset($xml->bankGuarantee->supplier->idNumberExtension)) {
                $supplier->id_number_extension = (string)$xml->bankGuarantee->supplier->idNumberExtension;
            }
            if (isset($xml->bankGuarantee->supplier->organizationName)) {
                $supplier->organization_name = (string)$xml->bankGuarantee->supplier->organizationName;
            }
            if (isset($xml->bankGuarantee->supplier->firmName)) {
                $supplier->firm_name = (string)$xml->bankGuarantee->supplier->firmName;
            }
            if (isset($xml->bankGuarantee->supplier->factualAddress)) {
                $supplier->fact_address = (string)$xml->bankGuarantee->supplier->factualAddress;
            }
            if (isset($xml->bankGuarantee->supplier->postAddress)) {
                $supplier->post_address = (string)$xml->bankGuarantee->supplier->postAddress;
            }
            if (isset($xml->bankGuarantee->supplier->contactEMail)) {
                $supplier->contact_email = (string)$xml->bankGuarantee->supplier->contactEMail;
            }
            if (isset($xml->bankGuarantee->supplier->contactPhone)) {
                $supplier->contact_phone = (string)$xml->bankGuarantee->supplier->contactPhone;
            }
            if (isset($xml->bankGuarantee->supplier->contactFax)) {
                $supplier->contact_fax = (string)$xml->bankGuarantee->supplier->contactFax;
            }
            if (isset($xml->bankGuarantee->supplier->additionalInfo)) {
                $supplier->additional_info = (string)$xml->bankGuarantee->supplier->additionalInfo;
            }
            if (isset($xml->bankGuarantee->supplier->status)) {
                $supplier->status = (string)$xml->bankGuarantee->supplier->status;
            }

            if (isset($legalFormSupplier) && !empty($legalFormSupplier['id'])) {
                $supplier->legal_form_id = $legalFormSupplier['id'];
            }
            if (isset($countrySupplier) && !empty($countrySupplier['id'])) {
                $supplier->country_id = $countrySupplier['id'];
            }
            if (isset($contactInfoSupplier) && !empty($contactInfoSupplier['id'])) {
                $supplier->contact_info_id = $contactInfoSupplier['id'];
            }
            if (isset($main) && !empty($main['id'])) {
                $supplier->main_id = $main['id'];
            }

            $supplier->save();
        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF)) {
            $individualPersonRf = new IndividualPersonRf();

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->INN)) {
                $individualPersonRf->inn = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->INN;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->OGRNIP)) {
                $individualPersonRf->ogrnip = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->OGRNIP;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->registrationDate)) {
                $individualPersonRf->registration_date = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->registrationDate;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->address)) {
                $individualPersonRf->address = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->address;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF->isIP)) {
                $individualPersonRf->is_ip = (string)$xml->bankGuarantee->supplierInfo->individualPersonRF->isIP;
            }

            if (isset($main) && !empty($main['id'])) {
                $individualPersonRf->main_id = $main['id'];
            }
            if (isset($contactInfoIPRF) && !empty($contactInfoIPRF['id'])) {
                $individualPersonRf->contact_info_id = $contactInfoIPRF['id'];
            }
            if (isset($subjectRFIPRF) && !empty($subjectRFIPRF['id'])) {
                $individualPersonRf->subject_rf_id = $subjectRFIPRF['id'];
            }
            if (isset($okatoIPRF) && !empty($okatoIPRF['id'])) {
                $individualPersonRf->okato_id = $okatoIPRF['id'];
            }
            if (isset($oktmoIPRF) && !empty($oktmoIPRF['id'])) {
                $individualPersonRf->oktmo_id = $oktmoIPRF['id'];
            }

            $individualPersonRf->save();
        }
//----------------------------------------------------------------------------------------------------------------------
        //if (isset($xml->bankGuarantee->supplierInfo->individualPersonRF)) {
        $bankGuarantee = new BankGuarantee();

        if (isset($xml->bankGuarantee->bank->regNum)) {
            $bankGuarantee->reg_number = (string)$xml->bankGuarantee->bank->regNum;
        }
        if (isset($xml->bankGuarantee->creditOrgNumber)) {
            $bankGuarantee->credit_org_number = (string)$xml->bankGuarantee->creditOrgNumber;
        }
        if (isset($xml->bankGuarantee->extendedDocNumber)) {
            $bankGuarantee->extended_doc_number = (string)$xml->bankGuarantee->extendedDocNumber;
        }
        if (isset($xml->bankGuarantee->versionNumber)) {
            $bankGuarantee->version_number = (string)$xml->bankGuarantee->versionNumber;
        }
        if (isset($xml->bankGuarantee->guarantee->purchaseCode)) {
            $bankGuarantee->guarantee_purchase_code = (string)$xml->bankGuarantee->guarantee->purchaseCode;
        }
        if (isset($main) && !empty($main['id'])) {
            $bankGuarantee->main_id = $main['id'];
        }
        if (isset($placingOrg)) {
            $bankGuarantee->placing_org_id = $placingOrg->id;
        }

        $bankGuarantee->save();
        //die;
        //}

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->registerInRFTaxBodies)) {
            $registerInRfTaxBodies = new RegisterInRfTaxBodies();

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN)) {
                $registerInRfTaxBodies->inn = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP)) {
                $registerInRfTaxBodies->kpp = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate)) {
                $registerInRfTaxBodies->registration_date = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate;
            }

            $registerInRfTaxBodies->save();
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState)) {
            $legalEntityForeignState = new LegalEntityForeignState();

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->fullName)) {
                $legalEntityForeignState->full_name = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->fullName;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->fullNameLat)) {
                $legalEntityForeignState->full_name_lat = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->fullNameLat;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->taxPayerCode)) {
                $legalEntityForeignState->tax_payer_code = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->address)) {
                $legalEntityForeignState->address = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityForeignState->main_id = $main['id'];
            }
            if (isset($countryLEFS) && !empty($countryLEFS['id'])) {
                $legalEntityForeignState->country_id = $countryLEFS['id'];
            }
            if (isset($registerInRfTaxBodies)) {
                $legalEntityForeignState->register_in_rf_tax_bodies_id = $registerInRfTaxBodies->id;
            }

            $legalEntityForeignState->save();
        }

        if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF)) {

            $placeOfStayInRf = new PlaceOfStayInRf();

            if (isset($xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->address)) {
                $placeOfStayInRf->address = (string)$xml->bankGuarantee->supplierInfo->legalEntityForeignState->placeOfStayInRF->address;
            }

            if (isset($legalEntityForeignState)) {
                $placeOfStayInRf->legal_entity_foreign_state_id = $legalEntityForeignState->id;
            }
            if (isset($subjectRFPOSIRF) && !empty($subjectRFPOSIRF['id'])) {
                $placeOfStayInRf->subject_rf_id = $subjectRFPOSIRF['id'];
            }
            if (isset($okatoPOSIRF) && !empty($okatoPOSIRF['id'])) {
                $placeOfStayInRf->okato_id = $okatoPOSIRF['id'];
            }
            if (isset($oktmoPOSIRF) && !empty($oktmoPOSIRF['id'])) {
                $placeOfStayInRf->oktmo_id = $oktmoPOSIRF['id'];
            }

            $placeOfStayInRf->save();
        }

        if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState)) {
            $individualPersonForeignState = new IndividualPersonForeignState();

            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->lastNameLat)) {
                $individualPersonForeignState->last_name_lat = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->lastNameLat;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->firstNameLat)) {
                $individualPersonForeignState->first_name_lat = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->firstNameLat;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->middleNameLat)) {
                $individualPersonForeignState->middle_name_lat = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->middleNameLat;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->INN)) {
                $individualPersonForeignState->inn = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->INN;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->taxPayerCode)) {
                $individualPersonForeignState->tax_payer_code = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuarantee->supplierInfo->individualPersonForeignState->address)) {
                $individualPersonForeignState->address = (string)$xml->bankGuarantee->supplierInfo->individualPersonForeignState->address;
            }

            if (isset($countryIPFS) && !empty($countryIPFS['id'])) {
                $individualPersonForeignState->country_id = $countryIPFS['id'];
            }
            if (isset($contactInfoIPFS) && !empty($contactInfoIPFS['id'])) {
                $individualPersonForeignState->contact_info_id = $contactInfoIPFS['id'];
            }

            $individualPersonForeignState->save();
        }

//*/
    }

    public static function parseGuaranteeInfoInvalid($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);

        if ($xml) {
            $main = [
                'id' => '',
                'filename' => basename($file),
                'bgId' => '',
                'externalId' => '',
                'docNumber' => '',
                'extendedDocNumber' => '',
                'docPublishDate' => '',
                'href' => '',

            ];

            if (isset($xml->bankGuaranteeInvalid->id)) {
                $main['bgId'] = (string)$xml->bankGuaranteeInvalid->id;
            }
            if (isset($xml->bankGuaranteeInvalid->externalId)) {
                $main['externalId'] = (string)$xml->bankGuaranteeInvalid->externalId;
            }
            if (isset($xml->bankGuaranteeInvalid->docNumber)) {
                $main['docNumber'] = (string)$xml->bankGuaranteeInvalid->docNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->extendedDocNumber)) {
                $main['extendedDocNumber'] = (string)$xml->bankGuaranteeInvalid->extendedDocNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->docPublishDate)) {
                $main['docPublishDate'] = (string)$xml->bankGuaranteeInvalid->docPublishDate;
            }
            if (isset($xml->bankGuaranteeInvalid->href)) {
                $main['href'] = (string)$xml->bankGuaranteeInvalid->href;
            }

            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO main (
                          filename,
                          bg_id,
                          external_id,
                          doc_number,
                          extended_doc_number,
                          doc_publish_date,
                          href
                      ) 
                      VALUES(
                          :filename,
                          :bgId,
                          :externalId,
                          :docNumber,
                          :extendedDocNumber,
                          :docPublishDate,
                          :href
                      )                      
					 ON CONFLICT (bg_id) DO NOTHING 
                      ";

            $returningState = Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':filename' => $main['filename'],
                    ':bgId' => $main['bgId'],
                    ':externalId' => $main['externalId'],
                    ':docNumber' => $main['docNumber'],
                    ':extendedDocNumber' => $main['extendedDocNumber'],
                    ':docPublishDate' => $main['docPublishDate'],
                    ':href' => $main['href'],
                ])
                ->execute();

            $main['id'] = Yii::$app->db_bank_guarantee->getLastInsertID('main_id_seq');

            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
            } else {
                echo "Record with bg_id = " . $main['bgId'] . " exist." . "\n";
                return;
            }
        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }

        //-----------print_form
        $printForm = [
            'main_id' => $main['id'],
            'url' => '',
        ];

        if (isset($xml->bankGuaranteeInvalid->printForm->url)) {
            $printForm['url'] = (string)$xml->bankGuaranteeInvalid->printForm->url;
        }
        //*-----------print_form
        //-----------print_form
        $printFormCondition =
            $printForm['url'] !== '';

        if ($printFormCondition) {
            $sql = "
                      INSERT INTO print_form (
                          main_id,
                          url
                      ) 
                      VALUES(
                          :mainId,
                          :url
                      )";

            Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':mainId' => $printForm['main_id'],
                    ':url' => $printForm['url'],
                ])
                ->execute();
        }
        //*-----------print_form

        $legalFormBank = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->legalForm->code)) {
            $legalFormBank['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->legalForm->code;
        }
        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->legalForm->singularName)) {
            $legalFormBank['singular_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->legalForm->singularName;
        }

        $legalFormBank['id'] = self::getLegalFormId($legalFormBank);

        $legalFormPlacingOrg = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->legalForm->code)) {
            $legalFormPlacingOrg['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->legalForm->code;
        }
        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->legalForm->singularName)) {
            $legalFormPlacingOrg['singular_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->legalForm->singularName;
        }

        $legalFormPlacingOrg['id'] = self::getLegalFormId($legalFormPlacingOrg);

        $legalFormLegalEntityRF = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->legalForm->code)) {
            $legalFormLegalEntityRF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->legalForm->code;
        }
        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->legalForm->singularName)) {
            $legalFormLegalEntityRF['singular_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->legalForm->singularName;
        }

        $legalFormLegalEntityRF['id'] = self::getLegalFormId($legalFormLegalEntityRF);

        $legalFormCustomer = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->legalForm->code)) {
            $legalFormCustomer['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->legalForm->code;
        }
        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->legalForm->singularName)) {
            $legalFormCustomer['singular_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->legalForm->singularName;
        }

        $legalFormCustomer['id'] = self::getLegalFormId($legalFormCustomer);

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF)) {
            $contactInfoIPRF = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->lastName)) {
                $contactInfoIPRF['last_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->lastName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->firstName)) {
                $contactInfoIPRF['first_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->firstName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->middleName)) {
                $contactInfoIPRF['middle_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->middleName;
            }

            $contactInfoIPRF['id'] = self::getContactInfoId($contactInfoIPRF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState)) {
            $contactInfoIPFS = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->lastName)) {
                $contactInfoIPFS['last_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->lastName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->firstName)) {
                $contactInfoIPFS['first_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->firstName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->middleName)) {
                $contactInfoIPFS['middle_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->middleName;

            }

            $contactInfoIPFS['id'] = self::getContactInfoId($contactInfoIPFS);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF)) {
            $subjectRFPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code)) {
                $subjectRFPOSIRF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name)) {
                $subjectRFPOSIRF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name;
            }

            $subjectRFPOSIRF['id'] = self::getSubjectRFId($subjectRFPOSIRF);

        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->subjectRF)) {
            $subjectRFIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->subjectRF->code)) {
                $subjectRFIPRF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->subjectRF->name)) {
                $subjectRFIPRF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->subjectRF->name;
            }

            $subjectRFIPRF['id'] = self::getSubjectRFId($subjectRFIPRF);

        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->subjectRF)) {
            $subjectRFLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->subjectRF->code)) {
                $subjectRFLERF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->subjectRF->name)) {
                $subjectRFLERF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->subjectRF->name;
            }

            $subjectRFLERF['id'] = self::getSubjectRFId($subjectRFLERF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->subjectRF)) {
            $subjectRFBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->subjectRF->code)) {
                $subjectRFBank['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->subjectRF->name)) {
                $subjectRFBank['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->subjectRF->name;
            }

            $subjectRFBank['id'] = self::getSubjectRFId($subjectRFBank);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->subjectRF)) {
            $subjectRFCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->subjectRF->code)) {
                $subjectRFCustomer['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->subjectRF->name)) {
                $subjectRFCustomer['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->subjectRF->name;
            }

            $subjectRFCustomer['id'] = self::getSubjectRFId($subjectRFCustomer);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->subjectRF)) {
            $subjectRFPlacingOrg = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->subjectRF->code)) {
                $subjectRFPlacingOrg['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->subjectRF->name)) {
                $subjectRFPlacingOrg['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->subjectRF->name;
            }

            $subjectRFPlacingOrg['id'] = self::getSubjectRFId($subjectRFPlacingOrg);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->OKTMO)) {
            $oktmoPlacingOrg = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->OKTMO->code)) {
                $oktmoPlacingOrg['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->OKTMO->name)) {
                $oktmoPlacingOrg['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->OKTMO->name;
            }

            $oktmoPlacingOrg['id'] = self::getOktmoId($oktmoPlacingOrg);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKTMO)) {
            $oktmoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKTMO->code)) {
                $oktmoLERF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKTMO->name)) {
                $oktmoLERF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKTMO->name;
            }

            $oktmoLERF['id'] = self::getOktmoId($oktmoLERF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO)) {
            $oktmoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code)) {
                $oktmoPOSIRF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name)) {
                $oktmoPOSIRF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name;
            }

            $oktmoPOSIRF['id'] = self::getOktmoId($oktmoPOSIRF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->OKTMO)) {
            $oktmoCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->OKTMO->code)) {
                $oktmoCustomer['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->OKTMO->name)) {
                $oktmoCustomer['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->OKTMO->name;
            }

            $oktmoCustomer['id'] = self::getOktmoId($oktmoCustomer);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->OKTMO)) {
            $oktmoBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->OKTMO->code)) {
                $oktmoBank['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->OKTMO->name)) {
                $oktmoBank['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->OKTMO->name;
            }

            $oktmoBank['id'] = self::getOktmoId($oktmoBank);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKTMO)) {
            $oktmoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKTMO->code)) {
                $oktmoIPRF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKTMO->name)) {
                $oktmoIPRF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKTMO->name;
            }

            $oktmoIPRF['id'] = self::getOktmoId($oktmoIPRF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO)) {
            $okatoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code)) {
                $okatoPOSIRF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name)) {
                $okatoPOSIRF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name;
            }

            $okatoPOSIRF['id'] = self::getOkatoId($okatoPOSIRF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKATO)) {
            $okatoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKATO->code)) {
                $okatoLERF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKATO->name)) {
                $okatoLERF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OKATO->name;
            }

            $okatoLERF['id'] = self::getOkatoId($okatoLERF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKATO)) {
            $okatoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKATO->code)) {
                $okatoIPRF['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKATO->name)) {
                $okatoIPRF['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OKATO->name;
            }

            $okatoIPRF['id'] = self::getOkatoId($okatoIPRF);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currency)) {
            $currencyGuarantee = [
                'id' => '',
                'code' => '',
                'digital_code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currency->code)) {
                $currencyGuarantee['code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currency->code;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currency->digitalCode)) {
                $currencyGuarantee['digital_code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currency->digitalCode;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currency->name)) {
                $currencyGuarantee['name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currency->name;
            }

            $currencyGuarantee['id'] = self::getCurrencyId($currencyGuarantee);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->country)) {
            $countryIPFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->country->countryCode)) {
                $countryIPFS['country_code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->country->countryFullName)) {
                $countryIPFS['country_full_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->country->countryFullName;
            }

            $countryIPFS['id'] = self::getCountryId($countryIPFS);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->country)) {
            $countryLEFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->country->countryCode)) {
                $countryLEFS['country_code'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->country->countryFullName)) {
                $countryLEFS['country_full_name'] = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->country->countryFullName;
            }

            $countryLEFS['id'] = self::getCountryId($countryLEFS);
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer)) {
            $customer = new Customer();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->regNum)) {
                $customer->reg_num = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->regNum;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->consRegistryNum)) {
                $customer->cons_registry_num = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->fullName)) {
                $customer->full_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->fullName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->shortName)) {
                $customer->short_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->shortName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->postAddress)) {
                $customer->post_address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->postAddress;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->factAddress)) {
                $customer->fact_address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->factAddress;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->INN)) {
                $customer->inn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->INN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->KPP)) {
                $customer->kpp = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->KPP;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->location)) {
                $customer->location = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->location;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->registrationDate)) {
                $customer->registration_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->registrationDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->IKU)) {
                $customer->iku = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->customer->IKU;
            }

            if (isset($legalFormCustomer) && !empty($legalFormCustomer['id'])) {
                $customer->legal_form_id = $legalFormCustomer['id'];
            }
            if (isset($subjectRFCustomer) && !empty($subjectRFCustomer['id'])) {
                $customer->subject_rf_id = $subjectRFCustomer['id'];
            }
            if (isset($oktmoCustomer) && !empty($oktmoCustomer['id'])) {
                $customer->oktmo_id = $oktmoCustomer['id'];
            }

            $customer->save();
            //$customer->id;
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank)) {
            $bank = new Bank();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->regNum)) {
                $bank->reg_num = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->regNum;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->consRegistryNum)) {
                $bank->cons_registry_num = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->fullName)) {
                $bank->full_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->fullName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->shortName)) {
                $bank->short_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->shortName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->postAddress)) {
                $bank->post_address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->postAddress;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->factAddress)) {
                $bank->fact_address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->factAddress;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->INN)) {
                $bank->inn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->INN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->KPP)) {
                $bank->kpp = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->KPP;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->location)) {
                $bank->location = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->location;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->registrationDate)) {
                $bank->registration_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->registrationDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->IKU)) {
                $bank->iku = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->IKU;
            }

            if (isset($main) && !empty($main['id'])) {
                $bank->main_id = $main['id'];
            }
            if (isset($legalFormBank) && !empty($legalFormBank['id'])) {
                $bank->legal_form_id = $legalFormBank['id'];
            }
            if (isset($subjectRFBank) && !empty($subjectRFBank['id'])) {
                $bank->subject_rf_id = $subjectRFBank['id'];
            }
            if (isset($oktmoBank) && !empty($oktmoBank['id'])) {
                $bank->oktmo_id = $oktmoBank['id'];
            }

            $bank->save();
        }

        if ($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg) {
            $placingOrg = new PlacingOrg();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->regNum)) {
                $placingOrg->reg_num = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->regNum;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->consRegistryNum)) {
                $placingOrg->cons_registry_num = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->fullName)) {
                $placingOrg->full_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->fullName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->shortName)) {
                $placingOrg->short_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->shortName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->postAddress)) {
                $placingOrg->post_address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->postAddress;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->factAddress)) {
                $placingOrg->fact_address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->factAddress;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->INN)) {
                $placingOrg->inn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->INN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->KPP)) {
                $placingOrg->kpp = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->KPP;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->location)) {
                $placingOrg->location = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->location;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->registrationDate)) {
                $placingOrg->registration_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->registrationDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->IKU)) {
                $placingOrg->iku = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->placingOrg->IKU;
            }

            if (isset($legalFormPlacingOrg) && !empty($legalFormPlacingOrg['id'])) {
                $placingOrg->legal_form_id = $legalFormPlacingOrg['id'];
            }
            if (isset($subjectRFPlacingOrg) && !empty($legalFormPlacingOrg['id'])) {
                $placingOrg->subject_rf_id = $subjectRFPlacingOrg['id'];
            }
            if (isset($oktmoPlacingOrg) && !empty($legalFormPlacingOrg['id'])) {
                $placingOrg->oktmo_id = $oktmoPlacingOrg['id'];
            }

            $placingOrg->save();
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF)) {
            $legalEntityRF = new LegalEntityRf();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->fullName)) {
                $legalEntityRF->full_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->fullName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->shortName)) {
                $legalEntityRF->short_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->shortName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->INN)) {
                $legalEntityRF->inn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->INN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->KPP)) {
                $legalEntityRF->kpp = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->KPP;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OGRN)) {
                $legalEntityRF->ogrn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->OGRN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->registrationDate)) {
                $legalEntityRF->registration_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->address)) {
                $legalEntityRF->address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityRF->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityRF->main_id = $main['id'];
            }
            if (isset($legalFormLegalEntityRF) && !empty($legalFormLegalEntityRF['id'])) {
                $legalEntityRF->legal_form_id = $legalFormLegalEntityRF['id'];
            }
            if (isset($subjectRFLERF) && !empty($subjectRFLERF['id'])) {
                $legalEntityRF->subject_rf_id = $subjectRFLERF['id'];
            }
            if (isset($okatoLERF) && !empty($okatoLERF['id'])) {
                $legalEntityRF->okato_id = $okatoLERF['id'];
            }
            if (isset($oktmoLERF) && !empty($oktmoLERF['id'])) {
                $legalEntityRF->oktmo_id = $oktmoLERF['id'];
            }

            $legalEntityRF->save();
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure)) {
            $contractExecutionEnsure = new ContractExecutionEnsure();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->regNum)) {
                $contractExecutionEnsure->reg_num = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->regNum;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->purchase->purchaseNumber)) {
                $contractExecutionEnsure->purchase_purchase_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->purchase->purchaseNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->purchase->notificationNumber)) {
                $contractExecutionEnsure->purchase_notification_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->purchase->notificationNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->purchase->lotNumber)) {
                $contractExecutionEnsure->purchase_lot_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->purchase->lotNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->mLots)) {
                $contractExecutionEnsure->m_lots = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->mLots;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->singleSupplier)) {
                $contractExecutionEnsure->single_supplier = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->contractExecutionEnsure->singleSupplier;
            }

            $contractExecutionEnsure->save();
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->purchaseNumber)) {
            $purchaseRequestEnsure = new PurchaseRequestEnsure();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->purchaseNumber)) {
                $purchaseRequestEnsure->purchase_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->purchaseNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->notificationNumber)) {
                $purchaseRequestEnsure->notification_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->notificationNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->lotNumber)) {
                $purchaseRequestEnsure->lot_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->lotNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->mLots)) {
                $purchaseRequestEnsure->m_lots = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseRequestEnsure->mLots;
            }

            $purchaseRequestEnsure->save();
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee)) {
            $guarantee = new \common\models\zakupki\guarantee\Guarantee();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->majorRepairEnsure->EFNumber)) {
                $guarantee->major_repair_ensure_ef_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->majorRepairEnsure->EFNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeDate)) {
                $guarantee->guarantee_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeNumber)) {
                $guarantee->guarantee_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeAmount)) {
                $guarantee->guarantee_amount = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeAmount;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->expireDate)) {
                $guarantee->expire_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->expireDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->entryForceDate)) {
                $guarantee->entry_force_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->entryForceDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->procedure)) {
                $guarantee->guarantee_procedure = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->procedure;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeAmountRUR)) {
                $guarantee->guarantee_amount_rur = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeAmountRUR;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currencyRate)) {
                $guarantee->currency_rate = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->currencyRate;
            }

            if (isset($main) && !empty($main['id'])) {
                $guarantee->main_id = $main['id'];
            }
            if (isset($purchaseRequestEnsure)) {
                $guarantee->purchase_request_ensure_id = $purchaseRequestEnsure->id;
            }

            if (isset($contractExecutionEnsure)) {
                $guarantee->contract_execution_ensure_id = $contractExecutionEnsure->id;
            }

            if (isset($customer)) {
                $guarantee->customer_id = $customer->id;
            }

            if (isset($currencyGuarantee) && !empty($currencyGuarantee['id'])) {
                $guarantee->currency_id = $currencyGuarantee['id'];
            }

            $guarantee->save();

            $guaranteeAdditionalInfo = new GuaranteeAdditionalInfo();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteePublishDate)) {
                $guaranteeAdditionalInfo->guarantee_publish_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteePublishDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->creditOrgNumber)) {
                $guaranteeAdditionalInfo->guarantee_credit_org_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->creditOrgNumber;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeGrantDate)) {
                $guaranteeAdditionalInfo->guarantee_grant_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->guaranteeGrantDate;
                $guaranteeAdditionalInfo->guarantee_id = $guarantee->id;
                $guaranteeAdditionalInfo->save();
            }
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseCodes)) {

            foreach ($xml->bankGuaranteeInvalid->guaranteeInfo->guarantee->purchaseCodes as $pC) {
                $purchaseCode = new PurchaseCode();

                if (isset($guarantee)) {
                    $purchaseCode->guarantee_id = $guarantee->id;
                }
                if (isset($pC->purchaseCode)) {
                    $purchaseCode->purchase_code = (string)$pC->purchaseCode;
                }
                $purchaseCode->save();
            }
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF)) {
            $individualPersonRf = new IndividualPersonRf();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->INN)) {
                $individualPersonRf->inn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->INN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OGRNIP)) {
                $individualPersonRf->ogrnip = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->OGRNIP;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->registrationDate)) {
                $individualPersonRf->registration_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->address)) {
                $individualPersonRf->address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->address;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->isIP)) {
                $individualPersonRf->is_ip = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonRF->isIP;
            }

            if (isset($main) && !empty($main['id'])) {
                $individualPersonRf->main_id = $main['id'];
            }
            if (isset($contactInfoIPRF) && !empty($contactInfoIPRF['id'])) {
                $individualPersonRf->contact_info_id = $contactInfoIPRF['id'];
            }
            if (isset($subjectRFIPRF) && !empty($subjectRFIPRF['id'])) {
                $individualPersonRf->subject_rf_id = $subjectRFIPRF['id'];
            }
            if (isset($okatoIPRF) && !empty($okatoIPRF['id'])) {
                $individualPersonRf->okato_id = $okatoIPRF['id'];
            }
            if (isset($oktmoIPRF) && !empty($oktmoIPRF['id'])) {
                $individualPersonRf->oktmo_id = $oktmoIPRF['id'];
            }

            $individualPersonRf->save();
        }

        $bankGuaranteeInvalid = new BankGuaranteeInvalid();

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->bank->regNum)) {
            $bankGuaranteeInvalid->reg_number = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->bank->regNum;
        }
        if (isset($xml->bankGuaranteeInvalid->reason)) {
            $bankGuaranteeInvalid->reason = (string)$xml->bankGuaranteeInvalid->reason;
        }
        if (isset($placingOrg)) {
            $bankGuaranteeInvalid->placing_org_id = $placingOrg->id;
        }

        $bankGuaranteeInvalid->save();

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->registerInRFTaxBodies)) {
            $registerInRfTaxBodies = new RegisterInRfTaxBodies();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN)) {
                $registerInRfTaxBodies->inn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP)) {
                $registerInRfTaxBodies->kpp = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate)) {
                $registerInRfTaxBodies->registration_date = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate;
            }

            $registerInRfTaxBodies->save();
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState)) {
            $legalEntityForeignState = new LegalEntityForeignState();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->fullName)) {
                $legalEntityForeignState->full_name = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->fullName;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->fullNameLat)) {
                $legalEntityForeignState->full_name_lat = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->fullNameLat;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->taxPayerCode)) {
                $legalEntityForeignState->tax_payer_code = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->address)) {
                $legalEntityForeignState->address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityForeignState->main_id = $main['id'];
            }
            if (isset($countryLEFS) && !empty($countryLEFS['id'])) {
                $legalEntityForeignState->country_id = $countryLEFS['id'];
            }
            if (isset($registerInRfTaxBodies)) {
                $legalEntityForeignState->register_in_rf_tax_bodies_id = $registerInRfTaxBodies->id;
            }

            $legalEntityForeignState->save();
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF)) {

            $placeOfStayInRf = new PlaceOfStayInRf();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->address)) {
                $placeOfStayInRf->address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->legalEntityForeignState->placeOfStayInRF->address;
            }

            if (isset($legalEntityForeignState)) {
                $placeOfStayInRf->legal_entity_foreign_state_id = $legalEntityForeignState->id;
            }
            if (isset($subjectRFPOSIRF) && !empty($subjectRFPOSIRF['id'])) {
                $placeOfStayInRf->subject_rf_id = $subjectRFPOSIRF['id'];
            }
            if (isset($okatoPOSIRF) && !empty($okatoPOSIRF['id'])) {
                $placeOfStayInRf->okato_id = $okatoPOSIRF['id'];
            }
            if (isset($oktmoPOSIRF) && !empty($oktmoPOSIRF['id'])) {
                $placeOfStayInRf->oktmo_id = $oktmoPOSIRF['id'];
            }

            $placeOfStayInRf->save();
        }

        if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState)) {
            $individualPersonForeignState = new IndividualPersonForeignState();

            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->lastNameLat)) {
                $individualPersonForeignState->last_name_lat = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->lastNameLat;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->firstNameLat)) {
                $individualPersonForeignState->first_name_lat = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->firstNameLat;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->middleNameLat)) {
                $individualPersonForeignState->middle_name_lat = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->middleNameLat;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->INN)) {
                $individualPersonForeignState->inn = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->INN;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->taxPayerCode)) {
                $individualPersonForeignState->tax_payer_code = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->address)) {
                $individualPersonForeignState->address = (string)$xml->bankGuaranteeInvalid->guaranteeInfo->supplierInfo->individualPersonForeignState->address;
            }

            if (isset($countryIPFS) && !empty($countryIPFS['id'])) {
                $individualPersonForeignState->country_id = $countryIPFS['id'];
            }
            if (isset($contactInfoIPFS) && !empty($contactInfoIPFS['id'])) {
                $individualPersonForeignState->contact_info_id = $contactInfoIPFS['id'];
            }

            $individualPersonForeignState->save();
        }

//*/
    }

    public static function parseGuaranteeReturn($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);

        if ($xml) {
            $main = [
                'id' => '',
                'filename' => basename($file),
                'bgId' => '',
                'externalId' => '',
                'docNumber' => '',
                'extendedDocNumber' => '',
                'docPublishDate' => '',
                'href' => '',

            ];

            if (isset($xml->bankGuaranteeReturn->id)) {
                $main['bgId'] = (string)$xml->bankGuaranteeReturn->id;
            }
            if (isset($xml->bankGuaranteeReturn->externalId)) {
                $main['externalId'] = (string)$xml->bankGuaranteeReturn->externalId;
            }
            if (isset($xml->bankGuaranteeReturn->docNumber)) {
                $main['docNumber'] = (string)$xml->bankGuaranteeReturn->docNumber;
            }
            if (isset($xml->bankGuaranteeReturn->extendedDocNumber)) {
                $main['extendedDocNumber'] = (string)$xml->bankGuaranteeReturn->extendedDocNumber;
            }
            if (isset($xml->bankGuaranteeReturn->docPublishDate)) {
                $main['docPublishDate'] = (string)$xml->bankGuaranteeReturn->docPublishDate;
            }
            if (isset($xml->bankGuaranteeReturn->href)) {
                $main['href'] = (string)$xml->bankGuaranteeReturn->href;
            }

            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO main (
                          filename,
                          bg_id,
                          external_id,
                          doc_number,
                          extended_doc_number,
                          doc_publish_date,
                          href
                      ) 
                      VALUES(
                          :filename,
                          :bgId,
                          :externalId,
                          :docNumber,
                          :extendedDocNumber,
                          :docPublishDate,
                          :href
                      )                      
					 ON CONFLICT (bg_id) DO NOTHING 
                      ";

            $returningState = Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':filename' => $main['filename'],
                    ':bgId' => $main['bgId'],
                    ':externalId' => $main['externalId'],
                    ':docNumber' => $main['docNumber'],
                    ':extendedDocNumber' => $main['extendedDocNumber'],
                    ':docPublishDate' => $main['docPublishDate'],
                    ':href' => $main['href'],
                ])
                ->execute();

            $main['id'] = Yii::$app->db_bank_guarantee->getLastInsertID('main_id_seq');

            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
            } else {
                echo "Record with bg_id = " . $main['bgId'] . " exist." . "\n";
                return;
            }
        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }

        //-----------print_form
        $printForm = [
            'main_id' => $main['id'],
            'url' => '',
        ];

        if (isset($xml->bankGuaranteeReturn->printForm->url)) {
            $printForm['url'] = (string)$xml->bankGuaranteeReturn->printForm->url;
        }
        //*-----------print_form
        //-----------print_form
        $printFormCondition =
            $printForm['url'] !== '';

        if ($printFormCondition) {
            $sql = "
                      INSERT INTO print_form (
                          main_id,
                          url
                      ) 
                      VALUES(
                          :mainId,
                          :url
                      )";

            Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':mainId' => $printForm['main_id'],
                    ':url' => $printForm['url'],
                ])
                ->execute();
        }
        //*-----------print_form

        $legalFormBank = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeReturn->bank->legalForm->code)) {
            $legalFormBank['code'] = (string)$xml->bankGuaranteeReturn->bank->legalForm->code;
        }
        if (isset($xml->bankGuaranteeReturn->bank->legalForm->singularName)) {
            $legalFormBank['singular_name'] = (string)$xml->bankGuaranteeReturn->bank->legalForm->singularName;
        }

        $legalFormBank['id'] = self::getLegalFormId($legalFormBank);

        $legalFormLegalEntityRF = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->legalForm->code)) {
            $legalFormLegalEntityRF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->legalForm->code;
        }
        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->legalForm->singularName)) {
            $legalFormLegalEntityRF['singular_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->legalForm->singularName;
        }

        $legalFormLegalEntityRF['id'] = self::getLegalFormId($legalFormLegalEntityRF);

        $legalFormCustomer = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeReturn->guarantee->customer->legalForm->code)) {
            $legalFormCustomer['code'] = (string)$xml->bankGuaranteeReturn->guarantee->customer->legalForm->code;
        }
        if (isset($xml->bankGuaranteeReturn->guarantee->customer->legalForm->singularName)) {
            $legalFormCustomer['singular_name'] = (string)$xml->bankGuaranteeReturn->guarantee->customer->legalForm->singularName;
        }

        $legalFormCustomer['id'] = self::getLegalFormId($legalFormCustomer);

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF)) {
            $contactInfoIPRF = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->lastName)) {
                $contactInfoIPRF['last_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->lastName;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->firstName)) {
                $contactInfoIPRF['first_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->firstName;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->middleName)) {
                $contactInfoIPRF['middle_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->middleName;
            }

            $contactInfoIPRF['id'] = self::getContactInfoId($contactInfoIPRF);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState)) {
            $contactInfoIPFS = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->lastName)) {
                $contactInfoIPFS['last_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->lastName;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->firstName)) {
                $contactInfoIPFS['first_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->firstName;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->middleName)) {
                $contactInfoIPFS['middle_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->middleName;

            }

            $contactInfoIPFS['id'] = self::getContactInfoId($contactInfoIPFS);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF)) {
            $subjectRFPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code)) {
                $subjectRFPOSIRF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name)) {
                $subjectRFPOSIRF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name;
            }

            $subjectRFPOSIRF['id'] = self::getSubjectRFId($subjectRFPOSIRF);

        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->subjectRF)) {
            $subjectRFIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->subjectRF->code)) {
                $subjectRFIPRF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->subjectRF->name)) {
                $subjectRFIPRF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->subjectRF->name;
            }

            $subjectRFIPRF['id'] = self::getSubjectRFId($subjectRFIPRF);

        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->subjectRF)) {
            $subjectRFLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->subjectRF->code)) {
                $subjectRFLERF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->subjectRF->name)) {
                $subjectRFLERF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->subjectRF->name;
            }

            $subjectRFLERF['id'] = self::getSubjectRFId($subjectRFLERF);
        }

        if (isset($xml->bankGuaranteeReturn->bank->subjectRF)) {
            $subjectRFBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->bank->subjectRF->code)) {
                $subjectRFBank['code'] = (string)$xml->bankGuaranteeReturn->bank->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeReturn->bank->subjectRF->name)) {
                $subjectRFBank['name'] = (string)$xml->bankGuaranteeReturn->bank->subjectRF->name;
            }

            $subjectRFBank['id'] = self::getSubjectRFId($subjectRFBank);
        }

        if (isset($xml->bankGuaranteeReturn->guarantee->customer->subjectRF)) {
            $subjectRFCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->guarantee->customer->subjectRF->code)) {
                $subjectRFCustomer['code'] = (string)$xml->bankGuaranteeReturn->guarantee->customer->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->subjectRF->name)) {
                $subjectRFCustomer['name'] = (string)$xml->bankGuaranteeReturn->guarantee->customer->subjectRF->name;
            }

            $subjectRFCustomer['id'] = self::getSubjectRFId($subjectRFCustomer);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKTMO)) {
            $oktmoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKTMO->code)) {
                $oktmoLERF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKTMO->name)) {
                $oktmoLERF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKTMO->name;
            }

            $oktmoLERF['id'] = self::getOktmoId($oktmoLERF);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO)) {
            $oktmoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code)) {
                $oktmoPOSIRF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name)) {
                $oktmoPOSIRF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name;
            }

            $oktmoPOSIRF['id'] = self::getOktmoId($oktmoPOSIRF);
        }

        if (isset($xml->bankGuaranteeReturn->guarantee->customer->OKTMO)) {
            $oktmoCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->guarantee->customer->OKTMO->code)) {
                $oktmoCustomer['code'] = (string)$xml->bankGuaranteeReturn->guarantee->customer->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->OKTMO->name)) {
                $oktmoCustomer['name'] = (string)$xml->bankGuaranteeReturn->guarantee->customer->OKTMO->name;
            }

            $oktmoCustomer['id'] = self::getOktmoId($oktmoCustomer);
        }

        if (isset($xml->bankGuaranteeReturn->bank->OKTMO)) {
            $oktmoBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->bank->OKTMO->code)) {
                $oktmoBank['code'] = (string)$xml->bankGuaranteeReturn->bank->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeReturn->bank->OKTMO->name)) {
                $oktmoBank['name'] = (string)$xml->bankGuaranteeReturn->bank->OKTMO->name;
            }

            $oktmoBank['id'] = self::getOktmoId($oktmoBank);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKTMO)) {
            $oktmoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKTMO->code)) {
                $oktmoIPRF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKTMO->name)) {
                $oktmoIPRF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKTMO->name;
            }

            $oktmoIPRF['id'] = self::getOktmoId($oktmoIPRF);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO)) {
            $okatoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code)) {
                $okatoPOSIRF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name)) {
                $okatoPOSIRF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name;
            }

            $okatoPOSIRF['id'] = self::getOkatoId($okatoPOSIRF);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKATO)) {
            $okatoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKATO->code)) {
                $okatoLERF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKATO->name)) {
                $okatoLERF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OKATO->name;
            }

            $okatoLERF['id'] = self::getOkatoId($okatoLERF);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKATO)) {
            $okatoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKATO->code)) {
                $okatoIPRF['code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKATO->name)) {
                $okatoIPRF['name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OKATO->name;
            }

            $okatoIPRF['id'] = self::getOkatoId($okatoIPRF);
        }

        if (isset($xml->bankGuaranteeReturn->guarantee->currency)) {
            $currencyGuarantee = [
                'id' => '',
                'code' => '',
                'digital_code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->guarantee->currency->code)) {
                $currencyGuarantee['code'] = (string)$xml->bankGuaranteeReturn->guarantee->currency->code;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->currency->digitalCode)) {
                $currencyGuarantee['digital_code'] = (string)$xml->bankGuaranteeReturn->guarantee->currency->digitalCode;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->currency->name)) {
                $currencyGuarantee['name'] = (string)$xml->bankGuaranteeReturn->guarantee->currency->name;
            }

            $currencyGuarantee['id'] = self::getCurrencyId($currencyGuarantee);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->country)) {
            $countryIPFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->country->countryCode)) {
                $countryIPFS['country_code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->country->countryFullName)) {
                $countryIPFS['country_full_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->country->countryFullName;
            }

            $countryIPFS['id'] = self::getCountryId($countryIPFS);
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->country)) {
            $countryLEFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->country->countryCode)) {
                $countryLEFS['country_code'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->country->countryFullName)) {
                $countryLEFS['country_full_name'] = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->country->countryFullName;
            }

            $countryLEFS['id'] = self::getCountryId($countryLEFS);
        }

        if (isset($xml->bankGuaranteeReturn->guarantee->customer)) {
            $customer = new Customer();

            if (isset($xml->bankGuaranteeReturn->guarantee->customer->regNum)) {
                $customer->reg_num = (string)$xml->bankGuaranteeReturn->guarantee->customer->regNum;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->consRegistryNum)) {
                $customer->cons_registry_num = (string)$xml->bankGuaranteeReturn->guarantee->customer->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->fullName)) {
                $customer->full_name = (string)$xml->bankGuaranteeReturn->guarantee->customer->fullName;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->shortName)) {
                $customer->short_name = (string)$xml->bankGuaranteeReturn->guarantee->customer->shortName;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->postAddress)) {
                $customer->post_address = (string)$xml->bankGuaranteeReturn->guarantee->customer->postAddress;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->factAddress)) {
                $customer->fact_address = (string)$xml->bankGuaranteeReturn->guarantee->customer->factAddress;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->INN)) {
                $customer->inn = (string)$xml->bankGuaranteeReturn->guarantee->customer->INN;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->KPP)) {
                $customer->kpp = (string)$xml->bankGuaranteeReturn->guarantee->customer->KPP;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->location)) {
                $customer->location = (string)$xml->bankGuaranteeReturn->guarantee->customer->location;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->registrationDate)) {
                $customer->registration_date = (string)$xml->bankGuaranteeReturn->guarantee->customer->registrationDate;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->customer->IKU)) {
                $customer->iku = (string)$xml->bankGuaranteeReturn->guarantee->customer->IKU;
            }

            if (isset($legalFormCustomer) && !empty($legalFormCustomer['id'])) {
                $customer->legal_form_id = $legalFormCustomer['id'];
            }
            if (isset($subjectRFCustomer) && !empty($subjectRFCustomer['id'])) {
                $customer->subject_rf_id = $subjectRFCustomer['id'];
            }
            if (isset($oktmoCustomer) && !empty($oktmoCustomer['id'])) {
                $customer->oktmo_id = $oktmoCustomer['id'];
            }

            $customer->save();
        }

        if (isset($xml->bankGuaranteeReturn->bank)) {
            $bank = new Bank();

            if (isset($xml->bankGuaranteeReturn->bank->regNum)) {
                $bank->reg_num = (string)$xml->bankGuaranteeReturn->bank->regNum;
            }
            if (isset($xml->bankGuaranteeReturn->bank->consRegistryNum)) {
                $bank->cons_registry_num = (string)$xml->bankGuaranteeReturn->bank->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeReturn->bank->fullName)) {
                $bank->full_name = (string)$xml->bankGuaranteeReturn->bank->fullName;
            }
            if (isset($xml->bankGuaranteeReturn->bank->shortName)) {
                $bank->short_name = (string)$xml->bankGuaranteeReturn->bank->shortName;
            }
            if (isset($xml->bankGuaranteeReturn->bank->postAddress)) {
                $bank->post_address = (string)$xml->bankGuaranteeReturn->bank->postAddress;
            }
            if (isset($xml->bankGuaranteeReturn->bank->factAddress)) {
                $bank->fact_address = (string)$xml->bankGuaranteeReturn->bank->factAddress;
            }
            if (isset($xml->bankGuaranteeReturn->bank->INN)) {
                $bank->inn = (string)$xml->bankGuaranteeReturn->bank->INN;
            }
            if (isset($xml->bankGuaranteeReturn->bank->KPP)) {
                $bank->kpp = (string)$xml->bankGuaranteeReturn->bank->KPP;
            }
            if (isset($xml->bankGuaranteeReturn->bank->location)) {
                $bank->location = (string)$xml->bankGuaranteeReturn->bank->location;
            }
            if (isset($xml->bankGuaranteeReturn->bank->registrationDate)) {
                $bank->registration_date = (string)$xml->bankGuaranteeReturn->bank->registrationDate;
            }
            if (isset($xml->bankGuaranteeReturn->bank->IKU)) {
                $bank->iku = (string)$xml->bankGuaranteeReturn->bank->IKU;
            }

            if (isset($main) && !empty($main['id'])) {
                $bank->main_id = $main['id'];
            }
            if (isset($legalFormBank) && !empty($legalFormBank['id'])) {
                $bank->legal_form_id = $legalFormBank['id'];
            }
            if (isset($subjectRFBank) && !empty($subjectRFBank['id'])) {
                $bank->subject_rf_id = $subjectRFBank['id'];
            }
            if (isset($oktmoBank) && !empty($oktmoBank['id'])) {
                $bank->oktmo_id = $oktmoBank['id'];
            }

            $bank->save();
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF)) {
            $legalEntityRF = new LegalEntityRf();

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->fullName)) {
                $legalEntityRF->full_name = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->fullName;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->shortName)) {
                $legalEntityRF->short_name = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->shortName;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->INN)) {
                $legalEntityRF->inn = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->INN;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->KPP)) {
                $legalEntityRF->kpp = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->KPP;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OGRN)) {
                $legalEntityRF->ogrn = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->OGRN;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->registrationDate)) {
                $legalEntityRF->registration_date = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->address)) {
                $legalEntityRF->address = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityRF->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityRF->main_id = $main['id'];
            }
            if (isset($legalFormLegalEntityRF) && !empty($legalFormLegalEntityRF['id'])) {
                $legalEntityRF->legal_form_id = $legalFormLegalEntityRF['id'];
            }
            if (isset($subjectRFLERF) && !empty($subjectRFLERF['id'])) {
                $legalEntityRF->subject_rf_id = $subjectRFLERF['id'];
            }
            if (isset($okatoLERF) && !empty($okatoLERF['id'])) {
                $legalEntityRF->okato_id = $okatoLERF['id'];
            }
            if (isset($oktmoLERF) && !empty($oktmoLERF['id'])) {
                $legalEntityRF->oktmo_id = $oktmoLERF['id'];
            }

            $legalEntityRF->save();
        }

        if (isset($xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure)) {
            $contractExecutionEnsure = new ContractExecutionEnsure();

            if (isset($xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->regNum)) {
                $contractExecutionEnsure->reg_num = (string)$xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->regNum;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->purchase->purchaseNumber)) {
                $contractExecutionEnsure->purchase_purchase_number = (string)$xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->purchase->purchaseNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->purchase->notificationNumber)) {
                $contractExecutionEnsure->purchase_notification_number = (string)$xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->purchase->notificationNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->purchase->lotNumber)) {
                $contractExecutionEnsure->purchase_lot_number = (string)$xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->purchase->lotNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->mLots)) {
                $contractExecutionEnsure->m_lots = (string)$xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->mLots;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->singleSupplier)) {
                $contractExecutionEnsure->single_supplier = (string)$xml->bankGuaranteeReturn->guarantee->contractExecutionEnsure->singleSupplier;
            }

            $contractExecutionEnsure->save();
        }

        if (isset($xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->purchaseNumber)) {
            $purchaseRequestEnsure = new PurchaseRequestEnsure();

            if (isset($xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->purchaseNumber)) {
                $purchaseRequestEnsure->purchase_number = (string)$xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->purchaseNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->notificationNumber)) {
                $purchaseRequestEnsure->notification_number = (string)$xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->notificationNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->lotNumber)) {
                $purchaseRequestEnsure->lot_number = (string)$xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->lotNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->mLots)) {
                $purchaseRequestEnsure->m_lots = (string)$xml->bankGuaranteeReturn->guarantee->purchaseRequestEnsure->mLots;
            }

            $purchaseRequestEnsure->save();
        }

        if (isset($xml->bankGuaranteeReturn->guarantee)) {
            $guarantee = new \common\models\zakupki\guarantee\Guarantee();

            if (isset($xml->bankGuaranteeReturn->guarantee->majorRepairEnsure->EFNumber)) {
                $guarantee->major_repair_ensure_ef_number = (string)$xml->bankGuaranteeReturn->guarantee->majorRepairEnsure->EFNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->guaranteeDate)) {
                $guarantee->guarantee_date = (string)$xml->bankGuaranteeReturn->guarantee->guaranteeDate;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->guaranteeNumber)) {
                $guarantee->guarantee_number = (string)$xml->bankGuaranteeReturn->guarantee->guaranteeNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->guaranteeAmount)) {
                $guarantee->guarantee_amount = (string)$xml->bankGuaranteeReturn->guarantee->guaranteeAmount;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->expireDate)) {
                $guarantee->expire_date = (string)$xml->bankGuaranteeReturn->guarantee->expireDate;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->entryForceDate)) {
                $guarantee->entry_force_date = (string)$xml->bankGuaranteeReturn->guarantee->entryForceDate;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->procedure)) {
                $guarantee->guarantee_procedure = (string)$xml->bankGuaranteeReturn->guarantee->procedure;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->guaranteeAmountRUR)) {
                $guarantee->guarantee_amount_rur = (string)$xml->bankGuaranteeReturn->guarantee->guaranteeAmountRUR;
            }
            if (isset($xml->bankGuaranteeReturn->guarantee->currencyRate)) {
                $guarantee->currency_rate = (string)$xml->bankGuaranteeReturn->guarantee->currencyRate;
            }

            if (isset($main) && !empty($main['id'])) {
                $guarantee->main_id = $main['id'];
            }
            if (isset($purchaseRequestEnsure)) {
                $guarantee->purchase_request_ensure_id = $purchaseRequestEnsure->id;
            }

            if (isset($contractExecutionEnsure)) {
                $guarantee->contract_execution_ensure_id = $contractExecutionEnsure->id;
            }

            if (isset($customer)) {
                $guarantee->customer_id = $customer->id;
            }

            if (isset($currencyGuarantee) && !empty($currencyGuarantee['id'])) {
                $guarantee->currency_id = $currencyGuarantee['id'];
            }

            $guarantee->save();

            $guaranteeAdditionalInfo = new GuaranteeAdditionalInfo();

            if (isset($xml->bankGuaranteeReturn->guaranteeInfo->guarantee->guaranteePublishDate)) {
                $guaranteeAdditionalInfo->guarantee_publish_date = (string)$xml->bankGuaranteeReturn->guaranteeInfo->guarantee->guaranteePublishDate;
            }
            if (isset($xml->bankGuaranteeReturn->guaranteeInfo->guarantee->creditOrgNumber)) {
                $guaranteeAdditionalInfo->guarantee_credit_org_number = (string)$xml->bankGuaranteeReturn->guaranteeInfo->guarantee->creditOrgNumber;
            }
            if (isset($xml->bankGuaranteeReturn->guaranteeInfo->guarantee->guaranteeGrantDate)) {
                $guaranteeAdditionalInfo->guarantee_grant_date = (string)$xml->bankGuaranteeReturn->guaranteeInfo->guarantee->guaranteeGrantDate;
                $guaranteeAdditionalInfo->guarantee_id = $guarantee->id;
                $guaranteeAdditionalInfo->save();
            }
        }

        if (isset($xml->bankGuaranteeReturn->guarantee->purchaseCodes)) {

            foreach ($xml->bankGuaranteeReturn->guarantee->purchaseCodes as $pC) {
                $purchaseCode = new PurchaseCode();

                if (isset($guarantee)) {
                    $purchaseCode->guarantee_id = $guarantee->id;
                }
                if (isset($pC->purchaseCode)) {
                    $purchaseCode->purchase_code = (string)$pC->purchaseCode;
                }
                $purchaseCode->save();
            }
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF)) {
            $individualPersonRf = new IndividualPersonRf();

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->INN)) {
                $individualPersonRf->inn = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->INN;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OGRNIP)) {
                $individualPersonRf->ogrnip = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->OGRNIP;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->registrationDate)) {
                $individualPersonRf->registration_date = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->address)) {
                $individualPersonRf->address = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->address;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->isIP)) {
                $individualPersonRf->is_ip = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonRF->isIP;
            }

            if (isset($main) && !empty($main['id'])) {
                $individualPersonRf->main_id = $main['id'];
            }
            if (isset($contactInfoIPRF) && !empty($contactInfoIPRF['id'])) {
                $individualPersonRf->contact_info_id = $contactInfoIPRF['id'];
            }
            if (isset($subjectRFIPRF) && !empty($subjectRFIPRF['id'])) {
                $individualPersonRf->subject_rf_id = $subjectRFIPRF['id'];
            }
            if (isset($okatoIPRF) && !empty($okatoIPRF['id'])) {
                $individualPersonRf->okato_id = $okatoIPRF['id'];
            }
            if (isset($oktmoIPRF) && !empty($oktmoIPRF['id'])) {
                $individualPersonRf->oktmo_id = $oktmoIPRF['id'];
            }

            $individualPersonRf->save();
        }
//----------------------------------------------------------------------------------------------------------------------
        $bankGuaranteeReturn = new BankGuaranteeReturn();

        if (isset($xml->bankGuaranteeReturn->bank->regNum)) {
            $bankGuaranteeReturn->reg_number = (string)$xml->bankGuaranteeReturn->bank->regNum;
        }
        if (isset($xml->bankGuaranteeReturn->versionNumber)) {
            $bankGuaranteeReturn->version_number = (string)$xml->bankGuaranteeReturn->versionNumber;
        }
        if (isset($xml->bankGuaranteeReturn->modificationInfo)) {
            $bankGuaranteeReturn->modification_info = (string)$xml->bankGuaranteeReturn->modificationInfo;
        }
        if (isset($xml->bankGuaranteeReturn->regNum)) {
            $bankGuaranteeReturn->reg_num = (string)$xml->bankGuaranteeReturn->regNum;
        }
        if (isset($main) && !empty($main['id'])) {
            $bankGuaranteeReturn->main_id = $main['id'];
        }

        $bankGuaranteeReturn->save();

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->registerInRFTaxBodies)) {
            $registerInRfTaxBodies = new RegisterInRfTaxBodies();

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN)) {
                $registerInRfTaxBodies->inn = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP)) {
                $registerInRfTaxBodies->kpp = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate)) {
                $registerInRfTaxBodies->registration_date = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate;
            }

            $registerInRfTaxBodies->save();
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState)) {
            $legalEntityForeignState = new LegalEntityForeignState();

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->fullName)) {
                $legalEntityForeignState->full_name = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->fullName;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->fullNameLat)) {
                $legalEntityForeignState->full_name_lat = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->fullNameLat;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->taxPayerCode)) {
                $legalEntityForeignState->tax_payer_code = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->address)) {
                $legalEntityForeignState->address = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityForeignState->main_id = $main['id'];
            }
            if (isset($countryLEFS) && !empty($countryLEFS['id'])) {
                $legalEntityForeignState->country_id = $countryLEFS['id'];
            }
            if (isset($registerInRfTaxBodies)) {
                $legalEntityForeignState->register_in_rf_tax_bodies_id = $registerInRfTaxBodies->id;
            }

            $legalEntityForeignState->save();
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF)) {

            $placeOfStayInRf = new PlaceOfStayInRf();

            if (isset($xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->address)) {
                $placeOfStayInRf->address = (string)$xml->bankGuaranteeReturn->supplierInfo->legalEntityForeignState->placeOfStayInRF->address;
            }

            if (isset($legalEntityForeignState)) {
                $placeOfStayInRf->legal_entity_foreign_state_id = $legalEntityForeignState->id;
            }
            if (isset($subjectRFPOSIRF) && !empty($subjectRFPOSIRF['id'])) {
                $placeOfStayInRf->subject_rf_id = $subjectRFPOSIRF['id'];
            }
            if (isset($okatoPOSIRF) && !empty($okatoPOSIRF['id'])) {
                $placeOfStayInRf->okato_id = $okatoPOSIRF['id'];
            }
            if (isset($oktmoPOSIRF) && !empty($oktmoPOSIRF['id'])) {
                $placeOfStayInRf->oktmo_id = $oktmoPOSIRF['id'];
            }

            $placeOfStayInRf->save();
        }

        if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState)) {
            $individualPersonForeignState = new IndividualPersonForeignState();

            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->lastNameLat)) {
                $individualPersonForeignState->last_name_lat = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->lastNameLat;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->firstNameLat)) {
                $individualPersonForeignState->first_name_lat = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->firstNameLat;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->middleNameLat)) {
                $individualPersonForeignState->middle_name_lat = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->middleNameLat;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->INN)) {
                $individualPersonForeignState->inn = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->INN;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->taxPayerCode)) {
                $individualPersonForeignState->tax_payer_code = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->address)) {
                $individualPersonForeignState->address = (string)$xml->bankGuaranteeReturn->supplierInfo->individualPersonForeignState->address;
            }

            if (isset($countryIPFS) && !empty($countryIPFS['id'])) {
                $individualPersonForeignState->country_id = $countryIPFS['id'];
            }
            if (isset($contactInfoIPFS) && !empty($contactInfoIPFS['id'])) {
                $individualPersonForeignState->contact_info_id = $contactInfoIPFS['id'];
            }

            $individualPersonForeignState->save();
        }

        if (isset($xml->bankGuaranteeReturn->placer)) {
            $placer = new Placer();
            if (isset($xml->bankGuaranteeReturn->placer->responsibleOrg->regNum)) {
                $placer->responsible_org_reg_num = (string)$xml->bankGuaranteeReturn->placer->responsibleOrg->regNum;
            }
            if (isset($xml->bankGuaranteeReturn->placer->responsibleOrg->consRegistryNum)) {
                $placer->responsible_org_cons_registry_num = (string)$xml->bankGuaranteeReturn->placer->responsibleOrg->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeReturn->placer->responsibleOrg->fullName)) {
                $placer->responsible_org_full_name = (string)$xml->bankGuaranteeReturn->placer->responsibleOrg->fullName;
            }
            if (isset($xml->bankGuaranteeReturn->placer->responsibleRole)) {
                $placer->responsible_role = (string)$xml->bankGuaranteeReturn->placer->responsibleRole;
            }

            if (isset($main) && !empty($main['id'])) {
                $placer->main_id = $main['id'];
            }

            $placer->save();
        }

        if (isset($xml->bankGuaranteeReturn->guaranteeReturns)) {

            foreach ($xml->bankGuaranteeReturn->guaranteeReturns as $gR) {

                $guaranteeReturn = new GuaranteeReturn();
                var_dump($gR->guaranteeReturn);

                if (isset($main) && !empty($main['id'])) {
                    $guaranteeReturn->main_id = $main['id'];
                }

                if (isset($gR->guaranteeReturn->bankGuaranteeReturn->regNumber)) {
                    $guaranteeReturn->bgr_reg_number = (string)$gR->guaranteeReturn->bankGuaranteeReturn->regNumber;
                }
                if (isset($gR->guaranteeReturn->bankGuaranteeReturn->docNumber)) {
                    $guaranteeReturn->bgr_doc_number = (string)$gR->guaranteeReturn->bankGuaranteeReturn->docNumber;
                }
                if (isset($gR->guaranteeReturn->bankGuaranteeReturn->returnDate)) {
                    $guaranteeReturn->bgr_return_date = (string)$gR->guaranteeReturn->bankGuaranteeReturn->returnDate;
                }
                if (isset($gR->guaranteeReturn->bankGuaranteeReturn->returnReason)) {
                    $guaranteeReturn->bgr_return_reason = (string)$gR->guaranteeReturn->bankGuaranteeReturn->returnReason;
                }
                if (isset($gR->guaranteeReturn->bankGuaranteeReturn->returnPublishDate)) {
                    $guaranteeReturn->bgr_return_publish_date = (string)$gR->guaranteeReturn->bankGuaranteeReturn->returnPublishDate;
                }

                if (isset($gR->guaranteeReturn->waiverNotice->regNumber)) {
                    $guaranteeReturn->wn_reg_number = (string)$gR->guaranteeReturn->waiverNotice->regNumber;
                }
                if (isset($gR->guaranteeReturn->waiverNotice->docNumber)) {
                    $guaranteeReturn->wn_doc_number = (string)$gR->guaranteeReturn->waiverNotice->docNumber;
                }
                if (isset($gR->guaranteeReturn->waiverNotice->noticeDate)) {
                    $guaranteeReturn->wn_notice_date = (string)$gR->guaranteeReturn->waiverNotice->noticeDate;
                }
                if (isset($gR->guaranteeReturn->waiverNotice->noticeNumber)) {
                    $guaranteeReturn->wn_notice_number = (string)$gR->guaranteeReturn->waiverNotice->noticeNumber;
                }
                if (isset($gR->guaranteeReturn->waiverNotice->noticeReason)) {
                    $guaranteeReturn->wn_notice_reason = (string)$gR->guaranteeReturn->waiverNotice->noticeReason;
                }
                if (isset($gR->guaranteeReturn->waiverNotice->noticePublishDate)) {
                    $guaranteeReturn->wn_notice_publish_date = (string)$gR->guaranteeReturn->waiverNotice->noticePublishDate;
                }

                $guaranteeReturn->save();
            }
        }
    }

    //public static function parseGuaranteeReturnInvalid($pathExtraction, $file){}

    public static function parseGuaranteeRefusal($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);

        if ($xml) {
            $main = [
                'id' => '',
                'filename' => basename($file),
                'bgId' => '',
                'externalId' => '',
                'docNumber' => '',
                'docPublishDate' => '',
                'href' => '',

            ];

            if (isset($xml->bankGuaranteeRefusal->id)) {
                $main['bgId'] = (string)$xml->bankGuaranteeRefusal->id;
            }
            if (isset($xml->bankGuaranteeRefusal->externalId)) {
                $main['externalId'] = (string)$xml->bankGuaranteeRefusal->externalId;
            }
            if (isset($xml->bankGuaranteeRefusal->docNumber)) {
                $main['docNumber'] = (string)$xml->bankGuaranteeRefusal->docNumber;
            }
            if (isset($xml->bankGuaranteeRefusal->docPublishDate)) {
                $main['docPublishDate'] = (string)$xml->bankGuaranteeRefusal->docPublishDate;
            }
            if (isset($xml->bankGuaranteeRefusal->href)) {
                $main['href'] = (string)$xml->bankGuaranteeRefusal->href;
            }

            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO main (
                          filename,
                          bg_id,
                          external_id,
                          doc_number,
                          doc_publish_date,
                          href
                      ) 
                      VALUES(
                          :filename,
                          :bgId,
                          :externalId,
                          :docNumber,
                          :docPublishDate,
                          :href
                      )                      
					 ON CONFLICT (bg_id) DO NOTHING 
                      ";

            $returningState = Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':filename' => $main['filename'],
                    ':bgId' => $main['bgId'],
                    ':externalId' => $main['externalId'],
                    ':docNumber' => $main['docNumber'],
                    ':docPublishDate' => $main['docPublishDate'],
                    ':href' => $main['href'],
                ])
                ->execute();

            $main['id'] = Yii::$app->db_bank_guarantee->getLastInsertID('main_id_seq');

            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
            } else {
                echo "Record with bg_id = " . $main['bgId'] . " exist." . "\n";
                return;
            }
        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }

        //-----------print_form
        $printForm = [
            'main_id' => $main['id'],
            'url' => '',
        ];

        if (isset($xml->bankGuaranteeRefusal->printForm->url)) {
            $printForm['url'] = (string)$xml->bankGuaranteeRefusal->printForm->url;
        }
        //*-----------print_form
        //-----------print_form
        $printFormCondition =
            $printForm['url'] !== '';

        if ($printFormCondition) {
            $sql = "
                      INSERT INTO print_form (
                          main_id,
                          url
                      ) 
                      VALUES(
                          :mainId,
                          :url
                      )";

            Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':mainId' => $printForm['main_id'],
                    ':url' => $printForm['url'],
                ])
                ->execute();
        }
        //*-----------print_form

        $legalFormBank = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeRefusal->bank->legalForm->code)) {
            $legalFormBank['code'] = (string)$xml->bankGuaranteeRefusal->bank->legalForm->code;
        }
        if (isset($xml->bankGuaranteeRefusal->bank->legalForm->singularName)) {
            $legalFormBank['singular_name'] = (string)$xml->bankGuaranteeRefusal->bank->legalForm->singularName;
        }

        $legalFormBank['id'] = self::getLegalFormId($legalFormBank);

        $legalFormLegalEntityRF = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->legalForm->code)) {
            $legalFormLegalEntityRF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->legalForm->code;
        }
        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->legalForm->singularName)) {
            $legalFormLegalEntityRF['singular_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->legalForm->singularName;
        }

        $legalFormLegalEntityRF['id'] = self::getLegalFormId($legalFormLegalEntityRF);

        $legalFormCustomer = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeRefusal->guarantee->customer->legalForm->code)) {
            $legalFormCustomer['code'] = (string)$xml->bankGuaranteeRefusal->guarantee->customer->legalForm->code;
        }
        if (isset($xml->bankGuaranteeRefusal->guarantee->customer->legalForm->singularName)) {
            $legalFormCustomer['singular_name'] = (string)$xml->bankGuaranteeRefusal->guarantee->customer->legalForm->singularName;
        }

        $legalFormCustomer['id'] = self::getLegalFormId($legalFormCustomer);

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF)) {
            $contactInfoIPRF = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->lastName)) {
                $contactInfoIPRF['last_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->lastName;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->firstName)) {
                $contactInfoIPRF['first_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->firstName;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->middleName)) {
                $contactInfoIPRF['middle_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->middleName;
            }

            $contactInfoIPRF['id'] = self::getContactInfoId($contactInfoIPRF);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState)) {
            $contactInfoIPFS = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->lastName)) {
                $contactInfoIPFS['last_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->lastName;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->firstName)) {
                $contactInfoIPFS['first_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->firstName;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->middleName)) {
                $contactInfoIPFS['middle_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->middleName;

            }

            $contactInfoIPFS['id'] = self::getContactInfoId($contactInfoIPFS);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF)) {
            $subjectRFPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code)) {
                $subjectRFPOSIRF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name)) {
                $subjectRFPOSIRF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name;
            }

            $subjectRFPOSIRF['id'] = self::getSubjectRFId($subjectRFPOSIRF);

        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->subjectRF)) {
            $subjectRFIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->subjectRF->code)) {
                $subjectRFIPRF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->subjectRF->name)) {
                $subjectRFIPRF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->subjectRF->name;
            }

            $subjectRFIPRF['id'] = self::getSubjectRFId($subjectRFIPRF);

        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->subjectRF)) {
            $subjectRFLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->subjectRF->code)) {
                $subjectRFLERF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->subjectRF->name)) {
                $subjectRFLERF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->subjectRF->name;
            }

            $subjectRFLERF['id'] = self::getSubjectRFId($subjectRFLERF);
        }

        if (isset($xml->bankGuaranteeRefusal->bank->subjectRF)) {
            $subjectRFBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->bank->subjectRF->code)) {
                $subjectRFBank['code'] = (string)$xml->bankGuaranteeRefusal->bank->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->subjectRF->name)) {
                $subjectRFBank['name'] = (string)$xml->bankGuaranteeRefusal->bank->subjectRF->name;
            }

            $subjectRFBank['id'] = self::getSubjectRFId($subjectRFBank);
        }

        if (isset($xml->bankGuaranteeRefusal->guarantee->customer->subjectRF)) {
            $subjectRFCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->subjectRF->code)) {
                $subjectRFCustomer['code'] = (string)$xml->bankGuaranteeRefusal->guarantee->customer->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->subjectRF->name)) {
                $subjectRFCustomer['name'] = (string)$xml->bankGuaranteeRefusal->guarantee->customer->subjectRF->name;
            }

            $subjectRFCustomer['id'] = self::getSubjectRFId($subjectRFCustomer);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKTMO)) {
            $oktmoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKTMO->code)) {
                $oktmoLERF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKTMO->name)) {
                $oktmoLERF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKTMO->name;
            }

            $oktmoLERF['id'] = self::getOktmoId($oktmoLERF);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO)) {
            $oktmoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code)) {
                $oktmoPOSIRF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name)) {
                $oktmoPOSIRF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name;
            }

            $oktmoPOSIRF['id'] = self::getOktmoId($oktmoPOSIRF);
        }

        if (isset($xml->bankGuaranteeRefusal->guarantee->customer->OKTMO)) {
            $oktmoCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->OKTMO->code)) {
                $oktmoCustomer['code'] = (string)$xml->bankGuaranteeRefusal->guarantee->customer->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->OKTMO->name)) {
                $oktmoCustomer['name'] = (string)$xml->bankGuaranteeRefusal->guarantee->customer->OKTMO->name;
            }

            $oktmoCustomer['id'] = self::getOktmoId($oktmoCustomer);
        }

        if (isset($xml->bankGuaranteeRefusal->bank->OKTMO)) {
            $oktmoBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->bank->OKTMO->code)) {
                $oktmoBank['code'] = (string)$xml->bankGuaranteeRefusal->bank->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->OKTMO->name)) {
                $oktmoBank['name'] = (string)$xml->bankGuaranteeRefusal->bank->OKTMO->name;
            }

            $oktmoBank['id'] = self::getOktmoId($oktmoBank);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKTMO)) {
            $oktmoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKTMO->code)) {
                $oktmoIPRF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKTMO->name)) {
                $oktmoIPRF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKTMO->name;
            }

            $oktmoIPRF['id'] = self::getOktmoId($oktmoIPRF);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO)) {
            $okatoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code)) {
                $okatoPOSIRF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name)) {
                $okatoPOSIRF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name;
            }

            $okatoPOSIRF['id'] = self::getOkatoId($okatoPOSIRF);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKATO)) {
            $okatoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKATO->code)) {
                $okatoLERF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKATO->name)) {
                $okatoLERF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OKATO->name;
            }

            $okatoLERF['id'] = self::getOkatoId($okatoLERF);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKATO)) {
            $okatoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKATO->code)) {
                $okatoIPRF['code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKATO->name)) {
                $okatoIPRF['name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OKATO->name;
            }

            $okatoIPRF['id'] = self::getOkatoId($okatoIPRF);
        }

        if (isset($xml->bankGuaranteeRefusal->guarantee->currency)) {
            $currencyGuarantee = [
                'id' => '',
                'code' => '',
                'digital_code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->guarantee->currency->code)) {
                $currencyGuarantee['code'] = (string)$xml->bankGuaranteeRefusal->guarantee->currency->code;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->currency->digitalCode)) {
                $currencyGuarantee['digital_code'] = (string)$xml->bankGuaranteeRefusal->guarantee->currency->digitalCode;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->currency->name)) {
                $currencyGuarantee['name'] = (string)$xml->bankGuaranteeRefusal->guarantee->currency->name;
            }

            $currencyGuarantee['id'] = self::getCurrencyId($currencyGuarantee);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->country)) {
            $countryIPFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->country->countryCode)) {
                $countryIPFS['country_code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->country->countryFullName)) {
                $countryIPFS['country_full_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->country->countryFullName;
            }

            $countryIPFS['id'] = self::getCountryId($countryIPFS);
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->country)) {
            $countryLEFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->country->countryCode)) {
                $countryLEFS['country_code'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->country->countryFullName)) {
                $countryLEFS['country_full_name'] = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->country->countryFullName;
            }

            $countryLEFS['id'] = self::getCountryId($countryLEFS);
        }

        if (isset($xml->bankGuaranteeRefusal->guarantee->customer)) {
            $customer = new Customer();

            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->regNum)) {
                $customer->reg_num = (string)$xml->bankGuaranteeRefusal->guarantee->customer->regNum;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->consRegistryNum)) {
                $customer->cons_registry_num = (string)$xml->bankGuaranteeRefusal->guarantee->customer->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->fullName)) {
                $customer->full_name = (string)$xml->bankGuaranteeRefusal->guarantee->customer->fullName;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->shortName)) {
                $customer->short_name = (string)$xml->bankGuaranteeRefusal->guarantee->customer->shortName;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->postAddress)) {
                $customer->post_address = (string)$xml->bankGuaranteeRefusal->guarantee->customer->postAddress;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->factAddress)) {
                $customer->fact_address = (string)$xml->bankGuaranteeRefusal->guarantee->customer->factAddress;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->INN)) {
                $customer->inn = (string)$xml->bankGuaranteeRefusal->guarantee->customer->INN;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->KPP)) {
                $customer->kpp = (string)$xml->bankGuaranteeRefusal->guarantee->customer->KPP;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->location)) {
                $customer->location = (string)$xml->bankGuaranteeRefusal->guarantee->customer->location;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->registrationDate)) {
                $customer->registration_date = (string)$xml->bankGuaranteeRefusal->guarantee->customer->registrationDate;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->customer->IKU)) {
                $customer->iku = (string)$xml->bankGuaranteeRefusal->guarantee->customer->IKU;
            }

            if (isset($legalFormCustomer) && !empty($legalFormCustomer['id'])) {
                $customer->legal_form_id = $legalFormCustomer['id'];
            }
            if (isset($subjectRFCustomer) && !empty($subjectRFCustomer['id'])) {
                $customer->subject_rf_id = $subjectRFCustomer['id'];
            }
            if (isset($oktmoCustomer) && !empty($oktmoCustomer['id'])) {
                $customer->oktmo_id = $oktmoCustomer['id'];
            }

            $customer->save();
        }

        if (isset($xml->bankGuaranteeRefusal->bank)) {
            $bank = new Bank();

            if (isset($xml->bankGuaranteeRefusal->bank->regNum)) {
                $bank->reg_num = (string)$xml->bankGuaranteeRefusal->bank->regNum;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->consRegistryNum)) {
                $bank->cons_registry_num = (string)$xml->bankGuaranteeRefusal->bank->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->fullName)) {
                $bank->full_name = (string)$xml->bankGuaranteeRefusal->bank->fullName;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->shortName)) {
                $bank->short_name = (string)$xml->bankGuaranteeRefusal->bank->shortName;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->postAddress)) {
                $bank->post_address = (string)$xml->bankGuaranteeRefusal->bank->postAddress;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->factAddress)) {
                $bank->fact_address = (string)$xml->bankGuaranteeRefusal->bank->factAddress;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->INN)) {
                $bank->inn = (string)$xml->bankGuaranteeRefusal->bank->INN;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->KPP)) {
                $bank->kpp = (string)$xml->bankGuaranteeRefusal->bank->KPP;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->location)) {
                $bank->location = (string)$xml->bankGuaranteeRefusal->bank->location;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->registrationDate)) {
                $bank->registration_date = (string)$xml->bankGuaranteeRefusal->bank->registrationDate;
            }
            if (isset($xml->bankGuaranteeRefusal->bank->IKU)) {
                $bank->iku = (string)$xml->bankGuaranteeRefusal->bank->IKU;
            }

            if (isset($main) && !empty($main['id'])) {
                $bank->main_id = $main['id'];
            }
            if (isset($legalFormBank) && !empty($legalFormBank['id'])) {
                $bank->legal_form_id = $legalFormBank['id'];
            }
            if (isset($subjectRFBank) && !empty($subjectRFBank['id'])) {
                $bank->subject_rf_id = $subjectRFBank['id'];
            }
            if (isset($oktmoBank) && !empty($oktmoBank['id'])) {
                $bank->oktmo_id = $oktmoBank['id'];
            }

            $bank->save();
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF)) {
            $legalEntityRF = new LegalEntityRf();

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->fullName)) {
                $legalEntityRF->full_name = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->fullName;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->shortName)) {
                $legalEntityRF->short_name = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->shortName;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->INN)) {
                $legalEntityRF->inn = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->INN;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->KPP)) {
                $legalEntityRF->kpp = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->KPP;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OGRN)) {
                $legalEntityRF->ogrn = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->OGRN;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->registrationDate)) {
                $legalEntityRF->registration_date = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->address)) {
                $legalEntityRF->address = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityRF->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityRF->main_id = $main['id'];
            }
            if (isset($legalFormLegalEntityRF) && !empty($legalFormLegalEntityRF['id'])) {
                $legalEntityRF->legal_form_id = $legalFormLegalEntityRF['id'];
            }
            if (isset($subjectRFLERF) && !empty($subjectRFLERF['id'])) {
                $legalEntityRF->subject_rf_id = $subjectRFLERF['id'];
            }
            if (isset($okatoLERF) && !empty($okatoLERF['id'])) {
                $legalEntityRF->okato_id = $okatoLERF['id'];
            }
            if (isset($oktmoLERF) && !empty($oktmoLERF['id'])) {
                $legalEntityRF->oktmo_id = $oktmoLERF['id'];
            }

            $legalEntityRF->save();
        }

        if (isset($xml->bankGuaranteeRefusal->guarantee)) {
            $guarantee = new \common\models\zakupki\guarantee\Guarantee();

            if (isset($xml->bankGuaranteeRefusal->guarantee->guaranteeDate)) {
                $guarantee->guarantee_date = (string)$xml->bankGuaranteeRefusal->guarantee->guaranteeDate;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->guaranteeNumber)) {
                $guarantee->guarantee_number = (string)$xml->bankGuaranteeRefusal->guarantee->guaranteeNumber;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->guaranteeAmount)) {
                $guarantee->guarantee_amount = (string)$xml->bankGuaranteeRefusal->guarantee->guaranteeAmount;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->expireDate)) {
                $guarantee->expire_date = (string)$xml->bankGuaranteeRefusal->guarantee->expireDate;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->entryForceDate)) {
                $guarantee->entry_force_date = (string)$xml->bankGuaranteeRefusal->guarantee->entryForceDate;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->procedure)) {
                $guarantee->guarantee_procedure = (string)$xml->bankGuaranteeRefusal->guarantee->procedure;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->guaranteeAmountRUR)) {
                $guarantee->guarantee_amount_rur = (string)$xml->bankGuaranteeRefusal->guarantee->guaranteeAmountRUR;
            }
            if (isset($xml->bankGuaranteeRefusal->guarantee->currencyRate)) {
                $guarantee->currency_rate = (string)$xml->bankGuaranteeRefusal->guarantee->currencyRate;
            }

            if (isset($main) && !empty($main['id'])) {
                $guarantee->main_id = $main['id'];
            }
            if (isset($purchaseRequestEnsure)) {
                $guarantee->purchase_request_ensure_id = $purchaseRequestEnsure->id;
            }

            if (isset($contractExecutionEnsure)) {
                $guarantee->contract_execution_ensure_id = $contractExecutionEnsure->id;
            }

            if (isset($customer)) {
                $guarantee->customer_id = $customer->id;
            }

            if (isset($currencyGuarantee) && !empty($currencyGuarantee['id'])) {
                $guarantee->currency_id = $currencyGuarantee['id'];
            }

            $guarantee->save();

            $guaranteeAdditionalInfo = new GuaranteeAdditionalInfo();

            if (isset($xml->bankGuaranteeRefusal->guaranteeInfo->guarantee->guaranteePublishDate)) {
                $guaranteeAdditionalInfo->guarantee_publish_date = (string)$xml->bankGuaranteeRefusal->guaranteeInfo->guarantee->guaranteePublishDate;
            }
            if (isset($xml->bankGuaranteeRefusal->guaranteeInfo->guarantee->creditOrgNumber)) {
                $guaranteeAdditionalInfo->guarantee_credit_org_number = (string)$xml->bankGuaranteeRefusal->guaranteeInfo->guarantee->creditOrgNumber;
            }
            if (isset($xml->bankGuaranteeRefusal->guaranteeInfo->guarantee->guaranteeGrantDate)) {
                $guaranteeAdditionalInfo->guarantee_grant_date = (string)$xml->bankGuaranteeRefusal->guaranteeInfo->guarantee->guaranteeGrantDate;
                $guaranteeAdditionalInfo->guarantee_id = $guarantee->id;
                $guaranteeAdditionalInfo->save();
            }
        }

        if (isset($xml->bankGuaranteeRefusal->guarantee->purchaseCodes)) {

            foreach ($xml->bankGuaranteeRefusal->guarantee->purchaseCodes as $pC) {
                $purchaseCode = new PurchaseCode();

                if (isset($guarantee)) {
                    $purchaseCode->guarantee_id = $guarantee->id;
                }
                if (isset($pC->purchaseCode)) {
                    $purchaseCode->purchase_code = (string)$pC->purchaseCode;
                }
                $purchaseCode->save();
            }
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF)) {
            $individualPersonRf = new IndividualPersonRf();

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->INN)) {
                $individualPersonRf->inn = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->INN;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OGRNIP)) {
                $individualPersonRf->ogrnip = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->OGRNIP;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->registrationDate)) {
                $individualPersonRf->registration_date = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->address)) {
                $individualPersonRf->address = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->address;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->isIP)) {
                $individualPersonRf->is_ip = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonRF->isIP;
            }

            if (isset($main) && !empty($main['id'])) {
                $individualPersonRf->main_id = $main['id'];
            }
            if (isset($contactInfoIPRF) && !empty($contactInfoIPRF['id'])) {
                $individualPersonRf->contact_info_id = $contactInfoIPRF['id'];
            }
            if (isset($subjectRFIPRF) && !empty($subjectRFIPRF['id'])) {
                $individualPersonRf->subject_rf_id = $subjectRFIPRF['id'];
            }
            if (isset($okatoIPRF) && !empty($okatoIPRF['id'])) {
                $individualPersonRf->okato_id = $okatoIPRF['id'];
            }
            if (isset($oktmoIPRF) && !empty($oktmoIPRF['id'])) {
                $individualPersonRf->oktmo_id = $oktmoIPRF['id'];
            }

            $individualPersonRf->save();
        }
//----------------------------------------------------------------------------------------------------------------------
        $bankGuaranteeRefusal = new BankGuaranteeRefusal();

        if (isset($xml->bankGuaranteeRefusal->bank->regNum)) {
            $bankGuaranteeRefusal->reg_number = (string)$xml->bankGuaranteeRefusal->bank->regNum;
        }
        if (isset($xml->bankGuaranteeRefusal->versionNumber)) {
            $bankGuaranteeRefusal->version_number = (string)$xml->bankGuaranteeRefusal->versionNumber;
        }
        if (isset($xml->bankGuaranteeRefusal->modificationInfo)) {
            $bankGuaranteeRefusal->modification_info = (string)$xml->bankGuaranteeRefusal->modificationInfo;
        }

        if (isset($main) && !empty($main['id'])) {
            $bankGuaranteeRefusal->main_id = $main['id'];
        }

        $bankGuaranteeRefusal->save();

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->registerInRFTaxBodies)) {
            $registerInRfTaxBodies = new RegisterInRfTaxBodies();

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN)) {
                $registerInRfTaxBodies->inn = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP)) {
                $registerInRfTaxBodies->kpp = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate)) {
                $registerInRfTaxBodies->registration_date = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate;
            }

            $registerInRfTaxBodies->save();
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState)) {
            $legalEntityForeignState = new LegalEntityForeignState();

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->fullName)) {
                $legalEntityForeignState->full_name = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->fullName;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->fullNameLat)) {
                $legalEntityForeignState->full_name_lat = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->fullNameLat;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->taxPayerCode)) {
                $legalEntityForeignState->tax_payer_code = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->address)) {
                $legalEntityForeignState->address = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityForeignState->main_id = $main['id'];
            }
            if (isset($countryLEFS) && !empty($countryLEFS['id'])) {
                $legalEntityForeignState->country_id = $countryLEFS['id'];
            }
            if (isset($registerInRfTaxBodies)) {
                $legalEntityForeignState->register_in_rf_tax_bodies_id = $registerInRfTaxBodies->id;
            }

            $legalEntityForeignState->save();
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF)) {

            $placeOfStayInRf = new PlaceOfStayInRf();

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->address)) {
                $placeOfStayInRf->address = (string)$xml->bankGuaranteeRefusal->supplierInfo->legalEntityForeignState->placeOfStayInRF->address;
            }

            if (isset($legalEntityForeignState)) {
                $placeOfStayInRf->legal_entity_foreign_state_id = $legalEntityForeignState->id;
            }
            if (isset($subjectRFPOSIRF) && !empty($subjectRFPOSIRF['id'])) {
                $placeOfStayInRf->subject_rf_id = $subjectRFPOSIRF['id'];
            }
            if (isset($okatoPOSIRF) && !empty($okatoPOSIRF['id'])) {
                $placeOfStayInRf->okato_id = $okatoPOSIRF['id'];
            }
            if (isset($oktmoPOSIRF) && !empty($oktmoPOSIRF['id'])) {
                $placeOfStayInRf->oktmo_id = $oktmoPOSIRF['id'];
            }

            $placeOfStayInRf->save();
        }

        if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState)) {
            $individualPersonForeignState = new IndividualPersonForeignState();

            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->lastNameLat)) {
                $individualPersonForeignState->last_name_lat = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->lastNameLat;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->firstNameLat)) {
                $individualPersonForeignState->first_name_lat = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->firstNameLat;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->middleNameLat)) {
                $individualPersonForeignState->middle_name_lat = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->middleNameLat;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->INN)) {
                $individualPersonForeignState->inn = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->INN;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->taxPayerCode)) {
                $individualPersonForeignState->tax_payer_code = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->address)) {
                $individualPersonForeignState->address = (string)$xml->bankGuaranteeRefusal->supplierInfo->individualPersonForeignState->address;
            }

            if (isset($countryIPFS) && !empty($countryIPFS['id'])) {
                $individualPersonForeignState->country_id = $countryIPFS['id'];
            }
            if (isset($contactInfoIPFS) && !empty($contactInfoIPFS['id'])) {
                $individualPersonForeignState->contact_info_id = $contactInfoIPFS['id'];
            }

            $individualPersonForeignState->save();
        }

        if (isset($xml->bankGuaranteeRefusal->placer)) {
            $placer = new Placer();
            if (isset($xml->bankGuaranteeRefusal->placer->responsibleOrg->regNum)) {
                $placer->responsible_org_reg_num = (string)$xml->bankGuaranteeRefusal->placer->responsibleOrg->regNum;
            }
            if (isset($xml->bankGuaranteeRefusal->placer->responsibleOrg->consRegistryNum)) {
                $placer->responsible_org_cons_registry_num = (string)$xml->bankGuaranteeRefusal->placer->responsibleOrg->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeRefusal->placer->responsibleOrg->fullName)) {
                $placer->responsible_org_full_name = (string)$xml->bankGuaranteeRefusal->placer->responsibleOrg->fullName;
            }
            if (isset($xml->bankGuaranteeRefusal->placer->responsibleRole)) {
                $placer->responsible_role = (string)$xml->bankGuaranteeRefusal->placer->responsibleRole;
            }

            if (isset($main) && !empty($main['id'])) {
                $placer->main_id = $main['id'];
            }

            $placer->save();
        }

        if (isset($xml->bankGuaranteeRefusal->refusalInfo)) {
            $refusalInfo = new RefusalInfo();
            if (isset($xml->bankGuaranteeRefusal->refusalInfo->docDate)) {
                $refusalInfo->doc_date = (string)$xml->bankGuaranteeRefusal->refusalInfo->docDate;
            }
            if (isset($xml->bankGuaranteeRefusal->refusalInfo->docNumber)) {
                $refusalInfo->doc_number = (string)$xml->bankGuaranteeRefusal->refusalInfo->docNumber;
            }
            if (isset($xml->bankGuaranteeRefusal->refusalInfo->docName)) {
                $refusalInfo->doc_name = (string)$xml->bankGuaranteeRefusal->refusalInfo->docName;
            }

            if (isset($main) && !empty($main['id'])) {
                $refusalInfo->main_id = $main['id'];
            }

            $refusalInfo->save();
        }

        if (isset($xml->bankGuaranteeRefusal->refusalInfo->refusalReasons)) {

            foreach ($xml->bankGuaranteeRefusal->refusalInfo->refusalReasons as $rR) {

                $refusalReason = new RefusalReason();
                var_dump($rR->RefusalReason);

                if (isset($main) && !empty($main['id'])) {
                    $refusalReason->main_id = $main['id'];
                }

                if (isset($rR->refusalReason->code)) {
                    $refusalReason->code = (string)$rR->refusalReason->code;
                }
                if (isset($rR->refusalReason->name)) {
                    $refusalReason->name = (string)$rR->refusalReason->name;
                }

                $refusalReason->save();
            }
        }
    }

    public static function parseGuaranteeTermination($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);

        if ($xml) {
            $main = [
                'id' => '',
                'filename' => basename($file),
                'bgId' => '',
                'externalId' => '',
                'docNumber' => '',
                'docPublishDate' => '',
                'href' => '',

            ];

            if (isset($xml->bankGuaranteeTermination->id)) {
                $main['bgId'] = (string)$xml->bankGuaranteeTermination->id;
            }
            if (isset($xml->bankGuaranteeTermination->externalId)) {
                $main['externalId'] = (string)$xml->bankGuaranteeTermination->externalId;
            }
            if (isset($xml->bankGuaranteeTermination->docNumber)) {
                $main['docNumber'] = (string)$xml->bankGuaranteeTermination->docNumber;
            }
            if (isset($xml->bankGuaranteeTermination->docPublishDate)) {
                $main['docPublishDate'] = (string)$xml->bankGuaranteeTermination->docPublishDate;
            }
            if (isset($xml->bankGuaranteeTermination->href)) {
                $main['href'] = (string)$xml->bankGuaranteeTermination->href;
            }

            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO main (
                          filename,
                          bg_id,
                          external_id,
                          doc_number,
                          doc_publish_date,
                          href
                      ) 
                      VALUES(
                          :filename,
                          :bgId,
                          :externalId,
                          :docNumber,
                          :docPublishDate,
                          :href
                      )                      
					 ON CONFLICT (bg_id) DO NOTHING 
                      ";

            $returningState = Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':filename' => $main['filename'],
                    ':bgId' => $main['bgId'],
                    ':externalId' => $main['externalId'],
                    ':docNumber' => $main['docNumber'],
                    ':docPublishDate' => $main['docPublishDate'],
                    ':href' => $main['href'],
                ])
                ->execute();

            $main['id'] = Yii::$app->db_bank_guarantee->getLastInsertID('main_id_seq');

            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
            } else {
                echo "Record with bg_id = " . $main['bgId'] . " exist." . "\n";
                return;
            }
        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }

        //-----------print_form
        $printForm = [
            'main_id' => $main['id'],
            'url' => '',
        ];

        if (isset($xml->bankGuaranteeTermination->printForm->url)) {
            $printForm['url'] = (string)$xml->bankGuaranteeTermination->printForm->url;
        }
        //*-----------print_form
        //-----------print_form
        $printFormCondition =
            $printForm['url'] !== '';

        if ($printFormCondition) {
            $sql = "
                      INSERT INTO print_form (
                          main_id,
                          url
                      ) 
                      VALUES(
                          :mainId,
                          :url
                      )";

            Yii::$app->db_bank_guarantee->createCommand($sql)
                ->bindValues([
                    ':mainId' => $printForm['main_id'],
                    ':url' => $printForm['url'],
                ])
                ->execute();
        }
        //*-----------print_form

        $legalFormBank = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeTermination->bank->legalForm->code)) {
            $legalFormBank['code'] = (string)$xml->bankGuaranteeTermination->bank->legalForm->code;
        }
        if (isset($xml->bankGuaranteeTermination->bank->legalForm->singularName)) {
            $legalFormBank['singular_name'] = (string)$xml->bankGuaranteeTermination->bank->legalForm->singularName;
        }

        $legalFormBank['id'] = self::getLegalFormId($legalFormBank);

        $legalFormLegalEntityRF = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->legalForm->code)) {
            $legalFormLegalEntityRF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->legalForm->code;
        }
        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->legalForm->singularName)) {
            $legalFormLegalEntityRF['singular_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->legalForm->singularName;
        }

        $legalFormLegalEntityRF['id'] = self::getLegalFormId($legalFormLegalEntityRF);

        $legalFormCustomer = [
            'id' => '',
            'code' => '',
            'singular_name' => '',
        ];

        if (isset($xml->bankGuaranteeTermination->guarantee->customer->legalForm->code)) {
            $legalFormCustomer['code'] = (string)$xml->bankGuaranteeTermination->guarantee->customer->legalForm->code;
        }
        if (isset($xml->bankGuaranteeTermination->guarantee->customer->legalForm->singularName)) {
            $legalFormCustomer['singular_name'] = (string)$xml->bankGuaranteeTermination->guarantee->customer->legalForm->singularName;
        }

        $legalFormCustomer['id'] = self::getLegalFormId($legalFormCustomer);

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF)) {
            $contactInfoIPRF = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->lastName)) {
                $contactInfoIPRF['last_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->lastName;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->firstName)) {
                $contactInfoIPRF['first_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->firstName;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->middleName)) {
                $contactInfoIPRF['middle_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->middleName;
            }

            $contactInfoIPRF['id'] = self::getContactInfoId($contactInfoIPRF);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState)) {
            $contactInfoIPFS = [
                'id' => '',
                'last_name' => '',
                'first_name' => '',
                'middle_name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->lastName)) {
                $contactInfoIPFS['last_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->lastName;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->firstName)) {
                $contactInfoIPFS['first_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->firstName;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->middleName)) {
                $contactInfoIPFS['middle_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->middleName;

            }

            $contactInfoIPFS['id'] = self::getContactInfoId($contactInfoIPFS);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF)) {
            $subjectRFPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code)) {
                $subjectRFPOSIRF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name)) {
                $subjectRFPOSIRF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->subjectRF->name;
            }

            $subjectRFPOSIRF['id'] = self::getSubjectRFId($subjectRFPOSIRF);

        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->subjectRF)) {
            $subjectRFIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->subjectRF->code)) {
                $subjectRFIPRF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->subjectRF->name)) {
                $subjectRFIPRF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->subjectRF->name;
            }

            $subjectRFIPRF['id'] = self::getSubjectRFId($subjectRFIPRF);

        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->subjectRF)) {
            $subjectRFLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->subjectRF->code)) {
                $subjectRFLERF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->subjectRF->name)) {
                $subjectRFLERF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->subjectRF->name;
            }

            $subjectRFLERF['id'] = self::getSubjectRFId($subjectRFLERF);
        }

        if (isset($xml->bankGuaranteeTermination->bank->subjectRF)) {
            $subjectRFBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->bank->subjectRF->code)) {
                $subjectRFBank['code'] = (string)$xml->bankGuaranteeTermination->bank->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeTermination->bank->subjectRF->name)) {
                $subjectRFBank['name'] = (string)$xml->bankGuaranteeTermination->bank->subjectRF->name;
            }

            $subjectRFBank['id'] = self::getSubjectRFId($subjectRFBank);
        }

        if (isset($xml->bankGuaranteeTermination->guarantee->customer->subjectRF)) {
            $subjectRFCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->guarantee->customer->subjectRF->code)) {
                $subjectRFCustomer['code'] = (string)$xml->bankGuaranteeTermination->guarantee->customer->subjectRF->code;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->subjectRF->name)) {
                $subjectRFCustomer['name'] = (string)$xml->bankGuaranteeTermination->guarantee->customer->subjectRF->name;
            }

            $subjectRFCustomer['id'] = self::getSubjectRFId($subjectRFCustomer);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKTMO)) {
            $oktmoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKTMO->code)) {
                $oktmoLERF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKTMO->name)) {
                $oktmoLERF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKTMO->name;
            }

            $oktmoLERF['id'] = self::getOktmoId($oktmoLERF);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO)) {
            $oktmoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code)) {
                $oktmoPOSIRF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name)) {
                $oktmoPOSIRF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKTMO->name;
            }

            $oktmoPOSIRF['id'] = self::getOktmoId($oktmoPOSIRF);
        }

        if (isset($xml->bankGuaranteeTermination->guarantee->customer->OKTMO)) {
            $oktmoCustomer = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->guarantee->customer->OKTMO->code)) {
                $oktmoCustomer['code'] = (string)$xml->bankGuaranteeTermination->guarantee->customer->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->OKTMO->name)) {
                $oktmoCustomer['name'] = (string)$xml->bankGuaranteeTermination->guarantee->customer->OKTMO->name;
            }

            $oktmoCustomer['id'] = self::getOktmoId($oktmoCustomer);
        }

        if (isset($xml->bankGuaranteeTermination->bank->OKTMO)) {
            $oktmoBank = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->bank->OKTMO->code)) {
                $oktmoBank['code'] = (string)$xml->bankGuaranteeTermination->bank->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeTermination->bank->OKTMO->name)) {
                $oktmoBank['name'] = (string)$xml->bankGuaranteeTermination->bank->OKTMO->name;
            }

            $oktmoBank['id'] = self::getOktmoId($oktmoBank);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKTMO)) {
            $oktmoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKTMO->code)) {
                $oktmoIPRF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKTMO->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKTMO->name)) {
                $oktmoIPRF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKTMO->name;
            }

            $oktmoIPRF['id'] = self::getOktmoId($oktmoIPRF);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO)) {
            $okatoPOSIRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code)) {
                $okatoPOSIRF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name)) {
                $okatoPOSIRF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->OKATO->name;
            }

            $okatoPOSIRF['id'] = self::getOkatoId($okatoPOSIRF);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKATO)) {
            $okatoLERF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKATO->code)) {
                $okatoLERF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKATO->name)) {
                $okatoLERF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OKATO->name;
            }

            $okatoLERF['id'] = self::getOkatoId($okatoLERF);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKATO)) {
            $okatoIPRF = [
                'id' => '',
                'code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKATO->code)) {
                $okatoIPRF['code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKATO->code;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKATO->name)) {
                $okatoIPRF['name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OKATO->name;
            }

            $okatoIPRF['id'] = self::getOkatoId($okatoIPRF);
        }

        if (isset($xml->bankGuaranteeTermination->guarantee->currency)) {
            $currencyGuarantee = [
                'id' => '',
                'code' => '',
                'digital_code' => '',
                'name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->guarantee->currency->code)) {
                $currencyGuarantee['code'] = (string)$xml->bankGuaranteeTermination->guarantee->currency->code;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->currency->digitalCode)) {
                $currencyGuarantee['digital_code'] = (string)$xml->bankGuaranteeTermination->guarantee->currency->digitalCode;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->currency->name)) {
                $currencyGuarantee['name'] = (string)$xml->bankGuaranteeTermination->guarantee->currency->name;
            }

            $currencyGuarantee['id'] = self::getCurrencyId($currencyGuarantee);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->country)) {
            $countryIPFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->country->countryCode)) {
                $countryIPFS['country_code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->country->countryFullName)) {
                $countryIPFS['country_full_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->country->countryFullName;
            }

            $countryIPFS['id'] = self::getCountryId($countryIPFS);
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->country)) {
            $countryLEFS = [
                'id' => '',
                'country_code' => '',
                'country_full_name' => '',
            ];

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->country->countryCode)) {
                $countryLEFS['country_code'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->country->countryCode;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->country->countryFullName)) {
                $countryLEFS['country_full_name'] = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->country->countryFullName;
            }

            $countryLEFS['id'] = self::getCountryId($countryLEFS);
        }

        if (isset($xml->bankGuaranteeTermination->guarantee->customer)) {
            $customer = new Customer();

            if (isset($xml->bankGuaranteeTermination->guarantee->customer->regNum)) {
                $customer->reg_num = (string)$xml->bankGuaranteeTermination->guarantee->customer->regNum;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->consRegistryNum)) {
                $customer->cons_registry_num = (string)$xml->bankGuaranteeTermination->guarantee->customer->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->fullName)) {
                $customer->full_name = (string)$xml->bankGuaranteeTermination->guarantee->customer->fullName;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->shortName)) {
                $customer->short_name = (string)$xml->bankGuaranteeTermination->guarantee->customer->shortName;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->postAddress)) {
                $customer->post_address = (string)$xml->bankGuaranteeTermination->guarantee->customer->postAddress;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->factAddress)) {
                $customer->fact_address = (string)$xml->bankGuaranteeTermination->guarantee->customer->factAddress;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->INN)) {
                $customer->inn = (string)$xml->bankGuaranteeTermination->guarantee->customer->INN;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->KPP)) {
                $customer->kpp = (string)$xml->bankGuaranteeTermination->guarantee->customer->KPP;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->location)) {
                $customer->location = (string)$xml->bankGuaranteeTermination->guarantee->customer->location;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->registrationDate)) {
                $customer->registration_date = (string)$xml->bankGuaranteeTermination->guarantee->customer->registrationDate;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->customer->IKU)) {
                $customer->iku = (string)$xml->bankGuaranteeTermination->guarantee->customer->IKU;
            }

            if (isset($legalFormCustomer) && !empty($legalFormCustomer['id'])) {
                $customer->legal_form_id = $legalFormCustomer['id'];
            }
            if (isset($subjectRFCustomer) && !empty($subjectRFCustomer['id'])) {
                $customer->subject_rf_id = $subjectRFCustomer['id'];
            }
            if (isset($oktmoCustomer) && !empty($oktmoCustomer['id'])) {
                $customer->oktmo_id = $oktmoCustomer['id'];
            }

            $customer->save();
        }

        if (isset($xml->bankGuaranteeTermination->bank)) {
            $bank = new Bank();

            if (isset($xml->bankGuaranteeTermination->bank->regNum)) {
                $bank->reg_num = (string)$xml->bankGuaranteeTermination->bank->regNum;
            }
            if (isset($xml->bankGuaranteeTermination->bank->consRegistryNum)) {
                $bank->cons_registry_num = (string)$xml->bankGuaranteeTermination->bank->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeTermination->bank->fullName)) {
                $bank->full_name = (string)$xml->bankGuaranteeTermination->bank->fullName;
            }
            if (isset($xml->bankGuaranteeTermination->bank->shortName)) {
                $bank->short_name = (string)$xml->bankGuaranteeTermination->bank->shortName;
            }
            if (isset($xml->bankGuaranteeTermination->bank->postAddress)) {
                $bank->post_address = (string)$xml->bankGuaranteeTermination->bank->postAddress;
            }
            if (isset($xml->bankGuaranteeTermination->bank->factAddress)) {
                $bank->fact_address = (string)$xml->bankGuaranteeTermination->bank->factAddress;
            }
            if (isset($xml->bankGuaranteeTermination->bank->INN)) {
                $bank->inn = (string)$xml->bankGuaranteeTermination->bank->INN;
            }
            if (isset($xml->bankGuaranteeTermination->bank->KPP)) {
                $bank->kpp = (string)$xml->bankGuaranteeTermination->bank->KPP;
            }
            if (isset($xml->bankGuaranteeTermination->bank->location)) {
                $bank->location = (string)$xml->bankGuaranteeTermination->bank->location;
            }
            if (isset($xml->bankGuaranteeTermination->bank->registrationDate)) {
                $bank->registration_date = (string)$xml->bankGuaranteeTermination->bank->registrationDate;
            }
            if (isset($xml->bankGuaranteeTermination->bank->IKU)) {
                $bank->iku = (string)$xml->bankGuaranteeTermination->bank->IKU;
            }

            if (isset($main) && !empty($main['id'])) {
                $bank->main_id = $main['id'];
            }
            if (isset($legalFormBank) && !empty($legalFormBank['id'])) {
                $bank->legal_form_id = $legalFormBank['id'];
            }
            if (isset($subjectRFBank) && !empty($subjectRFBank['id'])) {
                $bank->subject_rf_id = $subjectRFBank['id'];
            }
            if (isset($oktmoBank) && !empty($oktmoBank['id'])) {
                $bank->oktmo_id = $oktmoBank['id'];
            }

            $bank->save();
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF)) {
            $legalEntityRF = new LegalEntityRf();

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->fullName)) {
                $legalEntityRF->full_name = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->fullName;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->shortName)) {
                $legalEntityRF->short_name = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->shortName;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->INN)) {
                $legalEntityRF->inn = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->INN;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->KPP)) {
                $legalEntityRF->kpp = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->KPP;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OGRN)) {
                $legalEntityRF->ogrn = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->OGRN;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->registrationDate)) {
                $legalEntityRF->registration_date = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->address)) {
                $legalEntityRF->address = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityRF->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityRF->main_id = $main['id'];
            }
            if (isset($legalFormLegalEntityRF) && !empty($legalFormLegalEntityRF['id'])) {
                $legalEntityRF->legal_form_id = $legalFormLegalEntityRF['id'];
            }
            if (isset($subjectRFLERF) && !empty($subjectRFLERF['id'])) {
                $legalEntityRF->subject_rf_id = $subjectRFLERF['id'];
            }
            if (isset($okatoLERF) && !empty($okatoLERF['id'])) {
                $legalEntityRF->okato_id = $okatoLERF['id'];
            }
            if (isset($oktmoLERF) && !empty($oktmoLERF['id'])) {
                $legalEntityRF->oktmo_id = $oktmoLERF['id'];
            }

            $legalEntityRF->save();
        }

        if (isset($xml->bankGuaranteeTermination->guarantee)) {
            $guarantee = new \common\models\zakupki\guarantee\Guarantee();

            if (isset($xml->bankGuaranteeTermination->guarantee->guaranteeDate)) {
                $guarantee->guarantee_date = (string)$xml->bankGuaranteeTermination->guarantee->guaranteeDate;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->guaranteeNumber)) {
                $guarantee->guarantee_number = (string)$xml->bankGuaranteeTermination->guarantee->guaranteeNumber;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->guaranteeAmount)) {
                $guarantee->guarantee_amount = (string)$xml->bankGuaranteeTermination->guarantee->guaranteeAmount;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->expireDate)) {
                $guarantee->expire_date = (string)$xml->bankGuaranteeTermination->guarantee->expireDate;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->entryForceDate)) {
                $guarantee->entry_force_date = (string)$xml->bankGuaranteeTermination->guarantee->entryForceDate;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->procedure)) {
                $guarantee->guarantee_procedure = (string)$xml->bankGuaranteeTermination->guarantee->procedure;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->guaranteeAmountRUR)) {
                $guarantee->guarantee_amount_rur = (string)$xml->bankGuaranteeTermination->guarantee->guaranteeAmountRUR;
            }
            if (isset($xml->bankGuaranteeTermination->guarantee->currencyRate)) {
                $guarantee->currency_rate = (string)$xml->bankGuaranteeTermination->guarantee->currencyRate;
            }

            if (isset($main) && !empty($main['id'])) {
                $guarantee->main_id = $main['id'];
            }
            if (isset($purchaseRequestEnsure)) {
                $guarantee->purchase_request_ensure_id = $purchaseRequestEnsure->id;
            }

            if (isset($contractExecutionEnsure)) {
                $guarantee->contract_execution_ensure_id = $contractExecutionEnsure->id;
            }

            if (isset($customer)) {
                $guarantee->customer_id = $customer->id;
            }

            if (isset($currencyGuarantee) && !empty($currencyGuarantee['id'])) {
                $guarantee->currency_id = $currencyGuarantee['id'];
            }

            $guarantee->save();

            $guaranteeAdditionalInfo = new GuaranteeAdditionalInfo();

            if (isset($xml->bankGuaranteeTermination->guaranteeInfo->guarantee->guaranteePublishDate)) {
                $guaranteeAdditionalInfo->guarantee_publish_date = (string)$xml->bankGuaranteeTermination->guaranteeInfo->guarantee->guaranteePublishDate;
            }
            if (isset($xml->bankGuaranteeRefusal->guaranteeInfo->guarantee->creditOrgNumber)) {
                $guaranteeAdditionalInfo->guarantee_credit_org_number = (string)$xml->bankGuaranteeTermination->guaranteeInfo->guarantee->creditOrgNumber;
            }
            if (isset($xml->bankGuaranteeTermination->guaranteeInfo->guarantee->guaranteeGrantDate)) {
                $guaranteeAdditionalInfo->guarantee_grant_date = (string)$xml->bankGuaranteeTermination->guaranteeInfo->guarantee->guaranteeGrantDate;
                $guaranteeAdditionalInfo->guarantee_id = $guarantee->id;
                $guaranteeAdditionalInfo->save();
            }
        }

        if (isset($xml->bankGuaranteeTermination->guarantee->purchaseCodes)) {

            foreach ($xml->bankGuaranteeTermination->guarantee->purchaseCodes as $pC) {
                $purchaseCode = new PurchaseCode();

                if (isset($guarantee)) {
                    $purchaseCode->guarantee_id = $guarantee->id;
                }
                if (isset($pC->purchaseCode)) {
                    $purchaseCode->purchase_code = (string)$pC->purchaseCode;
                }
                $purchaseCode->save();
            }
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF)) {
            $individualPersonRf = new IndividualPersonRf();

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->INN)) {
                $individualPersonRf->inn = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->INN;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OGRNIP)) {
                $individualPersonRf->ogrnip = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->OGRNIP;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->registrationDate)) {
                $individualPersonRf->registration_date = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->registrationDate;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->address)) {
                $individualPersonRf->address = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->address;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->isIP)) {
                $individualPersonRf->is_ip = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonRF->isIP;
            }

            if (isset($main) && !empty($main['id'])) {
                $individualPersonRf->main_id = $main['id'];
            }
            if (isset($contactInfoIPRF) && !empty($contactInfoIPRF['id'])) {
                $individualPersonRf->contact_info_id = $contactInfoIPRF['id'];
            }
            if (isset($subjectRFIPRF) && !empty($subjectRFIPRF['id'])) {
                $individualPersonRf->subject_rf_id = $subjectRFIPRF['id'];
            }
            if (isset($okatoIPRF) && !empty($okatoIPRF['id'])) {
                $individualPersonRf->okato_id = $okatoIPRF['id'];
            }
            if (isset($oktmoIPRF) && !empty($oktmoIPRF['id'])) {
                $individualPersonRf->oktmo_id = $oktmoIPRF['id'];
            }

            $individualPersonRf->save();
        }
//----------------------------------------------------------------------------------------------------------------------
        $bankGuaranteeTermination = new BankGuaranteeTermination();

        if (isset($xml->bankGuaranteeTermination->bank->regNum)) {
            $bankGuaranteeTermination->reg_number = (string)$xml->bankGuaranteeTermination->bank->regNum;
        }
        if (isset($xml->bankGuaranteeTermination->versionNumber)) {
            $bankGuaranteeTermination->version_number = (string)$xml->bankGuaranteeTermination->versionNumber;
        }
        if (isset($xml->bankGuaranteeTermination->regNum)) {
            $bankGuaranteeTermination->reg_num = (string)$xml->bankGuaranteeTermination->regNum;
        }
        if (isset($xml->bankGuaranteeTermination->modificationInfo)) {
            $bankGuaranteeTermination->modification_info = (string)$xml->bankGuaranteeTermination->modificationInfo;
        }
        if (isset($main) && !empty($main['id'])) {
            $bankGuaranteeTermination->main_id = $main['id'];
        }

        $bankGuaranteeTermination->save();

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->registerInRFTaxBodies)) {
            $registerInRfTaxBodies = new RegisterInRfTaxBodies();

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN)) {
                $registerInRfTaxBodies->inn = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->INN;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP)) {
                $registerInRfTaxBodies->kpp = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->KPP;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate)) {
                $registerInRfTaxBodies->registration_date = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->registerInRFTaxBodies->registrationDate;
            }

            $registerInRfTaxBodies->save();
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState)) {
            $legalEntityForeignState = new LegalEntityForeignState();

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->fullName)) {
                $legalEntityForeignState->full_name = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->fullName;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->fullNameLat)) {
                $legalEntityForeignState->full_name_lat = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->fullNameLat;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->taxPayerCode)) {
                $legalEntityForeignState->tax_payer_code = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->address)) {
                $legalEntityForeignState->address = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->address;
            }

            if (isset($main) && !empty($main['id'])) {
                $legalEntityForeignState->main_id = $main['id'];
            }
            if (isset($countryLEFS) && !empty($countryLEFS['id'])) {
                $legalEntityForeignState->country_id = $countryLEFS['id'];
            }
            if (isset($registerInRfTaxBodies)) {
                $legalEntityForeignState->register_in_rf_tax_bodies_id = $registerInRfTaxBodies->id;
            }

            $legalEntityForeignState->save();
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF)) {

            $placeOfStayInRf = new PlaceOfStayInRf();

            if (isset($xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->address)) {
                $placeOfStayInRf->address = (string)$xml->bankGuaranteeTermination->supplierInfo->legalEntityForeignState->placeOfStayInRF->address;
            }

            if (isset($legalEntityForeignState)) {
                $placeOfStayInRf->legal_entity_foreign_state_id = $legalEntityForeignState->id;
            }
            if (isset($subjectRFPOSIRF) && !empty($subjectRFPOSIRF['id'])) {
                $placeOfStayInRf->subject_rf_id = $subjectRFPOSIRF['id'];
            }
            if (isset($okatoPOSIRF) && !empty($okatoPOSIRF['id'])) {
                $placeOfStayInRf->okato_id = $okatoPOSIRF['id'];
            }
            if (isset($oktmoPOSIRF) && !empty($oktmoPOSIRF['id'])) {
                $placeOfStayInRf->oktmo_id = $oktmoPOSIRF['id'];
            }

            $placeOfStayInRf->save();
        }

        if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState)) {
            $individualPersonForeignState = new IndividualPersonForeignState();

            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->lastNameLat)) {
                $individualPersonForeignState->last_name_lat = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->lastNameLat;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->firstNameLat)) {
                $individualPersonForeignState->first_name_lat = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->firstNameLat;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->middleNameLat)) {
                $individualPersonForeignState->middle_name_lat = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->middleNameLat;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->INN)) {
                $individualPersonForeignState->inn = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->INN;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->taxPayerCode)) {
                $individualPersonForeignState->tax_payer_code = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->taxPayerCode;
            }
            if (isset($xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->address)) {
                $individualPersonForeignState->address = (string)$xml->bankGuaranteeTermination->supplierInfo->individualPersonForeignState->address;
            }

            if (isset($countryIPFS) && !empty($countryIPFS['id'])) {
                $individualPersonForeignState->country_id = $countryIPFS['id'];
            }
            if (isset($contactInfoIPFS) && !empty($contactInfoIPFS['id'])) {
                $individualPersonForeignState->contact_info_id = $contactInfoIPFS['id'];
            }

            $individualPersonForeignState->save();
        }

        if (isset($xml->bankGuaranteeTermination->placer)) {
            $placer = new Placer();
            if (isset($xml->bankGuaranteeTermination->placer->responsibleOrg->regNum)) {
                $placer->responsible_org_reg_num = (string)$xml->bankGuaranteeTermination->placer->responsibleOrg->regNum;
            }
            if (isset($xml->bankGuaranteeTermination->placer->responsibleOrg->consRegistryNum)) {
                $placer->responsible_org_cons_registry_num = (string)$xml->bankGuaranteeTermination->placer->responsibleOrg->consRegistryNum;
            }
            if (isset($xml->bankGuaranteeTermination->placer->responsibleOrg->fullName)) {
                $placer->responsible_org_full_name = (string)$xml->bankGuaranteeTermination->placer->responsibleOrg->fullName;
            }
            if (isset($xml->bankGuaranteeTermination->placer->responsibleRole)) {
                $placer->responsible_role = (string)$xml->bankGuaranteeTermination->placer->responsibleRole;
            }

            if (isset($main) && !empty($main['id'])) {
                $placer->main_id = $main['id'];
            }

            $placer->save();
        }

        if (isset($xml->bankGuaranteeTermination->bankGuaranteeTermination)) {
            $guaranteeTermination = new GuaranteeTermination();

            if (isset($xml->bankGuaranteeTermination->bankGuaranteeTermination->regNumber)) {
                $guaranteeTermination->reg_number = (string)$xml->bankGuaranteeTermination->bankGuaranteeTermination->regNumber;
            }
            if (isset($xml->bankGuaranteeTermination->bankGuaranteeTermination->docNumber)) {
                $guaranteeTermination->doc_number = (string)$xml->bankGuaranteeTermination->bankGuaranteeTermination->docNumber;
            }
            if (isset($xml->bankGuaranteeTermination->bankGuaranteeTermination->terminationDate)) {
                $guaranteeTermination->termination_date = (string)$xml->bankGuaranteeTermination->bankGuaranteeTermination->terminationDate;
            }
            if (isset($xml->bankGuaranteeTermination->bankGuaranteeTermination->terminationReason)) {
                $guaranteeTermination->termination_reason = (string)$xml->bankGuaranteeTermination->bankGuaranteeTermination->terminationReason;
            }

            $guaranteeTermination->save();
        }
    }

    public static function getCountryId(array $country)
    {
        //условие для проверки, были ли заполнены необходимые поля
        $countryCondition =
            $country['country_code'] !== '' ||
            $country['country_full_name'] !== '';

        if ($countryCondition) {
            $result =
                //Проверка существования записи в БД
                Country::find()
                    ->where("country_code=:country_code", [":country_code" => $country['country_code']])
                    ->one();
            if (!$result) {
                $sql = "
                  INSERT INTO country (
                    country_code,
                    country_full_name
                  ) 
                  VALUES(
                    :countryCode,
                    :countryFullName
                )
                ON CONFLICT (country_code) DO NOTHING";
                Yii::$app->db_bank_guarantee->createCommand($sql)
                    ->bindValues([
                        ':countryCode' => $country['country_code'],
                        ':countryFullName' => $country['country_full_name'],
                    ])
                    ->execute();

                $lastInsertId = Yii::$app->db_bank_guarantee->getLastInsertID('country_id_seq');

                var_dump("такой записи еще нет country " . $result);
                return $lastInsertId;
            } else {
                var_dump("EXIST country id =" . $result->id);
                return $result->id;
            }
        }
        var_dump('country' . "\nError in country\n");
        return '';
    }

    public static function getCurrencyId(array $currency)
    {
        //условие для проверки, были ли заполнены необходимые поля
        $currencyCondition =
            $currency['name'] !== '' ||
            $currency['digital_code'] !== '' ||
            $currency['code'] !== '';

        if ($currencyCondition) {
            $result =
                //Проверка существования записи в БД
                Currency::find()
                    ->where("code=:code", [":code" => $currency['code']])
                    //->andWhere("digital_code=:digital_code", [":digital_code" => $currency['digital_code']])
                    //->andWhere("name=:name", [":name" => $currency['name']])
                    ->one();
            if (!$result) {
                $sql = "
                  INSERT INTO currency (
                    code,
                    digital_code,
                    name
                  ) 
                  VALUES(
                    :code,
                    :digitalCode,
                    :name
                )
                ON CONFLICT (code) DO NOTHING";
                Yii::$app->db_bank_guarantee->createCommand($sql)
                    ->bindValues([
                        ':code' => $currency['code'],
                        ':digitalCode' => $currency['digital_code'],
                        ':name' => $currency['name'],
                    ])
                    ->execute();

                $lastInsertId = Yii::$app->db_bank_guarantee->getLastInsertID('currency_id_seq');

                //var_dump("такой записи еще нет currency " . $result);
                return $lastInsertId;
            } else {
                //var_dump("EXIST currency id =" . $result->id);
                return $result->id;
            }
        }
        //var_dump('currency' . "\nError in currency\n");
        return '';
    }

    public static function getOkatoId(array $okato)
    {
        //var_dump($okato);
        //die;

        //условие для проверки, были ли заполнены необходимые поля
        $okatoCondition =
            $okato['name'] !== '' ||
            $okato['code'] !== '';

        if ($okatoCondition) {
            $result =
                //Проверка существования записи в БД
                Okato::find()
                    ->where("code=:code", [":code" => $okato['code']])
                    ->andWhere("name=:name", [":name" => $okato['name']])
                    ->one();
            if (!$result) {
                $sql = "
                  INSERT INTO okato (
                    code,
                    name
                  ) 
                  VALUES(
                    :code,
                    :name
                )
                ON CONFLICT (code) DO NOTHING";
                Yii::$app->db_bank_guarantee->createCommand($sql)
                    ->bindValues([
                        ':code' => $okato['code'],
                        ':name' => $okato['name'],
                    ])
                    ->execute();

                $lastInsertId = Yii::$app->db_bank_guarantee->getLastInsertID('okato_id_seq');

                //var_dump("такой записи еще нет oktmo " . $result);
                return $lastInsertId;
            } else {
                //var_dump("EXIST okato id =" . $result->id);
                return $result->id;
            }
        }
        //var_dump('$okato' . "\nError in okato\n");
        return '';
    }

    public static function getOktmoId(array $oktmo)
    {
        //var_dump($oktmo);
        //die;

        //условие для проверки, были ли заполнены необходимые поля
        $oktmoCondition =
            $oktmo['name'] !== '' ||
            $oktmo['code'] !== '';

        if ($oktmoCondition) {
            $result =
                //Проверка существования записи в БД
                Oktmo::find()
                    ->where("code=:code", [":code" => $oktmo['code']])
                    ->andWhere("name=:name", [":name" => $oktmo['name']])
                    ->one();
            if (!$result) {
                $sql = "
                  INSERT INTO oktmo (
                    code,
                    name
                  ) 
                  VALUES(
                    :code,
                    :name
                )
                ON CONFLICT (code) DO NOTHING";
                Yii::$app->db_bank_guarantee->createCommand($sql)
                    ->bindValues([
                        ':code' => $oktmo['code'],
                        ':name' => $oktmo['name'],
                    ])
                    ->execute();

                $lastInsertId = Yii::$app->db_bank_guarantee->getLastInsertID('oktmo_id_seq');

                //var_dump("такой записи еще нет oktmo " . $result);
                return $lastInsertId;
            } else {
                //var_dump("EXIST oktmo id =" . $result->id);
                return $result->id;
            }
        }
        //var_dump('$oktmo' . "\nError in oktmo\n");
        return '';
    }

    public static function getSubjectRFId(array $subjectRF)
    {
        //var_dump($subjectRF);
        //die;

        //условие для проверки, были ли заполнены необходимые поля
        $subjectRFCondition =
            $subjectRF['name'] !== '' ||
            $subjectRF['code'] !== '';

        if ($subjectRFCondition) {
            $result =
                //Проверка существования записи в БД
                SubjectRf::find()
                    ->where("code=:code", [":code" => $subjectRF['code']])
                    ->andWhere("name=:name", [":name" => $subjectRF['name']])
                    ->one();
            if (!$result) {
                $sql = "
                  INSERT INTO subject_rf (
                    code,
                    name
                  ) 
                  VALUES(
                    :code,
                    :name
                )
                ON CONFLICT (code) DO NOTHING";
                Yii::$app->db_bank_guarantee->createCommand($sql)
                    ->bindValues([
                        ':code' => $subjectRF['code'],
                        ':name' => $subjectRF['name'],
                    ])
                    ->execute();

                $lastInsertId = Yii::$app->db_bank_guarantee->getLastInsertID('subject_rf_id_seq');

                //var_dump("такой записи еще нет subjectRF " . $result);
                return $lastInsertId;
            } else {
                //var_dump("EXIST subjectRF id =" . $result->id);
                return $result->id;
            }
        }
        //var_dump($subjectRF['last_name' . "\nError in Contact Info\n"]);
        return '';
    }

    public static function getContactInfoId(array $contactInfo)
    {
        //var_dump($contactInfo);
        //die;

        //условие для проверки, были ли заполнены необходимые поля
        $contactInfoCondition =
            $contactInfo['last_name'] !== '' ||
            $contactInfo['first_name'] !== '' ||
            $contactInfo['middle_name'] !== '';

        if ($contactInfoCondition) {


            $result =
                //Проверка существования записи в БД
                ContactInfo::find()
                    ->where("last_name=:last_name", [":last_name" => $contactInfo['last_name']])
                    ->andWhere("first_name=:first_name", [":first_name" => $contactInfo['first_name']])
                    ->andWhere("middle_name=:middle_name", [":middle_name" => $contactInfo['middle_name']])
                    ->one();
            if (!$result) {
                $sql = "
                  INSERT INTO contact_info (
                    last_name,
                    first_name,
                    middle_name
                  ) 
                  VALUES(
                    :lastName,
                    :firstName,
                    :middleName
                )
                --ON CONFLICT (code) DO NOTHING";
                Yii::$app->db_bank_guarantee->createCommand($sql)
                    ->bindValues([
                        ':lastName' => $contactInfo['last_name'],
                        ':firstName' => $contactInfo['first_name'],
                        ':middleName' => $contactInfo['middle_name'],
                    ])
                    ->execute();

                $lastInsertId = Yii::$app->db_bank_guarantee->getLastInsertID('contact_info_id_seq');

                //var_dump("такой записи еще нет contactInfo " . $result);
                return $lastInsertId;
            } else {
                //var_dump("EXIST contactInfo id =" . $result->id);
                return $result->id;
            }
        }
        //var_dump($contactInfo['last_name' . "\nError in Contact Info\n"]);
        return '';
    }

    public static function getLegalFormId(array $legalForm)
    {
        //условие для проверки, были ли заполнены необходимые поля
        $legalFormCondition =
            //$legalForm['code'] !== '' ||
            $legalForm['singular_name'] !== '';

        if ($legalFormCondition) {
            $result =
                //Проверка существования записи в БД
                LegalForm::find()
                    ->where("code=:code", [":code" => $legalForm['code']])
                    ->one();
            if (!$result) {
                $sql = "
                  INSERT INTO legal_form (
                    code,
                    singular_name
                  ) 
                  VALUES(
                    :code,
                    :singularName
                )
                ON CONFLICT (code) DO NOTHING";
                Yii::$app->db_bank_guarantee->createCommand($sql)
                    ->bindValues([
                        ':code' => $legalForm['code'],
                        ':singularName' => $legalForm['singular_name'],
                    ])
                    ->execute();

                $lastInsertId = Yii::$app->db_bank_guarantee->getLastInsertID('legal_form_id_seq');

                //var_dump("такой записи еще нет legalForm " . $result);
                return $lastInsertId;
            } else {
                //var_dump("EXIST legalForm id =" . $result->id);
                return $result->id;
            }
        }
        //var_dump($legalForm['singular_name'."\nError in Legal Form\n"]);
        return '';
    }

    //TODO: заблокировать эту функцию на продакшн
    //Функция очистки данных из БД (сбрасывает счетчик на 0)
    public static function clearDb()
    {
        Yii::$app->db_bank_guarantee->createCommand("
            TRUNCATE 
                main,
                legal_form,
                currency,
                oktmo,
                okato,
                subject_rf,
                contact_info,
                country,
                contract_execution_ensure,
                customer,
                individual_person_foreign_state,
                placing_org,
                purchase_request_ensure,
                register_in_rf_tax_bodies
            RESTART IDENTITY CASCADE
            ")->execute();
        echo "TRUNCATE COMPLETE.Deleted all records." . PHP_EOL;
    }

    //приведение xml файла к валидной форме
    public static function normalizeXml($contentString)
    {
        $processedString = str_replace('oos:', "", $contentString);
        $processedString = str_replace('ns2:', "", $processedString);
        $processedString = str_replace('ns3:', "", $processedString);
        $processedString = str_replace('ns4:', "", $processedString);
        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
            LIBXML_NOCDATA | LIBXML_NOBLANKS);
        return $xml;
    }

    public static function decomposeFile($dirBuffer, $extractedPath, $file, $name)
    {
        $dir = $extractedPath . $name . '/';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        copy($dirBuffer . $file, $dir . $file);
        unlink($dirBuffer . $file);
    }

    public static function downloadFromFtpByBanks($bank, $connect, $contents, $pathForSavingZipFiles)
    {
        $zipSkippedCounter = 0;
        $zipEmptyCounter = 0;
        $zipNewCounter = 0;

        print_r("\nStart parsing as zip: " . $bank . " (current month)");

        foreach ($contents as $content) {
            if (ftp_size($connect, $content) < self::EMPTY_FILE_SIZE) {
                $zipEmptyCounter++;
                continue;
            };

            $local_file = $pathForSavingZipFiles . basename($content);
            if (file_exists($local_file)) {
                $zipSkippedCounter++;
                continue;
            }
            $handle = fopen($local_file, 'w');

            if (ftp_fget($connect, $handle, $content, FTP_BINARY, 0)) {
            } else {
                echo "При скачке $content в $local_file произошла проблема\n";
            }
            $zipNewCounter++;
            if ($zipNewCounter % 10 == 0) {
                echo "\n    Downloaded " . $zipNewCounter . " new files";
            }
            fclose($handle);
        }

        print_r("\n     Skipped file(s): " . $zipSkippedCounter .
            "\n     Empty file(s): " . $zipEmptyCounter .
            "\n     New file(s): " . $zipNewCounter .
            "\nParsing completed: " . $bank . "\n");
    }

    public static function downloadGuarantees()
    {
        $user = self::FTP_USER;
        $password = self::FTP_PASSWORD;
        $host = self::FTP_HOST;

        $connect = ftp_connect($host);
        $result = ftp_login($connect, $user, $password);

        if (!is_dir(self::LOCAL_GUARANTEE_PATH)) {
            mkdir(self::LOCAL_GUARANTEE_PATH);
        }

        $pathForSavingZipFiles = self::LOCAL_GUARANTEE_PATH . self::LOCAL_ZIP_PATH;
        if (!is_dir($pathForSavingZipFiles)) {
            mkdir($pathForSavingZipFiles);
        }

        if (!$result) {
            exit("Can not connect");
        }

        //$counter = 0;
        $banks = [];
        $contents = ftp_nlist($connect, "/fcs_banks/");
        foreach ($contents as $content) {
            //Все файлы с фтп, которые не имею расширения(только папки)
            if (pathinfo($content, PATHINFO_EXTENSION) === "") {
                if (preg_match('([0-9]{11})', $content)) {
                    array_push($banks, $content);
                    //$counter++;
                }
            }
        }

        foreach ($banks as $bank) {
            //Текущий банк
            $bankZips = ftp_nlist($connect, $bank . "/currMonth");
            Guarantee::downloadFromFtpByBanks($bank, $connect, $bankZips, $pathForSavingZipFiles);
        }
        ftp_close($connect);
    }

    public static function unzip($pathToZip, $extractedPath, $buffer)
    {
        if (!is_dir($extractedPath)) {
            mkdir($extractedPath);
        }

        $counter = PHP_INT_MAX;
        $dirBuffer = $extractedPath . $buffer;

        $startPositionZip = 0;
        $zipCounter = 0 + $startPositionZip;
        //пропуск нескольких zip

        //2 this is . & ..
        $totalZipCounter = count(scandir($pathToZip)) - 2;
        if ($handle = opendir($pathToZip)) {
            while (false !== ($content = readdir($handle)) && $counter > 0) {
                $counter--;
                if ($content != "." && $content != ".." && $content != "extracted" && $content != "zip") {
                    $local_file = $pathToZip . basename($content);

                    if ($startPositionZip > 0) {
                        echo "\nПропущено: " . $startPositionZip;
                        $startPositionZip--;
                        continue;
                    }

                    $zip = new \ZipArchive;
                    if ($zip->open($local_file) === true) {
                        $zip->extractTo($dirBuffer);
                        $zip->close();
                    } else {
                        echo "\n" . $local_file . "- archive not found\n";
                    }
                    $zipCounter++;

                    echo "\nUnpacked: " . $local_file . "\n";
                    echo "To a folder: " . $dirBuffer . "\n";
                    echo "Processed archives: " . $zipCounter . " of " . $totalZipCounter . "\n";

                    $totalCounter = 0;
                    $xmlFormatCount = 0;
                    $otherFormatCount = 0;

                    if (!is_dir($dirBuffer)) {
                        mkdir($dirBuffer);
                    }

                    if ($innerHandle = opendir($dirBuffer)) {
                        while (false !== ($file = readdir($innerHandle))) {
                            if ($file != "." && $file != "..") {
                                $totalCounter++;
                                if (pathinfo($dirBuffer . $file,
                                        PATHINFO_EXTENSION) == 'xml') {
                                    $xmlFormatCount++;
                                    ///*
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsGaranteeInfoInvalid") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsGaranteeInfoInvalid");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsGaranteeInfo") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "fcsGaranteeInfo");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "bankGuaranteeRefusalInvalid") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "bankGuaranteeRefusalInvalid");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "bankGuaranteeRefusal") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "bankGuaranteeRefusal");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "bankGuaranteeReturnInvalid") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "bankGuaranteeReturnInvalid");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "bankGuaranteeReturn") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "bankGuaranteeReturn");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "bankGuaranteeInvalid") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "bankGuaranteeInvalid");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsGaranteeRejectInfo") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "fcsGaranteeRejectInfo");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "bankGuaranteeTerminationInvalid") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "bankGuaranteeTerminationInvalid");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "bankGuaranteeTermination") !== false) {
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "bankGuaranteeTermination");
                                        continue;
                                    }
                                    //*/
                                } else {
                                    $otherFormatCount++;
                                    unlink($dirBuffer . $file);
                                }
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }
    }

    public static function saveXmlToDb()
    {
        $pathExtractionGuaranteeInfo = self::LOCAL_GUARANTEE_PATH . self::LOCAL_EXTRACTED_PATH . 'fcsGaranteeInfo/';
        $pathExtractionGuaranteeInfoInvalid = self::LOCAL_GUARANTEE_PATH . self::LOCAL_EXTRACTED_PATH . 'fcsGaranteeInfoInvalid/';
        $pathExtractionGuaranteeInfoInvalid2 = self::LOCAL_GUARANTEE_PATH . self::LOCAL_EXTRACTED_PATH . 'bankGuaranteeInvalid/';
        $pathExtractionGuaranteeReturn = self::LOCAL_GUARANTEE_PATH . self::LOCAL_EXTRACTED_PATH . 'bankGuaranteeReturn/';
        $pathExtractionGuaranteeReturnInvalid = self::LOCAL_GUARANTEE_PATH . self::LOCAL_EXTRACTED_PATH . 'bankGuaranteeReturnInvalid/';
        $pathExtractionGuaranteeRefusal = self::LOCAL_GUARANTEE_PATH . self::LOCAL_EXTRACTED_PATH . 'fcsGaranteeRejectInfo/';
        $pathExtractionGuaranteeTermination = self::LOCAL_GUARANTEE_PATH . self::LOCAL_EXTRACTED_PATH . 'bankGuaranteeTermination/';

        if (is_dir($pathExtractionGuaranteeInfo)) {
            $handle = opendir($pathExtractionGuaranteeInfo);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseGuaranteeInfo($pathExtractionGuaranteeInfo, $file);
                }
            }
            closedir($handle);
        }
        if (is_dir($pathExtractionGuaranteeInfoInvalid)) {
            $handle = opendir($pathExtractionGuaranteeInfoInvalid);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseGuaranteeInfoInvalid($pathExtractionGuaranteeInfoInvalid, $file);
                }
            }
            closedir($handle);
        }
        if (is_dir($pathExtractionGuaranteeInfoInvalid2)) {
            $handle = opendir($pathExtractionGuaranteeInfoInvalid2);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseGuaranteeInfoInvalid($pathExtractionGuaranteeInfoInvalid2, $file);
                }
            }
            closedir($handle);
        }
        if (is_dir($pathExtractionGuaranteeReturn)) {
            $handle = opendir($pathExtractionGuaranteeReturn);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseGuaranteeReturn($pathExtractionGuaranteeReturn, $file);
                }
            }
            closedir($handle);
        }
        if (is_dir($pathExtractionGuaranteeReturnInvalid)) {
            $handle = opendir($pathExtractionGuaranteeReturnInvalid);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseGuaranteeReturnInvalid($pathExtractionGuaranteeReturnInvalid, $file);
                }
            }
            closedir($handle);
        }
        if (is_dir($pathExtractionGuaranteeRefusal)) {
            $handle = opendir($pathExtractionGuaranteeRefusal);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseGuaranteeRefusal($pathExtractionGuaranteeRefusal, $file);
                }
            }
            closedir($handle);
        }
        /*
        if (is_dir($pathExtractionGuaranteeTermination)) {
            $handle = opendir($pathExtractionGuaranteeTermination);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseGuaranteeTermination($pathExtractionGuaranteeTermination, $file);
                }
            }
            closedir($handle);
        }
        */
    }
}