<?php

namespace console\models;

use common\models\NtfMain;
use Yii;

class Notification
{
    //Кол-во файлов ZIP для распаковки за 1 цикл. Для распаковки всех файлов в папке использовать -1
    //const ZIP_EXTRACTED_COUNT = 5;
    const ZIP_EXTRACTED_COUNT = -1;

    const FTP_USER = "free";
    const FTP_PASSWORD = "free";
    const FTP_HOST = "ftp.zakupki.gov.ru";
    const LOCAL_NOTIFICATION_PATH = "input/notification/";
    const LOCAL_ZIP_PATH = "zip/";
    const LOCAL_EXTRACTED_PATH = "extracted/";
    const LOCAL_EXTRACTED_PATH_BUFFER = "buffer/";

    //список всех регисонов
    private static $regions =
        [
            "Adygeja_Resp",
            "Altaj_Resp",
            "Altajskij_kraj",
            "Amurskaja_obl",
            "Arkhangelskaja_obl",
            "Astrakhanskaja_obl",
            "Bajkonur_g",
            "Bashkortostan_Resp",
            "Belgorodskaja_obl",
            "Brjanskaja_obl",
            "Burjatija_Resp",
            "Chechenskaja_Resp",
            "Cheljabinskaja_obl",
            "Chukotskij_AO",
            "Chuvashskaja_Resp",
            "Dagestan_Resp",
            "Evrejskaja_Aobl",
            "Ingushetija_Resp",
            "Irkutskaja_obl",
            "Ivanovskaja_obl",
            "Jamalo-Neneckij_AO",
            "Jaroslavskaja_obl",
            "Kabardino-Balkarskaja_Resp",
            "Kaliningradskaja_obl",
            "Kalmykija_Resp",
            "Kaluzhskaja_obl",
            "Kamchatskij_kraj",
            "Karachaevo-Cherkesskaja_Resp",
            "Karelija_Resp",
            "Kemerovskaja_obl",
            "Khabarovskij_kraj",
            "Khakasija_Resp",
            "Khanty-Mansijskij_AO-Jugra_AO",
            "Kirovskaja_obl",
            "Komi_Resp",
            "Kostromskaja_obl",
            "Krasnodarskij_kraj",
            "Krasnojarskij_kraj",
            "Krim_Resp",
            "Kurganskaja_obl",
            "Kurskaja_obl",
            "Leningradskaja_obl",
            "Lipeckaja_obl",
            "Magadanskaja_obl",
            "Marij_El_Resp",
            "Mordovija_Resp",
            "Moskovskaja_obl",
            "Moskva",
            "Murmanskaja_obl",
            "Neneckij_AO",
            "Nizhegorodskaja_obl",
            "Novgorodskaja_obl",
            "Novosibirskaja_obl",
            "Omskaja_obl",
            "Orenburgskaja_obl",
            "Orlovskaja_obl",
            "Penzenskaja_obl",
            "Permskij_kraj",
            "Primorskij_kraj",
            "Pskovskaja_obl",
            "Rjazanskaja_obl",
            "Rostovskaja_obl",
            "Sakha_Jakutija_Resp",
            "Sakhalinskaja_obl",
            "Samarskaja_obl",
            "Sankt-Peterburg",
            "Saratovskaja_obl",
            "Sevastopol_g",
            "Severnaja_Osetija-Alanija_Resp",
            "Smolenskaja_obl",
            "Stavropolskij_kraj",
            "Sverdlovskaja_obl",
            "Tambovskaja_obl",
            "Tatarstan_Resp",
            "Tjumenskaja_obl",
            "Tomskaja_obl",
            "Tulskaja_obl",
            "Tverskaja_obl",
            "Tyva_Resp",
            "Udmurtskaja_Resp",
            "Uljanovskaja_obl",
            "Vladimirskaja_obl",
            "Volgogradskaja_obl",
            "Vologodskaja_obl",
            "Voronezhskaja_obl",
            "Zabajkalskij_kraj",
        ];

    public static function saveXmlToDb()
    {

        $pathExtractionEP = self::LOCAL_NOTIFICATION_PATH . self::LOCAL_EXTRACTED_PATH . 'fcsNotificationEP44/';
        $pathExtractionZK = self::LOCAL_NOTIFICATION_PATH . self::LOCAL_EXTRACTED_PATH . 'fcsNotificationZK44/';
        $pathExtractionEA = self::LOCAL_NOTIFICATION_PATH . self::LOCAL_EXTRACTED_PATH . 'fcsNotificationEA44/';

        ///*
        if (is_dir($pathExtractionEP)) {
            $handle = opendir($pathExtractionEP);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseTypeEP($pathExtractionEP, $file);
                }
            }
            closedir($handle);
        }
        //*/
        ///*
        if (is_dir($pathExtractionZK)) {
            $handle = opendir($pathExtractionZK);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseTypeZK($pathExtractionZK, $file);
                }
            }
            closedir($handle);
        }
        //*/
        if (is_dir($pathExtractionEA)) {
            $handle = opendir($pathExtractionEA);
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    self::parseTypeEA($pathExtractionEA, $file);
                }
            }
            closedir($handle);
        }
    }

    public static function deleteDir()
    {
        //$dirPath = self::LOCAL_ZIP_PATH . self::LOCAL_EXTRACTED_PATH_NORM;
    }

    public static function parseTypeEP($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);
        if ($xml) {
            //print_r($xml);
            //die;
            //--------ntf_main variables
            $ntfId = '';
            $ntfType = "fcsNotificationEP44";
            $externalId = '';
            $purchaseNumber = '';
            $directDate = '';
            $docPublishDate = '';
            $docNumber = '';
            $href = '';
            $purchaseObjectInfo = '';
            $purchaseResponsibleResponsibleRole = '';
            $okpd2okved2 = '';
            $filename = basename($file);
            //*--------ntf_main variables

            //--------ntf_print_form variables
            $printFormUrl = '';
            //$printFormSignature = '';
            //*--------ntf_print_form variables

            //--------ntf_extprint_form variables
            $extPrintFormContent = '';
            $extPrintFormUrl = '';
            $extPrintFormFileType = '';
            $extPrintFormSignatureType = '';
            $extPrintFormControlPersonSignatureType = '';
            //*--------ntf_extprint_form variables

            //--------ntf_purchase_responsible_responsible_org variables
            $responsibleOrgRegNum = '';
            $responsibleOrgConsRegistryNum = '';
            $responsibleOrgFullName = '';
            $responsibleOrgShortName = '';
            $responsibleOrgPostAddress = '';
            $responsibleOrgFactAddress = '';
            $responsibleOrgInn = '';
            $responsibleOrgKpp = '';
            //*--------ntf_purchase_responsible_responsible_org variables

            //--------ntf_purchase_responsible_responsible_info variables
            $responsibleInfoOrgPostAddress = '';
            $responsibleInfoOrgFactAddress = '';
            $contactPersonLastName = '';
            $contactPersonFirstName = '';
            $contactPersonMiddleName = '';
            $responsibleInfoContactEMail = '';
            $responsibleInfoContactPhone = '';
            $responsibleInfoContactFax = '';
            $responsibleInfoAddInfo = '';
            //*--------ntf_purchase_responsible_responsible_info variables

            //--------ntf_purchase_responsible_specialized_org variables
            $specializedOrgRegNum = '';
            $specializedOrgConsRegistryNum = '';
            $specializedOrgFullName = '';
            $specializedOrgShortName = '';
            $specializedOrgPostAddress = '';
            $specializedOrgFactAddress = '';
            $specializedOrgInn = '';
            $specializedOrgKpp = '';
            //*--------ntf_purchase_responsible_specialized_org variables

            //--------ntf_placing_way variables
            $placingWayCode = '';
            $placingWayName = '';
            //*--------ntf_placing_way variables

            //--------ntf_lot variables
            $lotMaxPrice = '';
            $lotPriceFormula = '';
            $lotStandardContractNumber = '';
            $lotCurrencyCode = '';
            $lotCurrencyName = '';
            $lotFinanceSource = '';
            $lotInterbudgetaryTransfer = '';
            $lotQuantityUndefined = '';
            $lotPurchaseObjectsTotalSum = '';
            $lotDrugPurchaseObjectsInfoTotal = '';
            $lotRestrictInfo = '';
            $lotAddInfo = '';
            $lotNoPublicDiscussion = '';
            $lotMustPublicDiscussion = '';
            //*--------ntf_lot variables

            //--------ntf_public_discussion variables
            $publicDiscussionNumber = '';
            $publicDiscussionOrganizationCh5St15 = '';
            $publicDiscussionHref = '';
            $publicDiscussionPlace = '';
            $publicDiscussion2017ProtocolDate = '';
            $publicDiscussion2017ProtocolPublishDate = '';
            $publicDiscussion2017PublicDiscussionPhase2Num = '';
            $publicDiscussion2017HrefPhase2 = '';
            //*--------ntf_public_discussion variables

            //-----------ntf_main
            if (isset($xml->fcsNotificationEP->id)) {
                $ntfId = (string)$xml->fcsNotificationEP->id;
            }
            if (isset($xml->fcsNotificationEP->externalId)) {
                $externalId = (string)$xml->fcsNotificationEP->externalId;
            }
            if (isset($xml->fcsNotificationEP->purchaseNumber)) {
                $purchaseNumber = (string)$xml->fcsNotificationEP->purchaseNumber;
            }
            if (isset($xml->fcsNotificationEP->directDate)) {
                $directDate = (string)$xml->fcsNotificationEP->directDate;
            }
            if (isset($xml->fcsNotificationEP->docPublishDate)) {
                $docPublishDate = (string)$xml->fcsNotificationEP->docPublishDate;
            }
            if (isset($xml->fcsNotificationEP->docNumber)) {
                $docNumber = (string)$xml->fcsNotificationEP->docNumber;
            }
            if (isset($xml->fcsNotificationEP->href)) {
                $href = (string)$xml->fcsNotificationEP->href;
            }
            if (isset($xml->fcsNotificationEP->purchaseObjectInfo)) {
                $purchaseObjectInfo = (string)$xml->fcsNotificationEP->purchaseObjectInfo;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleRole)) {
                $purchaseResponsibleResponsibleRole = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleRole;
            }
            if (isset($xml->fcsNotificationEP->externalId)) {
                $externalId = (string)$xml->fcsNotificationEP->externalId;
            }
            //if (isset($xml->fcsNotificationEP->okpd2okved2)) {
            //    $okpd2okved2 = (string)$xml->fcsNotificationEP->okpd2okved2;
            //}

            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->regNum)) {
                $responsibleOrgRegNum = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->regNum;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->consRegistryNum)) {
                $responsibleOrgConsRegistryNum = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->fullName)) {
                $responsibleOrgFullName = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->fullName;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->shortName)) {
                $responsibleOrgShortName = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->shortName;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->postAddress)) {
                $responsibleOrgPostAddress = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->postAddress;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->factAddress)) {
                $responsibleOrgFactAddress = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->factAddress;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->INN)) {
                $responsibleOrgInn = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->INN;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->KPP)) {
                $responsibleOrgKpp = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleOrg->KPP;
            }

            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->orgPostAddress)) {
                $responsibleInfoOrgPostAddress = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->orgPostAddress;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->orgFactAddress)) {
                $responsibleInfoOrgFactAddress = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->orgFactAddress;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPerson->lastName)) {
                $contactPersonLastName = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPerson->lastName;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPerson->firstName)) {
                $contactPersonFirstName = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPerson->firstName;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPerson->middleName)) {
                $contactPersonMiddleName = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPerson->middleName;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactEMail)) {
                $responsibleInfoContactEMail = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactEMail;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPhone)) {
                $responsibleInfoContactPhone = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactPhone;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactFax)) {
                $responsibleInfoContactFax = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->contactFax;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->addInfo)) {
                $responsibleInfoAddInfo = (string)$xml->fcsNotificationEP->purchaseResponsible->responsibleInfo->addInfo;
            }

            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->regNum)) {
                $specializedOrgRegNum = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->regNum;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->consRegistryNum)) {
                $specializedOrgConsRegistryNum = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->fullName)) {
                $specializedOrgFullName = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->fullName;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->shortName)) {
                $specializedOrgShortName = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->shortName;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->postAddress)) {
                $specializedOrgPostAddress = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->postAddress;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->factAddress)) {
                $specializedOrgFactAddress = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->factAddress;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->INN)) {
                $specializedOrgInn = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->INN;
            }
            if (isset($xml->fcsNotificationEP->purchaseResponsible->specializedOrg->KPP)) {
                $specializedOrgKpp = (string)$xml->fcsNotificationEP->purchaseResponsible->specializedOrg->KPP;
            }
            //-----------ntf_placing_way
            if (isset($xml->fcsNotificationEP->placingWay->code)) {
                $placingWayCode = (string)$xml->fcsNotificationEP->placingWay->code;
            }
            if (isset($xml->fcsNotificationEP->placingWay->name)) {
                $placingWayName = (string)$xml->fcsNotificationEP->placingWay->name;
            }
            //*-----------ntf_placing_way

            //-----------ntf_public_discussion
            if (isset($xml->fcsNotificationEP->publicDiscussion->number)) {
                $publicDiscussionNumber = (string)$xml->fcsNotificationEP->publicDiscussion->number;
            }
            if (isset($xml->fcsNotificationEP->publicDiscussion->organizationCh5St15)) {
                $publicDiscussionOrganizationCh5St15 = (string)$xml->fcsNotificationEP->publicDiscussion->organizationCh5St15;
            }
            if (isset($xml->fcsNotificationEP->publicDiscussion->href)) {
                $publicDiscussionHref = (string)$xml->fcsNotificationEP->publicDiscussion->href;
            }
            if (isset($xml->fcsNotificationEP->publicDiscussion->place)) {
                $publicDiscussionPlace = (string)$xml->fcsNotificationEP->publicDiscussion->place;
            }
            if (isset($xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolDate)) {
                $publicDiscussion2017ProtocolDate = (string)$xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolDate;
            }
            if (isset($xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolPublishDate)) {
                $publicDiscussion2017ProtocolPublishDate = (string)$xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolPublishDate;
            }
            if (isset($xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->publicDiscussionPhase2Num)) {
                $publicDiscussion2017PublicDiscussionPhase2Num = (string)$xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->publicDiscussionPhase2Num;
            }
            if (isset($xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->hrefPhase2)) {
                $publicDiscussion2017HrefPhase2 = (string)$xml->fcsNotificationEP->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->hrefPhase2;
            }
            //*-----------ntf_public_discussion

            //*-----------ntf_main
            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO ntf_main (
                          ntf_id,
                          type,
                          external_id,
                          purchase_number,
                          direct_date,
                          doc_publish_date,
                          doc_number,
                          href,
                          purchase_object_info,
                          purchase_responsible_responsible_role,
                          filename,
                          created_at
                      ) 
                      VALUES(
                          :ntfId,
                          :ntfType,
                          :externalId,
                          :purchaseNumber,
                          :directDate,
                          :docPublishDate,
                          :docNumber,
                          :href,
                          :purchaseObjectInfo,
                          :purchaseResponsibleResponsibleRole,
                          :filename,
                          :createdAt
                      )                      
                      ON CONFLICT (ntf_id) DO NOTHING 
                      RETURNING id";
            $returningState = Yii::$app->db_zakupki->createCommand($sql)
                ->bindValues([
                    ':ntfId' => $ntfId,
                    ':ntfType' => $ntfType,
                    ':externalId' => $externalId,
                    ':purchaseNumber' => $purchaseNumber,
                    ':directDate' => $directDate,
                    ':docPublishDate' => $docPublishDate,
                    ':docNumber' => $docNumber,
                    ':href' => $href,
                    ':purchaseObjectInfo' => $purchaseObjectInfo,
                    ':purchaseResponsibleResponsibleRole' => $purchaseResponsibleResponsibleRole,
                    //':okpd2okved2' => $okpd2okved2,
                    ':filename' => $filename,
                    ':createdAt' => date("Y-m-d H:i:s"),
                ])
                ->execute();

            $ntfMainId = Yii::$app->db_zakupki->getLastInsertID('ntf_main_id_seq');

            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
                //var_dump($returningState);
                //var_dump($ntfMainId);
            } else {
                echo "Record with ntf_id = " . $ntfId . " exist." . "\n";
                return;
            }
            //*-----------Вставка данных в ntf_main
die;
            //-----------ntf_print_form
            if (isset($xml->fcsNotificationEP->printForm->url)) {
                $printFormUrl = (string)$xml->fcsNotificationEP->printForm->url;
            }
            if (isset($xml->fcsNotificationEP->printForm->signature)) {
                $printFormSignature = (string)$xml->fcsNotificationEP->printForm->signature;
            }
            //*-----------ntf_print_form
            //-----------Вставка данных в ntf_print_form
            if (
                $ntfMainId !== '' ||
                $printFormUrl !== ''
            ) {
                $sql = "
                  INSERT INTO ntf_print_form (
                    ntf_main_id,
                    url
                  ) 
                  VALUES(
                    :ntfMainId,
                    :printFormUrl
                )";
                Yii::$app->db_zakupki->createCommand($sql)
                    ->bindValues([
                        ':ntfMainId' => $ntfMainId,
                        ':printFormUrl' => $printFormUrl,
                    ])
                    ->execute();
                /*
                Yii::$app->db_zakupki->createCommand(
                    "INSERT INTO ntf_print_form
                             (
                              ntf_main_id,
                              url
                      ) 
                      VALUES (
                              '$ntfMainId',
                              '$printFormUrl'
                      )")
                    ->execute();
                */
            }
            //*-----------Вставка данных в ntf_print_form

            //-----------ntf_lot
            if (isset($xml->fcsNotificationEP->lot->maxPrice)) {
                $lotMaxPrice = (string)$xml->fcsNotificationEP->lot->maxPrice;
            }
            if (isset($xml->fcsNotificationEP->lot->priceFormula)) {
                $lotPriceFormula = (string)$xml->fcsNotificationEP->lot->priceFormula;
            }
            if (isset($xml->fcsNotificationEP->lot->standardContractNumber)) {
                $lotStandardContractNumber = (string)$xml->fcsNotificationEP->lot->standardContractNumber;
            }
            if (isset($xml->fcsNotificationEP->lot->currency->code)) {
                $lotCurrencyCode = (string)$xml->fcsNotificationEP->lot->currency->code;
            }
            if (isset($xml->fcsNotificationEP->lot->currency->name)) {
                $lotCurrencyName = (string)$xml->fcsNotificationEP->lot->currency->name;
            }
            if (isset($xml->fcsNotificationEP->lot->financeSource)) {
                $lotFinanceSource = (string)$xml->fcsNotificationEP->lot->financeSource;
            }
            if (isset($xml->fcsNotificationEP->lot->interbudgetaryTransfer)) {
                $lotInterbudgetaryTransfer = (string)$xml->fcsNotificationEP->lot->interbudgetaryTransfer;
            }
            if (isset($xml->fcsNotificationEP->lot->quantityUndefined)) {
                $lotQuantityUndefined = (string)$xml->fcsNotificationEP->lot->quantityUndefined;
            }
            if (isset($xml->fcsNotificationEP->lot->purchaseObjects->totalSum)) {
                $lotPurchaseObjectsTotalSum = (string)$xml->fcsNotificationEP->lot->purchaseObjects->totalSum;
            }
            if (isset($xml->fcsNotificationEP->lot->drugPurchaseObjectsInfo->total)) {
                $lotDrugPurchaseObjectsInfoTotal = (string)$xml->fcsNotificationEP->lot->drugPurchaseObjectsInfo->total;
            }
            if (isset($xml->fcsNotificationEP->lot->restrictInfo)) {
                $lotRestrictInfo = (string)$xml->fcsNotificationEP->lot->restrictInfo;
            }
            if (isset($xml->fcsNotificationEP->lot->addInfo)) {
                $lotAddInfo = (string)$xml->fcsNotificationEP->lot->addInfo;
            }
            if (isset($xml->fcsNotificationEP->lot->noPublicDiscussion)) {
                $lotNoPublicDiscussion = (string)$xml->fcsNotificationEP->lot->noPublicDiscussion;
            }
            if (isset($xml->fcsNotificationEP->lot->mustPublicDiscussion)) {
                $lotMustPublicDiscussion = (string)$xml->fcsNotificationEP->lot->mustPublicDiscussion;
            }
            //*-----------ntf_lot

            //-----------Вставка данных в ntf_lot
            if (
                $lotMaxPrice !== '' ||
                $lotPriceFormula !== '' ||
                $lotStandardContractNumber !== '' ||
                $lotCurrencyCode !== '' ||
                $lotCurrencyName !== '' ||
                $lotFinanceSource !== '' ||
                $lotInterbudgetaryTransfer !== '' ||
                $lotQuantityUndefined !== '' ||
                $lotPurchaseObjectsTotalSum !== '' ||
                $lotDrugPurchaseObjectsInfoTotal !== '' ||
                $lotRestrictInfo !== '' ||
                $lotAddInfo !== '' ||
                $lotNoPublicDiscussion !== '' ||
                $lotMustPublicDiscussion !== ''
            ) {
                $sql = "
                  INSERT INTO ntf_lot (
                    ntf_main_id,
                    max_price,
                    price_formula,
                    standard_contract_number,
                    currency_code,
                    currency_name,
                    finance_source,
                    interbudgetary_transfer,
                    quantity_undefined,
                    purchase_objects_total_sum,
                    drug_purchase_objects_info_total,
                    restrict_info,
                    add_info,
                    no_public_discussion,
                    must_public_discussion
                  ) 
                  VALUES(
                    :ntfMainId,
                    :lotMaxPrice,
                    :lotPriceFormula,
                    :lotStandardContractNumber,
                    :lotCurrencyCode,
                    :lotCurrencyName,
                    :lotFinanceSource,
                    :lotInterbudgetaryTransfer,
                    :lotQuantityUndefined,
                    :lotPurchaseObjectsTotalSum,
                    :lotDrugPurchaseObjectsInfoTotal,
                    :lotRestrictInfo,
                    :lotAddInfo,
                    :lotNoPublicDiscussion,
                    :lotMustPublicDiscussion
                )";
                Yii::$app->db_zakupki->createCommand($sql)
                    ->bindValues([
                        ':ntfMainId' => $ntfMainId,
                        ':lotMaxPrice' => $lotMaxPrice,
                        ':lotPriceFormula' => $lotPriceFormula,
                        ':lotStandardContractNumber' => $lotStandardContractNumber,
                        ':lotCurrencyCode' => $lotCurrencyCode,
                        ':lotCurrencyName' => $lotCurrencyName,
                        ':lotFinanceSource' => $lotFinanceSource,
                        ':lotInterbudgetaryTransfer' => $lotInterbudgetaryTransfer,
                        ':lotQuantityUndefined' => $lotQuantityUndefined,
                        ':lotPurchaseObjectsTotalSum' => $lotPurchaseObjectsTotalSum,
                        ':lotDrugPurchaseObjectsInfoTotal' => $lotDrugPurchaseObjectsInfoTotal,
                        ':lotRestrictInfo' => $lotRestrictInfo,
                        ':lotAddInfo' => $lotAddInfo,
                        ':lotNoPublicDiscussion' => $lotNoPublicDiscussion,
                        ':lotMustPublicDiscussion' => $lotMustPublicDiscussion,
                    ])
                    ->execute();
                /*
                Yii::$app->db_zakupki->createCommand(
                    "INSERT INTO ntf_lot
                             (
                              ntf_main_id,
                              max_price,
                              price_formula,
                              standard_contract_number,
                              currency_code,
                              currency_name,
                              finance_source,
                              interbudgetary_transfer,
                              quantity_undefined,
                              purchase_objects_total_sum,
                              drug_purchase_objects_info_total,
                              restrict_info,
                              add_info,
                              no_public_discussion,
                              must_public_discussion
                      ) 
                      VALUES (
                              '$ntfMainId',
                              '$lotMaxPrice',
                              '$lotPriceFormula',
                              '$lotStandardContractNumber',
                              '$lotCurrencyCode',
                              '$lotCurrencyName',
                              '$lotFinanceSource',
                              '$lotInterbudgetaryTransfer',
                              '$lotQuantityUndefined',
                              '$lotPurchaseObjectsTotalSum',
                              '$lotDrugPurchaseObjectsInfoTotal',
                              '$lotRestrictInfo',
                              '$lotAddInfo',
                              '$lotNoPublicDiscussion',
                              '$lotMustPublicDiscussion'
                      )")
                    ->execute();
                */
            }
            $ntfLotId = Yii::$app->db_zakupki->getLastInsertID('ntf_lot_id_seq');
            //*-----------Вставка данных в ntf_lot

            //-----------ntf_customer_requirement
            if (isset($xml->fcsNotificationEP->lot->customerRequirements)) {
                foreach ($xml->fcsNotificationEP->lot->customerRequirements as $customerRequirement) {
                    $customerRegNum = '';
                    $customerConsRegistryNum = '';
                    $customerFullName = '';
                    $maxPrice = '';
                    $deliveryPlace = '';
                    $deliveryTerm = '';
                    $addInfo = '';
                    $purchaseCode = '';
                    $nonbudgetFinancingTotalSum = '';
                    $budgetFinancingsTotalSum = '';
                    $BONumber = '';
                    $BODate = '';
                    $inputBOFlag = '';
                    $applicationGuaranteeAmount = '';
                    $applicationGuaranteePart = '';
                    $applicationGuaranteeProcedureInfo = '';
                    $applicationGuaranteeSettlementAccount = '';
                    $applicationGuaranteePersonalAccount = '';
                    $applicationGuaranteeBik = '';
                    $contractGuaranteeAmount = '';
                    $contractGuaranteePart = '';
                    $contractGuaranteeProcedureInfo = '';
                    $contractGuaranteeSettlementAccount = '';
                    $contractGuaranteePersonalAccount = '';
                    $contractGuaranteeBik = '';
                    $tenderPlanInfoPlanNumber = '';
                    $tenderPlanInfoPositionNumber = '';
                    $tenderPlanInfoPurchase83st544 = '';
                    $tenderPlanInfoPlan2017Number = '';
                    $tenderPlanInfoPosition2017Number = '';
                    $tenderPlanInfoPosition2017ExtNumber = '';

                    if (isset($customerRequirement->customerRequirement->customer->regNum)) {
                        $customerRegNum = (string)$customerRequirement->customerRequirement->customer->regNum;
                    }
                    if (isset($customerRequirement->customerRequirement->customer->consRegistryNum)) {
                        $customerConsRegistryNum = (string)$customerRequirement->customerRequirement->customer->consRegistryNum;
                    }
                    if (isset($customerRequirement->customerRequirement->customer->fullName)) {
                        $customerFullName = (string)$customerRequirement->customerRequirement->customer->fullName;
                    }
                    if (isset($customerRequirement->customerRequirement->maxPrice)) {
                        $maxPrice = (string)$customerRequirement->customerRequirement->maxPrice;
                    }
                    if (isset($customerRequirement->customerRequirement->deliveryPlace)) {
                        $deliveryPlace = (string)$customerRequirement->customerRequirement->deliveryPlace;
                    }
                    if (isset($customerRequirement->customerRequirement->deliveryTerm)) {
                        $deliveryTerm = (string)$customerRequirement->customerRequirement->deliveryTerm;
                    }
                    if (isset($customerRequirement->customerRequirement->addInfo)) {
                        $addInfo = (string)$customerRequirement->customerRequirement->addInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->purchaseCode)) {
                        $purchaseCode = (string)$customerRequirement->customerRequirement->purchaseCode;
                    }
                    if (isset($customerRequirement->customerRequirement->nonbudgetFinancing->totalSum)) {
                        $nonbudgetFinancingTotalSum = (string)$customerRequirement->customerRequirement->nonbudgetFinancing->totalSum;
                    }
                    if (isset($customerRequirement->customerRequirement->budgetFinancings->totalSum)) {
                        $budgetFinancingsTotalSum = (string)$customerRequirement->customerRequirement->budgetFinancings->totalSum;
                    }

                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->amount)) {
                        $applicationGuaranteeAmount = (string)$customerRequirement->customerRequirement->applicationGuarantee->amount;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->part)) {
                        $applicationGuaranteePart = (string)$customerRequirement->customerRequirement->applicationGuarantee->part;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->procedureInfo)) {
                        $applicationGuaranteeProcedureInfo = (string)$customerRequirement->customerRequirement->applicationGuarantee->procedureInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->settlementAccount)) {
                        $applicationGuaranteeSettlementAccount = (string)$customerRequirement->customerRequirement->applicationGuarantee->settlementAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->personalAccount)) {
                        $applicationGuaranteePersonalAccount = (string)$customerRequirement->customerRequirement->applicationGuarantee->personalAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->bik)) {
                        $applicationGuaranteeBik = (string)$customerRequirement->customerRequirement->applicationGuarantee->bik;
                    }

                    if (isset($customerRequirement->customerRequirement->contractGuarantee->amount)) {
                        $contractGuaranteeAmount = (string)$customerRequirement->customerRequirement->contractGuarantee->amount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->part)) {
                        $contractGuaranteePart = (string)$customerRequirement->customerRequirement->contractGuarantee->part;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->procedureInfo)) {
                        $contractGuaranteeProcedureInfo = (string)$customerRequirement->customerRequirement->contractGuarantee->procedureInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->settlementAccount)) {
                        $contractGuaranteeSettlementAccount = (string)$customerRequirement->customerRequirement->contractGuarantee->settlementAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->personalAccount)) {
                        $contractGuaranteePersonalAccount = (string)$customerRequirement->customerRequirement->contractGuarantee->personalAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->bik)) {
                        $contractGuaranteeBik = (string)$customerRequirement->customerRequirement->contractGuarantee->bik;
                    }

                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->planNumber)) {
                        $tenderPlanInfoPlanNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->planNumber;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->positionNumber)) {
                        $tenderPlanInfoPositionNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->positionNumber;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->purchase83st544)) {
                        $tenderPlanInfoPurchase83st544 = (string)$customerRequirement->customerRequirement->tenderPlanInfo->purchase83st544;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->plan2017Number)) {
                        $tenderPlanInfoPlan2017Number = (string)$customerRequirement->customerRequirement->tenderPlanInfo->plan2017Number;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->position2017Number)) {
                        $tenderPlanInfoPosition2017Number = (string)$customerRequirement->customerRequirement->tenderPlanInfo->position2017Number;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->position2017ExtNumber)) {
                        $tenderPlanInfoPosition2017ExtNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->position2017ExtNumber;
                    }

                    if (isset($customerRequirement->customerRequirement->BOInfo->BONumber)) {
                        $BONumber = (string)$customerRequirement->customerRequirement->BOInfo->BONumber;
                    }
                    if (isset($customerRequirement->customerRequirement->BOInfo->BODate)) {
                        $BODate = (string)$customerRequirement->customerRequirement->BOInfo->BODate;
                    }
                    if (isset($customerRequirement->customerRequirement->BOInfo->inputBOFlag)) {
                        $inputBOFlag = (string)$customerRequirement->customerRequirement->BOInfo->inputBOFlag;
                    }

                    //TODO: можно переделать в batchinsert всего массива данныъ, из foreach
                    //-----------Вставка данных в ntf_customer_requirement
                    if (
                        $customerRegNum !== '' ||
                        $customerConsRegistryNum !== '' ||
                        $customerFullName !== '' ||
                        $maxPrice !== '' ||
                        $deliveryPlace !== '' ||
                        $deliveryTerm !== '' ||
                        $addInfo !== '' ||
                        $purchaseCode !== '' ||
                        $nonbudgetFinancingTotalSum !== '' ||
                        $budgetFinancingsTotalSum !== '' ||
                        $BONumber !== '' ||
                        $BODate !== '' ||
                        $inputBOFlag !== '' ||
                        $applicationGuaranteeAmount !== '' ||
                        $applicationGuaranteePart !== '' ||
                        $applicationGuaranteeProcedureInfo !== '' ||
                        $applicationGuaranteeSettlementAccount !== '' ||
                        $applicationGuaranteePersonalAccount !== '' ||
                        $applicationGuaranteeBik !== '' ||
                        $contractGuaranteeAmount !== '' ||
                        $contractGuaranteePart !== '' ||
                        $contractGuaranteeProcedureInfo !== '' ||
                        $contractGuaranteeSettlementAccount !== '' ||
                        $contractGuaranteePersonalAccount !== '' ||
                        $contractGuaranteeBik !== '' ||
                        $tenderPlanInfoPlanNumber !== '' ||
                        $tenderPlanInfoPositionNumber !== '' ||
                        $tenderPlanInfoPurchase83st544 !== '' ||
                        $tenderPlanInfoPlan2017Number !== '' ||
                        $tenderPlanInfoPosition2017Number !== '' ||
                        $tenderPlanInfoPosition2017ExtNumber !== ''
                    ) {
                        $sql = "
                          INSERT INTO ntf_customer_requirement (
                            ntf_lot_id,
                            customer_reg_num,
                            customer_cons_registry_num,
                            customer_full_name,
                            max_price,
                            delivery_place,
                            delivery_term,
                            application_guarantee_amount,
                            application_guarantee_part,
                            application_guarantee_procedure_info,
                            application_guarantee_settlement_account,
                            application_guarantee_personal_account,
                            application_guarantee_bik,
                            contract_guarantee_amount,
                            contract_guarantee_part,
                            contract_guarantee_procedure_info,
                            contract_guarantee_settlement_account,
                            contract_guarantee_personal_account,
                            contract_guarantee_bik,
                            add_info,
                            purchase_code,
                            tender_plan_info_plan_number,
                            tender_plan_info_position_number,
                            tender_plan_info_purchase83st544,
                            tender_plan_info_plan2017_number,
                            tender_plan_info_position2017_number,
                            tender_plan_info_position2017_ext_number,
                            nonbudget_financings_total_sum,
                            budget_financings_total_sum,
                            bo_info_bo_number,
                            bo_info_bo_date,
                            bo_info_input_bo_flag
                          ) 
                          VALUES(
                            :ntfLotId,
                            :customerRegNum,
                            :customerConsRegistryNum,
                            :customerFullName,
                            :maxPrice,
                            :deliveryPlace,
                            :deliveryTerm,
                            :applicationGuaranteeAmount,
                            :applicationGuaranteePart,
                            :applicationGuaranteeProcedureInfo,
                            :applicationGuaranteeSettlementAccount,
                            :applicationGuaranteePersonalAccount,
                            :applicationGuaranteeBik,
                            :contractGuaranteeAmount,
                            :contractGuaranteePart,
                            :contractGuaranteeProcedureInfo,
                            :contractGuaranteeSettlementAccount,
                            :contractGuaranteePersonalAccount,
                            :contractGuaranteeBik,
                            :addInfo,
                            :purchaseCode,
                            :tenderPlanInfoPlanNumber,
                            :tenderPlanInfoPositionNumber,
                            :tenderPlanInfoPurchase83st544,
                            :tenderPlanInfoPlan2017Number,
                            :tenderPlanInfoPosition2017Number,
                            :tenderPlanInfoPosition2017ExtNumber,
                            :nonbudgetFinancingTotalSum,
                            :budgetFinancingsTotalSum,
                            :BONumber,
                            :BODate,
                            :inputBOFlag
                        )";
                        Yii::$app->db_zakupki->createCommand($sql)
                            ->bindValues([
                                ':ntfLotId' => $ntfLotId,
                                ':customerRegNum' => $customerRegNum,
                                ':customerConsRegistryNum' => $customerConsRegistryNum,
                                ':customerFullName' => $customerFullName,
                                ':maxPrice' => $maxPrice,
                                ':deliveryPlace' => $deliveryPlace,
                                ':deliveryTerm' => $deliveryTerm,
                                ':applicationGuaranteeAmount' => $applicationGuaranteeAmount,
                                ':applicationGuaranteePart' => $applicationGuaranteePart,
                                ':applicationGuaranteeProcedureInfo' => $applicationGuaranteeProcedureInfo,
                                ':applicationGuaranteeSettlementAccount' => $applicationGuaranteeSettlementAccount,
                                ':applicationGuaranteePersonalAccount' => $applicationGuaranteePersonalAccount,
                                ':applicationGuaranteeBik' => $applicationGuaranteeBik,
                                ':contractGuaranteeAmount' => $contractGuaranteeAmount,
                                ':contractGuaranteePart' => $contractGuaranteePart,
                                ':contractGuaranteeProcedureInfo' => $contractGuaranteeProcedureInfo,
                                ':contractGuaranteeSettlementAccount' => $contractGuaranteeSettlementAccount,
                                ':contractGuaranteePersonalAccount' => $contractGuaranteePersonalAccount,
                                ':contractGuaranteeBik' => $contractGuaranteeBik,
                                ':addInfo' => $addInfo,
                                ':purchaseCode' => $purchaseCode,
                                ':tenderPlanInfoPlanNumber' => $tenderPlanInfoPlanNumber,
                                ':tenderPlanInfoPositionNumber' => $tenderPlanInfoPositionNumber,
                                ':tenderPlanInfoPurchase83st544' => $tenderPlanInfoPurchase83st544,
                                ':tenderPlanInfoPlan2017Number' => $tenderPlanInfoPlan2017Number,
                                ':tenderPlanInfoPosition2017Number' => $tenderPlanInfoPosition2017Number,
                                ':tenderPlanInfoPosition2017ExtNumber' => $tenderPlanInfoPosition2017ExtNumber,
                                ':nonbudgetFinancingTotalSum' => $nonbudgetFinancingTotalSum,
                                ':budgetFinancingsTotalSum' => $budgetFinancingsTotalSum,
                                ':BONumber' => $BONumber,
                                ':BODate' => $BODate,
                                ':inputBOFlag' => $inputBOFlag,
                            ])
                            ->execute();
                        /*
                        Yii::$app->db_zakupki->createCommand(
                            "INSERT INTO ntf_customer_requirement
                             (
                              ntf_lot_id,
                              customer_reg_num,
                              customer_cons_registry_num,
                              customer_full_name,
                              max_price,
                              delivery_place,
                              delivery_term,
                              application_guarantee_amount,
                              application_guarantee_part,
                              application_guarantee_procedure_info,
                              application_guarantee_settlement_account,
                              application_guarantee_personal_account,
                              application_guarantee_bik,
                              contract_guarantee_amount,
                              contract_guarantee_part,
                              contract_guarantee_procedure_info,
                              contract_guarantee_settlement_account,
                              contract_guarantee_personal_account,
                              contract_guarantee_bik,
                              add_info,
                              purchase_code,
                              tender_plan_info_plan_number,
                              tender_plan_info_position_number,
                              tender_plan_info_purchase83st544,
                              tender_plan_info_plan2017_number,
                              tender_plan_info_position2017_number,
                              tender_plan_info_position2017_ext_number,
                              nonbudget_financings_total_sum,
                              budget_financings_total_sum,
                              bo_info_bo_number,
                              bo_info_bo_date,
                              bo_info_input_bo_flag
                      ) 
                      VALUES (
                              '$ntfLotId',
                              '$customerRegNum',
                              '$customerConsRegistryNum',
                              '$customerFullName',
                              '$maxPrice',
                              '$deliveryPlace',
                              '$deliveryTerm',
                              '$applicationGuaranteeAmount',
                              '$applicationGuaranteePart',
                              '$applicationGuaranteeProcedureInfo',
                              '$applicationGuaranteeSettlementAccount',
                              '$applicationGuaranteePersonalAccount',
                              '$applicationGuaranteeBik',
                              '$contractGuaranteeAmount',
                              '$contractGuaranteePart',
                              '$contractGuaranteeProcedureInfo',
                              '$contractGuaranteeSettlementAccount',
                              '$contractGuaranteePersonalAccount',
                              '$contractGuaranteeBik',
                              '$addInfo',
                              '$purchaseCode',
                              '$tenderPlanInfoPlanNumber',
                              '$tenderPlanInfoPositionNumber',
                              '$tenderPlanInfoPurchase83st544',
                              '$tenderPlanInfoPlan2017Number',
                              '$tenderPlanInfoPosition2017Number',
                              '$tenderPlanInfoPosition2017ExtNumber',
                              '$nonbudgetFinancingTotalSum',
                              '$budgetFinancingsTotalSum',
                              '$BONumber',
                              '$BODate',
                              '$inputBOFlag'
                      )")
                            ->execute();
                        */
                    }
                    $ntfCustomerRequirementId = Yii::$app->db_zakupki->getLastInsertID('ntf_customer_requirement_id_seq');
                    //*-----------Вставка данных в ntf_customer_requirement

                    //-----------ntf_kladr_place
                    if (isset($customerRequirement->customerRequirement->kladrPlaces->kladrPlace)) {
                        foreach ($customerRequirement->customerRequirement->kladrPlaces->kladrPlace as $kladrPlace) {
                            $kladrPlaceKladrKladrType = '';
                            $kladrPlaceKladrKladrCode = '';
                            $kladrPlaceKladrFullName = '';
                            $kladrPlaceCountryCountryCode = '';
                            $kladrPlaceCountryCountryFullName = '';
                            $kladrPlaceNoKladrForRegionSettlement = '';
                            $kladrPlaceDeliveryPlace = '';
                            $kladrPlaceNoKladrForRegionSettlementRegion = '';
                            $kladrPlaceNoKladrForRegionSettlementSettlement = '';

                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceKladrKladrType = (string)$kladrPlace->kladr->kladrType;
                            }
                            if (isset($kladrPlace->kladr->kladrCode)) {
                                $kladrPlaceKladrKladrCode = (string)$kladrPlace->kladr->kladrCode;
                            }
                            if (isset($kladrPlace->kladr->fullName)) {
                                $kladrPlaceKladrFullName = (string)$kladrPlace->kladr->fullName;
                            }
                            if (isset($kladrPlace->country->countryCode)) {
                                $kladrPlaceCountryCountryCode = (string)$kladrPlace->country->countryCode;
                            }
                            if (isset($kladrPlace->country->countryFullName)) {
                                $kladrPlaceCountryCountryFullName = (string)$kladrPlace->country->countryFullName;
                            }
                            if (isset($kladrPlace->noKladrForRegionSettlement)) {
                                $kladrPlaceNoKladrForRegionSettlement = (string)$kladrPlace->noKladrForRegionSettlement;
                            }
                            if (isset($kladrPlace->deliveryPlace)) {
                                $kladrPlaceDeliveryPlace = (string)$kladrPlace->deliveryPlace;
                            }
                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceNoKladrForRegionSettlementRegion = (string)$kladrPlace->noKladrForRegionSettlement->region;
                            }
                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceNoKladrForRegionSettlementSettlement = (string)$kladrPlace->noKladrForRegionSettlement->settlement;
                            }

                            //-----------Вставка данных в ntf_kladr_place
                            if (
                                $kladrPlaceKladrKladrType !== '' ||
                                $kladrPlaceKladrKladrCode !== '' ||
                                $kladrPlaceKladrFullName !== '' ||
                                $kladrPlaceCountryCountryCode !== '' ||
                                $kladrPlaceCountryCountryFullName !== '' ||
                                $kladrPlaceNoKladrForRegionSettlement !== '' ||
                                $kladrPlaceDeliveryPlace !== '' ||
                                $kladrPlaceNoKladrForRegionSettlementRegion !== '' ||
                                $kladrPlaceNoKladrForRegionSettlementSettlement !== ''
                            ) {
                                //-----------Вставка данных в ntf_kladr_place
                                $sql = "
                                  INSERT INTO ntf_kladr_place (
                                    ntf_customer_requirement_id,
                                    kladr_kladr_type,
                                    kladr_kladr_code,
                                    kladr_full_name,
                                    country_country_code,
                                    country_country_full_name,
                                    delivery_place,
                                    no_kladr_for_region_settlement_region,
                                    no_kladr_for_region_settlement_settlement,
                                    no_kladr_for_region_settlement
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :kladrPlaceKladrKladrType,
                                    :kladrPlaceKladrKladrCode,
                                    :kladrPlaceKladrFullName,
                                    :kladrPlaceCountryCountryCode,
                                    :kladrPlaceCountryCountryFullName,
                                    :kladrPlaceDeliveryPlace,
                                    :kladrPlaceNoKladrForRegionSettlementRegion,
                                    :kladrPlaceNoKladrForRegionSettlementSettlement,
                                    :kladrPlaceNoKladrForRegionSettlement
                                  )";
                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':kladrPlaceKladrKladrType' => $kladrPlaceKladrKladrType,
                                        ':kladrPlaceKladrKladrCode' => $kladrPlaceKladrKladrCode,
                                        ':kladrPlaceKladrFullName' => $kladrPlaceKladrFullName,
                                        ':kladrPlaceCountryCountryCode' => $kladrPlaceCountryCountryCode,
                                        ':kladrPlaceCountryCountryFullName' => $kladrPlaceCountryCountryFullName,
                                        ':kladrPlaceDeliveryPlace' => $kladrPlaceDeliveryPlace,
                                        ':kladrPlaceNoKladrForRegionSettlementRegion' => $kladrPlaceNoKladrForRegionSettlementRegion,
                                        ':kladrPlaceNoKladrForRegionSettlementSettlement' => $kladrPlaceNoKladrForRegionSettlementSettlement,
                                        ':kladrPlaceNoKladrForRegionSettlement' => $kladrPlaceNoKladrForRegionSettlement,
                                    ])
                                    ->execute();
                                /*
                                Yii::$app->db_zakupki->createCommand(
                                    "INSERT INTO ntf_kladr_place
                                                 (
                                                  ntf_customer_requirement_id,
                                                  kladr_kladr_type,
                                                  kladr_kladr_code,
                                                  kladr_full_name,
                                                  country_country_code,
                                                  country_country_full_name,
                                                  delivery_place,
                                                  no_kladr_for_region_settlement_region,
                                                  no_kladr_for_region_settlement_settlement,
                                                  no_kladr_for_region_settlement
                                          ) 
                                          VALUES (
                                                  '$ntfCustomerRequirementId',
                                                  '$kladrPlaceKladrKladrType',
                                                  '$kladrPlaceKladrKladrCode',
                                                  '$kladrPlaceKladrFullName',
                                                  '$kladrPlaceCountryCountryCode',
                                                  '$kladrPlaceCountryCountryFullName',
                                                  '$kladrPlaceDeliveryPlace',
                                                  '$kladrPlaceNoKladrForRegionSettlementRegion',
                                                  '$kladrPlaceNoKladrForRegionSettlementSettlement',
                                                  '$kladrPlaceNoKladrForRegionSettlement'
                                          )")
                                    ->execute();
                                */
                            }
                            //*-----------Вставка данных в ntf_kladr_place
                        }
                    }
                    //*-----------ntf_kladr_place

                    //-----------ntf_nonbudget_financing
                    if (isset($customerRequirement->customerRequirement->nonbudgetFinancings->nonbudgetFinancing)) {
                        foreach ($customerRequirement->customerRequirement->nonbudgetFinancings->nonbudgetFinancing as $nonbudgetFinancing) {
                            $nonbudgetFinancingKosguCode = '';
                            $nonbudgetFinancingKvrCode = '';
                            $nonbudgetFinancingYear = '';
                            $nonbudgetFinancingSum = '';

                            if (isset($nonbudgetFinancing->kosguCode)) {
                                $nonbudgetFinancingKosguCode = (string)$nonbudgetFinancing->kosguCode;
                            }
                            if (isset($nonbudgetFinancing->kvrCode)) {
                                $nonbudgetFinancingKvrCode = (string)$nonbudgetFinancing->kvrCode;
                            }
                            if (isset($nonbudgetFinancing->year)) {
                                $nonbudgetFinancingYear = (string)$nonbudgetFinancing->year;
                            }
                            if (isset($nonbudgetFinancing->sum)) {
                                $nonbudgetFinancingSum = (string)$nonbudgetFinancing->sum;
                            }
                            //-----------Вставка данных в ntf_nonbudget_financing
                            if (
                                $nonbudgetFinancingKosguCode !== '' ||
                                $nonbudgetFinancingKvrCode !== '' ||
                                $nonbudgetFinancingYear !== '' ||
                                $nonbudgetFinancingSum !== ''
                            ) {
                                $sql = "
                                  INSERT INTO ntf_nonbudget_financing (
                                    ntf_customer_requirement_id,
                                    kosgu_code,
                                    kvr_code,
                                    year,
                                    sum
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :nonbudgetFinancingKosguCode,
                                    :nonbudgetFinancingKvrCode,
                                    :nonbudgetFinancingYear,
                                    :nonbudgetFinancingSum
                                )";
                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':nonbudgetFinancingKosguCode' => $nonbudgetFinancingKosguCode,
                                        ':nonbudgetFinancingKvrCode' => $nonbudgetFinancingKvrCode,
                                        ':nonbudgetFinancingYear' => $nonbudgetFinancingYear,
                                        ':nonbudgetFinancingSum' => $nonbudgetFinancingSum,
                                    ])
                                    ->execute();
                                /*
                                Yii::$app->db_zakupki->createCommand(
                                    "INSERT INTO ntf_nonbudget_financing
                                                 (
                                                  ntf_customer_requirement_id,
                                                  kosgu_code,
                                                  kvr_code,
                                                  year,
                                                  sum
                                          ) 
                                          VALUES (
                                                  '$ntfCustomerRequirementId',
                                                  '$nonbudgetFinancingKosguCode',
                                                  '$nonbudgetFinancingKvrCode',
                                                  '$nonbudgetFinancingYear',
                                                  '$nonbudgetFinancingSum'
                                          )")
                                    ->execute();
                                */
                            }
                            //-----------Вставка данных в ntf_nonbudget_financing
                        }
                    }
                    //*-----------ntf_nonbudget_financing

                    //-----------ntf_budget_financing
                    if (isset($customerRequirement->customerRequirement->budgetFinancings->budgetFinancing)) {
                        foreach ($customerRequirement->customerRequirement->budgetFinancings->budgetFinancing as $budgetFinancing) {
                            $budgetFinancingKbkCode = '';
                            $budgetFinancingKbkCode2016 = '';
                            $budgetFinancingYear = '';
                            $budgetFinancingSum = '';

                            if (isset($budgetFinancing->kbkCode)) {
                                $budgetFinancingKbkCode = (string)$budgetFinancing->kbkCode;
                            }
                            if (isset($budgetFinancing->kbkCode2016)) {
                                $budgetFinancingKbkCode2016 = (string)$budgetFinancing->kbkCode2016;
                            }
                            if (isset($budgetFinancing->year)) {
                                $budgetFinancingYear = (string)$budgetFinancing->year;
                            }
                            if (isset($budgetFinancing->sum)) {
                                $budgetFinancingSum = (string)$budgetFinancing->sum;
                            }
                            //-----------Вставка данных в ntf_budget_financing
                            if (
                                $budgetFinancingKbkCode !== '' ||
                                $budgetFinancingKbkCode2016 !== '' ||
                                $budgetFinancingYear !== '' ||
                                $budgetFinancingSum !== ''
                            ) {
                                $sql = "
                                  INSERT INTO ntf_budget_financing (
                                    ntf_customer_requirement_id,
                                    kbk_code,
                                    kbk_code2016,
                                    year,
                                    sum
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :budgetFinancingKbkCode,
                                    :budgetFinancingKbkCode2016,
                                    :budgetFinancingYear,
                                    :budgetFinancingSum
                                )";

                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':budgetFinancingKbkCode' => $budgetFinancingKbkCode,
                                        ':budgetFinancingKbkCode2016' => $budgetFinancingKbkCode2016,
                                        ':budgetFinancingYear' => $budgetFinancingYear,
                                        ':budgetFinancingSum' => $budgetFinancingSum,
                                    ])
                                    ->execute();
                                /*
                                Yii::$app->db_zakupki->createCommand(
                                    "INSERT INTO ntf_budget_financing
                                                 (
                                                  ntf_customer_requirement_id,
                                                  kbk_code,
                                                  kbk_code2016,
                                                  year,
                                                  sum
                                          ) 
                                          VALUES (
                                                  '$ntfCustomerRequirementId',
                                                  '$budgetFinancingKbkCode',
                                                  '$budgetFinancingKbkCode2016',
                                                  '$budgetFinancingYear',
                                                  '$budgetFinancingSum'
                                          )")
                                    ->execute();
                                */
                            }
                            //-----------Вставка данных в ntf_budget_financing
                        }
                    }
                    //*-----------ntf_budget_financing
                }
                //-----------ntf_purchase_object
                if (isset($xml->fcsNotificationEP->lot->purchaseObjects->purchaseObject)) {
                    foreach ($xml->fcsNotificationEP->lot->purchaseObjects->purchaseObject as $purchaseObject) {
                        $purchaseObjectOKPDCode = '';
                        $purchaseObjectOKPDName = '';
                        $purchaseObjectOKPD2Code = '';
                        $purchaseObjectOKPD2Name = '';
                        $purchaseObjectName = '';
                        $purchaseObjectOKEICode = '';
                        $purchaseObjectOKEINationalCode = '';
                        $purchaseObjectPrice = '';
                        $purchaseObjectQuantityValue = '';
                        $purchaseObjectQuantityUndefined = '';
                        $purchaseObjectSum = '';

                        if (isset($purchaseObject->OKPD->code)) {
                            $purchaseObjectOKPDCode = (string)$purchaseObject->OKPD->code;
                        }
                        if (isset($purchaseObject->OKPD->name)) {
                            $purchaseObjectOKPDName = (string)$purchaseObject->OKPD->name;
                        }
                        if (isset($purchaseObject->OKPD2->code)) {
                            $purchaseObjectOKPD2Code = (string)$purchaseObject->OKPD2->code;
                        }
                        if (isset($purchaseObject->OKPD2->name)) {
                            $purchaseObjectOKPD2Name = (string)$purchaseObject->OKPD2->name;
                        }
                        if (isset($purchaseObject->name)) {
                            $purchaseObjectName = (string)$purchaseObject->name;
                        }
                        if (isset($purchaseObject->OKEI->code)) {
                            $purchaseObjectOKEICode = (string)$purchaseObject->OKEI->code;
                        }
                        if (isset($purchaseObject->OKEI->nationalCode)) {
                            $purchaseObjectOKEINationalCode = (string)$purchaseObject->OKEI->nationalCode;
                        }
                        if (isset($purchaseObject->price)) {
                            $purchaseObjectPrice = (string)$purchaseObject->price;
                        }
                        if (isset($purchaseObject->quantity->value)) {
                            $purchaseObjectQuantityValue = (string)$purchaseObject->quantity->value;
                        }
                        if (isset($purchaseObject->quantity->undefined)) {
                            $purchaseObjectQuantityUndefined = (string)$purchaseObject->quantity->undefined;
                        }
                        if (isset($purchaseObject->sum)) {
                            $purchaseObjectSum = (string)$purchaseObject->sum;
                        }

                        //-----------Вставка данных в ntf_purchase_object
                        if (
                            $purchaseObjectOKPDCode !== '' ||
                            $purchaseObjectOKPDName !== '' ||
                            $purchaseObjectOKPD2Code !== '' ||
                            $purchaseObjectOKPD2Name !== '' ||
                            $purchaseObjectName !== '' ||
                            $purchaseObjectOKEICode !== '' ||
                            $purchaseObjectOKEINationalCode !== '' ||
                            $purchaseObjectPrice !== '' ||
                            $purchaseObjectQuantityValue !== '' ||
                            $purchaseObjectQuantityUndefined !== '' ||
                            $purchaseObjectSum !== ''
                        ) {
                            $sql = "
                              INSERT INTO ntf_purchase_object (
                                ntf_lot_id,
                                okpd_code,
                                okpd_name,
                                okpd2_code,
                                okpd2_name,
                                name,
                                okei_code,
                                okei_national_code,
                                price,
                                quantity_value,
                                quantity_undefined,
                                sum
                              ) 
                              VALUES(
                                :ntfLotId,
                                :purchaseObjectOKPDCode,
                                :purchaseObjectOKPDName,
                                :purchaseObjectOKPD2Code,
                                :purchaseObjectOKPD2Name,
                                :purchaseObjectName,
                                :purchaseObjectOKEICode,
                                :purchaseObjectOKEINationalCode,
                                :purchaseObjectPrice,
                                :purchaseObjectQuantityValue,
                                :purchaseObjectQuantityUndefined,
                                :purchaseObjectSum
                          )";
                            Yii::$app->db_zakupki->createCommand($sql)
                                ->bindValues([
                                    ':ntfLotId' => $ntfLotId,
                                    ':purchaseObjectOKPDCode' => $purchaseObjectOKPDCode,
                                    ':purchaseObjectOKPDName' => $purchaseObjectOKPDName,
                                    ':purchaseObjectOKPD2Code' => $purchaseObjectOKPD2Code,
                                    ':purchaseObjectOKPD2Name' => $purchaseObjectOKPD2Name,
                                    ':purchaseObjectName' => $purchaseObjectName,
                                    ':purchaseObjectOKEICode' => $purchaseObjectOKEICode,
                                    ':purchaseObjectOKEINationalCode' => $purchaseObjectOKEINationalCode,
                                    ':purchaseObjectPrice' => $purchaseObjectPrice,
                                    ':purchaseObjectQuantityValue' => $purchaseObjectQuantityValue,
                                    ':purchaseObjectQuantityUndefined' => $purchaseObjectQuantityUndefined,
                                    ':purchaseObjectSum' => $purchaseObjectSum,
                                ])
                                ->execute();

                            /*
                            Yii::$app->db_zakupki->createCommand(
                                "INSERT INTO ntf_purchase_object
                                                 (
                                                  ntf_lot_id,
                                                  okpd_code,
                                                  okpd_name,
                                                  okpd2_code,
                                                  okpd2_name,
                                                  name,
                                                  okei_code,
                                                  okei_national_code,
                                                  price,
                                                  quantity_value,
                                                  quantity_undefined,
                                                  sum
                                          ) 
                                          VALUES (
                                                  '$ntfLotId',
                                                  '$purchaseObjectOKPDCode',
                                                  '$purchaseObjectOKPDName',
                                                  '$purchaseObjectOKPD2Code',
                                                  '$purchaseObjectOKPD2Name',
                                                  '$purchaseObjectName',
                                                  '$purchaseObjectOKEICode',
                                                  '$purchaseObjectOKEINationalCode',
                                                  '$purchaseObjectPrice',
                                                  '$purchaseObjectQuantityValue',
                                                  '$purchaseObjectQuantityUndefined',
                                                  '$purchaseObjectSum'
                                          )")
                                ->execute();
                            */
                            $ntfPurchaseObjectId = Yii::$app->db_zakupki->getLastInsertID('ntf_purchase_object_id_seq');
                        }
                        //*-----------Вставка данных в ntf_purchase_object

                        //-----------ntf_customer_quantity
                        if (isset($purchaseObject->customerQuantities->customerQuantity)) {
                            foreach ($purchaseObject->customerQuantities->customerQuantity as $customerQuantity) {
                                $customerRegNum = '';
                                $customerConsRegistryNum = '';
                                $customerFullName = '';
                                $quantity = '';

                                if (isset($customerQuantity->customer->regNum)) {
                                    $customerRegNum = (string)$customerQuantity->customer->regNum;
                                }
                                if (isset($customerQuantity->customer->consRegistryNum)) {
                                    $customerConsRegistryNum = (string)$customerQuantity->customer->consRegistryNum;
                                }
                                if (isset($customerQuantity->customer->fullName)) {
                                    $customerFullName = (string)$customerQuantity->customer->fullName;
                                }
                                if (isset($customerQuantity->quantity)) {
                                    $quantity = (string)$customerQuantity->quantity;
                                }

                                //-----------Вставка данных в ntf_customer_quantity
                                if (
                                    $customerRegNum !== '' ||
                                    $customerConsRegistryNum !== '' ||
                                    $customerFullName !== '' ||
                                    $quantity !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_customer_quantity (
                                                  ntf_purchase_object_id,
                                                  customer_reg_num,
                                                  customer_cons_registry_num,
                                                  customer_full_name,
                                                  quantity
                                      ) 
                                      VALUES(
                                        :ntfPurchaseObjectId,
                                        :customerRegNum,
                                        :customerConsRegistryNum,
                                        :customerFullName,
                                        :quantity
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfPurchaseObjectId' => $ntfPurchaseObjectId,
                                            ':customerRegNum' => $customerRegNum,
                                            ':customerConsRegistryNum' => $customerConsRegistryNum,
                                            ':customerFullName' => $customerFullName,
                                            ':quantity' => $quantity,
                                        ])
                                        ->execute();
                                    /*
                                    Yii::$app->db_zakupki->createCommand(
                                        "INSERT INTO ntf_customer_quantity
                                                 (
                                                  ntf_purchase_object_id,
                                                  customer_reg_num,
                                                  customer_cons_registry_num,
                                                  customer_full_name,
                                                  quantity
                                          ) 
                                          VALUES (
                                                  '$ntfPurchaseObjectId',
                                                  '$customerRegNum',
                                                  '$customerConsRegistryNum',
                                                  '$customerFullName',
                                                  '$quantity'
                                          )")
                                        ->execute();
                                    */
                                }
                                //*-----------Вставка данных в ntf_customer_quantity
                            }
                        }
                        //-----------ntf_customer_quantity
                    }
                }
                //*-----------ntf_purchase_object

                //-----------ntf_drug_purchase_object_info
                if (isset($xml->fcsNotificationEP->lot->drugPurchaseObjectsInfo->drugPurchaseObjectInfo)) {
                    foreach ($xml->fcsNotificationEP->lot->drugPurchaseObjectsInfo->drugPurchaseObjectInfo as $drugPurchaseObjectInfo) {
                        $isZNVLP = '';
                        $drugQuantityCustomersInfoTotal = '';
                        $pricePerUnit = '';
                        $positionPrice = '';

                        if (isset($drugPurchaseObjectInfo->isZNVLP)) {
                            $isZNVLP = (string)$drugPurchaseObjectInfo->isZNVLP;
                        }
                        if (isset($drugPurchaseObjectInfo->pricePerUnit)) {
                            $pricePerUnit = (string)$drugPurchaseObjectInfo->pricePerUnit;
                        }
                        if (isset($drugPurchaseObjectInfo->positionPrice)) {
                            $positionPrice = (string)$drugPurchaseObjectInfo->positionPrice;
                        }
                        if (isset($drugPurchaseObjectInfo->positionPrice->drugQuantityCustomersInfo->total)) {
                            $drugQuantityCustomersInfoTotal = (string)$drugPurchaseObjectInfo->positionPrice->drugQuantityCustomersInfo->total;
                        }
                        //-----------Вставка данных в ntf_drug_purchase_object_info
                        if (
                            $isZNVLP !== '' ||
                            $drugQuantityCustomersInfoTotal !== '' ||
                            $pricePerUnit !== '' ||
                            $positionPrice !== ''
                        ) {
                            $sql = "
                              INSERT INTO ntf_drug_purchase_object_info (
                                ntf_lot_id,
                                isznvlp,
                                drug_quantity_customers_info_total,
                                price_per_unit,
                                position_price
                              ) 
                              VALUES(
                                :ntfLotId,
                                :isZNVLP,
                                :drugQuantityCustomersInfoTotal,
                                :pricePerUnit,
                                :positionPrice
                            )";
                            Yii::$app->db_zakupki->createCommand($sql)
                                ->bindValues([
                                    ':ntfLotId' => $ntfLotId,
                                    ':isZNVLP' => $isZNVLP,
                                    ':drugQuantityCustomersInfoTotal' => $drugQuantityCustomersInfoTotal,
                                    ':pricePerUnit' => $pricePerUnit,
                                    ':positionPrice' => $positionPrice,
                                ])
                                ->execute();
                            /*
                            Yii::$app->db_zakupki->createCommand(
                                "INSERT INTO ntf_drug_purchase_object_info
                                                 (
                                                  ntf_lot_id,
                                                  isznvlp,
                                                  drug_quantity_customers_info_total,
                                                  price_per_unit,
                                                  position_price
                                          ) 
                                          VALUES (
                                                  '$ntfLotId',
                                                  '$isZNVLP',
                                                  '$drugQuantityCustomersInfoTotal',
                                                  '$pricePerUnit',
                                                  '$positionPrice'
                                          )")
                                ->execute();
                            */
                        }
                        $ntfDrugPurchaseObjectInfoId = Yii::$app->db_zakupki->getLastInsertID('ntf_drug_purchase_object_info_id_seq');
                        //*-----------Вставка данных в ntf_drug_purchase_object_info

                        //-----------ntf_drug_quantity_customer_info
                        if (isset($drugPurchaseObjectInfo->drugQuantityCustomersInfo->drugQuantityCustomerInfo)) {
                            foreach ($drugPurchaseObjectInfo->drugQuantityCustomersInfo->drugQuantityCustomerInfo as $drugQuantityCustomerInfo) {
                                $drugQuantityCustomerInfoCustomerRegNum = '';
                                $drugQuantityCustomerInfoCustomerConsRegistryNum = '';
                                $drugQuantityCustomerInfoCustomerFullName = '';
                                $drugQuantityCustomerInfoQuantity = '';
                                if (isset($drugQuantityCustomerInfo->customer->regNum)) {
                                    $drugQuantityCustomerInfoCustomerRegNum = (string)$drugQuantityCustomerInfo->customer->regNum;
                                }
                                if (isset($drugQuantityCustomerInfo->customer->consRegistryNum)) {
                                    $drugQuantityCustomerInfoCustomerConsRegistryNum = (string)$drugQuantityCustomerInfo->customer->consRegistryNum;
                                }
                                if (isset($drugQuantityCustomerInfo->customer->fullName)) {
                                    $drugQuantityCustomerInfoCustomerFullName = (string)$drugQuantityCustomerInfo->customer->fullName;
                                }
                                if (isset($drugQuantityCustomerInfo->quantity)) {
                                    $drugQuantityCustomerInfoQuantity = (string)$drugQuantityCustomerInfo->quantity;
                                }
                                //-----------Вставка данных в ntf_drug_quantity_customer_info
                                if (
                                    $drugQuantityCustomerInfoCustomerRegNum !== '' ||
                                    $drugQuantityCustomerInfoCustomerConsRegistryNum !== '' ||
                                    $drugQuantityCustomerInfoCustomerFullName !== '' ||
                                    $drugQuantityCustomerInfoQuantity !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_drug_quantity_customer_info (
                                        ntf_drug_purchase_object_info_id,
                                        customer_reg_num,
                                        customer_cons_registry_num,
                                        customer_full_name,
                                        quantity
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :drugQuantityCustomerInfoCustomerRegNum,
                                        :drugQuantityCustomerInfoCustomerConsRegistryNum,
                                        :drugQuantityCustomerInfoCustomerFullName,
                                        :drugQuantityCustomerInfoQuantity
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':drugQuantityCustomerInfoCustomerRegNum' => $drugQuantityCustomerInfoCustomerRegNum,
                                            ':drugQuantityCustomerInfoCustomerConsRegistryNum' => $drugQuantityCustomerInfoCustomerConsRegistryNum,
                                            ':drugQuantityCustomerInfoCustomerFullName' => $drugQuantityCustomerInfoCustomerFullName,
                                            ':drugQuantityCustomerInfoQuantity' => $drugQuantityCustomerInfoQuantity,
                                        ])
                                        ->execute();
                                    /*
                                    Yii::$app->db_zakupki->createCommand(
                                        "INSERT INTO ntf_drug_quantity_customer_info
                                                 (
                                                  ntf_drug_purchase_object_info_id,
                                                  customer_reg_num,
                                                  customer_cons_registry_num,
                                                  customer_full_name,
                                                  quantity
                                          ) 
                                          VALUES (
                                                  '$ntfDrugPurchaseObjectInfoId',
                                                  '$drugQuantityCustomerInfoCustomerRegNum',
                                                  '$drugQuantityCustomerInfoCustomerConsRegistryNum',
                                                  '$drugQuantityCustomerInfoCustomerFullName',
                                                  '$drugQuantityCustomerInfoQuantity'
                                          )")
                                        ->execute();
                                    */
                                }
                                //*-----------Вставка данных в ntf_drug_quantity_customer_info
                            }
                        }
                        //*-----------ntf_drug_quantity_customer_info

                        //-----------ntf_object_info_using_reference_info
                        if (isset($drugPurchaseObjectInfo->objectInfoUsingReferenceInfo->drugsInfo->drugInfo)) {
                            foreach ($drugPurchaseObjectInfo->objectInfoUsingReferenceInfo->drugsInfo->drugInfo as $drugInfo) {
                                $referenceInfoMNNExternalCode = '';
                                $referenceInfoMNNName = '';
                                $referenceInfoPositionTradeNameExternalCode = '';
                                $referenceInfoTradeName = '';
                                $referenceInfoMedicamentalFormName = '';
                                $referenceInfoDosageGRLSValue = '';
                                $referenceInfoDosageUserOKEICode = '';
                                $referenceInfoDosageUserOKEIName = '';
                                $referenceInfoPackaging1Quantity = '';
                                $referenceInfoPackaging2Quantity = '';
                                $referenceInfoSummaryPackagingQuantity = '';
                                $referenceInfoManualUserOKEICode = '';
                                $referenceInfoManualUserOKEIName = '';
                                $referenceInfoBasicUnit = '';
                                $referenceInfoDrugQuantity = '';
                                $referenceInfoSpecifyDrugPackageReason = '';

                                if (isset($drugInfo->MNNInfo->MNNExternalCode)) {
                                    $referenceInfoMNNExternalCode = (string)$drugInfo->MNNInfo->MNNExternalCode;
                                }
                                if (isset($drugInfo->MNNInfo->MNNName)) {
                                    $referenceInfoMNNName = (string)$drugInfo->MNNInfo->MNNName;
                                }
                                if (isset($drugInfo->tradeInfo->positionTradeNameExternalCode)) {
                                    $referenceInfoPositionTradeNameExternalCode = (string)$drugInfo->tradeInfo->positionTradeNameExternalCode;
                                }
                                if (isset($drugInfo->tradeInfo->tradeName)) {
                                    $referenceInfoTradeName = (string)$drugInfo->tradeInfo->tradeName;
                                }
                                if (isset($drugInfo->medicamentalFormInfo->medicamentalFormName)) {
                                    $referenceInfoMedicamentalFormName = (string)$drugInfo->medicamentalFormInfo->medicamentalFormName;
                                }
                                if (isset($drugInfo->dosageInfo->dosageGRLSValue)) {
                                    $referenceInfoDosageGRLSValue = (string)$drugInfo->dosageInfo->dosageGRLSValue;
                                }
                                if (isset($drugInfo->dosageInfo->dosageUserOKEI->code)) {
                                    $referenceInfoDosageUserOKEICode = (string)$drugInfo->dosageInfo->dosageUserOKEI->code;
                                }
                                if (isset($drugInfo->dosageInfo->dosageUserOKEI->name)) {
                                    $referenceInfoDosageUserOKEIName = (string)$drugInfo->dosageInfo->dosageUserOKEI->name;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->packaging1Quantity)) {
                                    $referenceInfoPackaging1Quantity = (string)$drugInfo->dosageInfo->packagingInfo->packaging1Quantity;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->packaging2Quantity)) {
                                    $referenceInfoPackaging2Quantity = (string)$drugInfo->dosageInfo->packagingInfo->packaging2Quantity;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity)) {
                                    $referenceInfoSummaryPackagingQuantity = (string)$drugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity;
                                }
                                if (isset($drugInfo->dosageInfo->manualUserOKEI->code)) {
                                    $referenceInfoManualUserOKEICode = (string)$drugInfo->dosageInfo->manualUserOKEI->code;
                                }
                                if (isset($drugInfo->dosageInfo->manualUserOKEI->name)) {
                                    $referenceInfoManualUserOKEIName = (string)$drugInfo->dosageInfo->manualUserOKEI->name;
                                }
                                if (isset($drugInfo->basicUnit)) {
                                    $referenceInfoBasicUnit = (string)$drugInfo->basicUnit;
                                }
                                if (isset($drugInfo->drugQuantity)) {
                                    $referenceInfoDrugQuantity = (string)$drugInfo->drugQuantity;
                                }
                                if (isset($drugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason)) {
                                    $referenceInfoSpecifyDrugPackageReason = (string)$drugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason;
                                }
                                //-----------Вставка данных в ntf_object_info_using_reference_info
                                if (
                                    $referenceInfoMNNExternalCode !== '' ||
                                    $referenceInfoMNNName !== '' ||
                                    $referenceInfoPositionTradeNameExternalCode !== '' ||
                                    $referenceInfoTradeName !== '' ||
                                    $referenceInfoMedicamentalFormName !== '' ||
                                    $referenceInfoDosageGRLSValue !== '' ||
                                    $referenceInfoDosageUserOKEICode !== '' ||
                                    $referenceInfoDosageUserOKEIName !== '' ||
                                    $referenceInfoPackaging1Quantity !== '' ||
                                    $referenceInfoPackaging2Quantity !== '' ||
                                    $referenceInfoSummaryPackagingQuantity !== '' ||
                                    $referenceInfoManualUserOKEICode !== '' ||
                                    $referenceInfoManualUserOKEIName !== '' ||
                                    $referenceInfoBasicUnit !== '' ||
                                    $referenceInfoDrugQuantity !== '' ||
                                    $referenceInfoSpecifyDrugPackageReason !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_object_info_using_reference_info (
                                        ntf_drug_purchase_object_info_id,
                                        drug_info_mmn_info_mmn_external_code,
                                        drug_info_mmn_info_mmn_name,
                                        drug_info_trade_info_position_trade_name_external_code,
                                        drug_info_trade_info_trade_name,
                                        drug_info_medicamental_form_info_medicamental_form_name,
                                        drug_info_dosage_info_dosage_grls_value,
                                        drug_info_dosage_info_dosage_user_okei_code,
                                        drug_info_dosage_info_dosage_user_okei_name,
                                        drug_info_packaging_info_packaging1_quantity,
                                        drug_info_packaging_info_packaging2_quantity,
                                        drug_info_packaging_info_summary_packaging_quantity,
                                        drug_info_manual_user_okei_code,
                                        drug_info_manual_user_okei_name,
                                        drug_info_basic_unit,
                                        drug_info_drug_quantity,
                                        must_specify_drug_package_specify_drug_package_reason
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :referenceInfoMNNExternalCode,
                                        :referenceInfoMNNName,
                                        :referenceInfoPositionTradeNameExternalCode,
                                        :referenceInfoTradeName,
                                        :referenceInfoMedicamentalFormName,
                                        :referenceInfoDosageGRLSValue,
                                        :referenceInfoDosageUserOKEICode,
                                        :referenceInfoDosageUserOKEIName,
                                        :referenceInfoPackaging1Quantity,
                                        :referenceInfoPackaging2Quantity,
                                        :referenceInfoSummaryPackagingQuantity,
                                        :referenceInfoManualUserOKEICode,
                                        :referenceInfoManualUserOKEIName,
                                        :referenceInfoBasicUnit,
                                        :referenceInfoDrugQuantity,
                                        :referenceInfoSpecifyDrugPackageReason
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':referenceInfoMNNExternalCode' => $referenceInfoMNNExternalCode,
                                            ':referenceInfoMNNName' => $referenceInfoMNNName,
                                            ':referenceInfoPositionTradeNameExternalCode' => $referenceInfoPositionTradeNameExternalCode,
                                            ':referenceInfoTradeName' => $referenceInfoTradeName,
                                            ':referenceInfoMedicamentalFormName' => $referenceInfoMedicamentalFormName,
                                            ':referenceInfoDosageGRLSValue' => $referenceInfoDosageGRLSValue,
                                            ':referenceInfoDosageUserOKEICode' => $referenceInfoDosageUserOKEICode,
                                            ':referenceInfoDosageUserOKEIName' => $referenceInfoDosageUserOKEIName,
                                            ':referenceInfoPackaging1Quantity' => $referenceInfoPackaging1Quantity,
                                            ':referenceInfoPackaging2Quantity' => $referenceInfoPackaging2Quantity,
                                            ':referenceInfoSummaryPackagingQuantity' => $referenceInfoSummaryPackagingQuantity,
                                            ':referenceInfoManualUserOKEICode' => $referenceInfoManualUserOKEICode,
                                            ':referenceInfoManualUserOKEIName' => $referenceInfoManualUserOKEIName,
                                            ':referenceInfoBasicUnit' => $referenceInfoBasicUnit,
                                            ':referenceInfoDrugQuantity' => $referenceInfoDrugQuantity,
                                            ':referenceInfoSpecifyDrugPackageReason' => $referenceInfoSpecifyDrugPackageReason,
                                        ])
                                        ->execute();
                                    /*
                                    Yii::$app->db_zakupki->createCommand(
                                        "INSERT INTO ntf_object_info_using_reference_info
                                                 (
                                                  ntf_drug_purchase_object_info_id,
                                                  drug_info_mmn_info_mmn_external_code,
                                                  drug_info_mmn_info_mmn_name,
                                                  drug_info_trade_info_position_trade_name_external_code,
                                                  drug_info_trade_info_trade_name,
                                                  drug_info_medicamental_form_info_medicamental_form_name,
                                                  drug_info_dosage_info_dosage_grls_value,
                                                  drug_info_dosage_info_dosage_user_okei_code,
                                                  drug_info_dosage_info_dosage_user_okei_name,
                                                  drug_info_packaging_info_packaging1_quantity,
                                                  drug_info_packaging_info_packaging2_quantity,
                                                  drug_info_packaging_info_summary_packaging_quantity,
                                                  drug_info_manual_user_okei_code,
                                                  drug_info_manual_user_okei_name,
                                                  drug_info_basic_unit,
                                                  drug_info_drug_quantity,
                                                  must_specify_drug_package_specify_drug_package_reason
                                          ) 
                                          VALUES (
                                                  '$ntfDrugPurchaseObjectInfoId',
                                                  '$referenceInfoMNNExternalCode',
                                                  '$referenceInfoMNNName',
                                                  '$referenceInfoPositionTradeNameExternalCode',
                                                  '$referenceInfoTradeName',
                                                  '$referenceInfoMedicamentalFormName',
                                                  '$referenceInfoDosageGRLSValue',
                                                  '$referenceInfoDosageUserOKEICode',
                                                  '$referenceInfoDosageUserOKEIName',
                                                  '$referenceInfoPackaging1Quantity',
                                                  '$referenceInfoPackaging2Quantity',
                                                  '$referenceInfoSummaryPackagingQuantity',
                                                  '$referenceInfoManualUserOKEICode',
                                                  '$referenceInfoManualUserOKEIName',
                                                  '$referenceInfoBasicUnit',
                                                  '$referenceInfoDrugQuantity',
                                                  '$referenceInfoSpecifyDrugPackageReason'
                                          )")
                                        ->execute();
                                    */
                                }
                                //*-----------Вставка данных в ntf_object_info_using_reference_info
                            }
                        }
                        //*-----------ntf_object_info_using_reference_info

                        //-----------ntf_object_info_using_text_form
                        if (isset($drugPurchaseObjectInfo->objectInfoUsingTextForm->drugsInfo->drugInfo)) {
                            foreach ($drugPurchaseObjectInfo->objectInfoUsingTextForm->drugsInfo->drugInfo as $textFormDrugInfo) {
                                $textFormMNNName = '';
                                $textFormTradeName = '';
                                $textFormMedicamentalFormName = '';
                                $textFormDosageGRLSValue = '';
                                $textFormPackaging1Quantity = '';
                                $textFormPackaging2Quantity = '';
                                $textFormSummaryPackagingQuantity = '';
                                $textFormManualUserOKEICode = '';
                                $textFormManualUserOKEIName = '';
                                $textFormBasicUnit = '';
                                $textFormDrugQuantity = '';
                                $textFormSpecifyDrugPackageReason = '';

                                if (isset($textFormDrugInfo->MNNInfo->MNNName)) {
                                    $textFormMNNName = (string)$textFormDrugInfo->MNNInfo->MNNName;
                                }
                                if (isset($textFormDrugInfo->tradeInfo->tradeName)) {
                                    $textFormTradeName = (string)$textFormDrugInfo->tradeInfo->tradeName;
                                }
                                if (isset($textFormDrugInfo->medicamentalFormInfo->medicamentalFormName)) {
                                    $textFormMedicamentalFormName = (string)$textFormDrugInfo->medicamentalFormInfo->medicamentalFormName;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->dosageGRLSValue)) {
                                    $textFormDosageGRLSValue = (string)$textFormDrugInfo->dosageInfo->dosageGRLSValue;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->packaging1Quantity)) {
                                    $textFormPackaging1Quantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->packaging1Quantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->packaging2Quantity)) {
                                    $textFormPackaging2Quantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->packaging2Quantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity)) {
                                    $textFormSummaryPackagingQuantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->manualUserOKEI->code)) {
                                    $textFormManualUserOKEICode = (string)$textFormDrugInfo->dosageInfo->manualUserOKEI->code;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->manualUserOKEI->name)) {
                                    $textFormManualUserOKEIName = (string)$textFormDrugInfo->dosageInfo->manualUserOKEI->name;
                                }
                                if (isset($textFormDrugInfo->basicUnit)) {
                                    $textFormBasicUnit = (string)$textFormDrugInfo->basicUnit;
                                }
                                if (isset($textFormDrugInfo->drugQuantity)) {
                                    $textFormDrugQuantity = (string)$textFormDrugInfo->drugQuantity;
                                }
                                if (isset($textFormDrugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason)) {
                                    $textFormSpecifyDrugPackageReason = (string)$textFormDrugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason;
                                }
                                //-----------Вставка данных в ntf_object_info_using_text_form
                                if (
                                    $textFormMNNName !== '' ||
                                    $textFormTradeName !== '' ||
                                    $textFormMedicamentalFormName !== '' ||
                                    $textFormDosageGRLSValue !== '' ||
                                    $textFormPackaging1Quantity !== '' ||
                                    $textFormPackaging2Quantity !== '' ||
                                    $textFormSummaryPackagingQuantity !== '' ||
                                    $textFormManualUserOKEICode !== '' ||
                                    $textFormManualUserOKEIName !== '' ||
                                    $textFormBasicUnit !== '' ||
                                    $textFormDrugQuantity !== '' ||
                                    $textFormSpecifyDrugPackageReason !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_object_info_using_text_form (
                                        ntf_drug_purchase_object_info_id,
                                        drug_info_mmn_info_mmn_name,
                                        drug_info_trade_info_trade_name,
                                        drug_info_medicamental_form_info_medicamental_form_name,
                                        drug_info_dosage_info_dosage_grls_value,
                                        drug_info_packaging_info_packaging1_quantity,
                                        drug_info_packaging_info_packaging2_quantity,
                                        drug_info_packaging_info_summary_packaging_quantity,
                                        drug_info_manual_user_okei_code,
                                        drug_info_manual_user_okei_name,
                                        drug_info_basic_unit,
                                        drug_info_drug_quantity,
                                        must_specify_drug_package_specify_drug_package_reason
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :textFormMNNName,
                                        :textFormTradeName,
                                        :textFormMedicamentalFormName,
                                        :textFormDosageGRLSValue,
                                        :textFormPackaging1Quantity,
                                        :textFormPackaging2Quantity,
                                        :textFormSummaryPackagingQuantity,
                                        :textFormManualUserOKEICode,
                                        :textFormManualUserOKEIName,
                                        :textFormBasicUnit,
                                        :textFormDrugQuantity,
                                        :textFormSpecifyDrugPackageReason
                                    )";

                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':textFormMNNName' => $textFormMNNName,
                                            ':textFormTradeName' => $textFormTradeName,
                                            ':textFormMedicamentalFormName' => $textFormMedicamentalFormName,
                                            ':textFormDosageGRLSValue' => $textFormDosageGRLSValue,
                                            ':textFormPackaging1Quantity' => $textFormPackaging1Quantity,
                                            ':textFormPackaging2Quantity' => $textFormPackaging2Quantity,
                                            ':textFormSummaryPackagingQuantity' => $textFormSummaryPackagingQuantity,
                                            ':textFormManualUserOKEICode' => $textFormManualUserOKEICode,
                                            ':textFormManualUserOKEIName' => $textFormManualUserOKEIName,
                                            ':textFormBasicUnit' => $textFormBasicUnit,
                                            ':textFormDrugQuantity' => $textFormDrugQuantity,
                                            ':textFormSpecifyDrugPackageReason' => $textFormSpecifyDrugPackageReason,
                                        ])
                                        ->execute();
                                    /*
                                    Yii::$app->db_zakupki->createCommand(
                                        "INSERT INTO ntf_object_info_using_text_form
                                                 (
                                                  ntf_drug_purchase_object_info_id,
                                                  drug_info_mmn_info_mmn_name,
                                                  drug_info_trade_info_trade_name,
                                                  drug_info_medicamental_form_info_medicamental_form_name,
                                                  drug_info_dosage_info_dosage_grls_value,
                                                  drug_info_packaging_info_packaging1_quantity,
                                                  drug_info_packaging_info_packaging2_quantity,
                                                  drug_info_packaging_info_summary_packaging_quantity,
                                                  drug_info_manual_user_okei_code,
                                                  drug_info_manual_user_okei_name,
                                                  drug_info_basic_unit,
                                                  drug_info_drug_quantity,
                                                  must_specify_drug_package_specify_drug_package_reason
                                          )
                                          VALUES (
                                                  '$ntfDrugPurchaseObjectInfoId',
                                                  '$textFormMNNName',
                                                  '$textFormTradeName',
                                                  '$textFormMedicamentalFormName',
                                                  '$textFormDosageGRLSValue',
                                                  '$textFormPackaging1Quantity',
                                                  '$textFormPackaging2Quantity',
                                                  '$textFormSummaryPackagingQuantity',
                                                  '$textFormManualUserOKEICode',
                                                  '$textFormManualUserOKEIName',
                                                  '$textFormBasicUnit',
                                                  '$textFormDrugQuantity',
                                                  '$textFormSpecifyDrugPackageReason'
                                          )")
                                        ->execute();
                                    */
                                }
                                //*-----------Вставка данных в ntf_object_info_using_text_form
                            }
                        }
                        //*-----------ntf_object_info_using_text_form
                    }
                }
                //*-----------ntf_drug_purchase_object_info
                //-----------Вставка данных в ntf_purchase_responsible_responsible_org
                if (
                    $responsibleOrgRegNum !== '' ||
                    $responsibleOrgConsRegistryNum !== '' ||
                    $responsibleOrgFullName !== '' ||
                    $responsibleOrgShortName !== '' ||
                    $responsibleOrgPostAddress !== '' ||
                    $responsibleOrgFactAddress !== '' ||
                    $responsibleOrgInn !== '' ||
                    $responsibleOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_responsible_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :responsibleOrgRegNum,
                        :responsibleOrgConsRegistryNum,
                        :responsibleOrgFullName,
                        :responsibleOrgShortName,
                        :responsibleOrgPostAddress,
                        :responsibleOrgFactAddress,
                        :responsibleOrgInn,
                        :responsibleOrgKpp
                    )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':responsibleOrgRegNum' => $responsibleOrgRegNum,
                            ':responsibleOrgConsRegistryNum' => $responsibleOrgConsRegistryNum,
                            ':responsibleOrgFullName' => $responsibleOrgFullName,
                            ':responsibleOrgShortName' => $responsibleOrgShortName,
                            ':responsibleOrgPostAddress' => $responsibleOrgPostAddress,
                            ':responsibleOrgFactAddress' => $responsibleOrgFactAddress,
                            ':responsibleOrgInn' => $responsibleOrgInn,
                            ':responsibleOrgKpp' => $responsibleOrgKpp,
                        ])
                        ->execute();
                    /*
                    Yii::$app->db_zakupki->createCommand(
                        "INSERT INTO ntf_purchase_responsible_responsible_org
                             (
                              ntf_main_id,
                              reg_num,
                              cons_registry_num,
                              full_name,
                              short_name,
                              post_address,
                              fact_address,
                              inn,
                              kpp
                      )
                      VALUES (
                              '$ntfMainId',
                              '$responsibleOrgRegNum',
                              '$responsibleOrgConsRegistryNum',
                              '$responsibleOrgFullName',
                              '$responsibleOrgShortName',
                              '$responsibleOrgPostAddress',
                              '$responsibleOrgFactAddress',
                              '$responsibleOrgInn',
                              '$responsibleOrgKpp'
                      )")
                        ->execute();
                    */
                }
                //*-----------Вставка данных в ntf_purchase_responsible_responsible_org

                //-----------Вставка данных в ntf_purchase_responsible_responsible_info
                if (
                    $responsibleInfoOrgPostAddress !== '' ||
                    $responsibleInfoOrgFactAddress !== '' ||
                    $contactPersonLastName !== '' ||
                    $contactPersonFirstName !== '' ||
                    $contactPersonMiddleName !== '' ||
                    $responsibleInfoContactEMail !== '' ||
                    $responsibleInfoContactPhone !== '' ||
                    $responsibleInfoContactFax !== '' ||
                    $responsibleInfoAddInfo !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_responsible_info (
                        ntf_main_id,
                        org_post_address,
                        org_fact_address,
                        contact_person_last_name,
                        contact_person_first_name,
                        contact_person_middle_name,
                        contact_email,
                        contact_phone,
                        contact_fax,
                        add_info
                      ) 
                      VALUES(
                        :ntfMainId,
                        :responsibleInfoOrgPostAddress,
                        :responsibleInfoOrgFactAddress,
                        :contactPersonLastName,
                        :contactPersonFirstName,
                        :contactPersonMiddleName,
                        :responsibleInfoContactEMail,
                        :responsibleInfoContactPhone,
                        :responsibleInfoContactFax,
                        :responsibleInfoAddInfo
                    )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':responsibleInfoOrgPostAddress' => $responsibleInfoOrgPostAddress,
                            ':responsibleInfoOrgFactAddress' => $responsibleInfoOrgFactAddress,
                            ':contactPersonLastName' => $contactPersonLastName,
                            ':contactPersonFirstName' => $contactPersonFirstName,
                            ':contactPersonMiddleName' => $contactPersonMiddleName,
                            ':responsibleInfoContactEMail' => $responsibleInfoContactEMail,
                            ':responsibleInfoContactPhone' => $responsibleInfoContactPhone,
                            ':responsibleInfoContactFax' => $responsibleInfoContactFax,
                            ':responsibleInfoAddInfo' => $responsibleInfoAddInfo,
                        ])
                        ->execute();
                    /*
                    Yii::$app->db_zakupki->createCommand(
                        "INSERT INTO ntf_purchase_responsible_responsible_info
                             (
                              ntf_main_id,
                              org_post_address,
                              org_fact_address,
                              contact_person_last_name,
                              contact_person_first_name,
                              contact_person_middle_name,
                              contact_email,
                              contact_phone,
                              contact_fax,
                              add_info
                      ) 
                      VALUES (
                              '$ntfMainId',
                              '$responsibleInfoOrgPostAddress',
                              '$responsibleInfoOrgFactAddress',
                              '$contactPersonLastName',
                              '$contactPersonFirstName',
                              '$contactPersonMiddleName',
                              '$responsibleInfoContactEMail',
                              '$responsibleInfoContactPhone',
                              '$responsibleInfoContactFax',
                              '$responsibleInfoAddInfo'
                      )")
                        ->execute();
                    */
                }
                //*-----------Вставка данных в ntf_purchase_responsible_responsible_info

                //-----------Вставка данных в ntf_purchase_responsible_specialized_org
                if (
                    $specializedOrgRegNum !== '' ||
                    $specializedOrgConsRegistryNum !== '' ||
                    $specializedOrgFullName !== '' ||
                    $specializedOrgShortName !== '' ||
                    $specializedOrgPostAddress !== '' ||
                    $specializedOrgFactAddress !== '' ||
                    $specializedOrgInn !== '' ||
                    $specializedOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_specialized_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :specializedOrgRegNum,
                        :specializedOrgConsRegistryNum,
                        :specializedOrgFullName,
                        :specializedOrgShortName,
                        :specializedOrgPostAddress,
                        :specializedOrgFactAddress,
                        :specializedOrgInn,
                        :specializedOrgKpp
                        )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':specializedOrgRegNum' => $specializedOrgRegNum,
                            ':specializedOrgConsRegistryNum' => $specializedOrgConsRegistryNum,
                            ':specializedOrgFullName' => $specializedOrgFullName,
                            ':specializedOrgShortName' => $specializedOrgShortName,
                            ':specializedOrgPostAddress' => $specializedOrgPostAddress,
                            ':specializedOrgFactAddress' => $specializedOrgFactAddress,
                            ':specializedOrgInn' => $specializedOrgInn,
                            ':specializedOrgKpp' => $specializedOrgKpp,
                        ])
                        ->execute();
                    /*
                    Yii::$app->db_zakupki->createCommand(
                        "INSERT INTO ntf_purchase_responsible_specialized_org
                             (
                              ntf_main_id,
                              reg_num,
                              cons_registry_num,
                              full_name,
                              short_name,
                              post_address,
                              fact_address,
                              inn,
                              kpp
                      ) 
                      VALUES (
                              '$ntfMainId',
                              '$specializedOrgRegNum',
                              '$specializedOrgConsRegistryNum',
                              '$specializedOrgFullName',
                              '$specializedOrgShortName',
                              '$specializedOrgPostAddress',
                              '$specializedOrgFactAddress',
                              '$specializedOrgInn',
                              '$specializedOrgKpp'
                      )")
                        ->execute();
                    */
                }
                //*-----------Вставка данных в ntf_purchase_responsible_specialized_org

                //-----------Вставка данных в ntf_public_discussion
                if (
                    $publicDiscussionNumber !== '' ||
                    $publicDiscussionOrganizationCh5St15 !== '' ||
                    $publicDiscussionHref !== '' ||
                    $publicDiscussionPlace !== '' ||
                    $publicDiscussion2017ProtocolDate !== '' ||
                    $publicDiscussion2017ProtocolPublishDate !== '' ||
                    $publicDiscussion2017PublicDiscussionPhase2Num !== '' ||
                    $publicDiscussion2017HrefPhase2 !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_public_discussion (
                            ntf_lot_id,
                            number,
                            organization_ch5_st15,
                            href,
                            place,
                            public_discussion2017_p_d_l_p_p2_protocol_date,
                            public_discussion2017_p_d_l_p_p2_protocol_publish_date,
                            public_discussion2017_p_d_l_p_p2_public_discussion_phase2_num,
                            public_discussion2017_p_d_l_p_p2_href_phase2
                          ) 
                          VALUES(
                            :ntfLotId,
                            :publicDiscussionNumber,
                            :publicDiscussionOrganizationCh5St15,
                            :publicDiscussionHref,
                            :publicDiscussionPlace,
                            :publicDiscussion2017ProtocolDate,
                            :publicDiscussion2017ProtocolPublishDate,
                            :publicDiscussion2017PublicDiscussionPhase2Num,
                            :publicDiscussion2017HrefPhase2
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfLotId' => $ntfLotId,
                            ':publicDiscussionNumber' => $publicDiscussionNumber,
                            ':publicDiscussionOrganizationCh5St15' => $publicDiscussionOrganizationCh5St15,
                            ':publicDiscussionHref' => $publicDiscussionHref,
                            ':publicDiscussionPlace' => $publicDiscussionPlace,
                            ':publicDiscussion2017ProtocolDate' => $publicDiscussion2017ProtocolDate,
                            ':publicDiscussion2017ProtocolPublishDate' => $publicDiscussion2017ProtocolPublishDate,
                            ':publicDiscussion2017PublicDiscussionPhase2Num' => $publicDiscussion2017PublicDiscussionPhase2Num,
                            ':publicDiscussion2017HrefPhase2' => $publicDiscussion2017HrefPhase2,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_public_discussion

                //-----------Вставка данных в ntf_placing_way
                if (
                    $placingWayCode !== '' ||
                    $placingWayName !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_placing_way (
                            ntf_main_id,
                            code,
                            name
                          ) 
                          VALUES(
                            :ntfMainId,
                            :placingWayCode,
                            :placingWayName
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':placingWayCode' => $placingWayCode,
                            ':placingWayName' => $placingWayName,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_placing_way
            }
        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }
    }

    public static function parseTypeZK($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);
        if ($xml) {
            //print_r($xml);
            //die;
            //--------ntf_main variables
            $ntfId = '';
            $ntfType = "fcsNotificationZK44";
            $externalId = '';
            $purchaseNumber = '';
            $directDate = '';
            $docPublishDate = '';
            $docNumber = '';
            $href = '';
            $purchaseObjectInfo = '';
            $purchaseResponsibleResponsibleRole = '';
            $okpd2okved2 = '';
            $contractServiceInfo = '';
            $filename = basename($file);
            //*--------ntf_main variables

            //--------ntf_print_form variables
            $printFormUrl = '';
            //$printFormSignature = '';
            //*--------ntf_print_form variables

            //--------ntf_extprint_form variables
            $extPrintFormContent = '';
            $extPrintFormUrl = '';
            $extPrintFormFileType = '';
            $extPrintFormSignatureType = '';
            $extPrintFormControlPersonSignatureType = '';
            //*--------ntf_extprint_form variables

            //--------ntf_purchase_responsible_responsible_org variables
            $responsibleOrgRegNum = '';
            $responsibleOrgConsRegistryNum = '';
            $responsibleOrgFullName = '';
            $responsibleOrgShortName = '';
            $responsibleOrgPostAddress = '';
            $responsibleOrgFactAddress = '';
            $responsibleOrgInn = '';
            $responsibleOrgKpp = '';
            //*--------ntf_purchase_responsible_responsible_org variables

            //--------ntf_purchase_responsible_responsible_info variables
            $responsibleInfoOrgPostAddress = '';
            $responsibleInfoOrgFactAddress = '';
            $contactPersonLastName = '';
            $contactPersonFirstName = '';
            $contactPersonMiddleName = '';
            $responsibleInfoContactEMail = '';
            $responsibleInfoContactPhone = '';
            $responsibleInfoContactFax = '';
            $responsibleInfoAddInfo = '';
            //*--------ntf_purchase_responsible_responsible_info variables

            //--------ntf_purchase_responsible_last_specialized_org variables
            $specializedOrgRegNum = '';
            $specializedOrgConsRegistryNum = '';
            $specializedOrgFullName = '';
            $specializedOrgShortName = '';
            $specializedOrgPostAddress = '';
            $specializedOrgFactAddress = '';
            $specializedOrgInn = '';
            $specializedOrgKpp = '';
            //*--------ntf_purchase_responsible_last_specialized_org variables

            //--------ntf_purchase_responsible_specialized_org variables
            $lastSpecializedOrgRegNum = '';
            $lastSpecializedOrgConsRegistryNum = '';
            $lastSpecializedOrgFullName = '';
            $lastSpecializedOrgShortName = '';
            $lastSpecializedOrgPostAddress = '';
            $lastSpecializedOrgFactAddress = '';
            $lastSpecializedOrgInn = '';
            $lastSpecializedOrgKpp = '';
            //*--------ntf_purchase_responsible_specialized_org variables

            //--------ntf_placing_way variables
            $placingWayCode = '';
            $placingWayName = '';
            //*--------ntf_placing_way variables

            //--------ntf_procedure_info_collecting variables
            $procedureInfoCollectingStartDate = '';
            $procedureInfoCollectingPlace = '';
            $procedureInfoCollectingOrder = '';
            $procedureInfoCollectingEndDate = '';
            $procedureInfoCollectingForm = '';
            //*--------ntf_procedure_info_collecting variables

            //--------ntf_procedure_info_opening variables
            $procedureInfoOpeningDate = '';
            $procedureInfoOpeningPlace = '';
            $procedureInfoOpeningAddInfo = '';
            //*--------ntf_procedure_info_opening variables

            //--------ntf_procedure_info_contracting variables
            $procedureInfoContractingContractingTerm = '';
            $procedureInfoContractingEvadeConditions = '';
            //*--------ntf_procedure_info_contracting variables

            //--------ntf_lot variables
            $lotMaxPrice = '';
            $lotMaxPriceInfo = '';
            $lotPriceFormula = '';
            $lotStandardContractNumber = '';
            $lotCurrencyCode = '';
            $lotCurrencyName = '';
            $lotFinanceSource = '';
            $lotInterbudgetaryTransfer = '';
            $lotQuantityUndefined = '';
            $lotPurchaseObjectsTotalSum = '';
            $lotDrugPurchaseObjectsInfoTotal = '';
            $lotRestrictInfo = '';
            $lotAddInfo = '';
            $lotNoPublicDiscussion = '';
            $lotMustPublicDiscussion = '';
            //*--------ntf_lot variables

            //--------ntf_public_discussion variables
            $publicDiscussionNumber = '';
            $publicDiscussionOrganizationCh5St15 = '';
            $publicDiscussionHref = '';
            $publicDiscussionPlace = '';
            $publicDiscussion2017ProtocolDate = '';
            $publicDiscussion2017ProtocolPublishDate = '';
            $publicDiscussion2017PublicDiscussionPhase2Num = '';
            $publicDiscussion2017HrefPhase2 = '';
            //*--------ntf_public_discussion variables

            //-----------ntf_main
            if (isset($xml->fcsNotificationZK->id)) {
                $ntfId = (string)$xml->fcsNotificationZK->id;
            }
            if (isset($xml->fcsNotificationZK->externalId)) {
                $externalId = (string)$xml->fcsNotificationZK->externalId;
            }
            if (isset($xml->fcsNotificationZK->purchaseNumber)) {
                $purchaseNumber = (string)$xml->fcsNotificationZK->purchaseNumber;
            }
            if (isset($xml->fcsNotificationZK->directDate)) {
                $directDate = (string)$xml->fcsNotificationZK->directDate;
            }
            if (isset($xml->fcsNotificationZK->docPublishDate)) {
                $docPublishDate = (string)$xml->fcsNotificationZK->docPublishDate;
            }
            if (isset($xml->fcsNotificationZK->docNumber)) {
                $docNumber = (string)$xml->fcsNotificationZK->docNumber;
            }
            if (isset($xml->fcsNotificationZK->href)) {
                $href = (string)$xml->fcsNotificationZK->href;
            }
            if (isset($xml->fcsNotificationZK->purchaseObjectInfo)) {
                $purchaseObjectInfo = (string)$xml->fcsNotificationZK->purchaseObjectInfo;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleRole)) {
                $purchaseResponsibleResponsibleRole = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleRole;
            }
            if (isset($xml->fcsNotificationZK->externalId)) {
                $externalId = (string)$xml->fcsNotificationZK->externalId;
            }
            if (isset($xml->fcsNotificationZK->okpd2okved2)) {
                $okpd2okved2 = (string)$xml->fcsNotificationZK->okpd2okved2;
            }
            if (isset($xml->fcsNotificationZK->contractServiceInfo)) {
                $contractServiceInfo = (string)$xml->fcsNotificationZK->contractServiceInfo;
            }

            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->regNum)) {
                $responsibleOrgRegNum = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->regNum;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->consRegistryNum)) {
                $responsibleOrgConsRegistryNum = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->fullName)) {
                $responsibleOrgFullName = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->fullName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->shortName)) {
                $responsibleOrgShortName = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->shortName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->postAddress)) {
                $responsibleOrgPostAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->postAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->factAddress)) {
                $responsibleOrgFactAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->factAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->INN)) {
                $responsibleOrgInn = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->INN;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->KPP)) {
                $responsibleOrgKpp = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleOrg->KPP;
            }

            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->orgPostAddress)) {
                $responsibleInfoOrgPostAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->orgPostAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->orgFactAddress)) {
                $responsibleInfoOrgFactAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->orgFactAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPerson->lastName)) {
                $contactPersonLastName = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPerson->lastName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPerson->firstName)) {
                $contactPersonFirstName = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPerson->firstName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPerson->middleName)) {
                $contactPersonMiddleName = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPerson->middleName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactEMail)) {
                $responsibleInfoContactEMail = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactEMail;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPhone)) {
                $responsibleInfoContactPhone = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactPhone;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactFax)) {
                $responsibleInfoContactFax = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->contactFax;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->addInfo)) {
                $responsibleInfoAddInfo = (string)$xml->fcsNotificationZK->purchaseResponsible->responsibleInfo->addInfo;
            }

            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->regNum)) {
                $specializedOrgRegNum = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->regNum;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->consRegistryNum)) {
                $specializedOrgConsRegistryNum = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->fullName)) {
                $specializedOrgFullName = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->fullName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->shortName)) {
                $specializedOrgShortName = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->shortName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->postAddress)) {
                $specializedOrgPostAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->postAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->factAddress)) {
                $specializedOrgFactAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->factAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->INN)) {
                $specializedOrgInn = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->INN;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->specializedOrg->KPP)) {
                $specializedOrgKpp = (string)$xml->fcsNotificationZK->purchaseResponsible->specializedOrg->KPP;
            }

            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->regNum)) {
                $lastSpecializedOrgRegNum = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->regNum;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->consRegistryNum)) {
                $lastSpecializedOrgConsRegistryNum = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->fullName)) {
                $lastSpecializedOrgFullName = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->fullName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->shortName)) {
                $lastSpecializedOrgShortName = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->shortName;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->postAddress)) {
                $lastSpecializedOrgPostAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->postAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->factAddress)) {
                $lastSpecializedOrgFactAddress = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->factAddress;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->INN)) {
                $lastSpecializedOrgInn = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->INN;
            }
            if (isset($xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->KPP)) {
                $lastSpecializedOrgKpp = (string)$xml->fcsNotificationZK->purchaseResponsible->lastSpecializedOrg->KPP;
            }
            //-----------ntf_placing_way
            if (isset($xml->fcsNotificationZK->placingWay->code)) {
                $placingWayCode = (string)$xml->fcsNotificationZK->placingWay->code;
            }
            if (isset($xml->fcsNotificationZK->placingWay->name)) {
                $placingWayName = (string)$xml->fcsNotificationZK->placingWay->name;
            }
            //*-----------ntf_placing_way

            //-----------ntf_procedure_info_collecting

            if (isset($xml->fcsNotificationZK->procedureInfo->collecting->startDate)) {
                $procedureInfoCollectingStartDate = (string)$xml->fcsNotificationZK->procedureInfo->collecting->startDate;
            }
            if (isset($xml->fcsNotificationZK->procedureInfo->collecting->place)) {
                $procedureInfoCollectingPlace = (string)$xml->fcsNotificationZK->procedureInfo->collecting->place;
            }
            if (isset($xml->fcsNotificationZK->procedureInfo->collecting->order)) {
                $procedureInfoCollectingOrder = (string)$xml->fcsNotificationZK->procedureInfo->collecting->order;
            }
            if (isset($xml->fcsNotificationZK->procedureInfo->collecting->endDate)) {
                $procedureInfoCollectingEndDate = (string)$xml->fcsNotificationZK->procedureInfo->collecting->endDate;
            }
            if (isset($xml->fcsNotificationZK->procedureInfo->collecting->form)) {
                $procedureInfoCollectingForm = (string)$xml->fcsNotificationZK->procedureInfo->collecting->form;
            }
            //*-----------ntf_procedure_info_collecting

            //-----------ntf_procedure_info_opening
            if (isset($xml->fcsNotificationZK->procedureInfo->opening->date)) {
                $procedureInfoOpeningDate = (string)$xml->fcsNotificationZK->procedureInfo->opening->date;
            }
            if (isset($xml->fcsNotificationZK->procedureInfo->opening->place)) {
                $procedureInfoOpeningPlace = (string)$xml->fcsNotificationZK->procedureInfo->opening->place;
            }
            if (isset($xml->fcsNotificationZK->procedureInfo->opening->addInfo)) {
                $procedureInfoOpeningAddInfo = (string)$xml->fcsNotificationZK->procedureInfo->opening->addInfo;
            }
            //*-----------ntf_procedure_info_opening

            //-----------ntf_procedure_info_contracting
            if (isset($xml->fcsNotificationZK->procedureInfo->contracting->contractingTerm)) {
                $procedureInfoContractingContractingTerm = (string)$xml->fcsNotificationZK->procedureInfo->contracting->contractingTerm;
            }
            if (isset($xml->fcsNotificationZK->procedureInfo->contracting->evadeConditions)) {
                $procedureInfoContractingEvadeConditions = (string)$xml->fcsNotificationZK->procedureInfo->contracting->evadeConditions;
            }
            //*-----------ntf_procedure_info_contracting

            //-----------ntf_public_discussion
            if (isset($xml->fcsNotificationZK->publicDiscussion->number)) {
                $publicDiscussionNumber = (string)$xml->fcsNotificationZK->publicDiscussion->number;
            }
            if (isset($xml->fcsNotificationZK->publicDiscussion->organizationCh5St15)) {
                $publicDiscussionOrganizationCh5St15 = (string)$xml->fcsNotificationZK->publicDiscussion->organizationCh5St15;
            }
            if (isset($xml->fcsNotificationZK->publicDiscussion->href)) {
                $publicDiscussionHref = (string)$xml->fcsNotificationZK->publicDiscussion->href;
            }
            if (isset($xml->fcsNotificationZK->publicDiscussion->place)) {
                $publicDiscussionPlace = (string)$xml->fcsNotificationZK->publicDiscussion->place;
            }
            if (isset($xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolDate)) {
                $publicDiscussion2017ProtocolDate = (string)$xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolDate;
            }
            if (isset($xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolPublishDate)) {
                $publicDiscussion2017ProtocolPublishDate = (string)$xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolPublishDate;
            }
            if (isset($xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->publicDiscussionPhase2Num)) {
                $publicDiscussion2017PublicDiscussionPhase2Num = (string)$xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->publicDiscussionPhase2Num;
            }
            if (isset($xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->hrefPhase2)) {
                $publicDiscussion2017HrefPhase2 = (string)$xml->fcsNotificationZK->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->hrefPhase2;
            }
            //*-----------ntf_public_discussion

            //*-----------ntf_main
            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO ntf_main (
                          ntf_id,
                          type,
                          external_id,
                          purchase_number,
                          direct_date,
                          doc_publish_date,
                          doc_number,
                          href,
                          purchase_object_info,
                          purchase_responsible_responsible_role,
                          okpd2okved2,
                          contract_service_info,
                          filename,
                          created_at
                      ) 
                      VALUES(
                          :ntfId,
                          :ntfType,
                          :externalId,
                          :purchaseNumber,
                          :directDate,
                          :docPublishDate,
                          :docNumber,
                          :href,
                          :purchaseObjectInfo,
                          :purchaseResponsibleResponsibleRole,
                          :okpd2okved2,
                          :contractServiceInfo,
                          :filename,
                          :createdAt
                      )
                      ON CONFLICT (ntf_id) DO NOTHING 
                      RETURNING id";
            $returningState = Yii::$app->db_zakupki->createCommand($sql)
                ->bindValues([
                    ':ntfId' => $ntfId,
                    ':ntfType' => $ntfType,
                    ':externalId' => $externalId,
                    ':purchaseNumber' => $purchaseNumber,
                    ':directDate' => $directDate,
                    ':docPublishDate' => $docPublishDate,
                    ':docNumber' => $docNumber,
                    ':href' => $href,
                    ':purchaseObjectInfo' => $purchaseObjectInfo,
                    ':purchaseResponsibleResponsibleRole' => $purchaseResponsibleResponsibleRole,
                    ':okpd2okved2' => $okpd2okved2,
                    ':contractServiceInfo' => $contractServiceInfo,
                    ':filename' => $filename,
                    ':createdAt' => date("Y-m-d H:i:s"),
                ])
                ->execute();

            $ntfMainId = Yii::$app->db_zakupki->getLastInsertID('ntf_main_id_seq');
            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
                //var_dump($returningState);
                //var_dump($ntfMainId);
            } else {
                echo "Record with ntf_id = " . $ntfId . " exist." . "\n";
                return;
            }
            //*-----------Вставка данных в ntf_main

            //-----------ntf_print_form
            if (isset($xml->fcsNotificationZK->printForm->url)) {
                $printFormUrl = (string)$xml->fcsNotificationZK->printForm->url;
            }
            if (isset($xml->fcsNotificationZK->printForm->signature)) {
                $printFormSignature = (string)$xml->fcsNotificationZK->printForm->signature;
            }
            //*-----------ntf_print_form
            //-----------Вставка данных в ntf_print_form
            if (
                $ntfMainId !== '' ||
                $printFormUrl !== ''
            ) {
                $sql = "
                  INSERT INTO ntf_print_form (
                    ntf_main_id,
                    url
                  ) 
                  VALUES(
                    :ntfMainId,
                    :printFormUrl
                )";
                Yii::$app->db_zakupki->createCommand($sql)
                    ->bindValues([
                        ':ntfMainId' => $ntfMainId,
                        ':printFormUrl' => $printFormUrl,
                    ])
                    ->execute();
            }
            //*-----------Вставка данных в ntf_print_form

            //-----------ntf_lot
            if (isset($xml->fcsNotificationZK->lot->maxPrice)) {
                $lotMaxPrice = (string)$xml->fcsNotificationZK->lot->maxPrice;
            }
            if (isset($xml->fcsNotificationZK->lot->lotMaxPriceInfo)) {
                $lotMaxPriceInfo = (string)$xml->fcsNotificationZK->lot->lotMaxPriceInfo;
            }
            if (isset($xml->fcsNotificationZK->lot->priceFormula)) {
                $lotPriceFormula = (string)$xml->fcsNotificationZK->lot->priceFormula;
            }
            if (isset($xml->fcsNotificationZK->lot->maxPriceInfo)) {
                $lotMaxPriceInfo = (string)$xml->fcsNotificationZK->lot->maxPriceInfo;
            }
            if (isset($xml->fcsNotificationZK->lot->standardContractNumber)) {
                $lotStandardContractNumber = (string)$xml->fcsNotificationZK->lot->standardContractNumber;
            }
            if (isset($xml->fcsNotificationZK->lot->currency->code)) {
                $lotCurrencyCode = (string)$xml->fcsNotificationZK->lot->currency->code;
            }
            if (isset($xml->fcsNotificationZK->lot->currency->name)) {
                $lotCurrencyName = (string)$xml->fcsNotificationZK->lot->currency->name;
            }
            if (isset($xml->fcsNotificationZK->lot->financeSource)) {
                $lotFinanceSource = (string)$xml->fcsNotificationZK->lot->financeSource;
            }
            if (isset($xml->fcsNotificationZK->lot->interbudgetaryTransfer)) {
                $lotInterbudgetaryTransfer = (string)$xml->fcsNotificationZK->lot->interbudgetaryTransfer;
            }
            if (isset($xml->fcsNotificationZK->lot->quantityUndefined)) {
                $lotQuantityUndefined = (string)$xml->fcsNotificationZK->lot->quantityUndefined;
            }
            if (isset($xml->fcsNotificationZK->lot->purchaseObjects->totalSum)) {
                $lotPurchaseObjectsTotalSum = (string)$xml->fcsNotificationZK->lot->purchaseObjects->totalSum;
            }
            if (isset($xml->fcsNotificationZK->lot->drugPurchaseObjectsInfo->total)) {
                $lotDrugPurchaseObjectsInfoTotal = (string)$xml->fcsNotificationZK->lot->drugPurchaseObjectsInfo->total;
            }
            if (isset($xml->fcsNotificationZK->lot->restrictInfo)) {
                $lotRestrictInfo = (string)$xml->fcsNotificationZK->lot->restrictInfo;
            }
            if (isset($xml->fcsNotificationZK->lot->addInfo)) {
                $lotAddInfo = (string)$xml->fcsNotificationZK->lot->addInfo;
            }
            if (isset($xml->fcsNotificationZK->lot->noPublicDiscussion)) {
                $lotNoPublicDiscussion = (string)$xml->fcsNotificationZK->lot->noPublicDiscussion;
            }
            if (isset($xml->fcsNotificationZK->lot->mustPublicDiscussion)) {
                $lotMustPublicDiscussion = (string)$xml->fcsNotificationZK->lot->mustPublicDiscussion;
            }
            //*-----------ntf_lot

            //-----------Вставка данных в ntf_lot
            if (
                $lotMaxPrice !== '' ||
                $lotMaxPriceInfo !== '' ||
                $lotPriceFormula !== '' ||
                $lotStandardContractNumber !== '' ||
                $lotCurrencyCode !== '' ||
                $lotCurrencyName !== '' ||
                $lotFinanceSource !== '' ||
                $lotInterbudgetaryTransfer !== '' ||
                $lotQuantityUndefined !== '' ||
                $lotPurchaseObjectsTotalSum !== '' ||
                $lotDrugPurchaseObjectsInfoTotal !== '' ||
                $lotRestrictInfo !== '' ||
                $lotAddInfo !== '' ||
                $lotNoPublicDiscussion !== '' ||
                $lotMustPublicDiscussion !== ''
            ) {
                $sql = "
                  INSERT INTO ntf_lot (
                    ntf_main_id,
                    max_price,
                    max_price_info,
                    price_formula,
                    standard_contract_number,
                    currency_code,
                    currency_name,
                    finance_source,
                    interbudgetary_transfer,
                    quantity_undefined,
                    purchase_objects_total_sum,
                    drug_purchase_objects_info_total,
                    restrict_info,
                    add_info,
                    no_public_discussion,
                    must_public_discussion
                  ) 
                  VALUES(
                    :ntfMainId,
                    :lotMaxPrice,
                    :lotMaxPriceInfo,
                    :lotPriceFormula,
                    :lotStandardContractNumber,
                    :lotCurrencyCode,
                    :lotCurrencyName,
                    :lotFinanceSource,
                    :lotInterbudgetaryTransfer,
                    :lotQuantityUndefined,
                    :lotPurchaseObjectsTotalSum,
                    :lotDrugPurchaseObjectsInfoTotal,
                    :lotRestrictInfo,
                    :lotAddInfo,
                    :lotNoPublicDiscussion,
                    :lotMustPublicDiscussion
                )";
                Yii::$app->db_zakupki->createCommand($sql)
                    ->bindValues([
                        ':ntfMainId' => $ntfMainId,
                        ':lotMaxPrice' => $lotMaxPrice,
                        ':lotMaxPriceInfo' => $lotMaxPriceInfo,
                        ':lotPriceFormula' => $lotPriceFormula,
                        ':lotStandardContractNumber' => $lotStandardContractNumber,
                        ':lotCurrencyCode' => $lotCurrencyCode,
                        ':lotCurrencyName' => $lotCurrencyName,
                        ':lotFinanceSource' => $lotFinanceSource,
                        ':lotInterbudgetaryTransfer' => $lotInterbudgetaryTransfer,
                        ':lotQuantityUndefined' => $lotQuantityUndefined,
                        ':lotPurchaseObjectsTotalSum' => $lotPurchaseObjectsTotalSum,
                        ':lotDrugPurchaseObjectsInfoTotal' => $lotDrugPurchaseObjectsInfoTotal,
                        ':lotRestrictInfo' => $lotRestrictInfo,
                        ':lotAddInfo' => $lotAddInfo,
                        ':lotNoPublicDiscussion' => $lotNoPublicDiscussion,
                        ':lotMustPublicDiscussion' => $lotMustPublicDiscussion,
                    ])
                    ->execute();
            }
            $ntfLotId = Yii::$app->db_zakupki->getLastInsertID('ntf_lot_id_seq');
            //*-----------Вставка данных в ntf_lot

            //-----------ntf_customer_requirement
            if (isset($xml->fcsNotificationZK->lot->customerRequirements)) {
                foreach ($xml->fcsNotificationZK->lot->customerRequirements as $customerRequirement) {
                    $customerRegNum = '';
                    $customerConsRegistryNum = '';
                    $customerFullName = '';
                    $maxPrice = '';
                    $deliveryPlace = '';
                    $deliveryTerm = '';
                    $addInfo = '';
                    $purchaseCode = '';
                    $nonbudgetFinancingTotalSum = '';
                    $budgetFinancingsTotalSum = '';
                    $BONumber = '';
                    $BODate = '';
                    $inputBOFlag = '';
                    $onesideRejection = '';
                    $contractGuaranteeAmount = '';
                    $contractGuaranteePart = '';
                    $contractGuaranteeProcedureInfo = '';
                    $contractGuaranteeSettlementAccount = '';
                    $contractGuaranteePersonalAccount = '';
                    $contractGuaranteeBik = '';
                    $tenderPlanInfoPlanNumber = '';
                    $tenderPlanInfoPositionNumber = '';
                    $tenderPlanInfoPurchase83st544 = '';
                    $tenderPlanInfoPlan2017Number = '';
                    $tenderPlanInfoPosition2017Number = '';
                    $tenderPlanInfoPosition2017ExtNumber = '';

                    if (isset($customerRequirement->customerRequirement->customer->regNum)) {
                        $customerRegNum = (string)$customerRequirement->customerRequirement->customer->regNum;
                    }
                    if (isset($customerRequirement->customerRequirement->customer->consRegistryNum)) {
                        $customerConsRegistryNum = (string)$customerRequirement->customerRequirement->customer->consRegistryNum;
                    }
                    if (isset($customerRequirement->customerRequirement->customer->fullName)) {
                        $customerFullName = (string)$customerRequirement->customerRequirement->customer->fullName;
                    }
                    if (isset($customerRequirement->customerRequirement->maxPrice)) {
                        $maxPrice = (string)$customerRequirement->customerRequirement->maxPrice;
                    }
                    if (isset($customerRequirement->customerRequirement->deliveryPlace)) {
                        $deliveryPlace = (string)$customerRequirement->customerRequirement->deliveryPlace;
                    }
                    if (isset($customerRequirement->customerRequirement->deliveryTerm)) {
                        $deliveryTerm = (string)$customerRequirement->customerRequirement->deliveryTerm;
                    }
                    if (isset($customerRequirement->customerRequirement->addInfo)) {
                        $addInfo = (string)$customerRequirement->customerRequirement->addInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->purchaseCode)) {
                        $purchaseCode = (string)$customerRequirement->customerRequirement->purchaseCode;
                    }
                    if (isset($customerRequirement->customerRequirement->nonbudgetFinancing->totalSum)) {
                        $nonbudgetFinancingTotalSum = (string)$customerRequirement->customerRequirement->nonbudgetFinancing->totalSum;
                    }
                    if (isset($customerRequirement->customerRequirement->budgetFinancings->totalSum)) {
                        $budgetFinancingsTotalSum = (string)$customerRequirement->customerRequirement->budgetFinancings->totalSum;
                    }
                    if (isset($customerRequirement->customerRequirement->onesideRejection)) {
                        $onesideRejection = (string)$customerRequirement->customerRequirement->onesideRejection;
                    }

                    if (isset($customerRequirement->customerRequirement->contractGuarantee->amount)) {
                        $contractGuaranteeAmount = (string)$customerRequirement->customerRequirement->contractGuarantee->amount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->part)) {
                        $contractGuaranteePart = (string)$customerRequirement->customerRequirement->contractGuarantee->part;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->procedureInfo)) {
                        $contractGuaranteeProcedureInfo = (string)$customerRequirement->customerRequirement->contractGuarantee->procedureInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->settlementAccount)) {
                        $contractGuaranteeSettlementAccount = (string)$customerRequirement->customerRequirement->contractGuarantee->settlementAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->personalAccount)) {
                        $contractGuaranteePersonalAccount = (string)$customerRequirement->customerRequirement->contractGuarantee->personalAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->bik)) {
                        $contractGuaranteeBik = (string)$customerRequirement->customerRequirement->contractGuarantee->bik;
                    }

                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->planNumber)) {
                        $tenderPlanInfoPlanNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->planNumber;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->positionNumber)) {
                        $tenderPlanInfoPositionNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->positionNumber;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->purchase83st544)) {
                        $tenderPlanInfoPurchase83st544 = (string)$customerRequirement->customerRequirement->tenderPlanInfo->purchase83st544;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->plan2017Number)) {
                        $tenderPlanInfoPlan2017Number = (string)$customerRequirement->customerRequirement->tenderPlanInfo->plan2017Number;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->position2017Number)) {
                        $tenderPlanInfoPosition2017Number = (string)$customerRequirement->customerRequirement->tenderPlanInfo->position2017Number;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->position2017ExtNumber)) {
                        $tenderPlanInfoPosition2017ExtNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->position2017ExtNumber;
                    }

                    if (isset($customerRequirement->customerRequirement->BOInfo->BONumber)) {
                        $BONumber = (string)$customerRequirement->customerRequirement->BOInfo->BONumber;
                    }
                    if (isset($customerRequirement->customerRequirement->BOInfo->BODate)) {
                        $BODate = (string)$customerRequirement->customerRequirement->BOInfo->BODate;
                    }
                    if (isset($customerRequirement->customerRequirement->BOInfo->inputBOFlag)) {
                        $inputBOFlag = (string)$customerRequirement->customerRequirement->BOInfo->inputBOFlag;
                    }

                    //TODO: можно переделать в batchinsert всего массива данныъ, из foreach
                    //-----------Вставка данных в ntf_customer_requirement
                    if (
                        $customerRegNum !== '' ||
                        $customerConsRegistryNum !== '' ||
                        $customerFullName !== '' ||
                        $maxPrice !== '' ||
                        $deliveryPlace !== '' ||
                        $deliveryTerm !== '' ||
                        $addInfo !== '' ||
                        $purchaseCode !== '' ||
                        $nonbudgetFinancingTotalSum !== '' ||
                        $budgetFinancingsTotalSum !== '' ||
                        $BONumber !== '' ||
                        $BODate !== '' ||
                        $inputBOFlag !== '' ||
                        $onesideRejection !== '' ||
                        $contractGuaranteeAmount !== '' ||
                        $contractGuaranteePart !== '' ||
                        $contractGuaranteeProcedureInfo !== '' ||
                        $contractGuaranteeSettlementAccount !== '' ||
                        $contractGuaranteePersonalAccount !== '' ||
                        $contractGuaranteeBik !== '' ||
                        $tenderPlanInfoPlanNumber !== '' ||
                        $tenderPlanInfoPositionNumber !== '' ||
                        $tenderPlanInfoPurchase83st544 !== '' ||
                        $tenderPlanInfoPlan2017Number !== '' ||
                        $tenderPlanInfoPosition2017Number !== '' ||
                        $tenderPlanInfoPosition2017ExtNumber !== ''
                    ) {
                        $sql = "
                          INSERT INTO ntf_customer_requirement (
                            ntf_lot_id,
                            customer_reg_num,
                            customer_cons_registry_num,
                            customer_full_name,
                            max_price,
                            delivery_place,
                            delivery_term,
                            oneside_rejection,
                            contract_guarantee_amount,
                            contract_guarantee_part,
                            contract_guarantee_procedure_info,
                            contract_guarantee_settlement_account,
                            contract_guarantee_personal_account,
                            contract_guarantee_bik,
                            add_info,
                            purchase_code,
                            tender_plan_info_plan_number,
                            tender_plan_info_position_number,
                            tender_plan_info_purchase83st544,
                            tender_plan_info_plan2017_number,
                            tender_plan_info_position2017_number,
                            tender_plan_info_position2017_ext_number,
                            nonbudget_financings_total_sum,
                            budget_financings_total_sum,
                            bo_info_bo_number,
                            bo_info_bo_date,
                            bo_info_input_bo_flag
                          ) 
                          VALUES(
                            :ntfLotId,
                            :customerRegNum,
                            :customerConsRegistryNum,
                            :customerFullName,
                            :maxPrice,
                            :deliveryPlace,
                            :deliveryTerm,
                            :onesideRejection,
                            :contractGuaranteeAmount,
                            :contractGuaranteePart,
                            :contractGuaranteeProcedureInfo,
                            :contractGuaranteeSettlementAccount,
                            :contractGuaranteePersonalAccount,
                            :contractGuaranteeBik,
                            :addInfo,
                            :purchaseCode,
                            :tenderPlanInfoPlanNumber,
                            :tenderPlanInfoPositionNumber,
                            :tenderPlanInfoPurchase83st544,
                            :tenderPlanInfoPlan2017Number,
                            :tenderPlanInfoPosition2017Number,
                            :tenderPlanInfoPosition2017ExtNumber,
                            :nonbudgetFinancingTotalSum,
                            :budgetFinancingsTotalSum,
                            :BONumber,
                            :BODate,
                            :inputBOFlag
                        )";
                        Yii::$app->db_zakupki->createCommand($sql)
                            ->bindValues([
                                ':ntfLotId' => $ntfLotId,
                                ':customerRegNum' => $customerRegNum,
                                ':customerConsRegistryNum' => $customerConsRegistryNum,
                                ':customerFullName' => $customerFullName,
                                ':maxPrice' => $maxPrice,
                                ':deliveryPlace' => $deliveryPlace,
                                ':deliveryTerm' => $deliveryTerm,
                                ':onesideRejection' => $onesideRejection,
                                ':contractGuaranteeAmount' => $contractGuaranteeAmount,
                                ':contractGuaranteePart' => $contractGuaranteePart,
                                ':contractGuaranteeProcedureInfo' => $contractGuaranteeProcedureInfo,
                                ':contractGuaranteeSettlementAccount' => $contractGuaranteeSettlementAccount,
                                ':contractGuaranteePersonalAccount' => $contractGuaranteePersonalAccount,
                                ':contractGuaranteeBik' => $contractGuaranteeBik,
                                ':addInfo' => $addInfo,
                                ':purchaseCode' => $purchaseCode,
                                ':tenderPlanInfoPlanNumber' => $tenderPlanInfoPlanNumber,
                                ':tenderPlanInfoPositionNumber' => $tenderPlanInfoPositionNumber,
                                ':tenderPlanInfoPurchase83st544' => $tenderPlanInfoPurchase83st544,
                                ':tenderPlanInfoPlan2017Number' => $tenderPlanInfoPlan2017Number,
                                ':tenderPlanInfoPosition2017Number' => $tenderPlanInfoPosition2017Number,
                                ':tenderPlanInfoPosition2017ExtNumber' => $tenderPlanInfoPosition2017ExtNumber,
                                ':nonbudgetFinancingTotalSum' => $nonbudgetFinancingTotalSum,
                                ':budgetFinancingsTotalSum' => $budgetFinancingsTotalSum,
                                ':BONumber' => $BONumber,
                                ':BODate' => $BODate,
                                ':inputBOFlag' => $inputBOFlag,
                            ])
                            ->execute();
                    }
                    $ntfCustomerRequirementId = Yii::$app->db_zakupki->getLastInsertID('ntf_customer_requirement_id_seq');
                    //*-----------Вставка данных в ntf_customer_requirement

                    //-----------ntf_kladr_place
                    if (isset($customerRequirement->customerRequirement->kladrPlaces->kladrPlace)) {
                        foreach ($customerRequirement->customerRequirement->kladrPlaces->kladrPlace as $kladrPlace) {
                            $kladrPlaceKladrKladrType = '';
                            $kladrPlaceKladrKladrCode = '';
                            $kladrPlaceKladrFullName = '';
                            $kladrPlaceCountryCountryCode = '';
                            $kladrPlaceCountryCountryFullName = '';
                            $kladrPlaceNoKladrForRegionSettlement = '';
                            $kladrPlaceDeliveryPlace = '';
                            $kladrPlaceNoKladrForRegionSettlementRegion = '';
                            $kladrPlaceNoKladrForRegionSettlementSettlement = '';

                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceKladrKladrType = (string)$kladrPlace->kladr->kladrType;
                            }
                            if (isset($kladrPlace->kladr->kladrCode)) {
                                $kladrPlaceKladrKladrCode = (string)$kladrPlace->kladr->kladrCode;
                            }
                            if (isset($kladrPlace->kladr->fullName)) {
                                $kladrPlaceKladrFullName = (string)$kladrPlace->kladr->fullName;
                            }
                            if (isset($kladrPlace->country->countryCode)) {
                                $kladrPlaceCountryCountryCode = (string)$kladrPlace->country->countryCode;
                            }
                            if (isset($kladrPlace->country->countryFullName)) {
                                $kladrPlaceCountryCountryFullName = (string)$kladrPlace->country->countryFullName;
                            }
                            if (isset($kladrPlace->noKladrForRegionSettlement)) {
                                $kladrPlaceNoKladrForRegionSettlement = (string)$kladrPlace->noKladrForRegionSettlement;
                            }
                            if (isset($kladrPlace->deliveryPlace)) {
                                $kladrPlaceDeliveryPlace = (string)$kladrPlace->deliveryPlace;
                            }
                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceNoKladrForRegionSettlementRegion = (string)$kladrPlace->noKladrForRegionSettlement->region;
                            }
                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceNoKladrForRegionSettlementSettlement = (string)$kladrPlace->noKladrForRegionSettlement->settlement;
                            }

                            //-----------Вставка данных в ntf_kladr_place
                            if (
                                $kladrPlaceKladrKladrType !== '' ||
                                $kladrPlaceKladrKladrCode !== '' ||
                                $kladrPlaceKladrFullName !== '' ||
                                $kladrPlaceCountryCountryCode !== '' ||
                                $kladrPlaceCountryCountryFullName !== '' ||
                                $kladrPlaceNoKladrForRegionSettlement !== '' ||
                                $kladrPlaceDeliveryPlace !== '' ||
                                $kladrPlaceNoKladrForRegionSettlementRegion !== '' ||
                                $kladrPlaceNoKladrForRegionSettlementSettlement !== ''
                            ) {
                                //-----------Вставка данных в ntf_kladr_place
                                $sql = "
                                  INSERT INTO ntf_kladr_place (
                                    ntf_customer_requirement_id,
                                    kladr_kladr_type,
                                    kladr_kladr_code,
                                    kladr_full_name,
                                    country_country_code,
                                    country_country_full_name,
                                    delivery_place,
                                    no_kladr_for_region_settlement_region,
                                    no_kladr_for_region_settlement_settlement,
                                    no_kladr_for_region_settlement
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :kladrPlaceKladrKladrType,
                                    :kladrPlaceKladrKladrCode,
                                    :kladrPlaceKladrFullName,
                                    :kladrPlaceCountryCountryCode,
                                    :kladrPlaceCountryCountryFullName,
                                    :kladrPlaceDeliveryPlace,
                                    :kladrPlaceNoKladrForRegionSettlementRegion,
                                    :kladrPlaceNoKladrForRegionSettlementSettlement,
                                    :kladrPlaceNoKladrForRegionSettlement
                                  )";
                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':kladrPlaceKladrKladrType' => $kladrPlaceKladrKladrType,
                                        ':kladrPlaceKladrKladrCode' => $kladrPlaceKladrKladrCode,
                                        ':kladrPlaceKladrFullName' => $kladrPlaceKladrFullName,
                                        ':kladrPlaceCountryCountryCode' => $kladrPlaceCountryCountryCode,
                                        ':kladrPlaceCountryCountryFullName' => $kladrPlaceCountryCountryFullName,
                                        ':kladrPlaceDeliveryPlace' => $kladrPlaceDeliveryPlace,
                                        ':kladrPlaceNoKladrForRegionSettlementRegion' => $kladrPlaceNoKladrForRegionSettlementRegion,
                                        ':kladrPlaceNoKladrForRegionSettlementSettlement' => $kladrPlaceNoKladrForRegionSettlementSettlement,
                                        ':kladrPlaceNoKladrForRegionSettlement' => $kladrPlaceNoKladrForRegionSettlement,
                                    ])
                                    ->execute();
                            }
                            //*-----------Вставка данных в ntf_kladr_place
                        }
                    }
                    //*-----------ntf_kladr_place

                    //-----------ntf_nonbudget_financing
                    if (isset($customerRequirement->customerRequirement->nonbudgetFinancings->nonbudgetFinancing)) {
                        foreach ($customerRequirement->customerRequirement->nonbudgetFinancings->nonbudgetFinancing as $nonbudgetFinancing) {
                            $nonbudgetFinancingKosguCode = '';
                            $nonbudgetFinancingKvrCode = '';
                            $nonbudgetFinancingYear = '';
                            $nonbudgetFinancingSum = '';

                            if (isset($nonbudgetFinancing->kosguCode)) {
                                $nonbudgetFinancingKosguCode = (string)$nonbudgetFinancing->kosguCode;
                            }
                            if (isset($nonbudgetFinancing->kvrCode)) {
                                $nonbudgetFinancingKvrCode = (string)$nonbudgetFinancing->kvrCode;
                            }
                            if (isset($nonbudgetFinancing->year)) {
                                $nonbudgetFinancingYear = (string)$nonbudgetFinancing->year;
                            }
                            if (isset($nonbudgetFinancing->sum)) {
                                $nonbudgetFinancingSum = (string)$nonbudgetFinancing->sum;
                            }
                            //-----------Вставка данных в ntf_nonbudget_financing
                            if (
                                $nonbudgetFinancingKosguCode !== '' ||
                                $nonbudgetFinancingKvrCode !== '' ||
                                $nonbudgetFinancingYear !== '' ||
                                $nonbudgetFinancingSum !== ''
                            ) {
                                $sql = "
                                  INSERT INTO ntf_nonbudget_financing (
                                    ntf_customer_requirement_id,
                                    kosgu_code,
                                    kvr_code,
                                    year,
                                    sum
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :nonbudgetFinancingKosguCode,
                                    :nonbudgetFinancingKvrCode,
                                    :nonbudgetFinancingYear,
                                    :nonbudgetFinancingSum
                                )";
                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':nonbudgetFinancingKosguCode' => $nonbudgetFinancingKosguCode,
                                        ':nonbudgetFinancingKvrCode' => $nonbudgetFinancingKvrCode,
                                        ':nonbudgetFinancingYear' => $nonbudgetFinancingYear,
                                        ':nonbudgetFinancingSum' => $nonbudgetFinancingSum,
                                    ])
                                    ->execute();
                            }
                            //-----------Вставка данных в ntf_nonbudget_financing
                        }
                    }
                    //*-----------ntf_nonbudget_financing

                    //-----------ntf_budget_financing
                    if (isset($customerRequirement->customerRequirement->budgetFinancings->budgetFinancing)) {
                        foreach ($customerRequirement->customerRequirement->budgetFinancings->budgetFinancing as $budgetFinancing) {
                            $budgetFinancingKbkCode = '';
                            $budgetFinancingKbkCode2016 = '';
                            $budgetFinancingYear = '';
                            $budgetFinancingSum = '';

                            if (isset($budgetFinancing->kbkCode)) {
                                $budgetFinancingKbkCode = (string)$budgetFinancing->kbkCode;
                            }
                            if (isset($budgetFinancing->kbkCode2016)) {
                                $budgetFinancingKbkCode2016 = (string)$budgetFinancing->kbkCode2016;
                            }
                            if (isset($budgetFinancing->year)) {
                                $budgetFinancingYear = (string)$budgetFinancing->year;
                            }
                            if (isset($budgetFinancing->sum)) {
                                $budgetFinancingSum = (string)$budgetFinancing->sum;
                            }
                            //-----------Вставка данных в ntf_budget_financing
                            if (
                                $budgetFinancingKbkCode !== '' ||
                                $budgetFinancingKbkCode2016 !== '' ||
                                $budgetFinancingYear !== '' ||
                                $budgetFinancingSum !== ''
                            ) {
                                $sql = "
                                  INSERT INTO ntf_budget_financing (
                                    ntf_customer_requirement_id,
                                    kbk_code,
                                    kbk_code2016,
                                    year,
                                    sum
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :budgetFinancingKbkCode,
                                    :budgetFinancingKbkCode2016,
                                    :budgetFinancingYear,
                                    :budgetFinancingSum
                                )";

                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':budgetFinancingKbkCode' => $budgetFinancingKbkCode,
                                        ':budgetFinancingKbkCode2016' => $budgetFinancingKbkCode2016,
                                        ':budgetFinancingYear' => $budgetFinancingYear,
                                        ':budgetFinancingSum' => $budgetFinancingSum,
                                    ])
                                    ->execute();
                            }
                            //-----------Вставка данных в ntf_budget_financing
                        }
                    }
                    //*-----------ntf_budget_financing
                }
                //-----------ntf_purchase_object
                if (isset($xml->fcsNotificationZK->lot->purchaseObjects->purchaseObject)) {
                    foreach ($xml->fcsNotificationZK->lot->purchaseObjects->purchaseObject as $purchaseObject) {
                        $purchaseObjectOKPDCode = '';
                        $purchaseObjectOKPDName = '';
                        $purchaseObjectOKPD2Code = '';
                        $purchaseObjectOKPD2Name = '';
                        $purchaseObjectName = '';
                        $purchaseObjectOKEICode = '';
                        $purchaseObjectOKEINationalCode = '';
                        $purchaseObjectPrice = '';
                        $purchaseObjectQuantityValue = '';
                        $purchaseObjectQuantityUndefined = '';
                        $purchaseObjectSum = '';

                        if (isset($purchaseObject->OKPD->code)) {
                            $purchaseObjectOKPDCode = (string)$purchaseObject->OKPD->code;
                        }
                        if (isset($purchaseObject->OKPD->name)) {
                            $purchaseObjectOKPDName = (string)$purchaseObject->OKPD->name;
                        }
                        if (isset($purchaseObject->OKPD2->code)) {
                            $purchaseObjectOKPD2Code = (string)$purchaseObject->OKPD2->code;
                        }
                        if (isset($purchaseObject->OKPD2->name)) {
                            $purchaseObjectOKPD2Name = (string)$purchaseObject->OKPD2->name;
                        }
                        if (isset($purchaseObject->name)) {
                            $purchaseObjectName = (string)$purchaseObject->name;
                        }
                        if (isset($purchaseObject->OKEI->code)) {
                            $purchaseObjectOKEICode = (string)$purchaseObject->OKEI->code;
                        }
                        if (isset($purchaseObject->OKEI->nationalCode)) {
                            $purchaseObjectOKEINationalCode = (string)$purchaseObject->OKEI->nationalCode;
                        }
                        if (isset($purchaseObject->price)) {
                            $purchaseObjectPrice = (string)$purchaseObject->price;
                        }
                        if (isset($purchaseObject->quantity->value)) {
                            $purchaseObjectQuantityValue = (string)$purchaseObject->quantity->value;
                        }
                        if (isset($purchaseObject->quantity->undefined)) {
                            $purchaseObjectQuantityUndefined = (string)$purchaseObject->quantity->undefined;
                        }
                        if (isset($purchaseObject->sum)) {
                            $purchaseObjectSum = (string)$purchaseObject->sum;
                        }

                        //-----------Вставка данных в ntf_purchase_object
                        if (
                            $purchaseObjectOKPDCode !== '' ||
                            $purchaseObjectOKPDName !== '' ||
                            $purchaseObjectOKPD2Code !== '' ||
                            $purchaseObjectOKPD2Name !== '' ||
                            $purchaseObjectName !== '' ||
                            $purchaseObjectOKEICode !== '' ||
                            $purchaseObjectOKEINationalCode !== '' ||
                            $purchaseObjectPrice !== '' ||
                            $purchaseObjectQuantityValue !== '' ||
                            $purchaseObjectQuantityUndefined !== '' ||
                            $purchaseObjectSum !== ''
                        ) {
                            $sql = "
                              INSERT INTO ntf_purchase_object (
                                ntf_lot_id,
                                okpd_code,
                                okpd_name,
                                okpd2_code,
                                okpd2_name,
                                name,
                                okei_code,
                                okei_national_code,
                                price,
                                quantity_value,
                                quantity_undefined,
                                sum
                              ) 
                              VALUES(
                                :ntfLotId,
                                :purchaseObjectOKPDCode,
                                :purchaseObjectOKPDName,
                                :purchaseObjectOKPD2Code,
                                :purchaseObjectOKPD2Name,
                                :purchaseObjectName,
                                :purchaseObjectOKEICode,
                                :purchaseObjectOKEINationalCode,
                                :purchaseObjectPrice,
                                :purchaseObjectQuantityValue,
                                :purchaseObjectQuantityUndefined,
                                :purchaseObjectSum
                          )";
                            Yii::$app->db_zakupki->createCommand($sql)
                                ->bindValues([
                                    ':ntfLotId' => $ntfLotId,
                                    ':purchaseObjectOKPDCode' => $purchaseObjectOKPDCode,
                                    ':purchaseObjectOKPDName' => $purchaseObjectOKPDName,
                                    ':purchaseObjectOKPD2Code' => $purchaseObjectOKPD2Code,
                                    ':purchaseObjectOKPD2Name' => $purchaseObjectOKPD2Name,
                                    ':purchaseObjectName' => $purchaseObjectName,
                                    ':purchaseObjectOKEICode' => $purchaseObjectOKEICode,
                                    ':purchaseObjectOKEINationalCode' => $purchaseObjectOKEINationalCode,
                                    ':purchaseObjectPrice' => $purchaseObjectPrice,
                                    ':purchaseObjectQuantityValue' => $purchaseObjectQuantityValue,
                                    ':purchaseObjectQuantityUndefined' => $purchaseObjectQuantityUndefined,
                                    ':purchaseObjectSum' => $purchaseObjectSum,
                                ])
                                ->execute();
                            $ntfPurchaseObjectId = Yii::$app->db_zakupki->getLastInsertID('ntf_purchase_object_id_seq');
                        }
                        //*-----------Вставка данных в ntf_purchase_object

                        //-----------ntf_customer_quantity
                        if (isset($purchaseObject->customerQuantities->customerQuantity)) {
                            foreach ($purchaseObject->customerQuantities->customerQuantity as $customerQuantity) {
                                $customerRegNum = '';
                                $customerConsRegistryNum = '';
                                $customerFullName = '';
                                $quantity = '';

                                if (isset($customerQuantity->customer->regNum)) {
                                    $customerRegNum = (string)$customerQuantity->customer->regNum;
                                }
                                if (isset($customerQuantity->customer->consRegistryNum)) {
                                    $customerConsRegistryNum = (string)$customerQuantity->customer->consRegistryNum;
                                }
                                if (isset($customerQuantity->customer->fullName)) {
                                    $customerFullName = (string)$customerQuantity->customer->fullName;
                                }
                                if (isset($customerQuantity->quantity)) {
                                    $quantity = (string)$customerQuantity->quantity;
                                }

                                //-----------Вставка данных в ntf_customer_quantity
                                if (
                                    $customerRegNum !== '' ||
                                    $customerConsRegistryNum !== '' ||
                                    $customerFullName !== '' ||
                                    $quantity !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_customer_quantity (
                                                  ntf_purchase_object_id,
                                                  customer_reg_num,
                                                  customer_cons_registry_num,
                                                  customer_full_name,
                                                  quantity
                                      ) 
                                      VALUES(
                                        :ntfPurchaseObjectId,
                                        :customerRegNum,
                                        :customerConsRegistryNum,
                                        :customerFullName,
                                        :quantity
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfPurchaseObjectId' => $ntfPurchaseObjectId,
                                            ':customerRegNum' => $customerRegNum,
                                            ':customerConsRegistryNum' => $customerConsRegistryNum,
                                            ':customerFullName' => $customerFullName,
                                            ':quantity' => $quantity,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_customer_quantity
                            }
                        }
                        //-----------ntf_customer_quantity
                    }
                }
                //*-----------ntf_purchase_object

                //-----------ntf_drug_purchase_object_info
                if (isset($xml->fcsNotificationZK->lot->drugPurchaseObjectsInfo->drugPurchaseObjectInfo)) {
                    foreach ($xml->fcsNotificationZK->lot->drugPurchaseObjectsInfo->drugPurchaseObjectInfo as $drugPurchaseObjectInfo) {
                        $isZNVLP = '';
                        $drugQuantityCustomersInfoTotal = '';
                        $pricePerUnit = '';
                        $positionPrice = '';

                        if (isset($drugPurchaseObjectInfo->isZNVLP)) {
                            $isZNVLP = (string)$drugPurchaseObjectInfo->isZNVLP;
                        }
                        if (isset($drugPurchaseObjectInfo->pricePerUnit)) {
                            $pricePerUnit = (string)$drugPurchaseObjectInfo->pricePerUnit;
                        }
                        if (isset($drugPurchaseObjectInfo->positionPrice)) {
                            $positionPrice = (string)$drugPurchaseObjectInfo->positionPrice;
                        }
                        if (isset($drugPurchaseObjectInfo->positionPrice->drugQuantityCustomersInfo->total)) {
                            $drugQuantityCustomersInfoTotal = (string)$drugPurchaseObjectInfo->positionPrice->drugQuantityCustomersInfo->total;
                        }
                        //-----------Вставка данных в ntf_drug_purchase_object_info
                        if (
                            $isZNVLP !== '' ||
                            $drugQuantityCustomersInfoTotal !== '' ||
                            $pricePerUnit !== '' ||
                            $positionPrice !== ''
                        ) {
                            $sql = "
                              INSERT INTO ntf_drug_purchase_object_info (
                                ntf_lot_id,
                                isznvlp,
                                drug_quantity_customers_info_total,
                                price_per_unit,
                                position_price
                              ) 
                              VALUES(
                                :ntfLotId,
                                :isZNVLP,
                                :drugQuantityCustomersInfoTotal,
                                :pricePerUnit,
                                :positionPrice
                            )";
                            Yii::$app->db_zakupki->createCommand($sql)
                                ->bindValues([
                                    ':ntfLotId' => $ntfLotId,
                                    ':isZNVLP' => $isZNVLP,
                                    ':drugQuantityCustomersInfoTotal' => $drugQuantityCustomersInfoTotal,
                                    ':pricePerUnit' => $pricePerUnit,
                                    ':positionPrice' => $positionPrice,
                                ])
                                ->execute();
                        }
                        $ntfDrugPurchaseObjectInfoId = Yii::$app->db_zakupki->getLastInsertID('ntf_drug_purchase_object_info_id_seq');
                        //*-----------Вставка данных в ntf_drug_purchase_object_info

                        //-----------ntf_drug_quantity_customer_info
                        if (isset($drugPurchaseObjectInfo->drugQuantityCustomersInfo->drugQuantityCustomerInfo)) {
                            foreach ($drugPurchaseObjectInfo->drugQuantityCustomersInfo->drugQuantityCustomerInfo as $drugQuantityCustomerInfo) {
                                $drugQuantityCustomerInfoCustomerRegNum = '';
                                $drugQuantityCustomerInfoCustomerConsRegistryNum = '';
                                $drugQuantityCustomerInfoCustomerFullName = '';
                                $drugQuantityCustomerInfoQuantity = '';

                                if (isset($drugQuantityCustomerInfo->customer->regNum)) {
                                    $drugQuantityCustomerInfoCustomerRegNum = (string)$drugQuantityCustomerInfo->customer->regNum;
                                }
                                if (isset($drugQuantityCustomerInfo->customer->consRegistryNum)) {
                                    $drugQuantityCustomerInfoCustomerConsRegistryNum = (string)$drugQuantityCustomerInfo->customer->consRegistryNum;
                                }
                                if (isset($drugQuantityCustomerInfo->customer->fullName)) {
                                    $drugQuantityCustomerInfoCustomerFullName = (string)$drugQuantityCustomerInfo->customer->fullName;
                                }
                                if (isset($drugQuantityCustomerInfo->quantity)) {
                                    $drugQuantityCustomerInfoQuantity = (string)$drugQuantityCustomerInfo->quantity;
                                }
                                //-----------Вставка данных в ntf_drug_quantity_customer_info
                                if (
                                    $drugQuantityCustomerInfoCustomerRegNum !== '' ||
                                    $drugQuantityCustomerInfoCustomerConsRegistryNum !== '' ||
                                    $drugQuantityCustomerInfoCustomerFullName !== '' ||
                                    $drugQuantityCustomerInfoQuantity !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_drug_quantity_customer_info (
                                        ntf_drug_purchase_object_info_id,
                                        customer_reg_num,
                                        customer_cons_registry_num,
                                        customer_full_name,
                                        quantity
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :drugQuantityCustomerInfoCustomerRegNum,
                                        :drugQuantityCustomerInfoCustomerConsRegistryNum,
                                        :drugQuantityCustomerInfoCustomerFullName,
                                        :drugQuantityCustomerInfoQuantity
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':drugQuantityCustomerInfoCustomerRegNum' => $drugQuantityCustomerInfoCustomerRegNum,
                                            ':drugQuantityCustomerInfoCustomerConsRegistryNum' => $drugQuantityCustomerInfoCustomerConsRegistryNum,
                                            ':drugQuantityCustomerInfoCustomerFullName' => $drugQuantityCustomerInfoCustomerFullName,
                                            ':drugQuantityCustomerInfoQuantity' => $drugQuantityCustomerInfoQuantity,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_drug_quantity_customer_info
                            }
                        }
                        //*-----------ntf_drug_quantity_customer_info

                        //-----------ntf_object_info_using_reference_info
                        if (isset($drugPurchaseObjectInfo->objectInfoUsingReferenceInfo->drugsInfo->drugInfo)) {
                            foreach ($drugPurchaseObjectInfo->objectInfoUsingReferenceInfo->drugsInfo->drugInfo as $drugInfo) {
                                $referenceInfoMNNExternalCode = '';
                                $referenceInfoMNNName = '';
                                $referenceInfoPositionTradeNameExternalCode = '';
                                $referenceInfoTradeName = '';
                                $referenceInfoMedicamentalFormName = '';
                                $referenceInfoDosageGRLSValue = '';
                                $referenceInfoDosageUserOKEICode = '';
                                $referenceInfoDosageUserOKEIName = '';
                                $referenceInfoPackaging1Quantity = '';
                                $referenceInfoPackaging2Quantity = '';
                                $referenceInfoSummaryPackagingQuantity = '';
                                $referenceInfoManualUserOKEICode = '';
                                $referenceInfoManualUserOKEIName = '';
                                $referenceInfoBasicUnit = '';
                                $referenceInfoDrugQuantity = '';
                                $referenceInfoSpecifyDrugPackageReason = '';

                                if (isset($drugInfo->MNNInfo->MNNExternalCode)) {
                                    $referenceInfoMNNExternalCode = (string)$drugInfo->MNNInfo->MNNExternalCode;
                                }
                                if (isset($drugInfo->MNNInfo->MNNName)) {
                                    $referenceInfoMNNName = (string)$drugInfo->MNNInfo->MNNName;
                                }
                                if (isset($drugInfo->tradeInfo->positionTradeNameExternalCode)) {
                                    $referenceInfoPositionTradeNameExternalCode = (string)$drugInfo->tradeInfo->positionTradeNameExternalCode;
                                }
                                if (isset($drugInfo->tradeInfo->tradeName)) {
                                    $referenceInfoTradeName = (string)$drugInfo->tradeInfo->tradeName;
                                }
                                if (isset($drugInfo->medicamentalFormInfo->medicamentalFormName)) {
                                    $referenceInfoMedicamentalFormName = (string)$drugInfo->medicamentalFormInfo->medicamentalFormName;
                                }
                                if (isset($drugInfo->dosageInfo->dosageGRLSValue)) {
                                    $referenceInfoDosageGRLSValue = (string)$drugInfo->dosageInfo->dosageGRLSValue;
                                }
                                if (isset($drugInfo->dosageInfo->dosageUserOKEI->code)) {
                                    $referenceInfoDosageUserOKEICode = (string)$drugInfo->dosageInfo->dosageUserOKEI->code;
                                }
                                if (isset($drugInfo->dosageInfo->dosageUserOKEI->name)) {
                                    $referenceInfoDosageUserOKEIName = (string)$drugInfo->dosageInfo->dosageUserOKEI->name;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->packaging1Quantity)) {
                                    $referenceInfoPackaging1Quantity = (string)$drugInfo->dosageInfo->packagingInfo->packaging1Quantity;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->packaging2Quantity)) {
                                    $referenceInfoPackaging2Quantity = (string)$drugInfo->dosageInfo->packagingInfo->packaging2Quantity;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity)) {
                                    $referenceInfoSummaryPackagingQuantity = (string)$drugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity;
                                }
                                if (isset($drugInfo->dosageInfo->manualUserOKEI->code)) {
                                    $referenceInfoManualUserOKEICode = (string)$drugInfo->dosageInfo->manualUserOKEI->code;
                                }
                                if (isset($drugInfo->dosageInfo->manualUserOKEI->name)) {
                                    $referenceInfoManualUserOKEIName = (string)$drugInfo->dosageInfo->manualUserOKEI->name;
                                }
                                if (isset($drugInfo->basicUnit)) {
                                    $referenceInfoBasicUnit = (string)$drugInfo->basicUnit;
                                }
                                if (isset($drugInfo->drugQuantity)) {
                                    $referenceInfoDrugQuantity = (string)$drugInfo->drugQuantity;
                                }
                                if (isset($drugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason)) {
                                    $referenceInfoSpecifyDrugPackageReason = (string)$drugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason;
                                }
                                //-----------Вставка данных в ntf_object_info_using_reference_info
                                if (
                                    $referenceInfoMNNExternalCode !== '' ||
                                    $referenceInfoMNNName !== '' ||
                                    $referenceInfoPositionTradeNameExternalCode !== '' ||
                                    $referenceInfoTradeName !== '' ||
                                    $referenceInfoMedicamentalFormName !== '' ||
                                    $referenceInfoDosageGRLSValue !== '' ||
                                    $referenceInfoDosageUserOKEICode !== '' ||
                                    $referenceInfoDosageUserOKEIName !== '' ||
                                    $referenceInfoPackaging1Quantity !== '' ||
                                    $referenceInfoPackaging2Quantity !== '' ||
                                    $referenceInfoSummaryPackagingQuantity !== '' ||
                                    $referenceInfoManualUserOKEICode !== '' ||
                                    $referenceInfoManualUserOKEIName !== '' ||
                                    $referenceInfoBasicUnit !== '' ||
                                    $referenceInfoDrugQuantity !== '' ||
                                    $referenceInfoSpecifyDrugPackageReason !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_object_info_using_reference_info (
                                        ntf_drug_purchase_object_info_id,
                                        drug_info_mmn_info_mmn_external_code,
                                        drug_info_mmn_info_mmn_name,
                                        drug_info_trade_info_position_trade_name_external_code,
                                        drug_info_trade_info_trade_name,
                                        drug_info_medicamental_form_info_medicamental_form_name,
                                        drug_info_dosage_info_dosage_grls_value,
                                        drug_info_dosage_info_dosage_user_okei_code,
                                        drug_info_dosage_info_dosage_user_okei_name,
                                        drug_info_packaging_info_packaging1_quantity,
                                        drug_info_packaging_info_packaging2_quantity,
                                        drug_info_packaging_info_summary_packaging_quantity,
                                        drug_info_manual_user_okei_code,
                                        drug_info_manual_user_okei_name,
                                        drug_info_basic_unit,
                                        drug_info_drug_quantity,
                                        must_specify_drug_package_specify_drug_package_reason
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :referenceInfoMNNExternalCode,
                                        :referenceInfoMNNName,
                                        :referenceInfoPositionTradeNameExternalCode,
                                        :referenceInfoTradeName,
                                        :referenceInfoMedicamentalFormName,
                                        :referenceInfoDosageGRLSValue,
                                        :referenceInfoDosageUserOKEICode,
                                        :referenceInfoDosageUserOKEIName,
                                        :referenceInfoPackaging1Quantity,
                                        :referenceInfoPackaging2Quantity,
                                        :referenceInfoSummaryPackagingQuantity,
                                        :referenceInfoManualUserOKEICode,
                                        :referenceInfoManualUserOKEIName,
                                        :referenceInfoBasicUnit,
                                        :referenceInfoDrugQuantity,
                                        :referenceInfoSpecifyDrugPackageReason
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':referenceInfoMNNExternalCode' => $referenceInfoMNNExternalCode,
                                            ':referenceInfoMNNName' => $referenceInfoMNNName,
                                            ':referenceInfoPositionTradeNameExternalCode' => $referenceInfoPositionTradeNameExternalCode,
                                            ':referenceInfoTradeName' => $referenceInfoTradeName,
                                            ':referenceInfoMedicamentalFormName' => $referenceInfoMedicamentalFormName,
                                            ':referenceInfoDosageGRLSValue' => $referenceInfoDosageGRLSValue,
                                            ':referenceInfoDosageUserOKEICode' => $referenceInfoDosageUserOKEICode,
                                            ':referenceInfoDosageUserOKEIName' => $referenceInfoDosageUserOKEIName,
                                            ':referenceInfoPackaging1Quantity' => $referenceInfoPackaging1Quantity,
                                            ':referenceInfoPackaging2Quantity' => $referenceInfoPackaging2Quantity,
                                            ':referenceInfoSummaryPackagingQuantity' => $referenceInfoSummaryPackagingQuantity,
                                            ':referenceInfoManualUserOKEICode' => $referenceInfoManualUserOKEICode,
                                            ':referenceInfoManualUserOKEIName' => $referenceInfoManualUserOKEIName,
                                            ':referenceInfoBasicUnit' => $referenceInfoBasicUnit,
                                            ':referenceInfoDrugQuantity' => $referenceInfoDrugQuantity,
                                            ':referenceInfoSpecifyDrugPackageReason' => $referenceInfoSpecifyDrugPackageReason,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_object_info_using_reference_info
                            }
                        }
                        //*-----------ntf_object_info_using_reference_info

                        //-----------ntf_object_info_using_text_form
                        if (isset($drugPurchaseObjectInfo->objectInfoUsingTextForm->drugsInfo->drugInfo)) {
                            foreach ($drugPurchaseObjectInfo->objectInfoUsingTextForm->drugsInfo->drugInfo as $textFormDrugInfo) {
                                $textFormMNNName = '';
                                $textFormTradeName = '';
                                $textFormMedicamentalFormName = '';
                                $textFormDosageGRLSValue = '';
                                $textFormPackaging1Quantity = '';
                                $textFormPackaging2Quantity = '';
                                $textFormSummaryPackagingQuantity = '';
                                $textFormManualUserOKEICode = '';
                                $textFormManualUserOKEIName = '';
                                $textFormBasicUnit = '';
                                $textFormDrugQuantity = '';
                                $textFormSpecifyDrugPackageReason = '';

                                if (isset($textFormDrugInfo->MNNInfo->MNNName)) {
                                    $textFormMNNName = (string)$textFormDrugInfo->MNNInfo->MNNName;
                                }
                                if (isset($textFormDrugInfo->tradeInfo->tradeName)) {
                                    $textFormTradeName = (string)$textFormDrugInfo->tradeInfo->tradeName;
                                }
                                if (isset($textFormDrugInfo->medicamentalFormInfo->medicamentalFormName)) {
                                    $textFormMedicamentalFormName = (string)$textFormDrugInfo->medicamentalFormInfo->medicamentalFormName;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->dosageGRLSValue)) {
                                    $textFormDosageGRLSValue = (string)$textFormDrugInfo->dosageInfo->dosageGRLSValue;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->packaging1Quantity)) {
                                    $textFormPackaging1Quantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->packaging1Quantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->packaging2Quantity)) {
                                    $textFormPackaging2Quantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->packaging2Quantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity)) {
                                    $textFormSummaryPackagingQuantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->summaryPackagingQuantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->manualUserOKEI->code)) {
                                    $textFormManualUserOKEICode = (string)$textFormDrugInfo->dosageInfo->manualUserOKEI->code;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->manualUserOKEI->name)) {
                                    $textFormManualUserOKEIName = (string)$textFormDrugInfo->dosageInfo->manualUserOKEI->name;
                                }
                                if (isset($textFormDrugInfo->basicUnit)) {
                                    $textFormBasicUnit = (string)$textFormDrugInfo->basicUnit;
                                }
                                if (isset($textFormDrugInfo->drugQuantity)) {
                                    $textFormDrugQuantity = (string)$textFormDrugInfo->drugQuantity;
                                }
                                if (isset($textFormDrugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason)) {
                                    $textFormSpecifyDrugPackageReason = (string)$textFormDrugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason;
                                }
                                //-----------Вставка данных в ntf_object_info_using_text_form
                                if (
                                    $textFormMNNName !== '' ||
                                    $textFormTradeName !== '' ||
                                    $textFormMedicamentalFormName !== '' ||
                                    $textFormDosageGRLSValue !== '' ||
                                    $textFormPackaging1Quantity !== '' ||
                                    $textFormPackaging2Quantity !== '' ||
                                    $textFormSummaryPackagingQuantity !== '' ||
                                    $textFormManualUserOKEICode !== '' ||
                                    $textFormManualUserOKEIName !== '' ||
                                    $textFormBasicUnit !== '' ||
                                    $textFormDrugQuantity !== '' ||
                                    $textFormSpecifyDrugPackageReason !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_object_info_using_text_form (
                                        ntf_drug_purchase_object_info_id,
                                        drug_info_mmn_info_mmn_name,
                                        drug_info_trade_info_trade_name,
                                        drug_info_medicamental_form_info_medicamental_form_name,
                                        drug_info_dosage_info_dosage_grls_value,
                                        drug_info_packaging_info_packaging1_quantity,
                                        drug_info_packaging_info_packaging2_quantity,
                                        drug_info_packaging_info_summary_packaging_quantity,
                                        drug_info_manual_user_okei_code,
                                        drug_info_manual_user_okei_name,
                                        drug_info_basic_unit,
                                        drug_info_drug_quantity,
                                        must_specify_drug_package_specify_drug_package_reason
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :textFormMNNName,
                                        :textFormTradeName,
                                        :textFormMedicamentalFormName,
                                        :textFormDosageGRLSValue,
                                        :textFormPackaging1Quantity,
                                        :textFormPackaging2Quantity,
                                        :textFormSummaryPackagingQuantity,
                                        :textFormManualUserOKEICode,
                                        :textFormManualUserOKEIName,
                                        :textFormBasicUnit,
                                        :textFormDrugQuantity,
                                        :textFormSpecifyDrugPackageReason
                                    )";

                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':textFormMNNName' => $textFormMNNName,
                                            ':textFormTradeName' => $textFormTradeName,
                                            ':textFormMedicamentalFormName' => $textFormMedicamentalFormName,
                                            ':textFormDosageGRLSValue' => $textFormDosageGRLSValue,
                                            ':textFormPackaging1Quantity' => $textFormPackaging1Quantity,
                                            ':textFormPackaging2Quantity' => $textFormPackaging2Quantity,
                                            ':textFormSummaryPackagingQuantity' => $textFormSummaryPackagingQuantity,
                                            ':textFormManualUserOKEICode' => $textFormManualUserOKEICode,
                                            ':textFormManualUserOKEIName' => $textFormManualUserOKEIName,
                                            ':textFormBasicUnit' => $textFormBasicUnit,
                                            ':textFormDrugQuantity' => $textFormDrugQuantity,
                                            ':textFormSpecifyDrugPackageReason' => $textFormSpecifyDrugPackageReason,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_object_info_using_text_form
                            }
                        }
                        //*-----------ntf_object_info_using_text_form
                    }
                }
                //*-----------ntf_drug_purchase_object_info
                //-----------Вставка данных в ntf_purchase_responsible_responsible_org
                if (
                    $responsibleOrgRegNum !== '' ||
                    $responsibleOrgConsRegistryNum !== '' ||
                    $responsibleOrgFullName !== '' ||
                    $responsibleOrgShortName !== '' ||
                    $responsibleOrgPostAddress !== '' ||
                    $responsibleOrgFactAddress !== '' ||
                    $responsibleOrgInn !== '' ||
                    $responsibleOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_responsible_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :responsibleOrgRegNum,
                        :responsibleOrgConsRegistryNum,
                        :responsibleOrgFullName,
                        :responsibleOrgShortName,
                        :responsibleOrgPostAddress,
                        :responsibleOrgFactAddress,
                        :responsibleOrgInn,
                        :responsibleOrgKpp
                    )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':responsibleOrgRegNum' => $responsibleOrgRegNum,
                            ':responsibleOrgConsRegistryNum' => $responsibleOrgConsRegistryNum,
                            ':responsibleOrgFullName' => $responsibleOrgFullName,
                            ':responsibleOrgShortName' => $responsibleOrgShortName,
                            ':responsibleOrgPostAddress' => $responsibleOrgPostAddress,
                            ':responsibleOrgFactAddress' => $responsibleOrgFactAddress,
                            ':responsibleOrgInn' => $responsibleOrgInn,
                            ':responsibleOrgKpp' => $responsibleOrgKpp,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_responsible_org

                //-----------Вставка данных в ntf_purchase_responsible_responsible_info
                if (
                    $responsibleInfoOrgPostAddress !== '' ||
                    $responsibleInfoOrgFactAddress !== '' ||
                    $contactPersonLastName !== '' ||
                    $contactPersonFirstName !== '' ||
                    $contactPersonMiddleName !== '' ||
                    $responsibleInfoContactEMail !== '' ||
                    $responsibleInfoContactPhone !== '' ||
                    $responsibleInfoContactFax !== '' ||
                    $responsibleInfoAddInfo !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_responsible_info (
                        ntf_main_id,
                        org_post_address,
                        org_fact_address,
                        contact_person_last_name,
                        contact_person_first_name,
                        contact_person_middle_name,
                        contact_email,
                        contact_phone,
                        contact_fax,
                        add_info
                      ) 
                      VALUES(
                        :ntfMainId,
                        :responsibleInfoOrgPostAddress,
                        :responsibleInfoOrgFactAddress,
                        :contactPersonLastName,
                        :contactPersonFirstName,
                        :contactPersonMiddleName,
                        :responsibleInfoContactEMail,
                        :responsibleInfoContactPhone,
                        :responsibleInfoContactFax,
                        :responsibleInfoAddInfo
                    )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':responsibleInfoOrgPostAddress' => $responsibleInfoOrgPostAddress,
                            ':responsibleInfoOrgFactAddress' => $responsibleInfoOrgFactAddress,
                            ':contactPersonLastName' => $contactPersonLastName,
                            ':contactPersonFirstName' => $contactPersonFirstName,
                            ':contactPersonMiddleName' => $contactPersonMiddleName,
                            ':responsibleInfoContactEMail' => $responsibleInfoContactEMail,
                            ':responsibleInfoContactPhone' => $responsibleInfoContactPhone,
                            ':responsibleInfoContactFax' => $responsibleInfoContactFax,
                            ':responsibleInfoAddInfo' => $responsibleInfoAddInfo,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_responsible_info

                //-----------Вставка данных в ntf_purchase_responsible_specialized_org
                if (
                    $specializedOrgRegNum !== '' ||
                    $specializedOrgConsRegistryNum !== '' ||
                    $specializedOrgFullName !== '' ||
                    $specializedOrgShortName !== '' ||
                    $specializedOrgPostAddress !== '' ||
                    $specializedOrgFactAddress !== '' ||
                    $specializedOrgInn !== '' ||
                    $specializedOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_specialized_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :specializedOrgRegNum,
                        :specializedOrgConsRegistryNum,
                        :specializedOrgFullName,
                        :specializedOrgShortName,
                        :specializedOrgPostAddress,
                        :specializedOrgFactAddress,
                        :specializedOrgInn,
                        :specializedOrgKpp
                        )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':specializedOrgRegNum' => $specializedOrgRegNum,
                            ':specializedOrgConsRegistryNum' => $specializedOrgConsRegistryNum,
                            ':specializedOrgFullName' => $specializedOrgFullName,
                            ':specializedOrgShortName' => $specializedOrgShortName,
                            ':specializedOrgPostAddress' => $specializedOrgPostAddress,
                            ':specializedOrgFactAddress' => $specializedOrgFactAddress,
                            ':specializedOrgInn' => $specializedOrgInn,
                            ':specializedOrgKpp' => $specializedOrgKpp,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_specialized_org

                //-----------Вставка данных в ntf_purchase_responsible_last_specialized_org
                if (
                    $lastSpecializedOrgRegNum !== '' ||
                    $lastSpecializedOrgConsRegistryNum !== '' ||
                    $lastSpecializedOrgFullName !== '' ||
                    $lastSpecializedOrgShortName !== '' ||
                    $lastSpecializedOrgPostAddress !== '' ||
                    $lastSpecializedOrgFactAddress !== '' ||
                    $lastSpecializedOrgInn !== '' ||
                    $lastSpecializedOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_specialized_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :lastSpecializedOrgRegNum,
                        :lastSpecializedOrgConsRegistryNum,
                        :lastSpecializedOrgFullName,
                        :lastSpecializedOrgShortName,
                        :lastSpecializedOrgPostAddress,
                        :lastSpecializedOrgFactAddress,
                        :lastSpecializedOrgInn,
                        :lastSpecializedOrgKpp
                        )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':lastSpecializedOrgRegNum' => $lastSpecializedOrgRegNum,
                            ':lastSpecializedOrgConsRegistryNum' => $lastSpecializedOrgConsRegistryNum,
                            ':lastSpecializedOrgFullName' => $lastSpecializedOrgFullName,
                            ':lastSpecializedOrgShortName' => $lastSpecializedOrgShortName,
                            ':lastSpecializedOrgPostAddress' => $lastSpecializedOrgPostAddress,
                            ':lastSpecializedOrgFactAddress' => $lastSpecializedOrgFactAddress,
                            ':lastSpecializedOrgInn' => $lastSpecializedOrgInn,
                            ':lastSpecializedOrgKpp' => $lastSpecializedOrgKpp,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_last_specialized_org

                //-----------Вставка данных в ntf_public_discussion
                if (
                    $publicDiscussionNumber !== '' ||
                    $publicDiscussionOrganizationCh5St15 !== '' ||
                    $publicDiscussionHref !== '' ||
                    $publicDiscussionPlace !== '' ||
                    $publicDiscussion2017ProtocolDate !== '' ||
                    $publicDiscussion2017ProtocolPublishDate !== '' ||
                    $publicDiscussion2017PublicDiscussionPhase2Num !== '' ||
                    $publicDiscussion2017HrefPhase2 !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_public_discussion (
                            ntf_lot_id,
                            number,
                            organization_ch5_st15,
                            href,
                            place,
                            public_discussion2017_p_d_l_p_p2_protocol_date,
                            public_discussion2017_p_d_l_p_p2_protocol_publish_date,
                            public_discussion2017_p_d_l_p_p2_public_discussion_phase2_num,
                            public_discussion2017_p_d_l_p_p2_href_phase2
                          ) 
                          VALUES(
                            :ntfLotId,
                            :publicDiscussionNumber,
                            :publicDiscussionOrganizationCh5St15,
                            :publicDiscussionHref,
                            :publicDiscussionPlace,
                            :publicDiscussion2017ProtocolDate,
                            :publicDiscussion2017ProtocolPublishDate,
                            :publicDiscussion2017PublicDiscussionPhase2Num,
                            :publicDiscussion2017HrefPhase2
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfLotId' => $ntfLotId,
                            ':publicDiscussionNumber' => $publicDiscussionNumber,
                            ':publicDiscussionOrganizationCh5St15' => $publicDiscussionOrganizationCh5St15,
                            ':publicDiscussionHref' => $publicDiscussionHref,
                            ':publicDiscussionPlace' => $publicDiscussionPlace,
                            ':publicDiscussion2017ProtocolDate' => $publicDiscussion2017ProtocolDate,
                            ':publicDiscussion2017ProtocolPublishDate' => $publicDiscussion2017ProtocolPublishDate,
                            ':publicDiscussion2017PublicDiscussionPhase2Num' => $publicDiscussion2017PublicDiscussionPhase2Num,
                            ':publicDiscussion2017HrefPhase2' => $publicDiscussion2017HrefPhase2,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_public_discussion

                //-----------Вставка данных в ntf_placing_way
                if (
                    $placingWayCode !== '' ||
                    $placingWayName !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_placing_way (
                            ntf_main_id,
                            code,
                            name
                          ) 
                          VALUES(
                            :ntfMainId,
                            :placingWayCode,
                            :placingWayName
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':placingWayCode' => $placingWayCode,
                            ':placingWayName' => $placingWayName,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_placing_way

                //-----------Вставка данных в ntf_procedure_info_collecting
                if (
                    $procedureInfoCollectingStartDate !== '' ||
                    $procedureInfoCollectingPlace !== '' ||
                    $procedureInfoCollectingOrder !== '' ||
                    $procedureInfoCollectingEndDate !== '' ||
                    $procedureInfoCollectingForm !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_procedure_info_collecting (
                            ntf_main_id,
                            start_date,
                            place,
                            ntf_order,
                            end_date,
                            form
                          ) 
                          VALUES(
                            :ntfMainId,
                            :procedureInfoCollectingStartDate,
                            :procedureInfoCollectingPlace,
                            :procedureInfoCollectingOrder,
                            :procedureInfoCollectingEndDate,
                            :procedureInfoCollectingForm
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':procedureInfoCollectingStartDate' => $procedureInfoCollectingStartDate,
                            ':procedureInfoCollectingPlace' => $procedureInfoCollectingPlace,
                            ':procedureInfoCollectingOrder' => $procedureInfoCollectingOrder,
                            ':procedureInfoCollectingEndDate' => $procedureInfoCollectingEndDate,
                            ':procedureInfoCollectingForm' => $procedureInfoCollectingForm,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_procedure_info_collecting

                //-----------Вставка данных в ntf_procedure_info_opening
                if (
                    $procedureInfoOpeningDate !== '' ||
                    $procedureInfoOpeningPlace !== '' ||
                    $procedureInfoOpeningAddInfo !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_procedure_info_opening (
                            ntf_main_id,
                            date,
                            place,
                            add_info
                          ) 
                          VALUES(
                            :ntfMainId,
                            :procedureInfoOpeningDate,
                            :procedureInfoOpeningPlace,
                            :procedureInfoOpeningAddInfo
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':procedureInfoOpeningDate' => $procedureInfoOpeningDate,
                            ':procedureInfoOpeningPlace' => $procedureInfoOpeningPlace,
                            ':procedureInfoOpeningAddInfo' => $procedureInfoOpeningAddInfo,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_procedure_info_opening

                //-----------Вставка данных в ntf_procedure_info_contracting
                if (
                    $procedureInfoContractingContractingTerm !== '' ||
                    $procedureInfoContractingEvadeConditions !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_procedure_info_contracting (
                            ntf_main_id,
                            contracting_term,
                            evade_conditions
                          ) 
                          VALUES(
                            :ntfMainId,
                            :procedureInfoContractingContractingTerm,
                            :procedureInfoContractingEvadeConditions
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':procedureInfoContractingContractingTerm' => $procedureInfoContractingContractingTerm,
                            ':procedureInfoContractingEvadeConditions' => $procedureInfoContractingEvadeConditions,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_procedure_info_contracting

            }
        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }
    }

    public static function parseTypeEA($pathExtraction, $file)
    {
        $contentString = file_get_contents($pathExtraction . $file);
        $xml = self::normalizeXml($contentString);
        if ($xml) {
            //print_r($xml);
            //die;
            //--------ntf_main variables
            $ntfId = '';
            $ntfType = "fcsNotificationEA44";
            $externalId = '';
            $purchaseNumber = '';
            $directDate = '';
            $docPublishDate = '';
            $docNumber = '';
            $href = '';
            $purchaseObjectInfo = '';
            $purchaseResponsibleResponsibleRole = '';
            $okpd2okved2 = '';
            $filename = basename($file);
            //*--------ntf_main variables

            //--------ntf_print_form variables
            $printFormUrl = '';
            //$printFormSignature = '';
            //*--------ntf_print_form variables

            //--------ntf_extprint_form variables
            $extPrintFormContent = '';
            $extPrintFormUrl = '';
            $extPrintFormFileType = '';
            $extPrintFormSignatureType = '';
            $extPrintFormControlPersonSignatureType = '';
            //*--------ntf_extprint_form variables

            //--------ntf_purchase_responsible_responsible_org variables
            $responsibleOrgRegNum = '';
            $responsibleOrgConsRegistryNum = '';
            $responsibleOrgFullName = '';
            $responsibleOrgShortName = '';
            $responsibleOrgPostAddress = '';
            $responsibleOrgFactAddress = '';
            $responsibleOrgInn = '';
            $responsibleOrgKpp = '';
            //*--------ntf_purchase_responsible_responsible_org variables

            //--------ntf_purchase_responsible_responsible_info variables
            $responsibleInfoOrgPostAddress = '';
            $responsibleInfoOrgFactAddress = '';
            $contactPersonLastName = '';
            $contactPersonFirstName = '';
            $contactPersonMiddleName = '';
            $responsibleInfoContactEMail = '';
            $responsibleInfoContactPhone = '';
            $responsibleInfoContactFax = '';
            $responsibleInfoAddInfo = '';
            //*--------ntf_purchase_responsible_responsible_info variables

            //--------ntf_purchase_responsible_specialized_org variables
            $specializedOrgRegNum = '';
            $specializedOrgConsRegistryNum = '';
            $specializedOrgFullName = '';
            $specializedOrgShortName = '';
            $specializedOrgPostAddress = '';
            $specializedOrgFactAddress = '';
            $specializedOrgInn = '';
            $specializedOrgKpp = '';
            //*--------ntf_purchase_responsible_specialized_org variables

            //--------ntf_purchase_responsible_last_specialized_org variables
            $lastSpecializedOrgRegNum = '';
            $lastSpecializedOrgConsRegistryNum = '';
            $lastSpecializedOrgFullName = '';
            $lastSpecializedOrgShortName = '';
            $lastSpecializedOrgPostAddress = '';
            $lastSpecializedOrgFactAddress = '';
            $lastSpecializedOrgInn = '';
            $lastSpecializedOrgKpp = '';
            //*--------ntf_purchase_responsible_last_specialized_org variables

            //--------ntf_etp variables
            $etpCode = '';
            $etpName = '';
            $etpUrl = '';
            //*--------ntf_etp variables

            //--------ntf_placing_way variables
            $placingWayCode = '';
            $placingWayName = '';
            //*--------ntf_placing_way variables

            //--------ntf_procedure_info_collecting variables
            $procedureInfoCollectingStartDate = '';
            $procedureInfoCollectingPlace = '';
            $procedureInfoCollectingOrder = '';
            $procedureInfoCollectingEndDate = '';
            //*--------ntf_procedure_info_collecting variables

            //--------ntf_procedure_info_scoring variables
            $procedureInfoScoringDate = '';
            //*--------ntf_procedure_info_scoring variables

            //--------ntf_procedure_info_bidding variables
            $procedureInfoBiddingDate = '';
            $procedureInfoBiddingAddInfo = '';
            //*--------ntf_procedure_info_bidding variables

            //--------ntf_lot variables
            $lotMaxPrice = '';
            $lotPriceFormula = '';
            $lotStandardContractNumber = '';
            $lotCurrencyCode = '';
            $lotCurrencyName = '';
            $lotFinanceSource = '';
            $lotInterbudgetaryTransfer = '';
            $lotQuantityUndefined = '';
            $lotPurchaseObjectsTotalSum = '';
            $lotDrugPurchaseObjectsInfoTotal = '';
            $lotRestrictInfo = '';
            $lotAddInfo = '';
            $lotNoPublicDiscussion = '';
            $lotMustPublicDiscussion = '';
            //*--------ntf_lot variables

            //--------ntf_public_discussion variables
            $publicDiscussionNumber = '';
            $publicDiscussionOrganizationCh5St15 = '';
            $publicDiscussionHref = '';
            $publicDiscussionPlace = '';
            $publicDiscussion2017ProtocolDate = '';
            $publicDiscussion2017ProtocolPublishDate = '';
            $publicDiscussion2017PublicDiscussionPhase2Num = '';
            $publicDiscussion2017HrefPhase2 = '';
            //*--------ntf_public_discussion variables

            //-----------ntf_main
            if (isset($xml->fcsNotificationEF->id)) {
                $ntfId = (string)$xml->fcsNotificationEF->id;
            }
            if (isset($xml->fcsNotificationEF->externalId)) {
                $externalId = (string)$xml->fcsNotificationEF->externalId;
            }
            if (isset($xml->fcsNotificationEF->purchaseNumber)) {
                $purchaseNumber = (string)$xml->fcsNotificationEF->purchaseNumber;
            }
            if (isset($xml->fcsNotificationEF->directDate)) {
                $directDate = (string)$xml->fcsNotificationEF->directDate;
            }
            if (isset($xml->fcsNotificationEF->docPublishDate)) {
                $docPublishDate = (string)$xml->fcsNotificationEF->docPublishDate;
            }
            if (isset($xml->fcsNotificationEF->docNumber)) {
                $docNumber = (string)$xml->fcsNotificationEF->docNumber;
            }
            if (isset($xml->fcsNotificationEF->href)) {
                $href = (string)$xml->fcsNotificationEF->href;
            }
            if (isset($xml->fcsNotificationEF->purchaseObjectInfo)) {
                $purchaseObjectInfo = (string)$xml->fcsNotificationEF->purchaseObjectInfo;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleRole)) {
                $purchaseResponsibleResponsibleRole = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleRole;
            }
            if (isset($xml->fcsNotificationEF->externalId)) {
                $externalId = (string)$xml->fcsNotificationEF->externalId;
            }
            if (isset($xml->fcsNotificationEF->okpd2okved2)) {
                $okpd2okved2 = (string)$xml->fcsNotificationEF->okpd2okved2;
            }

            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->regNum)) {
                $responsibleOrgRegNum = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->regNum;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->consRegistryNum)) {
                $responsibleOrgConsRegistryNum = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->fullName)) {
                $responsibleOrgFullName = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->fullName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->shortName)) {
                $responsibleOrgShortName = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->shortName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->postAddress)) {
                $responsibleOrgPostAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->postAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->factAddress)) {
                $responsibleOrgFactAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->factAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->INN)) {
                $responsibleOrgInn = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->INN;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->KPP)) {
                $responsibleOrgKpp = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleOrg->KPP;
            }

            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->orgPostAddress)) {
                $responsibleInfoOrgPostAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->orgPostAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->orgFactAddress)) {
                $responsibleInfoOrgFactAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->orgFactAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPerson->lastName)) {
                $contactPersonLastName = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPerson->lastName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPerson->firstName)) {
                $contactPersonFirstName = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPerson->firstName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPerson->middleName)) {
                $contactPersonMiddleName = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPerson->middleName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactEMail)) {
                $responsibleInfoContactEMail = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactEMail;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPhone)) {
                $responsibleInfoContactPhone = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactPhone;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactFax)) {
                $responsibleInfoContactFax = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->contactFax;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->addInfo)) {
                $responsibleInfoAddInfo = (string)$xml->fcsNotificationEF->purchaseResponsible->responsibleInfo->addInfo;
            }

            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->regNum)) {
                $specializedOrgRegNum = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->regNum;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->consRegistryNum)) {
                $specializedOrgConsRegistryNum = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->fullName)) {
                $specializedOrgFullName = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->fullName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->shortName)) {
                $specializedOrgShortName = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->shortName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->postAddress)) {
                $specializedOrgPostAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->postAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->factAddress)) {
                $specializedOrgFactAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->factAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->INN)) {
                $specializedOrgInn = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->INN;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->specializedOrg->KPP)) {
                $specializedOrgKpp = (string)$xml->fcsNotificationEF->purchaseResponsible->specializedOrg->KPP;
            }

            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->regNum)) {
                $lastSpecializedOrgRegNum = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->regNum;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->consRegistryNum)) {
                $lastSpecializedOrgConsRegistryNum = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->consRegistryNum;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->fullName)) {
                $lastSpecializedOrgFullName = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->fullName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->shortName)) {
                $lastSpecializedOrgShortName = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->shortName;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->postAddress)) {
                $lastSpecializedOrgPostAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->postAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->factAddress)) {
                $lastSpecializedOrgFactAddress = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->factAddress;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->INN)) {
                $lastSpecializedOrgInn = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->INN;
            }
            if (isset($xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->KPP)) {
                $lastSpecializedOrgKpp = (string)$xml->fcsNotificationEF->purchaseResponsible->lastSpecializedOrg->KPP;
            }

            if (isset($xml->fcsNotificationEF->ETP->code)) {
                $etpCode = (string)$xml->fcsNotificationEF->ETP->code;
            }
            if (isset($xml->fcsNotificationEF->ETP->name)) {
                $etpName = (string)$xml->fcsNotificationEF->ETP->name;
            }
            if (isset($xml->fcsNotificationEF->ETP->url)) {
                $etpUrl = (string)$xml->fcsNotificationEF->ETP->url;
            }

            //-----------ntf_placing_way
            if (isset($xml->fcsNotificationEF->placingWay->code)) {
                $placingWayCode = (string)$xml->fcsNotificationEF->placingWay->code;
            }
            if (isset($xml->fcsNotificationEF->placingWay->name)) {
                $placingWayName = (string)$xml->fcsNotificationEF->placingWay->name;
            }
            //*-----------ntf_placing_way

            //-----------ntf_procedure_info_collecting

            if (isset($xml->fcsNotificationEF->procedureInfo->collecting->startDate)) {
                $procedureInfoCollectingStartDate = (string)$xml->fcsNotificationEF->procedureInfo->collecting->startDate;
            }
            if (isset($xml->fcsNotificationEF->procedureInfo->collecting->place)) {
                $procedureInfoCollectingPlace = (string)$xml->fcsNotificationEF->procedureInfo->collecting->place;
            }
            if (isset($xml->fcsNotificationEF->procedureInfo->collecting->order)) {
                $procedureInfoCollectingOrder = (string)$xml->fcsNotificationEF->procedureInfo->collecting->order;
            }
            if (isset($xml->fcsNotificationEF->procedureInfo->collecting->endDate)) {
                $procedureInfoCollectingEndDate = (string)$xml->fcsNotificationEF->procedureInfo->collecting->endDate;
            }
            //*-----------ntf_procedure_info_collecting
            //-----------ntf_procedure_info_scoring
            if (isset($xml->fcsNotificationEF->procedureInfo->scoring->date)) {
                $procedureInfoScoringDate = (string)$xml->fcsNotificationEF->procedureInfo->scoring->date;
            }
            //*-----------ntf_procedure_info_scoring

            //-----------ntf_procedure_info_bidding
            if (isset($xml->fcsNotificationEF->procedureInfo->bidding->date)) {
                $procedureInfoBiddingDate = (string)$xml->fcsNotificationEF->procedureInfo->bidding->date;
            }
            if (isset($xml->fcsNotificationEF->procedureInfo->bidding->addInfo)) {
                $procedureInfoBiddingAddInfo = (string)$xml->fcsNotificationEF->procedureInfo->bidding->addInfo;
            }
            //*-----------ntf_procedure_info_bidding

            //-----------ntf_public_discussion
            if (isset($xml->fcsNotificationEF->publicDiscussion->number)) {
                $publicDiscussionNumber = (string)$xml->fcsNotificationEF->publicDiscussion->number;
            }
            if (isset($xml->fcsNotificationEF->publicDiscussion->organizationCh5St15)) {
                $publicDiscussionOrganizationCh5St15 = (string)$xml->fcsNotificationEF->publicDiscussion->organizationCh5St15;
            }
            if (isset($xml->fcsNotificationEF->publicDiscussion->href)) {
                $publicDiscussionHref = (string)$xml->fcsNotificationEF->publicDiscussion->href;
            }
            if (isset($xml->fcsNotificationEF->publicDiscussion->place)) {
                $publicDiscussionPlace = (string)$xml->fcsNotificationEF->publicDiscussion->place;
            }
            if (isset($xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolDate)) {
                $publicDiscussion2017ProtocolDate = (string)$xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolDate;
            }
            if (isset($xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolPublishDate)) {
                $publicDiscussion2017ProtocolPublishDate = (string)$xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->protocolPublishDate;
            }
            if (isset($xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->publicDiscussionPhase2Num)) {
                $publicDiscussion2017PublicDiscussionPhase2Num = (string)$xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->publicDiscussionPhase2Num;
            }
            if (isset($xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->hrefPhase2)) {
                $publicDiscussion2017HrefPhase2 = (string)$xml->fcsNotificationEF->publicDiscussion->publicDiscussion2017->publicDiscussionLargePurchasePhase2->hrefPhase2;
            }
            //*-----------ntf_public_discussion

            //*-----------ntf_main
            //-----------Вставка данных в ntf_main
            $sql = "
                      INSERT INTO ntf_main (
                          ntf_id,
                          type,
                          external_id,
                          purchase_number,
                          direct_date,
                          doc_publish_date,
                          doc_number,
                          href,
                          purchase_object_info,
                          purchase_responsible_responsible_role,
                          okpd2okved2,
                          filename,
                          created_at
                      ) 
                      VALUES(
                          :ntfId,
                          :ntfType,
                          :externalId,
                          :purchaseNumber,
                          :directDate,
                          :docPublishDate,
                          :docNumber,
                          :href,
                          :purchaseObjectInfo,
                          :purchaseResponsibleResponsibleRole,
                          :okpd2okved2,
                          :filename,
                          :createdAt
                      )
                      ON CONFLICT (ntf_id) DO NOTHING 
                      RETURNING id";
            $returningState = Yii::$app->db_zakupki->createCommand($sql)
                ->bindValues([
                    ':ntfId' => $ntfId,
                    ':ntfType' => $ntfType,
                    ':externalId' => $externalId,
                    ':purchaseNumber' => $purchaseNumber,
                    ':directDate' => $directDate,
                    ':docPublishDate' => $docPublishDate,
                    ':docNumber' => $docNumber,
                    ':href' => $href,
                    ':purchaseObjectInfo' => $purchaseObjectInfo,
                    ':purchaseResponsibleResponsibleRole' => $purchaseResponsibleResponsibleRole,
                    ':okpd2okved2' => $okpd2okved2,
                    ':filename' => $filename,
                    ':createdAt' => date("Y-m-d H:i:s"),
                ])
                ->execute();

            $ntfMainId = Yii::$app->db_zakupki->getLastInsertID('ntf_main_id_seq');
            if ($returningState !== 0) {
                //echo "такой записи еще нет." . "\n";
                //var_dump($returningState);
                //var_dump($ntfMainId);
            } else {
                echo "Record with ntf_id = " . $ntfId . " exist." . "\n";
                return;
            }
            //*-----------Вставка данных в ntf_main

            //-----------ntf_print_form
            if (isset($xml->fcsNotificationEF->printForm->url)) {
                $printFormUrl = (string)$xml->fcsNotificationEF->printForm->url;
            }
            if (isset($xml->fcsNotificationEF->printForm->signature)) {
                $printFormSignature = (string)$xml->fcsNotificationEF->printForm->signature;
            }
            //*-----------ntf_print_form
            //-----------Вставка данных в ntf_print_form
            if (
                $ntfMainId !== '' ||
                $printFormUrl !== ''
            ) {
                $sql = "
                  INSERT INTO ntf_print_form (
                    ntf_main_id,
                    url
                  ) 
                  VALUES(
                    :ntfMainId,
                    :printFormUrl
                )";
                Yii::$app->db_zakupki->createCommand($sql)
                    ->bindValues([
                        ':ntfMainId' => $ntfMainId,
                        ':printFormUrl' => $printFormUrl,
                    ])
                    ->execute();
            }
            //*-----------Вставка данных в ntf_print_form

            //-----------ntf_lot
            if (isset($xml->fcsNotificationEF->lot->maxPrice)) {
                $lotMaxPrice = (string)$xml->fcsNotificationEF->lot->maxPrice;
            }
            if (isset($xml->fcsNotificationEF->lot->priceFormula)) {
                $lotPriceFormula = (string)$xml->fcsNotificationEF->lot->priceFormula;
            }
            if (isset($xml->fcsNotificationEF->lot->standardContractNumber)) {
                $lotStandardContractNumber = (string)$xml->fcsNotificationEF->lot->standardContractNumber;
            }
            if (isset($xml->fcsNotificationEF->lot->currency->code)) {
                $lotCurrencyCode = (string)$xml->fcsNotificationEF->lot->currency->code;
            }
            if (isset($xml->fcsNotificationEF->lot->currency->name)) {
                $lotCurrencyName = (string)$xml->fcsNotificationEF->lot->currency->name;
            }
            if (isset($xml->fcsNotificationEF->lot->financeSource)) {
                $lotFinanceSource = (string)$xml->fcsNotificationEF->lot->financeSource;
            }
            if (isset($xml->fcsNotificationEF->lot->interbudgetaryTransfer)) {
                $lotInterbudgetaryTransfer = (string)$xml->fcsNotificationEF->lot->interbudgetaryTransfer;
            }
            if (isset($xml->fcsNotificationEF->lot->quantityUndefined)) {
                $lotQuantityUndefined = (string)$xml->fcsNotificationEF->lot->quantityUndefined;
            }
            if (isset($xml->fcsNotificationEF->lot->purchaseObjects->totalSum)) {
                $lotPurchaseObjectsTotalSum = (string)$xml->fcsNotificationEF->lot->purchaseObjects->totalSum;
            }
            if (isset($xml->fcsNotificationEF->lot->drugPurchaseObjectsInfo->total)) {
                $lotDrugPurchaseObjectsInfoTotal = (string)$xml->fcsNotificationEF->lot->drugPurchaseObjectsInfo->total;
            }
            if (isset($xml->fcsNotificationEF->lot->restrictInfo)) {
                $lotRestrictInfo = (string)$xml->fcsNotificationEF->lot->restrictInfo;
            }
            if (isset($xml->fcsNotificationEF->lot->addInfo)) {
                $lotAddInfo = (string)$xml->fcsNotificationEF->lot->addInfo;
            }
            if (isset($xml->fcsNotificationEF->lot->noPublicDiscussion)) {
                $lotNoPublicDiscussion = (string)$xml->fcsNotificationEF->lot->noPublicDiscussion;
            }
            if (isset($xml->fcsNotificationEF->lot->mustPublicDiscussion)) {
                $lotMustPublicDiscussion = (string)$xml->fcsNotificationEF->lot->mustPublicDiscussion;
            }
            //*-----------ntf_lot

            //-----------Вставка данных в ntf_lot
            if (
                $lotMaxPrice !== '' ||
                $lotPriceFormula !== '' ||
                $lotStandardContractNumber !== '' ||
                $lotCurrencyCode !== '' ||
                $lotCurrencyName !== '' ||
                $lotFinanceSource !== '' ||
                $lotInterbudgetaryTransfer !== '' ||
                $lotQuantityUndefined !== '' ||
                $lotPurchaseObjectsTotalSum !== '' ||
                $lotDrugPurchaseObjectsInfoTotal !== '' ||
                $lotRestrictInfo !== '' ||
                $lotAddInfo !== '' ||
                $lotNoPublicDiscussion !== '' ||
                $lotMustPublicDiscussion !== ''
            ) {
                $sql = "
                  INSERT INTO ntf_lot (
                    ntf_main_id,
                    max_price,
                    price_formula,
                    standard_contract_number,
                    currency_code,
                    currency_name,
                    finance_source,
                    interbudgetary_transfer,
                    quantity_undefined,
                    purchase_objects_total_sum,
                    drug_purchase_objects_info_total,
                    restrict_info,
                    add_info,
                    no_public_discussion,
                    must_public_discussion
                  ) 
                  VALUES(
                    :ntfMainId,
                    :lotMaxPrice,
                    :lotPriceFormula,
                    :lotStandardContractNumber,
                    :lotCurrencyCode,
                    :lotCurrencyName,
                    :lotFinanceSource,
                    :lotInterbudgetaryTransfer,
                    :lotQuantityUndefined,
                    :lotPurchaseObjectsTotalSum,
                    :lotDrugPurchaseObjectsInfoTotal,
                    :lotRestrictInfo,
                    :lotAddInfo,
                    :lotNoPublicDiscussion,
                    :lotMustPublicDiscussion
                )";
                Yii::$app->db_zakupki->createCommand($sql)
                    ->bindValues([
                        ':ntfMainId' => $ntfMainId,
                        ':lotMaxPrice' => $lotMaxPrice,
                        ':lotPriceFormula' => $lotPriceFormula,
                        ':lotStandardContractNumber' => $lotStandardContractNumber,
                        ':lotCurrencyCode' => $lotCurrencyCode,
                        ':lotCurrencyName' => $lotCurrencyName,
                        ':lotFinanceSource' => $lotFinanceSource,
                        ':lotInterbudgetaryTransfer' => $lotInterbudgetaryTransfer,
                        ':lotQuantityUndefined' => $lotQuantityUndefined,
                        ':lotPurchaseObjectsTotalSum' => $lotPurchaseObjectsTotalSum,
                        ':lotDrugPurchaseObjectsInfoTotal' => $lotDrugPurchaseObjectsInfoTotal,
                        ':lotRestrictInfo' => $lotRestrictInfo,
                        ':lotAddInfo' => $lotAddInfo,
                        ':lotNoPublicDiscussion' => $lotNoPublicDiscussion,
                        ':lotMustPublicDiscussion' => $lotMustPublicDiscussion,
                    ])
                    ->execute();
            }
            $ntfLotId = Yii::$app->db_zakupki->getLastInsertID('ntf_lot_id_seq');
            //*-----------Вставка данных в ntf_lot

            //-----------ntf_customer_requirement
            if (isset($xml->fcsNotificationEF->lot->customerRequirements)) {
                foreach ($xml->fcsNotificationEF->lot->customerRequirements as $customerRequirement) {
                    $customerRegNum = '';
                    $customerConsRegistryNum = '';
                    $customerFullName = '';
                    $maxPrice = '';
                    $deliveryPlace = '';
                    $deliveryTerm = '';
                    $addInfo = '';
                    $purchaseCode = '';
                    $nonbudgetFinancingTotalSum = '';
                    $budgetFinancingsTotalSum = '';
                    $BONumber = '';
                    $BODate = '';
                    $inputBOFlag = '';
                    $applicationGuaranteeAmount = '';
                    $applicationGuaranteePart = '';
                    $applicationGuaranteeProcedureInfo = '';
                    $applicationGuaranteeSettlementAccount = '';
                    $applicationGuaranteePersonalAccount = '';
                    $applicationGuaranteeBik = '';
                    $contractGuaranteeAmount = '';
                    $contractGuaranteePart = '';
                    $contractGuaranteeProcedureInfo = '';
                    $contractGuaranteeSettlementAccount = '';
                    $contractGuaranteePersonalAccount = '';
                    $contractGuaranteeBik = '';
                    $tenderPlanInfoPlanNumber = '';
                    $tenderPlanInfoPositionNumber = '';
                    $tenderPlanInfoPurchase83st544 = '';
                    $tenderPlanInfoPlan2017Number = '';
                    $tenderPlanInfoPosition2017Number = '';
                    $tenderPlanInfoPosition2017ExtNumber = '';

                    if (isset($customerRequirement->customerRequirement->customer->regNum)) {
                        $customerRegNum = (string)$customerRequirement->customerRequirement->customer->regNum;
                    }
                    if (isset($customerRequirement->customerRequirement->customer->consRegistryNum)) {
                        $customerConsRegistryNum = (string)$customerRequirement->customerRequirement->customer->consRegistryNum;
                    }
                    if (isset($customerRequirement->customerRequirement->customer->fullName)) {
                        $customerFullName = (string)$customerRequirement->customerRequirement->customer->fullName;
                    }
                    if (isset($customerRequirement->customerRequirement->maxPrice)) {
                        $maxPrice = (string)$customerRequirement->customerRequirement->maxPrice;
                    }
                    if (isset($customerRequirement->customerRequirement->deliveryPlace)) {
                        $deliveryPlace = (string)$customerRequirement->customerRequirement->deliveryPlace;
                    }
                    if (isset($customerRequirement->customerRequirement->deliveryTerm)) {
                        $deliveryTerm = (string)$customerRequirement->customerRequirement->deliveryTerm;
                    }
                    if (isset($customerRequirement->customerRequirement->addInfo)) {
                        $addInfo = (string)$customerRequirement->customerRequirement->addInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->purchaseCode)) {
                        $purchaseCode = (string)$customerRequirement->customerRequirement->purchaseCode;
                    }
                    if (isset($customerRequirement->customerRequirement->nonbudgetFinancing->totalSum)) {
                        $nonbudgetFinancingTotalSum = (string)$customerRequirement->customerRequirement->nonbudgetFinancing->totalSum;
                    }
                    if (isset($customerRequirement->customerRequirement->budgetFinancings->totalSum)) {
                        $budgetFinancingsTotalSum = (string)$customerRequirement->customerRequirement->budgetFinancings->totalSum;
                    }

                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->amount)) {
                        $applicationGuaranteeAmount = (string)$customerRequirement->customerRequirement->applicationGuarantee->amount;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->part)) {
                        $applicationGuaranteePart = (string)$customerRequirement->customerRequirement->applicationGuarantee->part;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->procedureInfo)) {
                        $applicationGuaranteeProcedureInfo = (string)$customerRequirement->customerRequirement->applicationGuarantee->procedureInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->settlementAccount)) {
                        $applicationGuaranteeSettlementAccount = (string)$customerRequirement->customerRequirement->applicationGuarantee->settlementAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->personalAccount)) {
                        $applicationGuaranteePersonalAccount = (string)$customerRequirement->customerRequirement->applicationGuarantee->personalAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->applicationGuarantee->bik)) {
                        $applicationGuaranteeBik = (string)$customerRequirement->customerRequirement->applicationGuarantee->bik;
                    }

                    if (isset($customerRequirement->customerRequirement->contractGuarantee->amount)) {
                        $contractGuaranteeAmount = (string)$customerRequirement->customerRequirement->contractGuarantee->amount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->part)) {
                        $contractGuaranteePart = (string)$customerRequirement->customerRequirement->contractGuarantee->part;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->procedureInfo)) {
                        $contractGuaranteeProcedureInfo = (string)$customerRequirement->customerRequirement->contractGuarantee->procedureInfo;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->settlementAccount)) {
                        $contractGuaranteeSettlementAccount = (string)$customerRequirement->customerRequirement->contractGuarantee->settlementAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->personalAccount)) {
                        $contractGuaranteePersonalAccount = (string)$customerRequirement->customerRequirement->contractGuarantee->personalAccount;
                    }
                    if (isset($customerRequirement->customerRequirement->contractGuarantee->bik)) {
                        $contractGuaranteeBik = (string)$customerRequirement->customerRequirement->contractGuarantee->bik;
                    }

                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->planNumber)) {
                        $tenderPlanInfoPlanNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->planNumber;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->positionNumber)) {
                        $tenderPlanInfoPositionNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->positionNumber;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->purchase83st544)) {
                        $tenderPlanInfoPurchase83st544 = (string)$customerRequirement->customerRequirement->tenderPlanInfo->purchase83st544;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->plan2017Number)) {
                        $tenderPlanInfoPlan2017Number = (string)$customerRequirement->customerRequirement->tenderPlanInfo->plan2017Number;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->position2017Number)) {
                        $tenderPlanInfoPosition2017Number = (string)$customerRequirement->customerRequirement->tenderPlanInfo->position2017Number;
                    }
                    if (isset($customerRequirement->customerRequirement->tenderPlanInfo->position2017ExtNumber)) {
                        $tenderPlanInfoPosition2017ExtNumber = (string)$customerRequirement->customerRequirement->tenderPlanInfo->position2017ExtNumber;
                    }

                    if (isset($customerRequirement->customerRequirement->BOInfo->BONumber)) {
                        $BONumber = (string)$customerRequirement->customerRequirement->BOInfo->BONumber;
                    }
                    if (isset($customerRequirement->customerRequirement->BOInfo->BODate)) {
                        $BODate = (string)$customerRequirement->customerRequirement->BOInfo->BODate;
                    }
                    if (isset($customerRequirement->customerRequirement->BOInfo->inputBOFlag)) {
                        $inputBOFlag = (string)$customerRequirement->customerRequirement->BOInfo->inputBOFlag;
                    }

                    //TODO: можно переделать в batchinsert всего массива данныъ, из foreach
                    //-----------Вставка данных в ntf_customer_requirement
                    if (
                        $customerRegNum !== '' ||
                        $customerConsRegistryNum !== '' ||
                        $customerFullName !== '' ||
                        $maxPrice !== '' ||
                        $deliveryPlace !== '' ||
                        $deliveryTerm !== '' ||
                        $addInfo !== '' ||
                        $purchaseCode !== '' ||
                        $nonbudgetFinancingTotalSum !== '' ||
                        $budgetFinancingsTotalSum !== '' ||
                        $BONumber !== '' ||
                        $BODate !== '' ||
                        $inputBOFlag !== '' ||
                        $applicationGuaranteeAmount !== '' ||
                        $applicationGuaranteePart !== '' ||
                        $applicationGuaranteeProcedureInfo !== '' ||
                        $applicationGuaranteeSettlementAccount !== '' ||
                        $applicationGuaranteePersonalAccount !== '' ||
                        $applicationGuaranteeBik !== '' ||
                        $contractGuaranteeAmount !== '' ||
                        $contractGuaranteePart !== '' ||
                        $contractGuaranteeProcedureInfo !== '' ||
                        $contractGuaranteeSettlementAccount !== '' ||
                        $contractGuaranteePersonalAccount !== '' ||
                        $contractGuaranteeBik !== '' ||
                        $tenderPlanInfoPlanNumber !== '' ||
                        $tenderPlanInfoPositionNumber !== '' ||
                        $tenderPlanInfoPurchase83st544 !== '' ||
                        $tenderPlanInfoPlan2017Number !== '' ||
                        $tenderPlanInfoPosition2017Number !== '' ||
                        $tenderPlanInfoPosition2017ExtNumber !== ''
                    ) {
                        $sql = "
                          INSERT INTO ntf_customer_requirement (
                            ntf_lot_id,
                            customer_reg_num,
                            customer_cons_registry_num,
                            customer_full_name,
                            max_price,
                            delivery_place,
                            delivery_term,
                            application_guarantee_amount,
                            application_guarantee_part,
                            application_guarantee_procedure_info,
                            application_guarantee_settlement_account,
                            application_guarantee_personal_account,
                            application_guarantee_bik,
                            contract_guarantee_amount,
                            contract_guarantee_part,
                            contract_guarantee_procedure_info,
                            contract_guarantee_settlement_account,
                            contract_guarantee_personal_account,
                            contract_guarantee_bik,
                            add_info,
                            purchase_code,
                            tender_plan_info_plan_number,
                            tender_plan_info_position_number,
                            tender_plan_info_purchase83st544,
                            tender_plan_info_plan2017_number,
                            tender_plan_info_position2017_number,
                            tender_plan_info_position2017_ext_number,
                            nonbudget_financings_total_sum,
                            budget_financings_total_sum,
                            bo_info_bo_number,
                            bo_info_bo_date,
                            bo_info_input_bo_flag
                          ) 
                          VALUES(
                            :ntfLotId,
                            :customerRegNum,
                            :customerConsRegistryNum,
                            :customerFullName,
                            :maxPrice,
                            :deliveryPlace,
                            :deliveryTerm,
                            :applicationGuaranteeAmount,
                            :applicationGuaranteePart,
                            :applicationGuaranteeProcedureInfo,
                            :applicationGuaranteeSettlementAccount,
                            :applicationGuaranteePersonalAccount,
                            :applicationGuaranteeBik,
                            :contractGuaranteeAmount,
                            :contractGuaranteePart,
                            :contractGuaranteeProcedureInfo,
                            :contractGuaranteeSettlementAccount,
                            :contractGuaranteePersonalAccount,
                            :contractGuaranteeBik,
                            :addInfo,
                            :purchaseCode,
                            :tenderPlanInfoPlanNumber,
                            :tenderPlanInfoPositionNumber,
                            :tenderPlanInfoPurchase83st544,
                            :tenderPlanInfoPlan2017Number,
                            :tenderPlanInfoPosition2017Number,
                            :tenderPlanInfoPosition2017ExtNumber,
                            :nonbudgetFinancingTotalSum,
                            :budgetFinancingsTotalSum,
                            :BONumber,
                            :BODate,
                            :inputBOFlag
                        )";
                        Yii::$app->db_zakupki->createCommand($sql)
                            ->bindValues([
                                ':ntfLotId' => $ntfLotId,
                                ':customerRegNum' => $customerRegNum,
                                ':customerConsRegistryNum' => $customerConsRegistryNum,
                                ':customerFullName' => $customerFullName,
                                ':maxPrice' => $maxPrice,
                                ':deliveryPlace' => $deliveryPlace,
                                ':deliveryTerm' => $deliveryTerm,
                                ':applicationGuaranteeAmount' => $applicationGuaranteeAmount,
                                ':applicationGuaranteePart' => $applicationGuaranteePart,
                                ':applicationGuaranteeProcedureInfo' => $applicationGuaranteeProcedureInfo,
                                ':applicationGuaranteeSettlementAccount' => $applicationGuaranteeSettlementAccount,
                                ':applicationGuaranteePersonalAccount' => $applicationGuaranteePersonalAccount,
                                ':applicationGuaranteeBik' => $applicationGuaranteeBik,
                                ':contractGuaranteeAmount' => $contractGuaranteeAmount,
                                ':contractGuaranteePart' => $contractGuaranteePart,
                                ':contractGuaranteeProcedureInfo' => $contractGuaranteeProcedureInfo,
                                ':contractGuaranteeSettlementAccount' => $contractGuaranteeSettlementAccount,
                                ':contractGuaranteePersonalAccount' => $contractGuaranteePersonalAccount,
                                ':contractGuaranteeBik' => $contractGuaranteeBik,
                                ':addInfo' => $addInfo,
                                ':purchaseCode' => $purchaseCode,
                                ':tenderPlanInfoPlanNumber' => $tenderPlanInfoPlanNumber,
                                ':tenderPlanInfoPositionNumber' => $tenderPlanInfoPositionNumber,
                                ':tenderPlanInfoPurchase83st544' => $tenderPlanInfoPurchase83st544,
                                ':tenderPlanInfoPlan2017Number' => $tenderPlanInfoPlan2017Number,
                                ':tenderPlanInfoPosition2017Number' => $tenderPlanInfoPosition2017Number,
                                ':tenderPlanInfoPosition2017ExtNumber' => $tenderPlanInfoPosition2017ExtNumber,
                                ':nonbudgetFinancingTotalSum' => $nonbudgetFinancingTotalSum,
                                ':budgetFinancingsTotalSum' => $budgetFinancingsTotalSum,
                                ':BONumber' => $BONumber,
                                ':BODate' => $BODate,
                                ':inputBOFlag' => $inputBOFlag,
                            ])
                            ->execute();
                    }
                    $ntfCustomerRequirementId = Yii::$app->db_zakupki->getLastInsertID('ntf_customer_requirement_id_seq');
                    //*-----------Вставка данных в ntf_customer_requirement

                    //-----------ntf_kladr_place
                    if (isset($customerRequirement->customerRequirement->kladrPlaces->kladrPlace)) {
                        foreach ($customerRequirement->customerRequirement->kladrPlaces->kladrPlace as $kladrPlace) {
                            $kladrPlaceKladrKladrType = '';
                            $kladrPlaceKladrKladrCode = '';
                            $kladrPlaceKladrFullName = '';
                            $kladrPlaceCountryCountryCode = '';
                            $kladrPlaceCountryCountryFullName = '';
                            $kladrPlaceNoKladrForRegionSettlement = '';
                            $kladrPlaceDeliveryPlace = '';
                            $kladrPlaceNoKladrForRegionSettlementRegion = '';
                            $kladrPlaceNoKladrForRegionSettlementSettlement = '';

                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceKladrKladrType = (string)$kladrPlace->kladr->kladrType;
                            }
                            if (isset($kladrPlace->kladr->kladrCode)) {
                                $kladrPlaceKladrKladrCode = (string)$kladrPlace->kladr->kladrCode;
                            }
                            if (isset($kladrPlace->kladr->fullName)) {
                                $kladrPlaceKladrFullName = (string)$kladrPlace->kladr->fullName;
                            }
                            if (isset($kladrPlace->country->countryCode)) {
                                $kladrPlaceCountryCountryCode = (string)$kladrPlace->country->countryCode;
                            }
                            if (isset($kladrPlace->country->countryFullName)) {
                                $kladrPlaceCountryCountryFullName = (string)$kladrPlace->country->countryFullName;
                            }
                            if (isset($kladrPlace->noKladrForRegionSettlement)) {
                                $kladrPlaceNoKladrForRegionSettlement = (string)$kladrPlace->noKladrForRegionSettlement;
                            }
                            if (isset($kladrPlace->deliveryPlace)) {
                                $kladrPlaceDeliveryPlace = (string)$kladrPlace->deliveryPlace;
                            }
                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceNoKladrForRegionSettlementRegion = (string)$kladrPlace->noKladrForRegionSettlement->region;
                            }
                            if (isset($kladrPlace->kladr->kladrType)) {
                                $kladrPlaceNoKladrForRegionSettlementSettlement = (string)$kladrPlace->noKladrForRegionSettlement->settlement;
                            }

                            //-----------Вставка данных в ntf_kladr_place
                            if (
                                $kladrPlaceKladrKladrType !== '' ||
                                $kladrPlaceKladrKladrCode !== '' ||
                                $kladrPlaceKladrFullName !== '' ||
                                $kladrPlaceCountryCountryCode !== '' ||
                                $kladrPlaceCountryCountryFullName !== '' ||
                                $kladrPlaceNoKladrForRegionSettlement !== '' ||
                                $kladrPlaceDeliveryPlace !== '' ||
                                $kladrPlaceNoKladrForRegionSettlementRegion !== '' ||
                                $kladrPlaceNoKladrForRegionSettlementSettlement !== ''
                            ) {
                                //-----------Вставка данных в ntf_kladr_place
                                $sql = "
                                  INSERT INTO ntf_kladr_place (
                                    ntf_customer_requirement_id,
                                    kladr_kladr_type,
                                    kladr_kladr_code,
                                    kladr_full_name,
                                    country_country_code,
                                    country_country_full_name,
                                    delivery_place,
                                    no_kladr_for_region_settlement_region,
                                    no_kladr_for_region_settlement_settlement,
                                    no_kladr_for_region_settlement
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :kladrPlaceKladrKladrType,
                                    :kladrPlaceKladrKladrCode,
                                    :kladrPlaceKladrFullName,
                                    :kladrPlaceCountryCountryCode,
                                    :kladrPlaceCountryCountryFullName,
                                    :kladrPlaceDeliveryPlace,
                                    :kladrPlaceNoKladrForRegionSettlementRegion,
                                    :kladrPlaceNoKladrForRegionSettlementSettlement,
                                    :kladrPlaceNoKladrForRegionSettlement
                                  )";
                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':kladrPlaceKladrKladrType' => $kladrPlaceKladrKladrType,
                                        ':kladrPlaceKladrKladrCode' => $kladrPlaceKladrKladrCode,
                                        ':kladrPlaceKladrFullName' => $kladrPlaceKladrFullName,
                                        ':kladrPlaceCountryCountryCode' => $kladrPlaceCountryCountryCode,
                                        ':kladrPlaceCountryCountryFullName' => $kladrPlaceCountryCountryFullName,
                                        ':kladrPlaceDeliveryPlace' => $kladrPlaceDeliveryPlace,
                                        ':kladrPlaceNoKladrForRegionSettlementRegion' => $kladrPlaceNoKladrForRegionSettlementRegion,
                                        ':kladrPlaceNoKladrForRegionSettlementSettlement' => $kladrPlaceNoKladrForRegionSettlementSettlement,
                                        ':kladrPlaceNoKladrForRegionSettlement' => $kladrPlaceNoKladrForRegionSettlement,
                                    ])
                                    ->execute();
                            }
                            //*-----------Вставка данных в ntf_kladr_place
                        }
                    }
                    //*-----------ntf_kladr_place

                    //-----------ntf_nonbudget_financing
                    if (isset($customerRequirement->customerRequirement->nonbudgetFinancings->nonbudgetFinancing)) {
                        foreach ($customerRequirement->customerRequirement->nonbudgetFinancings->nonbudgetFinancing as $nonbudgetFinancing) {
                            $nonbudgetFinancingKosguCode = '';
                            $nonbudgetFinancingKvrCode = '';
                            $nonbudgetFinancingYear = '';
                            $nonbudgetFinancingSum = '';

                            if (isset($nonbudgetFinancing->kosguCode)) {
                                $nonbudgetFinancingKosguCode = (string)$nonbudgetFinancing->kosguCode;
                            }
                            if (isset($nonbudgetFinancing->kvrCode)) {
                                $nonbudgetFinancingKvrCode = (string)$nonbudgetFinancing->kvrCode;
                            }
                            if (isset($nonbudgetFinancing->year)) {
                                $nonbudgetFinancingYear = (string)$nonbudgetFinancing->year;
                            }
                            if (isset($nonbudgetFinancing->sum)) {
                                $nonbudgetFinancingSum = (string)$nonbudgetFinancing->sum;
                            }
                            //-----------Вставка данных в ntf_nonbudget_financing
                            if (
                                $nonbudgetFinancingKosguCode !== '' ||
                                $nonbudgetFinancingKvrCode !== '' ||
                                $nonbudgetFinancingYear !== '' ||
                                $nonbudgetFinancingSum !== ''
                            ) {
                                $sql = "
                                  INSERT INTO ntf_nonbudget_financing (
                                    ntf_customer_requirement_id,
                                    kosgu_code,
                                    kvr_code,
                                    year,
                                    sum
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :nonbudgetFinancingKosguCode,
                                    :nonbudgetFinancingKvrCode,
                                    :nonbudgetFinancingYear,
                                    :nonbudgetFinancingSum
                                )";
                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':nonbudgetFinancingKosguCode' => $nonbudgetFinancingKosguCode,
                                        ':nonbudgetFinancingKvrCode' => $nonbudgetFinancingKvrCode,
                                        ':nonbudgetFinancingYear' => $nonbudgetFinancingYear,
                                        ':nonbudgetFinancingSum' => $nonbudgetFinancingSum,
                                    ])
                                    ->execute();
                            }
                            //-----------Вставка данных в ntf_nonbudget_financing
                        }
                    }
                    //*-----------ntf_nonbudget_financing

                    //-----------ntf_budget_financing
                    if (isset($customerRequirement->customerRequirement->budgetFinancings->budgetFinancing)) {
                        foreach ($customerRequirement->customerRequirement->budgetFinancings->budgetFinancing as $budgetFinancing) {
                            $budgetFinancingKbkCode = '';
                            $budgetFinancingKbkCode2016 = '';
                            $budgetFinancingYear = '';
                            $budgetFinancingSum = '';

                            if (isset($budgetFinancing->kbkCode)) {
                                $budgetFinancingKbkCode = (string)$budgetFinancing->kbkCode;
                            }
                            if (isset($budgetFinancing->kbkCode2016)) {
                                $budgetFinancingKbkCode2016 = (string)$budgetFinancing->kbkCode2016;
                            }
                            if (isset($budgetFinancing->year)) {
                                $budgetFinancingYear = (string)$budgetFinancing->year;
                            }
                            if (isset($budgetFinancing->sum)) {
                                $budgetFinancingSum = (string)$budgetFinancing->sum;
                            }
                            //-----------Вставка данных в ntf_budget_financing
                            if (
                                $budgetFinancingKbkCode !== '' ||
                                $budgetFinancingKbkCode2016 !== '' ||
                                $budgetFinancingYear !== '' ||
                                $budgetFinancingSum !== ''
                            ) {
                                $sql = "
                                  INSERT INTO ntf_budget_financing (
                                    ntf_customer_requirement_id,
                                    kbk_code,
                                    kbk_code2016,
                                    year,
                                    sum
                                  ) 
                                  VALUES(
                                    :ntfCustomerRequirementId,
                                    :budgetFinancingKbkCode,
                                    :budgetFinancingKbkCode2016,
                                    :budgetFinancingYear,
                                    :budgetFinancingSum
                                )";

                                Yii::$app->db_zakupki->createCommand($sql)
                                    ->bindValues([
                                        ':ntfCustomerRequirementId' => $ntfCustomerRequirementId,
                                        ':budgetFinancingKbkCode' => $budgetFinancingKbkCode,
                                        ':budgetFinancingKbkCode2016' => $budgetFinancingKbkCode2016,
                                        ':budgetFinancingYear' => $budgetFinancingYear,
                                        ':budgetFinancingSum' => $budgetFinancingSum,
                                    ])
                                    ->execute();
                            }
                            //-----------Вставка данных в ntf_budget_financing
                        }
                    }
                    //*-----------ntf_budget_financing
                }
                //-----------ntf_purchase_object
                if (isset($xml->fcsNotificationEF->lot->purchaseObjects->purchaseObject)) {
                    foreach ($xml->fcsNotificationEF->lot->purchaseObjects->purchaseObject as $purchaseObject) {
                        $purchaseObjectOKPDCode = '';
                        $purchaseObjectOKPDName = '';
                        $purchaseObjectOKPD2Code = '';
                        $purchaseObjectOKPD2Name = '';
                        $purchaseObjectName = '';
                        $purchaseObjectOKEICode = '';
                        $purchaseObjectOKEINationalCode = '';
                        $purchaseObjectPrice = '';
                        $purchaseObjectQuantityValue = '';
                        $purchaseObjectQuantityUndefined = '';
                        $purchaseObjectSum = '';

                        if (isset($purchaseObject->OKPD->code)) {
                            $purchaseObjectOKPDCode = (string)$purchaseObject->OKPD->code;
                        }
                        if (isset($purchaseObject->OKPD->name)) {
                            $purchaseObjectOKPDName = (string)$purchaseObject->OKPD->name;
                        }
                        if (isset($purchaseObject->OKPD2->code)) {
                            $purchaseObjectOKPD2Code = (string)$purchaseObject->OKPD2->code;
                        }
                        if (isset($purchaseObject->OKPD2->name)) {
                            $purchaseObjectOKPD2Name = (string)$purchaseObject->OKPD2->name;
                        }
                        if (isset($purchaseObject->name)) {
                            $purchaseObjectName = (string)$purchaseObject->name;
                        }
                        if (isset($purchaseObject->OKEI->code)) {
                            $purchaseObjectOKEICode = (string)$purchaseObject->OKEI->code;
                        }
                        if (isset($purchaseObject->OKEI->nationalCode)) {
                            $purchaseObjectOKEINationalCode = (string)$purchaseObject->OKEI->nationalCode;
                        }
                        if (isset($purchaseObject->price)) {
                            $purchaseObjectPrice = (string)$purchaseObject->price;
                        }
                        if (isset($purchaseObject->quantity->value)) {
                            $purchaseObjectQuantityValue = (string)$purchaseObject->quantity->value;
                        }
                        if (isset($purchaseObject->quantity->undefined)) {
                            $purchaseObjectQuantityUndefined = (string)$purchaseObject->quantity->undefined;
                        }
                        if (isset($purchaseObject->sum)) {
                            $purchaseObjectSum = (string)$purchaseObject->sum;
                        }

                        //-----------Вставка данных в ntf_purchase_object
                        if (
                            $purchaseObjectOKPDCode !== '' ||
                            $purchaseObjectOKPDName !== '' ||
                            $purchaseObjectOKPD2Code !== '' ||
                            $purchaseObjectOKPD2Name !== '' ||
                            $purchaseObjectName !== '' ||
                            $purchaseObjectOKEICode !== '' ||
                            $purchaseObjectOKEINationalCode !== '' ||
                            $purchaseObjectPrice !== '' ||
                            $purchaseObjectQuantityValue !== '' ||
                            $purchaseObjectQuantityUndefined !== '' ||
                            $purchaseObjectSum !== ''
                        ) {
                            $sql = "
                              INSERT INTO ntf_purchase_object (
                                ntf_lot_id,
                                okpd_code,
                                okpd_name,
                                okpd2_code,
                                okpd2_name,
                                name,
                                okei_code,
                                okei_national_code,
                                price,
                                quantity_value,
                                quantity_undefined,
                                sum
                              ) 
                              VALUES(
                                :ntfLotId,
                                :purchaseObjectOKPDCode,
                                :purchaseObjectOKPDName,
                                :purchaseObjectOKPD2Code,
                                :purchaseObjectOKPD2Name,
                                :purchaseObjectName,
                                :purchaseObjectOKEICode,
                                :purchaseObjectOKEINationalCode,
                                :purchaseObjectPrice,
                                :purchaseObjectQuantityValue,
                                :purchaseObjectQuantityUndefined,
                                :purchaseObjectSum
                          )";
                            Yii::$app->db_zakupki->createCommand($sql)
                                ->bindValues([
                                    ':ntfLotId' => $ntfLotId,
                                    ':purchaseObjectOKPDCode' => $purchaseObjectOKPDCode,
                                    ':purchaseObjectOKPDName' => $purchaseObjectOKPDName,
                                    ':purchaseObjectOKPD2Code' => $purchaseObjectOKPD2Code,
                                    ':purchaseObjectOKPD2Name' => $purchaseObjectOKPD2Name,
                                    ':purchaseObjectName' => $purchaseObjectName,
                                    ':purchaseObjectOKEICode' => $purchaseObjectOKEICode,
                                    ':purchaseObjectOKEINationalCode' => $purchaseObjectOKEINationalCode,
                                    ':purchaseObjectPrice' => $purchaseObjectPrice,
                                    ':purchaseObjectQuantityValue' => $purchaseObjectQuantityValue,
                                    ':purchaseObjectQuantityUndefined' => $purchaseObjectQuantityUndefined,
                                    ':purchaseObjectSum' => $purchaseObjectSum,
                                ])
                                ->execute();
                            $ntfPurchaseObjectId = Yii::$app->db_zakupki->getLastInsertID('ntf_purchase_object_id_seq');
                        }
                        //*-----------Вставка данных в ntf_purchase_object

                        //-----------ntf_customer_quantity
                        if (isset($purchaseObject->customerQuantities->customerQuantity)) {
                            foreach ($purchaseObject->customerQuantities->customerQuantity as $customerQuantity) {
                                $customerRegNum = '';
                                $customerConsRegistryNum = '';
                                $customerFullName = '';
                                $quantity = '';

                                if (isset($customerQuantity->customer->regNum)) {
                                    $customerRegNum = (string)$customerQuantity->customer->regNum;
                                }
                                if (isset($customerQuantity->customer->consRegistryNum)) {
                                    $customerConsRegistryNum = (string)$customerQuantity->customer->consRegistryNum;
                                }
                                if (isset($customerQuantity->customer->fullName)) {
                                    $customerFullName = (string)$customerQuantity->customer->fullName;
                                }
                                if (isset($customerQuantity->quantity)) {
                                    $quantity = (string)$customerQuantity->quantity;
                                }

                                //-----------Вставка данных в ntf_customer_quantity
                                if (
                                    $customerRegNum !== '' ||
                                    $customerConsRegistryNum !== '' ||
                                    $customerFullName !== '' ||
                                    $quantity !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_customer_quantity (
                                                  ntf_purchase_object_id,
                                                  customer_reg_num,
                                                  customer_cons_registry_num,
                                                  customer_full_name,
                                                  quantity
                                      ) 
                                      VALUES(
                                        :ntfPurchaseObjectId,
                                        :customerRegNum,
                                        :customerConsRegistryNum,
                                        :customerFullName,
                                        :quantity
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfPurchaseObjectId' => $ntfPurchaseObjectId,
                                            ':customerRegNum' => $customerRegNum,
                                            ':customerConsRegistryNum' => $customerConsRegistryNum,
                                            ':customerFullName' => $customerFullName,
                                            ':quantity' => $quantity,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_customer_quantity
                            }
                        }
                        //-----------ntf_customer_quantity
                    }
                }
                //*-----------ntf_purchase_object

                //-----------ntf_drug_purchase_object_info
                if (isset($xml->fcsNotificationEF->lot->drugPurchaseObjectsInfo->drugPurchaseObjectInfo)) {
                    foreach ($xml->fcsNotificationEF->lot->drugPurchaseObjectsInfo->drugPurchaseObjectInfo as $drugPurchaseObjectInfo) {
                        $isZNVLP = '';
                        $drugQuantityCustomersInfoTotal = '';
                        $pricePerUnit = '';
                        $positionPrice = '';

                        if (isset($drugPurchaseObjectInfo->isZNVLP)) {
                            $isZNVLP = (string)$drugPurchaseObjectInfo->isZNVLP;
                        }
                        if (isset($drugPurchaseObjectInfo->pricePerUnit)) {
                            $pricePerUnit = (string)$drugPurchaseObjectInfo->pricePerUnit;
                        }
                        if (isset($drugPurchaseObjectInfo->positionPrice)) {
                            $positionPrice = (string)$drugPurchaseObjectInfo->positionPrice;
                        }
                        if (isset($drugPurchaseObjectInfo->positionPrice->drugQuantityCustomersInfo->total)) {
                            $drugQuantityCustomersInfoTotal = (string)$drugPurchaseObjectInfo->positionPrice->drugQuantityCustomersInfo->total;
                        }
                        //-----------Вставка данных в ntf_drug_purchase_object_info
                        if (
                            $isZNVLP !== '' ||
                            $drugQuantityCustomersInfoTotal !== '' ||
                            $pricePerUnit !== '' ||
                            $positionPrice !== ''
                        ) {
                            $sql = "
                              INSERT INTO ntf_drug_purchase_object_info (
                                ntf_lot_id,
                                isznvlp,
                                drug_quantity_customers_info_total,
                                price_per_unit,
                                position_price
                              ) 
                              VALUES(
                                :ntfLotId,
                                :isZNVLP,
                                :drugQuantityCustomersInfoTotal,
                                :pricePerUnit,
                                :positionPrice
                            )";
                            Yii::$app->db_zakupki->createCommand($sql)
                                ->bindValues([
                                    ':ntfLotId' => $ntfLotId,
                                    ':isZNVLP' => $isZNVLP,
                                    ':drugQuantityCustomersInfoTotal' => $drugQuantityCustomersInfoTotal,
                                    ':pricePerUnit' => $pricePerUnit,
                                    ':positionPrice' => $positionPrice,
                                ])
                                ->execute();
                        }
                        $ntfDrugPurchaseObjectInfoId = Yii::$app->db_zakupki->getLastInsertID('ntf_drug_purchase_object_info_id_seq');
                        //*-----------Вставка данных в ntf_drug_purchase_object_info

                        //-----------ntf_drug_quantity_customer_info
                        if (isset($drugPurchaseObjectInfo->drugQuantityCustomersInfo->drugQuantityCustomerInfo)) {
                            foreach ($drugPurchaseObjectInfo->drugQuantityCustomersInfo->drugQuantityCustomerInfo as $drugQuantityCustomerInfo) {
                                $drugQuantityCustomerInfoCustomerRegNum = '';
                                $drugQuantityCustomerInfoCustomerConsRegistryNum = '';
                                $drugQuantityCustomerInfoCustomerFullName = '';
                                $drugQuantityCustomerInfoQuantity = '';

                                if (isset($drugQuantityCustomerInfo->customer->regNum)) {
                                    $drugQuantityCustomerInfoCustomerRegNum = (string)$drugQuantityCustomerInfo->customer->regNum;
                                }
                                if (isset($drugQuantityCustomerInfo->customer->consRegistryNum)) {
                                    $drugQuantityCustomerInfoCustomerConsRegistryNum = (string)$drugQuantityCustomerInfo->customer->consRegistryNum;
                                }
                                if (isset($drugQuantityCustomerInfo->customer->fullName)) {
                                    $drugQuantityCustomerInfoCustomerFullName = (string)$drugQuantityCustomerInfo->customer->fullName;
                                }
                                if (isset($drugQuantityCustomerInfo->quantity)) {
                                    $drugQuantityCustomerInfoQuantity = (string)$drugQuantityCustomerInfo->quantity;
                                }
                                //-----------Вставка данных в ntf_drug_quantity_customer_info
                                if (
                                    $drugQuantityCustomerInfoCustomerRegNum !== '' ||
                                    $drugQuantityCustomerInfoCustomerConsRegistryNum !== '' ||
                                    $drugQuantityCustomerInfoCustomerFullName !== '' ||
                                    $drugQuantityCustomerInfoQuantity !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_drug_quantity_customer_info (
                                        ntf_drug_purchase_object_info_id,
                                        customer_reg_num,
                                        customer_cons_registry_num,
                                        customer_full_name,
                                        quantity
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :drugQuantityCustomerInfoCustomerRegNum,
                                        :drugQuantityCustomerInfoCustomerConsRegistryNum,
                                        :drugQuantityCustomerInfoCustomerFullName,
                                        :drugQuantityCustomerInfoQuantity
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':drugQuantityCustomerInfoCustomerRegNum' => $drugQuantityCustomerInfoCustomerRegNum,
                                            ':drugQuantityCustomerInfoCustomerConsRegistryNum' => $drugQuantityCustomerInfoCustomerConsRegistryNum,
                                            ':drugQuantityCustomerInfoCustomerFullName' => $drugQuantityCustomerInfoCustomerFullName,
                                            ':drugQuantityCustomerInfoQuantity' => $drugQuantityCustomerInfoQuantity,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_drug_quantity_customer_info
                            }
                        }
                        //*-----------ntf_drug_quantity_customer_info

                        //-----------ntf_object_info_using_reference_info
                        if (isset($drugPurchaseObjectInfo->objectInfoUsingReferenceInfo->drugsInfo->drugInfo)) {
                            foreach ($drugPurchaseObjectInfo->objectInfoUsingReferenceInfo->drugsInfo->drugInfo as $drugInfo) {
                                $referenceInfoMNNExternalCode = '';
                                $referenceInfoMNNName = '';
                                $referenceInfoPositionTradeNameExternalCode = '';
                                $referenceInfoTradeName = '';
                                $referenceInfoMedicamentalFormName = '';
                                $referenceInfoDosageGRLSValue = '';
                                $referenceInfoDosageUserOKEICode = '';
                                $referenceInfoDosageUserOKEIName = '';
                                $referenceInfoPackaging1Quantity = '';
                                $referenceInfoPackaging2Quantity = '';
                                $referenceInfoPackagingQuantity = '';
                                $referenceInfoManualUserOKEICode = '';
                                $referenceInfoManualUserOKEIName = '';
                                $referenceInfoBasicUnit = '';
                                $referenceInfoDrugQuantity = '';
                                $referenceInfoSpecifyDrugPackageReason = '';

                                if (isset($drugInfo->MNNInfo->MNNExternalCode)) {
                                    $referenceInfoMNNExternalCode = (string)$drugInfo->MNNInfo->MNNExternalCode;
                                }
                                if (isset($drugInfo->MNNInfo->MNNName)) {
                                    $referenceInfoMNNName = (string)$drugInfo->MNNInfo->MNNName;
                                }
                                if (isset($drugInfo->tradeInfo->positionTradeNameExternalCode)) {
                                    $referenceInfoPositionTradeNameExternalCode = (string)$drugInfo->tradeInfo->positionTradeNameExternalCode;
                                }
                                if (isset($drugInfo->tradeInfo->tradeName)) {
                                    $referenceInfoTradeName = (string)$drugInfo->tradeInfo->tradeName;
                                }
                                if (isset($drugInfo->medicamentalFormInfo->medicamentalFormName)) {
                                    $referenceInfoMedicamentalFormName = (string)$drugInfo->medicamentalFormInfo->medicamentalFormName;
                                }
                                if (isset($drugInfo->dosageInfo->dosageGRLSValue)) {
                                    $referenceInfoDosageGRLSValue = (string)$drugInfo->dosageInfo->dosageGRLSValue;
                                }
                                if (isset($drugInfo->dosageInfo->dosageUserOKEI->code)) {
                                    $referenceInfoDosageUserOKEICode = (string)$drugInfo->dosageInfo->dosageUserOKEI->code;
                                }
                                if (isset($drugInfo->dosageInfo->dosageUserOKEI->name)) {
                                    $referenceInfoDosageUserOKEIName = (string)$drugInfo->dosageInfo->dosageUserOKEI->name;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->packaging1Quantity)) {
                                    $referenceInfoPackaging1Quantity = (string)$drugInfo->dosageInfo->packagingInfo->packaging1Quantity;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->packaging2Quantity)) {
                                    $referenceInfoPackaging2Quantity = (string)$drugInfo->dosageInfo->packagingInfo->packaging2Quantity;
                                }
                                if (isset($drugInfo->dosageInfo->packagingInfo->packagingQuantity)) {
                                    $referenceInfoPackagingQuantity = (string)$drugInfo->dosageInfo->packagingInfo->packagingQuantity;
                                }
                                if (isset($drugInfo->dosageInfo->manualUserOKEI->code)) {
                                    $referenceInfoManualUserOKEICode = (string)$drugInfo->dosageInfo->manualUserOKEI->code;
                                }
                                if (isset($drugInfo->dosageInfo->manualUserOKEI->name)) {
                                    $referenceInfoManualUserOKEIName = (string)$drugInfo->dosageInfo->manualUserOKEI->name;
                                }
                                if (isset($drugInfo->basicUnit)) {
                                    $referenceInfoBasicUnit = (string)$drugInfo->basicUnit;
                                }
                                if (isset($drugInfo->drugQuantity)) {
                                    $referenceInfoDrugQuantity = (string)$drugInfo->drugQuantity;
                                }
                                if (isset($drugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason)) {
                                    $referenceInfoSpecifyDrugPackageReason = (string)$drugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason;
                                }
                                //-----------Вставка данных в ntf_object_info_using_reference_info
                                if (
                                    $referenceInfoMNNExternalCode !== '' ||
                                    $referenceInfoMNNName !== '' ||
                                    $referenceInfoPositionTradeNameExternalCode !== '' ||
                                    $referenceInfoTradeName !== '' ||
                                    $referenceInfoMedicamentalFormName !== '' ||
                                    $referenceInfoDosageGRLSValue !== '' ||
                                    $referenceInfoDosageUserOKEICode !== '' ||
                                    $referenceInfoDosageUserOKEIName !== '' ||
                                    $referenceInfoPackaging1Quantity !== '' ||
                                    $referenceInfoPackaging2Quantity !== '' ||
                                    $referenceInfoPackagingQuantity !== '' ||
                                    $referenceInfoManualUserOKEICode !== '' ||
                                    $referenceInfoManualUserOKEIName !== '' ||
                                    $referenceInfoBasicUnit !== '' ||
                                    $referenceInfoDrugQuantity !== '' ||
                                    $referenceInfoSpecifyDrugPackageReason !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_object_info_using_reference_info (
                                        ntf_drug_purchase_object_info_id,
                                        drug_info_mmn_info_mmn_external_code,
                                        drug_info_mmn_info_mmn_name,
                                        drug_info_trade_info_position_trade_name_external_code,
                                        drug_info_trade_info_trade_name,
                                        drug_info_medicamental_form_info_medicamental_form_name,
                                        drug_info_dosage_info_dosage_grls_value,
                                        drug_info_dosage_info_dosage_user_okei_code,
                                        drug_info_dosage_info_dosage_user_okei_name,
                                        drug_info_packaging_info_packaging1_quantity,
                                        drug_info_packaging_info_packaging2_quantity,
                                        drug_info_packaging_info_summary_packaging_quantity,
                                        drug_info_manual_user_okei_code,
                                        drug_info_manual_user_okei_name,
                                        drug_info_basic_unit,
                                        drug_info_drug_quantity,
                                        must_specify_drug_package_specify_drug_package_reason
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :referenceInfoMNNExternalCode,
                                        :referenceInfoMNNName,
                                        :referenceInfoPositionTradeNameExternalCode,
                                        :referenceInfoTradeName,
                                        :referenceInfoMedicamentalFormName,
                                        :referenceInfoDosageGRLSValue,
                                        :referenceInfoDosageUserOKEICode,
                                        :referenceInfoDosageUserOKEIName,
                                        :referenceInfoPackaging1Quantity,
                                        :referenceInfoPackaging2Quantity,
                                        :referenceInfoPackagingQuantity,
                                        :referenceInfoManualUserOKEICode,
                                        :referenceInfoManualUserOKEIName,
                                        :referenceInfoBasicUnit,
                                        :referenceInfoDrugQuantity,
                                        :referenceInfoSpecifyDrugPackageReason
                                    )";
                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':referenceInfoMNNExternalCode' => $referenceInfoMNNExternalCode,
                                            ':referenceInfoMNNName' => $referenceInfoMNNName,
                                            ':referenceInfoPositionTradeNameExternalCode' => $referenceInfoPositionTradeNameExternalCode,
                                            ':referenceInfoTradeName' => $referenceInfoTradeName,
                                            ':referenceInfoMedicamentalFormName' => $referenceInfoMedicamentalFormName,
                                            ':referenceInfoDosageGRLSValue' => $referenceInfoDosageGRLSValue,
                                            ':referenceInfoDosageUserOKEICode' => $referenceInfoDosageUserOKEICode,
                                            ':referenceInfoDosageUserOKEIName' => $referenceInfoDosageUserOKEIName,
                                            ':referenceInfoPackaging1Quantity' => $referenceInfoPackaging1Quantity,
                                            ':referenceInfoPackaging2Quantity' => $referenceInfoPackaging2Quantity,
                                            ':referenceInfoPackagingQuantity' => $referenceInfoPackagingQuantity,
                                            ':referenceInfoManualUserOKEICode' => $referenceInfoManualUserOKEICode,
                                            ':referenceInfoManualUserOKEIName' => $referenceInfoManualUserOKEIName,
                                            ':referenceInfoBasicUnit' => $referenceInfoBasicUnit,
                                            ':referenceInfoDrugQuantity' => $referenceInfoDrugQuantity,
                                            ':referenceInfoSpecifyDrugPackageReason' => $referenceInfoSpecifyDrugPackageReason,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_object_info_using_reference_info
                            }
                        }
                        //*-----------ntf_object_info_using_reference_info

                        //-----------ntf_object_info_using_text_form
                        if (isset($drugPurchaseObjectInfo->objectInfoUsingTextForm->drugsInfo->drugInfo)) {
                            foreach ($drugPurchaseObjectInfo->objectInfoUsingTextForm->drugsInfo->drugInfo as $textFormDrugInfo) {
                                $textFormMNNName = '';
                                $textFormTradeName = '';
                                $textFormMedicamentalFormName = '';
                                $textFormDosageGRLSValue = '';
                                $textFormPackaging1Quantity = '';
                                $textFormPackaging2Quantity = '';
                                $textFormPackagingQuantity = '';
                                $textFormManualUserOKEICode = '';
                                $textFormManualUserOKEIName = '';
                                $textFormBasicUnit = '';
                                $textFormDrugQuantity = '';
                                $textFormSpecifyDrugPackageReason = '';

                                if (isset($textFormDrugInfo->MNNInfo->MNNName)) {
                                    $textFormMNNName = (string)$textFormDrugInfo->MNNInfo->MNNName;
                                }
                                if (isset($textFormDrugInfo->tradeInfo->tradeName)) {
                                    $textFormTradeName = (string)$textFormDrugInfo->tradeInfo->tradeName;
                                }
                                if (isset($textFormDrugInfo->medicamentalFormInfo->medicamentalFormName)) {
                                    $textFormMedicamentalFormName = (string)$textFormDrugInfo->medicamentalFormInfo->medicamentalFormName;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->dosageGRLSValue)) {
                                    $textFormDosageGRLSValue = (string)$textFormDrugInfo->dosageInfo->dosageGRLSValue;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->packaging1Quantity)) {
                                    $textFormPackaging1Quantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->packaging1Quantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->packaging2Quantity)) {
                                    $textFormPackaging2Quantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->packaging2Quantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->packagingInfo->packagingQuantity)) {
                                    $textFormPackagingQuantity = (string)$textFormDrugInfo->dosageInfo->packagingInfo->packagingQuantity;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->manualUserOKEI->code)) {
                                    $textFormManualUserOKEICode = (string)$textFormDrugInfo->dosageInfo->manualUserOKEI->code;
                                }
                                if (isset($textFormDrugInfo->dosageInfo->manualUserOKEI->name)) {
                                    $textFormManualUserOKEIName = (string)$textFormDrugInfo->dosageInfo->manualUserOKEI->name;
                                }
                                if (isset($textFormDrugInfo->basicUnit)) {
                                    $textFormBasicUnit = (string)$textFormDrugInfo->basicUnit;
                                }
                                if (isset($textFormDrugInfo->drugQuantity)) {
                                    $textFormDrugQuantity = (string)$textFormDrugInfo->drugQuantity;
                                }
                                if (isset($textFormDrugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason)) {
                                    $textFormSpecifyDrugPackageReason = (string)$textFormDrugInfo->mustSpecifyDrugPackage->specifyDrugPackageReason;
                                }
                                //-----------Вставка данных в ntf_object_info_using_text_form
                                if (
                                    $textFormMNNName !== '' ||
                                    $textFormTradeName !== '' ||
                                    $textFormMedicamentalFormName !== '' ||
                                    $textFormDosageGRLSValue !== '' ||
                                    $textFormPackaging1Quantity !== '' ||
                                    $textFormPackaging2Quantity !== '' ||
                                    $textFormPackagingQuantity !== '' ||
                                    $textFormManualUserOKEICode !== '' ||
                                    $textFormManualUserOKEIName !== '' ||
                                    $textFormBasicUnit !== '' ||
                                    $textFormDrugQuantity !== '' ||
                                    $textFormSpecifyDrugPackageReason !== ''
                                ) {
                                    $sql = "
                                      INSERT INTO ntf_object_info_using_text_form (
                                        ntf_drug_purchase_object_info_id,
                                        drug_info_mmn_info_mmn_name,
                                        drug_info_trade_info_trade_name,
                                        drug_info_medicamental_form_info_medicamental_form_name,
                                        drug_info_dosage_info_dosage_grls_value,
                                        drug_info_packaging_info_packaging1_quantity,
                                        drug_info_packaging_info_packaging2_quantity,
                                        drug_info_packaging_info_summary_packaging_quantity,
                                        drug_info_manual_user_okei_code,
                                        drug_info_manual_user_okei_name,
                                        drug_info_basic_unit,
                                        drug_info_drug_quantity,
                                        must_specify_drug_package_specify_drug_package_reason
                                      ) 
                                      VALUES(
                                        :ntfDrugPurchaseObjectInfoId,
                                        :textFormMNNName,
                                        :textFormTradeName,
                                        :textFormMedicamentalFormName,
                                        :textFormDosageGRLSValue,
                                        :textFormPackaging1Quantity,
                                        :textFormPackaging2Quantity,
                                        :textFormPackagingQuantity,
                                        :textFormManualUserOKEICode,
                                        :textFormManualUserOKEIName,
                                        :textFormBasicUnit,
                                        :textFormDrugQuantity,
                                        :textFormSpecifyDrugPackageReason
                                    )";

                                    Yii::$app->db_zakupki->createCommand($sql)
                                        ->bindValues([
                                            ':ntfDrugPurchaseObjectInfoId' => $ntfDrugPurchaseObjectInfoId,
                                            ':textFormMNNName' => $textFormMNNName,
                                            ':textFormTradeName' => $textFormTradeName,
                                            ':textFormMedicamentalFormName' => $textFormMedicamentalFormName,
                                            ':textFormDosageGRLSValue' => $textFormDosageGRLSValue,
                                            ':textFormPackaging1Quantity' => $textFormPackaging1Quantity,
                                            ':textFormPackaging2Quantity' => $textFormPackaging2Quantity,
                                            ':textFormPackagingQuantity' => $textFormPackagingQuantity,
                                            ':textFormManualUserOKEICode' => $textFormManualUserOKEICode,
                                            ':textFormManualUserOKEIName' => $textFormManualUserOKEIName,
                                            ':textFormBasicUnit' => $textFormBasicUnit,
                                            ':textFormDrugQuantity' => $textFormDrugQuantity,
                                            ':textFormSpecifyDrugPackageReason' => $textFormSpecifyDrugPackageReason,
                                        ])
                                        ->execute();
                                }
                                //*-----------Вставка данных в ntf_object_info_using_text_form
                            }
                        }
                        //*-----------ntf_object_info_using_text_form
                    }
                }
                //*-----------ntf_drug_purchase_object_info
                //-----------Вставка данных в ntf_purchase_responsible_responsible_org
                if (
                    $responsibleOrgRegNum !== '' ||
                    $responsibleOrgConsRegistryNum !== '' ||
                    $responsibleOrgFullName !== '' ||
                    $responsibleOrgShortName !== '' ||
                    $responsibleOrgPostAddress !== '' ||
                    $responsibleOrgFactAddress !== '' ||
                    $responsibleOrgInn !== '' ||
                    $responsibleOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_responsible_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :responsibleOrgRegNum,
                        :responsibleOrgConsRegistryNum,
                        :responsibleOrgFullName,
                        :responsibleOrgShortName,
                        :responsibleOrgPostAddress,
                        :responsibleOrgFactAddress,
                        :responsibleOrgInn,
                        :responsibleOrgKpp
                    )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':responsibleOrgRegNum' => $responsibleOrgRegNum,
                            ':responsibleOrgConsRegistryNum' => $responsibleOrgConsRegistryNum,
                            ':responsibleOrgFullName' => $responsibleOrgFullName,
                            ':responsibleOrgShortName' => $responsibleOrgShortName,
                            ':responsibleOrgPostAddress' => $responsibleOrgPostAddress,
                            ':responsibleOrgFactAddress' => $responsibleOrgFactAddress,
                            ':responsibleOrgInn' => $responsibleOrgInn,
                            ':responsibleOrgKpp' => $responsibleOrgKpp,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_responsible_org

                //-----------Вставка данных в ntf_purchase_responsible_responsible_info
                if (
                    $responsibleInfoOrgPostAddress !== '' ||
                    $responsibleInfoOrgFactAddress !== '' ||
                    $contactPersonLastName !== '' ||
                    $contactPersonFirstName !== '' ||
                    $contactPersonMiddleName !== '' ||
                    $responsibleInfoContactEMail !== '' ||
                    $responsibleInfoContactPhone !== '' ||
                    $responsibleInfoContactFax !== '' ||
                    $responsibleInfoAddInfo !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_responsible_info (
                        ntf_main_id,
                        org_post_address,
                        org_fact_address,
                        contact_person_last_name,
                        contact_person_first_name,
                        contact_person_middle_name,
                        contact_email,
                        contact_phone,
                        contact_fax,
                        add_info
                      ) 
                      VALUES(
                        :ntfMainId,
                        :responsibleInfoOrgPostAddress,
                        :responsibleInfoOrgFactAddress,
                        :contactPersonLastName,
                        :contactPersonFirstName,
                        :contactPersonMiddleName,
                        :responsibleInfoContactEMail,
                        :responsibleInfoContactPhone,
                        :responsibleInfoContactFax,
                        :responsibleInfoAddInfo
                    )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':responsibleInfoOrgPostAddress' => $responsibleInfoOrgPostAddress,
                            ':responsibleInfoOrgFactAddress' => $responsibleInfoOrgFactAddress,
                            ':contactPersonLastName' => $contactPersonLastName,
                            ':contactPersonFirstName' => $contactPersonFirstName,
                            ':contactPersonMiddleName' => $contactPersonMiddleName,
                            ':responsibleInfoContactEMail' => $responsibleInfoContactEMail,
                            ':responsibleInfoContactPhone' => $responsibleInfoContactPhone,
                            ':responsibleInfoContactFax' => $responsibleInfoContactFax,
                            ':responsibleInfoAddInfo' => $responsibleInfoAddInfo,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_responsible_info

                //-----------Вставка данных в ntf_purchase_responsible_specialized_org
                if (
                    $specializedOrgRegNum !== '' ||
                    $specializedOrgConsRegistryNum !== '' ||
                    $specializedOrgFullName !== '' ||
                    $specializedOrgShortName !== '' ||
                    $specializedOrgPostAddress !== '' ||
                    $specializedOrgFactAddress !== '' ||
                    $specializedOrgInn !== '' ||
                    $specializedOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_specialized_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :specializedOrgRegNum,
                        :specializedOrgConsRegistryNum,
                        :specializedOrgFullName,
                        :specializedOrgShortName,
                        :specializedOrgPostAddress,
                        :specializedOrgFactAddress,
                        :specializedOrgInn,
                        :specializedOrgKpp
                        )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':specializedOrgRegNum' => $specializedOrgRegNum,
                            ':specializedOrgConsRegistryNum' => $specializedOrgConsRegistryNum,
                            ':specializedOrgFullName' => $specializedOrgFullName,
                            ':specializedOrgShortName' => $specializedOrgShortName,
                            ':specializedOrgPostAddress' => $specializedOrgPostAddress,
                            ':specializedOrgFactAddress' => $specializedOrgFactAddress,
                            ':specializedOrgInn' => $specializedOrgInn,
                            ':specializedOrgKpp' => $specializedOrgKpp,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_specialized_org

                //-----------Вставка данных в ntf_purchase_responsible_last_specialized_org
                if (
                    $lastSpecializedOrgRegNum !== '' ||
                    $lastSpecializedOrgConsRegistryNum !== '' ||
                    $lastSpecializedOrgFullName !== '' ||
                    $lastSpecializedOrgShortName !== '' ||
                    $lastSpecializedOrgPostAddress !== '' ||
                    $lastSpecializedOrgFactAddress !== '' ||
                    $lastSpecializedOrgInn !== '' ||
                    $lastSpecializedOrgKpp !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_purchase_responsible_specialized_org (
                        ntf_main_id,
                        reg_num,
                        cons_registry_num,
                        full_name,
                        short_name,
                        post_address,
                        fact_address,
                        inn,
                        kpp
                      ) 
                      VALUES(
                        :ntfMainId,
                        :lastSpecializedOrgRegNum,
                        :lastSpecializedOrgConsRegistryNum,
                        :lastSpecializedOrgFullName,
                        :lastSpecializedOrgShortName,
                        :lastSpecializedOrgPostAddress,
                        :lastSpecializedOrgFactAddress,
                        :lastSpecializedOrgInn,
                        :lastSpecializedOrgKpp
                        )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':lastSpecializedOrgRegNum' => $lastSpecializedOrgRegNum,
                            ':lastSpecializedOrgConsRegistryNum' => $lastSpecializedOrgConsRegistryNum,
                            ':lastSpecializedOrgFullName' => $lastSpecializedOrgFullName,
                            ':lastSpecializedOrgShortName' => $lastSpecializedOrgShortName,
                            ':lastSpecializedOrgPostAddress' => $lastSpecializedOrgPostAddress,
                            ':lastSpecializedOrgFactAddress' => $lastSpecializedOrgFactAddress,
                            ':lastSpecializedOrgInn' => $lastSpecializedOrgInn,
                            ':lastSpecializedOrgKpp' => $lastSpecializedOrgKpp,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_last_specialized_org

                //-----------Вставка данных в ntf_purchase_responsible_last_specialized_org
                if (
                    $etpCode !== '' ||
                    $etpName !== '' ||
                    $etpUrl !== ''
                ) {
                    $sql = "
                      INSERT INTO ntf_etp (
                        ntf_main_id,
                        code,
                        name,
                        url
                      ) 
                      VALUES(
                        :ntfMainId,
                        :etpCode,
                        :etpName,
                        :etpUrl
                        )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':etpCode' => $etpCode,
                            ':etpName' => $etpName,
                            ':etpUrl' => $etpUrl,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_purchase_responsible_last_specialized_org

                //-----------Вставка данных в ntf_public_discussion
                if (
                    $publicDiscussionNumber !== '' ||
                    $publicDiscussionOrganizationCh5St15 !== '' ||
                    $publicDiscussionHref !== '' ||
                    $publicDiscussionPlace !== '' ||
                    $publicDiscussion2017ProtocolDate !== '' ||
                    $publicDiscussion2017ProtocolPublishDate !== '' ||
                    $publicDiscussion2017PublicDiscussionPhase2Num !== '' ||
                    $publicDiscussion2017HrefPhase2 !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_public_discussion (
                            ntf_lot_id,
                            number,
                            organization_ch5_st15,
                            href,
                            place,
                            public_discussion2017_p_d_l_p_p2_protocol_date,
                            public_discussion2017_p_d_l_p_p2_protocol_publish_date,
                            public_discussion2017_p_d_l_p_p2_public_discussion_phase2_num,
                            public_discussion2017_p_d_l_p_p2_href_phase2
                          ) 
                          VALUES(
                            :ntfLotId,
                            :publicDiscussionNumber,
                            :publicDiscussionOrganizationCh5St15,
                            :publicDiscussionHref,
                            :publicDiscussionPlace,
                            :publicDiscussion2017ProtocolDate,
                            :publicDiscussion2017ProtocolPublishDate,
                            :publicDiscussion2017PublicDiscussionPhase2Num,
                            :publicDiscussion2017HrefPhase2
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfLotId' => $ntfLotId,
                            ':publicDiscussionNumber' => $publicDiscussionNumber,
                            ':publicDiscussionOrganizationCh5St15' => $publicDiscussionOrganizationCh5St15,
                            ':publicDiscussionHref' => $publicDiscussionHref,
                            ':publicDiscussionPlace' => $publicDiscussionPlace,
                            ':publicDiscussion2017ProtocolDate' => $publicDiscussion2017ProtocolDate,
                            ':publicDiscussion2017ProtocolPublishDate' => $publicDiscussion2017ProtocolPublishDate,
                            ':publicDiscussion2017PublicDiscussionPhase2Num' => $publicDiscussion2017PublicDiscussionPhase2Num,
                            ':publicDiscussion2017HrefPhase2' => $publicDiscussion2017HrefPhase2,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_public_discussion

                //-----------Вставка данных в ntf_placing_way
                if (
                    $placingWayCode !== '' ||
                    $placingWayName !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_placing_way (
                            ntf_main_id,
                            code,
                            name
                          ) 
                          VALUES(
                            :ntfMainId,
                            :placingWayCode,
                            :placingWayName
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':placingWayCode' => $placingWayCode,
                            ':placingWayName' => $placingWayName,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_placing_way

                //-----------Вставка данных в ntf_procedure_info_collecting
                if (
                    $procedureInfoCollectingStartDate !== '' ||
                    $procedureInfoCollectingPlace !== '' ||
                    $procedureInfoCollectingOrder !== '' ||
                    $procedureInfoCollectingEndDate !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_procedure_info_collecting (
                            ntf_main_id,
                            start_date,
                            place,
                            ntf_order,
                            end_date
                          ) 
                          VALUES(
                            :ntfMainId,
                            :procedureInfoCollectingStartDate,
                            :procedureInfoCollectingPlace,
                            :procedureInfoCollectingOrder,
                            :procedureInfoCollectingEndDate
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':procedureInfoCollectingStartDate' => $procedureInfoCollectingStartDate,
                            ':procedureInfoCollectingPlace' => $procedureInfoCollectingPlace,
                            ':procedureInfoCollectingOrder' => $procedureInfoCollectingOrder,
                            ':procedureInfoCollectingEndDate' => $procedureInfoCollectingEndDate,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_procedure_info_collecting

                //-----------Вставка данных в ntf_procedure_info_scoring
                if (
                    $procedureInfoScoringDate !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_procedure_info_scoring (
                            ntf_main_id,
                            date
                          ) 
                          VALUES(
                            :ntfMainId,
                            :procedureInfoScoringDate
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':procedureInfoScoringDate' => $procedureInfoScoringDate,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_procedure_info_scoring

                //-----------Вставка данных в ntf_procedure_info_bidding
                if (
                    $procedureInfoBiddingDate !== '' ||
                    $procedureInfoBiddingAddInfo !== ''
                ) {
                    $sql = "
                          INSERT INTO ntf_procedure_info_bidding (
                            ntf_main_id,
                            date,
                            add_info
                          ) 
                          VALUES(
                            :ntfMainId,
                            :procedureInfoBiddingDate,
                            :procedureInfoBiddingAddInfo
                          )";
                    Yii::$app->db_zakupki->createCommand($sql)
                        ->bindValues([
                            ':ntfMainId' => $ntfMainId,
                            ':procedureInfoBiddingDate' => $procedureInfoBiddingDate,
                            ':procedureInfoBiddingAddInfo' => $procedureInfoBiddingAddInfo,
                        ])
                        ->execute();
                }
                //*-----------Вставка данных в ntf_procedure_info_bidding
            }

        } else {
            echo "Error in file: " . $file . PHP_EOL;
            die;
        }
    }

    //DONE
    //TODO: заблокировать эту функцию на продакшн
    //Функция очистки данных из БД (сбрасывает счетчик на 0)
    public static function clearDb()
    {
        Yii::$app->db_zakupki->createCommand("TRUNCATE ntf_main RESTART IDENTITY CASCADE")->execute();
        //Yii::$app->db_zakupki->createCommand()->delete('ntf_main')->execute();
        echo "TRUNCATE COMPLETE.Deleted all records." . PHP_EOL;
    }

    //DONE
    //приведение xml файла к валидной форме
    public static function normalizeXml($contentString)
    {
        $processedString = str_replace('oos:', "", $contentString);
        $processedString = str_replace('ns2:', "", $processedString);
        $processedString = str_replace('ns3:', "", $processedString);
        $processedString = str_replace('ns4:', "", $processedString);
        $xml = simplexml_load_string($processedString, 'SimpleXMLElement',
            LIBXML_NOCDATA | LIBXML_NOBLANKS);
        return $xml;
    }

    //DONE
    public static function totalXmlCount()
    {
        $extractedPath = self::LOCAL_NOTIFICATION_PATH . self::LOCAL_EXTRACTED_PATH;
        $totalFiles = 0;
        if (is_dir($extractedPath . '/buffer')) {
            $bufferCounter = (count(scandir($extractedPath . '/buffer')) - 2);
            $totalFiles += $bufferCounter;
            echo "Buffer files: " . $bufferCounter;
        }
        if (is_dir($extractedPath . '/fcsPlacementResult')) {
            $fcsPlacementResultCounter = (count(scandir($extractedPath . '/fcsPlacementResult')) - 2);
            $totalFiles += $fcsPlacementResultCounter;
            echo "\nfcsPlacementResult files : " . $fcsPlacementResultCounter;
        }
        if (is_dir($extractedPath . '/fcsClarification')) {
            $fcsClarificationCounter = (count(scandir($extractedPath . '/fcsClarification')) - 2);
            $totalFiles += $fcsClarificationCounter;
            echo "\nfcsClarification files : " . $fcsClarificationCounter;
        }
        if (is_dir($extractedPath . '/fcsContractSign')) {
            $fcsContractSignCounter = (count(scandir($extractedPath . '/fcsContractSign')) - 2);
            $totalFiles += $fcsContractSignCounter;
            echo "\nfcsContractSign files : " . $fcsContractSignCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationEFDateChange')) {
            $fcsNotificationEFDateChangeCounter = (count(scandir($extractedPath . '/fcsNotificationEFDateChange')) - 2);
            $totalFiles += $fcsNotificationEFDateChangeCounter;
            echo "\nfcs_notificationEFDateChange files : " . $fcsNotificationEFDateChangeCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationCancel')) {
            $fcsNotificationCancelCounter = (count(scandir($extractedPath . '/fcsNotificationCancel')) - 2);
            $totalFiles += $fcsNotificationCancelCounter;
            echo "\nfcsNotificationCancel files : " . $fcsNotificationCancelCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationCancelFailure')) {
            $fcsNotificationCancelFailureCounter = (count(scandir($extractedPath . '/fcsNotificationCancelFailure')) - 2);
            $totalFiles += $fcsNotificationCancelFailureCounter;
            echo "\nfcsNotificationCancelFailure files : " . $fcsNotificationCancelFailureCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationLotCancel')) {
            $fcsNotificationLotCancelCounter = (count(scandir($extractedPath . '/fcsNotificationLotCancel')) - 2);
            $totalFiles += $fcsNotificationLotCancelCounter;
            echo "\nfcsNotificationLotCancel files : " . $fcsNotificationLotCancelCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationOrgChange')) {
            $fcsNotificationOrgChangeCounter = (count(scandir($extractedPath . '/fcsNotificationOrgChange')) - 2);
            $totalFiles += $fcsNotificationOrgChangeCounter;
            echo "\nfcsNotificationOrgChange files : " . $fcsNotificationOrgChangeCounter;
        }
        if (is_dir($extractedPath . '/fcsPurchaseProlongationOK')) {
            $fcsPurchaseProlongationOKCounter = (count(scandir($extractedPath . '/fcsPurchaseProlongationOK')) - 2);
            $totalFiles += $fcsPurchaseProlongationOKCounter;
            echo "\nfcsPurchaseProlongationOK files : " . $fcsPurchaseProlongationOKCounter;
        }
        if (is_dir($extractedPath . '/fcsPurchaseProlongationZK')) {
            $fcsPurchaseProlongationZKCounter = (count(scandir($extractedPath . '/fcsPurchaseProlongationZK')) - 2);
            $totalFiles += $fcsPurchaseProlongationZKCounter;
            echo "\nfcsPurchaseProlongationZK files : " . $fcsPurchaseProlongationZKCounter;
        }
        echo PHP_EOL;
        if (is_dir($extractedPath . '/fcsNotificationEA44')) {
            $fcsNotificationEA44Counter = (count(scandir($extractedPath . '/fcsNotificationEA44')) - 2);
            $totalFiles += $fcsNotificationEA44Counter;
            echo "\nfcsNotificationEF44 files : " . $fcsNotificationEA44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationEFDateChange')) {
            $fcsNotificationEFDateChangeCounter = (count(scandir($extractedPath . '/fcsNotificationEFDateChange')) - 2);
            $totalFiles += $fcsNotificationEFDateChangeCounter;
            echo "\nfcsNotificationEFDateChange files : " . $fcsNotificationEFDateChangeCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationEP44')) {
            $fcsNotificationEP44Counter = (count(scandir($extractedPath . '/fcsNotificationEP44')) - 2);
            $totalFiles += $fcsNotificationEP44Counter;
            echo "\nfcsNotificationEP44 files : " . $fcsNotificationEP44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationINM')) {
            $fcsNotificationINMCounter = (count(scandir($extractedPath . '/fcsNotificationINM')) - 2);
            $totalFiles += $fcsNotificationINMCounter;
            echo "\nfcsNotificationINM files : " . $fcsNotificationINMCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationOK44')) {
            $fcsNotificationOK44Counter = (count(scandir($extractedPath . '/fcsNotificationOK44')) - 2);
            $totalFiles += $fcsNotificationOK44Counter;
            echo "\nfcsNotificationOK44 files : " . $fcsNotificationOK44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationOKD44')) {
            $fcsNotificationOKD44Counter = (count(scandir($extractedPath . '/fcsNotificationOKD44')) - 2);
            $totalFiles += $fcsNotificationOKD44Counter;
            echo "\nfcsNotificationOKD44 files : " . $fcsNotificationOKD44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationOKU44')) {
            $fcsNotificationOKU44Counter = (count(scandir($extractedPath . '/fcsNotificationOKU44')) - 2);
            $totalFiles += $fcsNotificationOKU44Counter;
            echo "\nfcsNotificationOKU44 files : " . $fcsNotificationOKU44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationPO44')) {
            $fcsNotificationPO44Counter = (count(scandir($extractedPath . '/fcsNotificationPO44')) - 2);
            $totalFiles += $fcsNotificationPO44Counter;
            echo "\nfcsNotificationPO44 files : " . $fcsNotificationPO44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationZA44')) {
            $fcsNotificationZA44Counter = (count(scandir($extractedPath . '/fcsNotificationZA44')) - 2);
            $totalFiles += $fcsNotificationZA44Counter;
            echo "\nfcsNotificationZA44 files : " . $fcsNotificationZA44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationZK44')) {
            $fcsNotificationZK44Counter = (count(scandir($extractedPath . '/fcsNotificationZK44')) - 2);
            $totalFiles += $fcsNotificationZK44Counter;
            echo "\nfcsNotificationZK44 files : " . $fcsNotificationZK44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationZKK')) {
            $fcsNotificationZKKCounter = (count(scandir($extractedPath . '/fcsNotificationZKK')) - 2);
            $totalFiles += $fcsNotificationZKKCounter;
            echo "\nfcsNotificationZKK files : " . $fcsNotificationZKKCounter;
        }
        if (is_dir($extractedPath . '/fcsNotificationZP44')) {
            $fcsNotificationZP44Counter = (count(scandir($extractedPath . '/fcsNotificationZP44')) - 2);
            $totalFiles += $fcsNotificationZP44Counter;
            echo "\nfcsNotificationZP44 files : " . $fcsNotificationZP44Counter;
        }
        if (is_dir($extractedPath . '/fcsNotificationZA44')) {
            $fcsNotificationZA44Counter = (count(scandir($extractedPath . '/fcsNotificationZA44')) - 2);
            $totalFiles += $fcsNotificationZA44Counter;
            echo "\nfcsNotificationZA44 files : " . $fcsNotificationZA44Counter;

        }
        echo PHP_EOL;
        echo "\nTotal files : " . $totalFiles;
    }

    public static function decomposeFile($dirBuffer, $extractedPath, $file, $name)
    {
        $dir = $extractedPath . $name . '/';
        //print_r(PHP_EOL . $dirBuffer);
        //print_r(PHP_EOL . $dir);
        //die;
        if (!is_dir($dir)) {
            mkdir($dir);
        }

        copy($dirBuffer . $file, $dir . $file);
        unlink($dirBuffer . $file);
    }

    /**
     * @return array
     */
    public static function getRegions()
    {
        return self::$regions;
    }

    public static function downloadFromFtpForAllRegions($pathForSavingZipFiles)
    {
        $user = Notification::FTP_USER;
        $password = Notification::FTP_PASSWORD;
        $host = Notification::FTP_HOST;

        $connect = ftp_connect($host);
        $result = ftp_login($connect, $user, $password);

        if (!is_dir($pathForSavingZipFiles)) {
            mkdir($pathForSavingZipFiles);
        }

        if (!$result) {
            exit("Can not connect");
        }

        foreach (Notification::getRegions() as $region) {
            //Текущий регион
            $contents = ftp_nlist($connect, "/fcs_regions/" . $region . "/notifications/currMonth");
            //$contents содержит все необходимые zip для загрузки с определенного региона
            Notification::downloadFromFtpByRegion($region, $connect, $contents, $pathForSavingZipFiles);
        }
        ftp_close($connect);
    }

    //Хороший метод
    public static function downloadFromFtpByRegion($region, $connect, $contents, $pathForSavingZipFiles)
    {
        $zipSkippedCounter = 0;
        $zipNewCounter = 0;

        print_r("\nStart parsing as zip: " . $region . " (current month)");

        foreach ($contents as $content) {
            $local_file = $pathForSavingZipFiles . basename($content);
            if (file_exists($local_file)) {
                $zipSkippedCounter++;
                continue;
            }
            $handle = fopen($local_file, 'w');

            if (ftp_fget($connect, $handle, $content, FTP_BINARY, 0)) {
            } else {
                echo "При скачке $content в $local_file произошла проблема\n";
            }
            $zipNewCounter++;
            if ($zipNewCounter % 10 == 0) {
                echo "\n    Downloaded " . $zipNewCounter . " new files";
            }
            fclose($handle);
        }

        print_r("\n     Skipped file(s): " . $zipSkippedCounter .
            "\n     New file(s): " . $zipNewCounter .
            "\nParsing completed: " . $region . "\n");
    }

    public static function unzip($pathToZip, $extractedPath, $buffer)
    {
        if (!is_dir($extractedPath)) {
            mkdir($extractedPath);
        }

        if (self::ZIP_EXTRACTED_COUNT === -1) {
            $counter = PHP_INT_MAX;
        } else {
            $counter = self::ZIP_EXTRACTED_COUNT;
        }

        $dirBuffer = $extractedPath . $buffer;

        //$startPositionZip = 1000;
        $startPositionZip = 0;
        $zipCounter = 0 + $startPositionZip;
        //пропуск нескольких zip

        //4 this is . & .. & extracted & extracted_norm
        $totalZipCounter = count(scandir($pathToZip)) - 4;
        if ($handle = opendir($pathToZip)) {
            while (false !== ($content = readdir($handle)) && $counter > 0) {
                $counter--;
                if ($content != "." && $content != ".." && $content != "extracted" && $content != "zip") {
                    $local_file = $pathToZip . basename($content);

                    if ($startPositionZip > 0) {
                        echo "\nПропущено: " . $startPositionZip;
                        $startPositionZip--;
                        continue;
                    }

                    $zip = new \ZipArchive;
                    if ($zip->open($local_file) === true) {
                        $zip->extractTo($dirBuffer);
                        $zip->close();
                    } else {
                        echo "\n" . $local_file . "- archive not found\n";
                    }
                    $zipCounter++;

                    echo "\nUnpacked: " . $local_file . "\n";
                    echo "To a folder: " . $dirBuffer . "\n";
                    echo "Processed archives: " . $zipCounter . " of " . $totalZipCounter . "\n";

                    $totalCounter = 0;
                    $xmlFormatCount = 0;
                    $otherFormatCount = 0;
                    $fcsPlacementResult = 0;
                    $fcsClarification = 0;
                    $fcsContractSign = 0;
                    $fcsNotificationEFDateChange = 0;
                    $fcsNotificationCancel = 0;
                    $fcsNotificationCancelFailure = 0;
                    $fcsNotificationLotCancel = 0;
                    $fcsNotificationOrgChange = 0;
                    $fcsPurchaseProlongationOK = 0;
                    $fcsPurchaseProlongationZK = 0;

                    $fcsNotificationEA44 = 0;
                    $fcsNotificationEP44 = 0;
                    $fcsNotificationINM = 0;
                    $fcsNotificationOK44 = 0;
                    $fcsNotificationOKU44 = 0;
                    $fcsNotificationPO44 = 0;
                    $fcsNotificationZK44 = 0;
                    $fcsNotificationZP44 = 0;
                    $fcsNotificationZA44 = 0;
                    $fcsNotificationZKK = 0;
                    $fcsNotificationOKD44 = 0;

                    if (!is_dir($dirBuffer)) {
                        mkdir($dirBuffer);
                    }

                    if ($innerHandle = opendir($dirBuffer)) {
                        while (false !== ($file = readdir($innerHandle))) {
                            if ($file != "." && $file != "..") {
                                $totalCounter++;
                                if (pathinfo($dirBuffer . $file,
                                        PATHINFO_EXTENSION) == 'xml') {
                                    $xmlFormatCount++;
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsPlacementResult") !== false) {
                                        $fcsPlacementResult++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "fcsPlacementResult");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsClarification") !== false) {
                                        $fcsClarification++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "fcsClarification");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsContractSign") !== false) {
                                        $fcsContractSign++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "fcsContractSign");
                                        continue;
                                    }
                                    //Определение форматов аукционов в папке
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationEA44") !== false) {
                                        $fcsNotificationEA44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationEA44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationEP44") !== false) {
                                        $fcsNotificationEP44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationEP44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationINM") !== false) {
                                        $fcsNotificationINM++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "fcsNotificationINM");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationOK44") !== false) {
                                        $fcsNotificationOK44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationOK44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationOKU44") !== false) {
                                        $fcsNotificationOKU44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationOKU44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationPO44") !== false) {
                                        $fcsNotificationPO44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationPO44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationZK44") !== false) {
                                        $fcsNotificationZK44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationZK44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationZA44") !== false) {
                                        $fcsNotificationZA44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationZA44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationZKK") !== false) {
                                        $fcsNotificationZKK++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file, "fcsNotificationZKK");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationZP44") !== false) {
                                        $fcsNotificationZP44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationZP44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationOKD44") !== false) {
                                        $fcsNotificationOKD44++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationOKD44");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcs_notificationEFDateChange") !== false) {
                                        $fcsNotificationEFDateChange++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcs_notificationEFDateChange");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationCancelFailure") !== false) {
                                        $fcsNotificationCancelFailure++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationCancelFailure");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationLotCancel") !== false) {
                                        $fcsNotificationLotCancel++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationLotCancel");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationOrgChange") !== false) {
                                        $fcsNotificationOrgChange++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationOrgChange");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsNotificationCancel") !== false) {
                                        $fcsNotificationCancel++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsNotificationCancel");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsPurchaseProlongationOK") !== false) {
                                        $fcsPurchaseProlongationOK++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsPurchaseProlongationOK");
                                        continue;
                                    }
                                    if (stripos(pathinfo($dirBuffer . $file,
                                            PATHINFO_FILENAME),
                                            "fcsPurchaseProlongationZK") !== false) {
                                        $fcsPurchaseProlongationZK++;
                                        self::decomposeFile($dirBuffer, $extractedPath, $file,
                                            "fcsPurchaseProlongationZK");
                                        continue;
                                    }
                                } else {
                                    $otherFormatCount++;
                                    unlink($dirBuffer . $file);
                                }
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }
    }

    public static function clearFormatsFromFolder($extractedPath)
    {
        $totalCounter = 0;
        $xmlFormatCount = 0;
        $otherFormatCount = 0;
        $fcsPlacementResult = 0;
        $fcsClarification = 0;
        $fcsContractSign = 0;

        $fcsNotificationEA44 = 0;
        $fcsNotificationEP44 = 0;
        $fcsNotificationINM = 0;
        $fcsNotificationOK44 = 0;
        $fcsNotificationOKU44 = 0;
        $fcsNotificationPO44 = 0;
        $fcsNotificationZK44 = 0;
        $fcsNotificationZP44 = 0;

        $fcsNotificationEFDateChange = 0;
        $fcsNotificationCancel = 0;
        $fcsNotificationCancelFailure = 0;
        $fcsPurchaseProlongationOK = 0;
        $fcsPurchaseProlongationZK = 0;

        if ($handle = opendir($extractedPath)) {
            while (false !== ($file = readdir($handle))) {
                //оставляем только файлы формата .xml
                if ($file != "." && $file != "..") {
                    $totalCounter++;
                    if (pathinfo($extractedPath . '/' . $file, PATHINFO_EXTENSION) == 'xml') {
                        $xmlFormatCount++;
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsPlacementResult") !== false) {
                            unlink($extractedPath . '/' . $file);
                            $fcsPlacementResult++;
                            unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsClarification") !== false) {
                            unlink($extractedPath . '/' . $file);
                            $fcsClarification++;
                            unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsContractSign") !== false) {
                            unlink($extractedPath . '/' . $file);
                            $fcsContractSign++;
                            unlink($extractedPath . '/' . $file);
                        }

                        //Определение форматов аукционов в папке
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationEA44") !== false) {
                            $fcsNotificationEA44++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationEP44") !== false) {
                            $fcsNotificationEP44++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationINM") !== false) {
                            $fcsNotificationINM++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationOK44") !== false) {
                            $fcsNotificationOK44++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationOKU44") !== false) {
                            $fcsNotificationOKU44++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationPO44") !== false) {
                            $fcsNotificationPO44++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationZK44") !== false) {
                            $fcsNotificationZK44++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationZP44") !== false) {
                            $fcsNotificationZP44++;
                            //unlink($extractedPath . '/' . $file);
                        }

                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcs_notificationEFDateChange") !== false) {
                            $fcsNotificationEFDateChange++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationCancel") !== false) {
                            $fcsNotificationCancel++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsNotificationCancelFailure") !== false) {
                            $fcsNotificationCancelFailure++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsPurchaseProlongationOK") !== false) {
                            $fcsPurchaseProlongationOK++;
                            //unlink($extractedPath . '/' . $file);
                        }
                        if (stripos(pathinfo($extractedPath . '/' . $file, PATHINFO_FILENAME),
                                "fcsPurchaseProlongationZK") !== false) {
                            $fcsPurchaseProlongationZK++;
                            //unlink($extractedPath . '/' . $file);
                        }
                    } else {
                        $otherFormatCount++;
                        unlink($extractedPath . '/' . $file);
                    }
                }
            }
        }

        echo "\nTotal files : " . $totalCounter;
        echo PHP_EOL;
        echo "\nUnnecessary files";
        echo "\nfcsPlacementResult files : " . $fcsPlacementResult;
        echo "\nfcsClarification files : " . $fcsClarification;
        echo "\nfcsContractSign files : " . $fcsContractSign;
        echo "\nfcs_notificationEFDateChange files : " . $fcsNotificationEFDateChange;
        echo "\nfcsNotificationCancel files : " . $fcsNotificationCancel;
        echo "\nfcsNotificationCancelFailure files : " . $fcsNotificationCancelFailure;
        echo "\nfcsPurchaseProlongationOK files : " . $fcsPurchaseProlongationOK;
        echo "\nfcsPurchaseProlongationZK files : " . $fcsPurchaseProlongationZK;
        echo PHP_EOL;
        echo "\nfcsNotificationEA44 files : " . $fcsNotificationEA44;
        echo "\nfcsNotificationEP44 files : " . $fcsNotificationEP44;
        echo "\nfcsNotificationINM files : " . $fcsNotificationINM;
        echo "\nfcsNotificationOK44 files : " . $fcsNotificationOK44;
        echo "\nfcsNotificationOKU44 files : " . $fcsNotificationOKU44;
        echo "\nfcsNotificationPO44 files : " . $fcsNotificationPO44;
        echo "\nfcsNotificationZK44 files : " . $fcsNotificationZK44;
        echo "\nfcsNotificationZP44 files : " . $fcsNotificationZP44;
        echo PHP_EOL;
        echo "\nFiles in the folder : " . ($otherFormatCount + $xmlFormatCount);
        echo "\nDeleted files of other formats : " . $otherFormatCount;
        echo "\nRemaining files for processing : " . $xmlFormatCount;
        closedir($handle);
    }
}