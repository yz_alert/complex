<?php

namespace common\models;

use serhatozles\simplehtmldom\SimpleHTMLDom;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "gks".
 *
 * @property int $id
 * @property string $inn
 * @property string $year
 * @property string $response
 * @property string $created_at
 */
class Gks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn', 'year'], 'required'],
            [['inn', 'year', 'created_at'], 'safe'],
            [['inn', 'year'], 'unique', 'targetAttribute' => ['inn', 'year']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'inn' => Yii::t('app', 'Inn'),
            'year' => Yii::t('app', 'Year'),
            'response' => Yii::t('app', 'Response'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public static function getGksAnswer($year, $inn)
    {
        $domain = 'http://www.gks.ru';
        $urlPart = '/accounting_report';
        $url = $domain . $urlPart;
        $apiKeyRuCaptcha = '5fb4d88557d3d974a7d4bc80b37fb395';

        $html = SimpleHTMLDom::str_get_html(self::getHTMLviaCurlMethodGet($url));

        //$captchaUrl = $html->find('#requestCaptcha', 0)->src;

        $buttonOnClick = $html->find('.ui-button', 0)->onclick;
        $captchaUrl = $html->find('#sendAccountingRequestForm', 0)->find('#requestCaptchaSpan', 0)->find('img', 0)->src;

        $buttonUrl = str_replace("', 'https://10.177.55.111'));jQuery('#sendAccountingRequestForm').submit();", '',
            $buttonOnClick);
        $buttonUrl = str_replace("jQuery('#sendAccountingRequestForm').attr('action', getRelativeUrl('https://10.177.55.111/web/guest/accounting;",
            '',
            $buttonUrl);
        $arr = explode('&', $buttonUrl);
        $arr2 = explode('?', implode('', [$arr[0], $arr[1]]));
        $sessionId = explode('=', $arr2[0]);
        $sessionId = $sessionId[1];

        $pAuth = $arr = explode('p_p_id', $arr2[1]);
        $pAuth = $arr = explode('=', $pAuth[0]);
        $pAuth = $pAuth[1];
        $urlPart = "/accounting_report;jsessionid=$sessionId?p_auth=$pAuth&p_p_id=AccountingReport_WAR_accountingreport&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_AccountingReport_WAR_accountingreport_execution=e1s1&_AccountingReport_WAR_accountingreport__eventId=do.request"; //generate by jQuery

        $image = urlencode(base64_encode(file_get_contents($domain . $captchaUrl)));
//-----------------------------------------------------------------------------------------------
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://rucaptcha.com/in.php');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "method=base64&key=$apiKeyRuCaptcha&body=$image");
        $data = curl_exec($ch);

        if ($data == false) {
            $err = curl_error($ch);
            echo $err;
        }
        curl_close($ch);

        echo PHP_EOL;
        $result = explode('|', $data);
        $resultStatus = $result[0];
        $resultValue = $result[1];

        //-----------Ответ от ruCaptcha
        $res = self::checkCaptchaResult($resultValue);
        $res = '';
        //$counter = 10;

        while (stripos($res, 'OK|') === false) {
            $res = self::checkCaptchaResult($resultValue);
        }

        $res = explode('|', $res);
        $res = $res[1];

        //---------------Отправка запроса для GKS
        $year = $_GET['year'];
        $inn = $_GET['inn'];

        $data = self::curlPost($res, $year, $inn, $domain . $urlPart);
        $html = SimpleHTMLDom::str_get_html($data);

        $jsonResult = [];
        foreach ($html->find('table', 0)->find('tr') as $tr) {
            $key = trim($tr->find('td', 0)->plaintext);
            $value = trim($tr->find('td', 1)->plaintext);
            $jsonResult[$key] = $value;
        }

        $jsonResult = json_encode($jsonResult, JSON_PRETTY_PRINT);
        return $jsonResult;

    }

    public static function getHTMLviaCurlMethodGet($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');

        $data = curl_exec($ch);

        if ($data == false) {
            $err = curl_error($ch);
            return $err;
        }
        curl_close($ch);
        return $data;
    }

    public static function checkCaptchaResult($id, $apiKeyRuCaptcha = '5fb4d88557d3d974a7d4bc80b37fb395')
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://rucaptcha.com/res.php?key=' . $apiKeyRuCaptcha . '&action=get&id=' . $id);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');

        $data = curl_exec($ch);

        if ($data == false) {
            $err = curl_error($ch);
            echo $err;
        }
        curl_close($ch);

        if ($data == 'CAPCHA_NOT_READY') {
            sleep(3);
            return '';
        } elseif ($data == 'ERROR_CAPTCHA_UNSOLVABLE') {
            return 'Мы не можем решить вашу капчу — три наших работника не смогли её решить, либо мы не получили ответ в течение 90 секунд.
Мы не спишем с вас деньги за этот запрос.';
        } elseif ($data == 'ERROR_WRONG_USER_KEY') {
            return 'Вы указали значение параметра key в неверном формате, ключ должен содержать 32 символа.';
        } elseif ($data == 'ERROR_KEY_DOES_NOT_EXIST') {
            return 'Ключ, который вы указали, не существует.';
        } elseif ($data == 'ERROR_WRONG_ID_FORMAT') {
            return '	Вы отправили ID капчи в неправильном формате. ID состоит только из цифр.';
        } elseif ($data == 'ERROR_WRONG_CAPTCHA_ID') {
            return 'Вы отправили неверный ID капчи.';
        } elseif ($data == 'ERROR_BAD_DUPLICATES') {
            return '	Ошибка возвращается, если вы используете функцию 100% распознавания. Ошибка означает, что мы достигли максимального числа попыток, но требуемое количество совпадений достигнуто не было.';
        } elseif ($data == 'REPORT_NOT_RECORDED') {
            return 'Ошибка возвращается при отправке жалобы на неверный ответ если вы уже пожаловались на большое количество верно решённых капч.';
        }

        return $data;
    }

    public static function curlPost($captcha, $year, $inn, $url)
    {
        $ch = curl_init();

        $headers = [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding: gzip, deflate',
            'Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            'Cache-Control: max-age=0',
            'Connection: keep-alive',
            'Host: www.gks.ru',
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);
        curl_setopt($ch, CURLOPT_TIMEOUT, 55);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "reportYearId=$year&inn=$inn&okpo=&resultFormatId=&email=&captcha=$captcha");

        $data = curl_exec($ch);

        if ($data == false) {
            $err = curl_error($ch);
            return $err;
        }
        curl_close($ch);

        return $data;
    }

    public function fields()
    {
        return [
            'inn',
            'year',
            'created_at',
            'response' => function ($model) {
                return Json::decode($model->response);
            },
        ];
    }
}
