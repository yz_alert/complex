<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "expired_passports".
 *
 * @property integer $id
 * @property string $series
 * @property string $number
 */
class ExpiredPassports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expired_passports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['series', 'number'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'series' => Yii::t('app', 'Series'),
            'number' => Yii::t('app', 'Number'),
        ];
    }

    public function fields()
    {
        return [
            'series',
            'number',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ExpiredPassportsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ExpiredPassportsQuery(get_called_class());
    }
}
