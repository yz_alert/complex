<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "court_arbitration_case".
 *
 * @property integer $id
 * @property string $name
 * @property string $nomer_dela
 * @property string $summa_iska
 * @property string $start_dat
 * @property string $ogrn_id
 * @property boolean $is_sure
 *
 * @property CourtArbitration $ogrn
 */
class CourtArbitrationCase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'court_arbitration_case';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'nomer_dela', 'summa_iska', 'start_dat'], 'string'],
            [['ogrn_id'], 'required'],
            [['ogrn_id'], 'integer'],
            [['is_sure'], 'boolean'],
            [['ogrn_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourtArbitration::className(), 'targetAttribute' => ['ogrn_id' => 'ogrn']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'nomer_dela' => Yii::t('app', 'Nomer Dela'),
            'summa_iska' => Yii::t('app', 'Summa Iska'),
            'start_dat' => Yii::t('app', 'Start Dat'),
            'ogrn_id' => Yii::t('app', 'Ogrn ID'),
            'is_sure' => Yii::t('app', 'Is Sure'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOgrn()
    {
        return $this->hasOne(CourtArbitration::className(), ['ogrn' => 'ogrn_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourtArbitrationCaseMembers()
    {
        return $this->hasMany(CourtArbitrationCaseMembers::className(), ['court_arbitration_case_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\CourtArbitrationCaseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CourtArbitrationCaseQuery(get_called_class());
    }
}
