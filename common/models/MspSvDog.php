<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sv_dog".
 *
 * @property integer $id
 * @property string $naim_ul_zd
 * @property string $inn_ul_zd
 * @property string $predm_dog
 * @property string $nom_dog_reestr
 * @property string $data_dog
 * @property integer $msp_dokument_id
 *
 * @property MspDokument $mspDokument
 */
class MspSvDog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sv_dog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['naim_ul_zd', 'inn_ul_zd', 'predm_dog', 'nom_dog_reestr'], 'string'],
            [['data_dog'], 'safe'],
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naim_ul_zd' => Yii::t('app', 'Naim Ul Zd'),
            'inn_ul_zd' => Yii::t('app', 'Inn Ul Zd'),
            'predm_dog' => Yii::t('app', 'Predm Dog'),
            'nom_dog_reestr' => Yii::t('app', 'Nom Dog Reestr'),
            'data_dog' => Yii::t('app', 'Data Dog'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvDogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvDogQuery(get_called_class());
    }
}
