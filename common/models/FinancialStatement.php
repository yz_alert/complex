<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * This is the model class for table "financial_statement".
 *
 * @property integer $id
 * @property string $response
 * @property string $request_string
 * @property string $va_nematerialnyye_aktivy_2016
 * @property string $va_nematerialnyye_aktivy_2012
 * @property string $va_nematerialnyye_aktivy_2013
 * @property string $va_nematerialnyye_aktivy_2014
 * @property string $va_nematerialnyye_aktivy_2015
 * @property string $va_osnovnyye_sredstva_2016
 * @property string $va_osnovnyye_sredstva_2012
 * @property string $va_osnovnyye_sredstva_2013
 * @property string $va_osnovnyye_sredstva_2014
 * @property string $va_osnovnyye_sredstva_2015
 * @property string $va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2016
 * @property string $va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2012
 * @property string $va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2013
 * @property string $va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2014
 * @property string $va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2015
 * @property string $va_finansovyye_vlozheniya_2016
 * @property string $va_finansovyye_vlozheniya_2012
 * @property string $va_finansovyye_vlozheniya_2013
 * @property string $va_finansovyye_vlozheniya_2014
 * @property string $va_finansovyye_vlozheniya_2015
 * @property string $va_otlozhennyye_nalogovyye_aktivy_2016
 * @property string $va_otlozhennyye_nalogovyye_aktivy_2012
 * @property string $va_otlozhennyye_nalogovyye_aktivy_2013
 * @property string $va_otlozhennyye_nalogovyye_aktivy_2014
 * @property string $va_otlozhennyye_nalogovyye_aktivy_2015
 * @property string $va_itogo_po_razdelu_1_2016
 * @property string $va_itogo_po_razdelu_1_2012
 * @property string $va_itogo_po_razdelu_1_2013
 * @property string $va_itogo_po_razdelu_1_2014
 * @property string $va_itogo_po_razdelu_1_2015
 * @property string $oa_zapasy_2016
 * @property string $oa_zapasy_2012
 * @property string $oa_zapasy_2013
 * @property string $oa_zapasy_2014
 * @property string $oa_zapasy_2015
 * @property string $oa_nds_po_priobretennym_tsennostyam_2016
 * @property string $oa_nds_po_priobretennym_tsennostyam_2012
 * @property string $oa_nds_po_priobretennym_tsennostyam_2013
 * @property string $oa_nds_po_priobretennym_tsennostyam_2014
 * @property string $oa_nds_po_priobretennym_tsennostyam_2015
 * @property string $oa_debitorskaya_zadolzhennost_2016
 * @property string $oa_debitorskaya_zadolzhennost_2012
 * @property string $oa_debitorskaya_zadolzhennost_2013
 * @property string $oa_debitorskaya_zadolzhennost_2014
 * @property string $oa_debitorskaya_zadolzhennost_2015
 * @property string $oa_finansovyye_vlozheniya_2016
 * @property string $oa_finansovyye_vlozheniya_2012
 * @property string $oa_finansovyye_vlozheniya_2013
 * @property string $oa_finansovyye_vlozheniya_2014
 * @property string $oa_finansovyye_vlozheniya_2015
 * @property string $oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2016
 * @property string $oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2012
 * @property string $oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2013
 * @property string $oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2014
 * @property string $oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2015
 * @property string $oa_prochiye_oborotnyye_aktivy_2016
 * @property string $oa_prochiye_oborotnyye_aktivy_2012
 * @property string $oa_prochiye_oborotnyye_aktivy_2013
 * @property string $oa_prochiye_oborotnyye_aktivy_2014
 * @property string $oa_prochiye_oborotnyye_aktivy_2015
 * @property string $oa_itogo_po_razdelu_2_2016
 * @property string $oa_itogo_po_razdelu_2_2012
 * @property string $oa_itogo_po_razdelu_2_2013
 * @property string $oa_itogo_po_razdelu_2_2014
 * @property string $oa_itogo_po_razdelu_2_2015
 * @property string $oa_balans_2016
 * @property string $oa_balans_2012
 * @property string $oa_balans_2013
 * @property string $oa_balans_2014
 * @property string $oa_balans_2015
 * @property string $kir_ustavnyy_kapital_2016
 * @property string $kir_ustavnyy_kapital_2012
 * @property string $kir_ustavnyy_kapital_2013
 * @property string $kir_ustavnyy_kapital_2014
 * @property string $kir_ustavnyy_kapital_2015
 * @property string $kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2016
 * @property string $kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2012
 * @property string $kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2013
 * @property string $kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2014
 * @property string $kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2015
 * @property string $kir_dobavochnyy_kapital_2016
 * @property string $kir_dobavochnyy_kapital_2012
 * @property string $kir_dobavochnyy_kapital_2013
 * @property string $kir_dobavochnyy_kapital_2014
 * @property string $kir_dobavochnyy_kapital_2015
 * @property string $kir_rezervnyy_kapital_2016
 * @property string $kir_rezervnyy_kapital_2012
 * @property string $kir_rezervnyy_kapital_2013
 * @property string $kir_rezervnyy_kapital_2014
 * @property string $kir_rezervnyy_kapital_2015
 * @property string $kir_neraspredelennaya_pribyl_2016
 * @property string $kir_neraspredelennaya_pribyl_2012
 * @property string $kir_neraspredelennaya_pribyl_2013
 * @property string $kir_neraspredelennaya_pribyl_2014
 * @property string $kir_neraspredelennaya_pribyl_2015
 * @property string $kir_itogo_po_razdelu_3_2016
 * @property string $kir_itogo_po_razdelu_3_2012
 * @property string $kir_itogo_po_razdelu_3_2013
 * @property string $kir_itogo_po_razdelu_3_2014
 * @property string $kir_itogo_po_razdelu_3_2015
 * @property string $do_zayemnyye_sredstva_2016
 * @property string $do_zayemnyye_sredstva_2012
 * @property string $do_zayemnyye_sredstva_2013
 * @property string $do_zayemnyye_sredstva_2014
 * @property string $do_zayemnyye_sredstva_2015
 * @property string $do_otlozhennyye_nalogovyye_obyazatelstva_2016
 * @property string $do_otlozhennyye_nalogovyye_obyazatelstva_2012
 * @property string $do_otlozhennyye_nalogovyye_obyazatelstva_2013
 * @property string $do_otlozhennyye_nalogovyye_obyazatelstva_2014
 * @property string $do_otlozhennyye_nalogovyye_obyazatelstva_2015
 * @property string $do_prochiye_obyazatelstva_2016
 * @property string $do_prochiye_obyazatelstva_2012
 * @property string $do_prochiye_obyazatelstva_2013
 * @property string $do_prochiye_obyazatelstva_2014
 * @property string $do_prochiye_obyazatelstva_2015
 * @property string $do_itogo_po_razdelu_4_2016
 * @property string $do_itogo_po_razdelu_4_2012
 * @property string $do_itogo_po_razdelu_4_2013
 * @property string $do_itogo_po_razdelu_4_2014
 * @property string $do_itogo_po_razdelu_4_2015
 * @property string $ko_zayemnyye_sredstva_2016
 * @property string $ko_zayemnyye_sredstva_2012
 * @property string $ko_zayemnyye_sredstva_2013
 * @property string $ko_zayemnyye_sredstva_2014
 * @property string $ko_zayemnyye_sredstva_2015
 * @property string $ko_kreditorskaya_zadolzhennost_2016
 * @property string $ko_kreditorskaya_zadolzhennost_2012
 * @property string $ko_kreditorskaya_zadolzhennost_2013
 * @property string $ko_kreditorskaya_zadolzhennost_2014
 * @property string $ko_kreditorskaya_zadolzhennost_2015
 * @property string $ko_dokhody_budushchikh_periodov_2016
 * @property string $ko_dokhody_budushchikh_periodov_2012
 * @property string $ko_dokhody_budushchikh_periodov_2013
 * @property string $ko_dokhody_budushchikh_periodov_2014
 * @property string $ko_dokhody_budushchikh_periodov_2015
 * @property string $ko_prochiye_obyazatelstva_2016
 * @property string $ko_prochiye_obyazatelstva_2012
 * @property string $ko_prochiye_obyazatelstva_2013
 * @property string $ko_prochiye_obyazatelstva_2014
 * @property string $ko_prochiye_obyazatelstva_2015
 * @property string $ko_itogo_po_razdelu_5_2016
 * @property string $ko_itogo_po_razdelu_5_2012
 * @property string $ko_itogo_po_razdelu_5_2013
 * @property string $ko_itogo_po_razdelu_5_2014
 * @property string $ko_itogo_po_razdelu_5_2015
 * @property string $ko_balans_2016
 * @property string $ko_balans_2012
 * @property string $ko_balans_2013
 * @property string $ko_balans_2014
 * @property string $ko_balans_2015
 * @property string $dirpovd_vyruchka_2012
 * @property string $dirpovd_vyruchka_2013
 * @property string $dirpovd_vyruchka_2014
 * @property string $dirpovd_vyruchka_2015
 * @property string $dirpovd_vyruchka_2016
 * @property string $dirpovd_sebestoimost_prodazh_2012
 * @property string $dirpovd_sebestoimost_prodazh_2013
 * @property string $dirpovd_sebestoimost_prodazh_2014
 * @property string $dirpovd_sebestoimost_prodazh_2015
 * @property string $dirpovd_sebestoimost_prodazh_2016
 * @property string $dirpovd_valovaya_pribyl_2012
 * @property string $dirpovd_valovaya_pribyl_2013
 * @property string $dirpovd_valovaya_pribyl_2014
 * @property string $dirpovd_valovaya_pribyl_2015
 * @property string $dirpovd_valovaya_pribyl_2016
 * @property string $dirpovd_kommercheskiye_raskhody_2012
 * @property string $dirpovd_kommercheskiye_raskhody_2013
 * @property string $dirpovd_kommercheskiye_raskhody_2014
 * @property string $dirpovd_kommercheskiye_raskhody_2015
 * @property string $dirpovd_kommercheskiye_raskhody_2016
 * @property string $dirpovd_upravlencheskiye_raskhody_2012
 * @property string $dirpovd_upravlencheskiye_raskhody_2013
 * @property string $dirpovd_upravlencheskiye_raskhody_2014
 * @property string $dirpovd_upravlencheskiye_raskhody_2015
 * @property string $dirpovd_upravlencheskiye_raskhody_2016
 * @property string $dirpovd_pribyl_ot_prodazh_2012
 * @property string $dirpovd_pribyl_ot_prodazh_2013
 * @property string $dirpovd_pribyl_ot_prodazh_2014
 * @property string $dirpovd_pribyl_ot_prodazh_2015
 * @property string $dirpovd_pribyl_ot_prodazh_2016
 * @property string $pdir_protsenty_k_polucheniyu_2012
 * @property string $pdir_protsenty_k_polucheniyu_2013
 * @property string $pdir_protsenty_k_polucheniyu_2014
 * @property string $pdir_protsenty_k_polucheniyu_2015
 * @property string $pdir_protsenty_k_polucheniyu_2016
 * @property string $pdir_protsenty_k_uplate_2012
 * @property string $pdir_protsenty_k_uplate_2013
 * @property string $pdir_protsenty_k_uplate_2014
 * @property string $pdir_protsenty_k_uplate_2015
 * @property string $pdir_protsenty_k_uplate_2016
 * @property string $pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2012
 * @property string $pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2013
 * @property string $pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2014
 * @property string $pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2015
 * @property string $pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2016
 * @property string $pdir_prochiye_dokhody_2012
 * @property string $pdir_prochiye_dokhody_2013
 * @property string $pdir_prochiye_dokhody_2014
 * @property string $pdir_prochiye_dokhody_2015
 * @property string $pdir_prochiye_dokhody_2016
 * @property string $pdir_prochiye_raskhody_2012
 * @property string $pdir_prochiye_raskhody_2013
 * @property string $pdir_prochiye_raskhody_2014
 * @property string $pdir_prochiye_raskhody_2015
 * @property string $pdir_prochiye_raskhody_2016
 * @property string $pdir_pribyl_do_nalogooblozheniya_2012
 * @property string $pdir_pribyl_do_nalogooblozheniya_2013
 * @property string $pdir_pribyl_do_nalogooblozheniya_2014
 * @property string $pdir_pribyl_do_nalogooblozheniya_2015
 * @property string $pdir_pribyl_do_nalogooblozheniya_2016
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2012
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2013
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2014
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2015
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2016
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2012
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2013
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2014
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2015
 * @property string $pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2016
 * @property string $pdir_tekushchiy_nalog_na_pribyl_2012
 * @property string $pdir_tekushchiy_nalog_na_pribyl_2013
 * @property string $pdir_tekushchiy_nalog_na_pribyl_2014
 * @property string $pdir_tekushchiy_nalog_na_pribyl_2015
 * @property string $pdir_tekushchiy_nalog_na_pribyl_2016
 * @property string $pdir_chistaya_pribyl_2012
 * @property string $pdir_chistaya_pribyl_2013
 * @property string $pdir_chistaya_pribyl_2014
 * @property string $pdir_chistaya_pribyl_2015
 * @property string $pdir_chistaya_pribyl_2016
 * @property string $pdir_postoyannyye_nalogovyye_obyazatelstva_2012
 * @property string $pdir_postoyannyye_nalogovyye_obyazatelstva_2013
 * @property string $pdir_postoyannyye_nalogovyye_obyazatelstva_2014
 * @property string $pdir_postoyannyye_nalogovyye_obyazatelstva_2015
 * @property string $pdir_postoyannyye_nalogovyye_obyazatelstva_2016
 * @property string $va_prochiye_vneoborotnyye_aktivy_2012
 * @property string $va_prochiye_vneoborotnyye_aktivy_2013
 * @property string $va_prochiye_vneoborotnyye_aktivy_2014
 * @property string $va_prochiye_vneoborotnyye_aktivy_2015
 * @property string $va_prochiye_vneoborotnyye_aktivy_2016
 */
class FinancialStatement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'financial_statement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'response',
                    'request_string',
                    'va_nematerialnyye_aktivy_2016',
                    'va_nematerialnyye_aktivy_2012',
                    'va_nematerialnyye_aktivy_2013',
                    'va_nematerialnyye_aktivy_2014',
                    'va_nematerialnyye_aktivy_2015',
                    'va_osnovnyye_sredstva_2016',
                    'va_osnovnyye_sredstva_2012',
                    'va_osnovnyye_sredstva_2013',
                    'va_osnovnyye_sredstva_2014',
                    'va_osnovnyye_sredstva_2015',
                    'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2016',
                    'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2012',
                    'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2013',
                    'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2014',
                    'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2015',
                    'va_finansovyye_vlozheniya_2016',
                    'va_finansovyye_vlozheniya_2012',
                    'va_finansovyye_vlozheniya_2013',
                    'va_finansovyye_vlozheniya_2014',
                    'va_finansovyye_vlozheniya_2015',
                    'va_otlozhennyye_nalogovyye_aktivy_2016',
                    'va_otlozhennyye_nalogovyye_aktivy_2012',
                    'va_otlozhennyye_nalogovyye_aktivy_2013',
                    'va_otlozhennyye_nalogovyye_aktivy_2014',
                    'va_otlozhennyye_nalogovyye_aktivy_2015',
                    'va_itogo_po_razdelu_1_2016',
                    'va_itogo_po_razdelu_1_2012',
                    'va_itogo_po_razdelu_1_2013',
                    'va_itogo_po_razdelu_1_2014',
                    'va_itogo_po_razdelu_1_2015',
                    'oa_zapasy_2016',
                    'oa_zapasy_2012',
                    'oa_zapasy_2013',
                    'oa_zapasy_2014',
                    'oa_zapasy_2015',
                    'oa_nds_po_priobretennym_tsennostyam_2016',
                    'oa_nds_po_priobretennym_tsennostyam_2012',
                    'oa_nds_po_priobretennym_tsennostyam_2013',
                    'oa_nds_po_priobretennym_tsennostyam_2014',
                    'oa_nds_po_priobretennym_tsennostyam_2015',
                    'oa_debitorskaya_zadolzhennost_2016',
                    'oa_debitorskaya_zadolzhennost_2012',
                    'oa_debitorskaya_zadolzhennost_2013',
                    'oa_debitorskaya_zadolzhennost_2014',
                    'oa_debitorskaya_zadolzhennost_2015',
                    'oa_finansovyye_vlozheniya_2016',
                    'oa_finansovyye_vlozheniya_2012',
                    'oa_finansovyye_vlozheniya_2013',
                    'oa_finansovyye_vlozheniya_2014',
                    'oa_finansovyye_vlozheniya_2015',
                    'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2016',
                    'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2012',
                    'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2013',
                    'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2014',
                    'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2015',
                    'oa_prochiye_oborotnyye_aktivy_2016',
                    'oa_prochiye_oborotnyye_aktivy_2012',
                    'oa_prochiye_oborotnyye_aktivy_2013',
                    'oa_prochiye_oborotnyye_aktivy_2014',
                    'oa_prochiye_oborotnyye_aktivy_2015',
                    'oa_itogo_po_razdelu_2_2016',
                    'oa_itogo_po_razdelu_2_2012',
                    'oa_itogo_po_razdelu_2_2013',
                    'oa_itogo_po_razdelu_2_2014',
                    'oa_itogo_po_razdelu_2_2015',
                    'oa_balans_2016',
                    'oa_balans_2012',
                    'oa_balans_2013',
                    'oa_balans_2014',
                    'oa_balans_2015',
                    'kir_ustavnyy_kapital_2016',
                    'kir_ustavnyy_kapital_2012',
                    'kir_ustavnyy_kapital_2013',
                    'kir_ustavnyy_kapital_2014',
                    'kir_ustavnyy_kapital_2015',
                    'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2016',
                    'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2012',
                    'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2013',
                    'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2014',
                    'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2015',
                    'kir_dobavochnyy_kapital_2016',
                    'kir_dobavochnyy_kapital_2012',
                    'kir_dobavochnyy_kapital_2013',
                    'kir_dobavochnyy_kapital_2014',
                    'kir_dobavochnyy_kapital_2015',
                    'kir_rezervnyy_kapital_2016',
                    'kir_rezervnyy_kapital_2012',
                    'kir_rezervnyy_kapital_2013',
                    'kir_rezervnyy_kapital_2014',
                    'kir_rezervnyy_kapital_2015',
                    'kir_neraspredelennaya_pribyl_2016',
                    'kir_neraspredelennaya_pribyl_2012',
                    'kir_neraspredelennaya_pribyl_2013',
                    'kir_neraspredelennaya_pribyl_2014',
                    'kir_neraspredelennaya_pribyl_2015',
                    'kir_itogo_po_razdelu_3_2016',
                    'kir_itogo_po_razdelu_3_2012',
                    'kir_itogo_po_razdelu_3_2013',
                    'kir_itogo_po_razdelu_3_2014',
                    'kir_itogo_po_razdelu_3_2015',
                    'do_zayemnyye_sredstva_2016',
                    'do_zayemnyye_sredstva_2012',
                    'do_zayemnyye_sredstva_2013',
                    'do_zayemnyye_sredstva_2014',
                    'do_zayemnyye_sredstva_2015',
                    'do_otlozhennyye_nalogovyye_obyazatelstva_2016',
                    'do_otlozhennyye_nalogovyye_obyazatelstva_2012',
                    'do_otlozhennyye_nalogovyye_obyazatelstva_2013',
                    'do_otlozhennyye_nalogovyye_obyazatelstva_2014',
                    'do_otlozhennyye_nalogovyye_obyazatelstva_2015',
                    'do_prochiye_obyazatelstva_2016',
                    'do_prochiye_obyazatelstva_2012',
                    'do_prochiye_obyazatelstva_2013',
                    'do_prochiye_obyazatelstva_2014',
                    'do_prochiye_obyazatelstva_2015',
                    'do_itogo_po_razdelu_4_2016',
                    'do_itogo_po_razdelu_4_2012',
                    'do_itogo_po_razdelu_4_2013',
                    'do_itogo_po_razdelu_4_2014',
                    'do_itogo_po_razdelu_4_2015',
                    'ko_zayemnyye_sredstva_2016',
                    'ko_zayemnyye_sredstva_2012',
                    'ko_zayemnyye_sredstva_2013',
                    'ko_zayemnyye_sredstva_2014',
                    'ko_zayemnyye_sredstva_2015',
                    'ko_kreditorskaya_zadolzhennost_2016',
                    'ko_kreditorskaya_zadolzhennost_2012',
                    'ko_kreditorskaya_zadolzhennost_2013',
                    'ko_kreditorskaya_zadolzhennost_2014',
                    'ko_kreditorskaya_zadolzhennost_2015',
                    'ko_dokhody_budushchikh_periodov_2016',
                    'ko_dokhody_budushchikh_periodov_2012',
                    'ko_dokhody_budushchikh_periodov_2013',
                    'ko_dokhody_budushchikh_periodov_2014',
                    'ko_dokhody_budushchikh_periodov_2015',
                    'ko_prochiye_obyazatelstva_2016',
                    'ko_prochiye_obyazatelstva_2012',
                    'ko_prochiye_obyazatelstva_2013',
                    'ko_prochiye_obyazatelstva_2014',
                    'ko_prochiye_obyazatelstva_2015',
                    'ko_itogo_po_razdelu_5_2016',
                    'ko_itogo_po_razdelu_5_2012',
                    'ko_itogo_po_razdelu_5_2013',
                    'ko_itogo_po_razdelu_5_2014',
                    'ko_itogo_po_razdelu_5_2015',
                    'ko_balans_2016',
                    'ko_balans_2012',
                    'ko_balans_2013',
                    'ko_balans_2014',
                    'ko_balans_2015',
                    'dirpovd_vyruchka_2012',
                    'dirpovd_vyruchka_2013',
                    'dirpovd_vyruchka_2014',
                    'dirpovd_vyruchka_2015',
                    'dirpovd_vyruchka_2016',
                    'dirpovd_sebestoimost_prodazh_2012',
                    'dirpovd_sebestoimost_prodazh_2013',
                    'dirpovd_sebestoimost_prodazh_2014',
                    'dirpovd_sebestoimost_prodazh_2015',
                    'dirpovd_sebestoimost_prodazh_2016',
                    'dirpovd_valovaya_pribyl_2012',
                    'dirpovd_valovaya_pribyl_2013',
                    'dirpovd_valovaya_pribyl_2014',
                    'dirpovd_valovaya_pribyl_2015',
                    'dirpovd_valovaya_pribyl_2016',
                    'dirpovd_kommercheskiye_raskhody_2012',
                    'dirpovd_kommercheskiye_raskhody_2013',
                    'dirpovd_kommercheskiye_raskhody_2014',
                    'dirpovd_kommercheskiye_raskhody_2015',
                    'dirpovd_kommercheskiye_raskhody_2016',
                    'dirpovd_upravlencheskiye_raskhody_2012',
                    'dirpovd_upravlencheskiye_raskhody_2013',
                    'dirpovd_upravlencheskiye_raskhody_2014',
                    'dirpovd_upravlencheskiye_raskhody_2015',
                    'dirpovd_upravlencheskiye_raskhody_2016',
                    'dirpovd_pribyl_ot_prodazh_2012',
                    'dirpovd_pribyl_ot_prodazh_2013',
                    'dirpovd_pribyl_ot_prodazh_2014',
                    'dirpovd_pribyl_ot_prodazh_2015',
                    'dirpovd_pribyl_ot_prodazh_2016',
                    'pdir_protsenty_k_polucheniyu_2012',
                    'pdir_protsenty_k_polucheniyu_2013',
                    'pdir_protsenty_k_polucheniyu_2014',
                    'pdir_protsenty_k_polucheniyu_2015',
                    'pdir_protsenty_k_polucheniyu_2016',
                    'pdir_protsenty_k_uplate_2012',
                    'pdir_protsenty_k_uplate_2013',
                    'pdir_protsenty_k_uplate_2014',
                    'pdir_protsenty_k_uplate_2015',
                    'pdir_protsenty_k_uplate_2016',
                    'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2012',
                    'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2013',
                    'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2014',
                    'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2015',
                    'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2016',
                    'pdir_prochiye_dokhody_2012',
                    'pdir_prochiye_dokhody_2013',
                    'pdir_prochiye_dokhody_2014',
                    'pdir_prochiye_dokhody_2015',
                    'pdir_prochiye_dokhody_2016',
                    'pdir_prochiye_raskhody_2012',
                    'pdir_prochiye_raskhody_2013',
                    'pdir_prochiye_raskhody_2014',
                    'pdir_prochiye_raskhody_2015',
                    'pdir_prochiye_raskhody_2016',
                    'pdir_pribyl_do_nalogooblozheniya_2012',
                    'pdir_pribyl_do_nalogooblozheniya_2013',
                    'pdir_pribyl_do_nalogooblozheniya_2014',
                    'pdir_pribyl_do_nalogooblozheniya_2015',
                    'pdir_pribyl_do_nalogooblozheniya_2016',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2012',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2013',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2014',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2015',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2016',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2012',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2013',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2014',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2015',
                    'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2016',
                    'pdir_tekushchiy_nalog_na_pribyl_2012',
                    'pdir_tekushchiy_nalog_na_pribyl_2013',
                    'pdir_tekushchiy_nalog_na_pribyl_2014',
                    'pdir_tekushchiy_nalog_na_pribyl_2015',
                    'pdir_tekushchiy_nalog_na_pribyl_2016',
                    'pdir_chistaya_pribyl_2012',
                    'pdir_chistaya_pribyl_2013',
                    'pdir_chistaya_pribyl_2014',
                    'pdir_chistaya_pribyl_2015',
                    'pdir_chistaya_pribyl_2016',
                    'pdir_postoyannyye_nalogovyye_obyazatelstva_2012',
                    'pdir_postoyannyye_nalogovyye_obyazatelstva_2013',
                    'pdir_postoyannyye_nalogovyye_obyazatelstva_2014',
                    'pdir_postoyannyye_nalogovyye_obyazatelstva_2015',
                    'pdir_postoyannyye_nalogovyye_obyazatelstva_2016',
                    'va_prochiye_vneoborotnyye_aktivy_2012',
                    'va_prochiye_vneoborotnyye_aktivy_2013',
                    'va_prochiye_vneoborotnyye_aktivy_2014',
                    'va_prochiye_vneoborotnyye_aktivy_2015',
                    'va_prochiye_vneoborotnyye_aktivy_2016',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'response' => Yii::t('app', 'Response'),
            'request_string' => Yii::t('app', 'Request String'),
            'va_nematerialnyye_aktivy_2016' => Yii::t('app', 'Va Nematerialnyye Aktivy 2016'),
            'va_nematerialnyye_aktivy_2012' => Yii::t('app', 'Va Nematerialnyye Aktivy 2012'),
            'va_nematerialnyye_aktivy_2013' => Yii::t('app', 'Va Nematerialnyye Aktivy 2013'),
            'va_nematerialnyye_aktivy_2014' => Yii::t('app', 'Va Nematerialnyye Aktivy 2014'),
            'va_nematerialnyye_aktivy_2015' => Yii::t('app', 'Va Nematerialnyye Aktivy 2015'),
            'va_osnovnyye_sredstva_2016' => Yii::t('app', 'Va Osnovnyye Sredstva 2016'),
            'va_osnovnyye_sredstva_2012' => Yii::t('app', 'Va Osnovnyye Sredstva 2012'),
            'va_osnovnyye_sredstva_2013' => Yii::t('app', 'Va Osnovnyye Sredstva 2013'),
            'va_osnovnyye_sredstva_2014' => Yii::t('app', 'Va Osnovnyye Sredstva 2014'),
            'va_osnovnyye_sredstva_2015' => Yii::t('app', 'Va Osnovnyye Sredstva 2015'),
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2016' => Yii::t('app',
                'Va Dokhodnyye Vlozheniya V Materialnyye Tsennosti 2016'),
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2012' => Yii::t('app',
                'Va Dokhodnyye Vlozheniya V Materialnyye Tsennosti 2012'),
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2013' => Yii::t('app',
                'Va Dokhodnyye Vlozheniya V Materialnyye Tsennosti 2013'),
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2014' => Yii::t('app',
                'Va Dokhodnyye Vlozheniya V Materialnyye Tsennosti 2014'),
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2015' => Yii::t('app',
                'Va Dokhodnyye Vlozheniya V Materialnyye Tsennosti 2015'),
            'va_finansovyye_vlozheniya_2016' => Yii::t('app', 'Va Finansovyye Vlozheniya 2016'),
            'va_finansovyye_vlozheniya_2012' => Yii::t('app', 'Va Finansovyye Vlozheniya 2012'),
            'va_finansovyye_vlozheniya_2013' => Yii::t('app', 'Va Finansovyye Vlozheniya 2013'),
            'va_finansovyye_vlozheniya_2014' => Yii::t('app', 'Va Finansovyye Vlozheniya 2014'),
            'va_finansovyye_vlozheniya_2015' => Yii::t('app', 'Va Finansovyye Vlozheniya 2015'),
            'va_otlozhennyye_nalogovyye_aktivy_2016' => Yii::t('app', 'Va Otlozhennyye Nalogovyye Aktivy 2016'),
            'va_otlozhennyye_nalogovyye_aktivy_2012' => Yii::t('app', 'Va Otlozhennyye Nalogovyye Aktivy 2012'),
            'va_otlozhennyye_nalogovyye_aktivy_2013' => Yii::t('app', 'Va Otlozhennyye Nalogovyye Aktivy 2013'),
            'va_otlozhennyye_nalogovyye_aktivy_2014' => Yii::t('app', 'Va Otlozhennyye Nalogovyye Aktivy 2014'),
            'va_otlozhennyye_nalogovyye_aktivy_2015' => Yii::t('app', 'Va Otlozhennyye Nalogovyye Aktivy 2015'),
            'va_itogo_po_razdelu_1_2016' => Yii::t('app', 'Va Itogo Po Razdelu 1 2016'),
            'va_itogo_po_razdelu_1_2012' => Yii::t('app', 'Va Itogo Po Razdelu 1 2012'),
            'va_itogo_po_razdelu_1_2013' => Yii::t('app', 'Va Itogo Po Razdelu 1 2013'),
            'va_itogo_po_razdelu_1_2014' => Yii::t('app', 'Va Itogo Po Razdelu 1 2014'),
            'va_itogo_po_razdelu_1_2015' => Yii::t('app', 'Va Itogo Po Razdelu 1 2015'),
            'oa_zapasy_2016' => Yii::t('app', 'Oa Zapasy 2016'),
            'oa_zapasy_2012' => Yii::t('app', 'Oa Zapasy 2012'),
            'oa_zapasy_2013' => Yii::t('app', 'Oa Zapasy 2013'),
            'oa_zapasy_2014' => Yii::t('app', 'Oa Zapasy 2014'),
            'oa_zapasy_2015' => Yii::t('app', 'Oa Zapasy 2015'),
            'oa_nds_po_priobretennym_tsennostyam_2016' => Yii::t('app', 'Oa Nds Po Priobretennym Tsennostyam 2016'),
            'oa_nds_po_priobretennym_tsennostyam_2012' => Yii::t('app', 'Oa Nds Po Priobretennym Tsennostyam 2012'),
            'oa_nds_po_priobretennym_tsennostyam_2013' => Yii::t('app', 'Oa Nds Po Priobretennym Tsennostyam 2013'),
            'oa_nds_po_priobretennym_tsennostyam_2014' => Yii::t('app', 'Oa Nds Po Priobretennym Tsennostyam 2014'),
            'oa_nds_po_priobretennym_tsennostyam_2015' => Yii::t('app', 'Oa Nds Po Priobretennym Tsennostyam 2015'),
            'oa_debitorskaya_zadolzhennost_2016' => Yii::t('app', 'Oa Debitorskaya Zadolzhennost 2016'),
            'oa_debitorskaya_zadolzhennost_2012' => Yii::t('app', 'Oa Debitorskaya Zadolzhennost 2012'),
            'oa_debitorskaya_zadolzhennost_2013' => Yii::t('app', 'Oa Debitorskaya Zadolzhennost 2013'),
            'oa_debitorskaya_zadolzhennost_2014' => Yii::t('app', 'Oa Debitorskaya Zadolzhennost 2014'),
            'oa_debitorskaya_zadolzhennost_2015' => Yii::t('app', 'Oa Debitorskaya Zadolzhennost 2015'),
            'oa_finansovyye_vlozheniya_2016' => Yii::t('app', 'Oa Finansovyye Vlozheniya 2016'),
            'oa_finansovyye_vlozheniya_2012' => Yii::t('app', 'Oa Finansovyye Vlozheniya 2012'),
            'oa_finansovyye_vlozheniya_2013' => Yii::t('app', 'Oa Finansovyye Vlozheniya 2013'),
            'oa_finansovyye_vlozheniya_2014' => Yii::t('app', 'Oa Finansovyye Vlozheniya 2014'),
            'oa_finansovyye_vlozheniya_2015' => Yii::t('app', 'Oa Finansovyye Vlozheniya 2015'),
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2016' => Yii::t('app',
                'Oa Denezhnyye Sredstva I Denezhnyye Ekvivalenty 2016'),
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2012' => Yii::t('app',
                'Oa Denezhnyye Sredstva I Denezhnyye Ekvivalenty 2012'),
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2013' => Yii::t('app',
                'Oa Denezhnyye Sredstva I Denezhnyye Ekvivalenty 2013'),
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2014' => Yii::t('app',
                'Oa Denezhnyye Sredstva I Denezhnyye Ekvivalenty 2014'),
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2015' => Yii::t('app',
                'Oa Denezhnyye Sredstva I Denezhnyye Ekvivalenty 2015'),
            'oa_prochiye_oborotnyye_aktivy_2016' => Yii::t('app', 'Oa Prochiye Oborotnyye Aktivy 2016'),
            'oa_prochiye_oborotnyye_aktivy_2012' => Yii::t('app', 'Oa Prochiye Oborotnyye Aktivy 2012'),
            'oa_prochiye_oborotnyye_aktivy_2013' => Yii::t('app', 'Oa Prochiye Oborotnyye Aktivy 2013'),
            'oa_prochiye_oborotnyye_aktivy_2014' => Yii::t('app', 'Oa Prochiye Oborotnyye Aktivy 2014'),
            'oa_prochiye_oborotnyye_aktivy_2015' => Yii::t('app', 'Oa Prochiye Oborotnyye Aktivy 2015'),
            'oa_itogo_po_razdelu_2_2016' => Yii::t('app', 'Oa Itogo Po Razdelu 2 2016'),
            'oa_itogo_po_razdelu_2_2012' => Yii::t('app', 'Oa Itogo Po Razdelu 2 2012'),
            'oa_itogo_po_razdelu_2_2013' => Yii::t('app', 'Oa Itogo Po Razdelu 2 2013'),
            'oa_itogo_po_razdelu_2_2014' => Yii::t('app', 'Oa Itogo Po Razdelu 2 2014'),
            'oa_itogo_po_razdelu_2_2015' => Yii::t('app', 'Oa Itogo Po Razdelu 2 2015'),
            'oa_balans_2016' => Yii::t('app', 'Oa Balans 2016'),
            'oa_balans_2012' => Yii::t('app', 'Oa Balans 2012'),
            'oa_balans_2013' => Yii::t('app', 'Oa Balans 2013'),
            'oa_balans_2014' => Yii::t('app', 'Oa Balans 2014'),
            'oa_balans_2015' => Yii::t('app', 'Oa Balans 2015'),
            'kir_ustavnyy_kapital_2016' => Yii::t('app', 'Kir Ustavnyy Kapital 2016'),
            'kir_ustavnyy_kapital_2012' => Yii::t('app', 'Kir Ustavnyy Kapital 2012'),
            'kir_ustavnyy_kapital_2013' => Yii::t('app', 'Kir Ustavnyy Kapital 2013'),
            'kir_ustavnyy_kapital_2014' => Yii::t('app', 'Kir Ustavnyy Kapital 2014'),
            'kir_ustavnyy_kapital_2015' => Yii::t('app', 'Kir Ustavnyy Kapital 2015'),
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2016' => Yii::t('app',
                'Kir Sobstvennyye Aktsii Vykuplennyye U Aktsionerov 2016'),
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2012' => Yii::t('app',
                'Kir Sobstvennyye Aktsii Vykuplennyye U Aktsionerov 2012'),
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2013' => Yii::t('app',
                'Kir Sobstvennyye Aktsii Vykuplennyye U Aktsionerov 2013'),
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2014' => Yii::t('app',
                'Kir Sobstvennyye Aktsii Vykuplennyye U Aktsionerov 2014'),
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2015' => Yii::t('app',
                'Kir Sobstvennyye Aktsii Vykuplennyye U Aktsionerov 2015'),
            'kir_dobavochnyy_kapital_2016' => Yii::t('app', 'Kir Dobavochnyy Kapital 2016'),
            'kir_dobavochnyy_kapital_2012' => Yii::t('app', 'Kir Dobavochnyy Kapital 2012'),
            'kir_dobavochnyy_kapital_2013' => Yii::t('app', 'Kir Dobavochnyy Kapital 2013'),
            'kir_dobavochnyy_kapital_2014' => Yii::t('app', 'Kir Dobavochnyy Kapital 2014'),
            'kir_dobavochnyy_kapital_2015' => Yii::t('app', 'Kir Dobavochnyy Kapital 2015'),
            'kir_rezervnyy_kapital_2016' => Yii::t('app', 'Kir Rezervnyy Kapital 2016'),
            'kir_rezervnyy_kapital_2012' => Yii::t('app', 'Kir Rezervnyy Kapital 2012'),
            'kir_rezervnyy_kapital_2013' => Yii::t('app', 'Kir Rezervnyy Kapital 2013'),
            'kir_rezervnyy_kapital_2014' => Yii::t('app', 'Kir Rezervnyy Kapital 2014'),
            'kir_rezervnyy_kapital_2015' => Yii::t('app', 'Kir Rezervnyy Kapital 2015'),
            'kir_neraspredelennaya_pribyl_2016' => Yii::t('app', 'Kir Neraspredelennaya Pribyl 2016'),
            'kir_neraspredelennaya_pribyl_2012' => Yii::t('app', 'Kir Neraspredelennaya Pribyl 2012'),
            'kir_neraspredelennaya_pribyl_2013' => Yii::t('app', 'Kir Neraspredelennaya Pribyl 2013'),
            'kir_neraspredelennaya_pribyl_2014' => Yii::t('app', 'Kir Neraspredelennaya Pribyl 2014'),
            'kir_neraspredelennaya_pribyl_2015' => Yii::t('app', 'Kir Neraspredelennaya Pribyl 2015'),
            'kir_itogo_po_razdelu_3_2016' => Yii::t('app', 'Kir Itogo Po Razdelu 3 2016'),
            'kir_itogo_po_razdelu_3_2012' => Yii::t('app', 'Kir Itogo Po Razdelu 3 2012'),
            'kir_itogo_po_razdelu_3_2013' => Yii::t('app', 'Kir Itogo Po Razdelu 3 2013'),
            'kir_itogo_po_razdelu_3_2014' => Yii::t('app', 'Kir Itogo Po Razdelu 3 2014'),
            'kir_itogo_po_razdelu_3_2015' => Yii::t('app', 'Kir Itogo Po Razdelu 3 2015'),
            'do_zayemnyye_sredstva_2016' => Yii::t('app', 'Do Zayemnyye Sredstva 2016'),
            'do_zayemnyye_sredstva_2012' => Yii::t('app', 'Do Zayemnyye Sredstva 2012'),
            'do_zayemnyye_sredstva_2013' => Yii::t('app', 'Do Zayemnyye Sredstva 2013'),
            'do_zayemnyye_sredstva_2014' => Yii::t('app', 'Do Zayemnyye Sredstva 2014'),
            'do_zayemnyye_sredstva_2015' => Yii::t('app', 'Do Zayemnyye Sredstva 2015'),
            'do_otlozhennyye_nalogovyye_obyazatelstva_2016' => Yii::t('app',
                'Do Otlozhennyye Nalogovyye Obyazatelstva 2016'),
            'do_otlozhennyye_nalogovyye_obyazatelstva_2012' => Yii::t('app',
                'Do Otlozhennyye Nalogovyye Obyazatelstva 2012'),
            'do_otlozhennyye_nalogovyye_obyazatelstva_2013' => Yii::t('app',
                'Do Otlozhennyye Nalogovyye Obyazatelstva 2013'),
            'do_otlozhennyye_nalogovyye_obyazatelstva_2014' => Yii::t('app',
                'Do Otlozhennyye Nalogovyye Obyazatelstva 2014'),
            'do_otlozhennyye_nalogovyye_obyazatelstva_2015' => Yii::t('app',
                'Do Otlozhennyye Nalogovyye Obyazatelstva 2015'),
            'do_prochiye_obyazatelstva_2016' => Yii::t('app', 'Do Prochiye Obyazatelstva 2016'),
            'do_prochiye_obyazatelstva_2012' => Yii::t('app', 'Do Prochiye Obyazatelstva 2012'),
            'do_prochiye_obyazatelstva_2013' => Yii::t('app', 'Do Prochiye Obyazatelstva 2013'),
            'do_prochiye_obyazatelstva_2014' => Yii::t('app', 'Do Prochiye Obyazatelstva 2014'),
            'do_prochiye_obyazatelstva_2015' => Yii::t('app', 'Do Prochiye Obyazatelstva 2015'),
            'do_itogo_po_razdelu_4_2016' => Yii::t('app', 'Do Itogo Po Razdelu 4 2016'),
            'do_itogo_po_razdelu_4_2012' => Yii::t('app', 'Do Itogo Po Razdelu 4 2012'),
            'do_itogo_po_razdelu_4_2013' => Yii::t('app', 'Do Itogo Po Razdelu 4 2013'),
            'do_itogo_po_razdelu_4_2014' => Yii::t('app', 'Do Itogo Po Razdelu 4 2014'),
            'do_itogo_po_razdelu_4_2015' => Yii::t('app', 'Do Itogo Po Razdelu 4 2015'),
            'ko_zayemnyye_sredstva_2016' => Yii::t('app', 'Ko Zayemnyye Sredstva 2016'),
            'ko_zayemnyye_sredstva_2012' => Yii::t('app', 'Ko Zayemnyye Sredstva 2012'),
            'ko_zayemnyye_sredstva_2013' => Yii::t('app', 'Ko Zayemnyye Sredstva 2013'),
            'ko_zayemnyye_sredstva_2014' => Yii::t('app', 'Ko Zayemnyye Sredstva 2014'),
            'ko_zayemnyye_sredstva_2015' => Yii::t('app', 'Ko Zayemnyye Sredstva 2015'),
            'ko_kreditorskaya_zadolzhennost_2016' => Yii::t('app', 'Ko Kreditorskaya Zadolzhennost 2016'),
            'ko_kreditorskaya_zadolzhennost_2012' => Yii::t('app', 'Ko Kreditorskaya Zadolzhennost 2012'),
            'ko_kreditorskaya_zadolzhennost_2013' => Yii::t('app', 'Ko Kreditorskaya Zadolzhennost 2013'),
            'ko_kreditorskaya_zadolzhennost_2014' => Yii::t('app', 'Ko Kreditorskaya Zadolzhennost 2014'),
            'ko_kreditorskaya_zadolzhennost_2015' => Yii::t('app', 'Ko Kreditorskaya Zadolzhennost 2015'),
            'ko_dokhody_budushchikh_periodov_2016' => Yii::t('app', 'Ko Dokhody Budushchikh Periodov 2016'),
            'ko_dokhody_budushchikh_periodov_2012' => Yii::t('app', 'Ko Dokhody Budushchikh Periodov 2012'),
            'ko_dokhody_budushchikh_periodov_2013' => Yii::t('app', 'Ko Dokhody Budushchikh Periodov 2013'),
            'ko_dokhody_budushchikh_periodov_2014' => Yii::t('app', 'Ko Dokhody Budushchikh Periodov 2014'),
            'ko_dokhody_budushchikh_periodov_2015' => Yii::t('app', 'Ko Dokhody Budushchikh Periodov 2015'),
            'ko_prochiye_obyazatelstva_2016' => Yii::t('app', 'Ko Prochiye Obyazatelstva 2016'),
            'ko_prochiye_obyazatelstva_2012' => Yii::t('app', 'Ko Prochiye Obyazatelstva 2012'),
            'ko_prochiye_obyazatelstva_2013' => Yii::t('app', 'Ko Prochiye Obyazatelstva 2013'),
            'ko_prochiye_obyazatelstva_2014' => Yii::t('app', 'Ko Prochiye Obyazatelstva 2014'),
            'ko_prochiye_obyazatelstva_2015' => Yii::t('app', 'Ko Prochiye Obyazatelstva 2015'),
            'ko_itogo_po_razdelu_5_2016' => Yii::t('app', 'Ko Itogo Po Razdelu 5 2016'),
            'ko_itogo_po_razdelu_5_2012' => Yii::t('app', 'Ko Itogo Po Razdelu 5 2012'),
            'ko_itogo_po_razdelu_5_2013' => Yii::t('app', 'Ko Itogo Po Razdelu 5 2013'),
            'ko_itogo_po_razdelu_5_2014' => Yii::t('app', 'Ko Itogo Po Razdelu 5 2014'),
            'ko_itogo_po_razdelu_5_2015' => Yii::t('app', 'Ko Itogo Po Razdelu 5 2015'),
            'ko_balans_2016' => Yii::t('app', 'Ko Balans 2016'),
            'ko_balans_2012' => Yii::t('app', 'Ko Balans 2012'),
            'ko_balans_2013' => Yii::t('app', 'Ko Balans 2013'),
            'ko_balans_2014' => Yii::t('app', 'Ko Balans 2014'),
            'ko_balans_2015' => Yii::t('app', 'Ko Balans 2015'),
            'dirpovd_vyruchka_2012' => Yii::t('app', 'Dirpovd Vyruchka 2012'),
            'dirpovd_vyruchka_2013' => Yii::t('app', 'Dirpovd Vyruchka 2013'),
            'dirpovd_vyruchka_2014' => Yii::t('app', 'Dirpovd Vyruchka 2014'),
            'dirpovd_vyruchka_2015' => Yii::t('app', 'Dirpovd Vyruchka 2015'),
            'dirpovd_vyruchka_2016' => Yii::t('app', 'Dirpovd Vyruchka 2016'),
            'dirpovd_sebestoimost_prodazh_2012' => Yii::t('app', 'Dirpovd Sebestoimost Prodazh 2012'),
            'dirpovd_sebestoimost_prodazh_2013' => Yii::t('app', 'Dirpovd Sebestoimost Prodazh 2013'),
            'dirpovd_sebestoimost_prodazh_2014' => Yii::t('app', 'Dirpovd Sebestoimost Prodazh 2014'),
            'dirpovd_sebestoimost_prodazh_2015' => Yii::t('app', 'Dirpovd Sebestoimost Prodazh 2015'),
            'dirpovd_sebestoimost_prodazh_2016' => Yii::t('app', 'Dirpovd Sebestoimost Prodazh 2016'),
            'dirpovd_valovaya_pribyl_2012' => Yii::t('app', 'Dirpovd Valovaya Pribyl 2012'),
            'dirpovd_valovaya_pribyl_2013' => Yii::t('app', 'Dirpovd Valovaya Pribyl 2013'),
            'dirpovd_valovaya_pribyl_2014' => Yii::t('app', 'Dirpovd Valovaya Pribyl 2014'),
            'dirpovd_valovaya_pribyl_2015' => Yii::t('app', 'Dirpovd Valovaya Pribyl 2015'),
            'dirpovd_valovaya_pribyl_2016' => Yii::t('app', 'Dirpovd Valovaya Pribyl 2016'),
            'dirpovd_kommercheskiye_raskhody_2012' => Yii::t('app', 'Dirpovd Kommercheskiye Raskhody 2012'),
            'dirpovd_kommercheskiye_raskhody_2013' => Yii::t('app', 'Dirpovd Kommercheskiye Raskhody 2013'),
            'dirpovd_kommercheskiye_raskhody_2014' => Yii::t('app', 'Dirpovd Kommercheskiye Raskhody 2014'),
            'dirpovd_kommercheskiye_raskhody_2015' => Yii::t('app', 'Dirpovd Kommercheskiye Raskhody 2015'),
            'dirpovd_kommercheskiye_raskhody_2016' => Yii::t('app', 'Dirpovd Kommercheskiye Raskhody 2016'),
            'dirpovd_upravlencheskiye_raskhody_2012' => Yii::t('app', 'Dirpovd Upravlencheskiye Raskhody 2012'),
            'dirpovd_upravlencheskiye_raskhody_2013' => Yii::t('app', 'Dirpovd Upravlencheskiye Raskhody 2013'),
            'dirpovd_upravlencheskiye_raskhody_2014' => Yii::t('app', 'Dirpovd Upravlencheskiye Raskhody 2014'),
            'dirpovd_upravlencheskiye_raskhody_2015' => Yii::t('app', 'Dirpovd Upravlencheskiye Raskhody 2015'),
            'dirpovd_upravlencheskiye_raskhody_2016' => Yii::t('app', 'Dirpovd Upravlencheskiye Raskhody 2016'),
            'dirpovd_pribyl_ot_prodazh_2012' => Yii::t('app', 'Dirpovd Pribyl Ot Prodazh 2012'),
            'dirpovd_pribyl_ot_prodazh_2013' => Yii::t('app', 'Dirpovd Pribyl Ot Prodazh 2013'),
            'dirpovd_pribyl_ot_prodazh_2014' => Yii::t('app', 'Dirpovd Pribyl Ot Prodazh 2014'),
            'dirpovd_pribyl_ot_prodazh_2015' => Yii::t('app', 'Dirpovd Pribyl Ot Prodazh 2015'),
            'dirpovd_pribyl_ot_prodazh_2016' => Yii::t('app', 'Dirpovd Pribyl Ot Prodazh 2016'),
            'pdir_protsenty_k_polucheniyu_2012' => Yii::t('app', 'Pdir Protsenty K Polucheniyu 2012'),
            'pdir_protsenty_k_polucheniyu_2013' => Yii::t('app', 'Pdir Protsenty K Polucheniyu 2013'),
            'pdir_protsenty_k_polucheniyu_2014' => Yii::t('app', 'Pdir Protsenty K Polucheniyu 2014'),
            'pdir_protsenty_k_polucheniyu_2015' => Yii::t('app', 'Pdir Protsenty K Polucheniyu 2015'),
            'pdir_protsenty_k_polucheniyu_2016' => Yii::t('app', 'Pdir Protsenty K Polucheniyu 2016'),
            'pdir_protsenty_k_uplate_2012' => Yii::t('app', 'Pdir Protsenty K Uplate 2012'),
            'pdir_protsenty_k_uplate_2013' => Yii::t('app', 'Pdir Protsenty K Uplate 2013'),
            'pdir_protsenty_k_uplate_2014' => Yii::t('app', 'Pdir Protsenty K Uplate 2014'),
            'pdir_protsenty_k_uplate_2015' => Yii::t('app', 'Pdir Protsenty K Uplate 2015'),
            'pdir_protsenty_k_uplate_2016' => Yii::t('app', 'Pdir Protsenty K Uplate 2016'),
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2012' => Yii::t('app',
                'Pdir Dokhody Ot Uchastiya V Drugikh Organizatsiyakh 2012'),
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2013' => Yii::t('app',
                'Pdir Dokhody Ot Uchastiya V Drugikh Organizatsiyakh 2013'),
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2014' => Yii::t('app',
                'Pdir Dokhody Ot Uchastiya V Drugikh Organizatsiyakh 2014'),
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2015' => Yii::t('app',
                'Pdir Dokhody Ot Uchastiya V Drugikh Organizatsiyakh 2015'),
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2016' => Yii::t('app',
                'Pdir Dokhody Ot Uchastiya V Drugikh Organizatsiyakh 2016'),
            'pdir_prochiye_dokhody_2012' => Yii::t('app', 'Pdir Prochiye Dokhody 2012'),
            'pdir_prochiye_dokhody_2013' => Yii::t('app', 'Pdir Prochiye Dokhody 2013'),
            'pdir_prochiye_dokhody_2014' => Yii::t('app', 'Pdir Prochiye Dokhody 2014'),
            'pdir_prochiye_dokhody_2015' => Yii::t('app', 'Pdir Prochiye Dokhody 2015'),
            'pdir_prochiye_dokhody_2016' => Yii::t('app', 'Pdir Prochiye Dokhody 2016'),
            'pdir_prochiye_raskhody_2012' => Yii::t('app', 'Pdir Prochiye Raskhody 2012'),
            'pdir_prochiye_raskhody_2013' => Yii::t('app', 'Pdir Prochiye Raskhody 2013'),
            'pdir_prochiye_raskhody_2014' => Yii::t('app', 'Pdir Prochiye Raskhody 2014'),
            'pdir_prochiye_raskhody_2015' => Yii::t('app', 'Pdir Prochiye Raskhody 2015'),
            'pdir_prochiye_raskhody_2016' => Yii::t('app', 'Pdir Prochiye Raskhody 2016'),
            'pdir_pribyl_do_nalogooblozheniya_2012' => Yii::t('app', 'Pdir Pribyl Do Nalogooblozheniya 2012'),
            'pdir_pribyl_do_nalogooblozheniya_2013' => Yii::t('app', 'Pdir Pribyl Do Nalogooblozheniya 2013'),
            'pdir_pribyl_do_nalogooblozheniya_2014' => Yii::t('app', 'Pdir Pribyl Do Nalogooblozheniya 2014'),
            'pdir_pribyl_do_nalogooblozheniya_2015' => Yii::t('app', 'Pdir Pribyl Do Nalogooblozheniya 2015'),
            'pdir_pribyl_do_nalogooblozheniya_2016' => Yii::t('app', 'Pdir Pribyl Do Nalogooblozheniya 2016'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2012' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Aktivov 2012'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2013' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Aktivov 2013'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2014' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Aktivov 2014'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2015' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Aktivov 2015'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2016' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Aktivov 2016'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2012' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Obyazatelstv 2012'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2013' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Obyazatelstv 2013'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2014' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Obyazatelstv 2014'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2015' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Obyazatelstv 2015'),
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2016' => Yii::t('app',
                'Pdir Izmeneniye Otlozhennykh Nalogovykh Obyazatelstv 2016'),
            'pdir_tekushchiy_nalog_na_pribyl_2012' => Yii::t('app', 'Pdir Tekushchiy Nalog Na Pribyl 2012'),
            'pdir_tekushchiy_nalog_na_pribyl_2013' => Yii::t('app', 'Pdir Tekushchiy Nalog Na Pribyl 2013'),
            'pdir_tekushchiy_nalog_na_pribyl_2014' => Yii::t('app', 'Pdir Tekushchiy Nalog Na Pribyl 2014'),
            'pdir_tekushchiy_nalog_na_pribyl_2015' => Yii::t('app', 'Pdir Tekushchiy Nalog Na Pribyl 2015'),
            'pdir_tekushchiy_nalog_na_pribyl_2016' => Yii::t('app', 'Pdir Tekushchiy Nalog Na Pribyl 2016'),
            'pdir_chistaya_pribyl_2012' => Yii::t('app', 'Pdir Chistaya Pribyl 2012'),
            'pdir_chistaya_pribyl_2013' => Yii::t('app', 'Pdir Chistaya Pribyl 2013'),
            'pdir_chistaya_pribyl_2014' => Yii::t('app', 'Pdir Chistaya Pribyl 2014'),
            'pdir_chistaya_pribyl_2015' => Yii::t('app', 'Pdir Chistaya Pribyl 2015'),
            'pdir_chistaya_pribyl_2016' => Yii::t('app', 'Pdir Chistaya Pribyl 2016'),
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2012' => Yii::t('app',
                'Pdir Postoyannyye Nalogovyye Obyazatelstva 2012'),
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2013' => Yii::t('app',
                'Pdir Postoyannyye Nalogovyye Obyazatelstva 2013'),
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2014' => Yii::t('app',
                'Pdir Postoyannyye Nalogovyye Obyazatelstva 2014'),
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2015' => Yii::t('app',
                'Pdir Postoyannyye Nalogovyye Obyazatelstva 2015'),
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2016' => Yii::t('app',
                'Pdir Postoyannyye Nalogovyye Obyazatelstva 2016'),
            'va_prochiye_vneoborotnyye_aktivy_2012' => Yii::t('app', 'Va Prochiye Vneoborotnyye Aktivy 2012'),
            'va_prochiye_vneoborotnyye_aktivy_2013' => Yii::t('app', 'Va Prochiye Vneoborotnyye Aktivy 2013'),
            'va_prochiye_vneoborotnyye_aktivy_2014' => Yii::t('app', 'Va Prochiye Vneoborotnyye Aktivy 2014'),
            'va_prochiye_vneoborotnyye_aktivy_2015' => Yii::t('app', 'Va Prochiye Vneoborotnyye Aktivy 2015'),
            'va_prochiye_vneoborotnyye_aktivy_2016' => Yii::t('app', 'Va Prochiye Vneoborotnyye Aktivy 2016'),
        ];
    }

    public static function getFinancialStatement($domain, $apiKey, $searchString)
    {
        $url = 'fs';
        $client = new Client(['baseUrl' => $domain]);
        $response = $client->post($url, ['api_key' => $apiKey, 'id' => $searchString])->send();
        if ($response->isOk) {
            return $response;
        }
        return 0;
    }

    public static function financialStatementWithZcb($searchString)
    {
        //Отправляем запрос на ЗЧБ
        $response = self::getFinancialStatement(CompanySearch::DOMAIN, CompanySearch::API_KEY, $searchString);

        //var_dump($response->data);
        //echo PHP_EOL.PHP_EOL.PHP_EOL;

        //Сохраняем ответ в таблицу FinancialStatement
        $model = new FinancialStatement();
        $model->request_string = $searchString;
        $model->response = json_encode($response->data);
        $model->save();

        $tempVa = $response->data['Бухгалтерский баланс']["Актив"]["I. ВНЕОБОРОТНЫЕ АКТИВЫ"];
        $tempOa = $response->data['Бухгалтерский баланс']["Актив"]["II. ОБОРОТНЫЕ АКТИВЫ"];
        $tempKir = $response->data['Бухгалтерский баланс']["Пассив"]["III. КАПИТАЛ И РЕЗЕРВЫ"];
        $tempDo = $response->data['Бухгалтерский баланс']["Пассив"]["IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА"];
        $tempKo = $response->data['Бухгалтерский баланс']["Пассив"]["V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА"];

        $tempDirpovd = $response->data['Отчет о прибылях и убытках']["Доходы и расходы по обычным видам деятельности"];
        $tempPdir = $response->data['Отчет о прибылях и убытках']["Прочие доходы и расходы"];


        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Nematerialnyye aktivy - 2012
        $model->va_nematerialnyye_aktivy_2012 = ArrayHelper::getValue($tempVa, 'Нематериальные активы.2012');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Nematerialnyye aktivy - 2013
        $model->va_nematerialnyye_aktivy_2013 = ArrayHelper::getValue($tempVa, 'Нематериальные активы.2013');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Nematerialnyye aktivy - 2014
        $model->va_nematerialnyye_aktivy_2014 = ArrayHelper::getValue($tempVa, 'Нематериальные активы.2014');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Nematerialnyye aktivy - 2015
        $model->va_nematerialnyye_aktivy_2015 = ArrayHelper::getValue($tempVa, 'Нематериальные активы.2015');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Nematerialnyye aktivy - 2016
        $model->va_nematerialnyye_aktivy_2016 = ArrayHelper::getValue($tempVa, 'Нематериальные активы.2016');

        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Osnovnyye sredstva - 2012
        $model->va_osnovnyye_sredstva_2012 = ArrayHelper::getValue($tempVa, 'Основные средства.2012');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Osnovnyye sredstva - 2013
        $model->va_osnovnyye_sredstva_2013 = ArrayHelper::getValue($tempVa, 'Основные средства.2013');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Osnovnyye sredstva - 2014
        $model->va_osnovnyye_sredstva_2014 = ArrayHelper::getValue($tempVa, 'Основные средства.2014');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Osnovnyye sredstva - 2015
        $model->va_osnovnyye_sredstva_2015 = ArrayHelper::getValue($tempVa, 'Основные средства.2015');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Osnovnyye sredstva - 2016
        $model->va_osnovnyye_sredstva_2016 = ArrayHelper::getValue($tempVa, 'Основные средства.2016');

        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Dokhodnyye vlozheniya v materialnyye tsennosti - 2012
        $model->va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2012 = ArrayHelper::getValue($tempVa,
            'Доходные вложения в материальные ценности.2012');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Dokhodnyye vlozheniya v materialnyye tsennosti - 2013
        $model->va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2013 = ArrayHelper::getValue($tempVa,
            'Доходные вложения в материальные ценности.2013');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Dokhodnyye vlozheniya v materialnyye tsennosti - 2014
        $model->va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2014 = ArrayHelper::getValue($tempVa,
            'Доходные вложения в материальные ценности.2014');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Dokhodnyye vlozheniya v materialnyye tsennosti - 2015
        $model->va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2015 = ArrayHelper::getValue($tempVa,
            'Доходные вложения в материальные ценности.2015');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Dokhodnyye vlozheniya v materialnyye tsennosti - 2016
        $model->va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2016 = ArrayHelper::getValue($tempVa,
            'Доходные вложения в материальные ценности.2016');

        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Finansovyye vlozheniya - 2012
        $model->va_finansovyye_vlozheniya_2012 = ArrayHelper::getValue($tempVa,
            'Финансовые вложения.2012');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Finansovyye vlozheniya - 2013
        $model->va_finansovyye_vlozheniya_2013 = ArrayHelper::getValue($tempVa,
            'Финансовые вложения.2013');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Finansovyye vlozheniya - 2014
        $model->va_finansovyye_vlozheniya_2014 = ArrayHelper::getValue($tempVa,
            'Финансовые вложения.2014');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Finansovyye vlozheniya - 2015
        $model->va_finansovyye_vlozheniya_2015 = ArrayHelper::getValue($tempVa,
            'Финансовые вложения.2015');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Finansovyye vlozheniya - 2016
        $model->va_finansovyye_vlozheniya_2016 = ArrayHelper::getValue($tempVa,
            'Финансовые вложения.2016');

        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Otlozhennyye nalogovyye aktivy - 2012
        $model->va_otlozhennyye_nalogovyye_aktivy_2012 = ArrayHelper::getValue($tempVa,
            'Отложенные налоговые активы.2012');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Otlozhennyye nalogovyye aktivy - 2013
        $model->va_otlozhennyye_nalogovyye_aktivy_2013 = ArrayHelper::getValue($tempVa,
            'Отложенные налоговые активы.2013');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Otlozhennyye nalogovyye aktivy - 2014
        $model->va_otlozhennyye_nalogovyye_aktivy_2014 = ArrayHelper::getValue($tempVa,
            'Отложенные налоговые активы.2014');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Otlozhennyye nalogovyye aktivy - 2015
        $model->va_otlozhennyye_nalogovyye_aktivy_2015 = ArrayHelper::getValue($tempVa,
            'Отложенные налоговые активы.2015');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Otlozhennyye nalogovyye aktivy - 2016
        $model->va_otlozhennyye_nalogovyye_aktivy_2016 = ArrayHelper::getValue($tempVa,
            'Отложенные налоговые активы.2016');

        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Prochiye vneoborotnyye aktivy - 2012
        $model->va_prochiye_vneoborotnyye_aktivy_2012 = ArrayHelper::getValue($tempVa,
            'Прочие внеоборотные активы.2012');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Prochiye vneoborotnyye aktivy - 2013
        $model->va_prochiye_vneoborotnyye_aktivy_2013 = ArrayHelper::getValue($tempVa,
            'Прочие внеоборотные активы.2013');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Prochiye vneoborotnyye aktivy - 2014
        $model->va_prochiye_vneoborotnyye_aktivy_2014 = ArrayHelper::getValue($tempVa,
            'Прочие внеоборотные активы.2014');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Prochiye vneoborotnyye aktivy - 2015
        $model->va_prochiye_vneoborotnyye_aktivy_2015 = ArrayHelper::getValue($tempVa,
            'Прочие внеоборотные активы.2015');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Prochiye vneoborotnyye aktivy - 2016
        $model->va_prochiye_vneoborotnyye_aktivy_2016 = ArrayHelper::getValue($tempVa,
            'Прочие внеоборотные активы.2016');

        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Itogo po razdelu 1 - 2012
        $model->va_itogo_po_razdelu_1_2012 = ArrayHelper::getValue($tempVa,
            'ИТОГО по разделу I.2012');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Itogo po razdelu 1 - 2013
        $model->va_itogo_po_razdelu_1_2013 = ArrayHelper::getValue($tempVa,
            'ИТОГО по разделу I.2013');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Itogo po razdelu 1 - 2014
        $model->va_itogo_po_razdelu_1_2014 = ArrayHelper::getValue($tempVa,
            'ИТОГО по разделу I.2014');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Itogo po razdelu 1 - 2015
        $model->va_itogo_po_razdelu_1_2015 = ArrayHelper::getValue($tempVa,
            'ИТОГО по разделу I.2015');
        //Bukhgalterskiy balans - Aktiv - 1Vneoborotnyye aktivy - Itogo po razdelu 1 - 2016
        $model->va_itogo_po_razdelu_1_2016 = ArrayHelper::getValue($tempVa,
            'ИТОГО по разделу I.2016');

//---------------------------------------
//---------------------------------------
//---------------------------------------
//---------------------------------------
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Zapasy - 2012
        $model->oa_zapasy_2012 = ArrayHelper::getValue($tempOa,
            'Запасы.2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Zapasy - 2013
        $model->oa_zapasy_2013 = ArrayHelper::getValue($tempOa,
            'Запасы.2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Zapasy - 2014
        $model->oa_zapasy_2014 = ArrayHelper::getValue($tempOa,
            'Запасы.2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Zapasy - 2015
        $model->oa_zapasy_2015 = ArrayHelper::getValue($tempOa,
            'Запасы.2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Zapasy - 2016
        $model->oa_zapasy_2016 = ArrayHelper::getValue($tempOa,
            'Запасы.2016');

        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Nalog na dobavlennuyu stoimost po priobretennym tsennostyam - 2012
        $model->oa_nds_po_priobretennym_tsennostyam_2012 = ArrayHelper::getValue($tempOa,
            'Налог на добавленную стоимость по приобретенным ценностям.2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Nalog na dobavlennuyu stoimost po priobretennym tsennostyam - 2013
        $model->oa_nds_po_priobretennym_tsennostyam_2013 = ArrayHelper::getValue($tempOa,
            'Налог на добавленную стоимость по приобретенным ценностям.2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Nalog na dobavlennuyu stoimost po priobretennym tsennostyam - 2014
        $model->oa_nds_po_priobretennym_tsennostyam_2014 = ArrayHelper::getValue($tempOa,
            'Налог на добавленную стоимость по приобретенным ценностям.2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Nalog na dobavlennuyu stoimost po priobretennym tsennostyam - 2015
        $model->oa_nds_po_priobretennym_tsennostyam_2015 = ArrayHelper::getValue($tempOa,
            'Налог на добавленную стоимость по приобретенным ценностям.2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Nalog na dobavlennuyu stoimost po priobretennym tsennostyam - 2016
        $model->oa_nds_po_priobretennym_tsennostyam_2016 = ArrayHelper::getValue($tempOa,
            'Налог на добавленную стоимость по приобретенным ценностям.2016');

        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Debitorskaya zadolzhennost - 2012
        $model->oa_debitorskaya_zadolzhennost_2012 = ArrayHelper::getValue($tempOa,
            'Дебиторская задолженность.2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Debitorskaya zadolzhennost - 2013
        $model->oa_debitorskaya_zadolzhennost_2013 = ArrayHelper::getValue($tempOa,
            'Дебиторская задолженность.2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Debitorskaya zadolzhennost - 2014
        $model->oa_debitorskaya_zadolzhennost_2014 = ArrayHelper::getValue($tempOa,
            'Дебиторская задолженность.2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Debitorskaya zadolzhennost - 2015
        $model->oa_debitorskaya_zadolzhennost_2015 = ArrayHelper::getValue($tempOa,
            'Дебиторская задолженность.2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Debitorskaya zadolzhennost - 2016
        $model->oa_debitorskaya_zadolzhennost_2016 = ArrayHelper::getValue($tempOa,
            'Дебиторская задолженность.2016');

        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Finansovyye vlozheniya - 2012
        $model->oa_finansovyye_vlozheniya_2012 = ArrayHelper::getValue($tempOa,
            'Финансовые вложения (за исключением денежных эквивалентов).2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Finansovyye vlozheniya - 2013
        $model->oa_finansovyye_vlozheniya_2013 = ArrayHelper::getValue($tempOa,
            'Финансовые вложения (за исключением денежных эквивалентов).2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Finansovyye vlozheniya - 2014
        $model->oa_finansovyye_vlozheniya_2014 = ArrayHelper::getValue($tempOa,
            'Финансовые вложения (за исключением денежных эквивалентов).2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Finansovyye vlozheniya - 2015
        $model->oa_finansovyye_vlozheniya_2015 = ArrayHelper::getValue($tempOa,
            'Финансовые вложения (за исключением денежных эквивалентов).2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Finansovyye vlozheniya - 2016
        $model->oa_finansovyye_vlozheniya_2016 = ArrayHelper::getValue($tempOa,
            'Финансовые вложения (за исключением денежных эквивалентов).2016');

        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Denezhnyye sredstva i denezhnyye ekvivalenty - 2012
        $model->oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2012 = ArrayHelper::getValue($tempOa,
            'Денежные средства и денежные эквиваленты.2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Denezhnyye sredstva i denezhnyye ekvivalenty - 2013
        $model->oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2013 = ArrayHelper::getValue($tempOa,
            'Денежные средства и денежные эквиваленты.2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Denezhnyye sredstva i denezhnyye ekvivalenty - 2014
        $model->oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2014 = ArrayHelper::getValue($tempOa,
            'Денежные средства и денежные эквиваленты.2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Denezhnyye sredstva i denezhnyye ekvivalenty - 2015
        $model->oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2015 = ArrayHelper::getValue($tempOa,
            'Денежные средства и денежные эквиваленты.2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Denezhnyye sredstva i denezhnyye ekvivalenty - 2016
        $model->oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2016 = ArrayHelper::getValue($tempOa,
            'Денежные средства и денежные эквиваленты.2016');

        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Prochiye oborotnyye aktivy - 2012
        $model->oa_prochiye_oborotnyye_aktivy_2012 = ArrayHelper::getValue($tempOa,
            'Прочие оборотные активы.2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Prochiye oborotnyye aktivy - 2013
        $model->oa_prochiye_oborotnyye_aktivy_2013 = ArrayHelper::getValue($tempOa,
            'Прочие оборотные активы.2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Prochiye oborotnyye aktivy - 2014
        $model->oa_prochiye_oborotnyye_aktivy_2014 = ArrayHelper::getValue($tempOa,
            'Прочие оборотные активы.2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Prochiye oborotnyye aktivy - 2015
        $model->oa_prochiye_oborotnyye_aktivy_2015 = ArrayHelper::getValue($tempOa,
            'Прочие оборотные активы.2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Prochiye oborotnyye aktivy - 2016
        $model->oa_prochiye_oborotnyye_aktivy_2016 = ArrayHelper::getValue($tempOa,
            'Прочие оборотные активы.2016');

        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Itogo po razdelu 2 - 2012
        $model->oa_itogo_po_razdelu_2_2012 = ArrayHelper::getValue($tempOa,
            'ИТОГО по разделу II.2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Itogo po razdelu 2 - 2013
        $model->oa_itogo_po_razdelu_2_2013 = ArrayHelper::getValue($tempOa,
            'ИТОГО по разделу II.2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Itogo po razdelu 2 - 2014
        $model->oa_itogo_po_razdelu_2_2014 = ArrayHelper::getValue($tempOa,
            'ИТОГО по разделу II.2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Itogo po razdelu 2 - 2015
        $model->oa_itogo_po_razdelu_2_2015 = ArrayHelper::getValue($tempOa,
            'ИТОГО по разделу II.2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Itogo po razdelu 2 - 2016
        $model->oa_itogo_po_razdelu_2_2016 = ArrayHelper::getValue($tempOa,
            'ИТОГО по разделу II.2016');

        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Balans - 2012
        $model->oa_balans_2012 = ArrayHelper::getValue($tempOa,
            'БАЛАНС.2012');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Balans - 2013
        $model->oa_balans_2013 = ArrayHelper::getValue($tempOa,
            'БАЛАНС.2013');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Balans - 2014
        $model->oa_balans_2014 = ArrayHelper::getValue($tempOa,
            'БАЛАНС.2014');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Balans - 2015
        $model->oa_balans_2015 = ArrayHelper::getValue($tempOa,
            'БАЛАНС.2015');
        //Bukhgalterskiy balans - Aktiv - 2Oborotnyye aktivy - Balans - 2016
        $model->oa_balans_2016 = ArrayHelper::getValue($tempOa,
            'БАЛАНС.2016');

        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Ustavnyy kapital - 2012
        $model->kir_ustavnyy_kapital_2012 = ArrayHelper::getValue($tempKir,
            'Уставный капитал (складочный капитал, уставный фонд, вклады товарищей).2012');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Ustavnyy kapital - 2013
        $model->kir_ustavnyy_kapital_2013 = ArrayHelper::getValue($tempKir,
            'Уставный капитал (складочный капитал, уставный фонд, вклады товарищей).2013');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Ustavnyy kapital - 2014
        $model->kir_ustavnyy_kapital_2014 = ArrayHelper::getValue($tempKir,
            'Уставный капитал (складочный капитал, уставный фонд, вклады товарищей).2014');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Ustavnyy kapital - 2015
        $model->kir_ustavnyy_kapital_2015 = ArrayHelper::getValue($tempKir,
            'Уставный капитал (складочный капитал, уставный фонд, вклады товарищей).2015');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Ustavnyy kapital - 2016
        $model->kir_ustavnyy_kapital_2016 = ArrayHelper::getValue($tempKir,
            'Уставный капитал (складочный капитал, уставный фонд, вклады товарищей).2016');

        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Sobstvennyye aktsii, vykuplennyye u aktsionerov - 2012
        $model->kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2012 = ArrayHelper::getValue($tempKir,
            'Собственные акции, выкупленные у акционеров.2012');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Sobstvennyye aktsii, vykuplennyye u aktsionerov - 2013
        $model->kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2013 = ArrayHelper::getValue($tempKir,
            'Собственные акции, выкупленные у акционеров.2013');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Sobstvennyye aktsii, vykuplennyye u aktsionerov - 2014
        $model->kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2014 = ArrayHelper::getValue($tempKir,
            'Собственные акции, выкупленные у акционеров.2014');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Sobstvennyye aktsii, vykuplennyye u aktsionerov - 2015
        $model->kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2015 = ArrayHelper::getValue($tempKir,
            'Собственные акции, выкупленные у акционеров.2015');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Sobstvennyye aktsii, vykuplennyye u aktsionerov - 2016
        $model->kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2016 = ArrayHelper::getValue($tempKir,
            'Собственные акции, выкупленные у акционеров.2016');

        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Dobavochnyy kapital - 2012
        $model->kir_dobavochnyy_kapital_2012 = ArrayHelper::getValue($tempKir,
            'Добавочный капитал (без переоценки).2012');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Dobavochnyy kapital - 2013
        $model->kir_dobavochnyy_kapital_2013 = ArrayHelper::getValue($tempKir,
            'Добавочный капитал (без переоценки).2013');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Dobavochnyy kapital - 2014
        $model->kir_dobavochnyy_kapital_2014 = ArrayHelper::getValue($tempKir,
            'Добавочный капитал (без переоценки).2014');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Dobavochnyy kapital - 2015
        $model->kir_dobavochnyy_kapital_2015 = ArrayHelper::getValue($tempKir,
            'Добавочный капитал (без переоценки).2015');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Dobavochnyy kapital - 2016
        $model->kir_dobavochnyy_kapital_2016 = ArrayHelper::getValue($tempKir,
            'Добавочный капитал (без переоценки).2016');

        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Rezervnyy kapital - 2012
        $model->kir_rezervnyy_kapital_2012 = ArrayHelper::getValue($tempKir,
            'Резервный капитал.2012');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Rezervnyy kapital - 2013
        $model->kir_rezervnyy_kapital_2013 = ArrayHelper::getValue($tempKir,
            'Резервный капитал.2013');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Rezervnyy kapital - 2014
        $model->kir_rezervnyy_kapital_2014 = ArrayHelper::getValue($tempKir,
            'Резервный капитал.2014');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Rezervnyy kapital - 2015
        $model->kir_rezervnyy_kapital_2015 = ArrayHelper::getValue($tempKir,
            'Резервный капитал.2015');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Rezervnyy kapital - 2016
        $model->kir_rezervnyy_kapital_2016 = ArrayHelper::getValue($tempKir,
            'Резервный капитал.2016');

        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Neraspredelennaya pribyl - 2012
        $model->kir_neraspredelennaya_pribyl_2012 = ArrayHelper::getValue($tempKir,
            'Нераспределенная прибыль (непокрытый убыток).2012');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Neraspredelennaya pribyl - 2013
        $model->kir_neraspredelennaya_pribyl_2013 = ArrayHelper::getValue($tempKir,
            'Нераспределенная прибыль (непокрытый убыток).2013');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Neraspredelennaya pribyl - 2014
        $model->kir_neraspredelennaya_pribyl_2014 = ArrayHelper::getValue($tempKir,
            'Нераспределенная прибыль (непокрытый убыток).2014');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Neraspredelennaya pribyl - 2015
        $model->kir_neraspredelennaya_pribyl_2015 = ArrayHelper::getValue($tempKir,
            'Нераспределенная прибыль (непокрытый убыток).2015');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Neraspredelennaya pribyl - 2016
        $model->kir_neraspredelennaya_pribyl_2016 = ArrayHelper::getValue($tempKir,
            'Нераспределенная прибыль (непокрытый убыток).2016');

        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Itogo po razdelu 3 - 2012
        $model->kir_itogo_po_razdelu_3_2012 = ArrayHelper::getValue($tempKir,
            'ИТОГО по разделу III.2012');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Itogo po razdelu 3 - 2013
        $model->kir_itogo_po_razdelu_3_2013 = ArrayHelper::getValue($tempKir,
            'ИТОГО по разделу III.2013');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Itogo po razdelu 3 - 2014
        $model->kir_itogo_po_razdelu_3_2014 = ArrayHelper::getValue($tempKir,
            'ИТОГО по разделу III.2014');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Itogo po razdelu 3 - 2015
        $model->kir_itogo_po_razdelu_3_2015 = ArrayHelper::getValue($tempKir,
            'ИТОГО по разделу III.2015');
        //Bukhgalterskiy balans - Passiv - 3 Kapital i rezervy - Itogo po razdelu 3 - 2016
        $model->kir_itogo_po_razdelu_3_2016 = ArrayHelper::getValue($tempKir,
            'ИТОГО по разделу III.2016');

        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Zayemnyye sredstva - 2012
        $model->do_zayemnyye_sredstva_2012 = ArrayHelper::getValue($tempDo,
            'Заемные средства.2012');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Zayemnyye sredstva - 2013
        $model->do_zayemnyye_sredstva_2013 = ArrayHelper::getValue($tempDo,
            'Заемные средства.2013');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Zayemnyye sredstva - 2014
        $model->do_zayemnyye_sredstva_2014 = ArrayHelper::getValue($tempDo,
            'Заемные средства.2014');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Zayemnyye sredstva - 2015
        $model->do_zayemnyye_sredstva_2015 = ArrayHelper::getValue($tempDo,
            'Заемные средства.2015');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Zayemnyye sredstva - 2016
        $model->do_zayemnyye_sredstva_2016 = ArrayHelper::getValue($tempDo,
            'Заемные средства.2016');

        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Otlozhennyye nalogovyye obyazatelstva - 2012
        $model->do_otlozhennyye_nalogovyye_obyazatelstva_2012 = ArrayHelper::getValue($tempDo,
            'Отложенные налоговые обязательства.2012');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Otlozhennyye nalogovyye obyazatelstva - 2013
        $model->do_otlozhennyye_nalogovyye_obyazatelstva_2013 = ArrayHelper::getValue($tempDo,
            'Отложенные налоговые обязательства.2013');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Otlozhennyye nalogovyye obyazatelstva - 2014
        $model->do_otlozhennyye_nalogovyye_obyazatelstva_2014 = ArrayHelper::getValue($tempDo,
            'Отложенные налоговые обязательства.2014');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Otlozhennyye nalogovyye obyazatelstva - 2015
        $model->do_otlozhennyye_nalogovyye_obyazatelstva_2015 = ArrayHelper::getValue($tempDo,
            'Отложенные налоговые обязательства.2015');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Otlozhennyye nalogovyye obyazatelstva - 2016
        $model->do_otlozhennyye_nalogovyye_obyazatelstva_2016 = ArrayHelper::getValue($tempDo,
            'Отложенные налоговые обязательства.2016');

        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Prochiye obyazatelstva - 2012
        $model->do_prochiye_obyazatelstva_2012 = ArrayHelper::getValue($tempDo,
            'Прочие обязательства.2012');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Prochiye obyazatelstva - 2013
        $model->do_prochiye_obyazatelstva_2013 = ArrayHelper::getValue($tempDo,
            'Прочие обязательства.2013');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Prochiye obyazatelstva - 2014
        $model->do_prochiye_obyazatelstva_2014 = ArrayHelper::getValue($tempDo,
            'Прочие обязательства.2014');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Prochiye obyazatelstva - 2015
        $model->do_prochiye_obyazatelstva_2015 = ArrayHelper::getValue($tempDo,
            'Прочие обязательства.2015');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Prochiye obyazatelstva - 2016
        $model->do_prochiye_obyazatelstva_2016 = ArrayHelper::getValue($tempDo,
            'Прочие обязательства.2016');

        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Itogo po razdelu 4 - 2012
        $model->do_itogo_po_razdelu_4_2012 = ArrayHelper::getValue($tempDo,
            'ИТОГО по разделу IV.2012');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Itogo po razdelu 4 - 2013
        $model->do_itogo_po_razdelu_4_2013 = ArrayHelper::getValue($tempDo,
            'ИТОГО по разделу IV.2013');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Itogo po razdelu 4 - 2014
        $model->do_itogo_po_razdelu_4_2014 = ArrayHelper::getValue($tempDo,
            'ИТОГО по разделу IV.2014');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Itogo po razdelu 4 - 2015
        $model->do_itogo_po_razdelu_4_2015 = ArrayHelper::getValue($tempDo,
            'ИТОГО по разделу IV.2015');
        //Bukhgalterskiy balans - Passiv - 4 Dolgosrochnyye obyazatelstva - Itogo po razdelu 4 - 2016
        $model->do_itogo_po_razdelu_4_2016 = ArrayHelper::getValue($tempDo,
            'ИТОГО по разделу IV.2016');

        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Zayemnyye sredstva - 2012
        $model->ko_zayemnyye_sredstva_2012 = ArrayHelper::getValue($tempKo,
            'Заемные средства.2012');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Zayemnyye sredstva - 2013
        $model->ko_zayemnyye_sredstva_2013 = ArrayHelper::getValue($tempKo,
            'Заемные средства.2013');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Zayemnyye sredstva - 2014
        $model->ko_zayemnyye_sredstva_2014 = ArrayHelper::getValue($tempKo,
            'Заемные средства.2014');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Zayemnyye sredstva - 2015
        $model->ko_zayemnyye_sredstva_2015 = ArrayHelper::getValue($tempKo,
            'Заемные средства.2015');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Zayemnyye sredstva - 2016
        $model->ko_zayemnyye_sredstva_2016 = ArrayHelper::getValue($tempKo,
            'Заемные средства.2016');

        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Kreditorskaya zadolzhennost - 2012
        $model->ko_kreditorskaya_zadolzhennost_2012 = ArrayHelper::getValue($tempKo,
            'Кредиторская задолженность.2012');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Kreditorskaya zadolzhennost - 2013
        $model->ko_kreditorskaya_zadolzhennost_2013 = ArrayHelper::getValue($tempKo,
            'Кредиторская задолженность.2013');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Kreditorskaya zadolzhennost - 2014
        $model->ko_kreditorskaya_zadolzhennost_2014 = ArrayHelper::getValue($tempKo,
            'Кредиторская задолженность.2014');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Kreditorskaya zadolzhennost - 2015
        $model->ko_kreditorskaya_zadolzhennost_2015 = ArrayHelper::getValue($tempKo,
            'Кредиторская задолженность.2015');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Kreditorskaya zadolzhennost - 2016
        $model->ko_kreditorskaya_zadolzhennost_2016 = ArrayHelper::getValue($tempKo,
            'Кредиторская задолженность.2016');

        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Dokhody budushchikh periodov - 2012
        $model->ko_dokhody_budushchikh_periodov_2012 = ArrayHelper::getValue($tempKo,
            'Доходы будущих периодов.2012');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Dokhody budushchikh periodov - 2013
        $model->ko_dokhody_budushchikh_periodov_2013 = ArrayHelper::getValue($tempKo,
            'Доходы будущих периодов.2013');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Dokhody budushchikh periodov - 2014
        $model->ko_dokhody_budushchikh_periodov_2014 = ArrayHelper::getValue($tempKo,
            'Доходы будущих периодов.2014');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Dokhody budushchikh periodov - 2015
        $model->ko_dokhody_budushchikh_periodov_2015 = ArrayHelper::getValue($tempKo,
            'Доходы будущих периодов.2015');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Dokhody budushchikh periodov - 2016
        $model->ko_dokhody_budushchikh_periodov_2016 = ArrayHelper::getValue($tempKo,
            'Доходы будущих периодов.2016');

        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Prochiye obyazatelstva - 2012
        $model->ko_prochiye_obyazatelstva_2012 = ArrayHelper::getValue($tempKo,
            'Прочие обязательства.2012');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Prochiye obyazatelstva - 2013
        $model->ko_prochiye_obyazatelstva_2013 = ArrayHelper::getValue($tempKo,
            'Прочие обязательства.2013');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Prochiye obyazatelstva - 2014
        $model->ko_prochiye_obyazatelstva_2014 = ArrayHelper::getValue($tempKo,
            'Прочие обязательства.2014');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Prochiye obyazatelstva - 2015
        $model->ko_prochiye_obyazatelstva_2015 = ArrayHelper::getValue($tempKo,
            'Прочие обязательства.2015');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Prochiye obyazatelstva - 2016
        $model->ko_prochiye_obyazatelstva_2016 = ArrayHelper::getValue($tempKo,
            'Прочие обязательства.2016');

        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Itogo po razdelu 5 - 2012
        $model->ko_itogo_po_razdelu_5_2012 = ArrayHelper::getValue($tempKo,
            'ИТОГО по разделу V.2012');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Itogo po razdelu 5 - 2013
        $model->ko_itogo_po_razdelu_5_2013 = ArrayHelper::getValue($tempKo,
            'ИТОГО по разделу V.2013');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Itogo po razdelu 5 - 2014
        $model->ko_itogo_po_razdelu_5_2014 = ArrayHelper::getValue($tempKo,
            'ИТОГО по разделу V.2014');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Itogo po razdelu 5 - 2015
        $model->ko_itogo_po_razdelu_5_2015 = ArrayHelper::getValue($tempKo,
            'ИТОГО по разделу V.2015');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Itogo po razdelu 5 - 2016
        $model->ko_itogo_po_razdelu_5_2016 = ArrayHelper::getValue($tempKo,
            'ИТОГО по разделу V.2016');

        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Balans - 2012
        $model->ko_balans_2012 = ArrayHelper::getValue($tempKo,
            'БАЛАНС.2012');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Balans - 2013
        $model->ko_balans_2013 = ArrayHelper::getValue($tempKo,
            'БАЛАНС.2013');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Balans - 2014
        $model->ko_balans_2014 = ArrayHelper::getValue($tempKo,
            'БАЛАНС.2014');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Balans - 2015
        $model->ko_balans_2015 = ArrayHelper::getValue($tempKo,
            'БАЛАНС.2015');
        //Bukhgalterskiy balans - Passiv - 5 Kratkosrochnyye obyazatelstva - Balans - 2016
        $model->ko_balans_2016 = ArrayHelper::getValue($tempKo,
            'БАЛАНС.2016');

        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Vyruchka - 2012
        $model->dirpovd_vyruchka_2012 = ArrayHelper::getValue($tempDirpovd,
            'Выручка.2012');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Vyruchka - 2013
        $model->dirpovd_vyruchka_2013 = ArrayHelper::getValue($tempDirpovd,
            'Выручка.2013');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Vyruchka - 2014
        $model->dirpovd_vyruchka_2014 = ArrayHelper::getValue($tempDirpovd,
            'Выручка.2014');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Vyruchka - 2015
        $model->dirpovd_vyruchka_2015 = ArrayHelper::getValue($tempDirpovd,
            'Выручка.2015');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Vyruchka - 2016
        $model->dirpovd_vyruchka_2016 = ArrayHelper::getValue($tempDirpovd,
            'Выручка.2016');

        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Sebestoimost prodazh - 2012
        $model->dirpovd_sebestoimost_prodazh_2012 = ArrayHelper::getValue($tempDirpovd,
            'Себестоимость продаж.2012');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Sebestoimost prodazh - 2013
        $model->dirpovd_sebestoimost_prodazh_2013 = ArrayHelper::getValue($tempDirpovd,
            'Себестоимость продаж.2013');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Sebestoimost prodazh - 2014
        $model->dirpovd_sebestoimost_prodazh_2014 = ArrayHelper::getValue($tempDirpovd,
            'Себестоимость продаж.2014');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Sebestoimost prodazh - 2015
        $model->dirpovd_sebestoimost_prodazh_2015 = ArrayHelper::getValue($tempDirpovd,
            'Себестоимость продаж.2015');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Sebestoimost prodazh - 2016
        $model->dirpovd_sebestoimost_prodazh_2016 = ArrayHelper::getValue($tempDirpovd,
            'Себестоимость продаж.2016');

        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Valovaya pribyl - 2012
        $model->dirpovd_valovaya_pribyl_2012 = ArrayHelper::getValue($tempDirpovd,
            'Валовая прибыль (убыток).2012');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Valovaya pribyl - 2013
        $model->dirpovd_valovaya_pribyl_2013 = ArrayHelper::getValue($tempDirpovd,
            'Валовая прибыль (убыток).2013');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Valovaya pribyl - 2014
        $model->dirpovd_valovaya_pribyl_2014 = ArrayHelper::getValue($tempDirpovd,
            'Валовая прибыль (убыток).2014');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Valovaya pribyl - 2015
        $model->dirpovd_valovaya_pribyl_2015 = ArrayHelper::getValue($tempDirpovd,
            'Валовая прибыль (убыток).2015');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Valovaya pribyl - 2016
        $model->dirpovd_valovaya_pribyl_2016 = ArrayHelper::getValue($tempDirpovd,
            'Валовая прибыль (убыток).2016');

        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Kommercheskiye raskhody - 2012
        $model->dirpovd_kommercheskiye_raskhody_2012 = ArrayHelper::getValue($tempDirpovd,
            'Коммерческие расходы.2012');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Kommercheskiye raskhody - 2013
        $model->dirpovd_kommercheskiye_raskhody_2013 = ArrayHelper::getValue($tempDirpovd,
            'Коммерческие расходы.2013');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Kommercheskiye raskhody - 2014
        $model->dirpovd_kommercheskiye_raskhody_2014 = ArrayHelper::getValue($tempDirpovd,
            'Коммерческие расходы.2014');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Kommercheskiye raskhody - 2015
        $model->dirpovd_kommercheskiye_raskhody_2015 = ArrayHelper::getValue($tempDirpovd,
            'Коммерческие расходы.2015');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Kommercheskiye raskhody - 2016
        $model->dirpovd_kommercheskiye_raskhody_2016 = ArrayHelper::getValue($tempDirpovd,
            'Коммерческие расходы.2016');

        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Upravlencheskiye raskhody - 2012
        $model->dirpovd_upravlencheskiye_raskhody_2012 = ArrayHelper::getValue($tempDirpovd,
            'Управленческие расходы.2012');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Upravlencheskiye raskhody - 2013
        $model->dirpovd_upravlencheskiye_raskhody_2013 = ArrayHelper::getValue($tempDirpovd,
            'Управленческие расходы.2013');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Upravlencheskiye raskhody - 2014
        $model->dirpovd_upravlencheskiye_raskhody_2014 = ArrayHelper::getValue($tempDirpovd,
            'Управленческие расходы.2014');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Upravlencheskiye raskhody - 2015
        $model->dirpovd_upravlencheskiye_raskhody_2015 = ArrayHelper::getValue($tempDirpovd,
            'Управленческие расходы.2015');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Upravlencheskiye raskhody - 2016
        $model->dirpovd_upravlencheskiye_raskhody_2016 = ArrayHelper::getValue($tempDirpovd,
            'Управленческие расходы.2016');

        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Pribyl ot prodazh - 2012
        $model->dirpovd_pribyl_ot_prodazh_2012 = ArrayHelper::getValue($tempDirpovd,
            'Прибыль (убыток) от продаж.2012');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Pribyl ot prodazh - 2013
        $model->dirpovd_pribyl_ot_prodazh_2013 = ArrayHelper::getValue($tempDirpovd,
            'Прибыль (убыток) от продаж.2013');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Pribyl ot prodazh - 2014
        $model->dirpovd_pribyl_ot_prodazh_2014 = ArrayHelper::getValue($tempDirpovd,
            'Прибыль (убыток) от продаж.2014');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Pribyl ot prodazh - 2015
        $model->dirpovd_pribyl_ot_prodazh_2015 = ArrayHelper::getValue($tempDirpovd,
            'Прибыль (убыток) от продаж.2015');
        //Otchet o pribylyakh i ubytkakh - Dokhody i raskhody po obychnym vidam deyatel'nosti - Pribyl ot prodazh - 2016
        $model->dirpovd_pribyl_ot_prodazh_2016 = ArrayHelper::getValue($tempDirpovd,
            'Прибыль (убыток) от продаж.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k polucheniyu - 2012
        $model->pdir_protsenty_k_polucheniyu_2012 = ArrayHelper::getValue($tempPdir,
            'Проценты к получению.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k polucheniyu - 2013
        $model->pdir_protsenty_k_polucheniyu_2013 = ArrayHelper::getValue($tempPdir,
            'Проценты к получению.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k polucheniyu - 2014
        $model->pdir_protsenty_k_polucheniyu_2014 = ArrayHelper::getValue($tempPdir,
            'Проценты к получению.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k polucheniyu - 2015
        $model->pdir_protsenty_k_polucheniyu_2015 = ArrayHelper::getValue($tempPdir,
            'Проценты к получению.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k polucheniyu - 2016
        $model->pdir_protsenty_k_polucheniyu_2016 = ArrayHelper::getValue($tempPdir,
            'Проценты к получению.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k uplate - 2012
        $model->pdir_protsenty_k_uplate_2012 = ArrayHelper::getValue($tempPdir,
            'Проценты к уплате.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k uplate - 2013
        $model->pdir_protsenty_k_uplate_2013 = ArrayHelper::getValue($tempPdir,
            'Проценты к уплате.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k uplate - 2014
        $model->pdir_protsenty_k_uplate_2014 = ArrayHelper::getValue($tempPdir,
            'Проценты к уплате.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k uplate - 2015
        $model->pdir_protsenty_k_uplate_2015 = ArrayHelper::getValue($tempPdir,
            'Проценты к уплате.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Protsenty k uplate - 2016
        $model->pdir_protsenty_k_uplate_2016 = ArrayHelper::getValue($tempPdir,
            'Проценты к уплате.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Dokhody ot uchastiya v drugikh organizatsiyakh - 2012
        $model->pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2012 = ArrayHelper::getValue($tempPdir,
            'Доходы от участия в других организациях.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Dokhody ot uchastiya v drugikh organizatsiyakh - 2013
        $model->pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2013 = ArrayHelper::getValue($tempPdir,
            'Доходы от участия в других организациях.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Dokhody ot uchastiya v drugikh organizatsiyakh - 2014
        $model->pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2014 = ArrayHelper::getValue($tempPdir,
            'Доходы от участия в других организациях.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Dokhody ot uchastiya v drugikh organizatsiyakh - 2015
        $model->pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2015 = ArrayHelper::getValue($tempPdir,
            'Доходы от участия в других организациях.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Dokhody ot uchastiya v drugikh organizatsiyakh - 2016
        $model->pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2016 = ArrayHelper::getValue($tempPdir,
            'Доходы от участия в других организациях.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye dokhody - 2012
        $model->pdir_prochiye_dokhody_2012 = ArrayHelper::getValue($tempPdir,
            'Прочие доходы.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye dokhody - 2013
        $model->pdir_prochiye_dokhody_2013 = ArrayHelper::getValue($tempPdir,
            'Прочие доходы.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye dokhody - 2014
        $model->pdir_prochiye_dokhody_2014 = ArrayHelper::getValue($tempPdir,
            'Прочие доходы.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye dokhody - 2015
        $model->pdir_prochiye_dokhody_2015 = ArrayHelper::getValue($tempPdir,
            'Прочие доходы.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye dokhody - 2016
        $model->pdir_prochiye_dokhody_2016 = ArrayHelper::getValue($tempPdir,
            'Прочие доходы.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye raskhody - 2012
        $model->pdir_prochiye_raskhody_2012 = ArrayHelper::getValue($tempPdir,
            'Прочие расходы.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye raskhody - 2013
        $model->pdir_prochiye_raskhody_2013 = ArrayHelper::getValue($tempPdir,
            'Прочие расходы.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye raskhody - 2014
        $model->pdir_prochiye_raskhody_2014 = ArrayHelper::getValue($tempPdir,
            'Прочие расходы.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye raskhody - 2015
        $model->pdir_prochiye_raskhody_2015 = ArrayHelper::getValue($tempPdir,
            'Прочие расходы.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Prochiye raskhody - 2016
        $model->pdir_prochiye_raskhody_2016 = ArrayHelper::getValue($tempPdir,
            'Прочие расходы.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Pribyl do nalogooblozheniya - 2012
        $model->pdir_pribyl_do_nalogooblozheniya_2012 = ArrayHelper::getValue($tempPdir,
            'Прибыль (убыток) до налогообложения.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Pribyl do nalogooblozheniya - 2013
        $model->pdir_pribyl_do_nalogooblozheniya_2013 = ArrayHelper::getValue($tempPdir,
            'Прибыль (убыток) до налогообложения.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Pribyl do nalogooblozheniya - 2014
        $model->pdir_pribyl_do_nalogooblozheniya_2014 = ArrayHelper::getValue($tempPdir,
            'Прибыль (убыток) до налогообложения.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Pribyl do nalogooblozheniya - 2015
        $model->pdir_pribyl_do_nalogooblozheniya_2015 = ArrayHelper::getValue($tempPdir,
            'Прибыль (убыток) до налогообложения.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Pribyl do nalogooblozheniya - 2016
        $model->pdir_pribyl_do_nalogooblozheniya_2016 = ArrayHelper::getValue($tempPdir,
            'Прибыль (убыток) до налогообложения.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh aktivov - 2012
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2012 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых активов.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh aktivov - 2013
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2013 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых активов.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh aktivov - 2014
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2014 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых активов.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh aktivov - 2015
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2015 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых активов.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh aktivov - 2016
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2016 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых активов.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh obyazatelstv - 2012
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2012 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых обязательств.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh obyazatelstv - 2013
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2013 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых обязательств.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh obyazatelstv - 2014
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2014 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых обязательств.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh obyazatelstv - 2015
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2015 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых обязательств.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Izmeneniye otlozhennykh nalogovykh obyazatelstv - 2016
        $model->pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2016 = ArrayHelper::getValue($tempPdir,
            'Изменение отложенных налоговых обязательств.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Tekushchiy nalog na pribyl - 2012
        $model->pdir_tekushchiy_nalog_na_pribyl_2012 = ArrayHelper::getValue($tempPdir,
            'Текущий налог на прибыль.2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Tekushchiy nalog na pribyl - 2013
        $model->pdir_tekushchiy_nalog_na_pribyl_2013 = ArrayHelper::getValue($tempPdir,
            'Текущий налог на прибыль.2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Tekushchiy nalog na pribyl - 2014
        $model->pdir_tekushchiy_nalog_na_pribyl_2014 = ArrayHelper::getValue($tempPdir,
            'Текущий налог на прибыль.2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Tekushchiy nalog na pribyl - 2015
        $model->pdir_tekushchiy_nalog_na_pribyl_2015 = ArrayHelper::getValue($tempPdir,
            'Текущий налог на прибыль.2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Tekushchiy nalog na pribyl - 2016
        $model->pdir_tekushchiy_nalog_na_pribyl_2016 = ArrayHelper::getValue($tempPdir,
            'Текущий налог на прибыль.2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Chistaya pribyl - 2012
        $model->pdir_chistaya_pribyl_2012 = ArrayHelper::getValue($tempPdir,
            'Чистая прибыль (убыток).2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Chistaya pribyl - 2013
        $model->pdir_chistaya_pribyl_2013 = ArrayHelper::getValue($tempPdir,
            'Чистая прибыль (убыток).2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Chistaya pribyl - 2014
        $model->pdir_chistaya_pribyl_2014 = ArrayHelper::getValue($tempPdir,
            'Чистая прибыль (убыток).2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Chistaya pribyl - 2015
        $model->pdir_chistaya_pribyl_2015 = ArrayHelper::getValue($tempPdir,
            'Чистая прибыль (убыток).2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Chistaya pribyl - 2016
        $model->pdir_chistaya_pribyl_2016 = ArrayHelper::getValue($tempPdir,
            'Чистая прибыль (убыток).2016');

        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Postoyannyye nalogovyye obyazatelstva - 2012
        $model->pdir_postoyannyye_nalogovyye_obyazatelstva_2012 = ArrayHelper::getValue($tempPdir,
            'Постоянные налоговые обязательства (активы).2012');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Postoyannyye nalogovyye obyazatelstva - 2013
        $model->pdir_postoyannyye_nalogovyye_obyazatelstva_2013 = ArrayHelper::getValue($tempPdir,
            'Постоянные налоговые обязательства (активы).2013');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Postoyannyye nalogovyye obyazatelstva - 2014
        $model->pdir_postoyannyye_nalogovyye_obyazatelstva_2014 = ArrayHelper::getValue($tempPdir,
            'Постоянные налоговые обязательства (активы).2014');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Postoyannyye nalogovyye obyazatelstva - 2015
        $model->pdir_postoyannyye_nalogovyye_obyazatelstva_2015 = ArrayHelper::getValue($tempPdir,
            'Постоянные налоговые обязательства (активы).2015');
        //Otchet o pribylyakh i ubytkakh - Prochiye dokhody i raskhody - Postoyannyye nalogovyye obyazatelstva - 2016
        $model->pdir_postoyannyye_nalogovyye_obyazatelstva_2016 = ArrayHelper::getValue($tempPdir,
            'Постоянные налоговые обязательства (активы).2016');

        $model->save();
        if (!$model->save()) {
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
            return $model->getErrors();
        }
        return [
            'reply' => ['4' => 'Присутствует, множественное'],
            'data' => $model,
        ];
        //return $model;
    }

    public function fields()
    {
        return [
            //'id',
            'va_nematerialnyye_aktivy_2016',
            'va_nematerialnyye_aktivy_2012',
            'va_nematerialnyye_aktivy_2013',
            'va_nematerialnyye_aktivy_2014',
            'va_nematerialnyye_aktivy_2015',
            'va_osnovnyye_sredstva_2016',
            'va_osnovnyye_sredstva_2012',
            'va_osnovnyye_sredstva_2013',
            'va_osnovnyye_sredstva_2014',
            'va_osnovnyye_sredstva_2015',
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2016',
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2012',
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2013',
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2014',
            'va_dokhodnyye_vlozheniya_v_materialnyye_tsennosti_2015',
            'va_finansovyye_vlozheniya_2016',
            'va_finansovyye_vlozheniya_2012',
            'va_finansovyye_vlozheniya_2013',
            'va_finansovyye_vlozheniya_2014',
            'va_finansovyye_vlozheniya_2015',
            'va_otlozhennyye_nalogovyye_aktivy_2016',
            'va_otlozhennyye_nalogovyye_aktivy_2012',
            'va_otlozhennyye_nalogovyye_aktivy_2013',
            'va_otlozhennyye_nalogovyye_aktivy_2014',
            'va_otlozhennyye_nalogovyye_aktivy_2015',
            'va_itogo_po_razdelu_1_2016',
            'va_itogo_po_razdelu_1_2012',
            'va_itogo_po_razdelu_1_2013',
            'va_itogo_po_razdelu_1_2014',
            'va_itogo_po_razdelu_1_2015',
            'oa_zapasy_2016',
            'oa_zapasy_2012',
            'oa_zapasy_2013',
            'oa_zapasy_2014',
            'oa_zapasy_2015',
            'oa_nds_po_priobretennym_tsennostyam_2016',
            'oa_nds_po_priobretennym_tsennostyam_2012',
            'oa_nds_po_priobretennym_tsennostyam_2013',
            'oa_nds_po_priobretennym_tsennostyam_2014',
            'oa_nds_po_priobretennym_tsennostyam_2015',
            'oa_debitorskaya_zadolzhennost_2016',
            'oa_debitorskaya_zadolzhennost_2012',
            'oa_debitorskaya_zadolzhennost_2013',
            'oa_debitorskaya_zadolzhennost_2014',
            'oa_debitorskaya_zadolzhennost_2015',
            'oa_finansovyye_vlozheniya_2016',
            'oa_finansovyye_vlozheniya_2012',
            'oa_finansovyye_vlozheniya_2013',
            'oa_finansovyye_vlozheniya_2014',
            'oa_finansovyye_vlozheniya_2015',
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2016',
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2012',
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2013',
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2014',
            'oa_denezhnyye_sredstva_i_denezhnyye_ekvivalenty_2015',
            'oa_prochiye_oborotnyye_aktivy_2016',
            'oa_prochiye_oborotnyye_aktivy_2012',
            'oa_prochiye_oborotnyye_aktivy_2013',
            'oa_prochiye_oborotnyye_aktivy_2014',
            'oa_prochiye_oborotnyye_aktivy_2015',
            'oa_itogo_po_razdelu_2_2016',
            'oa_itogo_po_razdelu_2_2012',
            'oa_itogo_po_razdelu_2_2013',
            'oa_itogo_po_razdelu_2_2014',
            'oa_itogo_po_razdelu_2_2015',
            'oa_balans_2016',
            'oa_balans_2012',
            'oa_balans_2013',
            'oa_balans_2014',
            'oa_balans_2015',
            'kir_ustavnyy_kapital_2016',
            'kir_ustavnyy_kapital_2012',
            'kir_ustavnyy_kapital_2013',
            'kir_ustavnyy_kapital_2014',
            'kir_ustavnyy_kapital_2015',
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2016',
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2012',
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2013',
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2014',
            'kir_sobstvennyye_aktsii_vykuplennyye_u_aktsionerov_2015',
            'kir_dobavochnyy_kapital_2016',
            'kir_dobavochnyy_kapital_2012',
            'kir_dobavochnyy_kapital_2013',
            'kir_dobavochnyy_kapital_2014',
            'kir_dobavochnyy_kapital_2015',
            'kir_rezervnyy_kapital_2016',
            'kir_rezervnyy_kapital_2012',
            'kir_rezervnyy_kapital_2013',
            'kir_rezervnyy_kapital_2014',
            'kir_rezervnyy_kapital_2015',
            'kir_neraspredelennaya_pribyl_2016',
            'kir_neraspredelennaya_pribyl_2012',
            'kir_neraspredelennaya_pribyl_2013',
            'kir_neraspredelennaya_pribyl_2014',
            'kir_neraspredelennaya_pribyl_2015',
            'kir_itogo_po_razdelu_3_2016',
            'kir_itogo_po_razdelu_3_2012',
            'kir_itogo_po_razdelu_3_2013',
            'kir_itogo_po_razdelu_3_2014',
            'kir_itogo_po_razdelu_3_2015',
            'do_zayemnyye_sredstva_2016',
            'do_zayemnyye_sredstva_2012',
            'do_zayemnyye_sredstva_2013',
            'do_zayemnyye_sredstva_2014',
            'do_zayemnyye_sredstva_2015',
            'do_otlozhennyye_nalogovyye_obyazatelstva_2016',
            'do_otlozhennyye_nalogovyye_obyazatelstva_2012',
            'do_otlozhennyye_nalogovyye_obyazatelstva_2013',
            'do_otlozhennyye_nalogovyye_obyazatelstva_2014',
            'do_otlozhennyye_nalogovyye_obyazatelstva_2015',
            'do_prochiye_obyazatelstva_2016',
            'do_prochiye_obyazatelstva_2012',
            'do_prochiye_obyazatelstva_2013',
            'do_prochiye_obyazatelstva_2014',
            'do_prochiye_obyazatelstva_2015',
            'do_itogo_po_razdelu_4_2016',
            'do_itogo_po_razdelu_4_2012',
            'do_itogo_po_razdelu_4_2013',
            'do_itogo_po_razdelu_4_2014',
            'do_itogo_po_razdelu_4_2015',
            'ko_zayemnyye_sredstva_2016',
            'ko_zayemnyye_sredstva_2012',
            'ko_zayemnyye_sredstva_2013',
            'ko_zayemnyye_sredstva_2014',
            'ko_zayemnyye_sredstva_2015',
            'ko_kreditorskaya_zadolzhennost_2016',
            'ko_kreditorskaya_zadolzhennost_2012',
            'ko_kreditorskaya_zadolzhennost_2013',
            'ko_kreditorskaya_zadolzhennost_2014',
            'ko_kreditorskaya_zadolzhennost_2015',
            'ko_dokhody_budushchikh_periodov_2016',
            'ko_dokhody_budushchikh_periodov_2012',
            'ko_dokhody_budushchikh_periodov_2013',
            'ko_dokhody_budushchikh_periodov_2014',
            'ko_dokhody_budushchikh_periodov_2015',
            'ko_prochiye_obyazatelstva_2016',
            'ko_prochiye_obyazatelstva_2012',
            'ko_prochiye_obyazatelstva_2013',
            'ko_prochiye_obyazatelstva_2014',
            'ko_prochiye_obyazatelstva_2015',
            'ko_itogo_po_razdelu_5_2016',
            'ko_itogo_po_razdelu_5_2012',
            'ko_itogo_po_razdelu_5_2013',
            'ko_itogo_po_razdelu_5_2014',
            'ko_itogo_po_razdelu_5_2015',
            'ko_balans_2016',
            'ko_balans_2012',
            'ko_balans_2013',
            'ko_balans_2014',
            'ko_balans_2015',
            'dirpovd_vyruchka_2012',
            'dirpovd_vyruchka_2013',
            'dirpovd_vyruchka_2014',
            'dirpovd_vyruchka_2015',
            'dirpovd_vyruchka_2016',
            'dirpovd_sebestoimost_prodazh_2012',
            'dirpovd_sebestoimost_prodazh_2013',
            'dirpovd_sebestoimost_prodazh_2014',
            'dirpovd_sebestoimost_prodazh_2015',
            'dirpovd_sebestoimost_prodazh_2016',
            'dirpovd_valovaya_pribyl_2012',
            'dirpovd_valovaya_pribyl_2013',
            'dirpovd_valovaya_pribyl_2014',
            'dirpovd_valovaya_pribyl_2015',
            'dirpovd_valovaya_pribyl_2016',
            'dirpovd_kommercheskiye_raskhody_2012',
            'dirpovd_kommercheskiye_raskhody_2013',
            'dirpovd_kommercheskiye_raskhody_2014',
            'dirpovd_kommercheskiye_raskhody_2015',
            'dirpovd_kommercheskiye_raskhody_2016',
            'dirpovd_upravlencheskiye_raskhody_2012',
            'dirpovd_upravlencheskiye_raskhody_2013',
            'dirpovd_upravlencheskiye_raskhody_2014',
            'dirpovd_upravlencheskiye_raskhody_2015',
            'dirpovd_upravlencheskiye_raskhody_2016',
            'dirpovd_pribyl_ot_prodazh_2012',
            'dirpovd_pribyl_ot_prodazh_2013',
            'dirpovd_pribyl_ot_prodazh_2014',
            'dirpovd_pribyl_ot_prodazh_2015',
            'dirpovd_pribyl_ot_prodazh_2016',
            'pdir_protsenty_k_polucheniyu_2012',
            'pdir_protsenty_k_polucheniyu_2013',
            'pdir_protsenty_k_polucheniyu_2014',
            'pdir_protsenty_k_polucheniyu_2015',
            'pdir_protsenty_k_polucheniyu_2016',
            'pdir_protsenty_k_uplate_2012',
            'pdir_protsenty_k_uplate_2013',
            'pdir_protsenty_k_uplate_2014',
            'pdir_protsenty_k_uplate_2015',
            'pdir_protsenty_k_uplate_2016',
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2012',
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2013',
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2014',
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2015',
            'pdir_dokhody_ot_uchastiya_v_drugikh_organizatsiyakh_2016',
            'pdir_prochiye_dokhody_2012',
            'pdir_prochiye_dokhody_2013',
            'pdir_prochiye_dokhody_2014',
            'pdir_prochiye_dokhody_2015',
            'pdir_prochiye_dokhody_2016',
            'pdir_prochiye_raskhody_2012',
            'pdir_prochiye_raskhody_2013',
            'pdir_prochiye_raskhody_2014',
            'pdir_prochiye_raskhody_2015',
            'pdir_prochiye_raskhody_2016',
            'pdir_pribyl_do_nalogooblozheniya_2012',
            'pdir_pribyl_do_nalogooblozheniya_2013',
            'pdir_pribyl_do_nalogooblozheniya_2014',
            'pdir_pribyl_do_nalogooblozheniya_2015',
            'pdir_pribyl_do_nalogooblozheniya_2016',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2012',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2013',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2014',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2015',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_aktivov_2016',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2012',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2013',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2014',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2015',
            'pdir_izmeneniye_otlozhennykh_nalogovykh_obyazatelstv_2016',
            'pdir_tekushchiy_nalog_na_pribyl_2012',
            'pdir_tekushchiy_nalog_na_pribyl_2013',
            'pdir_tekushchiy_nalog_na_pribyl_2014',
            'pdir_tekushchiy_nalog_na_pribyl_2015',
            'pdir_tekushchiy_nalog_na_pribyl_2016',
            'pdir_chistaya_pribyl_2012',
            'pdir_chistaya_pribyl_2013',
            'pdir_chistaya_pribyl_2014',
            'pdir_chistaya_pribyl_2015',
            'pdir_chistaya_pribyl_2016',
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2012',
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2013',
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2014',
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2015',
            'pdir_postoyannyye_nalogovyye_obyazatelstva_2016',
            'va_prochiye_vneoborotnyye_aktivy_2012',
            'va_prochiye_vneoborotnyye_aktivy_2013',
            'va_prochiye_vneoborotnyye_aktivy_2014',
            'va_prochiye_vneoborotnyye_aktivy_2015',
            'va_prochiye_vneoborotnyye_aktivy_2016',
            'response' => function ($model) {
                return Json::decode($model->response);
            },
        ];
    }
    /**
     * @inheritdoc
     * @return \common\models\query\FinancialStatementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\FinancialStatementQuery(get_called_class());
    }
}
