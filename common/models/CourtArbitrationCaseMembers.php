<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "court_arbitration_case_members".
 *
 * @property integer $id
 * @property integer $court_arbitration_case_id
 * @property string $inn
 * @property string $ogrn
 * @property string $name
 * @property string $type
 *
 * @property CourtArbitrationCase $courtArbitrationCase
 */
class CourtArbitrationCaseMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'court_arbitration_case_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['court_arbitration_case_id'], 'integer'],
            [['inn', 'ogrn', 'name', 'type'], 'string'],
            [['court_arbitration_case_id'], 'exist', 'skipOnError' => true, 'targetClass' => CourtArbitrationCase::className(), 'targetAttribute' => ['court_arbitration_case_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'court_arbitration_case_id' => Yii::t('app', 'Court Arbitration Case ID'),
            'inn' => Yii::t('app', 'Inn'),
            'ogrn' => Yii::t('app', 'Ogrn'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourtArbitrationCase()
    {
        return $this->hasOne(CourtArbitrationCase::className(), ['id' => 'court_arbitration_case_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\CourtArbitrationCaseMembersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CourtArbitrationCaseMembersQuery(get_called_class());
    }
}
