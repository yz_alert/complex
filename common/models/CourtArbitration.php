<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * This is the model class for table "court_arbitration".
 *
 * @property integer $id
 * @property string $request_string
 * @property string $response
 * @property string $tochno_vsego
 * @property string $netochno_vsego
 * @property string $ogrn
 *
 * @property CourtArbitrationCase[] $courtArbitrationCases
 */
class CourtArbitration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'court_arbitration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_string', 'response', 'tochno_vsego', 'netochno_vsego'], 'string'],
            [['ogrn'], 'integer'],
            [['ogrn'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'request_string' => Yii::t('app', 'Request String'),
            'response' => Yii::t('app', 'Response'),
            'tochno_vsego' => Yii::t('app', 'Tochno Vsego'),
            'netochno_vsego' => Yii::t('app', 'Netochno Vsego'),
            'ogrn' => Yii::t('app', 'Ogrn'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourtArbitrationCases()
    {
        return $this->hasMany(CourtArbitrationCase::className(), ['ogrn_id' => 'ogrn']);
    }

    public static function getCourtArbitration($domain, $apiKey, $searchString)
    {
        $url = 'court-arbitration';
        $client = new Client(['baseUrl' => $domain]);
        $response = $client->post($url, ['api_key' => $apiKey, 'id' => $searchString])->send();
        if ($response->isOk) {
            return $response;
        }
        return 0;
    }

    public static function courtArbitrationWithZcb($searchString)
    {
        //Отправляем запрос на ЗЧБ
        $response = self::getCourtArbitration(CompanySearch::DOMAIN, CompanySearch::API_KEY, $searchString);

        //Сохраняем ответ в таблицу CompanyCard
        $model = new CourtArbitration();
        $model->request_string = $searchString;
        $model->ogrn = $searchString;
        $model->response = json_encode($response->data);
        $model->tochno_vsego = (string)ArrayHelper::getValue($response->data, 'точно.всего');
        $model->netochno_vsego = (string)ArrayHelper::getValue($response->data, 'неточно.всего');

        $model->save();
        if (!$model->save()) {
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
            echo PHP_EOL;
            return $model->getErrors();
        }

        $tempTochnoDela = $response->data['точно']['дела'];
        $tempNeTochnoDela = $response->data['неточно']['дела'];

        foreach ($tempTochnoDela as $key => $case) {
            $courtArbitrationCase = new CourtArbitrationCase();
            $courtArbitrationCase->ogrn_id = $model->request_string;
            $courtArbitrationCase->is_sure = true;
            $courtArbitrationCase->name = $key;
            $courtArbitrationCase->nomer_dela = ArrayHelper::getValue($case, 'НомерДела');
            $courtArbitrationCase->summa_iska = (string)ArrayHelper::getValue($case, 'СуммаИска');
            $courtArbitrationCase->start_dat = ArrayHelper::getValue($case, 'СтартДата');
            $courtArbitrationCase->save();
            if (!$courtArbitrationCase->save()) {
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                return $courtArbitrationCase->getErrors();
            }
            if (ArrayHelper::keyExists('Истец', $case)) {
                foreach (ArrayHelper::getValue($case, 'Истец') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Истец';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (ArrayHelper::keyExists('Ответчик', $case)) {
                foreach (ArrayHelper::getValue($case, 'Ответчик') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Ответчик';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (ArrayHelper::keyExists('Третье лицо', $case)) {
                foreach (ArrayHelper::getValue($case, 'Третье лицо') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Третье лицо';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (ArrayHelper::keyExists('Иное лицо', $case)) {
                foreach (ArrayHelper::getValue($case, 'Иное лицо') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Иное лицо';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (!$caseMembers->save()) {
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                return $caseMembers->getErrors();
            }
        }

        foreach ($tempNeTochnoDela as $key => $case) {
            $courtArbitrationCase = new CourtArbitrationCase();
            $courtArbitrationCase->ogrn_id = $model->request_string;
            $courtArbitrationCase->is_sure = false;
            $courtArbitrationCase->name = $key;
            $courtArbitrationCase->nomer_dela = ArrayHelper::getValue($case, 'НомерДела');
            $courtArbitrationCase->summa_iska = (string)ArrayHelper::getValue($case, 'СуммаИска');
            $courtArbitrationCase->start_dat = ArrayHelper::getValue($case, 'СтартДата');
            $courtArbitrationCase->save();
            if (!$courtArbitrationCase->save()) {
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                return $courtArbitrationCase->getErrors();
            }
            if (ArrayHelper::keyExists('Истец', $case)) {
                foreach (ArrayHelper::getValue($case, 'Истец') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Истец';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (ArrayHelper::keyExists('Ответчик', $case)) {
                foreach (ArrayHelper::getValue($case, 'Ответчик') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Ответчик';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (ArrayHelper::keyExists('Третье лицо', $case)) {
                foreach (ArrayHelper::getValue($case, 'Третье лицо') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Третье лицо';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (ArrayHelper::keyExists('Иное лицо', $case)) {
                foreach (ArrayHelper::getValue($case, 'Иное лицо') as $plaintiff) {
                    $caseMembers = new CourtArbitrationCaseMembers();
                    $caseMembers->court_arbitration_case_id = $courtArbitrationCase->id;
                    $caseMembers->type = 'Иное лицо';
                    $caseMembers->inn = ArrayHelper::getValue($plaintiff, 'ИНН');
                    $caseMembers->ogrn = ArrayHelper::getValue($plaintiff, 'ОГРН');
                    $caseMembers->name = ArrayHelper::getValue($plaintiff, 'Наименование');
                    $caseMembers->save();
                }
            }
            if (!$caseMembers->save()) {
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                echo PHP_EOL;
                return $caseMembers->getErrors();
            }
        }
        return CourtArbitration::findOne(['ogrn' => $searchString]);
    }

    public function fields()
    {
        return [
            //'id',
            'ogrn',
            'tochno_vsego',
            'netochno_vsego',
            'response' => function ($model) {
                return Json::decode($model->response);
            },
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\CourtArbitrationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CourtArbitrationQuery(get_called_class());
    }
}
