<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unfair_supplier_223_document".
 *
 * @property integer $id
 * @property integer $unfair_supplier_223_id
 * @property string $guid
 * @property string $create_date_time
 * @property string $file_name
 * @property string $description
 * @property string $content
 * @property string $url
 * @property string $registration_number
 *
 * @property UnfairSupplier223 $unfairSupplier223
 */
class UnfairSupplier223Document extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unfair_supplier_223_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unfair_supplier_223_id'], 'required'],
            [['unfair_supplier_223_id'], 'integer'],
            [
                [
                    'guid',
                    'create_date_time',
                    'file_name',
                    'description',
                    'content',
                    'url',
                    'registration_number',
                ],
                'string',
            ],
            [
                ['unfair_supplier_223_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UnfairSupplier223::className(),
                'targetAttribute' => ['unfair_supplier_223_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unfair_supplier_223_id' => Yii::t('app', 'Unfair Supplier 223 ID'),
            'guid' => Yii::t('app', 'Guid'),
            'create_date_time' => Yii::t('app', 'Create Date Time'),
            'file_name' => Yii::t('app', 'File Name'),
            'description' => Yii::t('app', 'Description'),
            'content' => Yii::t('app', 'Content'),
            'url' => Yii::t('app', 'Url'),
            'registration_number' => Yii::t('app', 'Registration Number'),
        ];
    }

    public function fields()
    {
        return [
            'guid',
            'create_date_time',
            'file_name',
            'description',
            'content',
            'url',
            'registration_number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnfairSupplier223()
    {
        return $this->hasOne(UnfairSupplier223::className(), ['id' => 'unfair_supplier_223_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UnfairSupplier223DocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UnfairSupplier223DocumentQuery(get_called_class());
    }
}
