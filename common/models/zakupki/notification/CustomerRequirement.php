<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_customer_requirement}}".
 *
 * @property int $id
 * @property int $ntf_lot_id
 * @property string $customer_reg_num
 * @property string $customer_cons_registry_num
 * @property string $customer_full_name
 * @property string $max_price
 * @property string $delivery_place
 * @property string $delivery_term
 * @property string $oneside_rejection
 * @property string $application_guarantee_amount
 * @property string $application_guarantee_part
 * @property string $application_guarantee_procedure_info
 * @property string $application_guarantee_settlement_account
 * @property string $application_guarantee_personal_account
 * @property string $application_guarantee_bik
 * @property string $contract_guarantee_amount
 * @property string $contract_guarantee_part
 * @property string $contract_guarantee_procedure_info
 * @property string $contract_guarantee_settlement_account
 * @property string $contract_guarantee_personal_account
 * @property string $contract_guarantee_bik
 * @property string $add_info
 * @property string $purchase_code
 * @property string $tender_plan_info_plan_number
 * @property string $tender_plan_info_position_number
 * @property string $tender_plan_info_purchase83st544
 * @property string $tender_plan_info_plan2017_number
 * @property string $tender_plan_info_position2017_number
 * @property string $tender_plan_info_position2017_ext_number
 * @property string $nonbudget_financings_total_sum
 * @property string $budget_financings_total_sum
 * @property string $bo_info_bo_number
 * @property string $bo_info_bo_date
 * @property string $bo_info_input_bo_flag
 *
 * @property BudgetFinancing[] $budgetFinancings
 * @property Lot $lot
 * @property KladrPlace[] $kladrPlaces
 * @property NonbudgetFinancing[] $nonbudgetFinancings
 */
class CustomerRequirement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_customer_requirement}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_lot_id'], 'default', 'value' => null],
            [['ntf_lot_id'], 'integer'],
            [
                [
                    'customer_reg_num',
                    'customer_cons_registry_num',
                    'customer_full_name',
                    'max_price',
                    'delivery_place',
                    'delivery_term',
                    'oneside_rejection',
                    'application_guarantee_amount',
                    'application_guarantee_part',
                    'application_guarantee_procedure_info',
                    'application_guarantee_settlement_account',
                    'application_guarantee_personal_account',
                    'application_guarantee_bik',
                    'contract_guarantee_amount',
                    'contract_guarantee_part',
                    'contract_guarantee_procedure_info',
                    'contract_guarantee_settlement_account',
                    'contract_guarantee_personal_account',
                    'contract_guarantee_bik',
                    'add_info',
                    'purchase_code',
                    'tender_plan_info_plan_number',
                    'tender_plan_info_position_number',
                    'tender_plan_info_purchase83st544',
                    'tender_plan_info_plan2017_number',
                    'tender_plan_info_position2017_number',
                    'tender_plan_info_position2017_ext_number',
                    'nonbudget_financings_total_sum',
                    'budget_financings_total_sum',
                    'bo_info_bo_number',
                    'bo_info_bo_date',
                    'bo_info_input_bo_flag',
                ],
                'string',
            ],
            [
                ['ntf_lot_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lot::className(),
                'targetAttribute' => ['ntf_lot_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_lot_id' => Yii::t('app', 'Ntf Lot ID'),
            'customer_reg_num' => Yii::t('app', 'Customer Reg Num'),
            'customer_cons_registry_num' => Yii::t('app', 'Customer Cons Registry Num'),
            'customer_full_name' => Yii::t('app', 'Customer Full Name'),
            'max_price' => Yii::t('app', 'Max Price'),
            'delivery_place' => Yii::t('app', 'Delivery Place'),
            'delivery_term' => Yii::t('app', 'Delivery Term'),
            'oneside_rejection' => Yii::t('app', 'Oneside Rejection'),
            'application_guarantee_amount' => Yii::t('app', 'Application Guarantee Amount'),
            'application_guarantee_part' => Yii::t('app', 'Application Guarantee Part'),
            'application_guarantee_procedure_info' => Yii::t('app', 'Application Guarantee Procedure Info'),
            'application_guarantee_settlement_account' => Yii::t('app', 'Application Guarantee Settlement Account'),
            'application_guarantee_personal_account' => Yii::t('app', 'Application Guarantee Personal Account'),
            'application_guarantee_bik' => Yii::t('app', 'Application Guarantee Bik'),
            'contract_guarantee_amount' => Yii::t('app', 'Contract Guarantee Amount'),
            'contract_guarantee_part' => Yii::t('app', 'Contract Guarantee Part'),
            'contract_guarantee_procedure_info' => Yii::t('app', 'Contract Guarantee Procedure Info'),
            'contract_guarantee_settlement_account' => Yii::t('app', 'Contract Guarantee Settlement Account'),
            'contract_guarantee_personal_account' => Yii::t('app', 'Contract Guarantee Personal Account'),
            'contract_guarantee_bik' => Yii::t('app', 'Contract Guarantee Bik'),
            'add_info' => Yii::t('app', 'Add Info'),
            'purchase_code' => Yii::t('app', 'Purchase Code'),
            'tender_plan_info_plan_number' => Yii::t('app', 'Tender Plan Info Plan Number'),
            'tender_plan_info_position_number' => Yii::t('app', 'Tender Plan Info Position Number'),
            'tender_plan_info_purchase83st544' => Yii::t('app', 'Tender Plan Info Purchase83st544'),
            'tender_plan_info_plan2017_number' => Yii::t('app', 'Tender Plan Info Plan2017 Number'),
            'tender_plan_info_position2017_number' => Yii::t('app', 'Tender Plan Info Position2017 Number'),
            'tender_plan_info_position2017_ext_number' => Yii::t('app', 'Tender Plan Info Position2017 Ext Number'),
            'nonbudget_financings_total_sum' => Yii::t('app', 'Nonbudget Financings Total Sum'),
            'budget_financings_total_sum' => Yii::t('app', 'Budget Financings Total Sum'),
            'bo_info_bo_number' => Yii::t('app', 'Bo Info Bo Number'),
            'bo_info_bo_date' => Yii::t('app', 'Bo Info Bo Date'),
            'bo_info_input_bo_flag' => Yii::t('app', 'Bo Info Input Bo Flag'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBudgetFinancings()
    {
        return $this->hasMany(BudgetFinancing::className(), ['ntf_customer_requirement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'ntf_lot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKladrPlaces()
    {
        return $this->hasMany(KladrPlace::className(), ['ntf_customer_requirement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNonbudgetFinancings()
    {
        return $this->hasMany(NonbudgetFinancing::className(), ['ntf_customer_requirement_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\CustomerRequirementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\CustomerRequirementQuery(get_called_class());
    }

    public function fields()
    {
        return [
            //'id',
            'customer_reg_num',
            'customer_cons_registry_num',
            'customer_full_name',
            'max_price',
            'delivery_place',
            'delivery_term',
            'oneside_rejection',
            'application_guarantee_amount',
            'application_guarantee_part',
            'application_guarantee_procedure_info',
            'application_guarantee_settlement_account',
            'application_guarantee_personal_account',
            'application_guarantee_bik',
            'contract_guarantee_amount',
            'contract_guarantee_part',
            'contract_guarantee_procedure_info',
            'contract_guarantee_settlement_account',
            'contract_guarantee_personal_account',
            'contract_guarantee_bik',
            'add_info',
            'purchase_code',
            'tender_plan_info_plan_number',
            'tender_plan_info_position_number',
            'tender_plan_info_purchase83st544',
            'tender_plan_info_plan2017_number',
            'tender_plan_info_position2017_number',
            'tender_plan_info_position2017_ext_number',
            'nonbudget_financings_total_sum',
            'budget_financings_total_sum',
            'bo_info_bo_number',
            'bo_info_bo_date',
            'bo_info_input_bo_flag',
            'budget_financings' => function ($model) {
                return $model->budgetFinancings;
            },
            'nonbudget_financings' => function ($model) {
                return $model->nonbudgetFinancings;
            },
            'kladr_places' => function ($model) {
                return $model->kladrPlaces;
            },
        ];
    }

}
