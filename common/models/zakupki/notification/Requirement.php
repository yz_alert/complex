<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_requirement}}".
 *
 * @property int $id
 * @property int $ntf_lot_id
 * @property string $code
 * @property string $short_name
 * @property string $name
 * @property string $content
 *
 * @property Lot $lot
 */
class Requirement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_requirement}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_lot_id'], 'default', 'value' => null],
            [['ntf_lot_id'], 'integer'],
            [['code', 'short_name', 'name', 'content'], 'string'],
            [
                ['ntf_lot_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lot::className(),
                'targetAttribute' => ['ntf_lot_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_lot_id' => Yii::t('app', 'Ntf Lot ID'),
            'code' => Yii::t('app', 'Code'),
            'short_name' => Yii::t('app', 'Short Name'),
            'name' => Yii::t('app', 'Name'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'ntf_lot_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\RequirementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\RequirementQuery(get_called_class());
    }
}
