<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_main}}".
 *
 * @property int $id
 * @property string $ntf_id
 * @property string $type
 * @property string $external_id
 * @property string $purchase_number
 * @property string $direct_date
 * @property string $doc_publish_date
 * @property string $doc_number
 * @property string $href
 * @property string $purchase_object_info
 * @property string $purchase_responsible_responsible_role
 * @property string $okpd2okved2
 * @property string $contract_service_info
 * @property string $created_at
 * @property string $filename
 *
 * @property Etp $etp
 * @property ExtPrintForm[] $extPrintForms
 * @property Lot[] $lots
 * @property PlacingWay $placingWay
 * @property PrintForm $printForm
 * @property ProcedureInfoBidding $procedureInfoBidding
 * @property ProcedureInfoCollecting $procedureInfoCollecting
 * @property ProcedureInfoContracting $procedureInfoContracting
 * @property ProcedureInfoFinalOpening $procedureInfoFinalOpening
 * @property ProcedureInfoOpening $procedureInfoOpening
 * @property ProcedureInfoPrequalification $procedureInfoPrequalification
 * @property ProcedureInfoScoring $procedureInfoScoring
 * @property ProcedureInfoSelecting $procedureInfoSelecting
 * @property PurchaseDocumentation $purchaseDocumentation
 * @property PurchaseResponsibleLastSpecializedOrg $purchaseResponsibleLastSpecializedOrg
 * @property PurchaseResponsibleResponsibleInfo $purchaseResponsibleResponsibleInfo
 * @property PurchaseResponsibleResponsibleOrg $purchaseResponsibleResponsibleOrg
 * @property PurchaseResponsibleSpecializedOrg $purchaseResponsibleSpecializedOrg
 */
class Main extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_main}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'ntf_id',
                    'type',
                    'external_id',
                    'purchase_number',
                    'direct_date',
                    'doc_publish_date',
                    'doc_number',
                    'href',
                    'purchase_object_info',
                    'purchase_responsible_responsible_role',
                    'okpd2okved2',
                    'contract_service_info',
                    'filename',
                ],
                'string',
            ],
            [['created_at'], 'safe'],
            [['ntf_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_id' => Yii::t('app', 'Ntf ID'),
            'type' => Yii::t('app', 'Type'),
            'external_id' => Yii::t('app', 'External ID'),
            'purchase_number' => Yii::t('app', 'Purchase Number'),
            'direct_date' => Yii::t('app', 'Direct Date'),
            'doc_publish_date' => Yii::t('app', 'Doc Publish Date'),
            'doc_number' => Yii::t('app', 'Doc Number'),
            'href' => Yii::t('app', 'Href'),
            'purchase_object_info' => Yii::t('app', 'Purchase Object Info'),
            'purchase_responsible_responsible_role' => Yii::t('app', 'Purchase Responsible Responsible Role'),
            'okpd2okved2' => Yii::t('app', 'Okpd2okved2'),
            'contract_service_info' => Yii::t('app', 'Contract Service Info'),
            'created_at' => Yii::t('app', 'Created At'),
            'filename' => Yii::t('app', 'Filename'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtp()
    {
        return $this->hasOne(Etp::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtPrintForms()
    {
        return $this->hasMany(ExtPrintForm::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacingWay()
    {
        return $this->hasOne(PlacingWay::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrintForm()
    {
        return $this->hasOne(PrintForm::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoBidding()
    {
        return $this->hasOne(ProcedureInfoBidding::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoCollecting()
    {
        return $this->hasOne(ProcedureInfoCollecting::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoContracting()
    {
        return $this->hasOne(ProcedureInfoContracting::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoFinalOpening()
    {
        return $this->hasOne(ProcedureInfoFinalOpening::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoOpening()
    {
        return $this->hasOne(ProcedureInfoOpening::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoPrequalification()
    {
        return $this->hasOne(ProcedureInfoPrequalification::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoScoring()
    {
        return $this->hasOne(ProcedureInfoScoring::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProcedureInfoSelecting()
    {
        return $this->hasOne(ProcedureInfoSelecting::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseDocumentation()
    {
        return $this->hasOne(PurchaseDocumentation::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseResponsibleLastSpecializedOrg()
    {
        return $this->hasOne(PurchaseResponsibleLastSpecializedOrg::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseResponsibleResponsibleInfo()
    {
        return $this->hasOne(PurchaseResponsibleResponsibleInfo::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseResponsibleResponsibleOrg()
    {
        return $this->hasOne(PurchaseResponsibleResponsibleOrg::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseResponsibleSpecializedOrg()
    {
        return $this->hasOne(PurchaseResponsibleSpecializedOrg::className(), ['ntf_main_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\MainQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\MainQuery(get_called_class());
    }

    public function fields()
    {
        return [
            //'id',
            'ntf_id',
            'type',
            'external_id',
            'purchase_number',
            'direct_date',
            'doc_publish_date',
            'doc_number',
            'href',
            'purchase_object_info',
            'purchase_responsible_responsible_role',
            'okpd2okved2',
            'filename',
            //'contract_service_info',
            'created_at',
            'contract_service_info' => function ($model) {
                if ($model->type === "fcsNotificationEA44") {
                    return 11111;
                } //return Json::decode($model->response);
                return $model->contract_service_info;
            },
            'print_form' => function ($model) {
                return $model->printForm;
            },
            'placing_way' => function ($model) {
                return $model->placingWay;
            },
            'etp' => function ($model) {
                return $model->etp;
            },
            'procedure_info_bidding' => function ($model) {
                return $model->procedureInfoBidding;
            },
            'procedure_info_collecting' => function ($model) {
                return $model->procedureInfoCollecting;
            },
            'procedure_info_contracting' => function ($model) {
                return $model->procedureInfoContracting;
            },
            'procedure_info_final_opening' => function ($model) {
                return $model->procedureInfoFinalOpening;
            },
            'procedure_info_opening' => function ($model) {
                return $model->procedureInfoOpening;
            },
            'procedure_info_prequalification' => function ($model) {
                return $model->procedureInfoPrequalification;
            },
            'procedure_info_scoring' => function ($model) {
                return $model->procedureInfoScoring;
            },
            'procedure_info_selecting' => function ($model) {
                return $model->procedureInfoSelecting;
            },
            'purchase_documentation' => function ($model) {
                return $model->purchaseDocumentation;
            },
            'purchase_responsible_responsible_info' => function ($model) {
                return $model->purchaseResponsibleResponsibleInfo;
            },
            'purchase_responsible_responsible_org' => function ($model) {
                return $model->purchaseResponsibleResponsibleOrg;
            },
            'purchase_responsible_specialized_org' => function ($model) {
                return $model->purchaseResponsibleSpecializedOrg;
            },
            'purchase_responsible_last_specialized_org' => function ($model) {
                return $model->purchaseResponsibleLastSpecializedOrg;
            },
            'ext_print_forms' => function ($model) {
                return $model->extPrintForms;
            },
            'lots' => function ($model) {
                return $model->lots;
            },
        ];
    }

    public function extraFields()
    {
        //return ['etp','extPrintForms'];
        return ['lot'];
    }
}
