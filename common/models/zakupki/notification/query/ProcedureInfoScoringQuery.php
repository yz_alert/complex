<?php

namespace common\models\zakupki\notification\query;

/**
 * This is the ActiveQuery class for [[\common\models\zakupki\notification\ProcedureInfoScoring]].
 *
 * @see \common\models\zakupki\notification\ProcedureInfoScoring
 */
class ProcedureInfoScoringQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\ProcedureInfoScoring[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\ProcedureInfoScoring|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
