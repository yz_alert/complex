<?php

namespace common\models\zakupki\notification\query;

/**
 * This is the ActiveQuery class for [[\common\models\zakupki\notification\DrugQuantityCustomerInfo]].
 *
 * @see \common\models\zakupki\notification\DrugQuantityCustomerInfo
 */
class DrugQuantityCustomerInfoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\DrugQuantityCustomerInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\DrugQuantityCustomerInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
