<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_purchase_documentation}}".
 *
 * @property int $id
 * @property int $ntf_main_id
 * @property string $grant_start_date
 * @property string $grant_place
 * @property string $grant_order
 * @property string $languages
 * @property string $grant_means
 * @property string $grant_end_date
 * @property string $pay_currency_code
 * @property string $pay_currency_name
 * @property string $pay_info_part
 * @property string $pay_info_procedure_info
 * @property string $pay_info_settlement_account
 * @property string $pay_info_personal_account
 * @property string $pay_info_bik
 * @property string $pay_info_pay_currency_code
 * @property string $pay_info_pay_currency_name
 *
 * @property Main $main
 */
class PurchaseDocumentation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_purchase_documentation}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_main_id'], 'default', 'value' => null],
            [['ntf_main_id'], 'integer'],
            [
                [
                    'grant_start_date',
                    'grant_place',
                    'grant_order',
                    'languages',
                    'grant_means',
                    'grant_end_date',
                    'pay_currency_code',
                    'pay_currency_name',
                    'pay_info_part',
                    'pay_info_procedure_info',
                    'pay_info_settlement_account',
                    'pay_info_personal_account',
                    'pay_info_bik',
                    'pay_info_pay_currency_code',
                    'pay_info_pay_currency_name',
                ],
                'string',
            ],
            [['ntf_main_id'], 'unique'],
            [
                ['ntf_main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['ntf_main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_main_id' => Yii::t('app', 'Ntf Main ID'),
            'grant_start_date' => Yii::t('app', 'Grant Start Date'),
            'grant_place' => Yii::t('app', 'Grant Place'),
            'grant_order' => Yii::t('app', 'Grant Order'),
            'languages' => Yii::t('app', 'Languages'),
            'grant_means' => Yii::t('app', 'Grant Means'),
            'grant_end_date' => Yii::t('app', 'Grant End Date'),
            'pay_currency_code' => Yii::t('app', 'Pay Currency Code'),
            'pay_currency_name' => Yii::t('app', 'Pay Currency Name'),
            'pay_info_part' => Yii::t('app', 'Pay Info Part'),
            'pay_info_procedure_info' => Yii::t('app', 'Pay Info Procedure Info'),
            'pay_info_settlement_account' => Yii::t('app', 'Pay Info Settlement Account'),
            'pay_info_personal_account' => Yii::t('app', 'Pay Info Personal Account'),
            'pay_info_bik' => Yii::t('app', 'Pay Info Bik'),
            'pay_info_pay_currency_code' => Yii::t('app', 'Pay Info Pay Currency Code'),
            'pay_info_pay_currency_name' => Yii::t('app', 'Pay Info Pay Currency Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'ntf_main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\PurchaseDocumentationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\PurchaseDocumentationQuery(get_called_class());
    }
}
