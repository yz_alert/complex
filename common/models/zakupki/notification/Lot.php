<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_lot}}".
 *
 * @property int $id
 * @property int $ntf_main_id
 * @property string $lot_number
 * @property string $lot_object_info
 * @property string $max_price
 * @property string $max_price_info
 * @property string $price_formula
 * @property string $standard_contract_number
 * @property string $currency_code
 * @property string $currency_name
 * @property string $finance_source
 * @property string $interbudgetary_transfer
 * @property string $quantity_undefined
 * @property string $purchase_objects_total_sum
 * @property string $drug_purchase_objects_info_total
 * @property string $restrict_info
 * @property string $restrict_foreigns_info
 * @property string $add_info
 * @property string $no_public_discussion
 * @property string $must_public_discussion
 * @property string $before_pay
 * @property string $purchase_code
 * @property string $max_cost_definition_order
 * @property string $tender_plain_info_plain_number
 * @property string $tender_plain_info_position_number
 * @property string $tender_plain_info_purchase83st544
 * @property string $tender_plain_info_plan2017_number
 * @property string $tender_plain_info_positin2017_number
 * @property string $tender_plain_info_positin2017_ext_number
 *
 * @property Attachment[] $attachments
 * @property CustomerRequirement[] $customerRequirements
 * @property DrugPurchaseObjectInfo[] $drugPurchaseObjectInfos
 * @property Main $main
 * @property Modification $modification
 * @property Okpd $okpd
 * @property Okpd2 $okpd2
 * @property Preferense[] $preferenses
 * @property PublicDiscussion $publicDiscussion
 * @property PurchaseObject[] $purchaseObjects
 * @property Requirement[] $requirements
 * @property Restriction[] $restrictions
 */
class Lot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_lot}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_main_id'], 'default', 'value' => null],
            [['ntf_main_id'], 'integer'],
            [
                [
                    'lot_number',
                    'lot_object_info',
                    'max_price',
                    'max_price_info',
                    'price_formula',
                    'standard_contract_number',
                    'currency_code',
                    'currency_name',
                    'finance_source',
                    'interbudgetary_transfer',
                    'quantity_undefined',
                    'purchase_objects_total_sum',
                    'drug_purchase_objects_info_total',
                    'restrict_info',
                    'restrict_foreigns_info',
                    'add_info',
                    'no_public_discussion',
                    'must_public_discussion',
                    'before_pay',
                    'purchase_code',
                    'max_cost_definition_order',
                    'tender_plain_info_plain_number',
                    'tender_plain_info_position_number',
                    'tender_plain_info_purchase83st544',
                    'tender_plain_info_plan2017_number',
                    'tender_plain_info_positin2017_number',
                    'tender_plain_info_positin2017_ext_number',
                ],
                'string',
            ],
            [
                ['ntf_main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['ntf_main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_main_id' => Yii::t('app', 'Ntf Main ID'),
            'lot_number' => Yii::t('app', 'Lot Number'),
            'lot_object_info' => Yii::t('app', 'Lot Object Info'),
            'max_price' => Yii::t('app', 'Max Price'),
            'max_price_info' => Yii::t('app', 'Max Price Info'),
            'price_formula' => Yii::t('app', 'Price Formula'),
            'standard_contract_number' => Yii::t('app', 'Standard Contract Number'),
            'currency_code' => Yii::t('app', 'Currency Code'),
            'currency_name' => Yii::t('app', 'Currency Name'),
            'finance_source' => Yii::t('app', 'Finance Source'),
            'interbudgetary_transfer' => Yii::t('app', 'Interbudgetary Transfer'),
            'quantity_undefined' => Yii::t('app', 'Quantity Undefined'),
            'purchase_objects_total_sum' => Yii::t('app', 'Purchase Objects Total Sum'),
            'drug_purchase_objects_info_total' => Yii::t('app', 'Drug Purchase Objects Info Total'),
            'restrict_info' => Yii::t('app', 'Restrict Info'),
            'restrict_foreigns_info' => Yii::t('app', 'Restrict Foreigns Info'),
            'add_info' => Yii::t('app', 'Add Info'),
            'no_public_discussion' => Yii::t('app', 'No Public Discussion'),
            'must_public_discussion' => Yii::t('app', 'Must Public Discussion'),
            'before_pay' => Yii::t('app', 'Before Pay'),
            'purchase_code' => Yii::t('app', 'Purchase Code'),
            'max_cost_definition_order' => Yii::t('app', 'Max Cost Definition Order'),
            'tender_plain_info_plain_number' => Yii::t('app', 'Tender Plain Info Plain Number'),
            'tender_plain_info_position_number' => Yii::t('app', 'Tender Plain Info Position Number'),
            'tender_plain_info_purchase83st544' => Yii::t('app', 'Tender Plain Info Purchase83st544'),
            'tender_plain_info_plan2017_number' => Yii::t('app', 'Tender Plain Info Plan2017 Number'),
            'tender_plain_info_positin2017_number' => Yii::t('app', 'Tender Plain Info Positin2017 Number'),
            'tender_plain_info_positin2017_ext_number' => Yii::t('app', 'Tender Plain Info Positin2017 Ext Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerRequirements()
    {
        return $this->hasMany(CustomerRequirement::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrugPurchaseObjectInfos()
    {
        return $this->hasMany(DrugPurchaseObjectInfo::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'ntf_main_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModification()
    {
        return $this->hasOne(Modification::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkpd()
    {
        return $this->hasOne(Okpd::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkpd2()
    {
        return $this->hasOne(Okpd2::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreferenses()
    {
        return $this->hasMany(Preferense::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicDiscussion()
    {
        return $this->hasOne(PublicDiscussion::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseObjects()
    {
        return $this->hasMany(PurchaseObject::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequirements()
    {
        return $this->hasMany(Requirement::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestrictions()
    {
        return $this->hasMany(Restriction::className(), ['ntf_lot_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\LotQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\LotQuery(get_called_class());
    }

    public function fields()
    {
        return [
            //'id',
            'lot_number',
            'lot_object_info',
            'max_price',
            'max_price_info',
            'price_formula',
            'standard_contract_number',
            'currency_code',
            'currency_name',
            'finance_source',
            'interbudgetary_transfer',
            'quantity_undefined',
            'purchase_objects_total_sum',
            'drug_purchase_objects_info_total',
            'restrict_info',
            'restrict_foreigns_info',
            'add_info',
            'no_public_discussion',
            'must_public_discussion',
            'before_pay',
            'purchase_code',
            'max_cost_definition_order',
            'tender_plain_info_plain_number',
            'tender_plain_info_position_number',
            'tender_plain_info_purchase83st544',
            'tender_plain_info_plan2017_number',
            'tender_plain_info_positin2017_number',
            'tender_plain_info_positin2017_ext_number',
            'customer_requirements' => function ($model) {
                return $model->customerRequirements;
            },
            'drug_purchase_object_infos' => function ($model) {
                return $model->drugPurchaseObjectInfos;
            },
            'purchase_objects' => function ($model) {
                return $model->purchaseObjects;
            },
            'okpd' => function ($model) {
                return $model->okpd;
            },
            'okpd2' => function ($model) {
                return $model->okpd2;
            },
        ];
    }
}
