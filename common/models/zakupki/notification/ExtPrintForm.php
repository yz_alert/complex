<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_ext_print_form}}".
 *
 * @property int $id
 * @property int $ntf_main_id
 * @property string $content
 * @property string $url
 * @property string $file_type
 *
 * @property Main $main
 */
class ExtPrintForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_ext_print_form}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_main_id'], 'default', 'value' => null],
            [['ntf_main_id'], 'integer'],
            [
                [
                    'content',
                    'url',
                    'file_type',
                ],
                'string',
            ],
            [
                ['ntf_main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['ntf_main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_main_id' => Yii::t('app', 'Ntf Main ID'),
            'content' => Yii::t('app', 'Content'),
            'url' => Yii::t('app', 'Url'),
            'file_type' => Yii::t('app', 'File Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'ntf_main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\ExtPrintFormQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\ExtPrintFormQuery(get_called_class());
    }
}
