<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_purchase_responsible_responsible_info}}".
 *
 * @property int $id
 * @property int $ntf_main_id
 * @property string $org_post_address
 * @property string $org_fact_address
 * @property string $contact_person_last_name
 * @property string $contact_person_first_name
 * @property string $contact_person_middle_name
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $contact_fax
 * @property string $add_info
 *
 * @property Main $main
 */
class PurchaseResponsibleResponsibleInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_purchase_responsible_responsible_info}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_main_id'], 'default', 'value' => null],
            [['ntf_main_id'], 'integer'],
            [
                [
                    'org_post_address',
                    'org_fact_address',
                    'contact_person_last_name',
                    'contact_person_first_name',
                    'contact_person_middle_name',
                    'contact_email',
                    'contact_phone',
                    'contact_fax',
                    'add_info',
                ],
                'string',
            ],
            [['ntf_main_id'], 'unique'],
            [
                ['ntf_main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['ntf_main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_main_id' => Yii::t('app', 'Ntf Main ID'),
            'org_post_address' => Yii::t('app', 'Org Post Address'),
            'org_fact_address' => Yii::t('app', 'Org Fact Address'),
            'contact_person_last_name' => Yii::t('app', 'Contact Person Last Name'),
            'contact_person_first_name' => Yii::t('app', 'Contact Person First Name'),
            'contact_person_middle_name' => Yii::t('app', 'Contact Person Middle Name'),
            'contact_email' => Yii::t('app', 'Contact Email'),
            'contact_phone' => Yii::t('app', 'Contact Phone'),
            'contact_fax' => Yii::t('app', 'Contact Fax'),
            'add_info' => Yii::t('app', 'Add Info'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'ntf_main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\PurchaseResponsibleResponsibleInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\PurchaseResponsibleResponsibleInfoQuery(get_called_class());
    }
}
