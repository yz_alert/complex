<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_purchase_responsible_specialized_org}}".
 *
 * @property int $id
 * @property int $ntf_main_id
 * @property string $reg_num
 * @property string $cons_registry_num
 * @property string $full_name
 * @property string $short_name
 * @property string $post_address
 * @property string $fact_address
 * @property string $inn
 * @property string $kpp
 *
 * @property Main $main
 */
class PurchaseResponsibleSpecializedOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_purchase_responsible_specialized_org}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_main_id'], 'default', 'value' => null],
            [['ntf_main_id'], 'integer'],
            [
                [
                    'reg_num',
                    'cons_registry_num',
                    'full_name',
                    'short_name',
                    'post_address',
                    'fact_address',
                    'inn',
                    'kpp',
                ],
                'string',
            ],
            [['ntf_main_id'], 'unique'],
            [
                ['ntf_main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['ntf_main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_main_id' => Yii::t('app', 'Ntf Main ID'),
            'reg_num' => Yii::t('app', 'Reg Num'),
            'cons_registry_num' => Yii::t('app', 'Cons Registry Num'),
            'full_name' => Yii::t('app', 'Full Name'),
            'short_name' => Yii::t('app', 'Short Name'),
            'post_address' => Yii::t('app', 'Post Address'),
            'fact_address' => Yii::t('app', 'Fact Address'),
            'inn' => Yii::t('app', 'Inn'),
            'kpp' => Yii::t('app', 'Kpp'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'ntf_main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\PurchaseResponsibleSpecializedOrgQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\PurchaseResponsibleSpecializedOrgQuery(get_called_class());
    }
}
