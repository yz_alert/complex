<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_customer_quantity}}".
 *
 * @property int $id
 * @property int $ntf_purchase_object_id
 * @property string $customer_reg_num
 * @property string $customer_cons_registry_num
 * @property string $customer_full_name
 * @property string $quantity
 *
 * @property PurchaseObject $purchaseObject
 */
class CustomerQuantity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_customer_quantity}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_purchase_object_id'], 'default', 'value' => null],
            [['ntf_purchase_object_id'], 'integer'],
            [
                [
                    'customer_reg_num',
                    'customer_cons_registry_num',
                    'customer_full_name',
                    'quantity',
                ],
                'string',
            ],
            [
                ['ntf_purchase_object_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PurchaseObject::className(),
                'targetAttribute' => ['ntf_purchase_object_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_purchase_object_id' => Yii::t('app', 'Ntf Purchase Object ID'),
            'customer_reg_num' => Yii::t('app', 'Customer Reg Num'),
            'customer_cons_registry_num' => Yii::t('app', 'Customer Cons Registry Num'),
            'customer_full_name' => Yii::t('app', 'Customer Full Name'),
            'quantity' => Yii::t('app', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseObject()
    {
        return $this->hasOne(PurchaseObject::className(), ['id' => 'ntf_purchase_object_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\CustomerQuantityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\CustomerQuantityQuery(get_called_class());
    }
}
