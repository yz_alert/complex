<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_drug_purchase_object_info}}".
 *
 * @property int $id
 * @property int $ntf_lot_id
 * @property string $isznvlp
 * @property string $drug_quantity_customers_info_total
 * @property string $price_per_unit
 * @property string $position_price
 *
 * @property Lot $lot
 * @property DrugQuantityCustomerInfo[] $drugQuantityCustomerInfos
 * @property ObjectInfoUsingReferenceInfo[] $objectInfoUsingReferenceInfos
 * @property ObjectInfoUsingTextForm[] $objectInfoUsingTextForms
 */
class DrugPurchaseObjectInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_drug_purchase_object_info}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_lot_id'], 'default', 'value' => null],
            [['ntf_lot_id'], 'integer'],
            [
                [
                    'isznvlp',
                    'drug_quantity_customers_info_total',
                    'price_per_unit',
                    'position_price',
                ],
                'string',
            ],
            [
                ['ntf_lot_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lot::className(),
                'targetAttribute' => ['ntf_lot_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_lot_id' => Yii::t('app', 'Ntf Lot ID'),
            'isznvlp' => Yii::t('app', 'Isznvlp'),
            'drug_quantity_customers_info_total' => Yii::t('app', 'Drug Quantity Customers Info Total'),
            'price_per_unit' => Yii::t('app', 'Price Per Unit'),
            'position_price' => Yii::t('app', 'Position Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'ntf_lot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrugQuantityCustomerInfos()
    {
        return $this->hasMany(DrugQuantityCustomerInfo::className(), ['ntf_drug_purchase_object_info_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectInfoUsingReferenceInfos()
    {
        return $this->hasMany(ObjectInfoUsingReferenceInfo::className(), ['ntf_drug_purchase_object_info_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectInfoUsingTextForms()
    {
        return $this->hasMany(ObjectInfoUsingTextForm::className(), ['ntf_drug_purchase_object_info_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\DrugPurchaseObjectInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\DrugPurchaseObjectInfoQuery(get_called_class());
    }

    public function fields()
    {
        return [
            //'id',
            'isznvlp',
            'drug_quantity_customers_info_total',
            'price_per_unit',
            'position_price',
            'drug_quantity_customer_infos' => function ($model) {
                return $model->drugQuantityCustomerInfos;
            },
            'object_info_using_reference_infos' => function ($model) {
                return $model->objectInfoUsingReferenceInfos;
            },
            'object_info_using_text_forms' => function ($model) {
                return $model->objectInfoUsingTextForms;
            },
        ];
    }

}
