<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_public_discussion}}".
 *
 * @property int $id
 * @property int $ntf_lot_id
 * @property string $number
 * @property string $organization_ch5_st15
 * @property string $href
 * @property string $place
 * @property string $public_discussion2017_p_d_l_p_p2_protocol_date
 * @property string $public_discussion2017_p_d_l_p_p2_protocol_publish_date
 * @property string $public_discussion2017_p_d_l_p_p2_public_discussion_phase2_num
 * @property string $public_discussion2017_p_d_l_p_p2_href_phase2
 *
 * @property Lot $lot
 */
class PublicDiscussion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_public_discussion}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_lot_id'], 'default', 'value' => null],
            [['ntf_lot_id'], 'integer'],
            [
                [
                    'number',
                    'organization_ch5_st15',
                    'href',
                    'place',
                    'public_discussion2017_p_d_l_p_p2_protocol_date',
                    'public_discussion2017_p_d_l_p_p2_protocol_publish_date',
                    'public_discussion2017_p_d_l_p_p2_public_discussion_phase2_num',
                    'public_discussion2017_p_d_l_p_p2_href_phase2',
                ],
                'string',
            ],
            [['ntf_lot_id'], 'unique'],
            [
                ['ntf_lot_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lot::className(),
                'targetAttribute' => ['ntf_lot_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_lot_id' => Yii::t('app', 'Ntf Lot ID'),
            'number' => Yii::t('app', 'Number'),
            'organization_ch5_st15' => Yii::t('app', 'Organization Ch5 St15'),
            'href' => Yii::t('app', 'Href'),
            'place' => Yii::t('app', 'Place'),
            'public_discussion2017_p_d_l_p_p2_protocol_date' => Yii::t('app',
                'Public Discussion2017 P D L P P2 Protocol Date'),
            'public_discussion2017_p_d_l_p_p2_protocol_publish_date' => Yii::t('app',
                'Public Discussion2017 P D L P P2 Protocol Publish Date'),
            'public_discussion2017_p_d_l_p_p2_public_discussion_phase2_num' => Yii::t('app',
                'Public Discussion2017 P D L P P2 Public Discussion Phase2 Num'),
            'public_discussion2017_p_d_l_p_p2_href_phase2' => Yii::t('app',
                'Public Discussion2017 P D L P P2 Href Phase2'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'ntf_lot_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\PublicDiscussionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\PublicDiscussionQuery(get_called_class());
    }
}
