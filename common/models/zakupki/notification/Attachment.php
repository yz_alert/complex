<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_attachment}}".
 *
 * @property int $id
 * @property int $ntf_lot_id
 * @property string $published_content_id
 * @property string $file_name
 * @property string $file_size
 * @property string $doc_description
 * @property string $doc_date
 * @property string $url
 * @property string $content_id
 * @property string $content
 * @property string $crypto_signs_signature_type
 *
 * @property Lot $lot
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_attachment}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_lot_id'], 'default', 'value' => null],
            [['ntf_lot_id'], 'integer'],
            [
                [
                    'published_content_id',
                    'file_name',
                    'file_size',
                    'doc_description',
                    'doc_date',
                    'url',
                    'content_id',
                    'content',
                    'crypto_signs_signature_type',
                ],
                'string',
            ],
            [
                ['ntf_lot_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lot::className(),
                'targetAttribute' => ['ntf_lot_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_lot_id' => Yii::t('app', 'Ntf Lot ID'),
            'published_content_id' => Yii::t('app', 'Published Content ID'),
            'file_name' => Yii::t('app', 'File Name'),
            'file_size' => Yii::t('app', 'File Size'),
            'doc_description' => Yii::t('app', 'Doc Description'),
            'doc_date' => Yii::t('app', 'Doc Date'),
            'url' => Yii::t('app', 'Url'),
            'content_id' => Yii::t('app', 'Content ID'),
            'content' => Yii::t('app', 'Content'),
            'crypto_signs_signature_type' => Yii::t('app', 'Crypto Signs Signature Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'ntf_lot_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\AttachmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\AttachmentQuery(get_called_class());
    }
}
