<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_placing_way}}".
 *
 * @property int $id
 * @property int $ntf_main_id
 * @property string $code
 * @property string $name
 *
 * @property Main $main
 */
class PlacingWay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_placing_way}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_main_id'], 'default', 'value' => null],
            [['ntf_main_id'], 'integer'],
            [
                [
                    'code',
                    'name',
                ],
                'string',
            ],
            [['ntf_main_id'], 'unique'],
            [
                ['ntf_main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['ntf_main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_main_id' => Yii::t('app', 'Ntf Main ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'ntf_main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\PlacingWayQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\PlacingWayQuery(get_called_class());
    }
}
