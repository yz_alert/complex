<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_drug_quantity_customer_info}}".
 *
 * @property int $id
 * @property int $ntf_drug_purchase_object_info_id
 * @property string $customer_reg_num
 * @property string $customer_cons_registry_num
 * @property string $customer_full_name
 * @property string $quantity
 *
 * @property DrugPurchaseObjectInfo $drugPurchaseObjectInfo
 */
class DrugQuantityCustomerInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_drug_quantity_customer_info}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_drug_purchase_object_info_id'], 'default', 'value' => null],
            [['ntf_drug_purchase_object_info_id'], 'integer'],
            [
                [
                    'customer_reg_num',
                    'customer_cons_registry_num',
                    'customer_full_name',
                    'quantity',
                ],
                'string',
            ],
            [
                ['ntf_drug_purchase_object_info_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DrugPurchaseObjectInfo::className(),
                'targetAttribute' => ['ntf_drug_purchase_object_info_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_drug_purchase_object_info_id' => Yii::t('app', 'Ntf Drug Purchase Object Info ID'),
            'customer_reg_num' => Yii::t('app', 'Customer Reg Num'),
            'customer_cons_registry_num' => Yii::t('app', 'Customer Cons Registry Num'),
            'customer_full_name' => Yii::t('app', 'Customer Full Name'),
            'quantity' => Yii::t('app', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrugPurchaseObjectInfo()
    {
        return $this->hasOne(DrugPurchaseObjectInfo::className(), ['id' => 'ntf_drug_purchase_object_info_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\DrugQuantityCustomerInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\DrugQuantityCustomerInfoQuery(get_called_class());
    }
}
