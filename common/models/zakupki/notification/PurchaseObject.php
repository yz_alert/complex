<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_purchase_object}}".
 *
 * @property int $id
 * @property int $ntf_lot_id
 * @property string $okpd_code
 * @property string $okpd_name
 * @property string $okpd2_code
 * @property string $okpd2_name
 * @property string $name
 * @property string $okei_code
 * @property string $okei_national_code
 * @property string $price
 * @property string $quantity_value
 * @property string $quantity_undefined
 * @property string $sum
 *
 * @property CustomerQuantity[] $customerQuantities
 * @property Lot $lot
 */
class PurchaseObject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_purchase_object}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_lot_id'], 'default', 'value' => null],
            [['ntf_lot_id'], 'integer'],
            [
                [
                    'okpd_code',
                    'okpd_name',
                    'okpd2_code',
                    'okpd2_name',
                    'name',
                    'okei_code',
                    'okei_national_code',
                    'price',
                    'quantity_value',
                    'quantity_undefined',
                    'sum',
                ],
                'string',
            ],
            [
                ['ntf_lot_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lot::className(),
                'targetAttribute' => ['ntf_lot_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_lot_id' => Yii::t('app', 'Ntf Lot ID'),
            'okpd_code' => Yii::t('app', 'Okpd Code'),
            'okpd_name' => Yii::t('app', 'Okpd Name'),
            'okpd2_code' => Yii::t('app', 'Okpd2 Code'),
            'okpd2_name' => Yii::t('app', 'Okpd2 Name'),
            'name' => Yii::t('app', 'Name'),
            'okei_code' => Yii::t('app', 'Okei Code'),
            'okei_national_code' => Yii::t('app', 'Okei National Code'),
            'price' => Yii::t('app', 'Price'),
            'quantity_value' => Yii::t('app', 'Quantity Value'),
            'quantity_undefined' => Yii::t('app', 'Quantity Undefined'),
            'sum' => Yii::t('app', 'Sum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerQuantities()
    {
        return $this->hasMany(CustomerQuantity::className(), ['ntf_purchase_object_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'ntf_lot_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\PurchaseObjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\PurchaseObjectQuery(get_called_class());
    }

    public function fields()
    {
        return [
            //'id',
            'okpd_code',
            'okpd_name',
            'okpd2_code',
            'okpd2_name',
            'name',
            'okei_code',
            'okei_national_code',
            'price',
            'quantity_value',
            'quantity_undefined',
            'sum',
            'customer_quantities' => function ($model) {
                return $model->customerQuantities;
            },
        ];
    }

}
