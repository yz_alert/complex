<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_procedure_info_collecting}}".
 *
 * @property int $id
 * @property int $ntf_main_id
 * @property string $start_date
 * @property string $place
 * @property string $ntf_order
 * @property string $end_date
 * @property string $form
 *
 * @property Main $main
 */
class ProcedureInfoCollecting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_procedure_info_collecting}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_main_id'], 'default', 'value' => null],
            [['ntf_main_id'], 'integer'],
            [
                [
                    'start_date',
                    'place',
                    'ntf_order',
                    'end_date',
                    'form',
                ],
                'string',
            ],
            [['ntf_main_id'], 'unique'],
            [
                ['ntf_main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['ntf_main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_main_id' => Yii::t('app', 'Ntf Main ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'place' => Yii::t('app', 'Place'),
            'ntf_order' => Yii::t('app', 'Ntf Order'),
            'end_date' => Yii::t('app', 'End Date'),
            'form' => Yii::t('app', 'Form'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'ntf_main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\ProcedureInfoCollectingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\ProcedureInfoCollectingQuery(get_called_class());
    }
}
