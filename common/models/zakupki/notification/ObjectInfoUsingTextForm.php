<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_object_info_using_text_form}}".
 *
 * @property int $id
 * @property int $ntf_drug_purchase_object_info_id
 * @property string $drug_info_mmn_info_mmn_name
 * @property string $drug_info_trade_info_trade_name
 * @property string $drug_info_medicamental_form_info_medicamental_form_name
 * @property string $drug_info_dosage_info_dosage_grls_value
 * @property string $drug_info_packaging_info_packaging1_quantity
 * @property string $drug_info_packaging_info_packaging2_quantity
 * @property string $drug_info_packaging_info_summary_packaging_quantity
 * @property string $drug_info_manual_user_okei_code
 * @property string $drug_info_manual_user_okei_name
 * @property string $drug_info_basic_unit
 * @property string $drug_info_drug_quantity
 * @property string $must_specify_drug_package_specify_drug_package_reason
 *
 * @property DrugPurchaseObjectInfo $drugPurchaseObjectInfo
 */
class ObjectInfoUsingTextForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_object_info_using_text_form}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_drug_purchase_object_info_id'], 'default', 'value' => null],
            [['ntf_drug_purchase_object_info_id'], 'integer'],
            [
                [
                    'drug_info_mmn_info_mmn_name',
                    'drug_info_trade_info_trade_name',
                    'drug_info_medicamental_form_info_medicamental_form_name',
                    'drug_info_dosage_info_dosage_grls_value',
                    'drug_info_packaging_info_packaging1_quantity',
                    'drug_info_packaging_info_packaging2_quantity',
                    'drug_info_packaging_info_summary_packaging_quantity',
                    'drug_info_manual_user_okei_code',
                    'drug_info_manual_user_okei_name',
                    'drug_info_basic_unit',
                    'drug_info_drug_quantity',
                    'must_specify_drug_package_specify_drug_package_reason',
                ],
                'string',
            ],
            [
                ['ntf_drug_purchase_object_info_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => DrugPurchaseObjectInfo::className(),
                'targetAttribute' => ['ntf_drug_purchase_object_info_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_drug_purchase_object_info_id' => Yii::t('app', 'Ntf Drug Purchase Object Info ID'),
            'drug_info_mmn_info_mmn_name' => Yii::t('app', 'Drug Info Mmn Info Mmn Name'),
            'drug_info_trade_info_trade_name' => Yii::t('app', 'Drug Info Trade Info Trade Name'),
            'drug_info_medicamental_form_info_medicamental_form_name' => Yii::t('app',
                'Drug Info Medicamental Form Info Medicamental Form Name'),
            'drug_info_dosage_info_dosage_grls_value' => Yii::t('app', 'Drug Info Dosage Info Dosage Grls Value'),
            'drug_info_packaging_info_packaging1_quantity' => Yii::t('app',
                'Drug Info Packaging Info Packaging1 Quantity'),
            'drug_info_packaging_info_packaging2_quantity' => Yii::t('app',
                'Drug Info Packaging Info Packaging2 Quantity'),
            'drug_info_packaging_info_summary_packaging_quantity' => Yii::t('app',
                'Drug Info Packaging Info Summary Packaging Quantity'),
            'drug_info_manual_user_okei_code' => Yii::t('app', 'Drug Info Manual User Okei Code'),
            'drug_info_manual_user_okei_name' => Yii::t('app', 'Drug Info Manual User Okei Name'),
            'drug_info_basic_unit' => Yii::t('app', 'Drug Info Basic Unit'),
            'drug_info_drug_quantity' => Yii::t('app', 'Drug Info Drug Quantity'),
            'must_specify_drug_package_specify_drug_package_reason' => Yii::t('app',
                'Must Specify Drug Package Specify Drug Package Reason'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrugPurchaseObjectInfo()
    {
        return $this->hasOne(DrugPurchaseObjectInfo::className(), ['id' => 'ntf_drug_purchase_object_info_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\ObjectInfoUsingTextFormQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\ObjectInfoUsingTextFormQuery(get_called_class());
    }
}
