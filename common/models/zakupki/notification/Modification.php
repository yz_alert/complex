<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_modification}}".
 *
 * @property int $id
 * @property int $ntf_lot_id
 * @property string $modification_number
 * @property string $info
 * @property string $add_info
 * @property string $reason_responsible_decision_decision_date
 * @property string $reason_authority_prescription_reestr_prescription_check_result_
 * @property string $reason_authority_prescription_reestr_prescription_prescription_
 * @property string $reason_authority_prescription_reestr_prescription_foundation
 * @property string $reason_authority_prescription_reestr_prescription_authority_nam
 * @property string $reason_authority_prescription_reestr_prescription_doc_date
 * @property string $reason_authority_prescription_external_prescription_authority_n
 * @property string $reason_authority_prescription_external_prescription_authority_t
 * @property string $reason_authority_prescription_external_prescription_doc_name
 * @property string $reason_authority_prescription_external_prescription_doc_date
 * @property string $reason_authority_prescription_external_prescription_doc_number
 * @property string $reason_court_decision_court_name
 * @property string $reason_court_decision_doc_name
 * @property string $reason_court_decision_doc_date
 * @property string $reason_court_decision_doc_number
 * @property string $reason_discussion_result_doc_name
 * @property string $reason_discussion_result_doc_date
 * @property string $reason_discussion_result_doc_number
 *
 * @property Lot $lot
 */
class Modification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_modification}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_lot_id'], 'default', 'value' => null],
            [['ntf_lot_id'], 'integer'],
            [
                [
                    'modification_number',
                    'info',
                    'add_info',
                    'reason_responsible_decision_decision_date',
                    'reason_authority_prescription_reestr_prescription_check_result_',
                    'reason_authority_prescription_reestr_prescription_prescription_',
                    'reason_authority_prescription_reestr_prescription_foundation',
                    'reason_authority_prescription_reestr_prescription_authority_nam',
                    'reason_authority_prescription_reestr_prescription_doc_date',
                    'reason_authority_prescription_external_prescription_authority_n',
                    'reason_authority_prescription_external_prescription_authority_t',
                    'reason_authority_prescription_external_prescription_doc_name',
                    'reason_authority_prescription_external_prescription_doc_date',
                    'reason_authority_prescription_external_prescription_doc_number',
                    'reason_court_decision_court_name',
                    'reason_court_decision_doc_name',
                    'reason_court_decision_doc_date',
                    'reason_court_decision_doc_number',
                    'reason_discussion_result_doc_name',
                    'reason_discussion_result_doc_date',
                    'reason_discussion_result_doc_number',
                ],
                'string',
            ],
            [['ntf_lot_id'], 'unique'],
            [
                ['ntf_lot_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Lot::className(),
                'targetAttribute' => ['ntf_lot_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_lot_id' => Yii::t('app', 'Ntf Lot ID'),
            'modification_number' => Yii::t('app', 'Modification Number'),
            'info' => Yii::t('app', 'Info'),
            'add_info' => Yii::t('app', 'Add Info'),
            'reason_responsible_decision_decision_date' => Yii::t('app', 'Reason Responsible Decision Decision Date'),
            'reason_authority_prescription_reestr_prescription_check_result_' => Yii::t('app',
                'Reason Authority Prescription Reestr Prescription Check Result'),
            'reason_authority_prescription_reestr_prescription_prescription_' => Yii::t('app',
                'Reason Authority Prescription Reestr Prescription Prescription'),
            'reason_authority_prescription_reestr_prescription_foundation' => Yii::t('app',
                'Reason Authority Prescription Reestr Prescription Foundation'),
            'reason_authority_prescription_reestr_prescription_authority_nam' => Yii::t('app',
                'Reason Authority Prescription Reestr Prescription Authority Nam'),
            'reason_authority_prescription_reestr_prescription_doc_date' => Yii::t('app',
                'Reason Authority Prescription Reestr Prescription Doc Date'),
            'reason_authority_prescription_external_prescription_authority_n' => Yii::t('app',
                'Reason Authority Prescription External Prescription Authority N'),
            'reason_authority_prescription_external_prescription_authority_t' => Yii::t('app',
                'Reason Authority Prescription External Prescription Authority T'),
            'reason_authority_prescription_external_prescription_doc_name' => Yii::t('app',
                'Reason Authority Prescription External Prescription Doc Name'),
            'reason_authority_prescription_external_prescription_doc_date' => Yii::t('app',
                'Reason Authority Prescription External Prescription Doc Date'),
            'reason_authority_prescription_external_prescription_doc_number' => Yii::t('app',
                'Reason Authority Prescription External Prescription Doc Number'),
            'reason_court_decision_court_name' => Yii::t('app', 'Reason Court Decision Court Name'),
            'reason_court_decision_doc_name' => Yii::t('app', 'Reason Court Decision Doc Name'),
            'reason_court_decision_doc_date' => Yii::t('app', 'Reason Court Decision Doc Date'),
            'reason_court_decision_doc_number' => Yii::t('app', 'Reason Court Decision Doc Number'),
            'reason_discussion_result_doc_name' => Yii::t('app', 'Reason Discussion Result Doc Name'),
            'reason_discussion_result_doc_date' => Yii::t('app', 'Reason Discussion Result Doc Date'),
            'reason_discussion_result_doc_number' => Yii::t('app', 'Reason Discussion Result Doc Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'ntf_lot_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\ModificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\ModificationQuery(get_called_class());
    }
}
