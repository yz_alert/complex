<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_budget_financing}}".
 *
 * @property int $id
 * @property int $ntf_customer_requirement_id
 * @property string $kbk_code
 * @property string $kbk_code2016
 * @property string $year
 * @property string $sum
 *
 * @property CustomerRequirement $customerRequirement
 */
class BudgetFinancing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_budget_financing}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_customer_requirement_id'], 'default', 'value' => null],
            [['ntf_customer_requirement_id'], 'integer'],
            [
                [
                    'kbk_code',
                    'kbk_code2016',
                    'year',
                    'sum',
                ],
                'string',
            ],
            [
                ['ntf_customer_requirement_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CustomerRequirement::className(),
                'targetAttribute' => ['ntf_customer_requirement_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_customer_requirement_id' => Yii::t('app', 'Ntf Customer Requirement ID'),
            'kbk_code' => Yii::t('app', 'Kbk Code'),
            'kbk_code2016' => Yii::t('app', 'Kbk Code2016'),
            'year' => Yii::t('app', 'Year'),
            'sum' => Yii::t('app', 'Sum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerRequirement()
    {
        return $this->hasOne(CustomerRequirement::className(), ['id' => 'ntf_customer_requirement_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\BudgetFinancingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\BudgetFinancingQuery(get_called_class());
    }
}
