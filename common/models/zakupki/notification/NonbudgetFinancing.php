<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_nonbudget_financing}}".
 *
 * @property int $id
 * @property int $ntf_customer_requirement_id
 * @property string $kosgu_code
 * @property string $kvr_code
 * @property string $year
 * @property string $sum
 *
 * @property CustomerRequirement $customerRequirement
 */
class NonbudgetFinancing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_nonbudget_financing}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_customer_requirement_id'], 'default', 'value' => null],
            [['ntf_customer_requirement_id'], 'integer'],
            [
                [
                    'kosgu_code',
                    'kvr_code',
                    'year',
                    'sum',
                ],
                'string',
            ],
            [
                ['ntf_customer_requirement_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CustomerRequirement::className(),
                'targetAttribute' => ['ntf_customer_requirement_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_customer_requirement_id' => Yii::t('app', 'Ntf Customer Requirement ID'),
            'kosgu_code' => Yii::t('app', 'Kosgu Code'),
            'kvr_code' => Yii::t('app', 'Kvr Code'),
            'year' => Yii::t('app', 'Year'),
            'sum' => Yii::t('app', 'Sum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerRequirement()
    {
        return $this->hasOne(CustomerRequirement::className(), ['id' => 'ntf_customer_requirement_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\NonbudgetFinancingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\NonbudgetFinancingQuery(get_called_class());
    }
}
