<?php

namespace common\models\zakupki\notification;

use Yii;

/**
 * This is the model class for table "{{%ntf_kladr_place}}".
 *
 * @property int $id
 * @property int $ntf_customer_requirement_id
 * @property string $kladr_kladr_type
 * @property string $kladr_kladr_code
 * @property string $kladr_full_name
 * @property string $country_country_code
 * @property string $country_country_full_name
 * @property string $no_kladr_for_region_settlement
 * @property string $delivery_place
 * @property string $no_kladr_for_region_settlement_region
 * @property string $no_kladr_for_region_settlement_settlement
 *
 * @property CustomerRequirement $customerRequirement
 */
class KladrPlace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ntf_kladr_place}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->db_zakupki;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ntf_customer_requirement_id'], 'default', 'value' => null],
            [['ntf_customer_requirement_id'], 'integer'],
            [
                [
                    'kladr_kladr_type',
                    'kladr_kladr_code',
                    'kladr_full_name',
                    'country_country_code',
                    'country_country_full_name',
                    'no_kladr_for_region_settlement',
                    'delivery_place',
                    'no_kladr_for_region_settlement_region',
                    'no_kladr_for_region_settlement_settlement',
                ],
                'string',
            ],
            [
                ['ntf_customer_requirement_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CustomerRequirement::className(),
                'targetAttribute' => ['ntf_customer_requirement_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ntf_customer_requirement_id' => Yii::t('app', 'Ntf Customer Requirement ID'),
            'kladr_kladr_type' => Yii::t('app', 'Kladr Kladr Type'),
            'kladr_kladr_code' => Yii::t('app', 'Kladr Kladr Code'),
            'kladr_full_name' => Yii::t('app', 'Kladr Full Name'),
            'country_country_code' => Yii::t('app', 'Country Country Code'),
            'country_country_full_name' => Yii::t('app', 'Country Country Full Name'),
            'no_kladr_for_region_settlement' => Yii::t('app', 'No Kladr For Region Settlement'),
            'delivery_place' => Yii::t('app', 'Delivery Place'),
            'no_kladr_for_region_settlement_region' => Yii::t('app', 'No Kladr For Region Settlement Region'),
            'no_kladr_for_region_settlement_settlement' => Yii::t('app', 'No Kladr For Region Settlement Settlement'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerRequirement()
    {
        return $this->hasOne(CustomerRequirement::className(), ['id' => 'ntf_customer_requirement_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\notification\query\KladrPlaceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\notification\query\KladrPlaceQuery(get_called_class());
    }
}
