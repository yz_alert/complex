<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%country}}".
 *
 * @property int $id
 * @property string $country_code
 * @property string $country_full_name
 *
 * @property IndividualPersonForeignState[] $individualPersonForeignStates
 * @property LegalEntityForeignState[] $legalEntityForeignStates
 * @property Supplier[] $suppliers
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'country_code',
                    'country_full_name',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_code' => Yii::t('app', 'Country Code'),
            'country_full_name' => Yii::t('app', 'Country Full Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndividualPersonForeignStates()
    {
        return $this->hasMany(IndividualPersonForeignState::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalEntityForeignStates()
    {
        return $this->hasMany(LegalEntityForeignState::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Supplier::className(), ['country_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\CountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\CountryQuery(get_called_class());
    }
}
