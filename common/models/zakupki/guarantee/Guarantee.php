<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%guarantee}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $major_repair_ensure_ef_number
 * @property string $guarantee_number
 * @property string $guarantee_amount
 * @property string $expire_date
 * @property string $entry_force_date
 * @property string $guarantee_procedure
 * @property string $guarantee_amount_rur
 * @property string $currency_rate
 * @property string $guarantee_date
 * @property int $purchase_request_ensure_id
 * @property int $contract_execution_ensure_id
 * @property int $currency_id
 * @property int $customer_id
 *
 * @property ContractExecutionEnsure $contractExecutionEnsure
 * @property Currency $currency
 * @property Customer $customer
 * @property Main $main
 * @property GuaranteeAdditionalInfo $guaranteeAdditionalInfo
 * @property PurchaseCode[] $purchaseCodes
 * @property PurchaseRequestEnsure $purchaseRequestEnsure
 */
class Guarantee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%guarantee}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'main_id',
                    'purchase_request_ensure_id',
                    'contract_execution_ensure_id',
                    'currency_id',
                    'customer_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'main_id',
                    'purchase_request_ensure_id',
                    'contract_execution_ensure_id',
                    'currency_id',
                    'customer_id',
                ],
                'integer',
            ],
            [
                [
                    'guarantee_date',
                    'major_repair_ensure_ef_number',
                    'guarantee_number',
                    'guarantee_amount',
                    'expire_date',
                    'entry_force_date',
                    'guarantee_procedure',
                    'guarantee_amount_rur',
                    'currency_rate',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['currency_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Currency::className(),
                'targetAttribute' => ['currency_id' => 'id'],
            ],
            [
                ['customer_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Customer::className(),
                'targetAttribute' => ['customer_id' => 'id'],
            ],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'major_repair_ensure_ef_number' => Yii::t('app', 'Major Repair Ensure Ef Number'),
            'guarantee_date' => Yii::t('app', 'Guarantee Date'),
            'guarantee_number' => Yii::t('app', 'Guarantee Number'),
            'guarantee_amount' => Yii::t('app', 'Guarantee Amount'),
            'expire_date' => Yii::t('app', 'Expire Date'),
            'entry_force_date' => Yii::t('app', 'Entry Force Date'),
            'guarantee_procedure' => Yii::t('app', 'Guarantee Procedure'),
            'guarantee_amount_rur' => Yii::t('app', 'Guarantee Amount Rur'),
            'currency_rate' => Yii::t('app', 'Currency Rate'),
            'purchase_request_ensure_id' => Yii::t('app', 'Purchase Request Ensure ID'),
            'contract_execution_ensure_id' => Yii::t('app', 'Contract Execution Ensure ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractExecutionEnsure()
    {
        return $this->hasOne(ContractExecutionEnsure::className(), ['guarantee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuaranteeAdditionalInfo()
    {
        return $this->hasOne(GuaranteeAdditionalInfo::className(), ['guarantee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseCodes()
    {
        return $this->hasMany(PurchaseCode::className(), ['guarantee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseRequestEnsure()
    {
        return $this->hasOne(PurchaseRequestEnsure::className(), ['guarantee_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\GuaranteeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\GuaranteeQuery(get_called_class());
    }
}
