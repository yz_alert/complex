<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%supplier}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $participant_type
 * @property string $inn
 * @property string $kpp
 * @property string $ogrn
 * @property string $id_number
 * @property string $id_number_extension
 * @property string $organization_name
 * @property string $firm_name
 * @property string $fact_address
 * @property string $post_address
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $contact_fax
 * @property string $additional_info
 * @property string $status
 * @property int $legal_form_id
 * @property int $country_id
 * @property int $contact_info_id
 *
 * @property ContactInfo $contactInfo
 * @property Country $country
 * @property LegalForm $legalForm
 * @property Main $main
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%supplier}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'main_id',
                    'legal_form_id',
                    'country_id',
                    'contact_info_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'main_id',
                    'legal_form_id',
                    'country_id',
                    'contact_info_id',
                ],
                'integer',
            ],
            [
                [
                    'participant_type',
                    'inn',
                    'kpp',
                    'ogrn',
                    'id_number',
                    'id_number_extension',
                    'organization_name',
                    'firm_name',
                    'fact_address',
                    'post_address',
                    'contact_email',
                    'contact_phone',
                    'contact_fax',
                    'additional_info',
                    'status',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['contact_info_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ContactInfo::className(),
                'targetAttribute' => ['contact_info_id' => 'id'],
            ],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id'],
            ],
            [
                ['legal_form_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LegalForm::className(),
                'targetAttribute' => ['legal_form_id' => 'id'],
            ],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'participant_type' => Yii::t('app', 'Participant Type'),
            'inn' => Yii::t('app', 'Inn'),
            'kpp' => Yii::t('app', 'Kpp'),
            'ogrn' => Yii::t('app', 'Ogrn'),
            'id_number' => Yii::t('app', 'Id Number'),
            'id_number_extension' => Yii::t('app', 'Id Number Extension'),
            'organization_name' => Yii::t('app', 'Organization Name'),
            'firm_name' => Yii::t('app', 'Firm Name'),
            'fact_address' => Yii::t('app', 'Fact Address'),
            'post_address' => Yii::t('app', 'Post Address'),
            'contact_email' => Yii::t('app', 'Contact Email'),
            'contact_phone' => Yii::t('app', 'Contact Phone'),
            'contact_fax' => Yii::t('app', 'Contact Fax'),
            'additional_info' => Yii::t('app', 'Additional Info'),
            'status' => Yii::t('app', 'Status'),
            'legal_form_id' => Yii::t('app', 'Legal Form ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'contact_info_id' => Yii::t('app', 'Contact Info ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactInfo()
    {
        return $this->hasOne(ContactInfo::className(), ['id' => 'contact_info_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalForm()
    {
        return $this->hasOne(LegalForm::className(), ['id' => 'legal_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\SupplierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\SupplierQuery(get_called_class());
    }
}
