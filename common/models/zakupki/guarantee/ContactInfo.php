<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%contact_info}}".
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 *
 * @property IndividualPersonForeignState[] $individualPersonForeignStates
 * @property IndividualPersonRf[] $individualPersonRves
 * @property Supplier[] $suppliers
 */
class ContactInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contact_info}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'last_name',
                    'first_name',
                    'middle_name',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'last_name' => Yii::t('app', 'Last Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndividualPersonForeignStates()
    {
        return $this->hasMany(IndividualPersonForeignState::className(), ['contact_info_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndividualPersonRves()
    {
        return $this->hasMany(IndividualPersonRf::className(), ['contact_info_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Supplier::className(), ['contact_info_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\ContactInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\ContactInfoQuery(get_called_class());
    }
}
