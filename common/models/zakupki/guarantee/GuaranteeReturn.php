<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%guarantee_return}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $bgr_reg_number
 * @property string $bgr_doc_number
 * @property string $bgr_return_date
 * @property string $bgr_return_reason
 * @property string $bgr_return_publish_date
 * @property string $wn_reg_number
 * @property string $wn_doc_number
 * @property string $wn_notice_date
 * @property string $wn_notice_number
 * @property string $wn_notice_reason
 * @property string $wn_notice_publish_date
 *
 * @property Main $main
 */
class GuaranteeReturn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%guarantee_return}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_id'], 'default', 'value' => null],
            [['main_id'], 'integer'],
            [
                [
                    'bgr_reg_number',
                    'bgr_doc_number',
                    'bgr_return_date',
                    'bgr_return_reason',
                    'bgr_return_publish_date',
                    'wn_reg_number',
                    'wn_doc_number',
                    'wn_notice_date',
                    'wn_notice_number',
                    'wn_notice_reason',
                    'wn_notice_publish_date',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'bgr_reg_number' => Yii::t('app', 'Bgr Reg Number'),
            'bgr_doc_number' => Yii::t('app', 'Bgr Doc Number'),
            'bgr_return_date' => Yii::t('app', 'Bgr Return Date'),
            'bgr_return_reason' => Yii::t('app', 'Bgr Return Reason'),
            'bgr_return_publish_date' => Yii::t('app', 'Bgr Return Publish Date'),
            'wn_reg_number' => Yii::t('app', 'Wn Reg Number'),
            'wn_doc_number' => Yii::t('app', 'Wn Doc Number'),
            'wn_notice_date' => Yii::t('app', 'Wn Notice Date'),
            'wn_notice_number' => Yii::t('app', 'Wn Notice Number'),
            'wn_notice_reason' => Yii::t('app', 'Wn Notice Reason'),
            'wn_notice_publish_date' => Yii::t('app', 'Wn Notice Publish Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\GuaranteeReturnQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\GuaranteeReturnQuery(get_called_class());
    }
}
