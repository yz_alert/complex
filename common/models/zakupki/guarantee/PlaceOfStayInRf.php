<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%place_of_stay_in_rf}}".
 *
 * @property int $id
 * @property int $legal_entity_foreign_state_id
 * @property string $address
 * @property int $subject_rf_id
 * @property int $okato_id
 * @property int $oktmo_id
 *
 * @property LegalEntityForeignState $legalEntityForeignState
 * @property Okato $okato
 * @property Oktmo $oktmo
 */
class PlaceOfStayInRf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%place_of_stay_in_rf}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'legal_entity_foreign_state_id',
                    'subject_rf_id',
                    'okato_id',
                    'oktmo_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'legal_entity_foreign_state_id',
                    'subject_rf_id',
                    'okato_id',
                    'oktmo_id',
                ],
                'integer',
            ],
            [['address'], 'string'],
            [['legal_entity_foreign_state_id'], 'unique'],
            [
                ['legal_entity_foreign_state_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LegalEntityForeignState::className(),
                'targetAttribute' => ['legal_entity_foreign_state_id' => 'id'],
            ],
            [
                ['okato_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Okato::className(),
                'targetAttribute' => ['okato_id' => 'id'],
            ],
            [
                ['oktmo_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Oktmo::className(),
                'targetAttribute' => ['oktmo_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'legal_entity_foreign_state_id' => Yii::t('app', 'Legal Entity Foreign State ID'),
            'address' => Yii::t('app', 'Address'),
            'subject_rf_id' => Yii::t('app', 'Subject Rf ID'),
            'okato_id' => Yii::t('app', 'Okato ID'),
            'oktmo_id' => Yii::t('app', 'Oktmo ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalEntityForeignState()
    {
        return $this->hasOne(LegalEntityForeignState::className(), ['id' => 'legal_entity_foreign_state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkato()
    {
        return $this->hasOne(Okato::className(), ['id' => 'okato_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOktmo()
    {
        return $this->hasOne(Oktmo::className(), ['id' => 'oktmo_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\PlaceOfStayInRfQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\PlaceOfStayInRfQuery(get_called_class());
    }
}
