<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%guarantee_termination}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $reg_number
 * @property string $doc_number
 * @property string $termination_date
 * @property string $termination_reason
 *
 * @property Main $main
 */
class GuaranteeTermination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%guarantee_termination}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_id'], 'default', 'value' => null],
            [['main_id'], 'integer'],
            [
                [
                    'reg_number',
                    'doc_number',
                    'termination_date',
                    'termination_reason',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'reg_number' => Yii::t('app', 'Reg Number'),
            'doc_number' => Yii::t('app', 'Doc Number'),
            'termination_date' => Yii::t('app', 'Termination Date'),
            'termination_reason' => Yii::t('app', 'Termination Reason'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\GuaranteeTerminationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\GuaranteeTerminationQuery(get_called_class());
    }
}
