<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%okato}}".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 *
 * @property IndividualPersonRf[] $individualPersonRves
 * @property LegalEntityRf[] $legalEntityRves
 * @property PlaceOfStayInRf[] $placeOfStayInRves
 */
class Okato extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%okato}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'code',
                    'name',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndividualPersonRves()
    {
        return $this->hasMany(IndividualPersonRf::className(), ['okato_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalEntityRves()
    {
        return $this->hasMany(LegalEntityRf::className(), ['okato_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceOfStayInRves()
    {
        return $this->hasMany(PlaceOfStayInRf::className(), ['okato_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\OkatoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\OkatoQuery(get_called_class());
    }
}
