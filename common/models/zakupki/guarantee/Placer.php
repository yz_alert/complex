<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%placer}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $responsible_org_reg_num
 * @property string $responsible_org_cons_registry_num
 * @property string $responsible_org_full_name
 * @property string $responsible_role
 *
 * @property Main $main
 */
class Placer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%placer}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_id'], 'default', 'value' => null],
            [['main_id'], 'integer'],
            [
                [
                    'responsible_org_reg_num',
                    'responsible_org_cons_registry_num',
                    'responsible_org_full_name',
                    'responsible_role',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'responsible_org_reg_num' => Yii::t('app', 'Responsible Org Reg Num'),
            'responsible_org_cons_registry_num' => Yii::t('app', 'Responsible Org Cons Registry Num'),
            'responsible_org_full_name' => Yii::t('app', 'Responsible Org Full Name'),
            'responsible_role' => Yii::t('app', 'Responsible Role'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\PlacerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\PlacerQuery(get_called_class());
    }
}
