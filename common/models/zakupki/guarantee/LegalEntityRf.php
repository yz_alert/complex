<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%legal_entity_rf}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $full_name
 * @property string $short_name
 * @property string $inn
 * @property string $kpp
 * @property string $ogrn
 * @property string $registration_date
 * @property string $address
 * @property int $legal_form_id
 * @property int $subject_rf_id
 * @property int $okato_id
 * @property int $oktmo_id
 *
 * @property LegalForm $legalForm
 * @property Main $main
 * @property Okato $okato
 * @property Oktmo $oktmo
 * @property SubjectRf $subjectRf
 */
class LegalEntityRf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%legal_entity_rf}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'main_id',
                    'legal_form_id',
                    'subject_rf_id',
                    'okato_id',
                    'oktmo_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'main_id',
                    'legal_form_id',
                    'subject_rf_id',
                    'okato_id',
                    'oktmo_id',
                ],
                'integer',
            ],
            [
                [
                    'full_name',
                    'short_name',
                    'inn',
                    'kpp',
                    'ogrn',
                    'registration_date',
                    'address',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['legal_form_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LegalForm::className(),
                'targetAttribute' => ['legal_form_id' => 'id'],
            ],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
            [
                ['okato_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Okato::className(),
                'targetAttribute' => ['okato_id' => 'id'],
            ],
            [
                ['oktmo_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Oktmo::className(),
                'targetAttribute' => ['oktmo_id' => 'id'],
            ],
            [
                ['subject_rf_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SubjectRf::className(),
                'targetAttribute' => ['subject_rf_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'full_name' => Yii::t('app', 'Full Name'),
            'short_name' => Yii::t('app', 'Short Name'),
            'inn' => Yii::t('app', 'Inn'),
            'kpp' => Yii::t('app', 'Kpp'),
            'ogrn' => Yii::t('app', 'Ogrn'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'address' => Yii::t('app', 'Address'),
            'legal_form_id' => Yii::t('app', 'Legal Form ID'),
            'subject_rf_id' => Yii::t('app', 'Subject Rf ID'),
            'okato_id' => Yii::t('app', 'Okato ID'),
            'oktmo_id' => Yii::t('app', 'Oktmo ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalForm()
    {
        return $this->hasOne(LegalForm::className(), ['id' => 'legal_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkato()
    {
        return $this->hasOne(Okato::className(), ['id' => 'okato_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOktmo()
    {
        return $this->hasOne(Oktmo::className(), ['id' => 'oktmo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectRf()
    {
        return $this->hasOne(SubjectRf::className(), ['id' => 'subject_rf_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\LegalEntityRfQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\LegalEntityRfQuery(get_called_class());
    }
}
