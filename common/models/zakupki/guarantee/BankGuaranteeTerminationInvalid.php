<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%bank_guarantee_termination_invalid}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $termination_doc_number
 * @property string $reason
 *
 * @property Main $main
 */
class BankGuaranteeTerminationInvalid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bank_guarantee_termination_invalid}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_id'], 'default', 'value' => null],
            [['main_id'], 'integer'],
            [
                [
                    'termination_doc_number',
                    'reason',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'termination_doc_number' => Yii::t('app', 'Termination Doc Number'),
            'reason' => Yii::t('app', 'Reason'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\BankGuaranteeTerminationInvalidQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\BankGuaranteeTerminationInvalidQuery(get_called_class());
    }
}
