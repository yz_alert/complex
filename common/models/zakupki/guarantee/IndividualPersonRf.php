<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%individual_person_rf}}".
 *
 * @property int $id
 * @property int $main_id
 * @property int $contact_info_id
 * @property string $inn
 * @property string $ogrnip
 * @property string $registration_date
 * @property string $address
 * @property string $is_ip
 * @property int $subject_rf_id
 * @property int $okato_id
 * @property int $oktmo_id
 *
 * @property ContactInfo $contactInfo
 * @property Main $main
 * @property Okato $okato
 * @property Oktmo $oktmo
 * @property SubjectRf $subjectRf
 */
class IndividualPersonRf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%individual_person_rf}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'main_id',
                    'contact_info_id',
                    'subject_rf_id',
                    'okato_id',
                    'oktmo_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'main_id',
                    'contact_info_id',
                    'subject_rf_id',
                    'okato_id',
                    'oktmo_id',
                ],
                'integer',
            ],
            [
                [
                    'inn',
                    'ogrnip',
                    'registration_date',
                    'address',
                    'is_ip',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['contact_info_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ContactInfo::className(),
                'targetAttribute' => ['contact_info_id' => 'id'],
            ],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
            [
                ['okato_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Okato::className(),
                'targetAttribute' => ['okato_id' => 'id'],
            ],
            [
                ['oktmo_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Oktmo::className(),
                'targetAttribute' => ['oktmo_id' => 'id'],
            ],
            [
                ['subject_rf_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SubjectRf::className(),
                'targetAttribute' => ['subject_rf_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'contact_info_id' => Yii::t('app', 'Contact Info ID'),
            'inn' => Yii::t('app', 'Inn'),
            'ogrnip' => Yii::t('app', 'Ogrnip'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'address' => Yii::t('app', 'Address'),
            'is_ip' => Yii::t('app', 'Is Ip'),
            'subject_rf_id' => Yii::t('app', 'Subject Rf ID'),
            'okato_id' => Yii::t('app', 'Okato ID'),
            'oktmo_id' => Yii::t('app', 'Oktmo ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactInfo()
    {
        return $this->hasOne(ContactInfo::className(), ['id' => 'contact_info_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkato()
    {
        return $this->hasOne(Okato::className(), ['id' => 'okato_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOktmo()
    {
        return $this->hasOne(Oktmo::className(), ['id' => 'oktmo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectRf()
    {
        return $this->hasOne(SubjectRf::className(), ['id' => 'subject_rf_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\IndividualPersonRfQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\IndividualPersonRfQuery(get_called_class());
    }
}
