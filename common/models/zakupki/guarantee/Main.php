<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%main}}".
 *
 * @property int $id
 * @property string $filename
 * @property string $created_at
 * @property string $bg_id
 * @property string $external_id
 * @property string $doc_number
 * @property string $extended_doc_number
 * @property string $doc_publish_date
 * @property string $href
 *
 * @property Bank $bank
 * @property BankGuarantee $bankGuarantee
 * @property BankGuaranteeInvalid $bankGuaranteeInval
 * @property BankGuaranteeRefusal $bankGuaranteeRefusal
 * @property BankGuaranteeRefusalInvalid $bankGuaranteeRefusalInval
 * @property BankGuaranteeReturn $bankGuaranteeReturn
 * @property BankGuaranteeReturnInvalid $bankGuaranteeReturnInval
 * @property BankGuaranteeTermination $bankGuaranteeTermination
 * @property BankGuaranteeTerminationInvalid $bankGuaranteeTerminationInval
 * @property Guarantee $guarantee
 * @property GuaranteeReturn $guaranteeReturn
 * @property GuaranteeTermination $guaranteeTermination
 * @property IndividualPersonRf $individualPersonRf
 * @property LegalEntityForeignState $legalEntityForeignState
 * @property LegalEntityRf $legalEntityRf
 * @property Placer $placer
 * @property PrintForm $printForm
 * @property RefusalInfo $refusalInfo
 * @property RefusalReason $refusalReason
 * @property Supplier $supplier
 */
class Main extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%main}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'filename',
                    'bg_id',
                    'external_id',
                    'doc_number',
                    'extended_doc_number',
                    'doc_publish_date',
                    'href',
                ],
                'string',
            ],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'filename' => Yii::t('app', 'Filename'),
            'created_at' => Yii::t('app', 'Created At'),
            'bg_id' => Yii::t('app', 'Bg ID'),
            'external_id' => Yii::t('app', 'External ID'),
            'doc_number' => Yii::t('app', 'Doc Number'),
            'extended_doc_number' => Yii::t('app', 'Extended Doc Number'),
            'doc_publish_date' => Yii::t('app', 'Doc Publish Date'),
            'href' => Yii::t('app', 'Href'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(Bank::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuarantee()
    {
        return $this->hasOne(BankGuarantee::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeInval()
    {
        return $this->hasOne(BankGuaranteeInvalid::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeRefusal()
    {
        return $this->hasOne(BankGuaranteeRefusal::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeRefusalInval()
    {
        return $this->hasOne(BankGuaranteeRefusalInvalid::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeReturn()
    {
        return $this->hasOne(BankGuaranteeReturn::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeReturnInval()
    {
        return $this->hasOne(BankGuaranteeReturnInvalid::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeTermination()
    {
        return $this->hasOne(BankGuaranteeTermination::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeTerminationInval()
    {
        return $this->hasOne(BankGuaranteeTerminationInvalid::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuarantee()
    {
        return $this->hasOne(Guarantee::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuaranteeReturn()
    {
        return $this->hasOne(GuaranteeReturn::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuaranteeTermination()
    {
        return $this->hasOne(GuaranteeTermination::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndividualPersonRf()
    {
        return $this->hasOne(IndividualPersonRf::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalEntityForeignState()
    {
        return $this->hasOne(LegalEntityForeignState::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalEntityRf()
    {
        return $this->hasOne(LegalEntityRf::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacer()
    {
        return $this->hasOne(Placer::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrintForm()
    {
        return $this->hasOne(PrintForm::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefusalInfo()
    {
        return $this->hasOne(RefusalInfo::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefusalReason()
    {
        return $this->hasOne(RefusalReason::className(), ['main_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['main_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\MainQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\MainQuery(get_called_class());
    }
}
