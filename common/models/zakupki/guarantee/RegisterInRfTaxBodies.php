<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%register_in_rf_tax_bodies}}".
 *
 * @property int $id
 * @property string $inn
 * @property string $kpp
 * @property string $registration_date
 *
 * @property LegalEntityForeignState[] $legalEntityForeignStates
 */
class RegisterInRfTaxBodies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%register_in_rf_tax_bodies}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'inn',
                    'kpp',
                    'registration_date',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'inn' => Yii::t('app', 'Inn'),
            'kpp' => Yii::t('app', 'Kpp'),
            'registration_date' => Yii::t('app', 'Registration Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalEntityForeignStates()
    {
        return $this->hasMany(LegalEntityForeignState::className(), ['register_in_rf_tax_bodies_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\RegisterInRfTaxBodiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\RegisterInRfTaxBodiesQuery(get_called_class());
    }
}
