<?php

namespace common\models\zakupki\guarantee\query;

/**
 * This is the ActiveQuery class for [[\common\models\zakupki\guarantee\BankGuaranteeReturn]].
 *
 * @see \common\models\zakupki\guarantee\BankGuaranteeReturn
 */
class BankGuaranteeReturnQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\BankGuaranteeReturn[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\BankGuaranteeReturn|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
