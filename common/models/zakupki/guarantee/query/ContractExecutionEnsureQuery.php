<?php

namespace common\models\zakupki\guarantee\query;

/**
 * This is the ActiveQuery class for [[\common\models\zakupki\guarantee\ContractExecutionEnsure]].
 *
 * @see \common\models\zakupki\guarantee\ContractExecutionEnsure
 */
class ContractExecutionEnsureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\ContractExecutionEnsure[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\ContractExecutionEnsure|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
