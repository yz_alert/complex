<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%refusal_info}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $doc_date
 * @property string $doc_number
 * @property string $doc_name
 *
 * @property Main $main
 */
class RefusalInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%refusal_info}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_id'], 'default', 'value' => null],
            [['main_id'], 'integer'],
            [
                [
                    'doc_date',
                    'doc_number',
                    'doc_name',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'doc_date' => Yii::t('app', 'Doc Date'),
            'doc_number' => Yii::t('app', 'Doc Number'),
            'doc_name' => Yii::t('app', 'Doc Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\RefusalInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\RefusalInfoQuery(get_called_class());
    }
}
