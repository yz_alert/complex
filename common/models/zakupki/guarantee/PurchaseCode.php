<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%purchase_code}}".
 *
 * @property int $id
 * @property int $guarantee_id
 * @property string $purchase_code
 *
 * @property Guarantee $guarantee
 */
class PurchaseCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%purchase_code}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guarantee_id'], 'default', 'value' => null],
            [['guarantee_id'], 'integer'],
            [['purchase_code'], 'string'],
            [
                ['guarantee_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Guarantee::className(),
                'targetAttribute' => ['guarantee_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'guarantee_id' => Yii::t('app', 'Guarantee ID'),
            'purchase_code' => Yii::t('app', 'Purchase Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuarantee()
    {
        return $this->hasOne(Guarantee::className(), ['id' => 'guarantee_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\PurchaseCodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\PurchaseCodeQuery(get_called_class());
    }
}
