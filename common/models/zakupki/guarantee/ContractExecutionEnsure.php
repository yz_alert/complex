<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%contract_execution_ensure}}".
 *
 * @property int $id
 * @property int $guarantee_id
 * @property string $reg_num
 * @property string $purchase_purchase_number
 * @property string $purchase_notification_number
 * @property string $purchase_lot_number
 * @property string $m_lots
 * @property string $single_supplier
 *
 * @property Guarantee $guarantee
 */
class ContractExecutionEnsure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%contract_execution_ensure}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'reg_num',
                    'purchase_purchase_number',
                    'purchase_notification_number',
                    'purchase_lot_number',
                    'm_lots',
                    'single_supplier',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'reg_num' => Yii::t('app', 'Reg Num'),
            'purchase_purchase_number' => Yii::t('app', 'Purchase Purchase Number'),
            'purchase_notification_number' => Yii::t('app', 'Purchase Notification Number'),
            'purchase_lot_number' => Yii::t('app', 'Purchase Lot Number'),
            'm_lots' => Yii::t('app', 'M Lots'),
            'single_supplier' => Yii::t('app', 'Single Supplier'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuarantee()
    {
        return $this->hasOne(Guarantee::className(), ['id' => 'guarantee_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\ContractExecutionEnsureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\ContractExecutionEnsureQuery(get_called_class());
    }
}
