<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%purchase_request_ensure}}".
 *
 * @property int $id
 * @property int $guarantee_id
 * @property string $purchase_number
 * @property string $notification_number
 * @property string $lot_number
 * @property string $m_lots
 *
 * @property Guarantee $guarantee
 */
class PurchaseRequestEnsure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%purchase_request_ensure}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'purchase_number',
                    'notification_number',
                    'lot_number',
                    'm_lots',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'purchase_number' => Yii::t('app', 'Purchase Number'),
            'notification_number' => Yii::t('app', 'Notification Number'),
            'lot_number' => Yii::t('app', 'Lot Number'),
            'm_lots' => Yii::t('app', 'M Lots'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuarantee()
    {
        return $this->hasOne(Guarantee::className(), ['id' => 'guarantee_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\PurchaseRequestEnsureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\PurchaseRequestEnsureQuery(get_called_class());
    }
}
