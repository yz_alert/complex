<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%bank_guarantee_termination}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $reg_number
 * @property string $version_number
 * @property string $reg_num
 * @property string $reason
 *
 * @property Main $main
 */
class BankGuaranteeTermination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bank_guarantee_termination}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_id'], 'default', 'value' => null],
            [['main_id'], 'integer'],
            [
                [
                    'reg_number',
                    'version_number',
                    'reg_num',
                    'modification_info',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'reg_number' => Yii::t('app', 'Reg Number'),
            'version_number' => Yii::t('app', 'Version Number'),
            'reg_num' => Yii::t('app', 'Reg Num'),
            'modification_info' => Yii::t('app', 'Reason'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\BankGuaranteeTerminationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\BankGuaranteeTerminationQuery(get_called_class());
    }
}
