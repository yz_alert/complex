<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%placing_org}}".
 *
 * @property int $id
 * @property string $reg_num
 * @property int $cons_registry_num
 * @property string $full_name
 * @property string $short_name
 * @property string $post_address
 * @property string $fact_address
 * @property string $inn
 * @property string $kpp
 * @property string $location
 * @property string $registration_date
 * @property string $iku
 * @property int $legal_form_id
 * @property int $subject_rf_id
 * @property int $oktmo_id
 *
 * @property BankGuarantee $bankGuarantee
 * @property BankGuaranteeInvalid $bankGuaranteeInval
 * @property LegalForm $legalForm
 * @property Oktmo $oktmo
 * @property SubjectRf $subjectRf
 */
class PlacingOrg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%placing_org}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'reg_num',
                    'full_name',
                    'short_name',
                    'post_address',
                    'fact_address',
                    'inn',
                    'kpp',
                    'location',
                    'registration_date',
                    'iku',
                ],
                'string',
            ],
            [
                [
                    'cons_registry_num',
                    'legal_form_id',
                    'subject_rf_id',
                    'oktmo_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'cons_registry_num',
                    'legal_form_id',
                    'subject_rf_id',
                    'oktmo_id',
                ],
                'integer',
            ],
            [
                ['legal_form_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => LegalForm::className(),
                'targetAttribute' => ['legal_form_id' => 'id'],
            ],
            [
                ['oktmo_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Oktmo::className(),
                'targetAttribute' => ['oktmo_id' => 'id'],
            ],
            [
                ['subject_rf_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SubjectRf::className(),
                'targetAttribute' => ['subject_rf_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'reg_num' => Yii::t('app', 'Reg Num'),
            'cons_registry_num' => Yii::t('app', 'Cons Registry Num'),
            'full_name' => Yii::t('app', 'Full Name'),
            'short_name' => Yii::t('app', 'Short Name'),
            'post_address' => Yii::t('app', 'Post Address'),
            'fact_address' => Yii::t('app', 'Fact Address'),
            'inn' => Yii::t('app', 'Inn'),
            'kpp' => Yii::t('app', 'Kpp'),
            'location' => Yii::t('app', 'Location'),
            'registration_date' => Yii::t('app', 'Registration Date'),
            'iku' => Yii::t('app', 'Iku'),
            'legal_form_id' => Yii::t('app', 'Legal Form ID'),
            'subject_rf_id' => Yii::t('app', 'Subject Rf ID'),
            'oktmo_id' => Yii::t('app', 'Oktmo ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuarantee()
    {
        return $this->hasOne(BankGuarantee::className(), ['placing_org_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankGuaranteeInval()
    {
        return $this->hasOne(BankGuaranteeInvalid::className(), ['placing_org_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalForm()
    {
        return $this->hasOne(LegalForm::className(), ['id' => 'legal_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOktmo()
    {
        return $this->hasOne(Oktmo::className(), ['id' => 'oktmo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectRf()
    {
        return $this->hasOne(SubjectRf::className(), ['id' => 'subject_rf_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\PlacingOrgQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\PlacingOrgQuery(get_called_class());
    }
}
