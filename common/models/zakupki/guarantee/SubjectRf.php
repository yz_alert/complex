<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%subject_rf}}".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 *
 * @property Bank[] $banks
 * @property Customer[] $customers
 * @property IndividualPersonRf[] $individualPersonRves
 * @property LegalEntityRf[] $legalEntityRves
 * @property PlacingOrg[] $placingOrgs
 */
class SubjectRf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subject_rf}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'code',
                    'name',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanks()
    {
        return $this->hasMany(Bank::className(), ['subject_rf_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customer::className(), ['subject_rf_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndividualPersonRves()
    {
        return $this->hasMany(IndividualPersonRf::className(), ['subject_rf_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLegalEntityRves()
    {
        return $this->hasMany(LegalEntityRf::className(), ['subject_rf_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacingOrgs()
    {
        return $this->hasMany(PlacingOrg::className(), ['subject_rf_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\SubjectRfQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\SubjectRfQuery(get_called_class());
    }
}
