<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property int $id
 * @property string $code
 * @property string $digital_code
 * @property string $name
 *
 * @property Guarantee[] $guarantees
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'code',
                    'digital_code',
                    'name',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'digital_code' => Yii::t('app', 'Digital Code'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuarantees()
    {
        return $this->hasMany(Guarantee::className(), ['currency_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\CurrencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\CurrencyQuery(get_called_class());
    }
}
