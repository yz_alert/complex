<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%bank_guarantee}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $reg_number
 * @property string $credit_org_number
 * @property string $extended_doc_number
 * @property string $version_number
 * @property string $guarantee_purchase_code
 * @property int $placing_org_id
 *
 * @property Main $main
 * @property PlacingOrg $placingOrg
 */
class BankGuarantee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bank_guarantee}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'main_id',
                    'placing_org_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'main_id',
                    'placing_org_id',
                ],
                'integer',
            ],
            [
                [
                    'reg_number',
                    'credit_org_number',
                    'extended_doc_number',
                    'version_number',
                    'guarantee_purchase_code',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [['placing_org_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
            [
                ['placing_org_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PlacingOrg::className(),
                'targetAttribute' => ['placing_org_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'reg_number' => Yii::t('app', 'Reg Number'),
            'credit_org_number' => Yii::t('app', 'Credit Org Number'),
            'extended_doc_number' => Yii::t('app', 'Extended Doc Number'),
            'version_number' => Yii::t('app', 'Version Number'),
            'guarantee_purchase_code' => Yii::t('app', 'Guarantee Purchase Code'),
            'placing_org_id' => Yii::t('app', 'Placing Org ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacingOrg()
    {
        return $this->hasOne(PlacingOrg::className(), ['id' => 'placing_org_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\BankGuaranteeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\BankGuaranteeQuery(get_called_class());
    }
}
