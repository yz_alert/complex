<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%individual_person_foreign_state}}".
 *
 * @property int $id
 * @property int $contact_info_id
 * @property string $last_name_lat
 * @property string $first_name_lat
 * @property string $middle_name_lat
 * @property string $inn
 * @property string $tax_payer_code
 * @property string $address
 * @property int $country_id
 *
 * @property ContactInfo $contactInfo
 * @property Country $country
 */
class IndividualPersonForeignState extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%individual_person_foreign_state}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'contact_info_id',
                    'country_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'contact_info_id',
                    'country_id',
                ],
                'integer',
            ],
            [
                [
                    'last_name_lat',
                    'first_name_lat',
                    'middle_name_lat',
                    'inn',
                    'tax_payer_code',
                    'address',
                ],
                'string',
            ],
            [
                ['contact_info_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ContactInfo::className(),
                'targetAttribute' => ['contact_info_id' => 'id'],
            ],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contact_info_id' => Yii::t('app', 'Contact Info ID'),
            'last_name_lat' => Yii::t('app', 'Last Name Lat'),
            'first_name_lat' => Yii::t('app', 'First Name Lat'),
            'middle_name_lat' => Yii::t('app', 'Middle Name Lat'),
            'inn' => Yii::t('app', 'Inn'),
            'tax_payer_code' => Yii::t('app', 'Tax Payer Code'),
            'address' => Yii::t('app', 'Address'),
            'country_id' => Yii::t('app', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactInfo()
    {
        return $this->hasOne(ContactInfo::className(), ['id' => 'contact_info_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\IndividualPersonForeignStateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\IndividualPersonForeignStateQuery(get_called_class());
    }
}
