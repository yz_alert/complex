<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%guarantee_additional_info}}".
 *
 * @property int $id
 * @property int $guarantee_id
 * @property string $guarantee_grant_date
 * @property string $guarantee_publish_date
 * @property string $guarantee_credit_org_number
 *
 * @property Guarantee $guarantee
 */
class GuaranteeAdditionalInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%guarantee_additional_info}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guarantee_id'], 'default', 'value' => null],
            [['guarantee_id'], 'integer'],
            [
                [
                    'guarantee_grant_date',
                    'guarantee_publish_date',
                    'guarantee_credit_org_number',
                ],
                'string',
            ],
            [['guarantee_id'], 'unique'],
            [
                ['guarantee_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Guarantee::className(),
                'targetAttribute' => ['guarantee_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'guarantee_id' => Yii::t('app', 'Guarantee ID'),
            'guarantee_grant_date' => Yii::t('app', 'Guarantee Grant Date'),
            'guarantee_publish_date' => Yii::t('app', 'Guarantee Publish Date'),
            'guarantee_credit_org_number' => Yii::t('app', 'Guarantee Credit Org Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuarantee()
    {
        return $this->hasOne(Guarantee::className(), ['id' => 'guarantee_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\GuaranteeAdditionalInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\GuaranteeAdditionalInfoQuery(get_called_class());
    }
}
