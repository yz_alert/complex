<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%legal_entity_foreign_state}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $full_name
 * @property string $full_name_lat
 * @property string $tax_payer_code
 * @property string $address
 * @property int $register_in_rf_tax_bodies_id
 * @property int $country_id
 *
 * @property Country $country
 * @property Main $main
 * @property RegisterInRfTaxBodies $registerInRfTaxBodies
 * @property PlaceOfStayInRf $placeOfStayInRf
 */
class LegalEntityForeignState extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%legal_entity_foreign_state}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'main_id',
                    'register_in_rf_tax_bodies_id',
                    'country_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'main_id',
                    'register_in_rf_tax_bodies_id',
                    'country_id',
                ],
                'integer',
            ],
            [
                [
                    'full_name',
                    'full_name_lat',
                    'tax_payer_code',
                    'address',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id'],
            ],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
            [
                ['register_in_rf_tax_bodies_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => RegisterInRfTaxBodies::className(),
                'targetAttribute' => ['register_in_rf_tax_bodies_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'full_name' => Yii::t('app', 'Full Name'),
            'full_name_lat' => Yii::t('app', 'Full Name Lat'),
            'tax_payer_code' => Yii::t('app', 'Tax Payer Code'),
            'address' => Yii::t('app', 'Address'),
            'register_in_rf_tax_bodies_id' => Yii::t('app', 'Register In Rf Tax Bodies ID'),
            'country_id' => Yii::t('app', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegisterInRfTaxBodies()
    {
        return $this->hasOne(RegisterInRfTaxBodies::className(), ['id' => 'register_in_rf_tax_bodies_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceOfStayInRf()
    {
        return $this->hasOne(PlaceOfStayInRf::className(), ['legal_entity_foreign_state_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\LegalEntityForeignStateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\LegalEntityForeignStateQuery(get_called_class());
    }
}
