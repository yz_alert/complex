<?php

namespace common\models\zakupki\guarantee;

use Yii;

/**
 * This is the model class for table "{{%bank_guarantee_invalid}}".
 *
 * @property int $id
 * @property int $main_id
 * @property string $reg_number
 * @property string $reason
 * @property int $placing_org_id
 *
 * @property Main $main
 * @property PlacingOrg $placingOrg
 */
class BankGuaranteeInvalid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bank_guarantee_invalid}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_bank_guarantee');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'main_id',
                    'placing_org_id',
                ],
                'default',
                'value' => null,
            ],
            [
                [
                    'main_id',
                    'placing_org_id',
                ],
                'integer',
            ],
            [
                [
                    'reg_number',
                    'reason',
                ],
                'string',
            ],
            [['main_id'], 'unique'],
            [['placing_org_id'], 'unique'],
            [
                ['main_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Main::className(),
                'targetAttribute' => ['main_id' => 'id'],
            ],
            [
                ['placing_org_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PlacingOrg::className(),
                'targetAttribute' => ['placing_org_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'main_id' => Yii::t('app', 'Main ID'),
            'reg_number' => Yii::t('app', 'Reg Number'),
            'reason' => Yii::t('app', 'Reason'),
            'placing_org_id' => Yii::t('app', 'Placing Org ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMain()
    {
        return $this->hasOne(Main::className(), ['id' => 'main_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacingOrg()
    {
        return $this->hasOne(PlacingOrg::className(), ['id' => 'placing_org_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\zakupki\guarantee\query\BankGuaranteeInvalidQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\zakupki\guarantee\query\BankGuaranteeInvalidQuery(get_called_class());
    }
}
