<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unfair_supplier_223_contract_item".
 *
 * @property integer $id
 * @property integer $unfair_supplier_223_id
 * @property string $okpd2_code
 * @property string $okpd2_name
 * @property string $okved2_code
 * @property string $okved2_name
 * @property string $name
 *
 * @property UnfairSupplier223 $unfairSupplier223
 */
class UnfairSupplier223ContractItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unfair_supplier_223_contract_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unfair_supplier_223_id'], 'required'],
            [['unfair_supplier_223_id'], 'integer'],
            [['okpd2_code', 'okpd2_name', 'okved2_code', 'okved2_name', 'name'], 'string'],
            [
                ['unfair_supplier_223_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UnfairSupplier223::className(),
                'targetAttribute' => ['unfair_supplier_223_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unfair_supplier_223_id' => Yii::t('app', 'Unfair Supplier 223 ID'),
            'okpd2_code' => Yii::t('app', 'Okpd2 Code'),
            'okpd2_name' => Yii::t('app', 'Okpd2 Name'),
            'okved2_code' => Yii::t('app', 'Okved2 Code'),
            'okved2_name' => Yii::t('app', 'Okved2 Name'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnfairSupplier223()
    {
        return $this->hasOne(UnfairSupplier223::className(), ['id' => 'unfair_supplier_223_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UnfairSupplier223ContractItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UnfairSupplier223ContractItemQuery(get_called_class());
    }
}
