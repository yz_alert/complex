<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_dokument".
 *
 * @property integer $id
 * @property string $id_dok
 * @property string $data_sost
 * @property string $data_vkl_msp
 * @property string $vid_sub_msp
 * @property string $kat_sub_msp
 * @property string $priz_nov_msp
 * @property integer $msp_fayl_id
 *
 * @property MspFayl $mspFayl
 * @property MspIpVklMsp $mspIpVklMsp
 * @property MspOrgVklMsp $mspOrgVklMsp
 * @property MspSvDog $mspSvDog
 * @property MspSvKontr $mspSvKontr
 * @property MspSvLitsenz[] $mspSvLitsenzs
 * @property MspSvOkved $mspSvOkved
 * @property MspSvProd $mspSvProd
 * @property MspSvProgPart $mspSvProgPart
 * @property MspSvedMn $mspSvedMn
 */
class MspDokument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_dokument';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dok', 'vid_sub_msp', 'kat_sub_msp', 'priz_nov_msp'], 'string'],
            [['data_sost', 'data_vkl_msp'], 'safe'],
            [['msp_fayl_id'], 'integer'],
            [
                ['msp_fayl_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspFayl::className(),
                'targetAttribute' => ['msp_fayl_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_dok' => Yii::t('app', 'Id Dok'),
            'data_sost' => Yii::t('app', 'Data Sost'),
            'data_vkl_msp' => Yii::t('app', 'Data Vkl Msp'),
            'vid_sub_msp' => Yii::t('app', 'Vid Sub Msp'),
            'kat_sub_msp' => Yii::t('app', 'Kat Sub Msp'),
            'priz_nov_msp' => Yii::t('app', 'Priz Nov Msp'),
            'msp_fayl_id' => Yii::t('app', 'Msp Fayl ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspFayl()
    {
        return $this->hasOne(MspFayl::className(), ['id' => 'msp_fayl_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspIpVklMsp()
    {
        return $this->hasOne(MspIpVklMsp::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspOrgVklMsp()
    {
        return $this->hasOne(MspOrgVklMsp::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvDog()
    {
        return $this->hasOne(MspSvDog::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvKontr()
    {
        return $this->hasOne(MspSvKontr::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvLitsenzs()
    {
        return $this->hasMany(MspSvLitsenz::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvOkved()
    {
        return $this->hasOne(MspSvOkved::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvProd()
    {
        return $this->hasOne(MspSvProd::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvProgPart()
    {
        return $this->hasOne(MspSvProgPart::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvedMn()
    {
        return $this->hasOne(MspSvedMn::className(), ['msp_dokument_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspDokumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspDokumentQuery(get_called_class());
    }
}
