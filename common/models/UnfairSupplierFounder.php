<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unfair_supplier_founder".
 *
 * @property integer $id
 * @property integer $unfair_supplier_id
 * @property string $name
 * @property string $inn
 *
 * @property UnfairSupplier $unfairSupplier
 */
class UnfairSupplierFounder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unfair_supplier_founder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unfair_supplier_id'], 'required'],
            [['unfair_supplier_id'], 'integer'],
            [['names', 'inn'], 'string'],
            [
                ['unfair_supplier_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UnfairSupplier::className(),
                'targetAttribute' => ['unfair_supplier_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unfair_supplier_id' => Yii::t('app', 'Unfair Supplier ID'),
            'names' => Yii::t('app', 'Names'),
            'inn' => Yii::t('app', 'Inn'),
        ];
    }

    public function fields()
    {
        return [
            'names',
            'inn',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnfairSupplier()
    {
        return $this->hasOne(UnfairSupplier::className(), ['id' => 'unfair_supplier_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UnfairSupplierFounderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UnfairSupplierFounderQuery(get_called_class());
    }
}
