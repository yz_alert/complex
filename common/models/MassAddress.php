<?php

namespace common\models;

use Yii;
use common\models\helpers\MyHelper;

/**
 * This is the model class for table "mass_address".
 *
 * @property integer $id
 * @property string $region
 * @property string $location
 * @property string $city
 * @property string $settlement
 * @property string $street
 * @property string $house_number
 * @property string $corpus_number
 * @property string $office_number
 * @property string $quantity
 */
class MassAddress extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->get('db_kaleka');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mass_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'region',
                    'location',
                    'city',
                    'settlement',
                    'street',
                    'house_number',
                    'corpus_number',
                    'office_number',
                    'quantity',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region' => Yii::t('app', 'The name of the region'),
            'location' => Yii::t('app', 'The name of the  location'),
            'city' => Yii::t('app', 'The name of the city'),
            'settlement' => Yii::t('app', 'The name of the settlement'),
            'street' => Yii::t('app', 'The street name'),
            'house_number' => Yii::t('app', 'The number of house'),
            'corpus_number' => Yii::t('app', 'The number of corpus'),
            'office_number' => Yii::t('app', 'The number of the apartment (office)'),
            'quantity' => Yii::t('app', 'The number of juridical entity'),
        ];
    }

    public function fields()
    {
        return [
            //'id',
            'region',
            'location',
            'city',
            'settlement',
            'street',
            'house_number',
            'corpus_number',
            'office_number',
            'quantity',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MassAddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MassAddressQuery(get_called_class());
    }

    public static function curlGetNewProxy($url)
    {
        $proxy = Proxy::getRandomProxy();

        $proxy = explode(":", $proxy);
        $proxyLoginPass = $proxy[2] . ":" . $proxy[3];
        $proxyIpPort = $proxy[0] . ":" . $proxy[1];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $proxyIpPort);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');

        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyLoginPass);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);

        $data = curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
            MyHelper::echobr2('curlGet error', curl_getinfo($ch, CURLINFO_HTTP_CODE));

            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) === 505) {
                if ($data == false) {
                    $err = curl_error($ch);
                    MyHelper::echobr($err);
                }
                curl_close($ch);
                return $data;
            }
            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) === 403) {
                MyHelper::echobr("Error 403");
                //$proxy = Proxy::getRandomProxy();
                MyHelper::echobr2("Новый прокси", $proxy);
                self::curlGetNewProxy($url);
            }
            self::curlGetNewProxy($url);
        }

        if ($data == false) {
            $err = curl_error($ch);
            MyHelper::echobr2("Data curl == false", $err);
        }
        curl_close($ch);
        return $data;
    }
}
