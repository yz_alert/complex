<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unfair_supplier_223".
 *
 * @property integer $id
 * @property string $guid
 * @property string $placer_main_info_full_name
 * @property string $placer_main_info_short_name
 * @property string $placer_main_info_iko
 * @property string $placer_main_info_inn
 * @property string $placer_main_info_kpp
 * @property string $placer_main_info_ogrn
 * @property string $placer_main_info_legal_address
 * @property string $placer_main_info_postal_address
 * @property string $placer_main_info_phone
 * @property string $placer_main_info_fax
 * @property string $placer_main_info_email
 * @property string $placer_main_info_okato
 * @property string $placer_main_info_okopf
 * @property string $placer_main_info_okopf_name
 * @property string $placer_main_info_okpo
 * @property string $placer_main_info_customer_registration_date
 * @property string $customer_full_name
 * @property string $customer_short_name
 * @property string $customer_iko
 * @property string $customer_inn
 * @property string $customer_kpp
 * @property string $customer_ogrn
 * @property string $customer_legal_address
 * @property string $customer_postal_address
 * @property string $customer_phone
 * @property string $customer_fax
 * @property string $customer_email
 * @property string $customer_okato
 * @property string $customer_okopf
 * @property string $customer_okopf_name
 * @property string $customer_okpo
 * @property string $customer_customer_registration_date
 * @property string $customer_address_country_name
 * @property string $customer_address_country_digital_code
 * @property string $customer_address_area
 * @property string $customer_address_corpus
 * @property string $customer_address_house
 * @property string $customer_address_zip
 * @property string $customer_address_region
 * @property string $customer_address_structure
 * @property string $customer_address_city
 * @property string $customer_address_settlement
 * @property string $customer_address_street
 * @property string $customer_short_address
 * @property string $publication_date_time
 * @property string $registration_number
 * @property string $version
 * @property string $modification_description
 * @property string $url_oos
 * @property string $url_vsrz
 * @property string $status
 * @property string $create_date_time
 * @property string $cai_include_reason
 * @property string $cai_include_reason_text
 * @property string $cai_control_agency_full_name
 * @property string $cai_control_agency_short_name
 * @property string $cai_control_agency_iko
 * @property string $cai_control_agency_inn
 * @property string $cai_control_agency_kpp
 * @property string $cai_control_agency_ogrn
 * @property string $cai_control_agency_legal_address
 * @property string $cai_control_agency_postal_address
 * @property string $cai_control_agency_phone
 * @property string $cai_control_agency_fax
 * @property string $cai_control_agency_email
 * @property string $cai_control_agency_okato
 * @property string $cai_control_agency_okopf
 * @property string $cai_control_agency_okopf_name
 * @property string $cai_control_agency_okpo
 * @property string $cai_control_agency_customer_registration_date
 * @property string $cai_top_secret
 * @property string $cai_supplier_name
 * @property string $cai_supplier_inn
 * @property string $cai_supplier_kpp
 * @property string $cai_supplier_address
 * @property string $cai_supplier_email
 * @property string $cai_purchase_info_guid
 * @property string $cai_purchase_info_purchase_notice_number
 * @property string $cai_purchase_info_publication_date_time
 * @property string $cai_purchase_info_name
 * @property string $cai_purchase_info_purchase_method
 * @property string $cai_purchase_info_purchase_method_code
 * @property string $cai_purchase_info_purchase_method_code_name
 * @property string $cai_purchase_info_emergency
 * @property string $cai_purchase_info_lot_info
 * @property string $cai_purchase_info_purchase_date
 * @property string $cai_purchase_info_summingup_date
 * @property string $cai_purchase_info_cancellation_date
 * @property string $cai_purchase_info_document_requisites_document_number
 * @property string $cai_purchase_info_document_requisites_document_name
 * @property string $cai_purchase_info_document_requisites_document_date
 * @property string $cai_contract_info_contract_number
 * @property string $cai_contract_info_contract_date
 * @property string $cai_contract_info_name
 * @property string $cai_contract_info_currency_code
 * @property string $cai_contract_info_currency_digital_code
 * @property string $cai_contract_info_currency_name
 * @property string $cai_contract_info_sum
 * @property string $cai_contract_info_sum_info
 * @property string $cai_contract_info_fulfillment_date
 * @property string $cai_contract_info_cancellation_reason
 * @property string $cai_contract_info_cancellation_judgment_date
 * @property string $cai_contract_info_cancellation_date
 * @property string $approve_status
 * @property string $review_date
 * @property string $approve_date
 * @property string $rejection_date
 * @property string $rejection_reason
 * @property string $attachments_total_documents_count
 * @property string $attachments_additional_documents_count
 */
class UnfairSupplier223 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unfair_supplier_223';
    }

    static function getDb()
    {
        return Yii::$app->db_kaleka;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'guid',
                    'placer_main_info_full_name',
                    'placer_main_info_short_name',
                    'placer_main_info_iko',
                    'placer_main_info_inn',
                    'placer_main_info_kpp',
                    'placer_main_info_ogrn',
                    'placer_main_info_legal_address',
                    'placer_main_info_postal_address',
                    'placer_main_info_phone',
                    'placer_main_info_fax',
                    'placer_main_info_email',
                    'placer_main_info_okato',
                    'placer_main_info_okopf',
                    'placer_main_info_okopf_name',
                    'placer_main_info_okpo',
                    'placer_main_info_customer_registration_date',
                    'customer_full_name',
                    'customer_short_name',
                    'customer_iko',
                    'customer_inn',
                    'customer_kpp',
                    'customer_ogrn',
                    'customer_legal_address',
                    'customer_postal_address',
                    'customer_phone',
                    'customer_fax',
                    'customer_email',
                    'customer_okato',
                    'customer_okopf',
                    'customer_okopf_name',
                    'customer_okpo',
                    'customer_customer_registration_date',
                    'customer_address_country_name',
                    'customer_address_country_digital_code',
                    'customer_address_area',
                    'customer_address_corpus',
                    'customer_address_house',
                    'customer_address_zip',
                    'customer_address_region',
                    'customer_address_structure',
                    'customer_address_city',
                    'customer_address_settlement',
                    'customer_address_street',
                    'customer_short_address',
                    'publication_date_time',
                    'registration_number',
                    'version',
                    'modification_description',
                    'url_oos',
                    'url_vsrz',
                    'status',
                    'create_date_time',
                    'cai_include_reason',
                    'cai_include_reason_text',
                    'cai_control_agency_full_name',
                    'cai_control_agency_short_name',
                    'cai_control_agency_iko',
                    'cai_control_agency_inn',
                    'cai_control_agency_kpp',
                    'cai_control_agency_ogrn',
                    'cai_control_agency_legal_address',
                    'cai_control_agency_postal_address',
                    'cai_control_agency_phone',
                    'cai_control_agency_fax',
                    'cai_control_agency_email',
                    'cai_control_agency_okato',
                    'cai_control_agency_okopf',
                    'cai_control_agency_okopf_name',
                    'cai_control_agency_okpo',
                    'cai_control_agency_customer_registration_date',
                    'cai_top_secret',
                    'cai_supplier_name',
                    'cai_supplier_inn',
                    'cai_supplier_kpp',
                    'cai_supplier_address',
                    'cai_supplier_email',
                    'cai_purchase_info_guid',
                    'cai_purchase_info_purchase_notice_number',
                    'cai_purchase_info_publication_date_time',
                    'cai_purchase_info_name',
                    'cai_purchase_info_purchase_method',
                    'cai_purchase_info_purchase_method_code',
                    'cai_purchase_info_purchase_method_code_name',
                    'cai_purchase_info_emergency',
                    'cai_purchase_info_lot_info',
                    'cai_purchase_info_purchase_date',
                    'cai_purchase_info_summingup_date',
                    'cai_purchase_info_cancellation_date',
                    'cai_purchase_info_document_requisites_document_number',
                    'cai_purchase_info_document_requisites_document_name',
                    'cai_purchase_info_document_requisites_document_date',
                    'cai_contract_info_contract_number',
                    'cai_contract_info_contract_date',
                    'cai_contract_info_name',
                    'cai_contract_info_currency_code',
                    'cai_contract_info_currency_digital_code',
                    'cai_contract_info_currency_name',
                    'cai_contract_info_sum',
                    'cai_contract_info_sum_info',
                    'cai_contract_info_fulfillment_date',
                    'cai_contract_info_cancellation_reason',
                    'cai_contract_info_cancellation_judgment_date',
                    'cai_contract_info_cancellation_date',
                    'approve_status',
                    'review_date',
                    'approve_date',
                    'rejection_date',
                    'rejection_reason',
                    'attachments_total_documents_count',
                    'attachments_additional_documents_count',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'guid' => Yii::t('app', 'Guid'),
            'placer_main_info_full_name' => Yii::t('app', 'Placer Main Info Full Name'),
            'placer_main_info_short_name' => Yii::t('app', 'Placer Main Info Short Name'),
            'placer_main_info_iko' => Yii::t('app', 'Placer Main Info Iko'),
            'placer_main_info_inn' => Yii::t('app', 'Placer Main Info Inn'),
            'placer_main_info_kpp' => Yii::t('app', 'Placer Main Info Kpp'),
            'placer_main_info_ogrn' => Yii::t('app', 'Placer Main Info Ogrn'),
            'placer_main_info_legal_address' => Yii::t('app', 'Placer Main Info Legal Address'),
            'placer_main_info_postal_address' => Yii::t('app', 'Placer Main Info Postal Address'),
            'placer_main_info_phone' => Yii::t('app', 'Placer Main Info Phone'),
            'placer_main_info_fax' => Yii::t('app', 'Placer Main Info Fax'),
            'placer_main_info_email' => Yii::t('app', 'Placer Main Info Email'),
            'placer_main_info_okato' => Yii::t('app', 'Placer Main Info Okato'),
            'placer_main_info_okopf' => Yii::t('app', 'Placer Main Info Okopf'),
            'placer_main_info_okopf_name' => Yii::t('app', 'Placer Main Info Okopf Name'),
            'placer_main_info_okpo' => Yii::t('app', 'Placer Main Info okpo'),
            'placer_main_info_customer_registration_date' => Yii::t('app',
                'Placer Main Info Customer Registration Date'),
            'customer_full_name' => Yii::t('app', 'Customer Full Name'),
            'customer_short_name' => Yii::t('app', 'Customer Short Name'),
            'customer_iko' => Yii::t('app', 'Customer Iko'),
            'customer_inn' => Yii::t('app', 'Customer Inn'),
            'customer_kpp' => Yii::t('app', 'Customer Kpp'),
            'customer_ogrn' => Yii::t('app', 'Customer Ogrn'),
            'customer_legal_address' => Yii::t('app', 'Customer Legal Address'),
            'customer_postal_address' => Yii::t('app', 'Customer Postal Address'),
            'customer_phone' => Yii::t('app', 'Customer Phone'),
            'customer_fax' => Yii::t('app', 'Customer Fax'),
            'customer_email' => Yii::t('app', 'Customer Email'),
            'customer_okato' => Yii::t('app', 'Customer Okato'),
            'customer_okopf' => Yii::t('app', 'Customer Okopf'),
            'customer_okopf_name' => Yii::t('app', 'Customer Okopf Name'),
            'customer_okpo' => Yii::t('app', 'Customer okpo'),
            'customer_customer_registration_date' => Yii::t('app', 'Customer Customer Registration Date'),
            'customer_address_country_name' => Yii::t('app', 'Customer Address Country Name'),
            'customer_address_country_digital_code' => Yii::t('app', 'Customer Address Country Digital Code'),
            'customer_address_area' => Yii::t('app', 'Customer Address Area'),
            'customer_address_corpus' => Yii::t('app', 'Customer Address Corpus'),
            'customer_address_house' => Yii::t('app', 'Customer Address House'),
            'customer_address_zip' => Yii::t('app', 'Customer Address Zip'),
            'customer_address_region' => Yii::t('app', 'Customer Address Region'),
            'customer_address_structure' => Yii::t('app', 'Customer Address Structure'),
            'customer_address_city' => Yii::t('app', 'Customer Address City'),
            'customer_address_settlement' => Yii::t('app', 'Customer Address Settlement'),
            'customer_address_street' => Yii::t('app', 'Customer Address Street'),
            'customer_short_address' => Yii::t('app', 'Customer Short Address'),
            'publication_date_time' => Yii::t('app', 'Publication Date Time'),
            'registration_number' => Yii::t('app', 'Registration Number'),
            'version' => Yii::t('app', 'Version'),
            'modification_description' => Yii::t('app', 'Modification Description'),
            'url_oos' => Yii::t('app', 'Url Oos'),
            'url_vsrz' => Yii::t('app', 'Url Vsrz'),
            'status' => Yii::t('app', 'Status'),
            'create_date_time' => Yii::t('app', 'Create Date Time'),
            'cai_include_reason' => Yii::t('app', 'Cai Include Reason'),
            'cai_include_reason_text' => Yii::t('app', 'Cai Include Reason Text'),
            'cai_control_agency_full_name' => Yii::t('app', 'Cai Control Agency Full Name'),
            'cai_control_agency_short_name' => Yii::t('app', 'Cai Control Agency Short Name'),
            'cai_control_agency_iko' => Yii::t('app', 'Cai Control Agency Iko'),
            'cai_control_agency_inn' => Yii::t('app', 'Cai Control Agency Inn'),
            'cai_control_agency_kpp' => Yii::t('app', 'Ccai Control Agency Kpp'),
            'cai_control_agency_ogrn' => Yii::t('app', 'Cai Control Agency Ogrn'),
            'cai_control_agency_legal_address' => Yii::t('app', 'Cai Control Agency Legal Address'),
            'cai_control_agency_postal_address' => Yii::t('app', 'Cai Control Agency Postal Address'),
            'cai_control_agency_phone' => Yii::t('app', 'Cai Control Agency Phone'),
            'cai_control_agency_fax' => Yii::t('app', 'Cai Control Agency Fax'),
            'cai_control_agency_email' => Yii::t('app', 'Cai Control Agency Email'),
            'cai_control_agency_okato' => Yii::t('app', 'Cai Control Agency Okato'),
            'cai_control_agency_okopf' => Yii::t('app', 'Cai Control Agency Okopf'),
            'cai_control_agency_okopf_name' => Yii::t('app', 'Cai Control Agency Okopf Name'),
            'cai_control_agency_okpo' => Yii::t('app', 'Cai Control Agency Okpo'),
            'cai_control_agency_customer_registration_date' => Yii::t('app',
                'Cai Control Agency Customer Registration Date'),
            'cai_top_secret' => Yii::t('app', 'Cai Top Secret'),
            'cai_supplier_name' => Yii::t('app', 'Cai Supplier Name'),
            'cai_supplier_inn' => Yii::t('app', 'Cai Supplier Inn'),
            'cai_supplier_kpp' => Yii::t('app', 'Cai Supplier Kpp'),
            'cai_supplier_address' => Yii::t('app', 'Cai Supplier Address'),
            'cai_supplier_email' => Yii::t('app', 'Cai Supplier Email'),
            'cai_purchase_info_guid' => Yii::t('app', 'Cai Purchase Info Guid'),
            'cai_purchase_info_purchase_notice_number' => Yii::t('app', 'Cai Purchase Info Purchase Notice Number'),
            'cai_purchase_info_publication_date_time' => Yii::t('app', 'Cai Purchase Info Publication Date Time'),
            'cai_purchase_info_name' => Yii::t('app', 'Cai Purchase Info Name'),
            'cai_purchase_info_purchase_method' => Yii::t('app', 'Cai Purchase Info Purchase Method'),
            'cai_purchase_info_purchase_method_code' => Yii::t('app', 'Cai Purchase Info Purchase Method Code'),
            'cai_purchase_info_purchase_method_code_name' => Yii::t('app',
                'Cai Purchase Info Purchase Method Code Name'),
            'cai_purchase_info_emergency' => Yii::t('app', 'Cai Purchase Info Emergency'),
            'cai_purchase_info_lot_info' => Yii::t('app', 'Cai Purchase Info Lot Info'),
            'cai_purchase_info_purchase_date' => Yii::t('app', 'Cai Purchase Info Purchase Date'),
            'cai_purchase_info_summingup_date' => Yii::t('app', 'Cai Purchase Info Summingup Date'),
            'cai_purchase_info_cancellation_date' => Yii::t('app', 'Cai Purchase Info Cancellation Date'),
            'cai_purchase_info_document_requisites_document_number' => Yii::t('app',
                'Cai Purchase Info Document Requisites Document Number'),
            'cai_purchase_info_document_requisites_document_name' => Yii::t('app',
                'Cai Purchase Info Document Requisites Document Name'),
            'cai_purchase_info_document_requisites_document_date' => Yii::t('app',
                'Cai Purchase Info Document Requisites Document Date'),
            'cai_contract_info_contract_number' => Yii::t('app', 'Cai Contract Info Contract Number'),
            'cai_contract_info_contract_date' => Yii::t('app', 'Cai Contract Info Contract Date'),
            'cai_contract_info_name' => Yii::t('app', 'Cai Contract Info Name'),
            'cai_contract_info_currency_code' => Yii::t('app', 'Cai Contract Info Currency Code'),
            'cai_contract_info_currency_digital_code' => Yii::t('app', 'Cai Contract Info Currency Digital Code'),
            'cai_contract_info_currency_name' => Yii::t('app', 'Cai Contract Info Currency Name'),
            'cai_contract_info_sum' => Yii::t('app', 'Cai Contract Info Sum'),
            'cai_contract_info_sum_info' => Yii::t('app', 'Cai Contract Info Sum Info'),
            'cai_contract_info_fulfillment_date' => Yii::t('app', 'Cai Contract Info Fulfillment Date'),
            'cai_contract_info_cancellation_reason' => Yii::t('app', 'Cai Contract Info Cancellation Reason'),
            'cai_contract_info_cancellation_judgment_date' => Yii::t('app',
                'Cai Contract Info Cancellation Judgment Date'),
            'cai_contract_info_cancellation_date' => Yii::t('app', 'Cai Contract Info Cancellation Date'),
            'approve_status' => Yii::t('app', 'Approve Status'),
            'review_date' => Yii::t('app', 'Review Date'),
            'approve_date' => Yii::t('app', 'Approve Date'),
            'rejection_date' => Yii::t('app', 'Rejection Date'),
            'rejection_reason' => Yii::t('app', 'Rejection Reason'),
            'attachments_total_documents_count' => Yii::t('app', 'Attachments Total Documents Count'),
            'attachments_additional_documents_count' => Yii::t('app', 'Attachments Additional Documents Count'),
        ];
    }

    public static function newRecord($xml)
    {
        $dBRecord = self::find();
        $xmlValues = [];

        if (isset($xml->body->item->dishonestSupplierData->guid)) {
            $xmlValues['guid'] = (string)$xml->body->item->dishonestSupplierData->guid;
            $dBRecord->andWhere(['guid' => $xmlValues['guid']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->fullName)) {
            $xmlValues['placer_main_info_full_name'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->fullName;
            $dBRecord->andWhere(['placer_main_info_full_name' => $xmlValues['placer_main_info_full_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->shortName)) {
            $xmlValues['placer_main_info_short_name'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->shortName;
            $dBRecord->andWhere(['placer_main_info_short_name' => $xmlValues['placer_main_info_short_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->iko)) {
            $xmlValues['placer_main_info_iko'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->iko;
            $dBRecord->andWhere(['placer_main_info_iko' => $xmlValues['placer_main_info_iko']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->inn)) {
            $xmlValues['placer_main_info_inn'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->inn;
            $dBRecord->andWhere(['placer_main_info_inn' => $xmlValues['placer_main_info_inn']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->kpp)) {
            $xmlValues['placer_main_info_kpp'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->kpp;
            $dBRecord->andWhere(['placer_main_info_kpp' => $xmlValues['placer_main_info_kpp']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->ogrn)) {
            $xmlValues['placer_main_info_ogrn'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->ogrn;
            $dBRecord->andWhere(['placer_main_info_ogrn' => $xmlValues['placer_main_info_ogrn']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->legalAddress)) {
            $xmlValues['placer_main_info_legal_address'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->legalAddress;
            $dBRecord->andWhere(['placer_main_info_legal_address' => $xmlValues['placer_main_info_legal_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->postalAddress)) {
            $xmlValues['placer_main_info_postal_address'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->postalAddress;
            $dBRecord->andWhere(['placer_main_info_postal_address' => $xmlValues['placer_main_info_postal_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->phone)) {
            $xmlValues['placer_main_info_phone'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->phone;
            $dBRecord->andWhere(['placer_main_info_phone' => $xmlValues['placer_main_info_phone']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->fax)) {
            $xmlValues['placer_main_info_fax'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->fax;
            $dBRecord->andWhere(['placer_main_info_fax' => $xmlValues['placer_main_info_fax']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->email)) {
            $xmlValues['placer_main_info_email'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->email;
            $dBRecord->andWhere(['placer_main_info_email' => $xmlValues['placer_main_info_email']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->okato)) {
            $xmlValues['placer_main_info_okato'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->okato;
            $dBRecord->andWhere(['placer_main_info_okato' => $xmlValues['placer_main_info_okato']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->okopf)) {
            $xmlValues['placer_main_info_okopf'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->okopf;
            $dBRecord->andWhere(['placer_main_info_okopf' => $xmlValues['placer_main_info_okopf']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->okopfName)) {
            $xmlValues['placer_main_info_okopf_name'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->okopfName;
            $dBRecord->andWhere(['placer_main_info_okopf_name' => $xmlValues['placer_main_info_okopf_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->okpo)) {
            $xmlValues['placer_main_info_okpo'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->okpo;
            $dBRecord->andWhere(['placer_main_info_okpo' => $xmlValues['placer_main_info_okpo']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->placer->mainInfo->customerRegistrationDate)) {
            $xmlValues['placer_main_info_customer_registration_date'] = (string)$xml->body->item->dishonestSupplierData->placer->mainInfo->customerRegistrationDate;
            $dBRecord->andWhere(['placer_main_info_customer_registration_date' => $xmlValues['placer_main_info_customer_registration_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->fullName)) {
            $xmlValues['customer_full_name'] = (string)$xml->body->item->dishonestSupplierData->customer->fullName;
            $dBRecord->andWhere(['customer_full_name' => $xmlValues['customer_full_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->shortName)) {
            $xmlValues['customer_short_name'] = (string)$xml->body->item->dishonestSupplierData->customer->shortName;
            $dBRecord->andWhere(['customer_short_name' => $xmlValues['customer_short_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->iko)) {
            $xmlValues['customer_iko'] = (string)$xml->body->item->dishonestSupplierData->customer->iko;
            $dBRecord->andWhere(['customer_iko' => $xmlValues['customer_iko']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->inn)) {
            $xmlValues['customer_inn'] = (string)$xml->body->item->dishonestSupplierData->customer->inn;
            $dBRecord->andWhere(['customer_inn' => $xmlValues['customer_inn']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->kpp)) {
            $xmlValues['customer_kpp'] = (string)$xml->body->item->dishonestSupplierData->customer->kpp;
            $dBRecord->andWhere(['customer_kpp' => $xmlValues['customer_kpp']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->ogrn)) {
            $xmlValues['customer_ogrn'] = (string)$xml->body->item->dishonestSupplierData->customer->ogrn;
            $dBRecord->andWhere(['customer_ogrn' => $xmlValues['customer_ogrn']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->legalAddress)) {
            $xmlValues['customer_legal_address'] = (string)$xml->body->item->dishonestSupplierData->customer->legalAddress;
            $dBRecord->andWhere(['customer_legal_address' => $xmlValues['customer_legal_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->postalAddress)) {
            $xmlValues['customer_postal_address'] = (string)$xml->body->item->dishonestSupplierData->customer->postalAddress;
            $dBRecord->andWhere(['customer_postal_address' => $xmlValues['customer_postal_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->phone)) {
            $xmlValues['customer_phone'] = (string)$xml->body->item->dishonestSupplierData->customer->phone;
            $dBRecord->andWhere(['customer_phone' => $xmlValues['customer_phone']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->fax)) {
            $xmlValues['customer_fax'] = (string)$xml->body->item->dishonestSupplierData->customer->fax;
            $dBRecord->andWhere(['customer_fax' => $xmlValues['customer_fax']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->email)) {
            $xmlValues['customer_email'] = (string)$xml->body->item->dishonestSupplierData->customer->email;
            $dBRecord->andWhere(['customer_email' => $xmlValues['customer_email']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->okato)) {
            $xmlValues['customer_okato'] = (string)$xml->body->item->dishonestSupplierData->customer->okato;
            $dBRecord->andWhere(['customer_okato' => $xmlValues['customer_okato']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->okopf)) {
            $xmlValues['customer_okopf'] = (string)$xml->body->item->dishonestSupplierData->customer->okopf;
            $dBRecord->andWhere(['customer_okopf' => $xmlValues['customer_okopf']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->okopfName)) {
            $xmlValues['customer_okopf_name'] = (string)$xml->body->item->dishonestSupplierData->customer->okopfName;
            $dBRecord->andWhere(['customer_okopf_name' => $xmlValues['customer_okopf_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->okpo)) {
            $xmlValues['customer_okpo'] = (string)$xml->body->item->dishonestSupplierData->customer->okpo;
            $dBRecord->andWhere(['customer_okpo' => $xmlValues['customer_okpo']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->customerRegistrationDate)) {
            $xmlValues['customer_customer_registration_date'] = (string)$xml->body->item->dishonestSupplierData->customer->customerRegistrationDate;
            $dBRecord->andWhere(['customer_customer_registration_date' => $xmlValues['customer_customer_registration_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->country->name)) {
            $xmlValues['customer_address_country_name'] = (string)$xml->body->item->dishonestSupplierData->customer->address->country->name;
            $dBRecord->andWhere(['customer_address_country_name' => $xmlValues['customer_address_country_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->country->digitalCode)) {
            $xmlValues['customer_address_country_digital_code'] = (string)$xml->body->item->dishonestSupplierData->customer->address->country->digitalCode;
            $dBRecord->andWhere(['customer_address_country_digital_code' => $xmlValues['customer_address_country_digital_code']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->area)) {
            $xmlValues['customer_address_area'] = (string)$xml->body->item->dishonestSupplierData->customer->address->area;
            $dBRecord->andWhere(['customer_address_area' => $xmlValues['customer_address_area']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->corpus)) {
            $xmlValues['customer_address_corpus'] = (string)$xml->body->item->dishonestSupplierData->customer->address->corpus;
            $dBRecord->andWhere(['customer_address_corpus' => $xmlValues['customer_address_corpus']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->house)) {
            $xmlValues['customer_address_house'] = (string)$xml->body->item->dishonestSupplierData->customer->address->house;
            $dBRecord->andWhere(['customer_address_house' => $xmlValues['customer_address_house']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->zip)) {
            $xmlValues['customer_address_zip'] = (string)$xml->body->item->dishonestSupplierData->customer->address->zip;
            $dBRecord->andWhere(['customer_address_zip' => $xmlValues['customer_address_zip']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->region)) {
            $xmlValues['customer_address_region'] = (string)$xml->body->item->dishonestSupplierData->customer->address->region;
            $dBRecord->andWhere(['customer_address_region' => $xmlValues['customer_address_region']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->structure)) {
            $xmlValues['customer_address_structure'] = (string)$xml->body->item->dishonestSupplierData->customer->address->structure;
            $dBRecord->andWhere(['customer_address_structure' => $xmlValues['customer_address_structure']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->city)) {
            $xmlValues['customer_address_city'] = (string)$xml->body->item->dishonestSupplierData->customer->address->city;
            $dBRecord->andWhere(['customer_address_city' => $xmlValues['customer_address_city']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->settlement)) {
            $xmlValues['customer_address_settlement'] = (string)$xml->body->item->dishonestSupplierData->customer->address->settlement;
            $dBRecord->andWhere(['customer_address_settlement' => $xmlValues['customer_address_settlement']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->address->street)) {
            $xmlValues['customer_address_street'] = (string)$xml->body->item->dishonestSupplierData->customer->address->street;
            $dBRecord->andWhere(['customer_address_street' => $xmlValues['customer_address_street']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->customer->shortAddress)) {
            $xmlValues['customer_short_address'] = (string)$xml->body->item->dishonestSupplierData->customer->shortAddress;
            $dBRecord->andWhere(['customer_short_address' => $xmlValues['customer_short_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->publicationDateTime)) {
            $xmlValues['publication_date_time'] = (string)$xml->body->item->dishonestSupplierData->publicationDateTime;
            $dBRecord->andWhere(['publication_date_time' => $xmlValues['publication_date_time']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->registrationNumber)) {
            $xmlValues['registration_number'] = (string)$xml->body->item->dishonestSupplierData->registrationNumber;
            $dBRecord->andWhere(['registration_number' => $xmlValues['registration_number']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->version)) {
            $xmlValues['version'] = (string)$xml->body->item->dishonestSupplierData->version;
            $dBRecord->andWhere(['version' => $xmlValues['version']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->modificationDescription)) {
            $xmlValues['modification_description'] = (string)$xml->body->item->dishonestSupplierData->modificationDescription;
            $dBRecord->andWhere(['modification_description' => $xmlValues['modification_description']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->urlOOS)) {
            $xmlValues['url_oos'] = (string)$xml->body->item->dishonestSupplierData->urlOOS;
            $dBRecord->andWhere(['url_oos' => $xmlValues['url_oos']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->urlVSRZ)) {
            $xmlValues['url_vsrz'] = (string)$xml->body->item->dishonestSupplierData->urlVSRZ;
            $dBRecord->andWhere(['url_vsrz' => $xmlValues['url_vsrz']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->status)) {
            $xmlValues['status'] = (string)$xml->body->item->dishonestSupplierData->status;
            $dBRecord->andWhere(['status' => $xmlValues['status']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->createDateTime)) {
            $xmlValues['create_date_time'] = (string)$xml->body->item->dishonestSupplierData->createDateTime;
            $dBRecord->andWhere(['create_date_time' => $xmlValues['create_date_time']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->includeReason)) {
            $xmlValues['cai_include_reason'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->includeReason;
            $dBRecord->andWhere(['cai_include_reason' => $xmlValues['cai_include_reason']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->includeReasonText)) {
            $xmlValues['cai_include_reason_text'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->includeReasonText;
            $dBRecord->andWhere(['cai_include_reason_text' => $xmlValues['cai_include_reason_text']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->fullName)) {
            $xmlValues['cai_control_agency_full_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->fullName;
            $dBRecord->andWhere(['cai_control_agency_full_name' => $xmlValues['cai_control_agency_full_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->shortName)) {
            $xmlValues['cai_control_agency_short_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->shortName;
            $dBRecord->andWhere(['cai_control_agency_short_name' => $xmlValues['cai_control_agency_short_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->iko)) {
            $xmlValues['cai_control_agency_iko'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->iko;
            $dBRecord->andWhere(['cai_control_agency_iko' => $xmlValues['cai_control_agency_iko']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->inn)) {
            $xmlValues['cai_control_agency_inn'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->inn;
            $dBRecord->andWhere(['cai_control_agency_inn' => $xmlValues['cai_control_agency_inn']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->kpp)) {
            $xmlValues['cai_control_agency_kpp'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->kpp;
            $dBRecord->andWhere(['cai_control_agency_kpp' => $xmlValues['cai_control_agency_kpp']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->ogrn)) {
            $xmlValues['cai_control_agency_ogrn'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->ogrn;
            $dBRecord->andWhere(['cai_control_agency_ogrn' => $xmlValues['cai_control_agency_ogrn']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->legalAddress)) {
            $xmlValues['cai_control_agency_legal_address'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->legalAddress;
            $dBRecord->andWhere(['cai_control_agency_legal_address' => $xmlValues['cai_control_agency_legal_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->postalAddress)) {
            $xmlValues['cai_control_agency_postal_address'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->postalAddress;
            $dBRecord->andWhere(['cai_control_agency_postal_address' => $xmlValues['cai_control_agency_postal_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->phone)) {
            $xmlValues['cai_control_agency_phone'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->phone;
            $dBRecord->andWhere(['cai_control_agency_phone' => $xmlValues['cai_control_agency_phone']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->fax)) {
            $xmlValues['cai_control_agency_fax'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->fax;
            $dBRecord->andWhere(['cai_control_agency_fax' => $xmlValues['cai_control_agency_fax']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->email)) {
            $xmlValues['cai_control_agency_email'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->email;
            $dBRecord->andWhere(['cai_control_agency_email' => $xmlValues['cai_control_agency_email']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okato)) {
            $xmlValues['cai_control_agency_okato'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okato;
            $dBRecord->andWhere(['cai_control_agency_okato' => $xmlValues['cai_control_agency_okato']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okopf)) {
            $xmlValues['cai_control_agency_okopf'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okopf;
            $dBRecord->andWhere(['cai_control_agency_okopf' => $xmlValues['cai_control_agency_okopf']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okopfName)) {
            $xmlValues['cai_control_agency_okopf_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okopfName;
            $dBRecord->andWhere(['cai_control_agency_okopf_name' => $xmlValues['cai_control_agency_okopf_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okpo)) {
            $xmlValues['cai_control_agency_okpo'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->okpo;
            $dBRecord->andWhere(['cai_control_agency_okpo' => $xmlValues['cai_control_agency_okpo']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->customerRegistrationDate)) {
            $xmlValues['cai_control_agency_customer_registration_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->controlAgency->customerRegistrationDate;
            $dBRecord->andWhere(['cai_control_agency_customer_registration_date' => $xmlValues['cai_control_agency_customer_registration_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->topSecret)) {
            $xmlValues['cai_top_secret'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->topSecret;
            $dBRecord->andWhere(['cai_top_secret' => $xmlValues['cai_top_secret']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->name)) {
            $xmlValues['cai_supplier_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->name;
            $dBRecord->andWhere(['cai_supplier_name' => $xmlValues['cai_supplier_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->inn)) {
            $xmlValues['cai_supplier_inn'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->inn;
            $dBRecord->andWhere(['cai_supplier_inn' => $xmlValues['cai_supplier_inn']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->kpp)) {
            $xmlValues['cai_supplier_kpp'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->kpp;
            $dBRecord->andWhere(['cai_supplier_kpp' => $xmlValues['cai_supplier_kpp']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->address)) {
            $xmlValues['cai_supplier_address'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->address;
            $dBRecord->andWhere(['cai_supplier_address' => $xmlValues['cai_supplier_address']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->email)) {
            $xmlValues['cai_supplier_email'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->supplier->email;
            $dBRecord->andWhere(['cai_supplier_email' => $xmlValues['cai_supplier_email']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseNoticeNumber)) {
            $xmlValues['cai_purchase_info_purchase_notice_number'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseNoticeNumber;
            $dBRecord->andWhere(['cai_purchase_info_purchase_notice_number' => $xmlValues['cai_purchase_info_purchase_notice_number']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->publicationDateTime)) {
            $xmlValues['cai_purchase_info_publication_date_time'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->publicationDateTime;
            $dBRecord->andWhere(['cai_purchase_info_publication_date_time' => $xmlValues['cai_purchase_info_publication_date_time']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->name)) {
            $xmlValues['cai_purchase_info_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->name;
            $dBRecord->andWhere(['cai_purchase_info_name' => $xmlValues['cai_purchase_info_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseMethod)) {
            $xmlValues['cai_purchase_info_purchase_method'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseMethod;
            $dBRecord->andWhere(['cai_purchase_info_purchase_method' => $xmlValues['cai_purchase_info_purchase_method']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseMethodCode)) {
            $xmlValues['cai_purchase_info_purchase_method_code'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseMethodCode;
            $dBRecord->andWhere(['cai_purchase_info_purchase_method_code' => $xmlValues['cai_purchase_info_purchase_method_code']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseMethodCodeName)) {
            $xmlValues['cai_purchase_info_purchase_method_code_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseMethodCodeName;
            $dBRecord->andWhere(['cai_purchase_info_purchase_method_code_name' => $xmlValues['cai_purchase_info_purchase_method_code_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->emergency)) {
            $xmlValues['cai_purchase_info_emergency'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->emergency;
            $dBRecord->andWhere(['cai_purchase_info_emergency' => $xmlValues['cai_purchase_info_emergency']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->lotInfo)) {
            $xmlValues['cai_purchase_info_lot_info'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->lotInfo;
            $dBRecord->andWhere(['cai_purchase_info_lot_info' => $xmlValues['cai_purchase_info_lot_info']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseDate)) {
            $xmlValues['cai_purchase_info_purchase_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->purchaseDate;
            $dBRecord->andWhere(['cai_purchase_info_purchase_date' => $xmlValues['cai_purchase_info_purchase_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->summingupDate)) {
            $xmlValues['cai_purchase_info_summingup_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->summingupDate;
            $dBRecord->andWhere(['cai_purchase_info_summingup_date' => $xmlValues['cai_purchase_info_summingup_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->cancellationDate)) {
            $xmlValues['cai_purchase_info_cancellation_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->cancellationDate;
            $dBRecord->andWhere(['cai_purchase_info_cancellation_date' => $xmlValues['cai_purchase_info_cancellation_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->documentRequisites->documentNumber)) {
            $xmlValues['cai_purchase_info_document_requisites_document_number'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->documentRequisites->documentNumber;
            $dBRecord->andWhere(['cai_purchase_info_document_requisites_document_number' => $xmlValues['cai_purchase_info_document_requisites_document_number']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->documentRequisites->documentName)) {
            $xmlValues['cai_purchase_info_document_requisites_document_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->documentRequisites->documentName;
            $dBRecord->andWhere(['cai_purchase_info_document_requisites_document_name' => $xmlValues['cai_purchase_info_document_requisites_document_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->documentRequisites->documentDate)) {
            $xmlValues['cai_purchase_info_document_requisites_document_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->purchaseInfo->documentRequisites->documentDate;
            $dBRecord->andWhere(['cai_purchase_info_document_requisites_document_date' => $xmlValues['cai_purchase_info_document_requisites_document_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->contractNumber)) {
            $xmlValues['cai_contract_info_contract_number'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->contractNumber;
            $dBRecord->andWhere(['cai_contract_info_contract_number' => $xmlValues['cai_contract_info_contract_number']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->contractDate)) {
            $xmlValues['cai_contract_info_contract_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->contractDate;
            $dBRecord->andWhere(['cai_contract_info_contract_date' => $xmlValues['cai_contract_info_contract_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->name)) {
            $xmlValues['cai_contract_info_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->name;
            $dBRecord->andWhere(['cai_contract_info_name' => $xmlValues['cai_contract_info_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->currency->code)) {
            $xmlValues['cai_contract_info_currency_code'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->currency->code;
            $dBRecord->andWhere(['cai_contract_info_currency_code' => $xmlValues['cai_contract_info_currency_code']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->currency->digitalCode)) {
            $xmlValues['cai_contract_info_currency_digital_code'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->currency->digitalCode;
            $dBRecord->andWhere(['cai_contract_info_currency_digital_code' => $xmlValues['cai_contract_info_currency_digital_code']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->currency->name)) {
            $xmlValues['cai_contract_info_currency_name'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->currency->name;
            $dBRecord->andWhere(['cai_contract_info_currency_name' => $xmlValues['cai_contract_info_currency_name']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->sum)) {
            $xmlValues['cai_contract_info_sum'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->sum;
            $dBRecord->andWhere(['cai_contract_info_sum' => $xmlValues['cai_contract_info_sum']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->sumInfo)) {
            $xmlValues['cai_contract_info_sum_info'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->sumInfo;
            $dBRecord->andWhere(['cai_contract_info_sum_info' => $xmlValues['cai_contract_info_sum_info']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->fulfillmentDate)) {
            $xmlValues['cai_contract_info_fulfillment_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->fulfillmentDate;
            $dBRecord->andWhere(['cai_contract_info_fulfillment_date' => $xmlValues['cai_contract_info_fulfillment_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->cancellationReason)) {
            $xmlValues['cai_contract_info_cancellation_reason'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->cancellationReason;
            $dBRecord->andWhere(['cai_contract_info_cancellation_reason' => $xmlValues['cai_contract_info_cancellation_reason']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->cancellationJudgmentDate)) {
            $xmlValues['cai_contract_info_cancellation_judgment_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->cancellationJudgmentDate;
            $dBRecord->andWhere(['cai_contract_info_cancellation_judgment_date' => $xmlValues['cai_contract_info_cancellation_judgment_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->cancellationDate)) {
            $xmlValues['cai_contract_info_cancellation_date'] = (string)$xml->body->item->dishonestSupplierData->commonApplicationInfo->contractInfo->cancellationDate;
            $dBRecord->andWhere(['cai_contract_info_cancellation_date' => $xmlValues['cai_contract_info_cancellation_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->approveStatus)) {
            $xmlValues['approve_status'] = (string)$xml->body->item->dishonestSupplierData->approveStatus;
            $dBRecord->andWhere(['approve_status' => $xmlValues['approve_status']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->reviewDate)) {
            $xmlValues['review_date'] = (string)$xml->body->item->dishonestSupplierData->reviewDate;
            $dBRecord->andWhere(['review_date' => $xmlValues['review_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->approveDate)) {
            $xmlValues['approve_date'] = (string)$xml->body->item->dishonestSupplierData->approveDate;
            $dBRecord->andWhere(['approve_date' => $xmlValues['approve_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->rejectionDate)) {
            $xmlValues['rejection_date'] = (string)$xml->body->item->dishonestSupplierData->rejectionDate;
            $dBRecord->andWhere(['rejection_date' => $xmlValues['rejection_date']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->rejectionReason)) {
            $xmlValues['rejection_reason'] = (string)$xml->body->item->dishonestSupplierData->rejectionReason;
            $dBRecord->andWhere(['rejection_reason' => $xmlValues['rejection_reason']]);
        }
        if (isset($xml->body->item->dishonestSupplierData->attachments->totalDocumentsCount)) {
            $xmlValues['attachments_total_documents_count'] = (string)$xml->body->item->dishonestSupplierData->attachments->totalDocumentsCount;
            $dBRecord->andWhere(['attachments_total_documents_count' => $xmlValues['attachments_total_documents_count']]);
        }

        if (isset($xml->body->item->dishonestSupplierData->attachments->additionalDocumentsCount)) {
            $xmlValues['attachments_additional_documents_count'] = (string)$xml->body->item->dishonestSupplierData->attachments->additionalDocumentsCount;
            $dBRecord->andWhere(['attachments_additional_documents_count' => $xmlValues['attachments_additional_documents_count']]);
        }

        $result = $dBRecord->one();

        if (!$result) {
            $unfairSupplier = new self();
            $unfairSupplier->attributes = $xmlValues;

            if (!$unfairSupplier->save()) {
                print_r($unfairSupplier->getErrors());
                var_dump($xml);
                die;
            }
        }
    }

    public static function downloadFromFTP($connect, $contents, $path, $extractedPath)
    {
        $counter = 0;
        $counterInner = 0;
        //Проход по /out/published
        foreach ($contents as $file) {
            if ($file != "/out/published/archive" && $file != "/out/published/undefined") {
                $ftpFolder = $file . '/dishonestSupplier';
                $contentFiles = ftp_nlist($connect, $ftpFolder);
                foreach ($contentFiles as $zipFile) {
                    if ($zipFile != $file . '/dishonestSupplier/daily') {
                        $local_file = $path . basename($zipFile);
                        $counterInner++;
                        //Проверка на существование файлов локально
                        if (file_exists($local_file)) {
                            $counter++;
                            continue;
                        }
                        print_r($zipFile);
                        echo PHP_EOL;
                        $handle = fopen($local_file, 'w');
                        if (ftp_fget($connect, $handle, $zipFile, FTP_BINARY, 0)) {
                            //Распаковка файлов
                            $zip = new \ZipArchive;
                            if ($zip->open($local_file) === true) {
                                $zip->extractTo($extractedPath);
                                $zip->close();
                            } else {
                                echo "Архив не найден";
                            }
                        } else {
                            echo "При скачке $zipFile в $local_file произошла ошибка\n";
                        }
                        if ($counterInner % 10 == 0) {
                            print_r("Обработано файлов: " . $counterInner);
                            echo PHP_EOL;
                            print_r("Текущая папка: " . $ftpFolder);
                            echo PHP_EOL;
                            //    break;
                        }
                    }
                }
            }
        }
        print_r("Скачивание по FTP завершено. Пропущено файлов: " . $counter);
        echo PHP_EOL;
    }

    public function fields()
    {
        return [
            'id',
            'table_name' => function () {
                return self::tableName();
            },
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UnfairSupplier223Query the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UnfairSupplier223Query(get_called_class());
    }
}
