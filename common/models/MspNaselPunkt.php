<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_nasel_punkt".
 *
 * @property integer $id
 * @property string $tip
 * @property string $naim
 * @property integer $msp_sved_mn_id
 *
 * @property MspSvedMn $mspSvedMn
 */
class MspNaselPunkt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_nasel_punkt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tip', 'naim'], 'string'],
            [['msp_sved_mn_id'], 'required'],
            [['msp_sved_mn_id'], 'integer'],
            [['msp_sved_mn_id'], 'unique'],
            [
                ['msp_sved_mn_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspSvedMn::className(),
                'targetAttribute' => ['msp_sved_mn_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tip' => Yii::t('app', 'Tip'),
            'naim' => Yii::t('app', 'Naim'),
            'msp_sved_mn_id' => Yii::t('app', 'Msp Sved Mn ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvedMn()
    {
        return $this->hasOne(MspSvedMn::className(), ['id' => 'msp_sved_mn_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspNaselPunktQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspNaselPunktQuery(get_called_class());
    }
}
