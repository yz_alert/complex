<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "terrorists_ul_rus".
 *
 * @property integer $id
 * @property string $name
 * @property string $additional_info
 */
class TerroristsUlRus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terrorists_ul_rus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'additional_info'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'additional_info' => Yii::t('app', 'Additional Info'),
        ];
    }

    public function fields()
    {
        return [
            //'id',
            'name',
            'additional_info',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\TerroristsUlRusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TerroristsUlRusQuery(get_called_class());
    }
}
