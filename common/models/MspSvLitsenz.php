<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sv_litsenz".
 *
 * @property integer $id
 * @property integer $msp_dokument_id
 * @property string $naim_litsenz_vd
 * @property string $sved_adr_lits_vd
 * @property string $ser_litsenz
 * @property string $nom_litsenz
 * @property string $vid_litsenz
 * @property string $data_litsenz
 * @property string $data_nach_litsenz
 * @property string $data_kon_litsenz
 * @property string $org_vyd_litsenz
 * @property string $data_ost_litsenz
 * @property string $org_ost_litsenz
 *
 * @property MspDokument $mspDokument
 */
class MspSvLitsenz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sv_litsenz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [
                [
                    'naim_litsenz_vd',
                    'sved_adr_lits_vd',
                    'ser_litsenz',
                    'nom_litsenz',
                    'vid_litsenz',
                    'org_vyd_litsenz',
                    'org_ost_litsenz',
                ],
                'string',
            ],
            [['data_litsenz', 'data_nach_litsenz', 'data_kon_litsenz', 'data_ost_litsenz'], 'safe'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
            'naim_litsenz_vd' => Yii::t('app', 'Naim Litsenz Vd'),
            'sved_adr_lits_vd' => Yii::t('app', 'Sved Adr Lits Vd'),
            'ser_litsenz' => Yii::t('app', 'Ser Litsenz'),
            'nom_litsenz' => Yii::t('app', 'Nom Litsenz'),
            'vid_litsenz' => Yii::t('app', 'Vid Litsenz'),
            'data_litsenz' => Yii::t('app', 'Data Litsenz'),
            'data_nach_litsenz' => Yii::t('app', 'Data Nach Litsenz'),
            'data_kon_litsenz' => Yii::t('app', 'Data Kon Litsenz'),
            'org_vyd_litsenz' => Yii::t('app', 'Org Vyd Litsenz'),
            'data_ost_litsenz' => Yii::t('app', 'Data Ost Litsenz'),
            'org_ost_litsenz' => Yii::t('app', 'Org Ost Litsenz'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvLitsenzQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvLitsenzQuery(get_called_class());
    }
}
