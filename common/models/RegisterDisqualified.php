<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "register_disqualified".
 *
 * @property integer $id
 * @property string $number
 * @property string $full_name
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $organization_name
 * @property string $organization_inn
 * @property string $position
 * @property string $administrative_code_article
 * @property string $protocol_organization_name
 * @property string $judge_full_name
 * @property string $judge_position
 * @property string $period_of_ineligibility
 * @property string $start_date
 * @property string $expiration_date
 */
class RegisterDisqualified extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return Yii::$app->get('db_kaleka');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'register_disqualified';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'number',
                    'full_name',
                    'date_of_birth',
                    'place_of_birth',
                    'organization_name',
                    'protocol_organization_name',
                    'judge_full_name',
                    'period_of_ineligibility',
                    'expiration_date',
                ],
                'required',
            ],
            [['number'], 'integer'],
            [
                [
                    'full_name',
                    'place_of_birth',
                    'organization_name',
                    'organization_inn',
                    'position',
                    'administrative_code_article',
                    'protocol_organization_name',
                    'judge_full_name',
                    'judge_position',
                    'period_of_ineligibility',
                    'date_of_birth',
                    'start_date',
                    'expiration_date',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер записи из реестра дисквалифицированных лиц'),
            'number' => Yii::t('app', 'The number of the record from the register of disqualified persons'),
            'full_name' => Yii::t('app', 'Full name'),
            'date_of_birth' => Yii::t('app', 'Date of birth of the person'),
            'place_of_birth' => Yii::t('app', 'Place of birth'),
            'organization_name' => Yii::t('app',
                'Name of the organization where the person worked during the time of the offence'),
            'organization_inn' => Yii::t('app', 'Identification number of the organization'),
            'position' => Yii::t('app', 'The post in which the person worked during the time of the offence'),
            'administrative_code_article' => Yii::t('app',
                'Article of the administrative code, providing for administrative responsibility for committing an administrative offense'),
            'protocol_organization_name' => Yii::t('app',
                'Name of the organization who made the Protocol on an administrative offence'),
            'judge_full_name' => Yii::t('app', 'Full name of the judge who made the decision on disqualification'),
            'judge_position' => Yii::t('app', 'The position of the judge'),
            'period_of_ineligibility' => Yii::t('app', 'The period of Ineligibility'),
            'start_date' => Yii::t('app', 'Start date'),
            'expiration_date' => Yii::t('app', 'Date of expiry of the period of Ineligibility'),
        ];
    }

    public function fields()
    {
        return [
            //'id',
            'number',
            'full_name',
            'date_of_birth',
            'place_of_birth',
            'organization_name',
            'organization_inn',
            'position',
            'administrative_code_article',
            'protocol_organization_name',
            'judge_full_name',
            'judge_position',
            'period_of_ineligibility',
            'start_date',
            'expiration_date',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\RegisterDisqualifiedQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\RegisterDisqualifiedQuery(get_called_class());
    }
}
