<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unfair_supplier_615".
 *
 * @property string $id
 * @property string $registry_num
 * @property string $publish_date
 * @property string $approve_date
 * @property string $state
 * @property string $publish_org_reg_num
 * @property string $publish_org_cons_registry_num
 * @property string $publish_org_full_name
 * @property string $create_reason
 * @property string $approve_reason
 * @property string $customer_info_reg_num
 * @property string $customer_info_cons_registry_num
 * @property string $customer_info_full_name
 * @property string $customer_info_post_address
 * @property string $customer_info_inn
 * @property string $customer_info_kpp
 * @property string $customer_info_email
 * @property string $unfair_supplier_info_registry_num
 * @property string $unfair_supplier_info_type
 * @property string $unfair_supplier_info_full_name
 * @property string $unfair_supplier_info_firm_name
 * @property string $unfair_supplier_info_tax_payer_code
 * @property string $unfair_supplier_info_kpp
 * @property string $unfair_supplier_info_founders_info_names
 * @property string $unfair_supplier_info_founders_info_tax_payer_code
 * @property string $unfair_supplier_info_place_kladr_kladr_code
 * @property string $unfair_supplier_info_place_kladr_full_name
 * @property string $unfair_supplier_info_place_country_country_code
 * @property string $unfair_supplier_info_place_country_full_name
 * @property string $unfair_supplier_info_place_delivery_place
 * @property string $place_no_kladr_for_region_settlement_region
 * @property string $place_no_kladr_for_region_settlement_settlement
 * @property string $unfair_supplier_info_email
 * @property string $purchase_info_purchase_number
 * @property string $purchase_info_purchase_object_info
 * @property string $purchase_info_placing_way_name
 * @property string $purchase_info_protocol_date
 * @property string $purchase_info_purchase_subject_info_code
 * @property string $purchase_info_purchase_subject_info_name
 * @property string $purchase_info_document_info_doc_name
 * @property string $purchase_info_document_info_doc_number
 * @property string $purchase_info_document_info_doc_date
 * @property string $contract_info_reg_num
 * @property string $contract_info_sign_date
 * @property string $contract_info_purchase_subject_info_code
 * @property string $contract_info_purchase_subject_info_name
 * @property string $contract_info_price
 * @property string $contract_info_perfomance_date
 * @property string $contract_info_cancel_info_base_doc_info_doc_name
 * @property string $contract_info_cancel_info_base_doc_info_doc_number
 * @property string $contract_info_cancel_info_base_doc_info_doc_date
 * @property string $contract_info_cancel_info_cancel_date
 * @property string $exclude_info_basis
 * @property string $exclude_info_documents_info_document_info_doc_name
 * @property string $exclude_info_documents_info_document_info_doc_number
 * @property string $exclude_info_documents_info_document_info_doc_date
 * @property string $exclude_info_date
 * @property string $exclude_info_type
 */
class UnfairSupplier615 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unfair_supplier_615';
    }

    static function getDb()
    {
        return Yii::$app->db_kaleka;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['registry_num'], 'required'],
            [
                [
                    'registry_num',
                    'publish_date',
                    'approve_date',
                    'state',
                    'publish_org_reg_num',
                    'publish_org_cons_registry_num',
                    'publish_org_full_name',
                    'create_reason',
                    'approve_reason',
                    'customer_info_reg_num',
                    'customer_info_cons_registry_num',
                    'customer_info_full_name',
                    'customer_info_post_address',
                    'customer_info_inn',
                    'customer_info_kpp',
                    'customer_info_email',
                    'unfair_supplier_info_registry_num',
                    'unfair_supplier_info_type',
                    'unfair_supplier_info_full_name',
                    'unfair_supplier_info_firm_name',
                    'unfair_supplier_info_tax_payer_code',
                    'unfair_supplier_info_kpp',
                    'unfair_supplier_info_founders_info_names',
                    'unfair_supplier_info_founders_info_tax_payer_code',
                    'unfair_supplier_info_place_kladr_kladr_code',
                    'unfair_supplier_info_place_kladr_full_name',
                    'unfair_supplier_info_place_country_country_code',
                    'unfair_supplier_info_place_country_full_name',
                    'unfair_supplier_info_place_delivery_place',
                    'place_no_kladr_for_region_settlement_region',
                    'place_no_kladr_for_region_settlement_settlement',
                    'unfair_supplier_info_email',
                    'purchase_info_purchase_number',
                    'purchase_info_purchase_object_info',
                    'purchase_info_placing_way_name',
                    'purchase_info_protocol_date',
                    'purchase_info_purchase_subject_info_code',
                    'purchase_info_purchase_subject_info_name',
                    'purchase_info_document_info_doc_name',
                    'purchase_info_document_info_doc_number',
                    'purchase_info_document_info_doc_date',
                    'contract_info_reg_num',
                    'contract_info_sign_date',
                    'contract_info_purchase_subject_info_code',
                    'contract_info_purchase_subject_info_name',
                    'contract_info_price',
                    'contract_info_perfomance_date',
                    'contract_info_cancel_info_base_doc_info_doc_name',
                    'contract_info_cancel_info_base_doc_info_doc_number',
                    'contract_info_cancel_info_base_doc_info_doc_date',
                    'contract_info_cancel_info_cancel_date',
                    'exclude_info_basis',
                    'exclude_info_documents_info_document_info_doc_name',
                    'exclude_info_documents_info_document_info_doc_number',
                    'exclude_info_documents_info_document_info_doc_date',
                    'exclude_info_date',
                    'exclude_info_type',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'registry_num' => Yii::t('app', 'Registry Num'),
            'publish_date' => Yii::t('app', 'Publish Date'),
            'approve_date' => Yii::t('app', 'Approve Date'),
            'state' => Yii::t('app', 'State'),
            'publish_org_reg_num' => Yii::t('app', 'Publish Org Reg Num'),
            'publish_org_cons_registry_num' => Yii::t('app', 'Publish Org Cons Registry Num'),
            'publish_org_full_name' => Yii::t('app', 'Publish Org Full Name'),
            'create_reason' => Yii::t('app', 'Create Reason'),
            'approve_reason' => Yii::t('app', 'Approve Reason'),
            'customer_info_reg_num' => Yii::t('app', 'Customer Info Reg Num'),
            'customer_info_cons_registry_num' => Yii::t('app', 'Customer Info Cons Registry Num'),
            'customer_info_full_name' => Yii::t('app', 'Customer Info Full Name'),
            'customer_info_post_address' => Yii::t('app', 'Customer Info Post Address'),
            'customer_info_inn' => Yii::t('app', 'Customer Info Inn'),
            'customer_info_kpp' => Yii::t('app', 'Customer Info Kpp'),
            'customer_info_email' => Yii::t('app', 'Customer Info Email'),
            'unfair_supplier_info_registry_num' => Yii::t('app', 'Unfair Supplier Info Registry Num'),
            'unfair_supplier_info_type' => Yii::t('app', 'Unfair Supplier Info Type'),
            'unfair_supplier_info_full_name' => Yii::t('app', 'Unfair Supplier Info Full Name'),
            'unfair_supplier_info_firm_name' => Yii::t('app', 'Unfair Supplier Info Firm Name'),
            'unfair_supplier_info_tax_payer_code' => Yii::t('app', 'Unfair Supplier Info Tax Payer Code'),
            'unfair_supplier_info_kpp' => Yii::t('app', 'Unfair Supplier Info Kpp'),
            'unfair_supplier_info_founders_info_names' => Yii::t('app', 'Unfair Supplier Info Founders Info Names'),
            'unfair_supplier_info_founders_info_tax_payer_code' => Yii::t('app',
                'Unfair Supplier Info Founders Info Tax Payer Code'),
            'unfair_supplier_info_place_kladr_kladr_code' => Yii::t('app',
                'Unfair Supplier Info Place Kladr Kladr Code'),
            'unfair_supplier_info_place_kladr_full_name' => Yii::t('app', 'Unfair Supplier Info Place Kladr Full Name'),
            'unfair_supplier_info_place_country_country_code' => Yii::t('app',
                'Unfair Supplier Info Place Country Country Code'),
            'unfair_supplier_info_place_country_full_name' => Yii::t('app',
                'Unfair Supplier Info Place Country Full Name'),
            'unfair_supplier_info_place_delivery_place' => Yii::t('app', 'Unfair Supplier Info Place Delivery Place'),
            'place_no_kladr_for_region_settlement_region' => Yii::t('app',
                'Place No Kladr For Region Settlement Region'),
            'place_no_kladr_for_region_settlement_settlement' => Yii::t('app',
                'Place No Kladr For Region Settlement Settlement'),
            'unfair_supplier_info_email' => Yii::t('app', 'Unfair Supplier Info Email'),
            'purchase_info_purchase_number' => Yii::t('app', 'Purchase Info Purchase Number'),
            'purchase_info_purchase_object_info' => Yii::t('app', 'Purchase Info Purchase Object Info'),
            'purchase_info_placing_way_name' => Yii::t('app', 'Purchase Info Placing Way Name'),
            'purchase_info_protocol_date' => Yii::t('app', 'Purchase Info Protocol Date'),
            'purchase_info_purchase_subject_info_code' => Yii::t('app', 'Purchase Info Purchase Subject Info Code'),
            'purchase_info_purchase_subject_info_name' => Yii::t('app', 'Purchase Info Purchase Subject Info Name'),
            'purchase_info_document_info_doc_name' => Yii::t('app', 'Purchase Info Document Info Doc Name'),
            'purchase_info_document_info_doc_number' => Yii::t('app', 'Purchase Info Document Info Doc Number'),
            'purchase_info_document_info_doc_date' => Yii::t('app', 'Purchase Info Document Info Doc Date'),
            'contract_info_reg_num' => Yii::t('app', 'Contract Info Reg Num'),
            'contract_info_sign_date' => Yii::t('app', 'Contract Info Sign Date'),
            'contract_info_purchase_subject_info_code' => Yii::t('app', 'Contract Info Purchase Subject Info Code'),
            'contract_info_purchase_subject_info_name' => Yii::t('app', 'Contract Info Purchase Subject Info Name'),
            'contract_info_price' => Yii::t('app', 'Contract Info Price'),
            'contract_info_perfomance_date' => Yii::t('app', 'Contract Info Perfomance Date'),
            'contract_info_cancel_info_base_doc_info_doc_name' => Yii::t('app',
                'Contract Info Cancel Info Base Doc Info Doc Name'),
            'contract_info_cancel_info_base_doc_info_doc_number' => Yii::t('app',
                'Contract Info Cancel Info Base Doc Info Doc Number'),
            'contract_info_cancel_info_base_doc_info_doc_date' => Yii::t('app',
                'Contract Info Cancel Info Base Doc Info Doc Date'),
            'contract_info_cancel_info_cancel_date' => Yii::t('app', 'Contract Info Cancel Info Cancel Date'),
            'exclude_info_basis' => Yii::t('app', 'Exclude Info Basis'),
            'exclude_info_documents_info_document_info_doc_name' => Yii::t('app',
                'Exclude Info Documents Info Document Info Doc Name'),
            'exclude_info_documents_info_document_info_doc_number' => Yii::t('app',
                'Exclude Info Documents Info Document Info Doc Number'),
            'exclude_info_documents_info_document_info_doc_date' => Yii::t('app',
                'Exclude Info Documents Info Document Info Doc Date'),
            'exclude_info_date' => Yii::t('app', 'Exclude Info Date'),
            'exclude_info_type' => Yii::t('app', 'Exclude Info Type'),
        ];
    }

    public static function downloadFromFTP($connect, $contents, $path, $extractedPath)
    {
        $counter = 0;
        foreach ($contents as $file) {
            $local_file = $path . basename($file);
            if (file_exists($local_file)) {
                $counter++;
                continue;
            }
            $handle = fopen($local_file, 'w');
            if (ftp_fget($connect, $handle, $file, FTP_BINARY, 0)) {
                //Распаковка файлов
                $zip = new \ZipArchive;
                if ($zip->open($local_file) === true) {
                    $zip->extractTo($extractedPath);
                    $zip->close();
                } else {
                    echo "Архив не найден";
                }
            } else {
                echo "При скачке $file в $local_file произошла проблема\n";
            }
            $counter++;
            if ($counter % 10 == 0) {
                print_r("Обработано файлов: " . $counter);
                echo PHP_EOL;
            }

        }
        print_r("Скачивание по FTP завершено. Пропущено файлов: " . $counter);
        echo PHP_EOL;
    }

    public static function newRecord($xml)
    {

        $dBRecord = self::find();
        $xmlValues = [];

        if (isset($xml->pprf615UnfairContractor->commonInfo->registryNum)) {
            $xmlValues['registry_num'] = (string)$xml->pprf615UnfairContractor->commonInfo->registryNum;
            $dBRecord->andWhere(['registry_num' => $xmlValues['registry_num']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->publishDate)) {
            $xmlValues['publish_date'] = date('Y-m-d',
                strtotime($xml->pprf615UnfairContractor->commonInfo->publishDate));
            $dBRecord->andWhere(['publish_date' => $xmlValues['publish_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->approveDate)) {
            $xmlValues['approve_date'] = date('Y-m-d',
                strtotime($xml->pprf615UnfairContractor->commonInfo->approveDate));
            $dBRecord->andWhere(['approve_date' => $xmlValues['approve_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->state)) {
            $xmlValues['state'] = (string)$xml->pprf615UnfairContractor->commonInfo->state;
            $dBRecord->andWhere(['state' => $xmlValues['state']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->publishOrg->regNum)) {
            $xmlValues['publish_org_reg_num'] = (string)$xml->pprf615UnfairContractor->commonInfo->publishOrg->regNum;
            $dBRecord->andWhere(['publish_org_reg_num' => $xmlValues['publish_org_reg_num']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->publishOrg->consRegistryNum)) {
            $xmlValues['publish_org_cons_registry_num'] = (string)$xml->pprf615UnfairContractor->commonInfo->publishOrg->consRegistryNum;
            $dBRecord->andWhere(['publish_org_cons_registry_num' => $xmlValues['publish_org_cons_registry_num']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->publishOrg->fullName)) {
            $xmlValues['publish_org_full_name'] = (string)$xml->pprf615UnfairContractor->commonInfo->publishOrg->fullName;
            $dBRecord->andWhere(['publish_org_full_name' => $xmlValues['publish_org_full_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->createReason)) {
            $xmlValues['create_reason'] = (string)$xml->pprf615UnfairContractor->commonInfo->createReason;
            $dBRecord->andWhere(['create_reason' => $xmlValues['create_reason']]);
        }
        if (isset($xml->pprf615UnfairContractor->commonInfo->approveReason)) {
            $xmlValues['approve_reason'] = (string)$xml->pprf615UnfairContractor->commonInfo->approveReason;
            $dBRecord->andWhere(['approve_reason' => $xmlValues['approve_reason']]);
        }
        if (isset($xml->pprf615UnfairContractor->customerInfo->regNum)) {
            $xmlValues['customer_info_reg_num'] = (string)$xml->pprf615UnfairContractor->customerInfo->regNum;
            $dBRecord->andWhere(['customer_info_reg_num' => $xmlValues['customer_info_reg_num']]);
        }
        if (isset($xml->pprf615UnfairContractor->customerInfo->consRegistryNum)) {
            $xmlValues['customer_info_cons_registry_num'] = (string)$xml->pprf615UnfairContractor->customerInfo->consRegistryNum;
            $dBRecord->andWhere(['customer_info_cons_registry_num' => $xmlValues['customer_info_cons_registry_num']]);
        }
        if (isset($xml->pprf615UnfairContractor->customerInfo->fullName)) {
            $xmlValues['customer_info_full_name'] = (string)$xml->pprf615UnfairContractor->customerInfo->fullName;
            $dBRecord->andWhere(['customer_info_full_name' => $xmlValues['customer_info_full_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->customerInfo->postAddress)) {
            $xmlValues['customer_info_post_address'] = (string)$xml->pprf615UnfairContractor->customerInfo->postAddress;
            $dBRecord->andWhere(['customer_info_post_address' => $xmlValues['customer_info_post_address']]);
        }
        if (isset($xml->pprf615UnfairContractor->customerInfo->INN)) {
            $xmlValues['customer_info_inn'] = (string)$xml->pprf615UnfairContractor->customerInfo->INN;
            $dBRecord->andWhere(['customer_info_inn' => $xmlValues['customer_info_inn']]);
        }
        if (isset($xml->pprf615UnfairContractor->customerInfo->KPP)) {
            $xmlValues['customer_info_kpp'] = (string)$xml->pprf615UnfairContractor->customerInfo->KPP;
            $dBRecord->andWhere(['customer_info_kpp' => $xmlValues['customer_info_kpp']]);
        }
        if (isset($xml->pprf615UnfairContractor->customerInfo->eMail)) {
            $xmlValues['customer_info_email'] = (string)$xml->pprf615UnfairContractor->customerInfo->eMail;
            $dBRecord->andWhere(['customer_info_email' => $xmlValues['customer_info_email']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->registryNum)) {
            $xmlValues['unfair_supplier_info_registry_num'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->registryNum;
            $dBRecord->andWhere(['unfair_supplier_info_registry_num' => $xmlValues['unfair_supplier_info_registry_num']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->type)) {
            $xmlValues['unfair_supplier_info_type'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->type;
            $dBRecord->andWhere(['unfair_supplier_info_type' => $xmlValues['unfair_supplier_info_type']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->fullName)) {
            $xmlValues['unfair_supplier_info_full_name'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->fullName;
            $dBRecord->andWhere(['unfair_supplier_info_full_name' => $xmlValues['unfair_supplier_info_full_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->firmName)) {
            $xmlValues['unfair_supplier_info_firm_name'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->firmName;
            $dBRecord->andWhere(['unfair_supplier_info_firm_name' => $xmlValues['unfair_supplier_info_firm_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->taxPayerCode)) {
            $xmlValues['unfair_supplier_info_tax_payer_code'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->taxPayerCode;
            $dBRecord->andWhere(['unfair_supplier_info_tax_payer_code' => $xmlValues['unfair_supplier_info_tax_payer_code']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->kpp)) {
            $xmlValues['unfair_supplier_info_kpp'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->kpp;
            $dBRecord->andWhere(['unfair_supplier_info_kpp' => $xmlValues['unfair_supplier_info_kpp']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->foundersInfo->founderInfo->names)) {
            $xmlValues['unfair_supplier_info_founders_info_names'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->foundersInfo->founderInfo->names;
            $dBRecord->andWhere(['unfair_supplier_info_founders_info_names' => $xmlValues['unfair_supplier_info_founders_info_names']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->foundersInfo->founderInfo->taxPayerCode)) {
            $xmlValues['unfair_supplier_info_founders_info_tax_payer_code'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->foundersInfo->founderInfo->taxPayerCode;
            $dBRecord->andWhere(['unfair_supplier_info_founders_info_tax_payer_code' => $xmlValues['unfair_supplier_info_founders_info_tax_payer_code']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->place->kladr->kladrCode)) {
            $xmlValues['unfair_supplier_info_place_kladr_kladr_code'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->place->kladr->kladrCode;
            $dBRecord->andWhere(['unfair_supplier_info_place_kladr_kladr_code' => $xmlValues['unfair_supplier_info_place_kladr_kladr_code']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->place->kladr->fullName)) {
            $xmlValues['unfair_supplier_info_place_kladr_full_name'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->place->kladr->fullName;
            $dBRecord->andWhere(['unfair_supplier_info_place_kladr_full_name' => $xmlValues['unfair_supplier_info_place_kladr_full_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->place->country->countryCode)) {
            $xmlValues['unfair_supplier_info_place_country_country_code'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->place->country->countryCode;
            $dBRecord->andWhere(['unfair_supplier_info_place_country_country_code' => $xmlValues['unfair_supplier_info_place_country_country_code']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->place->country->fullName)) {
            $xmlValues['unfair_supplier_info_place_country_full_name'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->place->country->fullName;
            $dBRecord->andWhere(['unfair_supplier_info_place_country_full_name' => $xmlValues['unfair_supplier_info_place_country_full_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->place->deliveryPlace)) {
            $xmlValues['unfair_supplier_info_place_delivery_place'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->place->deliveryPlace;
            $dBRecord->andWhere(['unfair_supplier_info_place_delivery_place' => $xmlValues['unfair_supplier_info_place_delivery_place']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->place->noKladrForRegionSettlement->region)) {
            $xmlValues['place_no_kladr_for_region_settlement_region'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->place->noKladrForRegionSettlement->region;
            $dBRecord->andWhere(['place_no_kladr_for_region_settlement_region' => $xmlValues['place_no_kladr_for_region_settlement_region']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->place->noKladrForRegionSettlement->settlement)) {
            $xmlValues['place_no_kladr_for_region_settlement_settlement'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->place->noKladrForRegionSettlement->settlement;
            $dBRecord->andWhere(['place_no_kladr_for_region_settlement_settlement' => $xmlValues['place_no_kladr_for_region_settlement_settlement']]);
        }
        if (isset($xml->pprf615UnfairContractor->unfairSupplierInfo->eMail)) {
            $xmlValues['unfair_supplier_info_email'] = (string)$xml->pprf615UnfairContractor->unfairSupplierInfo->eMail;
            $dBRecord->andWhere(['unfair_supplier_info_email' => $xmlValues['unfair_supplier_info_email']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->purchaseNumber)) {
            $xmlValues['purchase_info_purchase_number'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->purchaseNumber;
            $dBRecord->andWhere(['purchase_info_purchase_number' => $xmlValues['purchase_info_purchase_number']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->purchaseObjectInfo)) {
            $xmlValues['purchase_info_purchase_object_info'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->purchaseObjectInfo;
            $dBRecord->andWhere(['purchase_info_purchase_object_info' => $xmlValues['purchase_info_purchase_object_info']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->placingWayName)) {
            $xmlValues['purchase_info_placing_way_name'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->placingWayName;
            $dBRecord->andWhere(['purchase_info_placing_way_name' => $xmlValues['purchase_info_placing_way_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->protocolDate)) {
            $xmlValues['purchase_info_protocol_date'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->protocolDate;
            $dBRecord->andWhere(['purchase_info_protocol_date' => $xmlValues['purchase_info_protocol_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->purchaseSubjectInfo->code)) {
            $xmlValues['purchase_info_purchase_subject_info_code'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->purchaseSubjectInfo->code;
            $dBRecord->andWhere(['purchase_info_purchase_subject_info_code' => $xmlValues['purchase_info_purchase_subject_info_code']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->purchaseSubjectInfo->name)) {
            $xmlValues['purchase_info_purchase_subject_info_name'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->purchaseSubjectInfo->name;
            $dBRecord->andWhere(['purchase_info_purchase_subject_info_name' => $xmlValues['purchase_info_purchase_subject_info_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->documentInfo->docName)) {
            $xmlValues['purchase_info_document_info_doc_name'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->documentInfo->docName;
            $dBRecord->andWhere(['purchase_info_document_info_doc_name' => $xmlValues['purchase_info_document_info_doc_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->documentInfo->docNumber)) {
            $xmlValues['purchase_info_document_info_doc_number'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->documentInfo->docNumber;
            $dBRecord->andWhere(['purchase_info_document_info_doc_number' => $xmlValues['purchase_info_document_info_doc_number']]);
        }
        if (isset($xml->pprf615UnfairContractor->purchaseInfo->documentInfo->docDate)) {
            $xmlValues['purchase_info_document_info_doc_date'] = (string)$xml->pprf615UnfairContractor->purchaseInfo->documentInfo->docDate;
            $dBRecord->andWhere(['purchase_info_document_info_doc_date' => $xmlValues['purchase_info_document_info_doc_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->regNum)) {
            $xmlValues['contract_info_reg_num'] = (string)$xml->pprf615UnfairContractor->contractInfo->regNum;
            $dBRecord->andWhere(['contract_info_reg_num' => $xmlValues['contract_info_reg_num']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->signDate)) {
            $xmlValues['contract_info_sign_date'] = (string)$xml->pprf615UnfairContractor->contractInfo->signDate;
            $dBRecord->andWhere(['contract_info_sign_date' => $xmlValues['contract_info_sign_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->purchaseInfo->purchaseSubjectInfo->code)) {
            $xmlValues['contract_info_purchase_subject_info_code'] = (string)$xml->pprf615UnfairContractor->contractInfo->purchaseInfo->purchaseSubjectInfo->code;
            $dBRecord->andWhere(['contract_info_purchase_subject_info_code' => $xmlValues['contract_info_purchase_subject_info_code']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->purchaseInfo->purchaseSubjectInfo->name)) {
            $xmlValues['contract_info_purchase_subject_info_name'] = (string)$xml->pprf615UnfairContractor->contractInfo->purchaseInfo->purchaseSubjectInfo->name;
            $dBRecord->andWhere(['contract_info_purchase_subject_info_name' => $xmlValues['contract_info_purchase_subject_info_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->price)) {
            $xmlValues['contract_info_price'] = (string)$xml->pprf615UnfairContractor->contractInfo->price;
            $dBRecord->andWhere(['contract_info_price' => $xmlValues['contract_info_price']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->perfomanceDate)) {
            $xmlValues['contract_info_perfomance_date'] = (string)$xml->pprf615UnfairContractor->contractInfo->perfomanceDate;
            $dBRecord->andWhere(['contract_info_perfomance_date' => $xmlValues['contract_info_perfomance_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->cancelInfo->baseDocInfo->docName)) {
            $xmlValues['contract_info_cancel_info_base_doc_info_doc_name'] = (string)$xml->pprf615UnfairContractor->contractInfo->cancelInfo->baseDocInfo->docName;
            $dBRecord->andWhere(['contract_info_cancel_info_base_doc_info_doc_name' => $xmlValues['contract_info_cancel_info_base_doc_info_doc_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->cancelInfo->baseDocInfo->docNumber)) {
            $xmlValues['contract_info_cancel_info_base_doc_info_doc_number'] = (string)$xml->pprf615UnfairContractor->contractInfo->cancelInfo->baseDocInfo->docNumber;
            $dBRecord->andWhere(['contract_info_cancel_info_base_doc_info_doc_number' => $xmlValues['contract_info_cancel_info_base_doc_info_doc_number']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->cancelInfo->baseDocInfo->docDate)) {
            $xmlValues['contract_info_cancel_info_base_doc_info_doc_date'] = (string)$xml->pprf615UnfairContractor->contractInfo->cancelInfo->baseDocInfo->docDate;
            $dBRecord->andWhere(['contract_info_cancel_info_base_doc_info_doc_date' => $xmlValues['contract_info_cancel_info_base_doc_info_doc_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->contractInfo->cancelInfo->cancelDate)) {
            $xmlValues['contract_info_cancel_info_cancel_date'] = (string)$xml->pprf615UnfairContractor->contractInfo->cancelInfo->cancelDate;
            $dBRecord->andWhere(['contract_info_cancel_info_cancel_date' => $xmlValues['contract_info_cancel_info_cancel_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->excludeInfo->basis)) {
            $xmlValues['exclude_info_basis'] = (string)$xml->pprf615UnfairContractor->excludeInfo->basis;
            $dBRecord->andWhere(['exclude_info_basis' => $xmlValues['exclude_info_basis']]);
        }
        if (isset($xml->pprf615UnfairContractor->excludeInfo->type->documentsInfo->documentInfo->docName)) {
            $xmlValues['exclude_info_documents_info_document_info_doc_name'] = (string)$xml->pprf615UnfairContractor->excludeInfo->type->documentsInfo->documentInfo->docName;
            $dBRecord->andWhere(['exclude_info_documents_info_document_info_doc_name' => $xmlValues['exclude_info_documents_info_document_info_doc_name']]);
        }
        if (isset($xml->pprf615UnfairContractor->excludeInfo->type->documentsInfo->documentInfo->docNumber)) {
            $xmlValues['exclude_info_documents_info_document_info_doc_number'] = (string)$xml->pprf615UnfairContractor->excludeInfo->type->documentsInfo->documentInfo->docNumber;
            $dBRecord->andWhere(['exclude_info_documents_info_document_info_doc_number' => $xmlValues['exclude_info_documents_info_document_info_doc_number']]);
        }
        if (isset($xml->pprf615UnfairContractor->excludeInfo->type->documentsInfo->documentInfo->docDate)) {
            $xmlValues['exclude_info_documents_info_document_info_doc_date'] = (string)$xml->pprf615UnfairContractor->excludeInfo->type->documentsInfo->documentInfo->docDate;
            $dBRecord->andWhere(['exclude_info_documents_info_document_info_doc_date' => $xmlValues['exclude_info_documents_info_document_info_doc_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->excludeInfo->date)) {
            $xmlValues['exclude_info_date'] = (string)$xml->pprf615UnfairContractor->excludeInfo->date;
            $dBRecord->andWhere(['exclude_info_date' => $xmlValues['exclude_info_date']]);
        }
        if (isset($xml->pprf615UnfairContractor->excludeInfo->type)) {
            $xmlValues['exclude_info_type'] = (string)$xml->pprf615UnfairContractor->excludeInfo->type;
            $dBRecord->andWhere(['exclude_info_type' => $xmlValues['exclude_info_type']]);
        }

        $result = $dBRecord->one();

        if (!$result) {
            $unfairSupplier = new self();
            $unfairSupplier->attributes = $xmlValues;

            if (!$unfairSupplier->save()) {
                print_r($unfairSupplier->getErrors());
                var_dump($xml);
                die;
            }
        }
    }

    public function fields()
    {
        return [
            'id',
            'table_name' => function () {
                return self::tableName();
            },
            'approve_date',
            'state',

        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\UnfairSupplier615Query the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UnfairSupplier615Query(get_called_class());
    }
}
