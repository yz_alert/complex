<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sved_mn".
 *
 * @property integer $id
 * @property string $kod_region
 * @property integer $msp_dokument_id
 * @property string $region_tip
 * @property string $region_naim
 *
 * @property MspGorod $mspGorod
 * @property MspNaselPunkt $mspNaselPunkt
 * @property MspRayon $mspRayon
 * @property MspDokument $mspDokument
 */
class MspSvedMn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sved_mn';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_region', 'region_tip', 'region_naim'], 'string'],
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [['msp_dokument_id'], 'unique'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kod_region' => Yii::t('app', 'Kod Regiona'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
            'region_tip' => Yii::t('app', 'Region Tip'),
            'region_naim' => Yii::t('app', 'Region Naim'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspGorod()
    {
        return $this->hasOne(MspGorod::className(), ['msp_sved_mn_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspNaselPunkt()
    {
        return $this->hasOne(MspNaselPunkt::className(), ['msp_sved_mn_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspRayon()
    {
        return $this->hasOne(MspRayon::className(), ['msp_sved_mn_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvedMnQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvedMnQuery(get_called_class());
    }
}
