<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * This is the model class for table "company_card".
 *
 * @property integer $id
 * @property string $tip_dokumenta
 * @property string $ogrn
 * @property string $naim_ul_sokr
 * @property string $naim_ul_poln
 * @property string $prizn_otsut_adres_ul
 * @property string $obr_data
 * @property string $data_ogrn
 * @property string $data_prekr_ul
 * @property string $aktivnost
 * @property string $inn
 * @property string $kpp
 * @property string $okpo
 * @property string $adres
 * @property string $sv_upr_org_array
 * @property string $sum_kap
 * @property string $nomin_stoim
 * @property string $derzh_inn
 * @property string $derzh_ogrn
 * @property string $derzh_naim_ul_poln
 * @property string $kod_okved
 * @property string $naim_okved
 * @property string $sv_okved_dop_array
 * @property string $sv_litsenziya
 * @property string $naim_no
 * @property string $data_post_uch
 * @property string $reg_nom_pf
 * @property string $data_reg_pf
 * @property string $reg_nom_fss
 * @property string $data_reg_fss
 * @property string $sv_filial
 * @property string $sv_predstav
 * @property string $rukovoditeli_array
 * @property string $fo2015
 * @property string $fo2014
 * @property string $fo2013
 * @property string $fo2012
 * @property string $fo2011
 * @property string $sv_uchredit_sum_cap
 * @property string $sv_uchredit_all_array
 * @property string $response
 * @property string $request_string
 * @property string $opisanie
 * @property string $fio
 * @property string $data_ogrnip
 * @property string $data_prekrashch
 * @property string $innfl
 * @property string $ogrnip
 * @property string $uch_naim_no
 * @property string $okato
 * @property string $oktmo
 * @property string $okogu
 * @property string $okopf
 * @property string $okfs
 * @property string $naim_vid_ip
 */
class CompanyCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'tip_dokumenta',
                    'ogrn',
                    'naim_ul_sokr',
                    'naim_ul_poln',
                    'prizn_otsut_adres_ul',
                    'obr_data',
                    'data_ogrn',
                    'data_prekr_ul',
                    'aktivnost',
                    'inn',
                    'kpp',
                    'okpo',
                    'adres',
                    'sv_upr_org_array',
                    'sum_kap',
                    'nomin_stoim',
                    'derzh_inn',
                    'derzh_ogrn',
                    'derzh_naim_ul_poln',
                    'kod_okved',
                    'naim_okved',
                    'sv_okved_dop_array',
                    'sv_litsenziya',
                    'naim_no',
                    'data_post_uch',
                    'reg_nom_pf',
                    'data_reg_pf',
                    'reg_nom_fss',
                    'data_reg_fss',
                    'sv_filial',
                    'sv_predstav',
                    'rukovoditeli_array',
                    'fo2015',
                    'fo2014',
                    'fo2013',
                    'fo2012',
                    'fo2011',
                    'sv_uchredit_sum_cap',
                    'sv_uchredit_all_array',
                    'response',
                    'request_string',
                    'opisanie',
                    'fio',
                    'data_ogrnip',
                    'data_prekrashch',
                    'innfl',
                    'ogrnip',
                    'uch_naim_no',
                    'okato',
                    'oktmo',
                    'okogu',
                    'okopf',
                    'okfs',
                    'naim_vid_ip',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tip_dokumenta' => Yii::t('app', 'Tip Dokumenta'),
            'ogrn' => Yii::t('app', 'Ogrn'),
            'naim_ul_sokr' => Yii::t('app', 'Naim Ul Sokr'),
            'naim_ul_poln' => Yii::t('app', 'Naim Ul Poln'),
            'prizn_otsut_adres_ul' => Yii::t('app', 'Prizn Otsut Adres Ul'),
            'obr_data' => Yii::t('app', 'Obr Data'),
            'data_ogrn' => Yii::t('app', 'Data Ogrn'),
            'data_prekr_ul' => Yii::t('app', 'Data Prekr Ul'),
            'aktivnost' => Yii::t('app', 'Aktivnost'),
            'inn' => Yii::t('app', 'Inn'),
            'kpp' => Yii::t('app', 'Kpp'),
            'okpo' => Yii::t('app', 'Okpo'),
            'adres' => Yii::t('app', 'Adres'),
            'sv_upr_org_array' => Yii::t('app', 'Sv Upr Org Array'),
            'sum_kap' => Yii::t('app', 'Sum Kap'),
            'nomin_stoim' => Yii::t('app', 'Nomin Stoim'),
            'derzh_inn' => Yii::t('app', 'Derzh Inn'),
            'derzh_ogrn' => Yii::t('app', 'Derzh Ogrn'),
            'derzh_naim_ul_poln' => Yii::t('app', 'Derzh Naim Ul Poln'),
            'kod_okved' => Yii::t('app', 'Kod Okved'),
            'naim_okved' => Yii::t('app', 'Naim Okved'),
            'sv_okved_dop_array' => Yii::t('app', 'Sv Okved Dop Array'),
            'sv_litsenziya' => Yii::t('app', 'Sv Litsenziya'),
            'naim_no' => Yii::t('app', 'Naim No'),
            'data_post_uch' => Yii::t('app', 'Data Post Uch'),
            'reg_nom_pf' => Yii::t('app', 'Reg Nom Pf'),
            'data_reg_pf' => Yii::t('app', 'Data Reg Pf'),
            'reg_nom_fss' => Yii::t('app', 'Reg Nom Fss'),
            'data_reg_fss' => Yii::t('app', 'Data Reg Fss'),
            'sv_filial' => Yii::t('app', 'Sv Filial'),
            'sv_predstav' => Yii::t('app', 'Sv Predstav'),
            'rukovoditeli_array' => Yii::t('app', 'Rukovoditeli Array'),
            'fo2015' => Yii::t('app', 'Fo2015'),
            'fo2014' => Yii::t('app', 'Fo2014'),
            'fo2013' => Yii::t('app', 'Fo2013'),
            'fo2012' => Yii::t('app', 'Fo2012'),
            'fo2011' => Yii::t('app', 'Fo2011'),
            'sv_uchredit_sum_cap' => Yii::t('app', 'Sv Uchredit Sum Cap'),
            'sv_uchredit_all_array' => Yii::t('app', 'Sv Uchredit All Array'),
            'response' => Yii::t('app', 'Response'),
            'request_string' => Yii::t('app', 'Request String'),
            'opisanie' => Yii::t('app', 'Opisanie'),
            'fio' => Yii::t('app', 'Fio'),
            'data_ogrnip' => Yii::t('app', 'Data Ogrnip'),
            'data_prekrashch' => Yii::t('app', 'Data Prekrashch'),
            'innfl' => Yii::t('app', 'Innfl'),
            'ogrnip' => Yii::t('app', 'Ogrnip'),
            'uch_naim_no' => Yii::t('app', 'Uch Naim No'),
            'okato' => Yii::t('app', 'Okato'),
            'oktmo' => Yii::t('app', 'Oktmo'),
            'okogu' => Yii::t('app', 'Okogu'),
            'okopf' => Yii::t('app', 'Okopf'),
            'okfs' => Yii::t('app', 'Okfs'),
            'naim_vid_ip' => Yii::t('app', 'Naim Vid Ip'),
        ];
    }

    public static function getCard($domain, $apiKey, $searchString)
    {
        $url = 'card';
        $client = new Client(['baseUrl' => $domain]);
        $response = $client->post($url, ['api_key' => $apiKey, 'id' => $searchString])->send();
        if ($response->isOk) {
            return $response;
        }
        return 0;
    }

    public static function cardWithZcb($searchString)
    {
        //Отправляем запрос на ЗЧБ
        $response = self::getCard(CompanySearch::DOMAIN, CompanySearch::API_KEY, $searchString);

        //Сохраняем ответ в таблицу CompanyCard
        $model = new CompanyCard();
        $model->request_string = $searchString;
        $model->response = json_encode($response->data);
        $model->save();

        //НаимВидИП
        if (ArrayHelper::keyExists('НаимВидИП', $response->data)) {
            $model->naim_vid_ip = (string)ArrayHelper::getValue($response->data, 'НаимВидИП');
            $model->tip_dokumenta = 'ip';
        }

        //ФИО
        if (ArrayHelper::keyExists('ФИО', $response->data)) {
            $model->fio = (string)ArrayHelper::getValue($response->data, 'ФИО');
        }

        //ДатаОГРНИП
        if (ArrayHelper::keyExists('ДатаОГРНИП', $response->data)) {
            $model->data_ogrnip = (string)ArrayHelper::getValue($response->data, 'ДатаОГРНИП');
        }

        //ДатаПрекращ
        if (ArrayHelper::keyExists('ДатаПрекращ', $response->data)) {
            $model->data_prekrashch = (string)ArrayHelper::getValue($response->data, 'ДатаПрекращ');
        }

        //ИННФЛ
        if (ArrayHelper::keyExists('ИННФЛ', $response->data)) {
            $model->innfl = (string)ArrayHelper::getValue($response->data, 'ИННФЛ');
        }

        //ОГРНИП
        if (ArrayHelper::keyExists('ОГРНИП', $response->data)) {
            $model->ogrnip = (string)ArrayHelper::getValue($response->data, 'ОГРНИП');
        }

        //УчНаимНО
        if (ArrayHelper::keyExists('УчНаимНО', $response->data)) {
            $model->uch_naim_no = (string)ArrayHelper::getValue($response->data, 'УчНаимНО');
        }

        //ОКАТО
        if (ArrayHelper::keyExists('ОКАТО', $response->data)) {
            $model->okato = (string)ArrayHelper::getValue($response->data, 'ОКАТО');
        }

        //ОКТМО
        if (ArrayHelper::keyExists('ОКТМО', $response->data)) {
            $model->oktmo = (string)ArrayHelper::getValue($response->data, 'ОКТМО');
        }

        //ОКОГУ
        if (ArrayHelper::keyExists('ОКОГУ', $response->data)) {
            $model->okogu = (string)ArrayHelper::getValue($response->data, 'ОКОГУ');
        }

        //ОКОПФ
        if (ArrayHelper::keyExists('ОКОПФ', $response->data)) {
            $model->okopf = (string)ArrayHelper::getValue($response->data, 'ОКОПФ');
        }

        //ОКФС
        if (ArrayHelper::keyExists('ОКФС', $response->data)) {
            $model->okfs = (string)ArrayHelper::getValue($response->data, 'ОКФС');
        }

        //ТипДокумента
        if (ArrayHelper::keyExists('ТипДокумента', $response->data)) {
            $model->tip_dokumenta = (string)ArrayHelper::getValue($response->data, 'ТипДокумента');
        }

        //ОГРН
        if (ArrayHelper::keyExists('ОГРН', $response->data)) {
            $model->ogrn = (string)ArrayHelper::getValue($response->data, 'ОГРН');
        }

        //НаимЮЛСокр
        if (ArrayHelper::keyExists('НаимЮЛСокр', $response->data)) {
            $model->naim_ul_sokr = (string)ArrayHelper::getValue($response->data, 'НаимЮЛСокр');
        }

        //НаимЮЛПолн
        if (ArrayHelper::keyExists('НаимЮЛПолн', $response->data)) {
            $model->naim_ul_poln = (string)ArrayHelper::getValue($response->data, 'НаимЮЛПолн');
        }

        //ПризнОтсутАдресЮЛ
        if (ArrayHelper::keyExists('ПризнОтсутАдресЮЛ', $response->data)) {
            $model->prizn_otsut_adres_ul = (string)ArrayHelper::getValue($response->data, 'ПризнОтсутАдресЮЛ');
        }

        //ОбрДата
        if (ArrayHelper::keyExists('ОбрДата', $response->data)) {
            $model->obr_data = (string)ArrayHelper::getValue($response->data, 'ОбрДата');
        }

        //ДатаОГРН
        if (ArrayHelper::keyExists('ДатаОГРН', $response->data)) {
            $model->data_ogrn = (string)ArrayHelper::getValue($response->data, 'ДатаОГРН');
        }

        //ДатаПрекрЮЛ
        if (ArrayHelper::keyExists('ДатаПрекрЮЛ', $response->data)) {
            $model->data_prekr_ul = (string)ArrayHelper::getValue($response->data, 'ДатаПрекрЮЛ');
        }

        //Активность
        if (ArrayHelper::keyExists('Активность', $response->data)) {
            $model->aktivnost = (string)ArrayHelper::getValue($response->data, 'Активность');
        }

        //ИНН
        if (ArrayHelper::keyExists('ИНН', $response->data)) {
            $model->inn = (string)ArrayHelper::getValue($response->data, 'ИНН');
        }

        //КПП
        if (ArrayHelper::keyExists('КПП', $response->data)) {
            $model->kpp = (string)ArrayHelper::getValue($response->data, 'КПП');
        }

        //ОКПО
        if (ArrayHelper::keyExists('ОКПО', $response->data)) {
            $model->okpo = (string)ArrayHelper::getValue($response->data, 'ОКПО');
        }

        //Адрес
        if (ArrayHelper::keyExists('Адрес', $response->data)) {
            $model->adres = (string)ArrayHelper::getValue($response->data, 'Адрес');
        }

        //-----===== СвУпрОрг =====-----
        if (ArrayHelper::keyExists('СвУпрОрг', $response->data)) {
            $model->sv_upr_org_array = Json::encode(ArrayHelper::getValue($response->data, 'СвУпрОрг'));
        }
        //------------------------------

        //СумКап
        if (ArrayHelper::keyExists('СумКап', $response->data)) {
            $model->sum_kap = (string)ArrayHelper::getValue($response->data, 'СумКап');
        }

        //НоминСтоим
        if (ArrayHelper::keyExists('НоминСтоим', $response->data)) {
            $model->nomin_stoim = (string)ArrayHelper::getValue($response->data, 'НоминСтоим');
        }

        //ДержИНН
        if (ArrayHelper::keyExists('ДержИНН', $response->data)) {
            $model->derzh_inn = (string)ArrayHelper::getValue($response->data, 'ДержИНН');
        }

        //ДержОГРН
        if (ArrayHelper::keyExists('ДержОГРН', $response->data)) {
            $model->derzh_ogrn = (string)ArrayHelper::getValue($response->data, 'ДержОГРН');
        }

        //ДержНаимЮЛПолн
        if (ArrayHelper::keyExists('ДержНаимЮЛПолн', $response->data)) {
            $model->derzh_naim_ul_poln = (string)ArrayHelper::getValue($response->data, 'ДержНаимЮЛПолн');
        }

        //КодОКВЭД
        if (ArrayHelper::keyExists('КодОКВЭД', $response->data)) {
            $model->kod_okved = (string)ArrayHelper::getValue($response->data, 'КодОКВЭД');
        }

        //НаимОКВЭД
        if (ArrayHelper::keyExists('НаимОКВЭД', $response->data)) {
            $model->naim_okved = (string)ArrayHelper::getValue($response->data, 'НаимОКВЭД');
        }

        //-----===== СвОКВЭДДоп =====-----
        if (ArrayHelper::keyExists('СвОКВЭДДоп', $response->data)) {
            $model->sv_okved_dop_array = Json::encode(ArrayHelper::getValue($response->data, 'СвОКВЭДДоп'));
        }
        //------------------------------

        //СвЛицензия
        if (ArrayHelper::keyExists('СвЛицензия', $response->data)) {
            $model->sv_litsenziya = (string)ArrayHelper::getValue($response->data, 'СвЛицензия');
        }

        //НаимНО
        if (ArrayHelper::keyExists('НаимНО', $response->data)) {
            $model->naim_no = (string)ArrayHelper::getValue($response->data, 'НаимНО');
        }

        //ДатаПостУч
        if (ArrayHelper::keyExists('ДатаПостУч', $response->data)) {
            $model->data_post_uch = (string)ArrayHelper::getValue($response->data, 'ДатаПостУч');
        }

        //РегНомПФ
        if (ArrayHelper::keyExists('РегНомПФ', $response->data)) {
            $model->reg_nom_pf = (string)ArrayHelper::getValue($response->data, 'РегНомПФ');
        }

        //ДатаРегПФ
        if (ArrayHelper::keyExists('ДатаРегПФ', $response->data)) {
            $model->data_reg_pf = (string)ArrayHelper::getValue($response->data, 'ДатаРегПФ');
        }

        //РегНомФСС
        if (ArrayHelper::keyExists('РегНомФСС', $response->data)) {
            $model->reg_nom_fss = (string)ArrayHelper::getValue($response->data, 'РегНомФСС');
        }

        //ДатаРегФСС
        if (ArrayHelper::keyExists('ДатаРегФСС', $response->data)) {
            $model->data_reg_fss = (string)ArrayHelper::getValue($response->data, 'ДатаРегФСС');
        }

        //СвФилиал
        if (ArrayHelper::keyExists('СвФилиал', $response->data)) {
            $model->sv_filial = (string)ArrayHelper::getValue($response->data, 'СвФилиал');
        }

        //СвПредстав
        if (ArrayHelper::keyExists('СвПредстав', $response->data)) {
            $model->sv_predstav = (string)ArrayHelper::getValue($response->data, 'СвПредстав');
        }

        //-----===== Руководители =====-----
        if (ArrayHelper::keyExists('Руководители', $response->data)) {
            $model->rukovoditeli_array = Json::encode(ArrayHelper::getValue($response->data, 'Руководители'));
        }
        //------------------------------

        //ФО2015
        if (ArrayHelper::keyExists('ФО2015', $response->data)) {
            $model->fo2015 = Json::encode(ArrayHelper::getValue($response->data, 'ФО2015'));
        }

        //ФО2014
        if (ArrayHelper::keyExists('ФО2014', $response->data)) {
            $model->fo2014 = Json::encode(ArrayHelper::getValue($response->data, 'ФО2014'));
        }

        //ФО2013
        if (ArrayHelper::keyExists('ФО2013', $response->data)) {
            $model->fo2013 = Json::encode(ArrayHelper::getValue($response->data, 'ФО2013'));
        }

        //ФО2012
        if (ArrayHelper::keyExists('ФО2012', $response->data)) {
            $model->fo2012 = Json::encode(ArrayHelper::getValue($response->data, 'ФО2012'));
        }

        //ФО2011
        if (ArrayHelper::keyExists('ФО2011', $response->data)) {
            $model->fo2011 = Json::encode(ArrayHelper::getValue($response->data, 'ФО2011'));
        }

        //-----===== СвУчредит =====-----
        if (ArrayHelper::keyExists('СвУчредит', $response->data)) {
            $model->sv_uchredit_all_array = Json::encode(ArrayHelper::getValue($response->data, 'СвУчредит'));
        }

        if (ArrayHelper::keyExists('СвУчредит.sumCap', $response->data)) {
            $model->sv_uchredit_sum_cap = (string)ArrayHelper::getValue($response->data, 'СвУчредит.sumCap');
        }

        //------------------------------

        //Описание
        if (ArrayHelper::keyExists('Описание', $response->data)) {
            $model->opisanie = (string)ArrayHelper::getValue($response->data, 'Описание');
        }

        $model->save();

        if (!$model->save()) {
            echo PHP_EOL;
            print_r($model->getErrors());
        }

        if($model->tip_dokumenta = 'ip'){
            return CompanyCard::findOne(['ogrnip' => $searchString]);
        }
        return CompanyCard::findOne(['ogrn' => $searchString]);
    }

    public function fields()
    {
        if($this->tip_dokumenta == 'ip'){
            return [
                'naim_vid_ip',
                'tip_dokumenta',
                'fio',
                'data_ogrnip',
                'data_prekrashch',
                'innfl',
                'ogrnip',
                'okpo',
                'kod_okved',
                'naim_okved',
                'sv_okved_dop_array' => function ($model) {
                    return Json::decode($model->sv_okved_dop_array);
                },
                'sv_litsenziya',
                'uch_naim_no',
                'data_post_uch',
                'reg_nom_pf',
                'data_reg_pf',
                'reg_nom_fss',
                'data_reg_fss',
                'okato',
                'oktmo',
                'okogu',
                'okopf',
                'okfs',
                'opisanie',
                'response' => function ($model) {
                    return Json::decode($model->response);
                },
            ];
        }
        else{
            return [
                //'id',
                'tip_dokumenta',
                'ogrn',
                'naim_ul_sokr',
                'naim_ul_poln',
                'prizn_otsut_adres_ul',
                'obr_data',
                'data_ogrn',
                'data_prekr_ul',
                'aktivnost',
                'inn',
                'kpp',
                'okpo',
                'adres',
                'sv_upr_org_array' => function ($model) {
                    return Json::decode($model->sv_upr_org_array);
                },
                'sum_kap',
                'nomin_stoim',
                'derzh_inn',
                'derzh_ogrn',
                'derzh_naim_ul_poln',
                'kod_okved',
                'naim_okved',
                'sv_okved_dop_array' => function ($model) {
                    return Json::decode($model->sv_okved_dop_array);
                },
                'sv_litsenziya',
                'naim_no',
                'data_post_uch',
                'reg_nom_pf',
                'data_reg_pf',
                'reg_nom_fss',
                'data_reg_fss',
                'sv_filial',
                'sv_predstav',
                'rukovoditeli_array' => function ($model) {
                    return Json::decode($model->rukovoditeli_array);
                },
                'fo2015',
                'fo2014',
                'fo2013',
                'fo2012',
                'fo2011',
                'sv_uchredit_sum_cap',
                'sv_uchredit_all_array' => function ($model) {
                    return Json::decode($model->sv_uchredit_all_array);
                },
                'opisanie',
                'response' => function ($model) {
                    return Json::decode($model->response);
                },
            ];
        }

    }

    /**
     * @inheritdoc
     * @return \common\models\query\CompanyCardQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CompanyCardQuery(get_called_class());
    }
}
