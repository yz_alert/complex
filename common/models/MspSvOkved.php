<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sv_okved".
 *
 * @property integer $id
 * @property integer $msp_dokument_id
 *
 * @property MspDokument $mspDokument
 * @property MspSvOkvedDop[] $mspSvOkvedDops
 * @property MspSvOkvedOsn $mspSvOkvedOsn
 */
class MspSvOkved extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sv_okved';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [['msp_dokument_id'], 'unique'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvOkvedDops()
    {
        return $this->hasMany(MspSvOkvedDop::className(), ['msp_sv_okved_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvOkvedOsn()
    {
        return $this->hasOne(MspSvOkvedOsn::className(), ['msp_sv_okved_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvOkvedQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvOkvedQuery(get_called_class());
    }
}
