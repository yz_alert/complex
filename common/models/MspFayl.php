<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_fayl".
 *
 * @property integer $id
 * @property string $id_fayl
 * @property string $vers_form
 * @property string $tip_inf
 * @property string $vers_prog
 * @property string $kol_dok
 *
 * @property MspDokument[] $mspDokuments
 * @property MspIdOtpr $mspIdOtpr
 */
class MspFayl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_fayl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_fayl', 'vers_form', 'tip_inf', 'vers_prog', 'kol_dok'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_fayl' => Yii::t('app', 'Id Fayl'),
            'vers_form' => Yii::t('app', 'Vers Form'),
            'tip_inf' => Yii::t('app', 'Tip Inf'),
            'vers_prog' => Yii::t('app', 'Vers Prog'),
            'kol_dok' => Yii::t('app', 'Kol Dok'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokuments()
    {
        return $this->hasMany(MspDokument::className(), ['msp_fayl_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspIdOtpr()
    {
        return $this->hasOne(MspIdOtpr::className(), ['msp_fayl_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspFaylQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspFaylQuery(get_called_class());
    }
}
