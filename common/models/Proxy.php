<?php

namespace common\models;

use Yii;
use common\models\helpers\MyHelper;

/**
 * This is the model class for table "proxy".
 *
 * @property int $id
 * @property string $ip
 * @property string $port
 * @property string $login
 * @property string $password
 * @property string $created_at
 *
 * @property ProxyChecker[] $proxyCheckers
 */
class Proxy extends \yii\db\ActiveRecord
{

    public static function getDb()
    {
        return Yii::$app->db_kaleka;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ip', 'port'], 'required'],
            [['ip', 'port', 'login', 'password'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ip' => Yii::t('app', 'Ip'),
            'port' => Yii::t('app', 'Port'),
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    //public function getProxyCheckers()
    //{
    //    return $this->hasMany(ProxyChecker::className(), ['proxy_id' => 'id']);
    //}

    public static function getRandomProxy()
    {
        $proxyList = Proxy::find()->asArray()->all();

        $rand_keys = array_rand($proxyList);
        $proxy = $proxyList[$rand_keys];

        $proxy = $proxy['ip'] . ':' . $proxy['port'] . ':' . $proxy['login'] . ':' . $proxy['password'];

        MyHelper::echobr2("Установлена новая прокси", $proxy);
        return $proxy;
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\ProxyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ProxyQuery(get_called_class());
    }
}