<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_ip_vkl_msp".
 *
 * @property integer $id
 * @property integer $msp_dokument_id
 * @property string $inn
 * @property string $fio_ip_familiya
 * @property string $fio_ip_imya
 * @property string $fio_ip_otchestvo
 *
 * @property MspDokument $mspDokument
 */
class MspIpVklMsp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_ip_vkl_msp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [['inn', 'fio_ip_familiya', 'fio_ip_imya', 'fio_ip_otchestvo'], 'string'],
            [['msp_dokument_id'], 'unique'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
            'inn' => Yii::t('app', 'Inn'),
            'fio_ip_familiya' => Yii::t('app', 'Fio Ip Familiya'),
            'fio_ip_imya' => Yii::t('app', 'Fio Ip Imya'),
            'fio_ip_otchestvo' => Yii::t('app', 'Fio Ip Otchestvo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspIpVklMspQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspIpVklMspQuery(get_called_class());
    }
}
