<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company_search".
 *
 * @property integer $id
 * @property string $request_string
 * @property string $response
 * @property string $created_at
 * @property boolean $is_success
 * @property integer $total_records
 *
 * @property CompanySearchResult[] $companySearchResults
 */
class CompanySearch extends \yii\db\ActiveRecord
{
    const API_KEY = 'gOeXqVN3KKvK_SZbyn3UhVz41LV4mltZ';
    const DOMAIN = 'https://zachestnyibiznesapi.ru/v2/data';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_search';
    }

    public function behaviors()
    {
        return [

            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_string', 'response'], 'string'],
            [['created_at'], 'safe'],
            [['is_success'], 'boolean'],
            [['total_records'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'request_string' => Yii::t('app', 'Строка поиска'),
            'response' => Yii::t('app', 'Ответ с сервера в формате JSON'),
            'created_at' => Yii::t('app', 'Дата и время создания'),
            'is_success' => Yii::t('app', 'Статус ответа - Ok 200'),
            'total_records' => Yii::t('app', 'Всего записей'),
        ];
    }

    public static function getSearch($domain, $apiKey, $searchString)
    {
        $url = 'search';
        $client = new Client(['baseUrl' => $domain]);
        $response = $client->post($url, ['api_key' => $apiKey, 'string' => $searchString])->send();
        if ($response->isOk) {
            return $response;
        }
        return 0;
    }

    public static function searchWithZcb($searchString)
    {
        //Отправляем запрос на ЗЧБ
        $response = self::getSearch(self::DOMAIN, self::API_KEY, $searchString);

        //Сохраняем ответ в таблицу CompanySearch
        $model = new CompanySearch();
        $model->request_string = $searchString;
        $model->total_records = ArrayHelper::getValue($response->data, 'total');
        $model->response = json_encode($response->data);
        $model->is_success = true;
        $model->save();

        if (!$model->save()) {
            echo PHP_EOL . $model->getErrors();
        }

        $response = Json::decode($model->response);

        return CompanySearch::createCompanySearchResult($model->id, $searchString, $response);
    }

    public static function createCompanySearchResult($requestId, $searchString, $response)
    {
        //Если массив корректный
        if (ArrayHelper::keyExists('total', $response) &&
            ArrayHelper::keyExists('docs', $response)
        ) {
            //Если в результате у нас только 1 компания
            if (ArrayHelper::getValue($response, 'total') === 1) {
                $companySearchResult = ArrayHelper::getValue($response, 'docs.0');
                $result = new CompanySearchResult();
                $result->request_id = $requestId;
                $result->ogrn = ArrayHelper::getValue($companySearchResult, 'id');
                $result->inn = ArrayHelper::getValue($companySearchResult, 'ИНН');
                if (ArrayHelper::getValue($companySearchResult, 'ИННФЛ') !== null) {
                    $result->inn = ArrayHelper::getValue($companySearchResult, 'ИННФЛ');
                }
                $result->save();
                if (!$result->save()) {
                    echo PHP_EOL . $result->getErrors();
                }
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => CompanySearchResult::findOne(['inn' => $result->inn]),
                ];

            } //Если в результате несколько компаний
            else {
                $model = CompanySearch::findOne(['request_string' => $searchString]);
                foreach (ArrayHelper::getValue(Json::decode($model->response), 'docs') as $companySearchResult) {
                    //ИНН
                    if (ArrayHelper::keyExists('ИНН', $companySearchResult)) {
                        if (ArrayHelper::getValue($companySearchResult, 'ИНН') == $searchString) {
                            $result = new CompanySearchResult();
                            $result->request_id = $requestId;
                            $result->ogrn = ArrayHelper::getValue($companySearchResult, 'id');
                            $result->inn = ArrayHelper::getValue($companySearchResult, 'ИНН');
                            $result->save();
                            if (!$result->save()) {
                                echo PHP_EOL . $result->getErrors();
                            }
                            return [
                                'reply' => ['3' => 'Присутствует'],
                                'data' => CompanySearchResult::findOne(['inn' => $result->inn]),
                            ];
                            //return CompanySearchResult::findOne(['inn' => $result->inn]);
                        }
                    }
                    //ИННФЛ
                    if (ArrayHelper::keyExists('ИННФЛ', $companySearchResult)) {
                        if (ArrayHelper::getValue($companySearchResult, 'ИННФЛ') == $searchString &&
                            ArrayHelper::getValue($companySearchResult, 'Активность') !== 'Деятельность прекращена'
                        ) {
                            $result = new CompanySearchResult();
                            $result->request_id = $requestId;
                            $result->ogrn = ArrayHelper::getValue($companySearchResult, 'id');
                            $result->inn = ArrayHelper::getValue($companySearchResult, 'ИННФЛ');
                            $result->save();
                            if (!$result->save()) {
                                echo PHP_EOL . $result->getErrors();
                            }
                            return [
                                'reply' => ['3' => 'Присутствует'],
                                'data' => CompanySearchResult::findOne(['inn' => $result->inn]),
                            ];
                            //return CompanySearchResult::findOne(['inn' => $result->inn]);
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getCompanySearchResults()
    {
        return $this->hasMany(CompanySearchResult::className(), ['request_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\CompanySearchQuery the active query used by this AR class.
     */
    public
    static function find()
    {
        return new \common\models\query\CompanySearchQuery(get_called_class());
    }
}
