<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mass_leaders".
 *
 * @property integer $id
 * @property string $inn
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property integer $quantity
 */
class MassLeaders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mass_leaders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn', 'surname', 'name', 'patronymic'], 'string'],
            [['surname', 'name', 'patronymic', 'quantity'], 'required'],
            [['quantity'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'inn' => Yii::t('app', 'Taxpayer identification number'),
            'surname' => Yii::t('app', 'Surname'),
            'name' => Yii::t('app', 'Name'),
            'patronymic' => Yii::t('app', 'Patronymic'),
            'quantity' => Yii::t('app', 'The number of organizations where the man is the head'),
        ];
    }

    public function fields()
    {
        return [
            //'id',
            'inn',
            'surname',
            'name',
            'patronymic',
            'quantity',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MassLeadersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MassLeadersQuery(get_called_class());
    }
}
