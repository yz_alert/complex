<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "terrorists_fl_rus".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $status
 */
class TerroristsFlRus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terrorists_fl_rus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name'], 'required'],
            [['full_name', 'place_of_birth', 'status'], 'string'],
            [['date_of_birth'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'full_name' => Yii::t('app', 'Full Name'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'place_of_birth' => Yii::t('app', 'Place Of Birth'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function fields()
    {
        return [
            //'id',
            'full_name',
            'date_of_birth',
            'place_of_birth',
            'status',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\TerroristsFlRusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TerroristsFlRusQuery(get_called_class());
    }
}
