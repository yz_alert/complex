<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\MspSvOkvedDop]].
 *
 * @see \common\models\MspSvOkvedDop
 */
class MspSvOkvedDopQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\MspSvOkvedDop[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\MspSvOkvedDop|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
