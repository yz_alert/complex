<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\UnfairSupplier223]].
 *
 * @see \common\models\UnfairSupplier223
 */
class UnfairSupplier223Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\UnfairSupplier223[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\UnfairSupplier223|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
