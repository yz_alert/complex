<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\UnfairSupplier223ContractItem]].
 *
 * @see \common\models\UnfairSupplier223ContractItem
 */
class UnfairSupplier223ContractItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\UnfairSupplier223ContractItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\UnfairSupplier223ContractItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
