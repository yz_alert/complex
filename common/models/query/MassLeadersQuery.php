<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\MassLeaders]].
 *
 * @see \common\models\MassLeaders
 */
class MassLeadersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\MassLeaders[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\MassLeaders|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
