<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\MspDokument]].
 *
 * @see \common\models\MspDokument
 */
class MspDokumentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\MspDokument[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\MspDokument|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
