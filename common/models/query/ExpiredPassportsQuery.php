<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\ExpiredPassports]].
 *
 * @see \common\models\ExpiredPassports
 */
class ExpiredPassportsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ExpiredPassports[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ExpiredPassports|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
