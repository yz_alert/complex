<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\CourtArbitrationCase]].
 *
 * @see \common\models\CourtArbitrationCase
 */
class CourtArbitrationCaseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\CourtArbitrationCase[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\CourtArbitrationCase|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
