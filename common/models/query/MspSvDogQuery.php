<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\MspSvDog]].
 *
 * @see \common\models\MspSvDog
 */
class MspSvDogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\MspSvDog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\MspSvDog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
