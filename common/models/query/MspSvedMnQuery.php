<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\MspSvedMn]].
 *
 * @see \common\models\MspSvedMn
 */
class MspSvedMnQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\MspSvedMn[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\MspSvedMn|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
