<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sv_prog_part".
 *
 * @property integer $id
 * @property string $naim_ul_pp
 * @property string $inn_ul_pp
 * @property string $nom_dog
 * @property string $data_dog
 * @property integer $msp_dokument_id
 *
 * @property MspDokument $mspDokument
 */
class MspSvProgPart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sv_prog_part';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['naim_ul_pp', 'inn_ul_pp', 'nom_dog'], 'string'],
            [['data_dog'], 'safe'],
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naim_ul_pp' => Yii::t('app', 'Naim Ul Pp'),
            'inn_ul_pp' => Yii::t('app', 'Inn Ul Pp'),
            'nom_dog' => Yii::t('app', 'Nom Dog'),
            'data_dog' => Yii::t('app', 'Data Dog'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvProgPartQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvProgPartQuery(get_called_class());
    }
}
