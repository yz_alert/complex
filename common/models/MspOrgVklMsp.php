<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_org_vkl_msp".
 *
 * @property integer $id
 * @property integer $msp_dokument_id
 * @property string $naim_org
 * @property string $naim_org_sokr
 * @property string $innul
 *
 * @property MspDokument $mspDokument
 */
class MspOrgVklMsp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_org_vkl_msp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [['naim_org', 'naim_org_sokr', 'innul'], 'string'],
            [['msp_dokument_id'], 'unique'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
            'naim_org' => Yii::t('app', 'Naim Org'),
            'naim_org_sokr' => Yii::t('app', 'Naim Org Sokr'),
            'innul' => Yii::t('app', 'Innul'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspOrgVklMspQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspOrgVklMspQuery(get_called_class());
    }
}
