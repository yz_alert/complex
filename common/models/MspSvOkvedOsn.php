<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sv_okved_osn".
 *
 * @property integer $id
 * @property string $kod_okved
 * @property string $naim_okved
 * @property string $vers_okved
 * @property integer $msp_sv_okved_id
 *
 * @property MspSvOkved $mspSvOkved
 */
class MspSvOkvedOsn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sv_okved_osn';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_okved', 'naim_okved', 'vers_okved'], 'string'],
            [['msp_sv_okved_id'], 'required'],
            [['msp_sv_okved_id'], 'integer'],
            [['msp_sv_okved_id'], 'unique'],
            [
                ['msp_sv_okved_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspSvOkved::className(),
                'targetAttribute' => ['msp_sv_okved_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kod_okved' => Yii::t('app', 'Kod Okved'),
            'naim_okved' => Yii::t('app', 'Naim Okved'),
            'vers_okved' => Yii::t('app', 'Vers Okved'),
            'msp_sv_okved_id' => Yii::t('app', 'Msp Sv Okved ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspSvOkved()
    {
        return $this->hasOne(MspSvOkved::className(), ['id' => 'msp_sv_okved_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvOkvedOsnQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvOkvedOsnQuery(get_called_class());
    }
}
