<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company_search_result".
 *
 * @property integer $id
 * @property integer $request_id
 * @property integer $ogrn
 * @property string $inn
 * @property boolean $is_new
 *
 * @property CompanySearch $request
 */
class CompanySearchResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_search_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id'], 'required'],
            [['request_id'], 'integer'],
            [['ogrn', 'inn'], 'string'],
            [['is_new'], 'boolean'],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanySearch::className(), 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'request_id' => Yii::t('app', 'Request ID'),
            'ogrn' => Yii::t('app', 'Zcb ID'),
            'inn' => Yii::t('app', 'Inn'),
            'is_new' => Yii::t('app', 'Is New'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(CompanySearch::className(), ['id' => 'request_id']);
    }

    public function fields()
    {
        return [
            //'id',
            'inn',
            'ogrn',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\CompanySearchResultQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CompanySearchResultQuery(get_called_class());
    }
}
