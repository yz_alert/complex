<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_id_otpr".
 *
 * @property integer $id
 * @property integer $msp_fayl_id
 * @property string $dolzh_otv
 * @property string $tlf
 * @property string $email
 * @property string $fio_otv_familiya
 * @property string $fio_otv_imya
 * @property string $fio_otv_otchestvo
 *
 * @property MspFayl $mspFayl
 */
class MspIdOtpr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_id_otpr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msp_fayl_id'], 'required'],
            [['msp_fayl_id'], 'integer'],
            [['dolzh_otv', 'tlf', 'email', 'fio_otv_familiya', 'fio_otv_imya', 'fio_otv_otchestvo'], 'string'],
            [['msp_fayl_id'], 'unique'],
            [
                ['msp_fayl_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspFayl::className(),
                'targetAttribute' => ['msp_fayl_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'msp_fayl_id' => Yii::t('app', 'Msp Fayl ID'),
            'dolzh_otv' => Yii::t('app', 'Dolzh Otv'),
            'tlf' => Yii::t('app', 'Tlf'),
            'email' => Yii::t('app', 'Email'),
            'fio_otv_familiya' => Yii::t('app', 'Fio Otv Familiya'),
            'fio_otv_imya' => Yii::t('app', 'Fio Otv Imya'),
            'fio_otv_otchestvo' => Yii::t('app', 'Fio Otv Otchestvo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspFayl()
    {
        return $this->hasOne(MspFayl::className(), ['id' => 'msp_fayl_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspIdOtprQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspIdOtprQuery(get_called_class());
    }
}
