<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sv_prod".
 *
 * @property integer $id
 * @property string $kod_prod
 * @property string $naim_prod
 * @property string $pr_otn_prod
 * @property integer $msp_dokument_id
 *
 * @property MspDokument $mspDokument
 */
class MspSvProd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sv_prod';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kod_prod', 'naim_prod', 'pr_otn_prod'], 'string'],
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kod_prod' => Yii::t('app', 'Kod Prod'),
            'naim_prod' => Yii::t('app', 'Naim Prod'),
            'pr_otn_prod' => Yii::t('app', 'Pr Otn Prod'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvProdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvProdQuery(get_called_class());
    }
}
