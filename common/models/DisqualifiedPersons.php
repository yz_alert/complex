<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "disqualified_persons".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $ogrn
 * @property string $inn
 * @property string $kpp
 * @property string $address
 */
class DisqualifiedPersons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disqualified_persons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'ogrn', 'inn', 'kpp', 'address'], 'required'],
            [['full_name', 'ogrn', 'inn', 'kpp', 'address'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'full_name' => Yii::t('app', 'Full name of organization'),
            'ogrn' => Yii::t('app', 'PSRN organization'),
            'inn' => Yii::t('app', 'Taxpayer identification number'),
            'kpp' => Yii::t('app', 'The reason of tax registration'),
            'address' => Yii::t('app', 'Postal address'),
        ];
    }

    public function fields()
    {
        return [
            //'id',
            'full_name',
            'ogrn',
            'inn',
            'kpp',
            'address',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\DisqualifiedPersonsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\DisqualifiedPersonsQuery(get_called_class());
    }
}
