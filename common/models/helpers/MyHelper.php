<?php

namespace common\models\helpers;

use Yii;

class MyHelper
{
    public static function clearString($string)
    {
        $string = trim($string);
        //Удаление системных символов из строки
        $string = preg_replace('/\r\n|\r|\n/u', ' ', $string);
        //Из HTML в строку
        $string = html_entity_decode($string);
        //Удаление пробелов в середине строки
        $string = preg_replace("/ {2,}/", " ", $string);
        //Удаление символов (Tab и т.д.)
        $string = preg_replace("/\s{2,}/", ' ', $string);

        return $string;
    }

    public static function dump($variable)
    {
        echo '<pre style="background: lightgray; border: 1px solid black; padding: 2px">';
        print_r($variable);
        echo '</pre>';
    }

    public static function echobr($variable)
    {
        echo PHP_EOL;
        print_r($variable);
    }

    public static function echobr2($key, $variable)
    {
        echo PHP_EOL;
        print_r($key . ': ' . $variable);
    }

    public static function dump_attributes($variable)
    {
        //echo '<pre>';
        echo '<pre style="background: lightgray; border: 1px solid black; padding: 2px">';
        print_r($variable->attributes);
        echo '</pre>';
    }

    public static function dumpp_attributes($variable)
    {
        //echo '<pre>';
        echo '<pre style="background: lightgray; border: 1px solid black; padding: 2px">';
        print_r($variable->attributes);
        echo '</pre>';
        die;
    }

    public static function dumpp($variable)
    {
        echo '<pre style="background: lightgray; border: 1px solid black; padding: 2px">';
        print_r($variable);
        echo '</pre>';
        die;
    }
}
