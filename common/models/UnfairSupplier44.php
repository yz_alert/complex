<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "unfair_supplier_44".
 *
 * @property integer $id
 * @property string $registry_num
 * @property string $publish_date
 * @property string $approve_date
 * @property string $state
 * @property string $publish_org_reg_num
 * @property string $publish_org_cons_registry_num
 * @property string $publish_org_full_name
 * @property string $create_reason
 * @property string $approve_reason
 * @property string $customer_reg_num
 * @property string $customer_cons_registry_num
 * @property string $customer_full_name
 * @property string $customer_short_name
 * @property string $customer_post_address
 * @property string $customer_fact_address
 * @property string $customer_inn
 * @property string $customer_kpp
 *
 *
 * @property string $unfair_supplier_full_name
 * @property string $unfair_supplier_type
 * @property string $unfair_supplier_firm_name
 * @property string $unfair_supplier_inn
 * @property string $unfair_supplier_kpp
 * @property string $unfair_supplier_place_kladr_kladr_type
 * @property string $unfair_supplier_place_kladr_kladr_code
 * @property string $unfair_supplier_place_kladr_full_name
 * @property string $unfair_supplier_place_kladr_subject_rf
 * @property string $unfair_supplier_place_kladr_area
 * @property string $unfair_supplier_place_kladr_city
 * @property string $unfair_supplier_place_kladr_street
 * @property string $unfair_supplier_place_kladr_building
 * @property string $unfair_supplier_place_kladr_office
 * @property string $unfair_supplier_place_country_country_code
 * @property string $unfair_supplier_place_country_country_full_name
 * @property string $unfair_supplier_place_zip
 * @property string $unfair_supplier_place_place
 * @property string $unfair_supplier_place_email
 * @property string $unfair_supplier_founders_names_inn
 * @property string $unfair_supplier_founders_inn
 * @property string $purchase_purchase_number
 * @property string $purchase_purchase_object_info
 * @property string $purchase_placing_way_name
 * @property string $purchase_protocol_date
 * @property string $purchase_lot_number
 * @property string $purchase_document_name
 * @property string $purchase_document_date
 * @property string $purchase_document_number
 * @property string $not_oos_purchase
 * @property string $contract_reg_num
 * @property string $contract_product_info
 * @property string $contract_okpd_code
 * @property string $contract_okpd_name
 * @property string $contract_currency_code
 * @property string $contract_currency_name
 * @property string $contract_price
 * @property string $contract_cancel_sign_date
 * @property string $contract_cancel_performance_date
 * @property string $contract_cancel_base_name
 * @property string $contract_cancel_base_number
 * @property string $contract_cancel_base_date
 * @property string $contract_cancel_cancel_date
 *
 * @property UnfairSupplierFounder[] $unfairSupplierFounders
 */
class UnfairSupplier44 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unfair_supplier_44';
    }

    static function getDb()
    {
        return Yii::$app->db_kaleka;
        //return Yii::$app->kaleka;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'registry_num',
                    'state',
                    'publish_org_reg_num',
                    'publish_org_cons_registry_num',
                    'publish_org_full_name',
                    'create_reason',
                    'approve_reason',
                    'customer_reg_num',
                    'customer_cons_registry_num',
                    'customer_full_name',
                    'customer_short_name',
                    'customer_post_address',
                    'customer_fact_address',
                    'customer_inn',
                    'customer_kpp',
                    'unfair_supplier_full_name',
                    'unfair_supplier_type',
                    'unfair_supplier_firm_name',
                    'unfair_supplier_inn',
                    'unfair_supplier_kpp',
                    'unfair_supplier_place_kladr_kladr_type',
                    'unfair_supplier_place_kladr_kladr_code',
                    'unfair_supplier_place_kladr_full_name',
                    'unfair_supplier_place_kladr_subject_rf',
                    'unfair_supplier_place_kladr_area',
                    'unfair_supplier_place_kladr_city',
                    'unfair_supplier_place_kladr_street',
                    'unfair_supplier_place_kladr_building',
                    'unfair_supplier_place_kladr_office',
                    'unfair_supplier_place_country_country_code',
                    'unfair_supplier_place_country_country_full_name',
                    'unfair_supplier_place_zip',
                    'unfair_supplier_place_place',
                    'unfair_supplier_place_email',
                    'unfair_supplier_founders_names_inn',
                    'unfair_supplier_founders_inn',
                    'purchase_purchase_number',
                    'purchase_purchase_object_info',
                    'purchase_placing_way_name',
                    'purchase_protocol_date',
                    'purchase_lot_number',
                    'purchase_document_name',
                    'purchase_document_date',
                    'purchase_document_number',
                    'not_oos_purchase',
                    'contract_reg_num',
                    'contract_product_info',
                    'contract_okpd_code',
                    'contract_okpd_name',
                    'contract_currency_code',
                    'contract_currency_name',
                    'contract_price',
                    'contract_cancel_sign_date',
                    'contract_cancel_performance_date',
                    'contract_cancel_base_name',
                    'contract_cancel_base_number',
                    'contract_cancel_base_date',
                    'contract_cancel_cancel_date',
                ],
                'string',
            ],
            [['approve_date', 'publish_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'registry_num' => Yii::t('app', 'Registry Num'),
            'publish_date' => Yii::t('app', 'Publish Date'),
            'approve_date' => Yii::t('app', 'Approve Date'),
            'state' => Yii::t('app', 'State'),
            'publish_org_reg_num' => Yii::t('app', 'Publish Org Reg Num'),
            'publish_org_cons_registry_num' => Yii::t('app', 'Publish Org Cons Registry Num'),
            'publish_org_full_name' => Yii::t('app', 'Publish Org Full Name'),
            'create_reason' => Yii::t('app', 'Create Reason'),
            'approve_reason' => Yii::t('app', 'Approve Reason'),
            'customer_reg_num' => Yii::t('app', 'Customer Reg Num'),
            'customer_cons_registry_num' => Yii::t('app', 'Customer Cons Registry Num'),
            'customer_full_name' => Yii::t('app', 'Customer Full Name'),
            'customer_short_name' => Yii::t('app', 'Customer Short Name'),
            'customer_post_address' => Yii::t('app', 'Customer Post Address'),
            'customer_fact_address' => Yii::t('app', 'Customer Fact Address'),
            'customer_inn' => Yii::t('app', 'Customer Inn'),
            'customer_kpp' => Yii::t('app', 'Customer Kpp'),
            'unfair_supplier_full_name' => Yii::t('app', 'Unfair Supplier Full Name'),
            'unfair_supplier_type' => Yii::t('app', 'Unfair Supplier Type'),
            'unfair_supplier_firm_name' => Yii::t('app', 'Unfair Supplier Firm Name'),
            'unfair_supplier_inn' => Yii::t('app', 'Unfair Supplier Inn'),
            'unfair_supplier_kpp' => Yii::t('app', 'Unfair Supplier Kpp'),
            'unfair_supplier_place_kladr_kladr_type' => Yii::t('app', 'Unfair Supplier Place Kladr Kladr Type'),
            'unfair_supplier_place_kladr_kladr_code' => Yii::t('app', 'Unfair Supplier Place Kladr Kladr Code'),
            'unfair_supplier_place_kladr_full_name' => Yii::t('app', 'Unfair Supplier Place Kladr Full Name'),
            'unfair_supplier_place_kladr_subject_rf' => Yii::t('app', 'Unfair Supplier Place Kladr Subject Rf'),
            'unfair_supplier_place_kladr_area' => Yii::t('app', 'Unfair Supplier Place Kladr Area'),
            'unfair_supplier_place_kladr_city' => Yii::t('app', 'Unfair Supplier Place Kladr City'),
            'unfair_supplier_place_kladr_street' => Yii::t('app', 'Unfair Supplier Place Kladr Street'),
            'unfair_supplier_place_kladr_building' => Yii::t('app', 'Unfair Supplier Place Kladr Building'),
            'unfair_supplier_place_kladr_office' => Yii::t('app', 'Unfair Supplier Place Kladr Office'),
            'unfair_supplier_place_country_country_code' => Yii::t('app', 'Unfair Supplier Place Country Country Code'),
            'unfair_supplier_place_country_country_full_name' => Yii::t('app',
                'Unfair Supplier Place Country Country Full Name'),
            'unfair_supplier_place_zip' => Yii::t('app', 'Unfair Supplier Place Zip'),
            'unfair_supplier_place_place' => Yii::t('app', 'Unfair Supplier Place Place'),
            'unfair_supplier_place_email' => Yii::t('app', 'Unfair Supplier Place Email'),
            'unfair_supplier_founders_names_inn' => Yii::t('app', 'Unfair Supplier Founders Names Inn'),
            'unfair_supplier_founders_inn' => Yii::t('app', 'Unfair Supplier Founders Inn'),
            'purchase_purchase_number' => Yii::t('app', 'Purchase Purchase Number'),
            'purchase_purchase_object_info' => Yii::t('app', 'Purchase Purchase Object Info'),
            'purchase_placing_way_name' => Yii::t('app', 'Purchase Placing Way Name'),
            'purchase_protocol_date' => Yii::t('app', 'Purchase Protocol Date'),
            'purchase_lot_number' => Yii::t('app', 'Purchase Lot Number'),
            'purchase_document_name' => Yii::t('app', 'Purchase Document Name'),
            'purchase_document_date' => Yii::t('app', 'Purchase Document Date'),
            'purchase_document_number' => Yii::t('app', 'Purchase Document Number'),
            'not_oos_purchase' => Yii::t('app', 'Not Oos Purchase'),
            'contract_reg_num' => Yii::t('app', 'Contract Reg Num'),
            'contract_product_info' => Yii::t('app', 'Contract Product Info'),
            'contract_okpd_code' => Yii::t('app', 'Contract Okpd Code'),
            'contract_okpd_name' => Yii::t('app', 'Contract Okpd Name'),
            'contract_currency_code' => Yii::t('app', 'Contract Currency Code'),
            'contract_currency_name' => Yii::t('app', 'Contract Currency Name'),
            'contract_price' => Yii::t('app', 'Contract Price'),
            'contract_cancel_sign_date' => Yii::t('app', 'Contract Cancel Sign Date'),
            'contract_cancel_performance_date' => Yii::t('app', 'Contract Cancel Performance Date'),
            'contract_cancel_base_name' => Yii::t('app', 'Contract Cancel Base Name'),
            'contract_cancel_base_number' => Yii::t('app', 'Contract Cancel Base Number'),
            'contract_cancel_base_date' => Yii::t('app', 'Contract Cancel Base Date'),
            'contract_cancel_cancel_date' => Yii::t('app', 'Contract Cancel Cancel Date'),
        ];
    }

    public static function downloadFromFTP($connect, $contents, $path, $extractedPath)
    {
        $counter = 0;

        foreach ($contents as $file) {
            $local_file = $path . basename($file);
            if (file_exists($local_file)) {
                $counter++;
                continue;
            }
            $handle = fopen($local_file, 'w');
            if (ftp_fget($connect, $handle, $file, FTP_BINARY, 0)) {
                //Распаковка файлов
                $zip = new \ZipArchive;
                if ($zip->open($local_file) === true) {
                    $zip->extractTo($extractedPath);
                    $zip->close();
                } else {
                    echo "Архив не найден";
                }
            } else {
                echo "При скачке $file в $local_file произошла проблема\n";
            }
            $counter++;
            if ($counter % 10 == 0) {
                print_r("Обработано файлов: " . $counter);
                echo PHP_EOL;
            }

        }
        print_r("Скачивание по FTP завершено. Пропущено файлов: " . $counter);
        echo PHP_EOL;
    }

    public static function newRecord($xml)
    {

        $dBRecord = self::find();
        $xmlValues = [];
        if (isset($xml->unfairSupplier->registryNum)) {
            $xmlValues['registry_num'] = (string)$xml->unfairSupplier->registryNum;
            $dBRecord->andWhere(['registry_num' => $xmlValues['registry_num']]);
        }

        if (isset($xml->unfairSupplier->publishDate)) {
            $xmlValues['publish_date'] = date('Y-m-d', strtotime($xml->unfairSupplier->publishDate));
            $dBRecord->andWhere(['publish_date' => $xmlValues['publish_date']]);
        }

        if (isset($xml->unfairSupplier->state)) {
            $xmlValues['state'] = (string)$xml->unfairSupplier->state;
            $dBRecord->andWhere(['state' => $xmlValues['state']]);
        }

        if (isset($xml->unfairSupplier->publishOrg->regNum)) {
            $xmlValues['publish_org_reg_num'] = (string)$xml->unfairSupplier->publishOrg->regNum;
            $dBRecord->andWhere(['publish_org_reg_num' => $xmlValues['publish_org_reg_num']]);
        }
        if (isset($xml->unfairSupplier->publishOrg->consRegistryNum)) {
            $xmlValues['publish_org_cons_registry_num'] = (string)$xml->unfairSupplier->publishOrg->consRegistryNum;
            $dBRecord->andWhere(['publish_org_cons_registry_num' => $xmlValues['publish_org_cons_registry_num']]);
        }
        if (isset($xml->unfairSupplier->publishOrg->fullName)) {
            $xmlValues['publish_org_full_name'] = (string)$xml->unfairSupplier->publishOrg->fullName;
            $dBRecord->andWhere(['publish_org_full_name' => $xmlValues['publish_org_full_name']]);
        }

        if (isset($xml->unfairSupplier->createReason)) {
            $xmlValues['create_reason'] = (string)$xml->unfairSupplier->createReason;
            $dBRecord->andWhere(['create_reason' => $xmlValues['create_reason']]);
        }
        if (isset($xml->unfairSupplier->approveReason)) {
            $xmlValues['approve_reason'] = (string)$xml->unfairSupplier->approveReason;
            $dBRecord->andWhere(['approve_reason' => $xmlValues['approve_reason']]);
        }

        if (isset($xml->unfairSupplier->approveDate)) {
            $xmlValues['approve_date'] = date('Y-m-d',
                strtotime($xml->unfairSupplier->approveDate));
            $dBRecord->andWhere(['approve_date' => $xmlValues['approve_date']]);
        }
        if (isset($xml->unfairSupplier->customer->regNum)) {
            $xmlValues['customer_reg_num'] = (string)$xml->unfairSupplier->customer->regNum;
            $dBRecord->andWhere(['customer_reg_num' => $xmlValues['customer_reg_num']]);
        }
        if (isset($xml->unfairSupplier->customer->consRegistryNum)) {
            $xmlValues['customer_cons_registry_num'] = (string)$xml->unfairSupplier->customer->consRegistryNum;
            $dBRecord->andWhere(['customer_cons_registry_num' => $xmlValues['customer_cons_registry_num']]);
        }
        if (isset($xml->unfairSupplier->customer->fullName)) {
            $xmlValues['customer_full_name'] = (string)$xml->unfairSupplier->customer->fullName;
            $dBRecord->andWhere(['customer_full_name' => $xmlValues['customer_full_name']]);
        }
        if (isset($xml->unfairSupplier->customer->shortName)) {
            $xmlValues['customer_short_name'] = (string)$xml->unfairSupplier->customer->shortName;
            $dBRecord->andWhere(['customer_short_name' => $xmlValues['customer_short_name']]);
        }
        if (isset($xml->unfairSupplier->customer->postAddress)) {
            $xmlValues['customer_post_address'] = (string)$xml->unfairSupplier->customer->postAddress;
            $dBRecord->andWhere(['customer_post_address' => $xmlValues['customer_post_address']]);
        }
        if (isset($xml->unfairSupplier->customer->factAddress)) {
            $xmlValues['customer_fact_address'] = (string)$xml->unfairSupplier->customer->factAddress;
            $dBRecord->andWhere(['customer_fact_address' => $xmlValues['customer_fact_address']]);
        }
        if (isset($xml->unfairSupplier->customer->INN)) {
            $xmlValues['customer_inn'] = (string)$xml->unfairSupplier->customer->INN;
            $dBRecord->andWhere(['customer_inn' => $xmlValues['customer_inn']]);
        }
        if (isset($xml->unfairSupplier->customer->KPP)) {
            $xmlValues['customer_kpp'] = (string)$xml->unfairSupplier->customer->KPP;
            $dBRecord->andWhere(['customer_kpp' => $xmlValues['customer_kpp']]);
        }

        if (isset($xml->unfairSupplier->unfairSupplier->fullName)) {
            $xmlValues['unfair_supplier_full_name'] = (string)$xml->unfairSupplier->unfairSupplier->fullName;
            $dBRecord->andWhere(['unfair_supplier_full_name' => $xmlValues['unfair_supplier_full_name']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->fullName)) {
            $xmlValues['unfair_supplier_type'] = (string)$xml->unfairSupplier->unfairSupplier->type;
            $dBRecord->andWhere(['unfair_supplier_type' => $xmlValues['unfair_supplier_type']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->fullName)) {
            $xmlValues['unfair_supplier_firm_name'] = (string)$xml->unfairSupplier->unfairSupplier->firmName;
            $dBRecord->andWhere(['unfair_supplier_firm_name' => $xmlValues['unfair_supplier_firm_name']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->fullName)) {
            $xmlValues['unfair_supplier_inn'] = (string)$xml->unfairSupplier->unfairSupplier->inn;
            $dBRecord->andWhere(['unfair_supplier_inn' => $xmlValues['unfair_supplier_inn']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->fullName)) {
            $xmlValues['unfair_supplier_kpp'] = (string)$xml->unfairSupplier->unfairSupplier->kpp;
            $dBRecord->andWhere(['unfair_supplier_kpp' => $xmlValues['unfair_supplier_kpp']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->kladrType)) {
            $xmlValues['unfair_supplier_place_kladr_kladr_type'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->kladrType;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_kladr_type' => $xmlValues['unfair_supplier_place_kladr_kladr_type']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->kladrCode)) {
            $xmlValues['unfair_supplier_place_kladr_kladr_code'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->kladrCode;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_kladr_code' => $xmlValues['unfair_supplier_place_kladr_kladr_code']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->fullName)) {
            $xmlValues['unfair_supplier_place_kladr_full_name'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->fullName;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_full_name' => $xmlValues['unfair_supplier_place_kladr_full_name']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->subjectRF)) {
            $xmlValues['unfair_supplier_place_kladr_subject_rf'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->subjectRF;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_subject_rf' => $xmlValues['unfair_supplier_place_kladr_subject_rf']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->area)) {
            $xmlValues['unfair_supplier_place_kladr_area'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->area;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_area' => $xmlValues['unfair_supplier_place_kladr_area']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->city)) {
            $xmlValues['unfair_supplier_place_kladr_city'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->city;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_city' => $xmlValues['unfair_supplier_place_kladr_city']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->street)) {
            $xmlValues['unfair_supplier_place_kladr_street'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->street;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_street' => $xmlValues['unfair_supplier_place_kladr_street']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->building)) {
            $xmlValues['unfair_supplier_place_kladr_building'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->building;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_building' => $xmlValues['unfair_supplier_place_kladr_building']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->kladr->office)) {
            $xmlValues['unfair_supplier_place_kladr_office'] = (string)$xml->unfairSupplier->unfairSupplier->place->kladr->office;
            $dBRecord->andWhere(['unfair_supplier_place_kladr_office' => $xmlValues['unfair_supplier_place_kladr_office']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->country->countryCode)) {
            $xmlValues['unfair_supplier_place_country_country_code'] = (string)$xml->unfairSupplier->unfairSupplier->place->country->countryCode;
            $dBRecord->andWhere(['unfair_supplier_place_country_country_code' => $xmlValues['unfair_supplier_place_country_country_code']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->country->countryFullName)) {
            $xmlValues['unfair_supplier_place_country_country_full_name'] = (string)$xml->unfairSupplier->unfairSupplier->place->country->countryFullName;
            $dBRecord->andWhere(['unfair_supplier_place_country_country_full_name' => $xmlValues['unfair_supplier_place_country_country_full_name']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->zip)) {
            $xmlValues['unfair_supplier_place_zip'] = (string)$xml->unfairSupplier->unfairSupplier->place->zip;
            $dBRecord->andWhere(['unfair_supplier_place_zip' => $xmlValues['unfair_supplier_place_zip']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->place)) {
            $xmlValues['unfair_supplier_place_place'] = (string)$xml->unfairSupplier->unfairSupplier->place->place;
            $dBRecord->andWhere(['unfair_supplier_place_place' => $xmlValues['unfair_supplier_place_place']]);
        }
        if (isset($xml->unfairSupplier->unfairSupplier->place->email)) {
            $xmlValues['unfair_supplier_place_email'] = (string)$xml->unfairSupplier->unfairSupplier->place->email;
            $dBRecord->andWhere(['unfair_supplier_place_email' => $xmlValues['unfair_supplier_place_email']]);
        }

        if (isset($xml->unfairSupplier->purchase->purchaseNumber)) {
            $xmlValues['purchase_purchase_number'] = (string)$xml->unfairSupplier->purchase->purchaseNumber;
            $dBRecord->andWhere(['purchase_purchase_number' => $xmlValues['purchase_purchase_number']]);
        }
        if (isset($xml->unfairSupplier->purchase->purchaseObjectInfo)) {
            $xmlValues['purchase_purchase_object_info'] = (string)$xml->unfairSupplier->purchase->purchaseObjectInfo;
            $dBRecord->andWhere(['purchase_purchase_object_info' => $xmlValues['purchase_purchase_object_info']]);
        }
        if (isset($xml->unfairSupplier->purchase->placingWayName)) {
            $xmlValues['purchase_placing_way_name'] = (string)$xml->unfairSupplier->purchase->placingWayName;
            $dBRecord->andWhere(['purchase_placing_way_name' => $xmlValues['purchase_placing_way_name']]);
        }
        if (isset($xml->unfairSupplier->purchase->protocolDate)) {
            $xmlValues['purchase_protocol_date'] = date('Y-m-d',
                strtotime($xml->unfairSupplier->purchase->protocolDate));
            $dBRecord->andWhere(['purchase_protocol_date' => $xmlValues['purchase_protocol_date']]);
        }
        if (isset($xml->unfairSupplier->purchase->lotNumber)) {
            $xmlValues['purchase_lot_number'] = (string)$xml->unfairSupplier->purchase->lotNumber;
            $dBRecord->andWhere(['purchase_lot_number' => $xmlValues['purchase_lot_number']]);
        }
        if (isset($xml->unfairSupplier->purchase->document->name)) {
            $xmlValues['purchase_document_name'] = (string)$xml->unfairSupplier->purchase->document->name;
            $dBRecord->andWhere(['purchase_document_name' => $xmlValues['purchase_document_name']]);
        }
        if (isset($xml->unfairSupplier->purchase->document->date)) {
            $xmlValues['purchase_document_date'] = date('Y-m-d',
                strtotime($xml->unfairSupplier->purchase->document->date));
            $dBRecord->andWhere(['purchase_document_date' => $xmlValues['purchase_document_date']]);
        }
        if (isset($xml->unfairSupplier->purchase->document->number)) {
            $xmlValues['purchase_document_number'] = (string)$xml->unfairSupplier->purchase->document->number;
            $dBRecord->andWhere(['purchase_document_number' => $xmlValues['purchase_document_number']]);
        }
        if (isset($xml->unfairSupplier->notOosPurchase)) {
            $xmlValues['not_oos_purchase'] = (string)$xml->unfairSupplier->notOosPurchase;
            $dBRecord->andWhere(['not_oos_purchase' => $xmlValues['not_oos_purchase']]);
        }

        if (isset($xml->unfairSupplier->contract->regNum)) {
            $xmlValues['contract_reg_num'] = (string)$xml->unfairSupplier->contract->regNum;
            $dBRecord->andWhere(['contract_reg_num' => $xmlValues['contract_reg_num']]);
        }
        if (isset($xml->unfairSupplier->contract->productInfo)) {
            $xmlValues['contract_product_info'] = (string)$xml->unfairSupplier->contract->productInfo;
            $dBRecord->andWhere(['contract_product_info' => $xmlValues['contract_product_info']]);
        }
        if (isset($xml->unfairSupplier->contract->OKPD->code)) {
            $xmlValues['contract_okpd_code'] = (string)$xml->unfairSupplier->contract->OKPD->code;
            $dBRecord->andWhere(['contract_okpd_code' => $xmlValues['contract_okpd_code']]);
        }
        if (isset($xml->unfairSupplier->contract->OKPD->name)) {
            $xmlValues['contract_okpd_name'] = (string)$xml->unfairSupplier->contract->OKPD->name;
            $dBRecord->andWhere(['contract_okpd_name' => $xmlValues['contract_okpd_name']]);
        }
        if (isset($xml->unfairSupplier->contract->currency->code)) {
            $xmlValues['contract_currency_code'] = (string)$xml->unfairSupplier->contract->currency->code;
            $dBRecord->andWhere(['contract_currency_code' => $xmlValues['contract_currency_code']]);
        }
        if (isset($xml->unfairSupplier->contract->currency->name)) {
            $xmlValues['contract_currency_name'] = (string)$xml->unfairSupplier->contract->currency->name;
            $dBRecord->andWhere(['contract_currency_name' => $xmlValues['contract_currency_name']]);
        }
        if (isset($xml->unfairSupplier->contract->price)) {
            $xmlValues['contract_price'] = (string)$xml->unfairSupplier->contract->price;
            $dBRecord->andWhere(['contract_price' => $xmlValues['contract_price']]);
        }
        if (isset($xml->unfairSupplier->contract->cancel->signDate)) {
            $xmlValues['contract_cancel_sign_date'] = date('Y-m-d',
                strtotime($xml->unfairSupplier->contract->cancel->signDate));
            $dBRecord->andWhere(['contract_cancel_sign_date' => $xmlValues['contract_cancel_sign_date']]);
        }
        if (isset($xml->unfairSupplier->contract->cancel->performanceDate)) {
            $xmlValues['contract_cancel_performance_date'] = date('Y-m-d',
                strtotime($xml->unfairSupplier->contract->cancel->performanceDate));
            $dBRecord->andWhere(['contract_cancel_performance_date' => $xmlValues['contract_cancel_performance_date']]);
        }
        if (isset($xml->unfairSupplier->contract->cancel->base->name)) {
            $xmlValues['contract_cancel_base_name'] = (string)$xml->unfairSupplier->contract->cancel->base->name;
            $dBRecord->andWhere(['contract_cancel_base_name' => $xmlValues['contract_cancel_base_name']]);
        }
        if (isset($xml->unfairSupplier->contract->cancel->base->number)) {
            $xmlValues['contract_cancel_base_number'] = (string)$xml->unfairSupplier->contract->cancel->base->number;
            $dBRecord->andWhere(['contract_cancel_base_number' => $xmlValues['contract_cancel_base_number']]);
        }
        if (isset($xml->unfairSupplier->contract->cancel->base->date)) {
            $xmlValues['contract_cancel_base_date'] = date('Y-m-d',
                strtotime($xml->unfairSupplier->contract->cancel->base->date));
            $dBRecord->andWhere(['contract_cancel_base_date' => $xmlValues['contract_cancel_base_date']]);
        }
        if (isset($xml->unfairSupplier->contract->cancel->cancelDate)) {
            $xmlValues['contract_cancel_cancel_date'] = date('Y-m-d',
                strtotime($xml->unfairSupplier->contract->cancel->cancelDate));
            $dBRecord->andWhere(['contract_cancel_cancel_date' => $xmlValues['contract_cancel_cancel_date']]);
        }

        $result = $dBRecord->one();

        if (!$result) {
            $unfairSupplier = new self();
            $unfairSupplier->attributes = $xmlValues;

            if (!$unfairSupplier->save()) {
                print_r($unfairSupplier->getErrors());
                var_dump($xml);
                die;
            }
        }
    }

    public function fields()
    {
        return [
            'id',
            'table_name' => function () {
                return self::tableName();
            },
            'approve_date',
            'state',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnfairSupplierFounders()
    {
        return $this->hasMany(UnfairSupplierFounder::className(), ['unfair_supplier_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UnfairSupplierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UnfairSupplierQuery(get_called_class());
    }
}
