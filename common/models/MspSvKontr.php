<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msp_sv_kontr".
 *
 * @property integer $id
 * @property string $naim_ul_zd
 * @property string $inn_ul_zd
 * @property string $predm_kontr
 * @property string $nom_kontr_reestr
 * @property string $data_kontr
 * @property integer $msp_dokument_id
 *
 * @property MspDokument $mspDokument
 */
class MspSvKontr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msp_sv_kontr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['naim_ul_zd', 'inn_ul_zd', 'predm_kontr', 'nom_kontr_reestr'], 'string'],
            [['data_kontr'], 'safe'],
            [['msp_dokument_id'], 'required'],
            [['msp_dokument_id'], 'integer'],
            [
                ['msp_dokument_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MspDokument::className(),
                'targetAttribute' => ['msp_dokument_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naim_ul_zd' => Yii::t('app', 'Naim Ul Zd'),
            'inn_ul_zd' => Yii::t('app', 'Inn Ul Zd'),
            'predm_kontr' => Yii::t('app', 'Predm Kontr'),
            'nom_kontr_reestr' => Yii::t('app', 'Nom Kontr Reestr'),
            'data_kontr' => Yii::t('app', 'Data Kontr'),
            'msp_dokument_id' => Yii::t('app', 'Msp Dokument ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMspDokument()
    {
        return $this->hasOne(MspDokument::className(), ['id' => 'msp_dokument_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MspSvKontrQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MspSvKontrQuery(get_called_class());
    }
}
