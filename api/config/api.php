<?php
 
// $db     = require(__DIR__ . '/../../config/db.php');
// $params = require(__DIR__ . '/../../config/params.php');
 
$config = [
    'id' => 'app-api',
    'name' => 'zcb',
    'basePath' => dirname(__DIR__).'/..',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'format' => yii\web\Response::FORMAT_JSON,
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/api.log',
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                //[
                //    'class' => 'yii\rest\UrlRule',
                //    'controller' => ['v1/waybill'],
                //    'only' => ['index', 'view']
                //],
                //[
                //    'class' => 'yii\rest\UrlRule',
                //    'controller' => ['v1/calculation'],
                //    'only' => ['index']
                //],
                
            ],
        ], 
        'user' => [
            'identityClass' => 'api\modules\v1\models\User',
            'enableAutoLogin' => false,
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=zcb_local',
            'username' => 'postgres',
            'password' => 'alert007',
            'charset' => 'utf8',
        ],
        'db_zakupki' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=zakupki',
            'username' => 'postgres',
            'password' => 'alert007',
            'charset' => 'utf8',
        ],
        'db_kaleka' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=kaleka',
            'username' => 'postgres',
            'password' => 'alert005',
            'charset' => 'utf8',
        ],
        /*
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=admin_zhb',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
        'db_zakupki' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=complex_zakupki_gov',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ],
         */
    ],
    'modules' => [
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
    ],
    // 'params' => $params,
];
 
return $config;