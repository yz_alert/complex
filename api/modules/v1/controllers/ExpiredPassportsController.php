<?php

namespace api\modules\v1\controllers;

use common\models\CompanySearchResult;
use common\models\ExpiredPassports;
use Yii;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Response;
use common\models\CompanySearch;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class ExpiredPassportsController extends ActiveController
{
    public $modelClass = 'common\models\ExpiredPassports';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        $series = Yii::$app->request->get('series');
        $number = Yii::$app->request->get('number');
        //Если введено пустое значение inn
        if ($series == null || $series == '' || $number == null || $number == '') {
            return ['reply' => ['1' => 'Некорректное значение']];
        }
        else{
            $model = ExpiredPassports::findOne(['series' => $series, 'number' => $number]);
            if ($model) {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $model,
                ];
            } else {
                return ['reply' => ['2' => 'Отсутствует']];
            }
        }
    }
}