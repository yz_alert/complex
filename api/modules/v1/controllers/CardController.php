<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\web\Response;
use common\models\CompanySearch;
use common\models\CompanyCard;
use common\models\CompanySearchResult;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class CardController extends ActiveController
{
    public $modelClass = 'common\models\CompanyCard';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        $query = Yii::$app->request->get('query');
        /*
        $companySearchff = CompanySearch::findOne(['request_string' => $query]);
        //print_r($companySearchff);
        if($companySearchff){
            //print_r($companySearchff->total_records);
            if($companySearchff->total_records == 0){

            }
        }
        */
        //ввели ИНН
        if (strlen($query) == 12 || strlen($query) == 10) {

            //Поиск существования такой записи в CompanyCard
            $model = CompanyCard::findOne(['inn' => $query]);
            //Если запись не существует по ИНН, то пробуем её найти по ИННИП
            if (!$model) {
                $model = CompanyCard::findOne(['innfl' => $query]);
            }
            //Если запись существует по ИННФЛ, то возвращаем её модель
            if ($model) {
                return $model;
                //Если ничего не найдено
            } else {
                $companySearchResult = CompanySearchResult::findOne(['inn' => $query]);
                //Если мы уже имеем результат поиска по этому ИНН в CompanySearchResult
                if ($companySearchResult) {
                    //получаем карточку по ОГРН из таблицы CompanySearchResult->ogrn
                    CompanyCard::cardWithZcb($companySearchResult->ogrn);
                    return CompanyCard::findOne(['inn' => $query]);
                }
                //Отправляем запрос на ЗЧБ по INN
                $tempCompanySearch = CompanySearch::searchWithZcb($query);
                /*
                if($tempCompanySearch->total_records === 0){
                    return ['error' => '0 records'];
                }
                */
                CompanyCard::cardWithZcb($tempCompanySearch["data"]->ogrn);
                $returnModel = CompanyCard::findOne(['inn' => $query]);
                if ($returnModel) {
                    return $returnModel;
                } else {
                    return CompanyCard::findOne(['innfl' => $query]);
                }
            }
        }
        //ввели ОГРН
        if (strlen($query) == 13) {
            //Поиск существования такой записи у нас в БД
            $model = CompanyCard::findOne(['ogrn' => $query]);
            //Если запись существует, то возвращаем её модель  CompanySearchResult
            if ($model) {
                return $model;
                //Если модель не существует
            } else {
                CompanyCard::cardWithZcb($query);
                return CompanyCard::findOne(['ogrn' => $query]);
            }
        }
        return ['error' => 'inn has been require'];
    }
}