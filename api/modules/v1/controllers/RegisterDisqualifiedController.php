<?php

namespace api\modules\v1\controllers;

use common\models\RegisterDisqualified;
use Yii;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class RegisterDisqualifiedController extends ActiveController
{
    public $modelClass = 'common\models\RegisterDisqualified';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {

        $fullName = Yii::$app->request->get('full_name');
        $dateOfBirth = Yii::$app->request->get('date_of_birth');
        if (Yii::$app->request->get('date_of_birth')) {
            $dateOfBirth = date('d.m.Y', strtotime(Yii::$app->request->get('date_of_birth')));
        }

        $number = Yii::$app->request->get('number');
        $organization_inn = Yii::$app->request->get('organization_inn');

        $query = RegisterDisqualified::find();

        if (!$fullName &&
            !$dateOfBirth &&
            !$number) {
            return ['reply' => ['1' => 'Некорректное значение']];
        }

        if ($fullName) {
            $query->andWhere(['ilike', 'full_name', $fullName]);
        }
        if ($dateOfBirth) {
            $query->andWhere(['=', 'date_of_birth', $dateOfBirth]);
        }

        if ($number) {
            $query->andWhere(['ilike', 'number', $number]);
        }
        if ($organization_inn) {
            $query->andWhere(['ilike', 'organization_inn', $organization_inn]);
        }
        $result = $query->all();

        if ($result) {
            if ($query->count() > 1) {
                return [
                    'reply' => ['4' => 'Присутствует, множественное'],
                    'data' => $result,
                ];
            } else {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $result,
                ];
            }
        } else {
            return ['reply' => ['2' => 'Отсутствует']];
        }
    }
}