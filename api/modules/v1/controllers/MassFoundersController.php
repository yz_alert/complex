<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\web\Response;
use common\models\CompanySearch;
use common\models\CompanyCard;
use common\models\CompanySearchResult;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\MassFounders;

class MassFoundersController extends ActiveController
{
    public $modelClass = 'common\models\MassFounders';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        $surname = Yii::$app->request->get('surname');
        $name = Yii::$app->request->get('name');
        $patronymic = Yii::$app->request->get('patronymic');
        $inn = Yii::$app->request->get('inn');

        $query = MassFounders::find();
        if (!$surname &&
            !$name &&
            !$patronymic &&
            !$inn) {
            return ['reply' => ['1' => 'Некорректное значение']];
        }
        if ($surname) {
            $query->andWhere(['ilike', 'surname', $surname]);
        }
        if ($name) {
            $query->andWhere(['ilike', 'name', $name]);
        }
        if ($patronymic) {
            $query->andWhere(['ilike', 'patronymic', $patronymic]);
        }
        if ($inn) {
            $query->andWhere(['ilike', 'inn', $inn]);
        }
        $result = $query->all();
        if ($result) {
            if ($query->count() > 1) {
                return [
                    'reply' => ['4' => 'Присутствует, множественное'],
                    'data' => $result,
                ];
            } else {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $result,
                ];
            }
        } else {
            return ['reply' => ['2' => 'Отсутствует']];
        }
    }
}