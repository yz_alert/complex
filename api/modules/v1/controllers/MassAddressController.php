<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Response;
use common\models\CompanySearch;
use common\models\CompanyCard;
use common\models\CompanySearchResult;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\MassAddress;

class MassAddressController extends ActiveController
{
    public $modelClass = 'common\models\MassAddress';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        $region = Yii::$app->request->get('region');
        $location = Yii::$app->request->get('location');
        $city = Yii::$app->request->get('city');
        $settlement = Yii::$app->request->get('settlement');
        $street = Yii::$app->request->get('street');
        $house_number = Yii::$app->request->get('house_number');
        $corpus_number = Yii::$app->request->get('corpus_number');
        $office_number = Yii::$app->request->get('office_number');

        $query = MassAddress::find();
        if (!$region &&
            !$location &&
            !$city &&
            !$settlement &&
            !$street &&
            !$house_number &&
            !$corpus_number &&
            !$office_number) {
            return ['reply' => ['1' => 'Некорректное значение']];
        }
        if ($region) {
            $query->andWhere(['ilike', 'region', $region]);
        }
        if ($location) {
            $query->andWhere(['ilike', 'location', $location]);
        }
        if ($city) {
            $query->andWhere(['ilike', 'city', $city]);
        }
        if ($settlement) {
            $query->andWhere(['ilike', 'settlement', $settlement]);
        }
        if ($street) {
            $query->andWhere(['ilike', 'street', $street]);
        }
        if ($house_number) {
            $query->andWhere(['ilike', 'house_number', $house_number]);
        }
        if ($corpus_number) {
            $query->andWhere(['ilike', 'corpus_number', $corpus_number]);
        }
        if ($office_number) {
            $query->andWhere(['ilike', 'office_number', $office_number]);
        }
        $result = $query->all();
        if ($result) {
            if ($query->count() > 1) {
                return [
                    'reply' => ['4' => 'Присутствует, множественное'],
                    'data' => $result,
                ];
            } else {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $result,
                ];
            }
        } else {
            return ['reply' => ['2' => 'Отсутствует']];
        }
    }
}