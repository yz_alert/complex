<?php

namespace api\modules\v1\controllers;

use common\models\TerroristsUlRus;
use Yii;
use yii\web\Response;
use common\models\CompanySearch;
use common\models\CompanyCard;
use common\models\CompanySearchResult;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\DisqualifiedPersons;

class TerroristsUlRusController extends ActiveController
{
    public $modelClass = 'common\models\TerroristsUlRus';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        $query = Yii::$app->request->get('query');
        if (strlen($query) > 2) {
            if ($query) {
                $searchQuery = TerroristsUlRus::find()
                    ->Where(['ilike', 'name', $query])
                    ->orWhere(['ilike', 'additional_info', $query]);
                $result = $searchQuery->all();
                if ($result) {
                    if ($searchQuery->count() > 1) {
                        return [
                            'reply' => ['4' => 'Присутствует, множественное'],
                            'data' => $result,
                        ];
                    } else {
                        return [
                            'reply' => ['3' => 'Присутствует'],
                            'data' => $result,
                        ];
                    }
                } else {
                    return ['reply' => ['2' => 'Отсутствует']];
                }
            }
        }
        return ['reply' => ['1' => 'Некорректное значение']];
    }
}