<?php

namespace api\modules\v1\controllers;

use common\models\TerroristsFlRus;
use Yii;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\DisqualifiedPersons;

class TerroristsFlRusController extends ActiveController
{
    public $modelClass = 'common\models\TerroristsFlRus';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {

        $full_name = Yii::$app->request->get('full_name');
        $date_of_birth = date('Y-m-d', strtotime(Yii::$app->request->get('date_of_birth')));
        $place_of_birth = Yii::$app->request->get('place_of_birth');

        if (!$full_name &&
            !$date_of_birth &&
            !$place_of_birth) {
            return ['reply' => ['1' => 'Некорректное значение']];
        }

        $query = TerroristsFlRus::find();
        if ($full_name) {
            $query->andWhere(['ilike', 'full_name', $full_name]);
        }
        if ($date_of_birth) {
            $query->andWhere(['=', 'date_of_birth', $date_of_birth]);
        }
        if ($place_of_birth) {
            $query->andWhere(['ilike', 'place_of_birth', $place_of_birth]);
        }
        $result = $query->all();
        if ($result) {
            if ($query->count() > 1) {
                return [
                    'reply' => ['4' => 'Присутствует, множественное'],
                    'data' => $result,
                ];
            } else {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $result,
                ];
            }
        } else {
            return ['reply' => ['2' => 'Отсутствует']];
        }
    }
}