<?php

namespace api\modules\v1\controllers;

use common\models\Gks;
use Yii;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class GksController extends ActiveController
{
    public $modelClass = 'common\models\Gks';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        $inn = Yii::$app->request->get('inn');
        $year = (integer)Yii::$app->request->get('year');

        if (!is_null($inn) && !is_null($year)) {

            if ($year >= 2012 && $year <= 2017) {
                $model = Gks::findOne(['inn' => $inn, 'year' => $year]);
                if ($model) {
                    return $model;
                } else {
                    $model = new Gks();
                    $model->year = $year;
                    $model->inn = $inn;
                    $model->response = Gks::getGksAnswer($year, $inn);
                    $model->save();
                    return Gks::findOne(['inn' => $inn, 'year' => $year]);
                }
            } else {
                return ['error' => 'year should be in the range 2012-2017'];
            }
        }
        //elseif (!is_null($inn) && is_null($year)){
        //    Gks::getGksAnswer($year,$inn);
        //}

        return ['error' => 'invalid values'];
    }
}