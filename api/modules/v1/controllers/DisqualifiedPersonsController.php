<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\web\Response;
use common\models\CompanySearch;
use common\models\CompanyCard;
use common\models\CompanySearchResult;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\DisqualifiedPersons;

class DisqualifiedPersonsController extends ActiveController
{
    public $modelClass = 'common\models\DisqualifiedPersons';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        $query = Yii::$app->request->get('query');
        //ИНН
        if (strlen($query) == 12 || strlen($query) == 10) {
            $model = DisqualifiedPersons::findOne(['inn' => $query]);
            if ($model) {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $model,
                ];
            } else {
                return ['reply' => ['2' => 'Отсутствует']];
            }
        } //ОГРН
        elseif (strlen($query) == 13) {
            $model = DisqualifiedPersons::findOne(['ogrn' => $query]);
            if ($model) {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $model,
                ];
            } else {
                return ['reply' => ['2' => 'Отсутствует']];
            }
        }
        return ['reply' => ['1' => 'Некорректное значение']];
    }
}