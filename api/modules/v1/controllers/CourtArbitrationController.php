<?php

namespace api\modules\v1\controllers;

use common\models\CompanyCard;
use common\models\CompanySearchResult;
use common\models\CourtArbitration;
use Yii;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Response;
use common\models\CompanySearch;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class CourtArbitrationController extends ActiveController
{
    public $modelClass = 'common\models\CourtArbitration';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        $ogrn = Yii::$app->request->get('ogrn');
        //Если введено пустое значение ogrn
        if ($ogrn == null || $ogrn == '') {
            return ['reply' => ['1' => 'Некорректное значение']];
        }
        else{
            //Поиск существования такой записи у нас в БД
            $model = CourtArbitration::findOne(['request_string' => $ogrn]);
            //Если запись существует, то возвращаем её модель  CompanySearchResult
            if ($model) {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $model,
                ];
                //Если модель не существует
            } else {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => CourtArbitration::courtArbitrationWithZcb($ogrn),
                ];
            }
        }
    }
}