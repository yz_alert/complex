<?php

namespace api\modules\v1\controllers;

use common\models\UnfairSupplier223;
use Yii;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class UnfairSupplier223Controller extends ActiveController
{
    public $modelClass = 'common\models\UnfairSupplier223';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        if (isset($_GET['name']) && $_GET['name'] != '') {
            $name = Yii::$app->request->get('name');
        }
        if (isset($_GET['inn']) && $_GET['inn'] != '') {
            $inn = Yii::$app->request->get('inn');
        }

        $query = UnfairSupplier223::find();
        if (!isset($name) &&
            !isset($inn)) {
            return ['reply' => ['1' => 'Некорректное значение']];
        }

        if (isset($name)) {
            $query->andWhere(['ilike', 'cai_supplier_name', $name]);
        }
        if (isset($inn)) {
            $query->andWhere(['=', 'cai_supplier_inn', $inn]);
        }

        $result = $query->all();
        if ($result) {
            if ($query->count() > 1) {
                return [
                    'reply' => ['4' => 'Присутствует, множественное'],
                    'data' => $result,
                ];
            } else {
                return [
                    'reply' => ['3' => 'Присутствует'],
                    'data' => $result,
                ];
            }
        } else {
            return ['reply' => ['2' => 'Отсутствует']];
        }
    }
}