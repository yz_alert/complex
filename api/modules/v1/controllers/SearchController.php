<?php

namespace api\modules\v1\controllers;

use common\models\CompanySearchResult;
use Yii;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Response;
use common\models\CompanySearch;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class SearchController extends ActiveController
{
    public $modelClass = 'common\models\CompanySearch';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        $inn = Yii::$app->request->get('inn');
        //Если введено пустое значение inn
        if ($inn == null || $inn == '') {
            return ['reply' => ['1' => 'Некорректное значение']];
        }
        else{
            //Поиск существования такой записи у нас в БД
            $model = CompanySearchResult::findOne(['inn' => $inn]);
            //Если запись существует, то возвращаем её модель  CompanySearchResult
            if ($model) {
                return [
                    'reply' => ['4' => 'Присутствует, множественное'],
                    'data' => $model,
                ];
            //Если модель не существует
            } else {
                //Отправляем запрос на ЗЧБ по INN
                return CompanySearch::searchWithZcb($inn);
            }
        }
    }
}