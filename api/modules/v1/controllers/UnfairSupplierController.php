<?php

namespace api\modules\v1\controllers;

use common\models\UnfairSupplier44;
use common\models\UnfairSupplier223;
use common\models\UnfairSupplier615;
use Yii;
use yii\web\Response;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class UnfairSupplierController extends ActiveController
{
    public $modelClass = 'common\models\UnfairSupplier44';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareIndexData'];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //$behaviors['authenticator'] = [
        //    'class' => CompositeAuth::className(),
        //    'authMethods' => [
        //        HttpBasicAuth::className(),
        //        HttpBearerAuth::className(),
        //        QueryParamAuth::className(),
        //    ],
        //];
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function prepareIndexData()
    {
        //Отправили запрос на наш сервер с параметром GET
        if (isset($_GET['inn']) && $_GET['inn'] != '') {
            $inn = Yii::$app->request->get('inn');
        }
//590851261353
        $query44 = UnfairSupplier44::find();
        $query223 = UnfairSupplier223::find();
        $query615 = UnfairSupplier615::find();
        if (!isset($inn)) {
            return ['reply' => ['1' => 'Некорректное значение']];
        }
        if (isset($inn)) {
            $query44->andWhere(['=', 'unfair_supplier_inn', $inn]);
            $query223->andWhere(['=', 'cai_supplier_inn', $inn]);
            $query615->andWhere(['=', 'unfair_supplier_info_tax_payer_code', $inn]);
        }

        $result44 = $query44->all();
        $result223 = $query223->all();
        $result615 = $query615->all();

        $commonResult = [];

        if ($result44) {
            if ($query44->count() > 1) {
                $commonResult = array_merge($commonResult, $result44);
                //$commonResult = [
                //'reply' => ['4' => 'Присутствует, множественное по 44ФЗ'],
                //'data' => $result44,
                //];
            } else {
                $commonResult = array_merge($commonResult, $result44);
                //$commonResult = [
                //'reply' => ['3' => 'Присутствует по 44ФЗ'],
                //'data' => $result44,
                //];
            }
        }

        //return $commonResult;

        if ($result223) {
            if ($query223->count() > 1) {
                $commonResult = array_merge($commonResult, $result223);
                //$commonResult = [
                //'reply' => ['4' => 'Присутствует, множественное по 223ФЗ'],
                //'data' => $result223,
                //];
            } else {
                $commonResult = array_merge($commonResult, $result223);
                //$commonResult = [
                //'reply' => ['3' => 'Присутствует по 223ФЗ'],
                //'data' => $result223,
                //];
            }
        }

        if ($result615) {
            if ($query615->count() > 1) {
                $commonResult = array_merge($commonResult, $result615);
                //$commonResult = [
                //'reply' => ['4' => 'Присутствует, множественное по 615ФЗ'],
                //'data' => $result615,
                //];
            } else {
                $commonResult = array_merge($commonResult, $result615);
                //$commonResult = [
                //'reply' => ['3' => 'Присутствует по 615ФЗ'],
                //'data' => $result615,
                //];
            }
        }

        if ($commonResult) {
            return $commonResult;
        }

        return ['reply' => ['2' => 'Отсутствует по 44,223,615 ФЗ']];
    }
}














































